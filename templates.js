$(document).ready(function() {
    //Template Engine
    //$("#tpl-sidebar").css({'height':($("#tpl-body").height()+'px')});
    //$('#tpl-sidebar').height($(window).height()-280);

    //Change text as you type
    $('.change-text').keyup(function(){
        var target = $(this).attr('data-target');
        $(target).html($(this).val());
        if(target === ".regbody_header_text"){
            // show registratio page
            showEditedDefaultTemplate("#registration-page");
        }
    });
    $('.cke_editable').keyup(function(){
        //var target = $(this).attr('data-target');
        $('.body_copy_text').html($(this).val());

    });

    //Reload template values  ::
    var oEvent_header = $('#event_header');
    if(oEvent_header.val() !=='' && oEvent_header.val() !== undefined){
        var target = $(oEvent_header).attr('data-target');
        $(target).html($(oEvent_header).val());
    }


    $('#submit-content-edit-template-form').click(function() {
        $('#content-edit-template-form').submit();
        return false;
    });

    //Fix lefthand side
    /*
    $(window).scroll(function(){
        if ($(this).scrollTop() > 132) {
            $('#tpl-body').addClass('fixed');
        } else {
            $('#tpl-body').removeClass('fixed');
        }
    });
    */

    // html content and selector is passed which is supposed to be removed
    var removeElements = function(text, selector) {
        var wrapped = $("<div>"+text+"</div>");
        wrapped.find(selector).remove();
        return wrapped.html();
    }   

    function SelectText(element) {
        var doc = document, text = doc.getElementsByClassName(element)[0], range, selection;

        if (doc.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            selection = window.getSelection();        
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }

    function escapeHtml(text) {
      return text
          .replace(/&/g, "&amp;")
          .replace(/</g, "&lt;")
          .replace(/>/g, "&gt;")
          .replace(/"/g, "&quot;")
          .replace(/'/g, "&#039;");
    }

    // download template file
    $("#download-html-file").click(function() {
        
        var template_type = "email"; //default template is email
        var html_content = "";
        var custom_config = !$(this).hasClass("default-template-webinar"); // if custom config the true

        if($('.template-page').is(':visible')) { //if page template is selected
            template_type = "page"; 
        } 

        if(custom_config) { // if custom configuration
            $("#custom_download_url").html("");
        }

        if(template_type == "email") {  //if template type is email

            inner_class = ".template-email:visible";
            if(custom_config) {
                inner_class = ".template-email:visible .tpl-container"; 
            }

            var all = $(inner_class).map(function() {
                return this.innerHTML;
            }).get();  

            html_content = all.join(""); // all modules tables combined

            clean_html = removeElements(html_content, "div");  // remove all div's 
            clean_html = removeElements(clean_html, "script"); // remove all script tags
        } else {

            inner_class = ".template-page:visible";
            if(custom_config) {
                inner_class = ".template-page:visible .tpl-container";
            }

            var all = $(inner_class).map(function() {
                return this.innerHTML;
            }).get();  

            html_content = all.join(""); // all modules tables combined

            clean_html = removeElements(html_content, "script"); // remove all script tags
        }

        var url = '/includes/_download_template.php';
        
        $.ajax({
            "url" : url,
            "type" : "post",
            "dataType" : "text",
            "data" : "content="+escape(clean_html)+"&template_type="+template_type,
            "success" : function(data) {
                html_head = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                +"<html xmlns='http://www.w3.org/1999/xhtml'>"
                +"<head>"
                +"<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>"
                +"<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
                +"<title>Template</title>";

                data = html_head + data;

                bootbox.dialog({
                  message: '<a class="btn-green plain pull-right" id="tpl-select-all" href="javascript:void(0)">Select All</a><br><script type="syntaxhighlighter" id="syntaxhigh" class="brush: js"><![CDATA['+data+"]]></script>"+"<div id='custom_download_url'></div>",
                  title: "HTML Generated For Third-Party Tools"
                }); // open dialog box with html code
                SyntaxHighlighter.highlight();

                if(custom_config) { // if custom config
                    if(template_type == "email") {  //if template type is email
                        $( ".upload-div .files p" ).each(function( index ) {
                            // header and content images
                            var imageLink = ($(this).html()).split("</a>");
                            $("#custom_download_url").append("<br>"+imageLink[1]+"</a>");
                        });
                        $( ".template-email:visible .tpl-container .fup-overlay" ).each(function( index ) {
                            // modules images
                            var imageLink = ($(this).html()).split("</a>");
                            // $("#custom_download_url").append("<br><a href='http://"+window.location.hostname+"/server/php/files/"+$("#user_id").val()+"/"+$( this ).text()+"' target='_blank'>"+"http://"+window.location.hostname+"/server/php/files/"+$("#user_id").val()+"/"+$( this ).text()+"</a>");
                            $("#custom_download_url").append("<br>"+imageLink[1]+"</a>");
                        });
                    } else {
                       $( ".template-page:visible .tpl-container .fup-overlay" ).each(function( index ) {
                            var imageLink = ($(this).html()).split("</a>");
                            // $("#custom_download_url").append("<br><a href='http://"+window.location.hostname+"/server/php/files/"+$("#user_id").val()+"/"+$( this ).text()+"' target='_blank'>"+"http://"+window.location.hostname+"/server/php/files/"+$("#user_id").val()+"/"+$( this ).text()+"</a>");
                            $("#custom_download_url").append("<br>"+imageLink[1]+"</a>");
                        }); 
                    }
                }

                $("#tpl-select-all").click(function(){
                    SelectText('code');
                });

            }
        });

        return false;
    });
    //Toggle preview
    $('#tpl-preview').click(function() {
        $('#tpl-sidebar').toggleClass('preview');
        $('#tpl-body').toggleClass('preview');

        $('.asset-preview-page').toggleClass('preview');
        $('.template-page').toggleClass('preview');

        $('.preview-mode').toggleClass('preview');

        $('.thumbnail-block h2').toggleClass('preview');
        $('.thumbnail-ul').toggleClass('preview');
        $('.image-selector').toggleClass('preview');
        $('.thumbnail-block').toggleClass('preview');
        $('.upload-div').toggleClass('preview');
        $('.button-refresh').toggleClass('preview');
        $('.button-active-hf').toggleClass('preview');
        $('.button-move').toggleClass('preview');
        $('.button-remove').toggleClass('preview');

        $('.section-note').toggleClass('preview');
        $('.button-mod-remove').toggleClass('preview');
        $('.tpl-rp-add-btn').toggleClass('preview');

        $('.fup-overlay').toggleClass('preview');
        $('.duplicate-header-footer-btns').toggleClass('preview');

        /* Registration form */
        $('#addCustomFieldsBtn').toggleClass('preview');
        $(".removeSocial").toggleClass('preview');
        $(".add-social-icon").toggleClass('preview');

        if ($(this).text() == 'Preview Mode') {
            $(this).text('Edit Mode');
            $("#download-html-file").show();
        } else {
            $(this).text('Preview Mode');
            $("#download-html-file").hide();
        }

        return false;
    });

    //Change which default template displays
    $('select#preview-select').on('change', function() {
        var showme = this.value;
        $('.template-block').hide();
        $(showme).show();
        $("#tpl-sidebar").css({'max-height':($("#tpl-body").height()+167+'px')});
    });
    $('a.menu-loader').on('click', function (e) {
        e.preventDefault();

        var showme = $(this).attr('href');
        $('.template-block').hide();
        $(showme).show();

        $('.menu-loader').removeClass('active');
        $(this).addClass('active');
        if(typeof tplEditor !== undefined)
            tplEditor.initialize(showme);
        //$("#tpl-sidebar").css({'max-height':($("#tpl-body").height()+167+'px')});
    });

    //Insert default content
    $('a.use-default').on('click', function (e) {
        e.preventDefault();

        var target = $(this).attr('data-target');
        var content = $(this).attr('data-default');

        tinyMCE.get(target).setContent(content);

        var target_up = $('#'+target).attr('data-target');
        $(target_up).html(tinyMCE.get(target).getContent());
    });

    $('.template-selector').click(function() {
        var curid = $(this).attr('id');

        $('.template-selector').removeClass('active');
        $(this).addClass('active');
        $('input:radio[name=template]').prop('checked', false);
        $('.btn-template-type').removeClass('active');
        $('.template-option-block').hide();
        $('#standard_template_type').val('');
        $('#instructions').hide();

        if (curid == 'template-standard') {
            $('input:radio[name=template][value="Standard Template"]').prop('checked', true);
            $('#template-buttons').show();
            $('#custom-text').show();
            $('#custom-assets-block').hide();
            $('#shown-customassets').empty();
            $('#files_customassets').val('');
        }else if (curid == 'template-standard-email' || curid == 'template-standard-landingpage') { //Special for Email
            $('input:radio[name=template][value="Standard Template"]').prop('checked', true);
            $('#template-buttons').show();
            $('#custom-text').show();
            $('#custom-assets-block').hide();
            $('#shown-customassets').empty();
            $('#files_customassets').val('');
            $('#standard_template_type').val('Custom');
        } else {
            $('input:radio[name=template][value="Custom Template"]').prop('checked', true);
            $('#template-buttons').hide();
            $('#custom-text').hide();
            $('#custom-assets-block').show();
            $('#standard_template_type').val('Custom');
        }
    });

    $('.btn-template-type').click(function(e) {
        e.preventDefault();
        var curid = $(this).attr('id');

        $('.btn-template-type').removeClass('active');
        $(this).addClass('active');
        $('.template-option-block').hide();
        $('#standard_template_type').val('');

        if (curid == 'configure-layout') {
            $('#configure-layout-block').show();
            $('#collapseOne').addClass('in');
            $('#standard_template_type').val('Custom');
            $('#instructions').show();
        } else {
            $('#simple-setup-block').show();
            $('#standard_template_type').val('Default');
            $('#instructions').show();
        }
    });

    $('.template-type-selector').click(function(e) {
        e.preventDefault();
        var curid = $(this).attr('id');

        $('.template-type-selector').removeClass('active');
        $(this).addClass('active');
        $('#standard_template_type').val('');

        if (curid == 'template-default') {
            $('#standard_template_type').val('Default');
            $('#content-update-form').submit();
        } else {
            $('#standard_template_type').val('Custom');
            $('#content-update-form').submit();
        }
    });

    /*Moving to bootbox in editor.js
    $('.template-type-links a').click(function(e) {
        e.preventDefault();
        var path = $(this).attr('href');

        if (confirm('Are you sure you want to switch the template configuration? All previously entered data will be lost.')) {
            window.location = path;
        }
    });
    */

    



    //Change visibility on Edit page
    $('select#template').on('change', function() {
        if (this.value == '') { //Hide thumbs
            $('#standard-template-type-chooser').hide();
            $('.template-row').hide();
            $('#template-warning').hide();
        } else if (this.value == 'Custom Template') { //Hide thumbs
            $('#standard-template-type-chooser').hide();
            $('.template-row').hide();
            $('#template-warning').show();
        } else {
            $('#standard-template-type-chooser').show();
            $('.template-row').show();
            $('#template-warning').hide();
            $('select#standard_template_type').val('Custom');
        }
    });
    $('select#standard_template_type').on('change', function() {
        if (this.value == '' || this.value == 'Default') { //Hide thumbs
            $('.template-row').hide();
            $('#template-warning').show();
        } else {
            $('.template-row').show();
            $('#template-warning').hide();
        }
    });

    //On page load, we're making anything unselected have a class
    $(".thumbnail-selector input:radio:checked").closest('li').addClass('active');
    $(".thumbnail-selector input:radio:not(:checked)").closest('li').addClass('unselected');

    $('.thumbnail-selector input:radio').click(function() {
        $(this).closest('.accordion-group').find('.accordion-heading').addClass('complete');
        $(this).closest('ul').find('li').removeClass('active');
        $(this).closest('ul').find('li').addClass('unselected');
        $(this).closest('li').addClass('active');
        $(this).closest('li').removeClass('unselected');
    });
    $('.thumbnail-selector input:checkbox').each(function() {
        if ($(this).is(':checked')) {
            $(this).closest('li').toggleClass('active');
        }
    });
    $('.thumbnail-selector input:checkbox').click(function() {
        $(this).closest('.accordion-group').find('.accordion-heading').addClass('complete');
        $(this).closest('li').toggleClass('active');

        //Show file upload fields
        //Location
        if ($('.chk-location').is(':checked')) {
            $('#location-image').show();
        } else {
            $('#location-image').hide();
            $('#files_pagelocationimage').val('');
            $('#shown-pagelocationimage').empty();
        }
        if ($('.chk-location-email').is(':checked')) {
            $('#location-image-email').show();
        } else {
            $('#location-image-email').hide();
            $('#files_emaillocationimage').val('');
            $('#shown-emaillocationimage').empty();
        }

        //Page Content Alt
        if ($('.chk-content').is(':checked')) {
            $('#content-alt-image').show();
        } else {
            $('#content-alt-image').hide();
            $('#files_pagecontentaltimage').val('');
            $('#shown-pagecontentaltimage').empty();
        }

        //Sponsors
        if ($('.chk-sponsor').is(':checked')) {
            $('#sponsor-image').show();
        } else {
            $('#sponsor-image').hide();
            $('#files_pagesponsorimage').val('');
            $('#shown-pagesponsorimage').empty();
        }

        //Sponsors
        if ($('.chk-speaker').is(':checked')) {
            $('#speaker-image').show();
        } else {
            $('#speaker-image').hide();
            $('#files_pagespeakerimage').val('');
            $('#shown-pagespeakerimage').empty();
        }
        if ($('.chk-speaker-email').is(':checked')) {
            $('#speaker-image-email').show();
        } else {
            $('#speaker-image-email').hide();
            $('#files_emailspeakerimage').val('');
            $('#shown-emailspeakerimage').empty();
        }

        //Events
        if ($('.chk-event').is(':checked')) {
            $('#event-image').show();
        } else {
            $('#event-image').hide();
            $('#files_pageeventimage').val('');
            $('#shown-pageeventimage').empty();
        }

        //Email Content Alt
        if ($('.chk-content-alt-email').is(':checked')) {
            $('#content-alt-image-email').show();
        } else {
            $('#content-alt-image-email').hide();
            $('#files_emailcontentaltimage').val('');
            $('#shown-emailcontentaltimage').empty();
        }
    });

    $('.accordion').on('show', function (e) {
        $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        $(e.target).prev('.accordion-heading').find('.accordion-toggle span.plusminus').html('-');

        var selected = $(this);
        var collapseh = $(".collapse .in").height();
        $('html, body').animate({ scrollTop: selected.offset().top-82 }, 500); //causing some jerkiness

    });
    $('.accordion').on('hide', function (e) {
        $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
        $(e.target).prev('.accordion-heading').find('.accordion-toggle span.plusminus').html('+');
    });


    $('.accordion-group a.continue').click(function (e) {
        e.preventDefault();
        $(this).closest('.accordion-group').find('.accordion-heading').addClass('complete');
    });


    //Email Banner
    $('input:radio[name=email-banner]').click(function () {
        var checkval = $('input:radio[name=email-banner]:checked').val();
        if (checkval == 'Email Banner A' || checkval == 'Email Banner B' || checkval == 'Email Banner C' || checkval == 'Email Banner D') {
            $('#choose-images-email-banner').show();
        } else {
            $('#choose-images-email-banner').hide();
        }
        $('#choose-images-email-banner input').removeAttr('checked');
        $('#choose-images-email-banner li').removeClass('active');
        $('#choose-images-email-banner li').removeClass('unselected');
    });

    //Email Body
    $('input:radio[name=email-body]').click(function () {
        var checkval = $('input:radio[name=email-body]:checked').val();
        if (checkval == 'Email Body A') {
            $('#choose-images-email-body').show();
        } else {
            $('#choose-images-email-body').hide();
        }
        $('#choose-images-email-body input').removeAttr('checked');
        $('#choose-images-email-body li').removeClass('active');
        $('#choose-images-email-body li').removeClass('unselected');
    });

    //Reg Page Banner
    $('input:radio[name=page-banner]').click(function () {
        var checkval = $('input:radio[name=page-banner]:checked').val();
        if (checkval == 'Registration Banner B') {
            $('#choose-images-page-banner').show();
        } else {
            $('#choose-images-page-banner').hide();
        }
        $('#choose-images-page-banner input').removeAttr('checked');
        $('#choose-images-page-banner li').removeClass('active');
        $('#choose-images-page-banner li').removeClass('unselected');
    });

});


//FILES
$(function () {
    'use strict';
    var url = 'server/php/';

    $('#fileupload_emailbannerimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-emailbannerimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-emailbannerimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_emailbannerimage" value="'+file.name+'" />');
                $(this).closest('ul').find('li').removeClass('active');
                $(this).closest('ul').find('li').removeClass('unselected');
                $("input[name=email-banner-image][value='custom']").prop('checked', true);
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-emailbannerimage').show();
            $('#progress-emailbannerimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-emailbannerimage');
            });
        }
    });

    $('#fileupload_emailbodyimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-emailbodyimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-emailbodyimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_emailbodyimage" value="'+file.name+'" />');
                $(this).closest('.choose-images').find('li').removeClass('active');
                $("input[name=email-body-image][value='custom']").prop('checked', true);
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-emailbodyimage').show();
            $('#progress-emailbodyimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-emailbodyimage');
            });
        }
    });

    $('#fileupload_pagebannerimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-pagebannerimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-pagebannerimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_pagebannerimage" value="'+file.name+'" />');
                $(this).closest('.choose-images').find('li').removeClass('active');
                $("input[name=page-banner-image][value='custom']").prop('checked', true);
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-pagebannerimage').show();
            $('#progress-pagebannerimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-pagebannerimage');
            });
        }
    });

    $('#fileupload_pagelocationimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-pagelocationimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-pagelocationimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_pagelocationimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-pagelocationimage').show();
            $('#progress-pagelocationimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-pagelocationimage');
            });
        }
    });

    $('#fileupload_pagecontentaltimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-pagecontentaltimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-pagecontentaltimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_pagecontentaltimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-pagecontentaltimage').show();
            $('#progress-pagecontentaltimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-pagecontentaltimage');
            });
        }
    });

    $('#fileupload_pagesponsorimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-pagesponsorimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-pagesponsorimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_pagesponsorimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-pagesponsorimage').show();
            $('#progress-pagesponsorimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-pagesponsorimage');
            });
        }
    });

    $('#fileupload_pagespeakerimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-pagespeakerimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-pagespeakerimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_pagespeakerimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-pagespeakerimage').show();
            $('#progress-pagespeakerimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-pagespeakerimage');
            });
        }
    });

    $('#fileupload_pageeventimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-pageeventimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-pageeventimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_pageeventimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-pageeventimage').show();
            $('#progress-pageeventimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-pageeventimage');
            });
        }
    });

    $('#fileupload_emailspeakerimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-emailspeakerimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-emailspeakerimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_emailspeakerimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-emailspeakerimage').show();
            $('#progress-emailspeakerimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-emailspeakerimage');
            });
        }
    });

    $('#fileupload_emaillocationimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-emaillocationimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-emaillocationimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_emaillocationimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-emaillocationimage').show();
            $('#progress-emaillocationimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-emaillocationimage');
            });
        }
    });

    $('#fileupload_emailcontentaltimage').fileupload({
        url: url,
        dataType: 'json',
        processalways: function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#shown-emailcontentaltimage').empty();
                var filebox = '<div name="' + (file.name) + '">';
                filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                filebox += '</div>';
                $(filebox).appendTo('#shown-emailcontentaltimage');
                $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_emailcontentaltimage" value="'+file.name+'" />');
                $('.delete-file').on('click',deleteCallback);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-emailcontentaltimage').show();
            $('#progress-emailcontentaltimage .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                $(error).appendTo('#shown-emailcontentaltimage');
            });
        }
    });

    function deleteCallback(e){
        var fileName = $(this).attr('data');
        e.preventDefault();
        $.ajax({
            url: '/server/php/?file=' + escape(fileName),
            data: {type:'REMOVE'},
            type: 'POST',
            filename: fileName,
            success: function(result) {
                var oHidden = $('input[value="'+this.filename + '"]');
                var filBox  =$('div[name="'+this.filename + '"]');
                var oBar = filBox.parent().siblings().find('.bar');

                oHidden && oHidden.remove();
                filBox && filBox.remove();
                oBar && oBar.parent().hide();
                $('.bar').closest().css('width','0%');
                return true;
            }
        });
    }
});

/* add social icons in footer section */
var x = 15;
function addSocialIcon(container) {    
    console.log("ADD SOCIAL ICON");
    var list = $("."+container);
    var openTag = '<td class="social" style="font-size: 16px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: 600; text-align: left; line-height:15px;padding: 0 5px 0 0;" valign="top" align="left">',
        closeTag = '</td>';
    if(container == "social-list"){
        openTag = "<li>";
        closeTag = "</li>";
    }
    list.prepend(openTag+'<a class="wys-inline-link" data-name="data-'+x+'" target="_blank" onclick="javascript:return false;"><img src="../../img/icons/social16x16.png" class="social-icon" style="font-size: 12px; outline: none; text-decoration: none; width: 16px; height: 16px; border: none;display:block;" height="80" width="80"></a>'+closeTag);
    tplEditor.reinitialize();
}

function removeThis(removeDiv) {   
    /* this function is used in the registration form functionality */
    // remove the input choice
    $(removeDiv).remove();
    return false;
}

function randomString(length, chars) {
    /* this function is used in the registration form functionality */
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

/* show edited page in default templates  */
function showEditedDefaultTemplate(templateId){
    $('.template-block').hide();
    $(templateId).show();
    $("#tpl-sidebar").css({'max-height':($("#tpl-body").height()+167+'px')});
    /* change preview selector option */
    $("select#preview-select option[value="+templateId+"]").attr('selected', 'selected');
}

