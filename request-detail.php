<?php
$loc = 'requests';
include('includes/head.php');
include('includes/header.php');
require_once('includes/_globals.php');
$c_type = true;
$step = 3; //This includes template.js
$editable = true; //Includes the JS

$owner = false;
$edit = false;
if (isset($_GET['edit'])) {
    $edit = true;
}
?>
    <script type="text/javascript">
    // remove div based on cross button clicked
    function removeThisUser(removeDiv) {   
        $(removeDiv).remove();
        return false;
    }
    </script>

    <style type="text/css">
    .notification-recipients {
        padding: 10px 0 0 4px;  
    }
    </style>

   <div class="container">
        <div class="row intro-body">
            <div class="span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
                            req.*,
                            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                            status.value AS status_value,
                            req.request_type AS request_type_id,
                            request_type.request_type AS request_type,
                            users.fname AS fname,
                            users.lname AS lname,
                            users.email AS email,
                            users.role AS role,
                            users.point_of_contact AS point_of_contact,
                            users.point_of_contact_email AS point_of_contact_email
                        FROM request AS req
                        INNER JOIN status on req.status = status.id
                        INNER JOIN request_type on req.request_type = request_type.id
                        INNER JOIN users on req.user_id = users.id";

                //This would be the All Requests view for every user. So losing this permission for now
                /*if ($_SESSION['admin'] == 1) {
                    $query.=" WHERE req.active = 1";
                } else {
                    $query.=" WHERE req.user_id = '".$_SESSION['user_id']."' AND req.active = 1";
                }*/

                $query.=" WHERE req.active = 1";

                $query.=" AND req.id = '".$_GET['id']."' LIMIT 1";

                $result = $mysqli->query($query);
                $row_cnt = $result->num_rows;
                $result_users = array();

                //print_r($query);

                if ($row_cnt > 0) {
                    //Show a message?
                    if (isset($_GET['update'])) {
                        if ($_GET['update'] == 'success') {
                            echo '<p style="color:#ff0000;"><strong>Request has been updated.</strong><br /></p>';
                        } else if ($_GET['update'] == 'fail') {
                            echo '<p style="color:#ff0000;"><strong>We\'re sorry, there has been a problem updating this request. Please <a href="support" style="color:#ff0000;text-decoration:underline;">contact support</a> or try again.</strong><br /></p>';
                        }
                    }
                    if (isset($_GET['action'])) {
                        if ($_GET['action'] == 'success') {
                            echo '<p style="color:#ff0000;"><strong>Your request has been submitted.</strong><br /></p>';
                        }
                    }

                    while ($obj = $result->fetch_object()) {
                        
                        $extra_owners = array(); 

                        if(!empty($obj->additional_owners)) // check if any additional owners exists
                            $extra_owners = unserialize($obj->additional_owners); // if yes then unserialize the array

                        //We need to know if this person owns this
                        if ($_SESSION['admin'] == 1 || $obj->user_id == $_SESSION['user_id'] || in_array($_SESSION['user_id'],$extra_owners)) { $owner = true; } else { $owner = false; }

                        echo '<h1>'.getTitle($_GET['id'], $obj->request_type_id).'</h1>';

                        //Admins/owners get a form
                        if (($_SESSION['admin'] == 1 || $edit == true) && ($owner == true)) {
                            echo '<form id="content-update-form-inline" action="includes/_requests_update.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" id="action" name="action" value="update">';
                                if ($edit) { echo '<input type="hidden" id="edit_form" name="edit_form" value="true">'; }
                                echo '<input type="hidden" id="user_id" name="user_id" value="'.$obj->user_id.'">
                                <input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">
                                <input type="hidden" id="request_id" name="request_id" value="'.$_GET['id'].'">
                                <input type="hidden" id="request_type_id" name="request_type_id" value="'.$obj->request_type_id.'">
                                <input type="hidden" id="user_email" name="user_email" value="'.getEmailAddress($obj->user_id).'">';
                        }

                        echo '<div id="zebra" class="single-request">';

                        //Grab user data for admin
                        if ($_SESSION['admin'] == 1) {
                            echo '<div class="row"><label>Requestor</label> <div class="pull-left">'.$obj->fname.' '.$obj->lname.' / <a href="mailto:'.$obj->email.'">'.$obj->email.'</a></div></div>';
                            if ($obj->point_of_contact != '') { echo '<div class="row"><label>Point of Contact</label> <div class="pull-left">'.$obj->point_of_contact; if ($obj->point_of_contact_email != '') { echo ' / <a href="mailto:'.$obj->point_of_contact_email.'">'.$obj->point_of_contact_email.'</a>'; } echo '</div></div>'; }
                        }
                        echo '<div class="row"><label>Date Created</label> <div class="pull-left">'.$obj->date_created.'</div></div>';
                        echo '<div class="row"><label>Request Type</label> <div class="pull-left">'.$obj->request_type.'</div></div>';

                        //Admin can change requestor
                        if ($_SESSION['admin'] == 1) {
                        echo '<div class="row"><label>Change Requestor</label> <div class="pull-left">';
                            $query_users = "SELECT id, fname, lname FROM users WHERE active = 1 ORDER BY fname ASC";
                            $result_users = $mysqli->query($query_users);
                            echo '<select id="new_user_id" name="new_user_id">';
                            while ($obj_users = $result_users->fetch_object()) {
                                echo '<option value="'.$obj_users->id.'"';
                                if ($obj->user_id == $obj_users->id) {
                                    echo ' selected="selected"';
                                }
                                echo '>'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                            }
                            echo '</select>';
                        echo '</div></div>';
                        } else {
                            echo '<input type="hidden" id="new_user_id" name="new_user_id" value="'.$obj->user_id.'">';
                        }

                        echo '<div class="row"><label>Status</label> <div class="pull-left">';
                        //Admin can change status
                        if ($_SESSION['admin'] == 1) {
                            $query_status = "SELECT * FROM status WHERE id != 14 ORDER BY sort_order ASC";
                            $result_status = $mysqli->query($query_status);
                            echo '<select id="status_update" name="status_update">';
                            while ($obj_s = $result_status->fetch_object()) {
                                $belongs_to = explode(',', $obj_s->belongs_to);
                                if (in_array($obj->request_type_id, $belongs_to)) {
                                    echo '<option value="'.$obj_s->id.'"';
                                    if ($obj->status == $obj_s->id) {
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$obj_s->value.'</option>';
                                }
                            }
                            echo '</select>';
                        } else {
                            echo '<input type="hidden" id="status_update" name="status_update" value="'.$obj->status.'">'; //For new Edit
                            echo $obj->status_value;
                        }
                        echo '</div></div>';
                        echo '<div class="row"><label>Assigned To</label> <div class="pull-left">';
                        //Admin can change status
                        if ($_SESSION['admin'] == 1) {
                            echo getAssignedTo($obj->assigned_to, 1);
                        } else {
                            echo '<input type="hidden" id="assigned_to" name="assigned_to" value="'.$obj->assigned_to.'">'; //For new Edit
                            echo getAssignedTo($obj->assigned_to, 0);
                        }
                        echo '</div></div>';
                        if ($_SESSION['admin'] == 1) {
                            //echo '<p><br /><a href="" id="submit-request-form-inline" class="btn-green submit pull-right" data-analytics-label="Submit Form: Request Update">Submit Updates</a></p><p>&nbsp;</p>';
                        }


                        // Request details box
                        echo '<h1><br />'.$obj->request_type.' Details';
                        /*if ($owner == true) {
                            if ($edit == false) {
                                echo '<a href="request-detail?id='.$_GET['id'].'&edit=1" class="btn-green download" style="margin-left:14px;">Edit</a>';
                            } else {
                                echo '<a href="request-detail?id='.$_GET['id'].'" class="btn-green download finish-edit" style="margin-left:14px;">Finish Edits</a>';
                            }
                        }*/
                        echo '</h1>';

                        //Now show the details of the particular type of request
                        echo getRequestDetails($obj->id, $obj->request_type_id, $edit, $owner);
                        
                        if(in_array($obj->request_type_id,array(1,2,5,6,7))) {
                            // additional recipients and additional owners of request
                            if(!$edit) {  // if view mode
                                $additional_username = $additional_username_owner = array();

                                echo '<div class="row row-border"><label>Additional Notified Recipients</label>';
                                if(!empty($obj->additional_recipients)) {  // if additional recipients are not empty
                                    $add_recipients = unserialize($obj->additional_recipients);

                                    if(!empty($add_recipients) && is_array($add_recipients)) { 

                                        foreach ($add_recipients as $user_id) {
                                            $additional_username[] = getUserName($user_id); // get name against id
                                        }

                                        if(!empty($additional_username)) { // if not empty username
                                            echo '<div class="pull-left">';
                                            echo implode(', ',$additional_username);  
                                            echo '</div>';
                                        }
                                          
                                    }
                                  
                                }
                                echo '</div>';  

                                echo '<div class="row row-border"><label>Additional Users That Can View/Edit</label>';                                    
                                if(!empty($obj->additional_owners)) { // if additional owners are not empty
                                    $add_owners = unserialize($obj->additional_owners);

                                    if(!empty($add_owners) && is_array($add_owners)) {

                                        foreach ($add_owners as $user_id) {
                                            $additional_username_owner[] = getUserName($user_id);  // get name against id
                                        }

                                        if(!empty($additional_username_owner)) { // if not empty username
                                            echo '<div class="pull-left">';
                                            echo implode(', ',$additional_username_owner);   
                                            echo '</div>';
                                        }
                                    }
                                }
                                echo '</div>';  
                            } else {
                                // in edit mode
                                echo '<div class="row row-border"><label for="additional-notifiers" class="control-label">Additional Notified Recipients</label>
                                <div class="pull-left">
                                <select id="additional-notifiers" name="additional-notifiers[]" data-placeholder="Choose a user..." multiple style="width:50%;border:none !important;" class="required">';
                                 
                                $users_string = "";
                                $userId = unserialize($obj->additional_recipients);
                                
                                // fetching values for select dropdown
                                $query_users = "SELECT id, fname, lname, email FROM users WHERE active = 1 ORDER BY fname ASC";
                                $result_users = $mysqli->query($query_users);
                                $cc_list = array();
                                $cc_str = '';
                                while ($obj_users = $result_users->fetch_object()) {
                                    if($obj_users->id !== $_SESSION['user_id']){
                                        $users_string .= '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                                    }
                                }
                                echo $users_string; 
                                echo '</select>
                                <div class="notification-recipients"><ul>';
                                // display the already saved additional recipients
                                foreach ($userId as $user) {
                                    if($user != ""){
                                        $query_users = "SELECT id, fname, lname, email FROM users WHERE id =".$user;
                                        $result_users = $mysqli->query($query_users);
                                        while ($obj_users = $result_users->fetch_object()) {
                                            echo '<li class="user-rec-'.$obj_users->id.'"><div class="remove removeUserRule" data-remove-id="first_name" alt="Remove" onclick="removeThisUser(\'.user-rec-'.$obj_users->id.'\');"></div>'.$obj_users->fname.' '.$obj_users->lname.' - '.$obj_users->email.'<input type="hidden" id="add-recipients" name="add-recipients[]" value="'.$obj_users->id.'"></li>';
                                        }
                                    }
                                }
                                echo '</ul></div>
                                <div class="chosen-notifiers notification-recipients"><ul></ul></div>
                                </div></div>';

                                echo '<div class="row row-border"><label for="additional-rights-users" class="control-label">Additional Users That Can View/Edit</label>
                                <div class="pull-left">
                                <select id="additional-rights-users" name="additional-rights-users[]" data-placeholder="Choose a user..." multiple style="width:50%;border:none !important;" class="required">';
                                 ////recipients list
                                $userId = unserialize($obj->additional_owners);
                                echo $users_string;                            
                                echo '</select>
                                <div class="notification-recipients"><ul>';
                                // display the already saved additional recipients
                                foreach ($userId as $user) {
                                    if($user != ""){
                                        $query_users = "SELECT id, fname, lname, email FROM users WHERE id =".$user;
                                        $result_users = $mysqli->query($query_users);
                                        while ($obj_users = $result_users->fetch_object()) {
                                            echo '<li class="user-'.$obj_users->id.'"><div class="remove removeUserRule" data-remove-id="first_name" alt="Remove" onclick="removeThisUser(\'.user-'.$obj_users->id.'\');"></div>'.$obj_users->fname.' '.$obj_users->lname.' - '.$obj_users->email.'<input type="hidden" id="add-owners" name="add-owners[]" value="'.$obj_users->id.'"></li>';
                                        }
                                    }
                                }
                                echo '</ul></div>
                                <div class="chosen-rights-users notification-recipients"><ul></ul></div>
                                </div></div>';
                            }
                        }    

                        //Contact lists
                        if ($obj->request_type_id != 4 && $obj->request_type_id != 9) {
                            $query_contactlists = "SELECT
                                                    rcl.id,
                                                    rcl.request_id,
                                                    rcl.name
                                                FROM request_contactlist rcl
                                                WHERE rcl.associated_tactic = '".$obj->id."'
                                                AND rcl.active = 1
                                                ORDER BY rcl.id DESC";
                            $result_contactlists = $mysqli->query($query_contactlists);
                            //print_r($query_contactlists);
                            $row_cnt_contactlists = $result_contactlists->num_rows;
                            if ($row_cnt_contactlists > 0) {
                                echo '<div class="row"><label>Contact List</label> <div class="pull-left" style="width:680px;"><ul class="notes">';
                                while ($obj_cl = $result_contactlists->fetch_object()) {
                                    echo '<li><a href="request-detail?id='.$obj_cl->request_id.'" target="_blank">'.$obj_cl->name.' <i class="icon icon-share" style="margin-top:3px;"></i></a></li>';
                                }
                                echo '</ul></div></div>';
                            }
                        }


                        //Notes
                        //Grab notes
                        $notes = '<ul class="notes">';
                        $query_notes = "SELECT user_id, notes, col, DATE_FORMAT(date_created, '%m/%d/%Y %l:%i%p') AS date_created_notes FROM notes WHERE request_id = '".$obj->id."' AND active = 1 ORDER BY date_created DESC";
                        $result_notes = $mysqli->query($query_notes);
                        $row_cnt_notes = $result_notes->num_rows;
                        if ($row_cnt_notes > 0) {
                            while ($obj_n = $result_notes->fetch_object()) {
                                $col_clean = str_replace('_', ' ', $obj_n->col);
                                if ($obj_n->notes == 'Value updated') {
                                    $notes.='<li>'.ucwords($col_clean).' Updated | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                } else {
                                    $notes.='<li>'.nl2br($obj_n->notes).' | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                }
                            }
                        } else {
                            $notes.='<li>No notes</li>';
                        }
                        $notes.='</ul>';
                        echo '<div class="row"><label>Notes</label> <div class="pull-left" style="width:680px;">'.$notes;
                        //Admin can add notes
                        if ($_SESSION['admin'] == 1 && $edit == true) {
                            echo '<a href="#" id="notes" class="editable" data-type="textarea" data-pk="'.$_GET['id'].'" data-url="/includes/_requests_update_inline.php?request_type_id='.$obj->request_type_id.'&type=notes">Add a note</a>';
                            //echo '<br /><textarea id="notes_update" name="notes_update" rows="3" cols="45" class="input-full" style="width:50%;"></textarea>';
                        }
                        echo '</div></div>';

                        if ($obj->request_type_id == 1) {
                            // show On24 Report Url field in webinar
                            $on24_report_url_val = getSingleField('on24_report_url', $obj->request_type_id, $_GET['id']);
                            echo "<div class=row><label>On24 Report Url</label> <div class=pull-left>".$on24_report_url_val."</div></div>";
                        }

                        //Grab speakers
                        if ($obj->request_type_id == 1) {
                            $speakers = '';
                            $query_speakers = "SELECT * FROM speakers WHERE request_id = '".$_GET['id']."' AND active = 1 ORDER BY id ASC";
                            $result_speakers = $mysqli->query($query_speakers);
                            $row_cnt_speakers = $result_speakers->num_rows;
                            $speaker_cnt = 1;
                            if ($row_cnt_speakers > 0) {
                                echo '<h1><br />Speakers</h1>';

                                while ($obj_speakers = $result_speakers->fetch_object()) {
                                    if ($edit == 1) {
                                        $speakers.='<input type="hidden" id="speaker_id_'.$speaker_cnt.'" name="speaker_id_'.$speaker_cnt.'" value="'.$obj_speakers->id.'">
                                                        <input type="text" id="speaker_name_'.$speaker_cnt.'" name="speaker_name_'.$speaker_cnt.'" class="input-wide required" placeholder="Name (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_name.'"><br />
                                                        <input type="text" id="speaker_email_'.$speaker_cnt.'" name="speaker_email_'.$speaker_cnt.'" class="input-wide" placeholder="Email" value="'.$obj_speakers->speaker_email.'"><br />
                                                        <label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$speaker_cnt.'[]" name="speaker_type_'.$speaker_cnt.'[]" value="Speaker"'; if (strpos($obj_speakers->speaker_type, 'Speaker') !== false) { $speakers.='checked'; } $speakers.='> Speaker</label>
                                                        <label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$speaker_cnt.'[]" name="speaker_type_'.$speaker_cnt.'[]" value="Moderator"'; if (strpos($obj_speakers->speaker_type, 'Moderator') !== false) { $speakers.='checked'; } $speakers.='> Moderator</label>
                                                        <div class="bios-extra" style="display:block;">
                                                            <input type="text" id="speaker_title_'.$speaker_cnt.'" name="speaker_title_'.$speaker_cnt.'" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_title.'"><br />
                                                            <input type="text" id="speaker_company_'.$speaker_cnt.'" name="speaker_company_'.$speaker_cnt.'" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_company.'"><br />
                                                            <textarea id="speaker_bio_'.$speaker_cnt.'" name="speaker_bio_'.$speaker_cnt.'" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048">'.$obj_speakers->speaker_bio.'</textarea><br />
                                                            <div class="upload-div">
                                                                <div id="photo_'.$speaker_cnt.'" class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <span class="btn-file btn-green add"><span class="fileupload-new">Add/Replace Photo</span><input id="fileupload_photo_'.$speaker_cnt.'" class="fileupload_photo" data-number="1" type="file" name="files[]"></span>
                                                                </div>
                                                                <div id="progress-photo_'.$speaker_cnt.'" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 160px;width:290px;display:none;">
                                                                    <div class="bar"></div>
                                                                </div>
                                                                <div id="shown-photo_'.$speaker_cnt.'" class="files" style="clear:both;"> </div>
                                                            </div>
                                                        </div><label class="checkbox inline" style="color:#cc0000;"> <input type="checkbox" id="speaker_delete_'.$speaker_cnt.'" name="speaker_delete_'.$speaker_cnt.'" value="1"> Remove Speaker?</label><br /><hr>';
                                        $speaker_cnt++;
                                    } else {
                                        $speakers.='<p class="speaker-list"><strong>'.$obj_speakers->speaker_name.'</strong>';
                                        if ($obj_speakers->speaker_type != '') {
                                            $speakers.=' (<em>'.$obj_speakers->speaker_type.'</em>)';
                                        }
                                        if ($obj_speakers->speaker_title != '') {
                                            $speakers.='<br />'.$obj_speakers->speaker_title;
                                        }
                                        if ($obj_speakers->speaker_company != '') {
                                            $speakers.='<br />'.$obj_speakers->speaker_company;
                                        }
                                        if ($obj_speakers->speaker_email != '') {
                                            $speakers.='<br /><a href="mailto:'.$obj_speakers->speaker_email.'">'.$obj_speakers->speaker_email.'</a>';
                                        }
                                        if ($obj_speakers->speaker_bio != '') {
                                            $speakers.='<br /><br />'.$obj_speakers->speaker_bio;
                                        }
                                        if (strlen($obj_speakers->speaker_photo) > 22) {
                                            $speakers.='<br /><br /><img src="'.$obj_speakers->speaker_photo.'">';
                                        }
                                        $speakers.='</p><hr>';
                                    }
                                }

                                echo '<div class="row row-border"><label>&nbsp;</label> <div class="pull-left" style="width:680px;">'.$speakers.'</div></div>';
                            }

                            //Add blank fields for new speaker
                            if ($edit == 1 && $row_cnt_speakers < 10) {
                                echo '<h1><br />Add Speaker</h1>';

                                 echo '<div class="row row-border"><label>&nbsp;</label> <div class="pull-left" style="width:680px;"><label style="width:auto;">Add another Speaker/Moderator</label><br /><input type="hidden" id="speaker_id_10" name="speaker_id_10" value="new">
                                <input type="text" id="speaker_name_10" name="speaker_name_10" class="input-wide" placeholder="Name (120 characters max)" maxlength="120" value=""><br />
                                <input type="text" id="speaker_email_10" name="speaker_email_10" class="input-wide" placeholder="Email" value=""><br />
                                <label class="checkbox inline"> <input type="checkbox" id="speaker_type_10[]" name="speaker_type_10[]" value="Speaker"> Speaker</label>
                                <label class="checkbox inline"> <input type="checkbox" id="speaker_type_10[]" name="speaker_type_10[]" value="Moderator"> Moderator</label>
                                <div class="bios-extra" style="display:block;">
                                    <input type="text" id="speaker_title_10" name="speaker_title_10" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value=""><br />
                                    <input type="text" id="speaker_company_10" name="speaker_company_10" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value=""><br />
                                    <textarea id="speaker_bio_10" name="speaker_bio_10" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048"></textarea><br />
                                    <div class="upload-div">
                                        <div id="photo_10" class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add"><span class="fileupload-new">Add Photo</span><input id="fileupload_photo_10" class="fileupload_photo" data-number="1" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-photo_10" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 160px;width:290px;display:none;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-photo_10" class="files" style="clear:both;"> </div>
                                    </div>
                                </div></div></div>';

                                //echo '<p><br /><a href="" id="submit-request-form-inline-speakers" class="btn-green submit pull-right">Submit Updates</a></p><p>&nbsp;</p>';
                            }
                        }

                        /////////////////////////////////////////////////////////////////////////////
                        //TEMPLATES
                        //Show template blocks, only for Webinar, eNurture(2), Newsletter(5), Email (6) and Landing Page (7)
                        if ($obj->request_type_id == 1 || $obj->request_type_id == 6 || $obj->request_type_id == 7 
                            || $obj->request_type_id == 2 || $obj->request_type_id == 5) {

                            $query_template  = "SELECT * FROM templates WHERE request_id = '".$obj->id."' ORDER BY date_created DESC LIMIT 1";
                            $result_template = $mysqli->query($query_template);
                            $result_template = $result_template->fetch_object();

                            if ($result_template) {

                                echo '<h1><br />Template and Layout Configuration</h1>';
                                echo '<div class="row row-border"><label>Template Type</label><div class="pull-left">';

                                if ($obj->request_type_id == 1) { //Webinar
                                    if (strtolower($result_template->template_type) === 'custom'){
                                        echo '<a href="templates-custom?type=webinar&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                    } else {
                                        echo '<a href="templates-default?type=webinar&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                    }
                                } else if ($obj->request_type_id == 6) { //Single email
                                    echo '<a href="templates-custom?type=email&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 7) { //Landing Page
                                    echo '<a href="templates-custom?type=landingpage&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 2) { //eNurture
                                    echo '<a href="templates-custom?type=enurture&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 5) { //Newsletter
                                    echo '<a href="templates-custom?type=newsletter&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                }



                                echo '</div></div>';
                            } else {
                                echo '<h1><br />Template and Layout Configuration</h1>';
                                echo '<div class="row row-border"><label>Template Type</label><div class="pull-left">';

                                if ($obj->request_type_id == 1) { //Webinar
                                    echo '<a href="templates-default?type=webinar&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 6) { //Single email
                                    echo '<a href="templates-custom?type=email&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 7) { //Landing Page
                                    echo '<a href="templates-custom?type=landingpage&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 2) { //eNurture
                                    echo '<a href="templates-custom?type=enurture&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                } else if ($obj->request_type_id == 5) { //Newsletter
                                    echo '<a href="templates-custom?type=newsletter&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                }


                                echo '</div></div>';
                            }

                        } //End tactic check for templates

                        /////////////////////////////////////////////////////////////////////////////
                        //TEMPLATES


                        //Now show the assets from the request
                        echo '<h1><br />Request Assets</h1>';
                        echo getRequestAssets($obj->id, $obj->request_type_id, $edit, $owner);

                        //Can upload extra files if in Edit mode
                        if ($edit) {
                            echo '<h1><br />Upload Additional Assets</h1>';
                            if ($obj->request_type_id != 4 && $obj->request_type_id != 9) {
                            echo '<div class="row row-border"><label>Tactic Assets</label>
                                <div class="pull-left">
                                    <div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Upload Asset Zip</span><input id="fileupload_customassets" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-customassets" class="files" style="clear:both;"></div>
                                    </div>
                                </div>
                            </div>';
                            }
                            //Only show Contact List upload for a contact list type, otherwise show a message
                            if ($obj->request_type_id == 4) { //Contact List
                                echo '<div class="row row-border"><label>Request Assets</label>
                                    <div class="pull-left">
                                        <div class="upload-div">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn-file btn-green add" style="width:165px;"><span class="fileupload-new">Upload Request Assets</span><input id="fileupload_contacts" type="file" name="files[]"></span>
                                            </div>
                                            <div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                                <div class="bar"></div>
                                            </div>
                                            <div id="shown-contacts" class="files" style="clear:both;"></div>
                                        </div>
                                    </div>
                                </div>';
                            } else if ($obj->request_type_id == 9) { //Lead List
                                echo '<div class="row row-border"><label>Request Assets</label>
                                    <div class="pull-left">
                                        <div class="upload-div">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn-file btn-green add" style="width:165px;"><span class="fileupload-new">Upload Request Assets</span><input id="fileupload_contacts" type="file" name="files[]"></span>
                                            </div>
                                            <div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                                <div class="bar"></div>
                                            </div>
                                            <div id="shown-contacts" class="files" style="clear:both;"></div>
                                        </div>
                                    </div>
                                </div>';
                            } else {
                                echo '<div class="row row-border"><label>Contact List</label>
                                    <div class="pull-left">If you would like to associate a contact list with this tactic, please <a href="/request" target="_blank">Upload a new Contact List <i class="icon icon-share" style="margin-top:3px;"></i></a> and choose this tactic to associate it with.</div>
                                </div>';
                            }
                            //Need IF belongs to this type
                            /*
                            echo '<div class="row"><label>Proof of Contact List Opt-in status</label>
                                <div class="pull-left">
                                    <div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Upload Proof of Opt-in</span><input id="fileupload_proof" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-proof" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-proof" class="files" style="clear:both;"></div>
                                    </div>
                                </div>
                            </div>';
                            */

                            //Webinar
                            if ($obj->request_type_id == 1) {
                            echo '<div class="row"><label>Campaign Brief</label> <div class="pull-left"><div class="upload-div">
                                    <div id="campaignbrief" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add Campaign Brief</span><input id="fileupload_campaignbrief" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-campaignbrief" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="errors-campaignbrief" class="files" style="clear:both;color:#cc0000;"></div>
                                    <div id="shown-campaignbrief" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                            echo '<div class="row"><label>Presentation</label> <div class="pull-left"><div class="upload-div">
                                    <div id="presentation" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add Presentation</span><input id="fileupload_presentation" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-presentation" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="errors-presentation" class="files" style="clear:both;color:#cc0000;"></div>
                                    <div id="shown-presentation" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                            echo '<div class="row"><label>Media Upload</label> <div class="pull-left"><div class="upload-div">
                                    <div id="media" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add Media</span><input id="fileupload_media" type="file" name="files[]" multiple></span>
                                    </div>
                                    <div id="progress-media" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="errors-media" class="files" style="clear:both;color:#cc0000;"></div>
                                    <div id="shown-media" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                            echo '<div class="row"><label>Survey</label> <div class="pull-left"><div class="upload-div">
                                    <div id="survey" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add Survey File</span><input id="fileupload_survey" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-survey" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-survey" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                            echo '<div class="row"><label>Resource List</label> <div class="pull-left"><div class="upload-div">
                                    <div id="resource" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add File(s)</span><input id="fileupload_resource" type="file" name="files[]" multiple></span>
                                    </div>
                                    <div id="progress-resource" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-resource" class="files" style="clear:both;"></div>
                                </div> </div></div>';

                            //TAKING OUT FOR NEW TEMPLATES
                            /*
                            echo '<div class="row"><label>Email Featured Image</label> <div class="pull-left">';
                                if (doesAssetExist($obj->id, 'Email Featured Image') == false) {
                                    echo '<div class="upload-div">
                                    <div id="emailfeaturedimage" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add File</span><input id="fileupload_emailfeaturedimage" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-emailfeaturedimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-emailfeaturedimage" class="files" style="clear:both;"></div></div>';
                                } else {
                                    echo 'Please delete current Email Featured Image above if you\'d like to choose a different one.';
                                }
                                echo '</div></div>';
                            echo '<div class="row"><label>Registration Page Image</label> <div class="pull-left">';
                            if (doesAssetExist($obj->id, 'Registration Page Image') == false) {
                                    echo '<div class="upload-div">
                                    <div id="registrationpageimage" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add File</span><input id="fileupload_registrationpageimage" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-registrationpageimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-registrationpageimage" class="files" style="clear:both;"></div></div>';
                                } else {
                                    echo 'Please delete current Registration Page Image above if you\'d like to choose a different one.';
                                }
                                echo '</div></div>';
                            echo '<div class="row"><label>Confirmation Page Image</label> <div class="pull-left">';
                            if (doesAssetExist($obj->id, 'Confirmation Page Image') == false) {
                                    echo '<div class="upload-div">
                                    <div id="confirmationpageimage" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add File</span><input id="fileupload_confirmationpageimage" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-confirmationpageimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-confirmationpageimage" class="files" style="clear:both;"></div></div>';
                                } else {
                                    echo 'Please delete current Confirmation Page Image above if you\'d like to choose a different one.';
                                }
                                echo '</div></div>';
                            echo '<div class="row"><label>Footer Logo</label> <div class="pull-left">';
                            if (doesAssetExist($obj->id, 'Footer Logo') == false) {
                                    echo '<div class="upload-div">
                                    <div id="footerimage" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:150px;"><span class="fileupload-new">Add File</span><input id="fileupload_footerimage" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-footerimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;width:250px;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-footerimage" class="files" style="clear:both;"></div></div>';
                                } else {
                                    echo 'Please delete current Footer Logo above if you\'d like to choose a different one.';
                                }
                                echo '</div></div>';
                                */
                            }

                            //echo '<p><br /><a href="" id="submit-request-form-inline-files" class="btn-green submit pull-right">Submit Updates</a></p><p>&nbsp;</p>';
                        }
   

                        //And then show the campaign details
                        echo '<h1><br />Campaign Details</h1>';
                        echo getCampaignDetails($obj->campaign_id, 8, $edit, $owner);


                        //Message Box

                        //if($obj->user_id !== $_SESSION['user_id']){ //Requestor should be able to see the Messages area, so taking this out. ejh
                            echo '<a name="messages"></a><h1><br />Messages</h1>';

                            echo '<div class="row row-border even"><label></label> <div class="pull-left" style="width:100%;">';

                            //Fetch messages
                            // $query_messages = "SELECT messages.*,users.fname,users.lname FROM messages INNER JOIN on messages.user_id = users.id WHERE request_id = " .  $obj->id . " ORDER BY date_created";
                            $query_messages = "SELECT messages.body, messages.date_created, users.fname,users.lname,users.id FROM messages INNER JOIN users ON messages.user_id = users.id where messages.request_id = " . $obj->id . " AND messages.active = 1 ORDER BY messages.date_created DESC";
                            echo '<div class="message-thread" style="margin-left:210px;">';
                            $result_messages = $mysqli->query($query_messages);
                            while ($obj_msg = $result_messages->fetch_object()) {
                                $attrvalue = $obj_msg->date_created;
                                date_default_timezone_set('America/Los_Angeles');
                                $attrvalue =  date('m/d/Y h:iA',strtotime("$attrvalue UTC"));
                                echo '<ul class="notes"><li>' . $obj_msg->body . '<br /><span class="user-name">'.$attrvalue.' ('.$obj_msg->fname.' '.$obj_msg->lname.')</span></li></ul><hr>';
                            }


                            echo '</div>';

                            //Message Body
                            echo '<div class="send-message">';
                            echo '<div class="control-group">';
                            echo '<div class="controls">';
                            echo '<label for="body">Send Message</label>';
                            echo '<textarea id="message-body" name="body" rows="6"  style="width:74%;" class="input-wide textarea-wide" placeholder="Send message about request"></textarea>';
                            //echo '<i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="type message for requestor" data-original-title="" title=""></i>'; //don't think we need helper text yet. ejh.
                            echo '</div></div>';

                            ////recipients
                            $query_users = "SELECT id, fname, lname FROM users WHERE active = 1 ORDER BY fname ASC";
                            $result_users = $mysqli->query($query_users);




                            echo '<div class="control-group">';
                            echo '<label for="message-recipients">Recipients</label>';
                            echo '<select id="message-recipients" name="message-recipients" multiple style="width:50%;border:none !important;">';

                            $cc_list = array();
                            $cc_str = '';
                            while ($obj_users = $result_users->fetch_object()) {
                                if($obj_users->id !==  $obj->assigned_to && $obj_users->id !== $obj->user_id){
                                    echo '<option value="'.$obj_users->id.'"';
                                    // if ($obj->user_id == $obj_users->id) {
                                    //     echo ' selected="selected"';
                                    // }
                                    echo '>'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                                }else{
                                    array_push($cc_list, $obj_users->id);
                                    $cc_str .=  $obj_users->fname . ' ' . $obj_users->lname . ', ';
                                }
                            }

                            $cc_str = substr($cc_str, 0, strlen($cc_str) -2);
                            echo '</select>';
                            echo '</div>';


                            echo '<div class="control-group">';
                            echo '<label for="message-cc">CC</label>';
                            echo '<p id="message-cc" data-list=\''. json_encode($cc_list) . '\'>' . $cc_str . '</p>';
                            // echo '<input type="text" id="message-cc" class="form-control" value="' . $cc_str . '" disabled="true" data-list=\''. json_encode($cc_list) . '\'>';
                            echo '</div><br/>';

                            echo '<a href="#" id="request-detail-send-message" class="btn-green" style="margin-left: 210px;">Send Message</a>';
                            echo '</div>'; //End messages

                            echo '</div></div>';

                        //} // END Message Box


                        echo '</div>'; //End single-request


                    }
                }

                if (isset($_GET['action']) && $_GET['action'] == 'success') {
                    echo '<p><br /><a href="requests" class="btn-green back pull-left">My Requests</a><p>&nbsp;</p><p>&nbsp;</p>';
                } else {
                    echo '<p><br /><a href="requests" class="btn-green back pull-left">Back to list</a><p>&nbsp;</p><p>&nbsp;</p>';
                }

                /*if ($owner == true) {
                    if ($edit == false) {
                        echo '<a href="request-detail?id='.$_GET['id'].'&edit=1" class="btn-green download pull-right">Edit</a>';
                    } else {
                        echo '<a href="request-detail?id='.$_GET['id'].'" class="btn-green download pull-right">Finish edits</a>';
                    }
                }*/

                if (($_SESSION['admin'] == 1 || $edit == 1) && ($owner == true)) {
                    //echo '<a href="" id="submit-request-form-inline" class="btn-green submit pull-right" data-analytics-label="Submit Form: Request Update">Submit updates</a></p></form></div>';
                    echo '</p></form></div>';
                } else {
                    echo '</p></div>';
                }


                //Message params form
                echo '<form id="request-detail-message">';
                ?>

            </div>
        </div>

        <?php
        if ($row_cnt > 0) {
            if (($_SESSION['admin'] == 1 || $owner == true)) {
                echo '
                    <div class="header-row">
                    <div class="spamm12" style="margin: 0 auto;">
                      <header>
                        <div class="header-right">';
                            if ($edit == false) {
                                echo '<a href="request-detail?id='.$_GET['id'].'&edit=1" class="btn-green plain" style="margin-right:6px;">Edit</a>';
                            } else {
                                //echo '<a href="request-detail?id='.$_GET['id'].'" class="btn-green plain finish-edit" style="margin-right:6px;">Finish Edits</a>';
                            }
                          if ($_SESSION['admin'] == 1 || $edit == true) { echo '<a href="#" id="submit-request-form-inline" class="btn-green submit">Save Changes</a>'; }
                        echo '</div>
                      </header>
                    </div>
                  </div>'; //Something to play around with
              }
          }
          ?>

    </div>

<?php include('includes/footer.php'); ?>
