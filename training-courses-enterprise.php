<?php
$loc = 'solutions';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
                <h1>Modern Marketer 101 - Eloqua 10 Overview Training</h1>
            </div>
        </div>
    </div>

    <div class="container">
    	<div class="row">
            <div class="intro span12">
                <iframe src="training-courses-files/Enterprise-EPO-FUIN/player.html" width="992" height="652" frameborder="0"></iframe>
            </div>
        </div>
        <p style="margin:30px 0 40px 0;padding-bottom:40px;"><a href="solutions" class="btn-green back pull-left">Back to Solutions</a></p>
    </div>

<?php include('includes/footer.php'); ?>