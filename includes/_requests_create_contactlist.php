<?php
require_once('validate.php');
require_once('_globals.php');

/////////////////////////////////////////////////////////////////////////////////////
//Create a new request
$query_request_con = "INSERT INTO request VALUES (
                NULL,
                '".$user_id."',
                '',
                '',
                '4',
                '".$campaign_id."',
                NULL,
                NULL,
                NULL,
                '0',
                '1',
                '1',
                NULL,
                '".$user_id."',
                NOW(),
                NOW(),
                NOW(),
                '1')";

$mysqli->query($query_request_con);

if ($mysqli->insert_id) {
    //New request ID
    $request_id_con = $mysqli->insert_id;
    $name_con = $contact_list_request_type.' List.'.$list_tag;
    $name_con = $mysqli->real_escape_string($name_con);
    $eloqua_source_id_con = createEloquaID($campaign_id, $name_con, $request_id_con);
    $eloqua_source_id_con = $mysqli->real_escape_string($eloqua_source_id_con);

    /////////////////////////////////////////////////////////////////////////////////////
    //Create a new individual request
    $query = "INSERT INTO request_contactlist VALUES (
                    NULL,
                    '".$user_id."',
                    '".$request_id_con."',
                    '".$marketing_id."',
                    '".$eloqua_source_id_con."',
                    '".$request_id."',
                    '".$name_con.".".$request_id_con."',
                    '".$list_tag."',
                    '".$contact_list_request_type."',
                    '1',
                    '1')";

    $mysqli->query($query);


    /////////////////////////////////////////////////////////////////////////////////////
    //Input the assets

    //Contacts
    uploadAssets($files_contacts, 'Contacts', $request_id_con, $user_id);


    /////////////////////////////////////////////////////////////////////////////////////
    //Append the name of the new list to Existing Contact List value of request
    if ($tactic_request_id) {
        $query_table = getTacticTable($request_type);
        $query_append = "UPDATE $query_table SET existing_contact_list = CONCAT_WS('', existing_contact_list, ' ".$name_con.".".$request_id_con."') WHERE id =  '".$tactic_request_id."'";
        $mysqli->query($query_append);
    }
    

    /////////////////////////////////////////////////////////////////////////////////////
    //Create a note
    $query_note_con = "INSERT INTO notes VALUES (NULL, '".$request_id_con."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
    $mysqli->query($query_note_con);


    /////////////////////////////////////////////////////////////////////////////////////
    //Send email to Intel
    $name_con = cleanForEmail($name_con);
    $list_tag = cleanForEmail($list_tag, 1);

    $message_ms_con = $message_header;
    $message_ms_con.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new contact list has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">".$name_con.".".$request_id_con."<br /></p>
        <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
            <tr>
                <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
            </tr>
            <tr>
                <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
            </tr>
        </table>
    <p><a href=\"".$domain."/request-detail?id=".$request_id_con."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

    $message_ms_con.=$message_footer;

    $mail3->AddAddress($email_ms);
    //$mail3->AddCC('dianex.c.schroeder@intel.com', 'Diane Schroeder'); //Special person
    //$mail3->AddCC('seamusx.james@intel.com', 'Seamus James'); //Special person
    //Special for Doug Marshall and Kristi
    //if ($user_id == 365) { $mail3->AddCC('kristi@ironhorseinteractive.com', 'Kristi Teplitz'); }
    $mail3->Subject = 'Intel Customer Connect: New Contact List: '.$list_tag.'.'.$request_id_con;
    $mail3->MsgHTML($message_ms_con);
    $mail3->AltBody = 'A new update request has been submitted. '.$domain.'/request-detail?id='.$request_id_con.'';
    $mail3->Send();


    //Send email user
    $message_user_con = $message_header;
    $message_user_con.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/request-detail?id=".$request_id_con."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
    $message_user_con.=$message_footer;

    $mail4->AddAddress($user_email);
    $mail4->Subject = 'Intel Customer Connect: Contact List Received: '.$list_tag.'.'.$request_id_con;
    $mail4->MsgHTML($message_user_con);
    $mail4->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
    $mail4->Send();
}
?>