<?php
session_start();
require_once('validate.php');
include('_globals.php');

if (!empty($_REQUEST)) {  // if not empty request data

    $start_date = $end_date = "NULL";
    $start_time_db = $end_time_db = $campaign_id = $additional_owners = $additional_own = "";

    $user_id = $_SESSION['user_id'];
    $request_type = $_REQUEST['request_type'];
    $name = $mysqli->real_escape_string($_REQUEST['name']); 
    $description = $mysqli->real_escape_string($_REQUEST['description']);
    $additional_owners = $_REQUEST['additional-rights-users'];

    if(!empty($_REQUEST['start_date'])) // if not empty start date
        $start_date = date('Y-m-d 00:00:00', strtotime($_REQUEST['start_date'])); //date format

    if(!empty($_REQUEST['end_date'])) // if not empty end date
        $end_date = date('Y-m-d 00:00:00', strtotime($_REQUEST['end_date'])); //date format

    if(!empty($_REQUEST['start_time']))  // if not empty start time
        $start_time_db = $_REQUEST['start_time'].' '.$_REQUEST['start_ampm'];

    if(!empty($_REQUEST['end_time'])) // if not empty end time
        $end_time_db = $_REQUEST['end_time'].' '.$_REQUEST['end_ampm'];
    
    if(!empty($_REQUEST['campaign_search_popup']))
        $campaign_id = getCampaignIdFromName($_REQUEST['campaign_search_popup']); // get campaign id from name
    
    if(!empty($additional_owners) && is_array($additional_owners)) {
        $additional_own = $mysqli->real_escape_string(serialize($additional_owners)); // serialized sring
    }

    if($request_type != 8 && empty($campaign_id)) {  // if incorrect campaign name entered
        echo "Please enter a correct campaign name";
        return false;
    }

    if(empty($name) || empty($description)) {  // if name and description fields are empty
        echo "Please make sure name and description fields are not empty";
        return false;
    }

    $owner = getUserName($user_id); // get owner name
    $owner_email = getEmailAddress($user_id); // get owner email address

    $owner = $mysqli->real_escape_string($owner);
    $owner_email = $mysqli->real_escape_string($owner_email);

    //Create a new request with placeholder bit set
    $request_id = createInProgressRequest($user_id, $request_type, $campaign_id, $additional_own);
    
    // generating insertion query based on request type selected
    switch ($request_type) {
        case 1: // webinar
                $query = "INSERT INTO request_webinar (user_id,request_id,name,webinar_title,description,start_date,
        start_time,end_time,tag,active,owner,owner_email) VALUES (
                    '".$user_id."',
                    '".$request_id."',
                    '".$name."',
                    '".$name."',
                    '".$description."',
                    '".$start_date."',
                    '".$start_time_db."',
                    '".$end_time_db."',
                    '".$name."',
                     1,
                     '".$owner."',
                     '".$owner_email."')";            
            break;
        case 2: // eNurture
        case 7: // Landing Page

                switch ($request_type) {  // change table name based on request type
                    case 2:
                        $table_request = "request_enurture";
                        break;
                    case 7:
                        $table_request = "request_landingpage";
                        break;       
                }

                $query = "INSERT INTO $table_request (user_id,request_id,name,description,start_date,
        end_date,tag,active,owner,owner_email) VALUES (
                    '".$user_id."',
                    '".$request_id."',
                    '".$name."',
                    '".$description."',
                    '".$start_date."',
                    '".$end_date."',
                    '".$name."',
                     1,
                     '".$owner."',
                     '".$owner_email."')";            
            break;    
        case 8: // campaign

                $query = "INSERT INTO request_campaign (user_id,request_id,name,description,start_date,
        end_date,tag,active) VALUES (
                    '".$user_id."',
                    '".$request_id."',
                    '".$name."',
                    '".$description."',
                    '".$start_date."',
                    '".$end_date."',
                    '".$name."',
                     1)";            
            break;        

        case 5: // Newsletter
                $query = "INSERT INTO request_newsletter (user_id,request_id,name,description,start_date,
        end_date,send_time,tag,active,owner,owner_email) VALUES (
                    '".$user_id."',
                    '".$request_id."',
                    '".$name."',
                    '".$description."',
                    '".$start_date."',
                    '".$end_date."',
                    '".$start_time_db."',
                    '".$name."',
                     1,
                     '".$owner."',
                     '".$owner_email."')";            
            break;    
        case 6: // Single Email
                $query = "INSERT INTO request_email (user_id,request_id,name,description,send_date,
        send_time,tag,active,owner,owner_email) VALUES (
                    '".$user_id."',
                    '".$request_id."',
                    '".$name."',
                    '".$description."',
                    '".$start_date."',
                    '".$start_time_db."',
                    '".$name."',
                     1,
                     '".$owner."',
                     '".$owner_email."')";            
            break;          
        
        default:
            $query = "";
            break;
    }

    if(!empty($query)) { // if not empty query string
        if($mysqli->query($query)) 
            echo 'success';
    }
}

?>
