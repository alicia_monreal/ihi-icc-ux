<?php
session_start();
include('_globals.php');

if (!empty($_POST) && !empty($_POST['action'])) {

    //This is the new starting point. Step 0. Create some sessions variables and send to the correct form
    if ($_POST['step'] == 0) {
        //Let's register some session variables so we can have these later
        $geo_array = array();
        foreach($_POST['geo'] as $val_geo) {
            array_push($geo_array, $val_geo);
        }
        $_SESSION['geo'] = $geo_array;
        $_SESSION['sector'] = $_POST['sector'];
        $_SESSION['subsector'] = $_POST['subsector'];
        $_SESSION['subsector_other'] = $_POST['subsector_other'];
        $_SESSION['type'] = $_POST['type'];
        $_SESSION['campaign_details'] = $_POST['campaign_details'];
        $_SESSION['language'] = $_POST['language'];

        //Now send them to Step 2
        switch ($_POST['type']) {
            case 'webinar':
                header('Location:/campaign-requests-webinar-2');
                break;
            case 'enurture':
                header('Location:/campaign-requests-enurture-2');
                break;
            case 'event':
                header('Location:/campaign-requests-event-2');
                break;
        }
    }

    //This is the starting point. Choosing what kind of request and how we route them.
    if ($_POST['step'] == 1) {
        //Let's register some session variables so we can have these later
        $_SESSION['type'] = $_POST['type'];
        $_SESSION['email_template'] = $_POST['email_template'];

        //Now send them to Step 2
        switch ($_POST['type']) {
            case 'webinar':
                header('Location:/campaign-requests-webinar-2');
                break;
            case 'event':
                header('Location:/campaign-requests-event-2');
                break;
        }
    }

    //Step 2. Saving values to session data
    if ($_POST['step'] == 2) {

        //WEBINAR
        if ($_SESSION['type'] == 'webinar') {
            //All our session values
            $_SESSION['webinar_title'] = $_POST['webinar_title'];
            $_SESSION['webinar_owner'] = $_POST['webinar_owner'];
            $_SESSION['webinar_date'] = $_POST['webinar_date'];
            $_SESSION['time'] = $_POST['time'];
            $_SESSION['ampm'] = $_POST['ampm'];
            $_SESSION['time_zone'] = $_POST['time_zone'];
            $_SESSION['length'] = $_POST['length'];
            $_SESSION['expected_attendees'] = $_POST['expected_attendees'];
            $geo_array = array();
            foreach($_POST['geo'] as $val_geo) {
                array_push($geo_array, $val_geo);
            }
            $_SESSION['geo'] = $geo_array;
            $_SESSION['webinar_initial_date'] = $_POST['webinar_initial_date'];
            $_SESSION['webinar_reminder_1_date'] = $_POST['webinar_reminder_1_date'];
            $_SESSION['webinar_reminder_2_date'] = $_POST['webinar_reminder_2_date'];
            $topics_array = array();
            foreach($_POST['topics_covered'] as $val_topics) {
                array_push($topics_array, $val_topics);
            }
            $_SESSION['topics_covered'] = $topics_array;
            $_SESSION['topics_covered_other'] = $_POST['topics_covered_other'];
            $_SESSION['description'] = $_POST['description'];
            $_SESSION['content_type'] = $_POST['content_type'];
            $_SESSION['series_based'] = $_POST['series_based'];
            $_SESSION['series_based_name'] = $_POST['series_based_name'];
            $_SESSION['sector'] = $_POST['sector'];
            $_SESSION['subsector'] = $_POST['subsector'];
            $_SESSION['subsector_other'] = $_POST['subsector_other'];
        }

        //Now send them to Step 3
        switch ($_SESSION['type']) {
            case 'webinar':
                header('Location:/campaign-requests-webinar-3');
                break;
            case 'event':
                header('Location:/campaign-requests-event-3');
                break;
        }
    }

    //Step 3. Saving values to session data
    if ($_POST['step'] == 3) {

        //WEBINAR
        if ($_SESSION['type'] == 'webinar') {
            //All our session values

            //Presentation file
            if (isset($_SESSION['files_presentation']) && !isset($_POST['files_presentation'])) {
                $_SESSION['files_presentation'] = $_SESSION['files_presentation'];
            } else {
                $_SESSION['files_presentation'] = $_POST['files_presentation'];
            }

            //Q&A (this is hardcoded as YES, so we don't need this)
            /*if (isset($_POST['qa']) && $_POST['qa'] == 1) {
                $_SESSION['qa'] = 1;
            } else {
                $_SESSION['qa'] = 0;
            }*/

            //Media files
            if (isset($_SESSION['files_media']) && !isset($_POST['files_media'])) {
                $_SESSION['files_media'] = $_SESSION['files_media'];
            } else {
                $media_array = array();
                foreach ($_POST['files_media'] as $val_media) {
                    array_push($media_array, $val_media);
                }
                $_SESSION['files_media'] = $media_array;
            }

            //Speakers
            //Breaking down individually. Not the best way to do it, but short on time
            $_SESSION['speaker_option'] = $_POST['speaker_option'];

            $_SESSION['speaker_name_1'] = $_POST['speaker_name_1'];
            $speaker_type_1_array = array();
            if (!empty($_POST['speaker_type_1'])) {
                foreach($_POST['speaker_type_1'] as $val_speaker_type_1) {
                    array_push($speaker_type_1_array, $val_speaker_type_1);
                }
            }
            $_SESSION['speaker_type_1'] = $speaker_type_1_array;
            $_SESSION['speaker_title_1'] = $_POST['speaker_title_1'];
            $_SESSION['speaker_company_1'] = $_POST['speaker_company_1'];
            $_SESSION['speaker_email_1'] = $_POST['speaker_email_1'];
            $_SESSION['speaker_bio_1'] = $_POST['speaker_bio_1'];
            if (isset($_SESSION['files_photo_1']) && !isset($_POST['files_photo_1'])) { $_SESSION['files_photo_1'] = $_SESSION['files_photo_1']; } else { $_SESSION['files_photo_1'] = $_POST['files_photo_1']; }

            $_SESSION['speaker_name_2'] = $_POST['speaker_name_2'];
            $speaker_type_2_array = array();
            if (!empty($_POST['speaker_type_2'])) {
                foreach($_POST['speaker_type_2'] as $val_speaker_type_2) {
                    array_push($speaker_type_2_array, $val_speaker_type_2);
                }
            }
            $_SESSION['speaker_type_2'] = $speaker_type_2_array;
            $_SESSION['speaker_title_2'] = $_POST['speaker_title_2'];
            $_SESSION['speaker_company_2'] = $_POST['speaker_company_2'];
            $_SESSION['speaker_email_2'] = $_POST['speaker_email_2'];
            $_SESSION['speaker_bio_2'] = $_POST['speaker_bio_2'];
            if (isset($_SESSION['files_photo_2']) && !isset($_POST['files_photo_2'])) { $_SESSION['files_photo_2'] = $_SESSION['files_photo_2']; } else { $_SESSION['files_photo_2'] = $_POST['files_photo_2']; }

            $_SESSION['speaker_name_3'] = $_POST['speaker_name_3'];
            $speaker_type_3_array = array();
            if (!empty($_POST['speaker_type_3'])) {
                foreach($_POST['speaker_type_3'] as $val_speaker_type_3) {
                    array_push($speaker_type_3_array, $val_speaker_type_3);
                }
            }
            $_SESSION['speaker_type_3'] = $speaker_type_3_array;
            $_SESSION['speaker_title_3'] = $_POST['speaker_title_3'];
            $_SESSION['speaker_company_3'] = $_POST['speaker_company_3'];
            $_SESSION['speaker_email_3'] = $_POST['speaker_email_3'];
            $_SESSION['speaker_bio_3'] = $_POST['speaker_bio_3'];
            if (isset($_SESSION['files_photo_3']) && !isset($_POST['files_photo_3'])) { $_SESSION['files_photo_3'] = $_SESSION['files_photo_3']; } else { $_SESSION['files_photo_3'] = $_POST['files_photo_3']; }

            $_SESSION['speaker_name_4'] = $_POST['speaker_name_4'];
            $speaker_type_4_array = array();
            if (!empty($_POST['speaker_type_4'])) {
                foreach($_POST['speaker_type_4'] as $val_speaker_type_4) {
                    array_push($speaker_type_4_array, $val_speaker_type_4);
                }
            }
            $_SESSION['speaker_type_4'] = $speaker_type_4_array;
            $_SESSION['speaker_title_4'] = $_POST['speaker_title_4'];
            $_SESSION['speaker_company_4'] = $_POST['speaker_company_4'];
            $_SESSION['speaker_email_4'] = $_POST['speaker_email_4'];
            $_SESSION['speaker_bio_4'] = $_POST['speaker_bio_4'];
            if (isset($_SESSION['files_photo_4']) && !isset($_POST['files_photo_4'])) { $_SESSION['files_photo_4'] = $_SESSION['files_photo_4']; } else { $_SESSION['files_photo_4'] = $_POST['files_photo_4']; }

            $_SESSION['speaker_name_5'] = $_POST['speaker_name_5'];
            $speaker_type_5_array = array();
            if (!empty($_POST['speaker_type_5'])) {
                foreach($_POST['speaker_type_5'] as $val_speaker_type_5) {
                    array_push($speaker_type_5_array, $val_speaker_type_5);
                }
            }
            $_SESSION['speaker_type_5'] = $speaker_type_5_array;
            $_SESSION['speaker_title_5'] = $_POST['speaker_title_5'];
            $_SESSION['speaker_company_5'] = $_POST['speaker_company_5'];
            $_SESSION['speaker_email_5'] = $_POST['speaker_email_5'];
            $_SESSION['speaker_bio_5'] = $_POST['speaker_bio_5'];
            if (isset($_SESSION['files_photo_5']) && !isset($_POST['files_photo_5'])) { $_SESSION['files_photo_5'] = $_SESSION['files_photo_5']; } else { $_SESSION['files_photo_5'] = $_POST['files_photo_5']; }

            $_SESSION['speaker_name_6'] = $_POST['speaker_name_6'];
            $speaker_type_6_array = array();
            if (!empty($_POST['speaker_type_6'])) {
                foreach($_POST['speaker_type_6'] as $val_speaker_type_6) {
                    array_push($speaker_type_6_array, $val_speaker_type_6);
                }
            }
            $_SESSION['speaker_type_6'] = $speaker_type_6_array;
            $_SESSION['speaker_title_6'] = $_POST['speaker_title_6'];
            $_SESSION['speaker_company_6'] = $_POST['speaker_company_6'];
            $_SESSION['speaker_email_6'] = $_POST['speaker_email_6'];
            $_SESSION['speaker_bio_6'] = $_POST['speaker_bio_6'];
            if (isset($_SESSION['files_photo_6']) && !isset($_POST['files_photo_6'])) { $_SESSION['files_photo_6'] = $_SESSION['files_photo_6']; } else { $_SESSION['files_photo_6'] = $_POST['files_photo_6']; }

            $_SESSION['speaker_name_7'] = $_POST['speaker_name_7'];
            $speaker_type_7_array = array();
            if (!empty($_POST['speaker_type_7'])) {
                foreach($_POST['speaker_type_7'] as $val_speaker_type_7) {
                    array_push($speaker_type_7_array, $val_speaker_type_7);
                }
            }
            $_SESSION['speaker_type_7'] = $speaker_type_7_array;
            $_SESSION['speaker_title_7'] = $_POST['speaker_title_7'];
            $_SESSION['speaker_company_7'] = $_POST['speaker_company_7'];
            $_SESSION['speaker_email_7'] = $_POST['speaker_email_7'];
            $_SESSION['speaker_bio_7'] = $_POST['speaker_bio_7'];
            if (isset($_SESSION['files_photo_7']) && !isset($_POST['files_photo_7'])) { $_SESSION['files_photo_7'] = $_SESSION['files_photo_7']; } else { $_SESSION['files_photo_7'] = $_POST['files_photo_7']; }

            $_SESSION['speaker_name_8'] = $_POST['speaker_name_8'];
            $speaker_type_8_array = array();
            if (!empty($_POST['speaker_type_8'])) {
                foreach($_POST['speaker_type_8'] as $val_speaker_type_8) {
                    array_push($speaker_type_8_array, $val_speaker_type_8);
                }
            }
            $_SESSION['speaker_type_8'] = $speaker_type_8_array;
            $_SESSION['speaker_title_8'] = $_POST['speaker_title_8'];
            $_SESSION['speaker_company_8'] = $_POST['speaker_company_8'];
            $_SESSION['speaker_email_8'] = $_POST['speaker_email_8'];
            $_SESSION['speaker_bio_8'] = $_POST['speaker_bio_8'];
            if (isset($_SESSION['files_photo_8']) && !isset($_POST['files_photo_8'])) { $_SESSION['files_photo_8'] = $_SESSION['files_photo_8']; } else { $_SESSION['files_photo_8'] = $_POST['files_photo_8']; }

            $_SESSION['speaker_name_9'] = $_POST['speaker_name_9'];
            $speaker_type_9_array = array();
            if (!empty($_POST['speaker_type_9'])) {
                foreach($_POST['speaker_type_9'] as $val_speaker_type_9) {
                    array_push($speaker_type_9_array, $val_speaker_type_9);
                }
            }
            $_SESSION['speaker_type_9'] = $speaker_type_9_array;
            $_SESSION['speaker_title_9'] = $_POST['speaker_title_9'];
            $_SESSION['speaker_company_9'] = $_POST['speaker_company_9'];
            $_SESSION['speaker_email_9'] = $_POST['speaker_email_9'];
            $_SESSION['speaker_bio_9'] = $_POST['speaker_bio_9'];
            if (isset($_SESSION['files_photo_9']) && !isset($_POST['files_photo_9'])) { $_SESSION['files_photo_9'] = $_SESSION['files_photo_9']; } else { $_SESSION['files_photo_9'] = $_POST['files_photo_9']; }

            $_SESSION['speaker_name_10'] = $_POST['speaker_name_10'];
            $speaker_type_10_array = array();
            if (!empty($_POST['speaker_type_10'])) {
                foreach($_POST['speaker_type_10'] as $val_speaker_type_10) {
                    array_push($speaker_type_10_array, $val_speaker_type_10);
                }
            }
            $_SESSION['speaker_type_10'] = $speaker_type_10_array;
            $_SESSION['speaker_title_10'] = $_POST['speaker_title_10'];
            $_SESSION['speaker_company_10'] = $_POST['speaker_company_10'];
            $_SESSION['speaker_email_10'] = $_POST['speaker_email_10'];
            $_SESSION['speaker_bio_10'] = $_POST['speaker_bio_10'];
            if (isset($_SESSION['files_photo_10']) && !isset($_POST['files_photo_10'])) { $_SESSION['files_photo_10'] = $_SESSION['files_photo_10']; } else { $_SESSION['files_photo_10'] = $_POST['files_photo_10']; }

            //URL
            if (isset($_POST['url']) && $_POST['url'] == 1) {
                $_SESSION['url'] = 1;
                $_SESSION['url_value'] = $_POST['url_value'];
            } else {
                $_SESSION['url'] = 0;
            }

            //Survey file
            if (isset($_POST['surveycheck']) && $_POST['surveycheck'] == 1) {
                $_SESSION['surveycheck'] = 1;
            } else {
                $_SESSION['surveycheck'] = 0;
            }

            if (isset($_SESSION['files_survey']) && !isset($_POST['files_survey'])) {
                $_SESSION['files_survey'] = $_SESSION['files_survey'];
            } else {
                $_SESSION['files_survey'] = $_POST['files_survey'];
            }

            //Social Media
            if (isset($_POST['socialmedia']) && $_POST['socialmedia'] == 1) {
                $_SESSION['socialmedia'] = 1;
                $_SESSION['socialmedia_url'] = $_POST['socialmedia_url'];
            } else {
                $_SESSION['socialmedia'] = 0;
            }

            //Resource files
            if (isset($_POST['resourcelist']) && $_POST['resourcelist'] == 1) {
                $_SESSION['resourcelist'] = 1;
                $_SESSION['resourcelist_url'] = $_POST['resourcelist_url'];
            } else {
                $_SESSION['resourcelist'] = 0;
            }

            if (isset($_SESSION['files_resource']) && !isset($_POST['files_resource'])) {
                $_SESSION['files_resource'] = $_SESSION['files_resource'];
            } else {
                $resource_array = array();
                foreach ($_POST['files_resource'] as $val_resource) {
                    array_push($resource_array, $val_resource);
                }
                $_SESSION['files_resource'] = $resource_array;
            }

        }

        //Now send them to Step 4
        switch ($_SESSION['type']) {
            case 'webinar':
                header('Location:/campaign-requests-webinar-4');
                break;
            case 'event':
                header('Location:/campaign-requests-event-4');
                break;
        }
    }


    //Step 4. Saving values to session data, AND writing to the database
    if ($_POST['step'] == 4) {

        //WEBINAR
        if ($_SESSION['type'] == 'webinar') {
            //Lots of variables here. Don't need to save to session for Step 4 because we're wiping it all out anyway.
            $user_id = $_POST['user_id'];
            $user_email = $_POST['user_email'];

            //STEP 1
            $type = $_SESSION['type'];
            $email_template = $_SESSION['email_template'];

            //STEP 2
            $webinar_title = $mysqli->real_escape_string($_SESSION['webinar_title']);
            $webinar_owner = $mysqli->real_escape_string($_SESSION['webinar_owner']);
            //$webinar_date = $mysqli->real_escape_string($_SESSION['webinar_date']);
            $webinar_date = date('Y-m-d 00:00:00', strtotime($_SESSION['webinar_date'])); //date format
            $time = $mysqli->real_escape_string($_SESSION['time']).''.$_SESSION['ampm'];
            $time_zone = $mysqli->real_escape_string($_SESSION['time_zone']);
            $length = $mysqli->real_escape_string($_SESSION['length']);
            $expected_attendees = $mysqli->real_escape_string($_SESSION['expected_attendees']);
            $geo_db = implode(", ",$_SESSION['geo']); //array
            /*$webinar_initial_date = $mysqli->real_escape_string($_SESSION['webinar_initial_date']);
            $webinar_reminder_1_date = $mysqli->real_escape_string($_SESSION['webinar_reminder_1_date']);
            $webinar_reminder_2_date = $mysqli->real_escape_string($_SESSION['webinar_reminder_2_date']);*/
            $webinar_initial_date = date('Y-m-d 00:00:00', strtotime($_SESSION['webinar_initial_date'])); //date format
            $webinar_reminder_1_date = date('Y-m-d 00:00:00', strtotime($_SESSION['webinar_reminder_1_date'])); //date format
            $webinar_reminder_2_date = date('Y-m-d 00:00:00', strtotime($_SESSION['webinar_reminder_2_date'])); //date format
            $topics_covered_db = implode(", ",$_SESSION['topics_covered']); //array
            $topics_covered_other = $mysqli->real_escape_string($_SESSION['topics_covered_other']);
            $description = $mysqli->real_escape_string($_SESSION['description']);
            $content_type = $mysqli->real_escape_string($_SESSION['content_type']);
            $series_based = $mysqli->real_escape_string($_SESSION['series_based']);
            $series_based_name = $mysqli->real_escape_string($_SESSION['series_based_name']);
            $sector = $mysqli->real_escape_string($_SESSION['sector']);
            $subsector = $mysqli->real_escape_string($_SESSION['subsector']);
            $subsector_other = $mysqli->real_escape_string($_SESSION['subsector_other']);

            //STEP 3
            $files_presentation = $mysqli->real_escape_string($_SESSION['files_presentation']);
            $files_media = $_SESSION['files_media']; //array
            $speaker_option = $mysqli->real_escape_string($_SESSION['speaker_option']);
            
            $speaker_name_1 = $mysqli->real_escape_string($_SESSION['speaker_name_1']);
            $speaker_type_db_1 = implode(", ",$_SESSION['speaker_type_1']); //array
            $speaker_title_1 = $mysqli->real_escape_string($_SESSION['speaker_title_1']);
            $speaker_company_1 = $mysqli->real_escape_string($_SESSION['speaker_company_1']);
            $speaker_email_1 = $mysqli->real_escape_string($_SESSION['speaker_email_1']);
            $speaker_bio_1 = $mysqli->real_escape_string($_SESSION['speaker_bio_1']);
            $files_photo_1 = $mysqli->real_escape_string($_SESSION['files_photo_1']);

            $speaker_name_2 = $mysqli->real_escape_string($_SESSION['speaker_name_2']);
            $speaker_type_db_2 = implode(", ",$_SESSION['speaker_type_2']); //array
            $speaker_title_2 = $mysqli->real_escape_string($_SESSION['speaker_title_2']);
            $speaker_company_2 = $mysqli->real_escape_string($_SESSION['speaker_company_2']);
            $speaker_email_2 = $mysqli->real_escape_string($_SESSION['speaker_email_2']);
            $speaker_bio_2 = $mysqli->real_escape_string($_SESSION['speaker_bio_2']);
            $files_photo_2 = $mysqli->real_escape_string($_SESSION['files_photo_2']);

            $speaker_name_3 = $mysqli->real_escape_string($_SESSION['speaker_name_3']);
            $speaker_type_db_3 = implode(", ",$_SESSION['speaker_type_3']); //array
            $speaker_title_3 = $mysqli->real_escape_string($_SESSION['speaker_title_3']);
            $speaker_company_3 = $mysqli->real_escape_string($_SESSION['speaker_company_3']);
            $speaker_email_3 = $mysqli->real_escape_string($_SESSION['speaker_email_3']);
            $speaker_bio_3 = $mysqli->real_escape_string($_SESSION['speaker_bio_3']);
            $files_photo_3 = $mysqli->real_escape_string($_SESSION['files_photo_3']);

            $speaker_name_4 = $mysqli->real_escape_string($_SESSION['speaker_name_4']);
            $speaker_type_db_4 = implode(", ",$_SESSION['speaker_type_4']); //array
            $speaker_title_4 = $mysqli->real_escape_string($_SESSION['speaker_title_4']);
            $speaker_company_4 = $mysqli->real_escape_string($_SESSION['speaker_company_4']);
            $speaker_email_4 = $mysqli->real_escape_string($_SESSION['speaker_email_4']);
            $speaker_bio_4 = $mysqli->real_escape_string($_SESSION['speaker_bio_4']);
            $files_photo_4 = $mysqli->real_escape_string($_SESSION['files_photo_4']);

            $speaker_name_5 = $mysqli->real_escape_string($_SESSION['speaker_name_5']);
            $speaker_type_db_5 = implode(", ",$_SESSION['speaker_type_5']); //array
            $speaker_title_5 = $mysqli->real_escape_string($_SESSION['speaker_title_5']);
            $speaker_company_5 = $mysqli->real_escape_string($_SESSION['speaker_company_5']);
            $speaker_email_5 = $mysqli->real_escape_string($_SESSION['speaker_email_5']);
            $speaker_bio_5 = $mysqli->real_escape_string($_SESSION['speaker_bio_5']);
            $files_photo_5 = $mysqli->real_escape_string($_SESSION['files_photo_5']);

            $speaker_name_6 = $mysqli->real_escape_string($_SESSION['speaker_name_6']);
            $speaker_type_db_6 = implode(", ",$_SESSION['speaker_type_6']); //array
            $speaker_title_6 = $mysqli->real_escape_string($_SESSION['speaker_title_6']);
            $speaker_company_6 = $mysqli->real_escape_string($_SESSION['speaker_company_6']);
            $speaker_email_6 = $mysqli->real_escape_string($_SESSION['speaker_email_6']);
            $speaker_bio_6 = $mysqli->real_escape_string($_SESSION['speaker_bio_6']);
            $files_photo_6 = $mysqli->real_escape_string($_SESSION['files_photo_6']);

            $speaker_name_7 = $mysqli->real_escape_string($_SESSION['speaker_name_7']);
            $speaker_type_db_7 = implode(", ",$_SESSION['speaker_type_7']); //array
            $speaker_title_7 = $mysqli->real_escape_string($_SESSION['speaker_title_7']);
            $speaker_company_7 = $mysqli->real_escape_string($_SESSION['speaker_company_7']);
            $speaker_email_7 = $mysqli->real_escape_string($_SESSION['speaker_email_7']);
            $speaker_bio_7 = $mysqli->real_escape_string($_SESSION['speaker_bio_7']);
            $files_photo_7 = $mysqli->real_escape_string($_SESSION['files_photo_7']);

            $speaker_name_8 = $mysqli->real_escape_string($_SESSION['speaker_name_8']);
            $speaker_type_db_8 = implode(", ",$_SESSION['speaker_type_8']); //array
            $speaker_title_8 = $mysqli->real_escape_string($_SESSION['speaker_title_8']);
            $speaker_company_8 = $mysqli->real_escape_string($_SESSION['speaker_company_8']);
            $speaker_email_8 = $mysqli->real_escape_string($_SESSION['speaker_email_8']);
            $speaker_bio_8 = $mysqli->real_escape_string($_SESSION['speaker_bio_8']);
            $files_photo_8 = $mysqli->real_escape_string($_SESSION['files_photo_8']);

            $speaker_name_9 = $mysqli->real_escape_string($_SESSION['speaker_name_9']);
            $speaker_type_db_9 = implode(", ",$_SESSION['speaker_type_9']); //array
            $speaker_title_9 = $mysqli->real_escape_string($_SESSION['speaker_title_9']);
            $speaker_company_9 = $mysqli->real_escape_string($_SESSION['speaker_company_9']);
            $speaker_email_9 = $mysqli->real_escape_string($_SESSION['speaker_email_9']);
            $speaker_bio_9 = $mysqli->real_escape_string($_SESSION['speaker_bio_9']);
            $files_photo_9 = $mysqli->real_escape_string($_SESSION['files_photo_9']);

            $speaker_name_10 = $mysqli->real_escape_string($_SESSION['speaker_name_10']);
            $speaker_type_db_10 = implode(", ",$_SESSION['speaker_type_10']); //array
            $speaker_title_10 = $mysqli->real_escape_string($_SESSION['speaker_title_10']);
            $speaker_company_10 = $mysqli->real_escape_string($_SESSION['speaker_company_10']);
            $speaker_email_10 = $mysqli->real_escape_string($_SESSION['speaker_email_10']);
            $speaker_bio_10 = $mysqli->real_escape_string($_SESSION['speaker_bio_10']);
            $files_photo_10 = $mysqli->real_escape_string($_SESSION['files_photo_10']);

            $url = $_SESSION['url'];
            $url_value = $mysqli->real_escape_string($_SESSION['url_value']);
            $surveycheck = $_SESSION['surveycheck'];
            $files_survey = $mysqli->real_escape_string($_SESSION['files_survey']);
            $socialmedia = $_SESSION['socialmedia'];
            $socialmedia_url = $mysqli->real_escape_string($_SESSION['socialmedia_url']);
            $resourcelist = $_SESSION['resourcelist'];
            $resourcelist_url = $mysqli->real_escape_string($_SESSION['resourcelist_url']);
            $files_resource = $_SESSION['files_resource']; //array

            //STEP 4
            $files_contacts = $_POST['files_contacts'];
            $contactlist_optin_file = $_POST['contactlist_optin_file'];
            $contactlist_url = $_POST['contactlist_url'];
            $contactlist_optin_url = $_POST['contactlist_optin_url'];
            $eloqua_contact_list = $mysqli->real_escape_string($_POST['eloqua_contact_list']);
            $routing_rules = $mysqli->real_escape_string($_POST['routing_rules']);
            $routing_rules_notify = $_POST['routing_rules_notify'];
            $briefs_array = array();
            foreach($_POST['files_briefs'] as $val_briefs) {
                array_push($briefs_array, $val_briefs);
            }
            $files_regpage = $_POST['files_regpage'];
            $files_confpage = $_POST['files_confpage'];
            $files_logo = $_POST['files_logo'];



            //And now let's send this all into a series of tables
            $query = "INSERT INTO requests VALUES (NULL, '".$user_id."', '".$type."', '".$webinar_title."', '".$webinar_owner."', '".$webinar_date."', '".$time."', '".$time_zone."', '".$length."', '".$expected_attendees."', '".$geo_db."', '".$webinar_initial_date."', '".$webinar_reminder_1_date."', '".$webinar_reminder_2_date."', '".$topics_covered_db."', '".$topics_covered_other."', '".$description."', '".$content_type."', '".$series_based."', '".$series_based_name."', '', '".$sector."', '".$subsector."', '".$subsector_other."', '".$speaker_option."', '".$url."', '".$url_value."', '".$surveycheck."', '".$socialmedia."', '".$socialmedia_url."', '".$resourcelist."', '".$resourcelist_url."', '".$contactlist_optin_file."', '".$contactlist_url."', '".$contactlist_optin_url."', '".$eloqua_contact_list."', '".$routing_rules."', '".$routing_rules_notify."', '".$email_template."', '1', NULL, '', '', '', '', '', '', '', NOW(), NOW(), NULL, '1')";

            $mysqli->query($query);

            if ($mysqli->insert_id) {
                //New request ID
                $request_id = $mysqli->insert_id;

                /////////////////////////////////////////////////////////////////////////////////////
                //Input the speakers
                /*for ($i = 1; $i <= 10; $i++) {
                    if (!empty($speaker_name_$i)) {

                    }
                }*/

                $query_speakers = "INSERT INTO speakers (id, request_id, user_id, speaker_type, speaker_name, speaker_title, speaker_company, speaker_email, speaker_bio, speaker_photo, date_created, date_updated, active) VALUES ";
                if (!empty($speaker_name_1)) { $query_speakers.="(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_1."', '".$speaker_name_1."', '".$speaker_title_1."', '".$speaker_company_1."', '".$speaker_email_1."', '".$speaker_bio_1."', '/server/php/files/".$user_id."/".$files_photo_1."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_2)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_2."', '".$speaker_name_2."', '".$speaker_title_2."', '".$speaker_company_2."', '".$speaker_email_2."', '".$speaker_bio_2."', '/server/php/files/".$user_id."/".$files_photo_2."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_3)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_3."', '".$speaker_name_3."', '".$speaker_title_3."', '".$speaker_company_3."', '".$speaker_email_3."', '".$speaker_bio_3."', '/server/php/files/".$user_id."/".$files_photo_3."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_4)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_4."', '".$speaker_name_4."', '".$speaker_title_4."', '".$speaker_company_4."', '".$speaker_email_4."', '".$speaker_bio_4."', '/server/php/files/".$user_id."/".$files_photo_4."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_5)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_5."', '".$speaker_name_5."', '".$speaker_title_5."', '".$speaker_company_5."', '".$speaker_email_5."', '".$speaker_bio_5."', '/server/php/files/".$user_id."/".$files_photo_5."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_6)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_6."', '".$speaker_name_6."', '".$speaker_title_6."', '".$speaker_company_6."', '".$speaker_email_6."', '".$speaker_bio_6."', '/server/php/files/".$user_id."/".$files_photo_6."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_7)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_7."', '".$speaker_name_7."', '".$speaker_title_7."', '".$speaker_company_7."', '".$speaker_email_7."', '".$speaker_bio_7."', '/server/php/files/".$user_id."/".$files_photo_7."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_8)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_8."', '".$speaker_name_8."', '".$speaker_title_8."', '".$speaker_company_8."', '".$speaker_email_8."', '".$speaker_bio_8."', '/server/php/files/".$user_id."/".$files_photo_8."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_9)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_9."', '".$speaker_name_9."', '".$speaker_title_9."', '".$speaker_company_9."', '".$speaker_email_9."', '".$speaker_bio_9."', '/server/php/files/".$user_id."/".$files_photo_9."', NOW(), NOW(), '1')"; }
                if (!empty($speaker_name_10)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_10."', '".$speaker_name_10."', '".$speaker_title_10."', '".$speaker_company_10."', '".$speaker_email_10."', '".$speaker_bio_10."', '/server/php/files/".$user_id."/".$files_photo_10."', NOW(), NOW(), '1')"; }

                //Insert speakers into db
                $query_speakers.=";";
                $mysqli->query($query_speakers);


                /////////////////////////////////////////////////////////////////////////////////////
                //Input the assets (using individual queries because the comma was throwing things off)
                //$query_docs = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                //Presentation
                if (!empty($files_presentation)) {
                    $query_presentation = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_presentation."', '/server/php/files/".$user_id."/".$files_presentation."', 'Presentation', NOW(), '1')";
                    $mysqli->query($query_presentation);
                }

                //Media
                $media_email = '';
                if (!empty($files_media)) {
                    $media_number = count($files_media);
                    $media_counter = 1;

                    $query_media = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
                    
                    foreach ($files_media as $media) {
                        $query_media.="(NULL, '".$request_id."', '".$user_id."', '".$media."', '/server/php/files/".$user_id."/".$media."', 'Media', NOW(), '1')";
                        $media_counter++;
                        if ($media_counter <= $media_number) {
                            $query_media.=", ";
                        }
                        $media_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$media.'">'.$media.'</a><br />';
                    }
                    $query_media.=";";
                    $mysqli->query($query_media);
                }

                //Survey
                if (!empty($files_survey)) {
                    $query_survey = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_survey."', '/server/php/files/".$user_id."/".$files_survey."', 'Survey', NOW(), '1')";
                    $mysqli->query($query_survey);
                }

                //Resource
                $resource_email = '';
                if (!empty($files_resource)) {
                    $resource_number = count($files_resource);
                    $resource_counter = 1;

                    $query_resource = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                    foreach ($files_resource as $resource) {
                        $query_resource.="(NULL, '".$request_id."', '".$user_id."', '".$resource."', '/server/php/files/".$user_id."/".$resource."', 'Resource', NOW(), '1')";
                        $resource_counter++;
                        if ($resource_counter <= $resource_number) {
                            $query_resource.=", ";
                        }
                        $resource_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$resource.'">'.$resource.'</a><br />';
                    }
                    $query_resource.=";";
                    $mysqli->query($query_resource);
                }

                //Contacts
                if (!empty($files_contacts)) {
                    $query_contacts = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_contacts."', '/server/php/files/".$user_id."/".$files_contacts."', 'Contacts', NOW(), '1')";
                    $mysqli->query($query_contacts);
                }

                //Briefs
                $briefs_email = '';
                if (!empty($briefs_array)) {
                    $briefs_number = count($briefs_array);
                    $briefs_counter = 1;

                    $query_briefs = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                    foreach ($briefs_array as $briefs) {
                        $query_briefs.="(NULL, '".$request_id."', '".$user_id."', '".$briefs."', '/server/php/files/".$user_id."/".$briefs."', 'Briefs', NOW(), '1')";
                        $briefs_counter++;
                        if ($briefs_counter <= $briefs_number) {
                            $query_briefs.=", ";
                        }
                        $briefs_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$briefs.'">'.$briefs.'</a><br />';
                    }
                    $query_briefs.=";";
                    $mysqli->query($query_briefs);
                }

                //Reg Page
                if (!empty($files_regpage)) {
                    $query_regpage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_regpage."', '/server/php/files/".$user_id."/".$files_regpage."', 'Regpage', NOW(), '1')";
                    $mysqli->query($query_regpage);
                }

                //Conf Page
                if (!empty($files_confpage)) {
                    $query_confpage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_confpage."', '/server/php/files/".$user_id."/".$files_confpage."', 'Confpage', NOW(), '1')";
                    $mysqli->query($query_confpage);
                }

                //Logo
                if (!empty($files_logo)) {
                    $query_logo = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_logo."', '/server/php/files/".$user_id."/".$files_logo."', 'Logo', NOW(), '1')";
                    $mysqli->query($query_logo);
                }

                //Finally, insert assets into db
                //$query_docs.=";";
                //$mysqli->query($query_docs);


                /////////////////////////////////////////////////////////////////////////////////////
                //Create a note
                $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
                $mysqli->query($query_note);


                /////////////////////////////////////////////////////////////////////////////////////
                //Send email to Intel
                $description_email = nl2br($_SESSION['description'], false);
                $message_ms = $message_header;
                $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new requests campaign has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">Webinar Title: ".stripslashes($webinar_title)." (#".$request_id.")<br /></p>
                    <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Date</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".date('m/d/Y')."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Owner</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$webinar_owner."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Date</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['webinar_date']."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Time</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$time." (".$time_zone.")</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Length</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$length."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Expected Attendees</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$expected_attendees."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Target GEO</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$geo_db."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Topics Covered</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$topics_covered_db." ".$topics_covered_other."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Description</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$description_email."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Content Type</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$content_type."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Sector</b></p></td>
                            <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$sector."</p></td>
                        </tr>
                        <tr>
                            <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Sub-Sector</b></p></td>
                            <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                            <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">"; if ($subsector == 'Other' || $subsector_other != '') { $message_ms.=$subsector_other; } else { $message_ms.=$subsector; } $message_ms.="</p></td>
                        </tr>
                    </table>
                <p><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

                $message_ms.=$message_footer;

                $mail->AddAddress($email_ms);
                $mail->Subject = 'Intel Customer Connect: New Update Request';
                $mail->MsgHTML($message_ms);
                $mail->AltBody = 'A new update request has been submitted. '.$domain.'/requests?id='.$request_id.'';
                $mail->Send();


                //Send email user
                $message_user = $message_header;
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $message_user.=$message_footer;

                $mail2->AddAddress($user_email);
                $mail2->Subject = 'Intel Customer Connect: Update Request Received';
                $mail2->MsgHTML($message_user);
                $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
                $mail2->Send();

                //We need to kill all these session variables. Oy!
                unset($_SESSION['type']);
                unset($_SESSION['email_template']);
                unset($_SESSION['webinar_title']);
                unset($_SESSION['webinar_owner']);
                unset($_SESSION['webinar_date']);
                unset($_SESSION['time']);
                unset($_SESSION['ampm']);
                unset($_SESSION['time_zone']);
                unset($_SESSION['length']);
                unset($_SESSION['expected_attendees']);
                unset($_SESSION['geo']);
                unset($_SESSION['webinar_initial_date']);
                unset($_SESSION['webinar_reminder_1_date']);
                unset($_SESSION['webinar_reminder_2_date']);
                unset($_SESSION['topics_covered']);
                unset($_SESSION['topics_covered_other']);
                unset($_SESSION['description']);
                unset($_SESSION['content_type']);
                unset($_SESSION['series_based']);
                unset($_SESSION['series_based_name']);
                unset($_SESSION['sector']);
                unset($_SESSION['subsector']);
                unset($_SESSION['subsector_other']);
                unset($_SESSION['files_presentation']);
                unset($_SESSION['files_media']);
                unset($_SESSION['speaker_option']);
                
                for ($i = 1; $i <= 10; $i++) {
                    unset($_SESSION['speaker_name_'.$i]);
                    unset($_SESSION['speaker_type_'.$i]);
                    unset($_SESSION['speaker_title_'.$i]);
                    unset($_SESSION['speaker_company_'.$i]);
                    unset($_SESSION['speaker_email_'.$i]);
                    unset($_SESSION['speaker_bio_'.$i]);
                    unset($_SESSION['files_photo_'.$i]);
                }

                unset($_SESSION['url']);
                unset($_SESSION['url_value']);
                unset($_SESSION['surveycheck']);
                unset($_SESSION['files_survey']);
                unset($_SESSION['socialmedia']);
                unset($_SESSION['socialmedia_url']);
                unset($_SESSION['resourcelist']);
                unset($_SESSION['resourcelist_url']);
                unset($_SESSION['files_resource']);

                //Finally, let's let 'em know
                header('Location:/requests?id='.$request_id.'&action=success');

            }
        }
    }

    //Create new request
    if ($_POST['action'] == 'create4444') {

        //Values
        $user_id = $_POST['user_id'];
        $user_email = $_POST['user_email'];
        $title = $mysqli->real_escape_string($_POST['title']);
        $guidelines = $mysqli->real_escape_string($_POST['guidelines']);
        $request_type = $mysqli->real_escape_string($_POST['request_type']);
        $timeline = $mysqli->real_escape_string($_POST['timeline']);
        if ($timeline == 'Defined') {
            $desired_date = $mysqli->real_escape_string($_POST['desired_date']);
            $desired_date_email = $mysqli->real_escape_string($_POST['desired_date']);
        } else {
            $desired_date = NULL;
            $desired_date_email = $timeline;
        }

        $query = "INSERT INTO requests VALUES (NULL, '".$user_id."', '".$title."', '".$guidelines."', '".$request_type."', '1', NULL, NULL, '".$timeline."', '".$desired_date."', NULL, NOW(), NOW(), NULL, '1')";
        $mysqli->query($query);

        if ($mysqli->insert_id) {
            //New request ID
            $request_id = $mysqli->insert_id;

            $assets_email = '';

            //Insert docs
            if ($_POST['docs']) {
                $doc_number = count($_POST['docs']);
                $doc_counter = 1;

                $query_docs = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                foreach ($_POST['docs'] as $doc) {
                    $query_docs.="(NULL, '".$request_id."', '".$user_id."', '".$doc."', '/server/php/files/".$user_id."/".$doc."', 'doc', NOW(), '1')";
                    $doc_counter++;
                    if ($doc_counter <= $doc_number) {
                        $query_docs.=", ";
                    }
                    $assets_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$doc.'">'.$doc.'</a><br />';
                }
                $query_docs.=";";
                $mysqli->query($query_docs);
            }

            //Insert assets
            if ($_POST['assets']) {
                $asset_number = count($_POST['assets']);
                $asset_counter = 1;

                $query_asset= "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                foreach ($_POST['assets'] as $asset) {
                    $query_asset.="(NULL, '".$request_id."', '".$user_id."', '".$asset."', '/server/php/files/".$user_id."/".$asset."', 'asset', NOW(), '1')";
                    $asset_counter++;
                    if ($asset_counter <= $asset_number) {
                        $query_asset.=", ";
                    }
                    $assets_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$asset.'">'.$asset.'</a><br />';
                }
                $query_asset.=";";
                $mysqli->query($query_asset);
            }

            //Insert external assets
            if ($_POST['external_url']) {
                foreach ($_POST['external_url'] as $key => $value) {
                    if (!empty($value)) {
                        $value = strtolower($value);
                        if (strpos($value, "http") === 0) { $value = $value; } else { $value = 'http://'.$value; }

                        $query_ext.="INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
                        $query_ext.="(NULL, '".$request_id."', '".$user_id."', '".$mysqli->real_escape_string($_POST['external_title'][$key])."', '".$mysqli->real_escape_string($value)."', 'url', NOW(), '1');";
                        $mysqli->query($query_ext);
                        $query_ext= "";

                        if ($_POST['external_title'][$key] != '') {
                            $assets_email.='<a href="'.$value.'">'.$_POST['external_title'][$key].'</a> (URL)<br />';
                        } else {
                            $assets_email.='<a href="'.$value.'">'.$value.'</a> (URL)<br />';
                        }
                    }
                }
                
            }

            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);

            //Send email to MS
            //$guidelines_email = nl2br($guidelines, false); //trying original POST rather than escaped version
            $guidelines_email = nl2br($_POST['guidelines'], false);
            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new update request has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">Request: ".$title." (#".$request_id.")<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Date</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".date('m/d/Y')."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Description</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$guidelines_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Request Type</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$request_type."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Assets Submitted</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$assets_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Status</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">New Request</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Desired Date</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$desired_date_email."</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            $mail->Subject = 'Intel Customer Connect: New Update Request';
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/requests?id='.$request_id.'';
            $mail->Send();


            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Intel Customer Connect: Update Request Received';
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();

            header('Location:/requests?id='.$request_id.'&action=success');
        } else {
            header('Location:/content-update-request?action=failure');
        }

        /* close connection */
        $mysqli->close();
    }
    //END Create new request



    //Update a request
    if ($_POST['action'] == 'update') {

        //Values
        $user_id = $_POST['user_id'];
        $user_email = $_POST['user_email'];
        $admin_id = $_POST['admin_id'];
        $request_id = $_POST['request_id'];
        $status = $_POST['status_update'];
        $notes = $mysqli->real_escape_string($_POST['notes_update']);


        //Only send an email if status has been updated
        $no_status_change = false;
        $query_chk = "SELECT status FROM requests WHERE id = ".$request_id." LIMIT 1";
        $result_chk = $mysqli->query($query_chk);
        while ($obj_chk = $result_chk->fetch_object()) {
            if ($obj_chk->status == $status) { //Status isn't being updated, just something else. Which means no email and no Date Completed update.
                $no_status_change = true;
            }
        }

        $query = "UPDATE requests SET status = '".$status."', date_updated = NOW()";
        if ($status == 4 && $no_status_change == false) {
            $query.=", date_completed = NOW()";

            //Also add a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request completed', NOW(), '1')";
            $mysqli->query($query_note);
        }
        $query.=" WHERE id = ".$request_id."";
        $result = $mysqli->query($query);


        //WE'RE MAKING AN EDIT TO THE REQUEST. MAYBE BY USER, MAYBE BY ADMIN
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == true) {

            //STEP 2
            $webinar_title = $mysqli->real_escape_string($_POST['webinar_title']);
            $webinar_owner = $mysqli->real_escape_string($_POST['webinar_owner']);
            //$webinar_date = $mysqli->real_escape_string($_POST['webinar_date']);
            $time = $mysqli->real_escape_string($_POST['time']);
            $time_zone = $mysqli->real_escape_string($_POST['time_zone']);
            $length = $mysqli->real_escape_string($_POST['length']);
            $expected_attendees = $mysqli->real_escape_string($_POST['expected_attendees']);
            $geo_db = implode(", ",$_POST['geo']); //array
            /*$webinar_initial_date = $mysqli->real_escape_string($_POST['webinar_initial_date']);
            $webinar_reminder_1_date = $mysqli->real_escape_string($_POST['webinar_reminder_1_date']);
            $webinar_reminder_2_date = $mysqli->real_escape_string($_POST['webinar_reminder_2_date']);*/
            $webinar_initial_date = date('Y-m-d 00:00:00', strtotime($_POST['webinar_initial_date'])); //date format
            $webinar_reminder_1_date = date('Y-m-d 00:00:00', strtotime($_POST['webinar_reminder_1_date'])); //date format
            $webinar_reminder_2_date = date('Y-m-d 00:00:00', strtotime($_POST['webinar_reminder_2_date'])); //date format
            $topics_covered_db = implode(", ",$_POST['topics_covered']); //array
            $topics_covered_other = $mysqli->real_escape_string($_POST['topics_covered_other']);
            $description = $mysqli->real_escape_string($_POST['description']);
            $content_type = $mysqli->real_escape_string($_POST['content_type']);
            $series_based = $mysqli->real_escape_string($_POST['series_based']);
            $series_based_name = $mysqli->real_escape_string($_POST['series_based_name']);
            $sector = $mysqli->real_escape_string($_POST['sector']);
            $subsector = $mysqli->real_escape_string($_POST['subsector']);
            $subsector_other = $mysqli->real_escape_string($_POST['subsector_other']);

            //STEP 3
            $files_presentation = $mysqli->real_escape_string($_POST['files_presentation']);
            $files_media = $_POST['files_media']; //array
            $url = $_POST['url'];
            $url_value = $mysqli->real_escape_string($_POST['url_value']);
            $surveycheck = $_POST['surveycheck'];
            $files_survey = $mysqli->real_escape_string($_POST['files_survey']);
            $socialmedia = $_POST['socialmedia'];
            $socialmedia_url = $mysqli->real_escape_string($_POST['socialmedia_url']);
            $resourcelist = $_POST['resourcelist'];
            $resourcelist_url = $mysqli->real_escape_string($_POST['resourcelist_url']);
            $files_resource = $_POST['files_resource']; //array

            //SPEAKERS
            $speaker_name_1 = $mysqli->real_escape_string($_POST['speaker_name_1']);
            $speaker_type_db_1 = implode(", ",$_POST['speaker_type_1']); //array
            $speaker_title_1 = $mysqli->real_escape_string($_POST['speaker_title_1']);
            $speaker_company_1 = $mysqli->real_escape_string($_POST['speaker_company_1']);
            $speaker_email_1 = $mysqli->real_escape_string($_POST['speaker_email_1']);
            $speaker_bio_1 = $mysqli->real_escape_string($_POST['speaker_bio_1']);
            $files_photo_1 = $mysqli->real_escape_string($_POST['files_photo_1']);

            $speaker_name_2 = $mysqli->real_escape_string($_POST['speaker_name_2']);
            $speaker_type_db_2 = implode(", ",$_POST['speaker_type_2']); //array
            $speaker_title_2 = $mysqli->real_escape_string($_POST['speaker_title_2']);
            $speaker_company_2 = $mysqli->real_escape_string($_POST['speaker_company_2']);
            $speaker_email_2 = $mysqli->real_escape_string($_POST['speaker_email_2']);
            $speaker_bio_2 = $mysqli->real_escape_string($_POST['speaker_bio_2']);
            $files_photo_2 = $mysqli->real_escape_string($_POST['files_photo_2']);

            $speaker_name_3 = $mysqli->real_escape_string($_POST['speaker_name_3']);
            $speaker_type_db_3 = implode(", ",$_POST['speaker_type_3']); //array
            $speaker_title_3 = $mysqli->real_escape_string($_POST['speaker_title_3']);
            $speaker_company_3 = $mysqli->real_escape_string($_POST['speaker_company_3']);
            $speaker_email_3 = $mysqli->real_escape_string($_POST['speaker_email_3']);
            $speaker_bio_3 = $mysqli->real_escape_string($_POST['speaker_bio_3']);
            $files_photo_3 = $mysqli->real_escape_string($_POST['files_photo_3']);

            $speaker_name_4 = $mysqli->real_escape_string($_POST['speaker_name_4']);
            $speaker_type_db_4 = implode(", ",$_POST['speaker_type_4']); //array
            $speaker_title_4 = $mysqli->real_escape_string($_POST['speaker_title_4']);
            $speaker_company_4 = $mysqli->real_escape_string($_POST['speaker_company_4']);
            $speaker_email_4 = $mysqli->real_escape_string($_POST['speaker_email_4']);
            $speaker_bio_4 = $mysqli->real_escape_string($_POST['speaker_bio_4']);
            $files_photo_4 = $mysqli->real_escape_string($_POST['files_photo_4']);

            $speaker_name_5 = $mysqli->real_escape_string($_POST['speaker_name_5']);
            $speaker_type_db_5 = implode(", ",$_POST['speaker_type_5']); //array
            $speaker_title_5 = $mysqli->real_escape_string($_POST['speaker_title_5']);
            $speaker_company_5 = $mysqli->real_escape_string($_POST['speaker_company_5']);
            $speaker_email_5 = $mysqli->real_escape_string($_POST['speaker_email_5']);
            $speaker_bio_5 = $mysqli->real_escape_string($_POST['speaker_bio_5']);
            $files_photo_5 = $mysqli->real_escape_string($_POST['files_photo_5']);

            $speaker_name_6 = $mysqli->real_escape_string($_POST['speaker_name_6']);
            $speaker_type_db_6 = implode(", ",$_POST['speaker_type_6']); //array
            $speaker_title_6 = $mysqli->real_escape_string($_POST['speaker_title_6']);
            $speaker_company_6 = $mysqli->real_escape_string($_POST['speaker_company_6']);
            $speaker_email_6 = $mysqli->real_escape_string($_POST['speaker_email_6']);
            $speaker_bio_6 = $mysqli->real_escape_string($_POST['speaker_bio_6']);
            $files_photo_6 = $mysqli->real_escape_string($_POST['files_photo_6']);

            $speaker_name_7 = $mysqli->real_escape_string($_POST['speaker_name_7']);
            $speaker_type_db_7 = implode(", ",$_POST['speaker_type_7']); //array
            $speaker_title_7 = $mysqli->real_escape_string($_POST['speaker_title_7']);
            $speaker_company_7 = $mysqli->real_escape_string($_POST['speaker_company_7']);
            $speaker_email_7 = $mysqli->real_escape_string($_POST['speaker_email_7']);
            $speaker_bio_7 = $mysqli->real_escape_string($_POST['speaker_bio_7']);
            $files_photo_7 = $mysqli->real_escape_string($_POST['files_photo_7']);

            $speaker_name_8 = $mysqli->real_escape_string($_POST['speaker_name_8']);
            $speaker_type_db_8 = implode(", ",$_POST['speaker_type_8']); //array
            $speaker_title_8 = $mysqli->real_escape_string($_POST['speaker_title_8']);
            $speaker_company_8 = $mysqli->real_escape_string($_POST['speaker_company_8']);
            $speaker_email_8 = $mysqli->real_escape_string($_POST['speaker_email_8']);
            $speaker_bio_8 = $mysqli->real_escape_string($_POST['speaker_bio_8']);
            $files_photo_8 = $mysqli->real_escape_string($_POST['files_photo_8']);

            $speaker_name_9 = $mysqli->real_escape_string($_POST['speaker_name_9']);
            $speaker_type_db_9 = implode(", ",$_POST['speaker_type_9']); //array
            $speaker_title_9 = $mysqli->real_escape_string($_POST['speaker_title_9']);
            $speaker_company_9 = $mysqli->real_escape_string($_POST['speaker_company_9']);
            $speaker_email_9 = $mysqli->real_escape_string($_POST['speaker_email_9']);
            $speaker_bio_9 = $mysqli->real_escape_string($_POST['speaker_bio_9']);
            $files_photo_9 = $mysqli->real_escape_string($_POST['files_photo_9']);

            $speaker_name_10 = $mysqli->real_escape_string($_POST['speaker_name_10']);
            $speaker_type_db_10 = implode(", ",$_POST['speaker_type_10']); //array
            $speaker_title_10 = $mysqli->real_escape_string($_POST['speaker_title_10']);
            $speaker_company_10 = $mysqli->real_escape_string($_POST['speaker_company_10']);
            $speaker_email_10 = $mysqli->real_escape_string($_POST['speaker_email_10']);
            $speaker_bio_10 = $mysqli->real_escape_string($_POST['speaker_bio_10']);
            $files_photo_10 = $mysqli->real_escape_string($_POST['files_photo_10']);

            //STEP 4
            $files_contacts = $_POST['files_contacts'];
            $contactlist_optin_file = $_POST['contactlist_optin_file'];
            $contactlist_url = $mysqli->real_escape_string($_POST['contactlist_url']);
            $contactlist_optin_url = $_POST['contactlist_optin_url'];
            $eloqua_contact_list = $mysqli->real_escape_string($_POST['eloqua_contact_list']);
            $routing_rules = $mysqli->real_escape_string($_POST['routing_rules']);
            $routing_rules_notify = $_POST['routing_rules_notify'];
            $briefs_array = array();
            foreach($_POST['files_briefs'] as $val_briefs) {
                array_push($briefs_array, $val_briefs);
            }
            $files_regpage = $_POST['files_regpage'];
            $files_confpage = $_POST['files_confpage'];
            $files_logo = $_POST['files_logo'];

            //And now let's update the request
            $query_edit = "UPDATE requests SET webinar_title = '".$webinar_title."', webinar_owner = '".$webinar_owner."', time = '".$time."', time_zone = '".$time_zone."', length = '".$length."', expected_attendees = '".$expected_attendees."', geo = '".$geo_db."', webinar_initial_date = '".$webinar_initial_date."', webinar_reminder_1_date = '".$webinar_reminder_1_date."', webinar_reminder_2_date = '".$webinar_reminder_2_date."', topics_covered = '".$topics_covered_db."', topics_covered_other = '".$topics_covered_other."', description = '".$description."', content_type = '".$content_type."', series_based = '".$series_based."', sector = '".$sector."', subsector = '".$subsector."', subsector_other = '".$subsector_other."', url = '".$url."', url_value = '".$url_value."', surveycheck = '".$surveycheck."', socialmedia = '".$socialmedia."', socialmedia_url = '".$socialmedia_url."', resourcelist = '".$resourcelist."', resourcelist_url = '".$resourcelist_url."', contactlist_optin_file = '".$contactlist_optin_file."', contactlist_url = '".$contactlist_url."', contactlist_optin_url = '".$contactlist_optin_url."', eloqua_contact_list = '".$eloqua_contact_list."', routing_rules = '".$routing_rules."', routing_rules_notify = '".$routing_rules_notify."', date_updated = NOW() WHERE id = ".$request_id."";
            $result_edit = $mysqli->query($query_edit);

            //And now let's update speakers. Very repetative, this
            if (isset($_POST['speaker_id_1'])) {
                $query_speaker_1 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_1."', speaker_name = '".$speaker_name_1."', speaker_title = '".$speaker_title_1."', speaker_company = '".$speaker_company_1."', speaker_email = '".$speaker_email_1."', speaker_bio = '".$speaker_bio_1."',";
                if ($_POST['files_photo_1']) { $query_speaker_1.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_1."',"; }
                $query_speaker_1.=" date_updated = NOW()";
                if ($_POST['speaker_delete_1'] == 1) { $query_speaker_1.=", active = 0"; }
                $query_speaker_1.=" WHERE id = '".$_POST['speaker_id_1']."'";
                $result_speaker_1 = $mysqli->query($query_speaker_1);
            }
            if (isset($_POST['speaker_id_2'])) {
                $query_speaker_2 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_2."', speaker_name = '".$speaker_name_2."', speaker_title = '".$speaker_title_2."', speaker_company = '".$speaker_company_2."', speaker_email = '".$speaker_email_2."', speaker_bio = '".$speaker_bio_2."',";
                if ($_POST['files_photo_2']) { $query_speaker_2.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_2."',"; }
                $query_speaker_2.=" date_updated = NOW()";
                if ($_POST['speaker_delete_2'] == 1) { $query_speaker_2.=", active = 0"; }
                $query_speaker_2.=" WHERE id = '".$_POST['speaker_id_2']."'";
                $result_speaker_2 = $mysqli->query($query_speaker_2);
            }
            if (isset($_POST['speaker_id_3'])) {
                $query_speaker_3 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_3."', speaker_name = '".$speaker_name_3."', speaker_title = '".$speaker_title_3."', speaker_company = '".$speaker_company_3."', speaker_email = '".$speaker_email_3."', speaker_bio = '".$speaker_bio_3."',";
                if ($_POST['files_photo_3']) { $query_speaker_3.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_3."',"; }
                $query_speaker_3.=" date_updated = NOW()";
                if ($_POST['speaker_delete_3'] == 1) { $query_speaker_3.=", active = 0"; }
                $query_speaker_3.=" WHERE id = '".$_POST['speaker_id_3']."'";
                $result_speaker_3 = $mysqli->query($query_speaker_3);
            }
            if (isset($_POST['speaker_id_4'])) {
                $query_speaker_4 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_4."', speaker_name = '".$speaker_name_4."', speaker_title = '".$speaker_title_4."', speaker_company = '".$speaker_company_4."', speaker_email = '".$speaker_email_4."', speaker_bio = '".$speaker_bio_4."',";
                if ($_POST['files_photo_4']) { $query_speaker_4.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_4."',"; }
                $query_speaker_4.=" date_updated = NOW()";
                if ($_POST['speaker_delete_4'] == 1) { $query_speaker_4.=", active = 0"; }
                $query_speaker_4.=" WHERE id = '".$_POST['speaker_id_4']."'";
                $result_speaker_4 = $mysqli->query($query_speaker_4);
            }
            if (isset($_POST['speaker_id_5'])) {
                $query_speaker_5 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_5."', speaker_name = '".$speaker_name_5."', speaker_title = '".$speaker_title_5."', speaker_company = '".$speaker_company_5."', speaker_email = '".$speaker_email_5."', speaker_bio = '".$speaker_bio_5."',";
                if ($_POST['files_photo_5']) { $query_speaker_5.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_5."',"; }
                $query_speaker_5.=" date_updated = NOW()";
                if ($_POST['speaker_delete_5'] == 1) { $query_speaker_5.=", active = 0"; }
                $query_speaker_5.=" WHERE id = '".$_POST['speaker_id_5']."'";
                $result_speaker_5 = $mysqli->query($query_speaker_5);
            }
            if (isset($_POST['speaker_id_6'])) {
                $query_speaker_6 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_6."', speaker_name = '".$speaker_name_6."', speaker_title = '".$speaker_title_6."', speaker_company = '".$speaker_company_6."', speaker_email = '".$speaker_email_6."', speaker_bio = '".$speaker_bio_6."',";
                if ($_POST['files_photo_6']) { $query_speaker_6.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_6."',"; }
                $query_speaker_6.=" date_updated = NOW()";
                if ($_POST['speaker_delete_6'] == 1) { $query_speaker_6.=", active = 0"; }
                $query_speaker_6.=" WHERE id = '".$_POST['speaker_id_6']."'";
                $result_speaker_6 = $mysqli->query($query_speaker_6);
            }
            if (isset($_POST['speaker_id_7'])) {
                $query_speaker_7 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_7."', speaker_name = '".$speaker_name_7."', speaker_title = '".$speaker_title_7."', speaker_company = '".$speaker_company_7."', speaker_email = '".$speaker_email_7."', speaker_bio = '".$speaker_bio_7."',";
                if ($_POST['files_photo_7']) { $query_speaker_7.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_7."',"; }
                $query_speaker_7.=" date_updated = NOW()";
                if ($_POST['speaker_delete_7'] == 1) { $query_speaker_7.=", active = 0"; }
                $query_speaker_7.=" WHERE id = '".$_POST['speaker_id_7']."'";
                $result_speaker_7 = $mysqli->query($query_speaker_7);
            }
            if (isset($_POST['speaker_id_8'])) {
                $query_speaker_8 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_8."', speaker_name = '".$speaker_name_8."', speaker_title = '".$speaker_title_8."', speaker_company = '".$speaker_company_8."', speaker_email = '".$speaker_email_8."', speaker_bio = '".$speaker_bio_8."',";
                if ($_POST['files_photo_8']) { $query_speaker_8.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_8."',"; }
                $query_speaker_8.=" date_updated = NOW()";
                if ($_POST['speaker_delete_8'] == 1) { $query_speaker_8.=", active = 0"; }
                $query_speaker_8.=" WHERE id = '".$_POST['speaker_id_8']."'";
                $result_speaker_8 = $mysqli->query($query_speaker_8);
            }
            if (isset($_POST['speaker_id_9'])) {
                $query_speaker_9 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_9."', speaker_name = '".$speaker_name_9."', speaker_title = '".$speaker_title_9."', speaker_company = '".$speaker_company_9."', speaker_email = '".$speaker_email_9."', speaker_bio = '".$speaker_bio_9."',";
                if ($_POST['files_photo_9']) { $query_speaker_9.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_9."',"; }
                $query_speaker_9.=" date_updated = NOW()";
                if ($_POST['speaker_delete_9'] == 1) { $query_speaker_9.=", active = 0"; }
                $query_speaker_9.=" WHERE id = '".$_POST['speaker_id_9']."'";
                $result_speaker_9 = $mysqli->query($query_speaker_9);
            }
            if (isset($_POST['speaker_id_10'])) {
                $query_speaker_10 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_10."', speaker_name = '".$speaker_name_10."', speaker_title = '".$speaker_title_10."', speaker_company = '".$speaker_company_10."', speaker_email = '".$speaker_email_10."', speaker_bio = '".$speaker_bio_10."',";
                if ($_POST['files_photo_10']) { $query_speaker_10.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_10."',"; }
                $query_speaker_10.=" date_updated = NOW()";
                if ($_POST['speaker_delete_10'] == 1) { $query_speaker_10.=", active = 0"; }
                $query_speaker_10.=" WHERE id = '".$_POST['speaker_id_10']."'";
                $result_speaker_10 = $mysqli->query($query_speaker_10);
            }

            //Insert a new speaker?
            if (isset($_POST['speaker_id_10']) && $_POST['speaker_id_10'] == 'new' && $_POST['speaker_name_10'] != '') {
                $query_speakers_new = "INSERT INTO speakers (id, request_id, user_id, speaker_type, speaker_name, speaker_title, speaker_company, speaker_email, speaker_bio, speaker_photo, date_created, date_updated, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_10."', '".$speaker_name_10."', '".$speaker_title_10."', '".$speaker_company_10."', '".$speaker_email_10."', '".$speaker_bio_10."', '/server/php/files/".$_SESSION['user_id']."/".$files_photo_10."', NOW(), NOW(), '1')";
                $mysqli->query($query_speakers_new);
            }

            //And now the assets. Let's first remove the assets they requested (we're doing this by marking in-active)
            if (!empty($_POST['delete_asset'])) {
                $query_delete = "UPDATE assets SET active = CASE";
                foreach ($_POST['delete_asset'] as $key => $value) {
                    $query_delete.=" WHEN id = ".$value." THEN 0";
                }
                $query_delete.=" ELSE active END;";
                $result_delete = $mysqli->query($query_delete);
            }

            //Presentation
            if (!empty($files_presentation)) {
                $query_presentation = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_presentation."', '/server/php/files/".$_SESSION['user_id']."/".$files_presentation."', 'Presentation', NOW(), '1')";
                $mysqli->query($query_presentation);
            }

            //Media
            $media_email = '';
            if (!empty($files_media)) {
                $media_number = count($files_media);
                $media_counter = 1;

                $query_media = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
                
                foreach ($files_media as $media) {
                    $query_media.="(NULL, '".$request_id."', '".$user_id."', '".$media."', '/server/php/files/".$_SESSION['user_id']."/".$media."', 'Media', NOW(), '1')";
                    $media_counter++;
                    if ($media_counter <= $media_number) {
                        $query_media.=", ";
                    }
                    $media_email.='<a href="'.$domain.'/server/php/files/'.$_SESSION['user_id'].'/'.$media.'">'.$media.'</a><br />';
                }
                $query_media.=";";
                $mysqli->query($query_media);
            }

            //Survey
            if (!empty($files_survey)) {
                $query_survey = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_survey."', '/server/php/files/".$_SESSION['user_id']."/".$files_survey."', 'Survey', NOW(), '1')";
                $mysqli->query($query_survey);
            }

            //Resource
            $resource_email = '';
            if (!empty($files_resource)) {
                $resource_number = count($files_resource);
                $resource_counter = 1;

                $query_resource = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                foreach ($files_resource as $resource) {
                    $query_resource.="(NULL, '".$request_id."', '".$user_id."', '".$resource."', '/server/php/files/".$_SESSION['user_id']."/".$resource."', 'Resource', NOW(), '1')";
                    $resource_counter++;
                    if ($resource_counter <= $resource_number) {
                        $query_resource.=", ";
                    }
                    $resource_email.='<a href="'.$domain.'/server/php/files/'.$_SESSION['user_id'].'/'.$resource.'">'.$resource.'</a><br />';
                }
                $query_resource.=";";
                $mysqli->query($query_resource);
            }

            //Contacts
            if (!empty($files_contacts)) {
                $query_contacts = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_contacts."', '/server/php/files/".$_SESSION['user_id']."/".$files_contacts."', 'Contacts', NOW(), '1')";
                $mysqli->query($query_contacts);
            }

            //Briefs
            $briefs_email = '';
            if (!empty($briefs_array)) {
                $briefs_number = count($briefs_array);
                $briefs_counter = 1;

                $query_briefs = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                foreach ($briefs_array as $briefs) {
                    $query_briefs.="(NULL, '".$request_id."', '".$user_id."', '".$briefs."', '/server/php/files/".$_SESSION['user_id']."/".$briefs."', 'Briefs', NOW(), '1')";
                    $briefs_counter++;
                    if ($briefs_counter <= $briefs_number) {
                        $query_briefs.=", ";
                    }
                    $briefs_email.='<a href="'.$domain.'/server/php/files/'.$_SESSION['user_id'].'/'.$briefs.'">'.$briefs.'</a><br />';
                }
                $query_briefs.=";";
                $mysqli->query($query_briefs);
            }

            //Reg Page
            if (!empty($files_regpage)) {
                $query_regpage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_regpage."', '/server/php/files/".$_SESSION['user_id']."/".$files_regpage."', 'Regpage', NOW(), '1')";
                $mysqli->query($query_regpage);
            }

            //Conf Page
            if (!empty($files_confpage)) {
                $query_confpage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_confpage."', '/server/php/files/".$_SESSION['user_id']."/".$files_confpage."', 'Confpage', NOW(), '1')";
                $mysqli->query($query_confpage);
            }

            //Logo
            if (!empty($files_logo)) {
                $query_logo = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_logo."', '/server/php/files/".$_SESSION['user_id']."/".$files_logo."', 'Logo', NOW(), '1')";
                $mysqli->query($query_logo);
            }

            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request updated', NOW(), '1')";
            $mysqli->query($query_note);

            //And now alert the sys admin that an update has been made:
            //Send email user
            $message_update = $message_header;
            $message_update.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following request has been updated by its original creator:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
            $alt_update = 'The following request has been updated: '.$domain.'/requests?id='.$request_id;

            $message_update.=$message_footer;

            $mail->AddAddress($email_ms);
            $mail->Subject = 'Intel Customer Connect: Request #'.$request_id.' has been updated';
            $mail->MsgHTML($message_update);
            $mail->AltBody = $alt_update;
            //$mail->Send(); //PUT THIS BACK IN ON LAUNCH

        }


        //Ok, back to normal admin update... which is just notes and additional info and etc
        if ($notes != '') {
            $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', '".$admin_id."', '".$notes."', NOW(), '1')";
            $result_notes = $mysqli->query($query_notes);
        }

        //Update Additional Info
        if ($_SESSION['admin'] == 1) {
            $presenter_info = $mysqli->real_escape_string($_POST['presenter_info']);
            $end_user_info = $mysqli->real_escape_string($_POST['end_user_info']);
            $on24_analytics_url = $mysqli->real_escape_string($_POST['on24_analytics_url']);
            $eloqua_analytics_url = $mysqli->real_escape_string($_POST['eloqua_analytics_url']);
            $regpage_url = $mysqli->real_escape_string($_POST['regpage_url']);
            $aprimo_id = $mysqli->real_escape_string($_POST['aprimo_id']);
            $webinar_id = $mysqli->real_escape_string($_POST['webinar_id']);
            $query_additional = "UPDATE requests SET presenter_info = '".$presenter_info."', end_user_info = '".$end_user_info."', on24_analytics_url = '".$on24_analytics_url."', eloqua_analytics_url = '".$eloqua_analytics_url."', regpage_url = '".$regpage_url."', aprimo_id = '".$aprimo_id."', webinar_id = '".$webinar_id."', date_updated = NOW() WHERE id = ".$request_id."";
            $result_additional = $mysqli->query($query_additional);
        }

        //Send email user
        $message_user = $message_header;

        //Message based on Status
        $alt = '';
        $send = false;
        switch ($status) {
            case 1: //New request
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $alt = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
                $send = false; //Don't think we need to send if status goes back to original, but just in case
                break;
            case 2: //Request in review
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. It is now being reviewed. We will contact you if we have additional questions.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $alt = 'Thank you for submitting your request. It is now being reviewed. We will contact you if we have additional questions.';
                $send = false;
                break;
            case 3: //Request scheduled
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Your request has been scheduled and will be processed based on the standard SLA.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $alt = 'Your request has been scheduled and will be processed based on the standard SLA.';
                $send = false;
                break;
            case 4: //Request complete
                //$message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Your request is complete. Thank you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                //$message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">We want to thank you for using Intel Customer Connect for your recent request. Our records indicate we have completed the request.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">As part of our effort to drive efficiencies <i>and</i> satisfaction for users of the Update Center, we ask that you please participate in a short survey to help us learn more about your experience.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Please take a minute to complete the survey. We have five short questions for you <a href=\"".$domain."/survey?id=".$request_id."\" style=\"color:#1570a6;\">here</a>.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you again.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The Customer Connect team</p>";
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">We want to thank you for using Intel Customer Connect for your recent request. Our records indicate we have completed the request.</p></p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you again.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The Intel Customer Connect team</p>";
                $alt = 'Your request is complete. Thank you.';
                $send = true;
                break;
        }

        $message_user.=$message_footer;

        $mail2->AddAddress($user_email);
        $mail2->Subject = 'Intel Customer Connect: Update Request Status (#'.$request_id.')';
        $mail2->MsgHTML($message_user);
        $mail2->AltBody = $alt;
        if ($send == true && $no_status_change == false) {
            $mail2->Send();
        }





        if ($result || $result_edit) {
            header('Location:/requests?id='.$request_id.'&update=success');
        } else {
            header('Location:/requests?id='.$request_id.'&update=fail');
        }

        /* close connection */
        $mysqli->close();
    }
    //END Update a request

} else {
    header('Location:content-update-request?action=failure'); 
}
?>