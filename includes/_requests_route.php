<?php
session_start();
require_once('validate.php');
ob_start();
include('_globals.php');

if ((!empty($_POST) && !empty($_POST['action'])) || isset($_GET['in_progress_id'])) {
    //In progress check
    if (isset($_GET['in_progress_id']) && !empty($_GET['in_progress_id'])) {
        $in_progress = true;
        $_SESSION['in_progress'] = 1;
        $_SESSION['in_progress_id'] = $_GET['in_progress_id'];
        $request_type = $_GET['in_progress_type'];

        //Step 0 fields
        $_SESSION['request_type'] = $request_type;
        $_SESSION['language'] = getSingleField('language', $request_type, $_GET['in_progress_id']);
        //$_SESSION['marketing_id'] = getSingleField('marketing_id', $request_type, $_GET['marketing_id']);

        //Campaign
        $_SESSION['campaign_id'] = $_GET['campaign_id'];

        //Create some session values for the fields that aren't cloned
        createBulkSessionValues($_GET['in_progress_id'], $request_type);

        //Template session vals
        if (isset($_SESSION['standard_template_type']) && !empty($_SESSION['standard_template_type'])) {
            //DEFAULT
            if (strtolower($_SESSION['standard_template_type']) == 'default') {
                $query_template_d  = "SELECT * FROM templates_data WHERE request_id = '".$_GET['in_progress_id']."' LIMIT 1";
                $result_template_d = $mysqli->query($query_template_d);

                if ($result_template_d->num_rows > 0) {
                    $record_d = (array)$result_template_d->fetch_object();

                    $_SESSION['t_request_id']           =  $_GET['in_progress_id'];
                    $_SESSION['templates_data']         =  $record_d;
                }
            } else if (strtolower($_SESSION['standard_template_type']) == 'custom') {
                //CUSTOM
                if($_SESSION['request_type'] == 2 || $_SESSION['request_type'] == 5) { // if enurture / newsletter

                    // get data from enurture / newsletter templates table
                    $query_template  = "SELECT * FROM multiple_templates_data WHERE request_id = '".$_GET['in_progress_id']."'";
                    $result_template = $mysqli->query($query_template);

                    $row_cnt = $result_template->num_rows;
                    if ($row_cnt > 0) {  // if record exists 
                        while ($obj = $result_template->fetch_object()) { 
         
                            $record['user_id'] = $obj->user_id;
                            $record['request_id'] = $obj->request_id;                

                            if($_SESSION['request_type'] == 2)  // if enurture
                                $request_type_config = "enurture";  
                            else  
                                $request_type_config = "newsletter";

                            if(!empty($obj->email_config))  //email_config exists
                                $record[$request_type_config.'_email_config_'.$obj->order_number] = $obj->email_config;

                            if(!empty($obj->landing_page_config))  // landing_page exists
                                $record[$request_type_config.'_landingpage_config_'.$obj->order_number] = $obj->landing_page_config;
                        }

                        $_SESSION['t_request_id']           =  $_GET['in_progress_id'];
                        $_SESSION['templates_data']         =  $record; // record saved to session
                    }

                } else {
                    $query_template  = "SELECT * FROM templates_custom_data WHERE request_id = '".$_GET['in_progress_id']."' LIMIT 1";
                    $result_template = $mysqli->query($query_template);

                    if ($result_template->num_rows > 0) {
                        $record = (array)$result_template->fetch_object();

                        $_SESSION['t_request_id']           =  $_GET['in_progress_id'];
                        $_SESSION['templates_data']         =  $record;
                    }
                }
            }
        }

    } else {
        $in_progress = false;
        $_SESSION['in_progress'] = 0;
        $_SESSION['in_progress_id'] = NULL;
        $request_type = $_POST['request_type'];

        //Step 0 fields
        $_SESSION['request_type'] = $request_type;
        $_SESSION['language'] = $_POST['language'];
        $_SESSION['marketing_id'] = $_POST['marketing_id'];

        //Shahzad
        //Refreshing template session data just in case. Might not need this.
        //$_SESSION['t_request_id']           =  '';
        //$_SESSION['templates_data']         =  '';
        //$_SESSION['standard_template_type'] =  '';

        //Campaign
        //Only need to worry about this if it's a webinar
        if ($request_type == 1) {
            //if (isset($_POST['eloqua_integrate']) && $_POST['eloqua_integrate'] == 0) { // new Generate Leads question
            if (isset($_POST['generate_leads_q']) && $_POST['generate_leads_q'] == 0) {
                $_SESSION['campaign_id'] = 17;
                $camp = 17;
                $_SESSION['generate_leads'] = 0;

                if (isset($_POST['itp_audience']) && $_POST['itp_audience'] == 1) {
                    if (isset($_POST['target_geography'])) {
                        switch ($_POST['target_geography']) {
                            case 'APJ':
                                $_SESSION['campaign_id'] = 51;
                                $camp = 51;
                                break;
                            case 'ASMO':
                                $_SESSION['campaign_id'] = 54;
                                $camp = 54;
                                break;
                            case 'EMEA':
                                $_SESSION['campaign_id'] = 52;
                                $camp = 52;
                                break;
                            case 'LAR':
                                $_SESSION['campaign_id'] = 53;
                                $camp = 53;
                                break;
                            case 'PRC':
                                $_SESSION['campaign_id'] = 55;
                                $camp = 55;
                                break;
                        }
                    }

                    //ITP logic
                    $_SESSION['ITP'] = true;
                }
            } else {
                //$_SESSION['campaign_id'] = $_POST['campaign_id']; //Normal select
                $_SESSION['campaign_id'] = getCampaignIdFromName($_POST['campaign_search']);
                $camp = getCampaignIdFromName($_POST['campaign_search']);
                $_SESSION['generate_leads'] = 1;
            }
        } else {
            $_SESSION['campaign_id'] = getCampaignIdFromName($_POST['campaign_search']);
        }
    }
    //End in progress

    //ITP logic
    $_SESSION['ITP'] = false;
    if (isset($_SESSION['campaign_id']) && $_SESSION['campaign_id'] != '') {
        $ss = getSubSector($_SESSION['campaign_id']);
        if ($ss == 'Intel Technology Provider (ITP)') { $_SESSION['ITP'] = true; }
    }

    //Cloning a campaign?
    if (isset($_POST['cloned_campaign']) && $_POST['cloned_campaign'] == 1) {
        $clone_campaign_id = getCampaignIdFromName($_POST['request_search_campaign']);
        $_SESSION['clone_campaign_id'] = $clone_campaign_id;
        $_SESSION['cloned_campaign'] = 1;
    } else {
        $_SESSION['clone_campaign_id'] = 0;
        $_SESSION['cloned_campaign'] = 0;
    }

    //Cloning a request?
    if ((isset($_POST['cloned']) && $_POST['cloned'] == 1) || ($in_progress == true)) {

        if ($in_progress == true) {
            $clone_id = getTacticIdFromRequestId($_GET['in_progress_id'], $request_type);
            $_SESSION['clone_id'] = $clone_id;
        } else {
            $clone_id = getRequestIdFromName($_POST['request_search'], $request_type);
            $_SESSION['clone_id'] = $clone_id;
        }

        $_SESSION['clone_user_id'] = getRequestIdFromName($clone_id, $request_type);

        //We need to add assets to the various asset SESSION vars. We're doing this here because of the way assets are handled.
        $query = "SELECT name, path, type FROM assets WHERE request_id = ".getRequestIdFromTacticId($clone_id, $request_type)." AND active = 1";
        $result = $mysqli->query($query);
        $row_cnt = $result->num_rows;
        if ($row_cnt > 0) {
            //Starter arrays
            $_SESSION['files_media'] = array();
            $_SESSION['files_media_path'] = array();
            $_SESSION['files_resource'] = array();
            $_SESSION['files_resource_path'] = array();

            while ($obj = $result->fetch_object()) {
                if ($obj->type == 'Custom Assets') {
                    $_SESSION['files_customassets'] = $obj->name;
                    $_SESSION['files_customassets_path'] = $obj->path;
                }
                if ($obj->type == 'Presentation') {
                    $_SESSION['files_presentation'] = $obj->name;
                    $_SESSION['files_presentation_path'] = $obj->path;
                }
                if ($obj->type == 'Survey') {
                    $_SESSION['files_survey'] = $obj->name;
                    $_SESSION['files_survey_path'] = $obj->path;
                }
                if ($obj->type == 'Briefs') {
                    $_SESSION['files_campaignbrief'] = $obj->name;
                    $_SESSION['files_campaignbrief_path'] = $obj->path;
                }
                if ($obj->type == 'Email Featured Image') {
                    $_SESSION['files_emailfeaturedimage'] = $obj->name;
                    $_SESSION['files_emailfeaturedimage_path'] = $obj->path;
                    //Special for stock image selector
                    $pos = strpos($obj->path, 'email-images');
                    if ($pos !== false) { $_SESSION['email_featured_image'] = $obj->name; } else { $_SESSION['email_featured_image'] = 'custom'; }
                }
                if ($obj->type == 'Registration Page Image') {
                    $_SESSION['files_registrationpageimage'] = $obj->name;
                    $_SESSION['files_registrationpageimage_path'] = $obj->path;
                    //Special for stock image selector
                    $pos_rp = strpos($obj->path, 'email-images');
                    if ($pos_rp !== false) { $_SESSION['registration_page_image'] = $obj->name; } else { $_SESSION['registration_page_image'] = 'custom'; }
                }
                if ($obj->type == 'Confirmation Page Image') {
                    $_SESSION['files_confirmationpageimage'] = $obj->name;
                    $_SESSION['files_confirmationpageimage_path'] = $obj->path;
                    //Special for stock image selector
                    $pos_cp = strpos($obj->path, 'email-images');
                    if ($pos_cp !== false) { $_SESSION['confirmation_page_image'] = $obj->name; } else { $_SESSION['confirmation_page_image'] = 'custom'; }
                }
                if ($obj->type == 'Footer Logo') {
                    $_SESSION['files_footerimage'] = $obj->name;
                    $_SESSION['files_footerimage_path'] = $obj->path;
                }

                //Templates
                if ($obj->type == 'Email Banner Image') {
                    //Special for stock image selector
                    $pos_eh = strpos($obj->path, 'default-images');
                    if ($pos_eh !== false) {
                        $_SESSION['email_banner_image'] = $obj->name;
                    } else {
                        $_SESSION['email_banner_image'] = 'custom';
                        $_SESSION['files_emailbannerimage'] = $obj->name;
                        $_SESSION['files_emailbannerimage_path'] = $obj->path;
                    }
                }
                if ($obj->type == 'Email Body Image') {
                    //Special for stock image selector
                    $pos_eb = strpos($obj->path, 'default-images');
                    if ($pos_eb !== false) {
                        $_SESSION['email_body_image'] = $obj->name;
                    } else {
                        $_SESSION['email_body_image'] = 'custom';
                        $_SESSION['files_emailbodyimage'] = $obj->name;
                        $_SESSION['files_emailbodyimage_path'] = $obj->path;
                    }
                }
                if ($obj->type == 'Registration Page Banner Image') {
                    //Special for stock image selector
                    $pos_ph = strpos($obj->path, 'default-images');
                    if ($pos_ph !== false) {
                        $_SESSION['page_banner_image'] = $obj->name;
                    } else {
                        $_SESSION['page_banner_image'] = 'custom';
                        $_SESSION['files_pagebannerimage'] = $obj->name;
                        $_SESSION['files_pagebannerimage_path'] = $obj->path;
                    }
                }
                if ($obj->type == 'Module: Location Image') {
                    $_SESSION['files_pagelocationimage'] = $obj->name;
                    $_SESSION['files_pagelocationimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Page Content Alt Image') {
                    $_SESSION['files_pagecontentaltimage'] = $obj->name;
                    $_SESSION['files_pagecontentaltimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Speaker Image') {
                    $_SESSION['files_pagespeakerimage'] = $obj->name;
                    $_SESSION['files_pagespeakerimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Sponsor Image') {
                    $_SESSION['files_pagesponsorimage'] = $obj->name;
                    $_SESSION['files_pagesponsorimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Event Image') {
                    $_SESSION['files_pageeventimage'] = $obj->name;
                    $_SESSION['files_pageeventimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Email Location Image') {
                    $_SESSION['files_emaillocationimage'] = $obj->name;
                    $_SESSION['files_emaillocationimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Email Speaker Image') {
                    $_SESSION['files_emailspeakerimage'] = $obj->name;
                    $_SESSION['files_emailspeakerimage_path'] = $obj->path;
                }
                if ($obj->type == 'Module: Email Content Alt Image') {
                    $_SESSION['files_emailcontentaltimage'] = $obj->name;
                    $_SESSION['files_emailcontentaltimage_path'] = $obj->path;
                }


                //Arrays
                if ($obj->type == 'Media') {
                    array_push($_SESSION['files_media'], $obj->name);
                    array_push($_SESSION['files_media_path'], $obj->path);
                }
                if ($obj->type == 'Resource') {
                    array_push($_SESSION['files_resource'], $obj->name);
                    array_push($_SESSION['files_resource_path'], $obj->path);
                }
            }
        }

        //Speakers for Webinars
        if ($request_type == 1) {
            $query = "SELECT * FROM speakers WHERE request_id = ".getRequestIdFromTacticId($clone_id, $request_type)." AND active = 1";
            $result = $mysqli->query($query);
            $row_cnt = $result->num_rows;
            if ($row_cnt > 0) {
                //Forcing the detailed view:
                $_SESSION['speaker_option'] = 'Detailed';

                $cnt = 1;
                while ($obj = $result->fetch_object()) {
                    if ($obj->speaker_type != '') {
                        $speaker_type = explode(', ', $obj->speaker_type);
                        $_SESSION['speaker_type_'.$cnt.''] = $speaker_type;
                    } else {
                        $_SESSION['speaker_type_'.$cnt.''] = '';
                    }
                    
                    $_SESSION['speaker_name_'.$cnt.''] = $obj->speaker_name;
                    $_SESSION['speaker_title_'.$cnt.''] = $obj->speaker_title;
                    $_SESSION['speaker_company_'.$cnt.''] = $obj->speaker_company;
                    $_SESSION['speaker_email_'.$cnt.''] = $obj->speaker_email;
                    $_SESSION['speaker_bio_'.$cnt.''] = $obj->speaker_bio;
                    $_SESSION['speaker_photo_'.$cnt.''] = $obj->speaker_photo;
                    if (strlen($obj->speaker_photo) > 22) { //Should do something different here. At point of creation.
                        $speaker_photo = substr(strrchr(rtrim($obj->speaker_photo, '/'), '/'), 1);
                        $_SESSION['files_photo_'.$cnt.''] = $speaker_photo;
                    }
                    $cnt++;
                }
            }
        }

        /* Not cloning Campaign ID
        if ($request_type != 8) {
            $_SESSION['campaign_id'] = getCampaignIdFromRequestId($clone_id, $request_type);
        }
        */
        
        $_SESSION['cloned'] = 1;
    } else {
        $_SESSION['clone_id'] = 0;
        $_SESSION['cloned'] = 0;
    }


    if (isset($_POST['existing_mid']) && $_POST['existing_mid'] == 0) {
        $_SESSION['marketing_id'] = '';
        $_SESSION['source_activity_name'] = $_POST['source_activity_name'];
        $_SESSION['source_description'] = $_POST['source_description'];
        $_SESSION['source_program'] = $_POST['source_program'];
        $_SESSION['source_activity_type'] = $_POST['source_activity_type'];
        $_SESSION['source_offer_type'] = $_POST['source_offer_type'];
        $_SESSION['marketing_team'] = $_POST['marketing_team'];
    } else {
        $_SESSION['source_activity_name'] = '';
        $_SESSION['source_description'] = '';
        $_SESSION['source_program'] = '';
        $_SESSION['source_activity_type'] = '';
        $_SESSION['source_offer_type'] = '';
        $_SESSION['marketing_team'] = '';
    }
    

    //Now send them to Step 2
    switch ($request_type) {
        case '1':
            header('Location:/request-webinar-2');
            break;
        case '2':
            header('Location:/request-enurture-2');
            break;
        case '4':
            header('Location:/request-contactlist-2');
            break;
        case '5':
            header('Location:/request-newsletter-2');
            break;
        case '6':
            header('Location:/request-email-2');
            break;
        case '7':
            header('Location:/request-landingpage-2');
            break;
        case '8':
            header('Location:/request-campaign-2');
            break;
        case '9':
            header('Location:/request-leadlist-2');
            break;
        case '999999':
            header('Location:/request-leadform-2');
            break;
    }

}
?>