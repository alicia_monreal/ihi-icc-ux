<?php
session_start();
require_once('validate.php');
include('_globals.php');
$data = '';
$today = date('m/d/Y');

if (isset($_GET['type'])) {
	//Tactics
	if ($_GET['type'] == 'tactics') {

		$query = "SELECT
		            req.*,
		            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created_clean,
		            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed_clean,
		            status.value AS status_value,
		            request_type.request_type AS request_type,
		            request_type.id AS request_type_id
		        FROM request AS req
		        INNER JOIN status on req.status = status.id
		        INNER JOIN request_type on req.request_type = request_type.id
		        WHERE req.active = 1
		        AND req.request_type != 8
		        ORDER BY req.id DESC";

		$result = $mysqli->query($query);
		$row_cnt = $result->num_rows;

		if ($row_cnt > 0) {
		    $data.='"Tactic Name","Campaign Name","GEO","Sector","Subsector","Tactic Owner","Assigned To","Request Type","Status","Date Created","Date Completed","Turnaround (hours)"';
		    $data.="\n";
		    while ($obj = $result->fetch_object()) {
		    	//Completed? Let's add a date_completed value
		    	if ($obj->status == 4) {
		    		$date_completed = $obj->date_completed_clean;
		    		$date_diff = strtotime($obj->date_completed) - strtotime($obj->date_created);
     				//$turnaround = floor($date_diff/(60*60*24)); //days
     				$turnaround = floor($date_diff/3600); //hours
		    	} else {
		    		$date_completed = '';
		    		$turnaround = '';
		    	}
		    	

		    	//Get the campaign details, may be able to roll this into a function, but for the time being
		    	$query_cam = "SELECT 
		    					request_campaign.id,
		    					request_campaign.name,
		    					request_campaign.geo,
		    					request_campaign.sector,
		    					request_campaign.subsector
					        FROM request_campaign
					        WHERE request_campaign.id = '".$obj->campaign_id."'
					        AND request_campaign.active = 1
					        LIMIT 1";

				$result_cam = $mysqli->query($query_cam);
				$row_cnt_cam = $result_cam->num_rows;

				if ($row_cnt_cam > 0) {
					while ($obj_cam = $result_cam->fetch_object()) {
						if(!empty($obj->assigned_to)) { // if assigned_to field is not empty
							$assigned_to_username = getUserName($obj->assigned_to); // fetch username
						} else {
							$assigned_to_username = "-"; // set as blank
						}
						$data.='"'.getTitle($obj->id, $obj->request_type_id).'","'.$obj_cam->name.'","'.$obj_cam->geo.'","'.$obj_cam->sector.'","'.$obj_cam->subsector.'","'.getUserName($obj->user_id).'","'.$assigned_to_username.'","'.$obj->request_type.'","'.getStatus($obj->status).'","'.$obj->date_created_clean.'","'.$date_completed.'","'.$turnaround.'"';
						$data.="\n";
				    }
				} //End if campaign
		    }
		}

		header("Pragma: cache");
		header("Content-type:text/csv");
		header("Content-Disposition: attachment; filename=ICC_Export-Tactics(".$today.").csv");
		echo $data;

		exit;
	}


	//Campaigns
	if ($_GET['type'] == 'campaigns') {

		$query = "SELECT
		            req.*,
		            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
		            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
		            status.value AS status_value,
		            request_type.request_type AS request_type,
		            request_type.id AS request_type_id
		        FROM request AS req
		        INNER JOIN status on req.status = status.id
		        INNER JOIN request_type on req.request_type = request_type.id
		        WHERE req.active = 1
		        ORDER BY req.id DESC";

		$result = $mysqli->query($query);
		$row_cnt = $result->num_rows;

		if ($row_cnt > 0) {
		    $data.='"Campaign Name","Tactic Name","GEO","Sector","Subsector","Tactic Owner","Assigned To","Request Type","Status","Campaign Owner","Campaign Start","Campaign End","Tactic Date Created"';
		    $data.="\n";
		    while ($obj = $result->fetch_object()) {

		    	//Get the campaign details, may be able to roll this into a function, but for the time being
		    	$query_cam = "SELECT 
		    					request_campaign.id,
		    					request_campaign.user_id,
		    					request_campaign.name,
		    					request_campaign.geo,
		    					request_campaign.sector,
		    					request_campaign.subsector,
		    					DATE_FORMAT( request_campaign.start_date,  '%m/%d/%Y' ) AS start_date_cam,
		            			DATE_FORMAT( request_campaign.end_date,  '%m/%d/%Y' ) AS end_date_cam
					        FROM request_campaign";
					        if ($obj->campaign_id == NULL) {
					        	$query_cam.=" WHERE request_campaign.request_id = '".$obj->id."'";
					        } else {
					        	$query_cam.=" WHERE request_campaign.id = '".$obj->campaign_id."'";
					        }
					        $query_cam.=" AND request_campaign.active = 1
					        LIMIT 1";

				$result_cam = $mysqli->query($query_cam);
				$row_cnt_cam = $result_cam->num_rows;

				if ($row_cnt_cam > 0) {
					while ($obj_cam = $result_cam->fetch_object()) {
						if(!empty($obj->assigned_to)) { // if assigned_to field is not empty
							$assigned_to_username = getUserName($obj->assigned_to); // fetch username
						} else {
							$assigned_to_username = "-"; // set as blank
						}
						$data.='"'.$obj_cam->name.'","'.getTitle($obj->id, $obj->request_type_id).'","'.$obj_cam->geo.'","'.$obj_cam->sector.'","'.$obj_cam->subsector.'","'.getUserName($obj->user_id).'","'.$assigned_to_username.'","'.$obj->request_type.'","'.getStatus($obj->status).'","'.getUserName($obj_cam->user_id).'","'.$obj_cam->start_date_cam.'","'.$obj_cam->end_date_cam.'","'.$obj->date_created.'"';
						$data.="\n";
				    }
				} //End if campaign
		    }
		}

		header("Pragma: cache");
		header("Content-type:text/csv");
		header("Content-Disposition: attachment; filename=ICC_Export-Campaigns(".$today.").csv");
		echo $data;

		exit;
	}
} else {
	header("Location:/");
	die();
}

?>