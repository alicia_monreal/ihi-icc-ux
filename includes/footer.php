</div>
</div><!--end WRAP-->
<div class="clearfix"></div>
    <footer<?php //if ($loc == 'login') { echo ' class="anchor"'; } ?>>
        <div class="container">
            <ul>
                <li>&copy; Intel Corporation</li>
                <li><a href="http://www.intel.com/content/www/us/en/legal/terms-of-use.html" target="_blank">Terms of Use</a></li>
                <li><a href="http://www.intel.com/content/www/us/en/legal/trademarks.html" target="_blank">*Trademarks</a></li>
                <li><a href="http://www.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" target="_blank">Privacy</a></li>
                <li><a href="http://www.intel.com/content/www/us/en/privacy/intel-cookie-notice.html" target="_blank">Cookies</a></li>
            </ul>
        </div>
    </footer>

    <!-- Editable includes -->
    <?php if (isset($edit) && $edit == true) { ?>
    <script src="js/forms-editable.js"></script>
    <?php } ?>

    <!-- Colorbox -->
    <script src="js/jquery.colorbox.js"></script>

    <!-- Cycle -->
    <?php if ($loc == 'home') { ?>
    <script src="js/jquery.cycle.all.js"></script>
    <?php } ?>

    <!-- Filtrify -->
    <script src="js/highlight.pack.js"></script>
    <script src="js/filtrify.js"></script>

    <!-- Choosen plugin -->
    <script src="js/chosen.jquery.min.js"></script>

    <!-- Calendar -->
    <?php if ($loc == 'training-calendarXXXXXX') { ?>
    <script type="text/javascript" src="js/underscore-min.js"></script>
    <script src="js/calendar.min.js"></script>
    <script type="text/javascript">
        var cal_options = {
            events_source: '/api/events.json.php',
            view: 'month',
            tmpl_path: '/tmpls/',
            tmpl_cache: false,
            modal: "#events-modal",
            modal_type: "ajax",
            modal_title: function (e) { return e.title },
            //day: '2013-03-12',
            onAfterEventsLoad: function(events) {
                if(!events) {
                    return;
                }
                var list = $('#eventlist');
                list.html('');

                $.each(events, function(key, val) {
                    $(document.createElement('li'))
                        .html('<a href="' + val.url + '">' + val.title + '</a>')
                        .appendTo(list);
                });
            },
            onAfterViewLoad: function(view) {
                //$('.intro h1').text(this.getTitle()); //ADD THIS BACK WHEN GOING LIVE
                $('.btn-group button').removeClass('active');
                $('button[data-calendar-view="' + view + '"]').addClass('active');
            },
            classes: {
                months: {
                    general: 'label'
                }
            }
        };
        var calendar = $('#calendar').calendar(cal_options);

        $('.btn-group button[data-calendar-nav]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.navigate($this.data('calendar-nav'));
            });
        });

        $('.btn-group button[data-calendar-view]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.view($this.data('calendar-view'));
            });
        });
    </script>
    <?php } ?>

    <!-- Cookies -->
    <script src="js/jquery.cookie.js"></script>

    <!-- Tables -->
    <script src="js/jquery.dataTables.js"></script>


    <?php if ($loc == 'campaign-requests' || $loc == 'requests' || $loc == 'all-requests' || $loc == 'manage-resources' || $loc == 'calendar' || $loc == 'templates') { ?>
    <!-- Auto Complete -->
    <script src="js/typeahead.bundle.js"></script>

    <!-- Date Picker -->
    <script src="js/date.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
    var date = new Date();
    date.setDate(date.getDate());

    $('#datetimepicker, #datetimepicker_end, #webinar_initial_date_picker, #webinar_reminder_1_date_picker, #webinar_reminder_2_date_picker, #available_on_demand_date_picker').datepicker({
        format: 'mm/dd/yyyy',
        pickTime: false,
        autoclose: true,
        startDate: new Date()
    });

    /*$('#datetimepicker_time').datetimepicker({
        //format: 'mm/dd/yyyy HH:MM PP',
        pickTime: true,
        autoclose: true,
        pick12HourFormat: true,
        pickSeconds: false,
        startDate: new Date()
    });
    $('#webinar_date').click(function () {
        $('#datetimepicker_time').datetimepicker('show');
    });*/

    $('#datetimepicker_time, #datetimepicker_time_end').datetimepicker({
        //format: 'mm/dd/yyyy HH:MM PP',
        format: "mm/dd/yyyy HH:ii P",
        showMeridian: true,
        autoclose: true,
        pickerPosition: 'bottom-left',
        startDate: new Date()
    });

    $('#publish_datepicker').datetimepicker({
        format: 'mm/dd/yyyy',
        pickTime: false,
        autoclose: true
    });

    $("#start_date").change(function() {
        var d_val = $(this).val();

        //Invitation
        var d_i = new Date(d_val);
        d_i.setDate(d_i.getDate()-14);

        //Reminder 1
        var d_r1 = new Date(d_val);
        d_r1.setDate(d_r1.getDate()-7);

        //Reminder 2
        var d_r2 = new Date(d_val);
        d_r2.setDate(d_r2.getDate()-1);

        $('#webinar_initial_date').val(d_i.toString('MM/dd/yyyy')); //toString uses date.js
        $('#webinar_reminder_1_date').val(d_r1.toString('MM/dd/yyyy')); //toString uses date.js
        $('#webinar_reminder_2_date').val(d_r2.toString('MM/dd/yyyy')); //toString uses date.js
    });
    </script>

    <!-- File Uploaded -->
    <script src="js/bootstrap-fileupload.min.js"></script>
    <script src="js/fileuploader/jquery.ui.widget.js"></script>
    <script src="js/fileuploader/jquery.iframe-transport.js"></script>
    <script src="js/fileuploader/jquery.fileupload.js"></script>

    <script>
    $(function () {
        'use strict';
        var url = 'server/php/';
        var userId = $(".session-user-id").val();
        <?php if (isset($c_type)) { //This means it's a form ?>

        $('#fileupload_presentation').fileupload({
            add: function(e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /(\.|\/)(ppt|pptx|pptm|potx|potm|ppam|ppsx|ppsm|sldx|sldm|thmx)$/i;
                //if(data.originalFiles.length > 1) {
                //    uploadErrors.push('You may only upload one Presentation');
                //}
                //This isn't working, pulling the following for extension: application/vnd.openxmlformats-officedocument.presentationml.presentation
                //if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                //    uploadErrors.push('File must be a PowerPoint document');
                //}
                if(data.originalFiles[0]['size'] > 26214400) {
                    uploadErrors.push('Filesize must be under 25MB');
                }
                if(uploadErrors.length > 0) {
                    //alert(uploadErrors.join("\n"));
                    uploadErrors.join("\n")
                    var errorz = $('<p/>').text(uploadErrors);
                    $('#errors-presentation').empty();
                    $(errorz).appendTo('#errors-presentation');
                    uploadErrors = [];
                } else {
                    data.submit();
                }
            },
            url: url,
            dataType: 'json',
            //maxNumberOfFiles: 1,
            //acceptFileTypes: /(\.|\/)(ppt|pptx|pptm|potx|potm|ppam|ppsx|ppsm|sldx|sldm|thmx)$/i,
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-presentation').empty();
                    $('input[name=files_presentation]').remove();
                    $('#presentation span.fileupload-new').text('Change File');
                    // $('<p/>').text(file.name).appendTo('#shown-presentation');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a><a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-presentation');

                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_presentation" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                    //$("#fileupload_presentation").attr("disabled", "disabled");
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-presentation').show();
                $('#progress-presentation .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-presentation');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });

        $('#fileupload_media').fileupload({
            add: function(e, data) {
                var uploadErrors = [];
                if(data.originalFiles[0]['size'] > 26214400) {
                    uploadErrors.push('Filesize must be under 25MB');
                    //alert(data.originalFiles[0]['size']);
                }
                if(uploadErrors.length > 0) {
                    uploadErrors.join("\n")
                    var errorz = $('<p/>').text(uploadErrors);
                    $('#errors-media').empty();
                    $(errorz).appendTo('#errors-media');
                    uploadErrors = [];
                } else {
                    data.submit();
                }
            },
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    //$('#media span.fileupload-new').text('Change File(s)');
                    // $('<p/>').text(file.name).appendTo('#shown-media');

                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-media');

                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_media[]" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-media').show();
                $('#progress-media .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-media');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });



        $('#fileupload_survey').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-survey').empty();
                    $('input[name=files_survey]').remove();
                    $('#survey span.fileupload-new').text('Change File');
                    // $('<p/>').text(file.name).appendTo('#shown-survey');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-survey');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_survey" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
                //Check the Survey checkbox
                $('#surveycheck').prop('checked', true);

                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-survey').show();
                $('#progress-survey .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-survey');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });

        <?php
        //Speaker photos loop
        for ($i_js = 1; $i_js <= 10; $i_js++) { ?>
        $('#fileupload_photo_<?php echo $i_js; ?>').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-photo_<?php echo $i_js; ?>').empty();
                    $('input[name=files_photo_<?php echo $i_js; ?>]').remove();
                    $('#photo_<?php echo $i_js; ?> span.fileupload-new').text('Change File');
                    // $('<p/>').text(file.name).appendTo('#shown-photo_<?php echo $i_js; ?>');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-photo_<?php echo $i_js; ?>');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_photo_<?php echo $i_js; ?>" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-photo_<?php echo $i_js; ?>').show();
                $('#progress-photo_<?php echo $i_js; ?> .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-photo_<?php echo $i_js; ?>');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });
        <?php } ?>

        $('#fileupload_resource').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    // $('<p/>').text(file.name).appendTo('#shown-resource');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-resource');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_resource[]" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
                //Check the Resource checkbox
                $('#resourcelist').prop('checked', true);

                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-resource').show();
                $('#progress-resource .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-resource');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });

        $('#fileupload_contacts').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {

                $.each(data.result.files, function (index, file) {
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-contacts');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_contacts[]" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-contacts').show();
                $('#progress-contacts .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-contacts');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });



        $('#fileupload_proof').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    // $('<p/>').text(file.name).appendTo('#shown-proof');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-proof');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_proof" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-proof').show();
                $('#progress-proof .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-proof');
                });
                //Now we can submit the form
                $('a#submit-request-form').show();
            }
        });

        $('#fileupload_briefs').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    // $('<p/>').text(file.name).appendTo('#shown-briefs');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/'+ (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-briefs');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_briefs[]" value="'+file.name+'" />');
                    //Now we can submit the form
                    $('a#submit-request-form').removeClass('submit-disabled');
                    $('a#submit-request-form').text('Next');
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-briefs').show();
                $('#progress-briefs .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                //$('a#submit-request-form').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-briefs');
                });
            }
        });

        $('#fileupload_campaignbrief').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    // $('<p/>').text(file.name).appendTo('#shown-campaignbrief');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-campaignbrief');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_campaignbrief[]" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-campaignbrief').show();
                $('#progress-campaignbrief .bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-campaignbrief');
                });
            }
        });

        $('#fileupload_customassets').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-customassets').empty();
                    // $('<p/>').text(file.name).appendTo('#shown-customassets');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-customassets');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_customassets" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-customassets').show();
                $('#progress-customassets .bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-customassets');
                });
            }
        });

        //Email header image
        $('#fileupload_emailfeaturedimage').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-emailfeaturedimage').empty();
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a><a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-emailfeaturedimage');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_emailfeaturedimage" value="'+file.name+'" />');
                    $("input[name=email-featured-image][value='custom']").prop('checked', true);
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-emailfeaturedimage').show();
                $('#progress-emailfeaturedimage .bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-emailfeaturedimage');
                });
            }
        });
        $('#fileupload_registrationpageimage').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-registrationpageimage').empty();
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-registrationpageimage');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_registrationpageimage" value="'+file.name+'" />');
                    $("input[name=registration-page-image][value='custom']").prop('checked', true);
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-registrationpageimage').show();
                $('#progress-registrationpageimage .bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-registrationpageimage');
                });
            }
        });
        $('#fileupload_confirmationpageimage').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-confirmationpageimage').empty();
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-confirmationpageimage');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_confirmationpageimage" value="'+file.name+'" />');
                    $("input[name=confirmation-page-image][value='custom']").prop('checked', true);
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-confirmationpageimage').show();
                $('#progress-confirmationpageimage .bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-confirmationpageimage');
                });
            }
        });
        $('#fileupload_footerimage').fileupload({
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-footerimage').empty();
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-footerimage');
                    $('#content-update-form, #content-update-form-inline').append('<input type="hidden" name="files_footerimage" value="'+file.name+'" />');
                    $('.delete-file').on('click',deleteCallback);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-footerimage').show();
                $('#progress-footerimage .bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-footerimage');
                });
            }
        });


        <?php } //END if c_type isset, meaning it's a form ?>


        <?php
        //RESOURCES FILE UPLOADS//////////////////////////
        if ($loc == 'manage-resources') { ?>

        $('#fileupload_resource').fileupload({
            //url: 'files/resources/', //not working
            url: url,
            dataType: 'json',
            processalways: function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append(file.error);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#shown-resource').empty();
                    $('input[name=files_resource]').remove();
                    $('#resource span.fileupload-new').text('Change File');
                    // $('<p/>').text(file.name).appendTo('#shown-resource');
                    var filebox = '<div name="' + (file.name) + '">';
                    filebox += '<p><a class="btn btn-danger delete-file" data="/server/php/files/'+userId+'/' + (file.name) + '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="server/php/files/'+userId+'/'+file.name+'"> ' + file.name + '</a></p>';
                    filebox += '</div>';
                    $(filebox).appendTo('#shown-resource');
                    $('#resource-update-form').append('<input type="hidden" name="files_resource" value="'+file.name+'" />');
                    $('#link').val("/server/php/files/<?php echo $_SESSION['user_id']; ?>/"+file.name);
                    //$('#link').prop('disabled', true);
                    //$('input:radio[name=internal]').prop('disabled', true);
                    $('input:radio[name=internal]')[1].checked = true;
                    $('.delete-file').on('click',deleteCallback);
                });
                //Now we can submit the form
                $('.submit-btn').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress-resource').show();
                $('#progress-resource .bar').css(
                    'width',
                    progress + '%'
                );
                //Can't submit form till done
                $('.submit-btn').hide();
            },
            fail: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var error = $('<span/>').text(file.error);
                    $(error).appendTo('#shown-resource');
                });
                //Now we can submit the form
                $('.submit-btn').show();
            }
        });

        <?php } ?>

        //Delete for fileuploads
        function deleteCallback(e){
            var fileName = $(this).attr('data');
            e.preventDefault();
            $.ajax({
                url: '/server/php/?file=' + escape(fileName),
                data: {type:'REMOVE'},
                type: 'POST',
                filename: fileName,
                success: function(result) {
                    var oHidden = $('input[value="'+this.filename + '"]');
                    var filBox  =$('div[name="'+this.filename + '"]');
                    var oBar = filBox.parent().siblings().find('.bar');

                    oHidden && oHidden.remove();
                    filBox && filBox.remove();
                    oBar && oBar.parent().hide();
                    $('.bar').closest().css('width','0%');
                    return true;
                }
            });
        }

        //Register Delete
        $('.delete-file').on('click',deleteCallback);

        //Message CC BOX
        $('#message-recipients').attr("data-placeholder","Choose additional recipients").chosen();


        //Send Message hanlder on request detail
        $('#request-detail-send-message').on('click',function(event){
            var params = $('#content-update-form-inline').serialize();
            var recipients = ($('#message-recipients').val());

            var cc_list = ($('#message-cc').attr('data-list'));


            var recipients_str = '';
            if(recipients !== null){
                for(var i=0; i<recipients.length; i++){
                    if(i === recipients.length-1)
                        recipients_str += recipients[i];
                    else
                        recipients_str += recipients[i] + ',';
                }
            }

            //Concat CC list
            if(cc_list.length > 0){
                cc_list = JSON.parse(cc_list);
                for(var i=0; i<cc_list.length; i++){
                    if(recipients === null && i===0)
                        recipients_str +=cc_list[i];
                    else
                        recipients_str +=  ',' +  cc_list[i];
                }
            }


            params += '&recipients=' + recipients + '&body=' + $('#message-body').val();
            $.ajax({
                url: 'includes/_messages.php' ,
                data: params,
                type: 'POST',
                success: function(result) {
                    result  = JSON.parse(result);
                    var msgBody = $('#message-body');
                    var html = '';
                    html+= '<ul class="notes"><li>' + msgBody.val() + '<br /><span class="user-name">' + result.date + ' (' + result.fname + ' ' + result.lname + ')</span></li></ul><hr>';
                    $('#message-body').val('');
                    $('#message-recipients').val('');

                    $('.message-thread').prepend(html).slideDown();
                    return false;

                }
            });
            event.preventDefault();

        });


        $("input[name=speaker_option]").change(function() {
            var test = $(this).val();
            if (test == 'Detailed') {
                $('.bios-extra').show();
            } else {
                $('.bios-extra').hide();
            }
        });

        $('#addSpeaker').on('click', function() { //WAS live LIKE BELOW
            var numOfVisibleRows = $('.speaker:visible').length;
            var nextNum = parseInt(numOfVisibleRows) + 1;
            var openNext = '.speaker_'+nextNum;
            $(openNext).show();
            return false;
        });

        var bios = $('#eternal-assets');
        var i = $('#eternal-assets p').size() + 1;

        $('#addInput').on('click', function() {
            $('<p style="clear:both;"><input type="text" name="external_title['+i+']" value="" placeholder="Title" class="pull-left"><input type="text" name="external_url['+i+']" value="" placeholder="URL" class="pull-left" style="margin-left:12px;"> <a href="" id="remScnt" class="pull-left" style="margin-left:12px;margin-top:5px;">Remove</a></p>').appendTo(bios);
            i++;
            return false;
        });

        $('#remScnt').on('click', function() {
            $(this).parents('p').remove();
            //i--;
            return false;
        });
    });
    </script>
    <?php } ?>

    <!-- Main -->
    <script src="js/jquery.validate.min.js"></script>
    <!--<script src="js/html5placeholder.jquery.js?ver=<?php echo rand(); ?>"></script> doesn't work with modals -->
    <script src="js/jquery.placeholder.js?ver=<?php echo rand(); ?>"></script>
    <?php
    if ((isset($step) && $step == 3) || ($loc == 'templates')) { //Only want to pull this in on the template step
        echo '<script src="js/templates.js?ver='.rand().'"></script>';
    }

    global $campaign_array_popup,$campaign_array;
    ?>

    <?php if ($loc == 'calendar') { // if calendar page ?> 
        <script>
            var campaign_popup = <?php echo json_encode($campaign_array_popup); ?>;
        </script>
    <?php } else { // rest of the pages ?>
        <script>
            var campaign_popup = <?php echo json_encode($campaign_array); ?>;
        </script>
    <?php } ?>

    <script src="js/main.js?ver=<?php echo rand(); ?>"></script>
    <script src="js/editor/lib/util/json2.js"></script>

    <?php if ($loc == 'home') { ?>
        <script type="text/javascript">
        $(document).ready(function() {

            //Slideshow
            $('#slideshow').after('<div id="nav" class="slideshow-nav">').cycle({
                speed:  400,
                timeout: 13000,
                cleartype: true,
                cleartypeNoBg: true,
                filter:'',
                pager:  '#nav'
            });

        });
        </script>
    <?php } ?>

    <?php if ($loc == 'campaign-requests' || $loc == 'calendar') { ?>
        <script type="text/javascript">
        $(document).ready(function() {
          var substringMatcher = function(strs) {
              return function findMatches(q, cb) {
                var matches, substringRegex;
                matches = [];
                substrRegex = new RegExp(q, 'i');

                $.each(strs, function(i, str) {
                  if (substrRegex.test(str)) {
                    matches.push({ value: str });
                  }
                });

                cb(matches);
              };
            };

            //CAMPAIGNS
            var campaigns = <?php echo json_encode($campaign_array); ?>;
            
            $('#campaign_search').typeahead({
              hint: true,
              highlight: true,
              minLength: 1
            },
            {
              name: 'campaigns',
              displayKey: 'value',
              source: substringMatcher(campaigns)
            });

            $('#request_search_campaign').typeahead({
              hint: true,
              highlight: true,
              minLength: 1
            },
            {
              name: 'request_search_campaign',
              displayKey: 'value',
              source: substringMatcher(campaigns)
            });

            $('input:radio[name=request_type]').click(function () {

                checkval_reqtype = $('input:radio[name=request_type]:checked').val();

                if (checkval_reqtype != 8) { //No campaigns

                    //REQUESTS
                    switch (checkval_reqtype) {
                        case '1':
                            request_vals = <?php echo json_encode(getRequestNameArray(1)); ?>;
                            break;
                        case '2':
                            request_vals = <?php echo json_encode(getRequestNameArray(2)); ?>;
                            break;
                        case '4':
                            request_vals = <?php echo json_encode(getRequestNameArray(4)); ?>;
                            break;
                        case '5':
                            request_vals = <?php echo json_encode(getRequestNameArray(5)); ?>;
                            break;
                        case '6':
                            request_vals = <?php echo json_encode(getRequestNameArray(6)); ?>;
                            break;
                        case '7':
                            request_vals = <?php echo json_encode(getRequestNameArray(7)); ?>;
                            break;
                        case '9':
                            request_vals = <?php echo json_encode(getRequestNameArray(9)); ?>;
                            break;
                    }

                    var requests = request_vals;

                    $('#request_search').typeahead({
                      hint: true,
                      highlight: true,
                      minLength: 1
                    },
                    {
                      //name: 'requests',
                      displayKey: 'value',
                      source: substringMatcher(requests)
                    });

                }
            });
        });
        </script>

    <?php } ?>
    <!--
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-17654109-33', 'intelonlinesales.com');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    -->

</body>
</html>
