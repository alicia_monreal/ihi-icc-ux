<?php
session_start();
require_once('validate.php');
include('_globals.php');

if (!empty($_POST) && !empty($_POST['action']) && $_POST['action'] == 'update') {

    //Values
    $user_id = $_POST['user_id'];
    $new_user_id = $_POST['new_user_id'];
    $user_email = $_POST['user_email'];
    $admin_id = $_POST['admin_id'];
    $request_id = $_POST['request_id'];
    $request_type_id = $_POST['request_type_id'];
    $status = $_POST['status_update'];
    $assigned_to = $_POST['assigned_to'];

    // additional recipients and additional owners
    $additional_notifiers = $_POST['additional-notifiers'];
    $add_recipients = $_POST['add-recipients'];
    $additional_rights_users = $_POST['additional-rights-users'];
    $add_owners = $_POST['add-owners'];

    $addition_recipients = $addition_owners = array();
    
    foreach ($additional_notifiers as $value) {
        if($value != "")
            array_push($addition_recipients, $value);   
    }
    foreach($add_recipients as $value) {
        if($value != "")
            array_push($addition_recipients, $value);   
    }

    foreach ($additional_rights_users as $value) {
        if($value != "")
            array_push($addition_owners, $value);   
    }
    foreach($add_owners as $value) {
        if($value != "")
            array_push($addition_owners, $value);   
    }

    if(!empty($addition_owners) || !empty($addition_recipients)) { // if one of them is not empty then update
         $addition_owners_db = $mysqli->real_escape_string(serialize(array_unique($addition_owners))); //serialize unique array
         $addition_recipients_db = $mysqli->real_escape_string(serialize(array_unique($addition_recipients))); // serialize unique array

         // update query for owners and recipients
        $query_updating = "UPDATE request SET id_last_update = '".$_SESSION['user_id']."', date_updated = NOW(), additional_recipients = '".$addition_recipients_db."', additional_owners = '".$addition_owners_db."' WHERE id = ".$request_id."";
        $mysqli->query($query_updating);
        if($mysqli->affected_rows) { // if effected rows then insert into notes
            $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'additional_recipients, additional_owners', 'Request Table: Fields Updated', NOW(), '1')";
            $result_notes = $mysqli->query($query_notes);
        }
    }

    //$notes = $mysqli->real_escape_string($_POST['notes_update']);
    $the_title = getTitle($request_id, $request_type_id);
    $tactic_type = getTacticType($request_type_id);

    //What table are we editing
    $query_table = getTacticTable($request_type_id);

    /////////////////////////////////////////////////////////////////
    //Status changes
    //Only send an email if status has been updated
    $no_status_change = false;
    $query_chk = "SELECT status FROM request WHERE id = ".$request_id." LIMIT 1";
    $result_chk = $mysqli->query($query_chk);
    while ($obj_chk = $result_chk->fetch_object()) {
        if ($obj_chk->status == $status) { //Status isn't being updated, just something else. Which means no email and no Date Completed update.
            $no_status_change = true;
        }
    }

    $query = "UPDATE request SET status = '".$status."', id_last_update = '".$_SESSION['user_id']."', date_updated = NOW()";
    if ($status == 4 && $no_status_change == false) {
        $query.=", date_completed = NOW()";

        //Also add a note
        $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', NULL, 'Request completed', NOW(), '1')";
        $mysqli->query($query_note);
    }
    $query.=" WHERE id = ".$request_id."";
    $result = $mysqli->query($query);

    //Need to add a note of the status change
    if ($no_status_change == false && $status != 4) {
        $new_status = getStatus($status);

        $query_note_status = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', NULL, 'Status update: ".$new_status."', NOW(), '1')";
        $mysqli->query($query_note_status);
    }



    /////////////////////////////////////////////////////////////////
    //Assigned to changes
    //Only send an email if assigned to has been updated
    $no_assigned_change = false;
    $query_ascheck = "SELECT assigned_to FROM request WHERE id = ".$request_id." LIMIT 1";
    $result_ascheck = $mysqli->query($query_ascheck);
    while ($obj_ascheck = $result_ascheck->fetch_object()) {
        if ($obj_ascheck->assigned_to == $assigned_to) { //Assigned to isn't being updated, just something else. Which means no email.
            $no_assigned_change = true;
        }
    }

    if ($assigned_to != '') {
        $query_assigned = "UPDATE request SET assigned_to = '".$assigned_to."', id_last_update = '".$_SESSION['user_id']."', date_updated = NOW() WHERE id = ".$request_id."";
        $result_assigned = $mysqli->query($query_assigned);
    }
    //If there's an assignor with an email address, set it here to CC on later emails
    $assigned_to_email = getEmailAddress($assigned_to);
    if ($assigned_to_email != '') { $mail2->AddCC($assigned_to_email); }

    // additional recipients added in cc
    if(!empty($addition_recipients) && is_array($addition_recipients)) {
        foreach ($addition_recipients as $recipients) {
            if($user_email == getEmailAddress($recipients) || $assigned_to_email == getEmailAddress($recipients)) // making sure recipients is not the same as assigned_to or user_email
                continue;

            $mail3->AddCC(getEmailAddress($recipients), getUserName($recipients));  // add recipients to the CC 
        }
    }
    /////////////////////////////////////////////////////////////////
    //Change requestor
    //Only send an email if requestor has been updated
    $no_requestor_change = false;
    $query_reqcheck = "SELECT user_id FROM request WHERE user_id = ".$new_user_id." AND id = ".$request_id." LIMIT 1";
    $result_reqcheck = $mysqli->query($query_reqcheck);
    while ($obj_reqcheck = $result_reqcheck->fetch_object()) {
        if ($obj_reqcheck->user_id == $new_user_id) { //Requestor isn't being updated, just something else. Which means no update.
            $no_requestor_change = true;
        }
    }

    if ($no_requestor_change == false) {
        $query_requestor = "UPDATE request SET user_id = '".$new_user_id."', id_last_update = '".$_SESSION['user_id']."', date_updated = NOW() WHERE id = ".$request_id."";
        $result_requestor = $mysqli->query($query_requestor);

        $query_requestor2 = "UPDATE $query_table SET user_id = '".$new_user_id."' WHERE request_id = ".$request_id."";
        $result_requestor2 = $mysqli->query($query_requestor2);
    }


    /////////////////////////////////////////////////////////////////
    //Assets
    //Let's first remove the assets they requested (we're doing this by marking in-active)
    if (!empty($_POST['delete_asset'])) {
        $query_delete = "UPDATE assets SET active = CASE";
        foreach ($_POST['delete_asset'] as $key => $value) {
            $query_delete.=" WHEN id = ".$value." THEN 0";
        }
        $query_delete.=" ELSE active END;";
        $result_delete = $mysqli->query($query_delete);
    }

    //Now let's see if there are any new assets
    $files_customassets = $_POST['files_customassets']; //files
    $files_contacts = array();
    foreach ($_POST['files_contacts'] as $val_contacts) {
        array_push($files_contacts, $val_contacts);
    }
    $files_proof = $_POST['files_proof']; //files
    $files_presentation = $_POST['files_presentation']; //files
    $media_array = array();
    foreach ($_POST['files_media'] as $val_media) {
        array_push($media_array, $val_media);
    }
    $files_media = $media_array; //files
    $campaignbrief_array = array();
    foreach ($_POST['files_campaignbrief'] as $val_campaignbrief) {
        array_push($campaignbrief_array, $val_campaignbrief);
    }
    $files_campaignbrief = $campaignbrief_array; //files
    $files_survey = $_POST['files_survey']; //files
    $resource_array = array();
    foreach ($_POST['files_resource'] as $val_resource) {
        array_push($resource_array, $val_resource);
    }
    $files_resource = $resource_array; //files

    /* Moving to template system
    $files_emailfeaturedimage = $_POST['files_emailfeaturedimage']; //files
    $files_registrationpageimage = $_POST['files_registrationpageimage']; //files
    $files_confirmationpageimage = $_POST['files_confirmationpageimage']; //files
    $files_footerimage = $_POST['files_footerimage']; //files
    */

    //Custom assets
    if (!empty($files_customassets)) {
        $query_customassets = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', '".$files_customassets."', '/server/php/files/".$_SESSION['user_id']."/".$files_customassets."', 'Custom Assets', NOW(), '1')";
        $mysqli->query($query_customassets);
    }

    //Contacts
    if (!empty($files_contacts)) {
        if ($request_type_id == 9) { $file_type_upload = 'Leads'; } else { $file_type_upload = 'Contacts'; }
        uploadAssets($files_contacts, $file_type_upload, $request_id, $_SESSION['user_id']);
    }

    //Proof
    if (!empty($files_proof)) {
        $query_proof = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', '".$files_proof."', '/server/php/files/".$_SESSION['user_id']."/".$files_proof."', 'Proof', NOW(), '1')";
        $mysqli->query($query_proof);
    }

    //Presentation
    if (!empty($files_presentation)) {
        $query_presentation = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', '".$files_presentation."', '/server/php/files/".$_SESSION['user_id']."/".$files_presentation."', 'Presentation', NOW(), '1')";
        $mysqli->query($query_presentation);
    }

    //Media
    if (!empty($files_media)) {
        $media_number = count($files_media);
        $media_counter = 1;

        $query_media = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
        
        foreach ($files_media as $media) {
            $query_media.="(NULL, '".$request_id."', '".$user_id."', '".$media."', '/server/php/files/".$_SESSION['user_id']."/".$media."', 'Media', NOW(), '1')";
            $media_counter++;
            if ($media_counter <= $media_number) {
                $query_media.=", ";
            }
        }
        $query_media.=";";
        $mysqli->query($query_media);
    }

    //Campaign Brief (Multi-Webinar)
    if (!empty($files_campaignbrief)) {
        $campaignbrief_number = count($files_campaignbrief);
        $campaignbrief_counter = 1;

        $query_campaignbrief = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
        
        foreach ($files_campaignbrief as $campaignbrief) {
            $query_campaignbrief.="(NULL, '".$request_id."', '".$user_id."', '".$campaignbrief."', '/server/php/files/".$_SESSION['user_id']."/".$campaignbrief."', 'Briefs', NOW(), '1')";
            $campaignbrief_counter++;
            if ($campaignbrief_counter <= $campaignbrief_number) {
                $query_campaignbrief.=", ";
            }
        }
        $query_campaignbrief.=";";
        $mysqli->query($query_campaignbrief);
    }

    //Survey
    if (!empty($files_survey)) {
        $query_survey = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', '".$files_survey."', '/server/php/files/".$_SESSION['user_id']."/".$files_survey."', 'Survey', NOW(), '1')";
        $mysqli->query($query_survey);
    }

    //Resource
    if (!empty($files_resource)) {
        $resource_number = count($files_resource);
        $resource_counter = 1;

        $query_resource = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

        foreach ($files_resource as $resource) {
            $query_resource.="(NULL, '".$request_id."', '".$_SESSION['user_id']."', '".$resource."', '/server/php/files/".$_SESSION['user_id']."/".$resource."', 'Resource', NOW(), '1')";
            $resource_counter++;
            if ($resource_counter <= $resource_number) {
                $query_resource.=", ";
            }
        }
        $query_resource.=";";
        $mysqli->query($query_resource);
    }

    /* Moving to template system
    //Email Featured Image
    if (!empty($files_emailfeaturedimage)) {
        $query_emailfeaturedimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_emailfeaturedimage."', '/server/php/files/".$user_id."/".$files_emailfeaturedimage."', 'Email Featured Image', NOW(), '1')";
        $mysqli->query($query_emailfeaturedimage);
    }

    //Registration Page Image
    if (!empty($files_registrationpageimage)) {
        $query_registrationpageimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_registrationpageimage."', '/server/php/files/".$user_id."/".$files_registrationpageimage."', 'Registration Page Image', NOW(), '1')";
        $mysqli->query($query_registrationpageimage);
    }

    //Confirmation Page Image
    if (!empty($files_confirmationpageimage)) {
        $query_confirmationpageimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_confirmationpageimage."', '/server/php/files/".$user_id."/".$files_confirmationpageimage."', 'Confirmation Page Image', NOW(), '1')";
        $mysqli->query($query_confirmationpageimage);
    }

    //Footer Logo
    if (!empty($files_footerimage)) {
        $query_footerimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_footerimage."', '/server/php/files/".$user_id."/".$files_footerimage."', 'Footer Logo', NOW(), '1')";
        $mysqli->query($query_footerimage);
    }
    */


    /////////////////////////////////////////////////////////////////
    //TEMPLATES
    //And now let's update templates. Really only doing this for a few, so let's block the rest.
    if ($request_type_id == 1 || $request_type_id == 6 || $request_type_id == 7) {

        //Webinar and Email
        if ($request_type_id == 1 || $request_type_id == 6) {
            if ($_POST['email-banner'] != '') { updateField('email_banner', $_POST['email-banner'], $request_id, $request_type_id); }
            if ($_POST['email-body'] != '') { updateField('email_body', $_POST['email-body'], $request_id, $request_type_id); }
            if ($_POST['email-footer'] != '') { updateField('email_footer', $_POST['email-footer'], $request_id, $request_type_id); }

            $emailmodules_array = array();
            foreach($_POST['email-modules'] as $val_emailmodules) {
                array_push($emailmodules_array, $val_emailmodules);
            }
            if ($emailmodules_array != '') { updateField('email_modules', $emailmodules_array, $request_id, $request_type_id); }
        }

        //Webinar and Landing Page
        if ($request_type_id == 1 || $request_type_id == 7) {
            if ($_POST['page-banner'] != '') { updateField('page_banner', $_POST['page-banner'], $request_id, $request_type_id); }
            if ($_POST['page-body'] != '') { updateField('page_body', $_POST['page-body'], $request_id, $request_type_id); }
            if ($_POST['page-footer'] != '') { updateField('page_footer', $_POST['page-footer'], $request_id, $request_type_id); }

            $pagemodules_array = array();
            foreach($_POST['page-modules'] as $val_pagemodules) {
                array_push($pagemodules_array, $val_pagemodules);
            }
            if ($pagemodules_array != '') { updateField('page_modules', $pagemodules_array, $request_id, $request_type_id); }
        }


        //Files
        //Email Banner Image
        $files_emailbannerimage = '';
        if (isset($_POST['files_emailbannerimage']) && $_POST['files_emailbannerimage'] != '') {
            $files_emailbannerimage = $_POST['files_emailbannerimage']; //files
        }
        if (isset($_POST['email-banner-image']) && $_POST['email-banner-image'] != 'custom') {
            $files_emailbannerimage = $_POST['email-banner-image']; //Stock image
            $email_banner_image = $_POST['email-banner-image'];
        }
        if (!empty($files_emailbannerimage)) {
            $path_emailbannerimage = "/server/php/files/".$user_id;
            if (isset($_POST['email-banner-image']) && $_POST['email-banner-image'] != 'custom') {
                $path_emailbannerimage = '/img/templates/default-images/previews';
            }
            deleteAsset('Email Banner Image', $request_id); //Delete old one first
            $query_emailbannerimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_emailbannerimage."', '".$path_emailbannerimage."/".$files_emailbannerimage."', 'Email Banner Image', NOW(), '1')";
            $mysqli->query($query_emailbannerimage);
        }

        //Email Body Image

        $files_emailbodyimage = '';
        if (isset($_POST['files_emailbodyimage']) && $_POST['files_emailbodyimage'] != '') {
            $files_emailbodyimage = $_POST['files_emailbodyimage']; //files
        }
        if (isset($_POST['email-body-image']) && $_POST['email-body-image'] != 'custom') {
            $files_emailbodyimage = $_POST['email-body-image']; //Stock image
            $email_body_image = $_POST['email-body-image'];
        }
        if (!empty($files_emailbodyimage)) {
            $path_emailbodyimage = "/server/php/files/".$user_id;
            if (isset($_POST['email-body-image']) && $_POST['email-body-image'] != 'custom') {
                $path_emailbodyimage = '/img/templates/default-images/previews';
            }
            deleteAsset('Email Body Image', $request_id); //Delete old one first
            $query_emailbodyimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_emailbodyimage."', '".$path_emailbodyimage."/".$files_emailbodyimage."', 'Email Body Image', NOW(), '1')";
            $mysqli->query($query_emailbodyimage);
        }

        //Page Banner Image
        $files_pagebannerimage = '';
        if (isset($_POST['files_pagebannerimage']) && $_POST['files_pagebannerimage'] != '') {
            $files_pagebannerimage = $_POST['files_pagebannerimage']; //files
        }
        if (isset($_POST['page-banner-image']) && $_POST['page-banner-image'] != 'custom') {
            $files_pagebannerimage = $_POST['page-banner-image']; //Stock image
            $page_banner_image = $_POST['page-banner-image'];
        }
        if (!empty($files_pagebannerimage)) {
            $path_pagebannerimage = "/server/php/files/".$user_id;
            if (isset($_POST['page-banner-image']) && $_POST['page-banner-image'] != 'custom') {
                $path_pagebannerimage = '/img/templates/default-images/previews';
            }
            deleteAsset('Registration Page Banner Image', $request_id); //Delete old one first
            $query_pagebannerimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_pagebannerimage."', '".$path_pagebannerimage."/".$files_pagebannerimage."', 'Registration Page Banner Image', NOW(), '1')";
            $mysqli->query($query_pagebannerimage);
        }

        //Speaker Module
        if (isset($_POST['files_pagespeakerimage'])) {
            $files_pagespeakerimage = $_POST['files_pagespeakerimage']; //files
            if (!empty($files_pagespeakerimage)) {
                deleteAsset('Module: Speaker Image', $request_id); //Delete old one first
                $query_pagespeakerimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_pagespeakerimage."', '/server/php/files/".$user_id."/".$files_pagespeakerimage."', 'Module: Speaker Image', NOW(), '1')";
                $mysqli->query($query_pagespeakerimage);
            }
        }

        //Sponsor Module
        if (isset($_POST['files_pagesponsorimage'])) {
            $files_pagesponsorimage = $_POST['files_pagesponsorimage']; //files
            if (!empty($files_pagesponsorimage)) {
                deleteAsset('Module: Sponsor Image', $request_id); //Delete old one first
                $query_pagesponsorimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_pagesponsorimage."', '/server/php/files/".$user_id."/".$files_pagesponsorimage."', 'Module: Sponsor Image', NOW(), '1')";
                $mysqli->query($query_pagesponsorimage);
            }
        }

        //Location Module
        if (isset($_POST['files_pagelocationimage'])) {
            $files_pagelocationimage = $_POST['files_pagelocationimage']; //files
            if (!empty($files_pagelocationimage)) {
                deleteAsset('Module: Location Image', $request_id); //Delete old one first
                $query_pagelocationimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_pagelocationimage."', '/server/php/files/".$user_id."/".$files_pagelocationimage."', 'Module: Location Image', NOW(), '1')";
                $mysqli->query($query_pagelocationimage);
            }
        }

        //Event Module
        if (isset($_POST['files_pageeventimage'])) {
            $files_pageeventimage = $_POST['files_pageeventimage']; //files
            if (!empty($files_pageeventimage)) {
                deleteAsset('Module: Event Image', $request_id); //Delete old one first
                $query_pageeventimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_pageeventimage."', '/server/php/files/".$user_id."/".$files_pageeventimage."', 'Module: Event Image', NOW(), '1')";
                $mysqli->query($query_pageeventimage);
            }
        }

        //Email Speaker Module
        if (isset($_POST['files_emailspeakerimage'])) {
            $files_emailspeakerimage = $_POST['files_emailspeakerimage']; //files
            if (!empty($files_emailspeakerimage)) {
                deleteAsset('Module: Email Speaker Image', $request_id); //Delete old one first
                $query_emailspeakerimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_emailspeakerimage."', '/server/php/files/".$user_id."/".$files_emailspeakerimage."', 'Module: Email Speaker Image', NOW(), '1')";
                $mysqli->query($query_emailspeakerimage);
            }
        }

        //Email Location Image
        if (isset($_POST['files_emaillocationimage'])) {
            $files_emaillocationimage = $_POST['files_emaillocationimage']; //files
            if (!empty($files_emaillocationimage)) {
                deleteAsset('Module: Email Location Image', $request_id); //Delete old one first
                $query_emaillocationimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_emaillocationimage."', '/server/php/files/".$user_id."/".$files_emaillocationimage."', 'Module: Email Location Image', NOW(), '1')";
                $mysqli->query($query_emaillocationimage);
            }
        }

        //Page Content Alt Image
        if (isset($_POST['files_pagecontentaltimage'])) {
            $files_pagecontentaltimage = $_POST['files_pagecontentaltimage']; //files
            if (!empty($files_pagecontentaltimage)) {
                deleteAsset('Module: Page Content Alt Image', $request_id); //Delete old one first
                $query_pagecontentaltimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_pagecontentaltimage."', '/server/php/files/".$user_id."/".$files_pagecontentaltimage."', 'Module: Page Content Alt Image', NOW(), '1')";
                $mysqli->query($query_pagecontentaltimage);
            }
        }

        //Email Content Alt Image
        if (isset($_POST['files_emailcontentaltimage'])) {
            $files_emailcontentaltimage = $_POST['files_emailcontentaltimage']; //files
            if (!empty($files_emailcontentaltimage)) {
                deleteAsset('Module: Email Content Alt Image', $request_id); //Delete old one first
                $query_emailcontentaltimage = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_emailcontentaltimage."', '/server/php/files/".$user_id."/".$files_emailcontentaltimage."', 'Module: Email Content Alt Image', NOW(), '1')";
                $mysqli->query($query_emailcontentaltimage);
            }
        }

        ////////////////////
        //This is where we clear out data if they selected a Custom template
        if (isset($_POST['template']) && $_POST['template'] != '') {

            //Webinar
            if ($request_type_id == 1) {
                if ($_POST['template'] == 'Custom Template') {
                    $query_template = "UPDATE request_webinar SET template = 'Custom Template', standard_template_type = 'Default', email_banner = '', email_body = '', email_footer = '', email_modules = '', page_banner = '', page_footer = '', page_modules = '', template_instructions = '' WHERE request_id = '".$request_id."' LIMIT 1";
                    $result_template = $mysqli->query($query_template);

                    $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                    $result_notes = $mysqli->query($query_notes);
                } else if ($_POST['template'] == 'Standard Template') {
                    if (isset($_POST['standard_template_type']) && $_POST['standard_template_type'] == 'Default') {
                        $query_template = "UPDATE request_webinar SET template = 'Standard Template', standard_template_type = 'Default', email_banner = '', email_body = '', email_footer = '', email_modules = '', page_banner = '', page_footer = '', page_modules = '', template_instructions = '' WHERE request_id = '".$request_id."' LIMIT 1";
                        $result_template = $mysqli->query($query_template);

                        $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                        $result_notes = $mysqli->query($query_notes);
                    } else {
                        $query_template = "UPDATE request_webinar SET template = 'Standard Template', standard_template_type = 'Custom' WHERE request_id = '".$request_id."' LIMIT 1";
                        $result_template = $mysqli->query($query_template);

                        $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                        $result_notes = $mysqli->query($query_notes);
                    }
                }
            }

            //Email
            if ($request_type_id == 6) {
                if ($_POST['template'] == 'Custom Template') {
                    $query_template = "UPDATE request_email SET template = 'Custom Template', standard_template_type = '', email_banner = '', email_body = '', email_footer = '', email_modules = '', template_instructions = '' WHERE request_id = '".$request_id."' LIMIT 1";
                    $result_template = $mysqli->query($query_template);

                    $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                    $result_notes = $mysqli->query($query_notes);
                } else if ($_POST['template'] == 'Standard Template') {
                    $query_template = "UPDATE request_email SET template = 'Standard Template', standard_template_type = 'Custom' WHERE request_id = '".$request_id."' LIMIT 1";
                    $result_template = $mysqli->query($query_template);

                    $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                    $result_notes = $mysqli->query($query_notes);
                }
            }

            //Landing page
            if ($request_type_id == 7) {
                if ($_POST['template'] == 'Custom Template') {
                    $query_template = "UPDATE request_landingpage SET template = 'Custom Template', standard_template_type = '', page_banner = '', page_footer = '', page_modules = '', template_instructions = '' WHERE request_id = '".$request_id."' LIMIT 1";
                    $result_template = $mysqli->query($query_template);

                    $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                    $result_notes = $mysqli->query($query_notes);
                } else if ($_POST['template'] == 'Standard Template') {
                    $query_template = "UPDATE request_landingpage SET template = 'Standard Template', standard_template_type = 'Custom' WHERE request_id = '".$request_id."' LIMIT 1";
                    $result_template = $mysqli->query($query_template);

                    $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', 'template', 'Value updated', NOW(), '1')";
                    $result_notes = $mysqli->query($query_notes);
                }
            }

        }

    }



    /////////////////////////////////////////////////////////////////
    //SPEAKERS
    //And now let's update speakers. Very repetative, this
    if ($request_type_id == 1) { //Webinars only
        $speaker_name_1 = $mysqli->real_escape_string($_POST['speaker_name_1']);
        $speaker_type_db_1 = implode(", ",$_POST['speaker_type_1']); //array
        $speaker_title_1 = $mysqli->real_escape_string($_POST['speaker_title_1']);
        $speaker_company_1 = $mysqli->real_escape_string($_POST['speaker_company_1']);
        $speaker_email_1 = $mysqli->real_escape_string($_POST['speaker_email_1']);
        $speaker_bio_1 = $mysqli->real_escape_string($_POST['speaker_bio_1']);
        $files_photo_1 = $mysqli->real_escape_string($_POST['files_photo_1']);

        $speaker_name_2 = $mysqli->real_escape_string($_POST['speaker_name_2']);
        $speaker_type_db_2 = implode(", ",$_POST['speaker_type_2']); //array
        $speaker_title_2 = $mysqli->real_escape_string($_POST['speaker_title_2']);
        $speaker_company_2 = $mysqli->real_escape_string($_POST['speaker_company_2']);
        $speaker_email_2 = $mysqli->real_escape_string($_POST['speaker_email_2']);
        $speaker_bio_2 = $mysqli->real_escape_string($_POST['speaker_bio_2']);
        $files_photo_2 = $mysqli->real_escape_string($_POST['files_photo_2']);

        $speaker_name_3 = $mysqli->real_escape_string($_POST['speaker_name_3']);
        $speaker_type_db_3 = implode(", ",$_POST['speaker_type_3']); //array
        $speaker_title_3 = $mysqli->real_escape_string($_POST['speaker_title_3']);
        $speaker_company_3 = $mysqli->real_escape_string($_POST['speaker_company_3']);
        $speaker_email_3 = $mysqli->real_escape_string($_POST['speaker_email_3']);
        $speaker_bio_3 = $mysqli->real_escape_string($_POST['speaker_bio_3']);
        $files_photo_3 = $mysqli->real_escape_string($_POST['files_photo_3']);

        $speaker_name_4 = $mysqli->real_escape_string($_POST['speaker_name_4']);
        $speaker_type_db_4 = implode(", ",$_POST['speaker_type_4']); //array
        $speaker_title_4 = $mysqli->real_escape_string($_POST['speaker_title_4']);
        $speaker_company_4 = $mysqli->real_escape_string($_POST['speaker_company_4']);
        $speaker_email_4 = $mysqli->real_escape_string($_POST['speaker_email_4']);
        $speaker_bio_4 = $mysqli->real_escape_string($_POST['speaker_bio_4']);
        $files_photo_4 = $mysqli->real_escape_string($_POST['files_photo_4']);

        $speaker_name_5 = $mysqli->real_escape_string($_POST['speaker_name_5']);
        $speaker_type_db_5 = implode(", ",$_POST['speaker_type_5']); //array
        $speaker_title_5 = $mysqli->real_escape_string($_POST['speaker_title_5']);
        $speaker_company_5 = $mysqli->real_escape_string($_POST['speaker_company_5']);
        $speaker_email_5 = $mysqli->real_escape_string($_POST['speaker_email_5']);
        $speaker_bio_5 = $mysqli->real_escape_string($_POST['speaker_bio_5']);
        $files_photo_5 = $mysqli->real_escape_string($_POST['files_photo_5']);

        $speaker_name_6 = $mysqli->real_escape_string($_POST['speaker_name_6']);
        $speaker_type_db_6 = implode(", ",$_POST['speaker_type_6']); //array
        $speaker_title_6 = $mysqli->real_escape_string($_POST['speaker_title_6']);
        $speaker_company_6 = $mysqli->real_escape_string($_POST['speaker_company_6']);
        $speaker_email_6 = $mysqli->real_escape_string($_POST['speaker_email_6']);
        $speaker_bio_6 = $mysqli->real_escape_string($_POST['speaker_bio_6']);
        $files_photo_6 = $mysqli->real_escape_string($_POST['files_photo_6']);

        $speaker_name_7 = $mysqli->real_escape_string($_POST['speaker_name_7']);
        $speaker_type_db_7 = implode(", ",$_POST['speaker_type_7']); //array
        $speaker_title_7 = $mysqli->real_escape_string($_POST['speaker_title_7']);
        $speaker_company_7 = $mysqli->real_escape_string($_POST['speaker_company_7']);
        $speaker_email_7 = $mysqli->real_escape_string($_POST['speaker_email_7']);
        $speaker_bio_7 = $mysqli->real_escape_string($_POST['speaker_bio_7']);
        $files_photo_7 = $mysqli->real_escape_string($_POST['files_photo_7']);

        $speaker_name_8 = $mysqli->real_escape_string($_POST['speaker_name_8']);
        $speaker_type_db_8 = implode(", ",$_POST['speaker_type_8']); //array
        $speaker_title_8 = $mysqli->real_escape_string($_POST['speaker_title_8']);
        $speaker_company_8 = $mysqli->real_escape_string($_POST['speaker_company_8']);
        $speaker_email_8 = $mysqli->real_escape_string($_POST['speaker_email_8']);
        $speaker_bio_8 = $mysqli->real_escape_string($_POST['speaker_bio_8']);
        $files_photo_8 = $mysqli->real_escape_string($_POST['files_photo_8']);

        $speaker_name_9 = $mysqli->real_escape_string($_POST['speaker_name_9']);
        $speaker_type_db_9 = implode(", ",$_POST['speaker_type_9']); //array
        $speaker_title_9 = $mysqli->real_escape_string($_POST['speaker_title_9']);
        $speaker_company_9 = $mysqli->real_escape_string($_POST['speaker_company_9']);
        $speaker_email_9 = $mysqli->real_escape_string($_POST['speaker_email_9']);
        $speaker_bio_9 = $mysqli->real_escape_string($_POST['speaker_bio_9']);
        $files_photo_9 = $mysqli->real_escape_string($_POST['files_photo_9']);

        $speaker_name_10 = $mysqli->real_escape_string($_POST['speaker_name_10']);
        $speaker_type_db_10 = implode(", ",$_POST['speaker_type_10']); //array
        $speaker_title_10 = $mysqli->real_escape_string($_POST['speaker_title_10']);
        $speaker_company_10 = $mysqli->real_escape_string($_POST['speaker_company_10']);
        $speaker_email_10 = $mysqli->real_escape_string($_POST['speaker_email_10']);
        $speaker_bio_10 = $mysqli->real_escape_string($_POST['speaker_bio_10']);
        $files_photo_10 = $mysqli->real_escape_string($_POST['files_photo_10']);

        if (isset($_POST['speaker_id_1'])) {
            $query_speaker_1 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_1."', speaker_name = '".$speaker_name_1."', speaker_title = '".$speaker_title_1."', speaker_company = '".$speaker_company_1."', speaker_email = '".$speaker_email_1."', speaker_bio = '".$speaker_bio_1."',";
            if ($_POST['files_photo_1']) { $query_speaker_1.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_1."',"; }
            $query_speaker_1.=" date_updated = NOW()";
            if ($_POST['speaker_delete_1'] == 1) { $query_speaker_1.=", active = 0"; }
            $query_speaker_1.=" WHERE id = '".$_POST['speaker_id_1']."'";
            $result_speaker_1 = $mysqli->query($query_speaker_1);
        }
        if (isset($_POST['speaker_id_2'])) {
            $query_speaker_2 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_2."', speaker_name = '".$speaker_name_2."', speaker_title = '".$speaker_title_2."', speaker_company = '".$speaker_company_2."', speaker_email = '".$speaker_email_2."', speaker_bio = '".$speaker_bio_2."',";
            if ($_POST['files_photo_2']) { $query_speaker_2.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_2."',"; }
            $query_speaker_2.=" date_updated = NOW()";
            if ($_POST['speaker_delete_2'] == 1) { $query_speaker_2.=", active = 0"; }
            $query_speaker_2.=" WHERE id = '".$_POST['speaker_id_2']."'";
            $result_speaker_2 = $mysqli->query($query_speaker_2);
        }
        if (isset($_POST['speaker_id_3'])) {
            $query_speaker_3 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_3."', speaker_name = '".$speaker_name_3."', speaker_title = '".$speaker_title_3."', speaker_company = '".$speaker_company_3."', speaker_email = '".$speaker_email_3."', speaker_bio = '".$speaker_bio_3."',";
            if ($_POST['files_photo_3']) { $query_speaker_3.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_3."',"; }
            $query_speaker_3.=" date_updated = NOW()";
            if ($_POST['speaker_delete_3'] == 1) { $query_speaker_3.=", active = 0"; }
            $query_speaker_3.=" WHERE id = '".$_POST['speaker_id_3']."'";
            $result_speaker_3 = $mysqli->query($query_speaker_3);
        }
        if (isset($_POST['speaker_id_4'])) {
            $query_speaker_4 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_4."', speaker_name = '".$speaker_name_4."', speaker_title = '".$speaker_title_4."', speaker_company = '".$speaker_company_4."', speaker_email = '".$speaker_email_4."', speaker_bio = '".$speaker_bio_4."',";
            if ($_POST['files_photo_4']) { $query_speaker_4.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_4."',"; }
            $query_speaker_4.=" date_updated = NOW()";
            if ($_POST['speaker_delete_4'] == 1) { $query_speaker_4.=", active = 0"; }
            $query_speaker_4.=" WHERE id = '".$_POST['speaker_id_4']."'";
            $result_speaker_4 = $mysqli->query($query_speaker_4);
        }
        if (isset($_POST['speaker_id_5'])) {
            $query_speaker_5 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_5."', speaker_name = '".$speaker_name_5."', speaker_title = '".$speaker_title_5."', speaker_company = '".$speaker_company_5."', speaker_email = '".$speaker_email_5."', speaker_bio = '".$speaker_bio_5."',";
            if ($_POST['files_photo_5']) { $query_speaker_5.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_5."',"; }
            $query_speaker_5.=" date_updated = NOW()";
            if ($_POST['speaker_delete_5'] == 1) { $query_speaker_5.=", active = 0"; }
            $query_speaker_5.=" WHERE id = '".$_POST['speaker_id_5']."'";
            $result_speaker_5 = $mysqli->query($query_speaker_5);
        }
        if (isset($_POST['speaker_id_6'])) {
            $query_speaker_6 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_6."', speaker_name = '".$speaker_name_6."', speaker_title = '".$speaker_title_6."', speaker_company = '".$speaker_company_6."', speaker_email = '".$speaker_email_6."', speaker_bio = '".$speaker_bio_6."',";
            if ($_POST['files_photo_6']) { $query_speaker_6.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_6."',"; }
            $query_speaker_6.=" date_updated = NOW()";
            if ($_POST['speaker_delete_6'] == 1) { $query_speaker_6.=", active = 0"; }
            $query_speaker_6.=" WHERE id = '".$_POST['speaker_id_6']."'";
            $result_speaker_6 = $mysqli->query($query_speaker_6);
        }
        if (isset($_POST['speaker_id_7'])) {
            $query_speaker_7 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_7."', speaker_name = '".$speaker_name_7."', speaker_title = '".$speaker_title_7."', speaker_company = '".$speaker_company_7."', speaker_email = '".$speaker_email_7."', speaker_bio = '".$speaker_bio_7."',";
            if ($_POST['files_photo_7']) { $query_speaker_7.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_7."',"; }
            $query_speaker_7.=" date_updated = NOW()";
            if ($_POST['speaker_delete_7'] == 1) { $query_speaker_7.=", active = 0"; }
            $query_speaker_7.=" WHERE id = '".$_POST['speaker_id_7']."'";
            $result_speaker_7 = $mysqli->query($query_speaker_7);
        }
        if (isset($_POST['speaker_id_8'])) {
            $query_speaker_8 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_8."', speaker_name = '".$speaker_name_8."', speaker_title = '".$speaker_title_8."', speaker_company = '".$speaker_company_8."', speaker_email = '".$speaker_email_8."', speaker_bio = '".$speaker_bio_8."',";
            if ($_POST['files_photo_8']) { $query_speaker_8.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_8."',"; }
            $query_speaker_8.=" date_updated = NOW()";
            if ($_POST['speaker_delete_8'] == 1) { $query_speaker_8.=", active = 0"; }
            $query_speaker_8.=" WHERE id = '".$_POST['speaker_id_8']."'";
            $result_speaker_8 = $mysqli->query($query_speaker_8);
        }
        if (isset($_POST['speaker_id_9'])) {
            $query_speaker_9 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_9."', speaker_name = '".$speaker_name_9."', speaker_title = '".$speaker_title_9."', speaker_company = '".$speaker_company_9."', speaker_email = '".$speaker_email_9."', speaker_bio = '".$speaker_bio_9."',";
            if ($_POST['files_photo_9']) { $query_speaker_9.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_9."',"; }
            $query_speaker_9.=" date_updated = NOW()";
            if ($_POST['speaker_delete_9'] == 1) { $query_speaker_9.=", active = 0"; }
            $query_speaker_9.=" WHERE id = '".$_POST['speaker_id_9']."'";
            $result_speaker_9 = $mysqli->query($query_speaker_9);
        }
        if (isset($_POST['speaker_id_10'])) {
            $query_speaker_10 = "UPDATE speakers SET speaker_type = '".$speaker_type_db_10."', speaker_name = '".$speaker_name_10."', speaker_title = '".$speaker_title_10."', speaker_company = '".$speaker_company_10."', speaker_email = '".$speaker_email_10."', speaker_bio = '".$speaker_bio_10."',";
            if ($_POST['files_photo_10']) { $query_speaker_10.=" speaker_photo = '/server/php/files/".$_SESSION['user_id']."/".$files_photo_10."',"; }
            $query_speaker_10.=" date_updated = NOW()";
            if ($_POST['speaker_delete_10'] == 1) { $query_speaker_10.=", active = 0"; }
            $query_speaker_10.=" WHERE id = '".$_POST['speaker_id_10']."'";
            $result_speaker_10 = $mysqli->query($query_speaker_10);
        }

        //Insert a new speaker?
        if (isset($_POST['speaker_id_10']) && $_POST['speaker_id_10'] == 'new' && $_POST['speaker_name_10'] != '') {
            $query_speakers_new = "INSERT INTO speakers (id, request_id, user_id, speaker_type, speaker_name, speaker_title, speaker_company, speaker_email, speaker_bio, speaker_photo, date_created, date_updated, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_10."', '".$speaker_name_10."', '".$speaker_title_10."', '".$speaker_company_10."', '".$speaker_email_10."', '".$speaker_bio_10."', '/server/php/files/".$_SESSION['user_id']."/".$files_photo_10."', NOW(), NOW(), '1')";
            $mysqli->query($query_speakers_new);
        }
    } //End Webinars only



    /////////////////////////////////////////////////////////////////
    //WE'RE MAKING AN EDIT TO THE REQUEST. MAYBE BY USER, MAYBE BY ADMIN
    if (isset($_POST['edit_form']) && $_POST['edit_form'] == true) {
        //Create a note
        $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', NULL, 'Request updated', NOW(), '1')";
        $mysqli->query($query_note);

        //And now alert the sys admin that an update has been made:
        //Send email user
        $message_update = $message_header;
        $message_update.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following request has been updated by its original creator:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/request-detail?id=".$request_id."</a></p>";
        $alt_update = 'The following request has been updated: '.$domain.'/request-detail?id='.$request_id;

        $message_update.=$message_footer;

        $mail->AddAddress($email_ms);
        $mail->Subject = 'Intel Customer Connect: Request #'.$request_id.' has been updated';
        $mail->MsgHTML($message_update);
        $mail->AltBody = $alt_update;
        //$mail->Send(); //PUT THIS BACK IN ON LAUNCH
    }


    //Ok, back to normal admin update... which is just notes and additional info and etc
    /*MOVED TO EDITABLE
    if ($notes != '') {
        $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', NULL, '".$notes."', NOW(), '1')";
        $result_notes = $mysqli->query($query_notes);
    }
    */

    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //We're revamping the messaging based on status because it's all unique now.
    $alt = '';
    $send = false;
    $message_user_body = '';
    $message_user = $message_header;

    //ALL OTHER: Complete
    if (($request_type_id == 1 || $request_type_id == 2 || $request_type_id == 3 || $request_type_id == 5 || $request_type_id == 6 || $request_type_id == 7) && $status == 4) {
        $message_user_body = "Your tactic request has been successfully completed. Please contact support if you have any further questions.";
        $alt = 'Your tactic request has been successfully completed. Please contact support if you have any further questions.';
        $send = true;
    }
    //ALL OTHERS: On Hold
    if (($request_type_id == 1 || $request_type_id == 2 || $request_type_id == 3 || $request_type_id == 5 || $request_type_id == 6 || $request_type_id == 7) && $status == 6) {
        $message_user_body = "Your tactic request is currently on hold. Please check the \"Comments\" section in your request for further instructions. Please send support an email if you have further questions.";
        $alt = 'Your tactic request is currently on hold. Please check the "Comments" section in your request for further instructions. Please send support an email if you have further questions.';
        $send = true;
    }
    //Webinars: Parked
    if ($request_type_id == 1 && $status == 13) {
        $message_user_body = "Your webinar request is currently parked. Please check the \"Comments\" section in your request for further instructions. Please send support an email if you have further questions.";
        $alt = 'Your webinar request is currently parked. Please check the "Comments" section in your request for further instructions. Please send support an email if you have further questions.';
        $send = true;
    }
    //Request Campaign: Complete
    if ($request_type_id == 8 && $status == 4) {
        $message_user_body = "The campaign that you have requested has been successfully completed.";
        $alt = 'The campaign that you have requested has been successfully completed.';
        $send = true;
    }
    //Request Campaign: Incomplete
    if ($request_type_id == 8 && $status == 3) {
        $message_user_body = "The campaign that you have requested has been setup but is incomplete/insufficient information. You must complete the campaign information properly before we can begin to build any assets for any associated tactics.";
        $alt = 'The campaign that you have requested has been setup but is incomplete/insufficient information. You must complete the campaign information properly before we can begin to build any assets for any associated tactics.';
        $send = true;
    }
    //Contact Upload: Complete
    if ($request_type_id == 4 && $status == 4) {
        $message_user_body = "The contact list you have provided has been successfully uploaded. Please refer to the receipt document attached to your request for details.";
        $alt = 'The contact list you have provided has been successfully uploaded. Please refer to the receipt document attached to your request for details.';
        $send = true;
    }
    //Contact Upload: Rejected
    if ($request_type_id == 4 && $status == 10) {
        $message_user_body = "The contact list you have provided has errors. Please refer to the receipt document attached to your request for details.";
        $alt = 'The contact list you have provided has errors. Please refer to the receipt document attached to your request for details.';
        $send = true;
    }
    //Lead Upload: Complete
    if ($request_type_id == 9 && $status == 4) {
        $message_user_body = "The lead list you have provided has been successfully uploaded. Please refer to the receipt document attached to your request for details.";
        $alt = 'The lead list you have provided has been successfully uploaded. Please refer to the receipt document attached to your request for details.';
        $send = true;
    }
    //Lead Upload: Rejected
    if ($request_type_id == 9 && $status == 10) {
        $message_user_body = "The lead list you have provided has errors. Please refer to the receipt document attached to your request for details.";
        $alt = 'The lead list you have provided has errors. Please refer to the receipt document attached to your request for details.';
        $send = true;
    }
    //All requests: Cancelled
    if ($status == 11) {
        $message_user_body = "Your request has been cancelled.";
        $alt = 'Your request has been cancelled.';
        $send = true;
    }


    //Ok, final message building
    $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">".$message_user_body."</p><p><a href=\"".$domain."/"; if ($request_type_id == 8) { $message_user.="campaign-detail"; } else { $message_user.="request-detail"; } $message_user.="?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

    $message_user.=$message_footer;
    $mail2->AddAddress($user_email);
    $mail2->Subject = 'Intel Customer Connect: Status Update: '.$the_title;
    $mail2->MsgHTML($message_user);
    $mail2->AltBody = $alt;
    if ($send == true && $no_status_change == false && $message_user_body != '') {
        $mail2->Send();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Sending the Assigned To person an email
    $alt_as = '';
    $message_as = $message_header;

    $message_as.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">You have been assigned to support the ".$tactic_type." request for ".$the_title.".</p><p><a href=\"".$domain."/"; if ($request_type_id == 8) { $message_as.="campaign-detail"; } else { $message_as.="request-detail"; } $message_as.="?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
    $alt_as = 'You have been assigned to support the '.$tactic_type.' request for '.$the_title;

    $message_as.=$message_footer;
    $mail->AddAddress(getEmailAddress($assigned_to));
    $mail->Subject = 'Intel Customer Connect: You have been assigned: '.$the_title;
    $mail->MsgHTML($message_as);
    $mail->AltBody = $alt_as;
    if ($no_assigned_change == false) {
        $mail->Send();
    }


    if ($result || $result_edit) {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //Sending the Assigned To person an email to let them know updates have been made (but only if they didn't get another email about being assigned to the ticket)
        if ($no_assigned_change == true) {
            $alt_up = '';
            $message_up = $message_header;

            $message_up.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Updates have been made to the ".$tactic_type." request: ".$the_title.". Please view the notes on the request detail page.</p><p><a href=\"".$domain."/"; if ($request_type_id == 8) { $message_up.="campaign-detail"; } else { $message_up.="request-detail"; } $message_up.="?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
            $alt_up = 'Updates have been made to the '.$tactic_type.' request: '.$the_title;

            $message_up.=$message_footer;

            $mail3->AddAddress(getEmailAddress($assigned_to));
            $mail3->AddReplyTo($user_email);
            $mail3->Subject = 'Intel Customer Connect: Updates have been made to: '.$the_title;
            $mail3->MsgHTML($message_up);
            $mail3->AltBody = $alt_up;
            $mail3->Send();
        }

        if ($request_type_id == 8) {
            header('Location:/campaign-detail?id='.$request_id.'&update=success');
        } else {
            header('Location:/request-detail?id='.$request_id.'&update=success');
        }
    } else {
        if ($request_type_id == 8) {
            header('Location:/campaign-detail?id='.$request_id.'&update=fail');
        } else {
            header('Location:/request-detail?id='.$request_id.'&update=fail');
        }
    }

    /* close connection */
    $mysqli->close();

} else {
    header('Location:request?action=failure'); 
}
?>