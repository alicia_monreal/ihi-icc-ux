<?php 
    require_once('../includes/_globals.php');
?>

<style type="text/css">
.notification-recipients {
    padding: 10px 6px 0 !important;
}
ul {
    list-style: none;
    margin: 0 0 10px 4px;
}
</style>

<form style="margin-bottom:0;" enctype="multipart/form-data" method="POST" action="includes/_placeholder_process.php" class="form-horizontal form-taut" id="placeholder-request-form" novalidate="novalidate">
<h4 id="message-notify" style="color:#ff0000;font-size:13px"></h4>
<div class="control-group" id="request-type-select" style="display: block;">
    <label class="control-label" for="request_type">Request Type</label>
    <div class="controls">&nbsp;
        <select class="input-wide valid" name="request_type" id="request_type" style="width:370px">
            <?php
            $query_req = "SELECT id, request_type FROM request_type
                          WHERE request_type NOT IN ('Event','Contact List','Lead List','Placeholder','Campaign') ORDER BY sort_order";
                          // all request types except contact list, lead list, placeholder and event
            $result_req = $mysqli->query($query_req);
            while ($obj_req = $result_req->fetch_object()) {
                echo '<option value="'.$obj_req->id.'"'; if ($obj_req->id == 1) { echo ' selected'; } echo '>'.$obj_req->request_type.'</option>';
            }
            ?>
        </select>
    </div>
</div>
<div id="campaign-search-popup" class="control-group">
    <label for="campaign_search_popup" class="control-label">Campaign</label>
    <div class="controls">
            <span class="required-mark">*</span>&nbsp;&nbsp;<input type="text" id="campaign_search_popup" name="campaign_search_popup" class="input-wide required text-modal" placeholder="Start typing the GEOs or keywords from the campaign name" value="" >
    </div>
</div>
<div class="control-group" id="request-name">
    <label for="name" class="control-label">Name</label>
    <div class="controls"><span class="required-mark">*</span>&nbsp;&nbsp;<input type="text" id="name" name="name" class="input-wide required text-modal" value=""></div>
</div>
<div class="control-group" id="request-description">
    <label for="description" class="control-label">Description</label>
    <div class="controls"><span class="required-mark" style="vertical-align:top;">*</span>&nbsp;&nbsp;<textarea id="description" name="description" class="input-wide textarea-medium required text-modal"></textarea></div>
</div>

<?php 
    $users_string = $current_user = "";

    if(!empty($_SESSION['user_id'])) // remove session id from list
        $current_user = "AND id !=".$_SESSION['user_id'];

    // fetch all active users data except the logged-in user
    $query_users = "SELECT id, fname, lname, email FROM users WHERE active = 1 $current_user ORDER BY fname ASC";
    $result_users = $mysqli->query($query_users);
    
    while ($obj_users = $result_users->fetch_object()) {
         $users_string .= '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
    }
?>
<div class="control-group" id="additional-user-rights" style="margin-bottom:5px">
    <label for="additional-rights-users" class="control-label">Select Additional Owners</label>
    <div class="controls" style="margin-top:8px">&nbsp;
    <select id="additional-rights-users" name="additional-rights-users[]" data-placeholder="Choose a user..." multiple style="width:370px;border:none !important;" class="required">
    <?php echo $users_string; // same users option shown in this dropdown aswell ?>
    </select>
    <div class="chosen-rights-users notification-recipients"><ul></ul></div>
    </div>
</div>
<div class="control-group" id="request-start-date">
    <label class="control-label" for="start_date">Start Date</label>
    <div class="controls">
    <span class="required-mark">*</span>&nbsp;&nbsp;
        <div style="" class="input-append date" id="datetimepicker"><input type="text" value="" style="width:134px;" class="input-medium valid" name="start_date" id="start_date"><span style="margin-right:2px;" class="add-on"><i class="icon-th"></i></span></div>
    </div>
</div>
<div class="control-group" id="request-end-date" style="display:none">
    <label class="control-label" for="end_date">End Date</label>
    <div class="controls">
    <span class="required-mark">*</span>&nbsp;&nbsp;
        <div class="input-append date" id="datetimepicker_end"><input type="text" value="" style="width:134px;" class="input-medium" name="end_date" id="end_date"><span class="add-on"><i class="icon-th"></i></span></div>
    </div>
</div>

<div class="control-group" id="request-start-time">
    <label for="start_time" class="control-label">Start Time</label>
    <div class="controls"><span class="required-mark">*</span>&nbsp;&nbsp;&nbsp;<select id="start_time" name="start_time" class="input-medium required" style="width:120px;margin-right:2px;">
        <option value="">Select One</option>
        <option value="01:00">01:00</option>
        <option value="01:30">01:30</option>
        <option value="02:00">02:00</option>
        <option value="02:30">02:30</option>
        <option value="03:00">03:00</option>
        <option value="03:30">03:30</option>
        <option value="04:00">04:00</option>
        <option value="04:30">04:30</option>
        <option value="05:00">05:00</option>
        <option value="05:30">05:30</option>
        <option value="06:00">06:00</option>
        <option value="06:30">06:30</option>
        <option value="07:00">07:00</option>
        <option value="07:30">07:30</option>
        <option value="08:00">08:00</option>
        <option value="08:30">08:30</option>
        <option value="09:00">09:00</option>
        <option value="09:30">09:30</option>
        <option value="10:00">10:00</option>
        <option value="10:30">10:30</option>
        <option value="11:00">11:00</option>
        <option value="11:30">11:30</option>
        <option value="12:00">12:00</option>
        <option value="12:30">12:30</option>
    </select><select id="start_ampm" name="start_ampm" style="width: 64px;"><option value="AM" selected="">AM</option><option value="PM" >PM</option></select>
    </div>
</div>

<div class="control-group" id="request-end-time">
    <label for="end_time" class="control-label">End Time</label>
    <div class="controls"><span class="required-mark">*</span>&nbsp;&nbsp;&nbsp;<select id="end_time" name="end_time" class="input-medium required" style="width:120px;margin-right:2px;">
        <option value="">Select One</option>
        <option value="01:00">01:00</option>
        <option value="01:30">01:30</option>
        <option value="02:00">02:00</option>
        <option value="02:30">02:30</option>
        <option value="03:00">03:00</option>
        <option value="03:30">03:30</option>
        <option value="04:00">04:00</option>
        <option value="04:30">04:30</option>
        <option value="05:00">05:00</option>
        <option value="05:30">05:30</option>
        <option value="06:00">06:00</option>
        <option value="06:30">06:30</option>
        <option value="07:00">07:00</option>
        <option value="07:30">07:30</option>
        <option value="08:00">08:00</option>
        <option value="08:30">08:30</option>
        <option value="09:00">09:00</option>
        <option value="09:30">09:30</option>
        <option value="10:00">10:00</option>
        <option value="10:30">10:30</option>
        <option value="11:00">11:00</option>
        <option value="11:30">11:30</option>
        <option value="12:00">12:00</option>
        <option value="12:30">12:30</option>
    </select><select id="end_ampm" name="end_ampm" style="width: 64px;"><option value="AM" selected="">AM</option><option value="PM">PM</option></select>
    </div>
</div>

</form>
