<?php
session_start();
require_once('validate.php');
include('_globals.php');
include('notification_rules_email.php');

if (!empty($_POST) && !empty($_POST['action'])) {

    //Some globals
    $user_id = $_POST['user_id'];
    $user_email = $_POST['user_email'];
    $request_type = 2;
    $campaign_id = $_SESSION['campaign_id'];

    //Step 2. Saving values to session data
    if ($_POST['step'] == 2) {

        $request_id = $_SESSION['in_progress_id'];

        //All our session values
        $_SESSION['tag'] = $_POST['tag'];
        $_SESSION['owner'] = $_POST['owner'];
        $_SESSION['owner_email'] = $_POST['owner_email'];
        $_SESSION['vertical'] = '';
        $_SESSION['name'] = createTacticName('eNurture', $_POST['tag']);
        $_SESSION['start_date'] = $_POST['start_date'];
        $_SESSION['end_date'] = $_POST['end_date'];
        $topics_array = array();
        foreach($_POST['topics_covered'] as $val_topics) {
            array_push($topics_array, $val_topics);
        }
        $_SESSION['topics_covered'] = $topics_array;
        $_SESSION['topics_covered_other'] = $_POST['topics_covered_other'];
        $_SESSION['description'] = $_POST['description'];
        $_SESSION['asset_reviewers'] = $_POST['asset_reviewers'];

        $_SESSION['email_from_name'] = $_POST['email_from_name'];
        $_SESSION['email_from_address'] = $_POST['email_from_address'];
        $_SESSION['email_reply_name'] = $_POST['email_reply_name'];
        $_SESSION['email_reply_address'] = $_POST['email_reply_address'];

        //Create a new individual request
        $language = $_SESSION['language'];
        $marketing_id = $_SESSION['marketing_id'];

        $name = $mysqli->real_escape_string($_SESSION['name']);
        $tag = $mysqli->real_escape_string($_POST['tag']);
        $owner = $mysqli->real_escape_string($_POST['owner']);
        $owner_email = $mysqli->real_escape_string($_POST['owner_email']);
        $vertical_db = '';
        $start_date = date('Y-m-d 00:00:00', strtotime($_POST['start_date'])); //date format
        $end_date = date('Y-m-d 00:00:00', strtotime($_POST['end_date'])); //date format
        $topics_covered_db = implode(", ",$topics_array); //array
        $topics_covered_other = $mysqli->real_escape_string($_POST['topics_covered_other']);
        $description = $mysqli->real_escape_string($_POST['description']);
        $asset_reviewers = $mysqli->real_escape_string($_POST['asset_reviewers']);

        $email_from_name = $mysqli->real_escape_string($_POST['email_from_name']);
        $email_from_address = $mysqli->real_escape_string($_POST['email_from_address']);
        $email_reply_name = $mysqli->real_escape_string($_POST['email_reply_name']);
        $email_reply_address = $mysqli->real_escape_string($_POST['email_reply_address']);

        //MID
        $source_activity_name = $mysqli->real_escape_string($_SESSION['source_activity_name']);
        $source_description = $mysqli->real_escape_string($_SESSION['source_description']);
        $source_program = $_SESSION['source_program'];
        $source_activity_type = $_SESSION['source_activity_type'];
        $source_offer_type = $_SESSION['source_offer_type'];
        $marketing_team = $_SESSION['marketing_team'];

        //IN PROGRESS
        //This is in progress
        if (isset($_SESSION['in_progress_id']) && $_SESSION['in_progress_id'] != NULL) {
            $eloqua_source_id = createEloquaID($campaign_id, $_SESSION['name'], $_SESSION['in_progress_id']);
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);

            //Insert UPDATE statement instead
            $query = "UPDATE request_enurture SET
                            eloqua_source_id = '".$eloqua_source_id."',
                            name = '".$name.".".$request_id."',
                            tag = '".$tag."',
                            owner = '".$owner."',
                            owner_email = '".$owner_email."',
                            start_date = '".$start_date."',
                            end_date = '".$end_date."',
                            topics_covered = '".$topics_covered_db."',
                            topics_covered_other = '".$topics_covered_other."',
                            description = '".$description."',
                            asset_reviewers = '".$asset_reviewers."',
                            email_from_name = '".$email_from_name."',
                            email_from_address = '".$email_from_address."',
                            email_reply_name = '".$email_reply_name."',
                            email_reply_address = '".$email_reply_address."'
                        WHERE request_id = ".$request_id;
            $mysqli->query($query);
            $_SESSION['tactic_request_id'] = getTacticIdFromRequestId($_SESSION['in_progress_id'], $request_type);

        } else {
            
            //Create a new request
            $request_id = createInProgressRequest($user_id, $request_type, $campaign_id);

            $eloqua_source_id = createEloquaID($campaign_id, $_SESSION['name'], $request_id);
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);

            $query = "INSERT INTO request_enurture VALUES (
                            NULL,
                            '".$user_id."',
                            '".$request_id."',
                            '".$marketing_id."',
                            '".$source_activity_name."',
                            '".$source_description."',
                            '".$source_program."',
                            '".$source_activity_type."',
                            '".$source_offer_type."',
                            '".$marketing_team."',
                            '".$eloqua_source_id."',
                            '".$language."',
                            '".$name.".".$request_id."',
                            '".$tag."',
                            '".$owner."',
                            '".$owner_email."',
                            '".$vertical_db."',
                            '".$start_date."',
                            '".$end_date."',
                            '".$topics_covered_db."',
                            '".$topics_covered_other."',
                            '".$description."',
                            '".$asset_reviewers."',
                            '".$email_from_name."',
                            '".$email_from_address."',
                            '".$email_reply_name."',
                            '".$email_reply_address."',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '1',
                            '1')";

            $mysqli->query($query);
            $_SESSION['in_progress_id'] = $request_id;
            $_SESSION['tactic_request_id'] = $mysqli->insert_id;
        }

        if (isset($_POST['in_progress_form']) && $_POST['in_progress_form'] == 1) {
            header('Location:/requests?inprogress=true&request_id='.$request_id);
        } else {
            header('Location:/request-enurture-3');
        }
    }

    //Step 3. Saving values to session data
    if ($_POST['step'] == 3) {

        if(isset($_POST['template']) && $_POST['template'] !== ""){

            $request_id = $_SESSION['in_progress_id'];
            $template = '';
            $standard_template_type = '';

            if($_POST['action'] === 'savetemplate' && $_POST['template'] === 'custom'){
                $template = 'Standard Template';
                $standard_template_type = 'Custom';

                $email_data = $_POST;
                
                unset($email_data['template']);
                unset($email_data['action']);
                unset($email_data['step']);
                unset($email_data['c_type']);

                //Encode characters
                foreach ($email_data as $key => $value) {
                    $email_data[$key] = $mysqli->real_escape_string($value);
                }

                $enurture_emails = $enurture_landingpages = array();
                foreach($email_data as $key => $value) {
                    $key_name = explode("_",$key);

                    if($key_name[0] == "enurture") { // if type is enurture
                        if($key_name[1] == "email") { // if type is email
                            $enurture_emails[] = $value;
                        } else if ($key_name[1] == "landingpage") {
                            $enurture_landingpages[] = $value;
                        }
                    }
                }

                foreach($enurture_emails as $key => $value) {
                    $session_email_data['enurture_email_config_'.$key] = $value; // fixed order defined
                }

                foreach ($enurture_landingpages as $key => $value) {
                    $session_email_data['enurture_landingpage_config_'.$key] = $value; // fixed order defined
                }

                $session_email_data['request_id'] = $email_data['request_id'];
                $session_email_data['user_id'] = $email_data['user_id'];
                
                $_SESSION['t_request_id']           =  $request_id; 
                $_SESSION['templates_data']         =  $session_email_data;  // save content values into session
                
                // delete previous enurture template data for same request
                $query_remove = 'DELETE FROM multiple_templates_data WHERE request_id = '.$request_id;
                $mysqli->query($query_remove);

                if(!empty($enurture_emails)) { // if not empty enurture emails
                    // enurture emails insertion
                    foreach($enurture_emails as $key => $value) {
                        $query = 'INSERT INTO multiple_templates_data VALUES (
                                        NULL,
                                        "' .$email_data['user_id'] .'",
                                        "' .$request_id.'",
                                        "' .$value.'",
                                        "",
                                        '.$key.')';
                        $mysqli->query($query); 
                    } 
                }

                if(!empty($enurture_landingpages)) { // if not empty enurture landing pages
                    // enurture landing pages insertion
                    foreach($enurture_landingpages as $key => $value) {
                        $query = 'INSERT INTO multiple_templates_data VALUES (
                                        NULL,
                                        "' .$email_data['user_id'] .'",
                                        "' .$request_id.'",
                                        "",
                                        "' .$value.'",
                                        '.$key.')';
                        $mysqli->query($query);
                    }
                }

            }

            //Custom template, no fields
            else if ($_POST['template'] == 'Custom Template') {
                $template = 'Custom Template';
                $standard_template_type = '';

                if (!isset($_SESSION['files_customassets']) && !isset($_POST['files_customassets'])) {
                    // no uploaded file 
                    header('Location:/request-enurture-3?message=nofile');
                    return;
                }

                //Custom assets
                if (isset($_SESSION['files_customassets']) && !isset($_POST['files_customassets'])) {
                    $_SESSION['files_customassets'] = $_SESSION['files_customassets'];
                } else {
                    $_SESSION['files_customassets'] = $_POST['files_customassets'];
                }
                $files_customassets = $_SESSION['files_customassets'];

                //Custom assets
                if (!empty($_SESSION['files_customassets'])) {
                    $path = "/server/php/files/".$user_id."/".$files_customassets;
                    if (isset($_SESSION['files_customassets_path']) && $_SESSION['files_customassets_path'] != '') {
                        $path_exisiting = $abspath."/server/php/files/".$user_id."/".$files_customassets;
                        if (!file_exists($path_exisiting)) { //This means they didn't upload something themselves even if there's a path set in session (i.e. overwritten file)
                            $path = $_SESSION['files_customassets_path'];
                        }
                    }

                    $query_customassets = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_customassets."', '".$path."', 'Custom Assets', NOW(), '1')";
                    $mysqli->query($query_customassets);
                }
            }

            $_SESSION['template'] = $template;
            $_SESSION['standard_template_type'] = $standard_template_type;

            //Insert UPDATE statement
            $query_3 = "UPDATE request_enurture SET template = '".$template."', standard_template_type = '".$standard_template_type."' WHERE id = ".$_SESSION['tactic_request_id'];
            $mysqli->query($query_3);

            //Only insert template data if they've selected a template type
            if ($template == 'Standard Template' && !empty($standard_template_type)) {
                $query_template = "INSERT INTO templates VALUES (NULL, '".$user_id."', '".$request_id."', '".$standard_template_type."', NOW())";
                $mysqli->query($query_template);
                $template_id = $mysqli->insert_id;
            }

            //Now send them to Step 4
            if (isset($_POST['in_progress_form']) && $_POST['in_progress_form'] == 1) {
                header('Location:/requests?inprogress=true&request_id='.$request_id);
            } else {
                header('Location:/request-enurture-4');
            }
        }else{
            // no selected template 
            header('Location:/request-enurture-3?message=notemplate');
        }
    }

    //Step 4. Saving values to session data
    if ($_POST['step'] == 4) {

        $request_id = $_SESSION['in_progress_id'];

        $_SESSION['eloqua_templates'] = $_POST['eloqua_templates'];

        //Custom assets file
        if (isset($_SESSION['files_customassets']) && !isset($_POST['files_customassets'])) {
            $_SESSION['files_customassets'] = $_SESSION['files_customassets'];
            $files_customassets_path = $_SESSION['files_customassets_path'];    
            $fcaPath = explode("/", $files_customassets_path);
            $fcaPath[sizeof($fcaPath)-1] = $_SESSION['files_customassets'];
            $path = implode("/", $fcaPath);
        } else {
            $_SESSION['files_customassets'] = $_POST['files_customassets'];
            $path = "/server/php/files/".$user_id."/".$_SESSION['files_customassets'];
        }
        $files_customassets = $_SESSION['files_customassets'];

        //Custom assets
        if (!empty($_SESSION['files_customassets'])) {

            $query_customassets = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_customassets."', '".$path."', 'Custom Assets', NOW(), '1')";
            $mysqli->query($query_customassets);
        }

        if (isset($_POST['in_progress_form']) && $_POST['in_progress_form'] == 1) {
            header('Location:/requests?inprogress=true&request_id='.$request_id);
        } else {
            header('Location:/request-enurture-5');
        }
    }


    //Step 5. Saving values to session data, AND writing to the database
    if ($_POST['step'] == 5) {

        $request_id = $_SESSION['in_progress_id'];

        //STEP 4
        $create_contactlist = $_POST['create_contactlist'];
        $list_tag = $mysqli->real_escape_string($_POST['list_tag']);
        $contact_list_request_type = $mysqli->real_escape_string($_POST['contact_list_request_type']);
        $files_contacts = $addition_owners = array();
        foreach ($_POST['files_contacts'] as $val_contacts) {
            array_push($files_contacts, $val_contacts);
        }
        $existing_contact_list = $mysqli->real_escape_string($_POST['existing_contact_list']);
        $additional_instructions = $mysqli->real_escape_string($_POST['additional_instructions']);

        $additional_recipients = $_POST['additional-notifiers'];
        $additional_owners = $_POST['additional-rights-users'];
        $add_owners = $_POST['add-owners']; // additional owners added through placeholder

        if(!empty($add_owners)) {
            foreach($add_owners as $value) {
                if($value != "")
                    array_push($additional_owners, $value);   // final additional owner array
            }
        }

        if(!empty($additional_recipients) && is_array($additional_recipients)) {
            $additional_rec = $mysqli->real_escape_string(serialize($additional_recipients)); // serialized string 
            $add_recipients = ",additional_recipients = \"$additional_rec\"";
        }

        if(!empty($additional_owners) && is_array($additional_owners)) {
            $additional_own = $mysqli->real_escape_string(serialize(array_unique($additional_owners))); // serialized sring
            $adding_owners = ",additional_owners = \"$additional_own\"";
        }
        /////////////////////////////////////////////////////////////////////////////////////
        //Create a new Contact List tactic if they chose to do so
        if ($create_contactlist == 1) {
            include('_requests_create_contactlist.php');
        }

        //Insert UPDATE statement
        $query_4 = "UPDATE request_enurture SET existing_contact_list = '".$existing_contact_list."', additional_instructions = '".$additional_instructions."' WHERE id = ".$_SESSION['tactic_request_id'];
        $mysqli->query($query_4);

        /////////////////////////////////////////////////////////////////////////////////////
        //Update the status of the request
        $query_final = "UPDATE request SET status = 1 $add_recipients $adding_owners WHERE id = ".$request_id." LIMIT 1";
        $final = $mysqli->query($query_final);

        //SUCCESS
        if ($final) {

            /////////////////////////////////////////////////////////////////////////////////////
            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);


            /////////////////////////////////////////////////////////////////////////////////////
            //Send email to Intel
            $name = cleanForEmail($_SESSION['name']);
            $tag = cleanForEmail($_SESSION['tag'], 1);
            $description_email = cleanForEmail($_SESSION['description']);

            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new eNurture tactic request has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">".$name.".".$request_id."<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>eNurture Owner</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['owner']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Start/End Date</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".date('m/d/Y', strtotime($_SESSION['start_date']))." - ".date('m/d/Y', strtotime($_SESSION['end_date']))."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Description</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$description_email."</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            
            if (getSector($request_id) == 'Educator') {
                //$mail->AddCC('thomas.hanna@intel.com', 'Thomas Hanna');
            } else {
                //$mail->AddCC('seamusx.james@intel.com', 'Seamus James'); //Special person
                //$mail->AddCC('jeffx.ouano@intel.com', 'Jeff Ouano'); //Special person
            }

            /* Send notification rules email to assignees */
            $mail = addAddtionalRecipients($mail,$additional_recipients,$email_ms);
            $mail = sendNotificationRulesEmail($mail,$request_type,$campaign_id, $request_id, $mysqli, $message_header,$message_footer, $domain, $mail3);            

            //Special for Doug Marshall and Kristi
            //if ($user_id == 365) { $mail->AddCC('kristi@ironhorseinteractive.com', 'Kristi Teplitz'); }
            $mail->Subject = 'Intel Customer Connect: New eNurture: '.$tag.'.'.$request_id;
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/request-detail?id='.$request_id.'';
            $mail->Send();

            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Automated reports begin delivering EOD from when the first email is sent out daily for a week (5 business days) EST for each email. These are basic Eloqua reporting emails that have key metrics like: opens, clickthrough rate, click to open rate, possible email forwards, unique open rate.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Intel Customer Connect: eNurture Request Received: '.$tag.'.'.$request_id;
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();
            
            //Kill some session vars
            killSessionVars();

            //Finally, let's let 'em know
            header('Location:/request-detail?id='.$request_id.'&action=success');

        }
    }

} else {
    header('Location:content-update-request?action=failure'); 
}
?>