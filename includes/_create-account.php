<?php
session_start();
include('_globals.php');

function verifyFormToken($form) {
    if(!isset($_SESSION[$form.'_token'])) { 
        return false;
    }
    if(!isset($_POST['token'])) {
        return false;
    }
    if ($_SESSION[$form.'_token'] !== $_POST['token']) {
        return false;
    }
    
    return true;
}

if (!empty($_POST) && !empty($_POST['fname']) && verifyFormToken('request-form')) {

    //Values
    $fname = $mysqli->real_escape_string($_POST['fname']);
    $lname = $mysqli->real_escape_string($_POST['lname']);
    //$phone = $mysqli->real_escape_string($_POST['phone']);
    $phone = '';
    $email = $mysqli->real_escape_string($_POST['email']);
    $title = $mysqli->real_escape_string($_POST['title']);
    $company = $mysqli->real_escape_string($_POST['company']);

    //IE fix
    if ($_POST['point_of_contact'] == 'Who is your Intel point of contact?*') {
        $point_of_contact = '';
    } else {
        $point_of_contact = $mysqli->real_escape_string($_POST['point_of_contact']);
    }
    if ($_POST['point_of_contact_email'] == 'Point of contact email address*') {
        $point_of_contact_email = '';
    } else {
        $point_of_contact_email = $mysqli->real_escape_string($_POST['point_of_contact_email']);
    }
    $role = $_POST['role'];
    $password = generatePassword(8,4);
    $password_md5 = md5($password); //Not using md5.

    //Need to make sure that email isn't already in use
    $query_check = "SELECT password FROM users WHERE email = '".$email."' ORDER BY date_created DESC LIMIT 1";
    $result_check = $mysqli->query($query_check);
    $row_cnt_check = $result_check->num_rows;

    if ($row_cnt_check > 0) {
        echo 'failure';
        //echo 'That email account is already in use. If you\'ve forgotten your password, please use the Forgot Password link to retrieve it. <a href="" data-dismiss="modal">Close window</a>';
        die();
    }

    //Not already in use? Let's get a new account created
    $query = "INSERT INTO users VALUES (NULL, '".$fname."', '".$lname."', '".$phone."', '".$email."', '".$title."', '".$company."', '".$point_of_contact."', '".$point_of_contact_email."', '".$role."', '".$password."', 0, 0, 0, 0, '1, 2', NOW(), NOW())";
    $mysqli->query($query);

    if ($mysqli->insert_id) {
        
        //Send email to Intel
        $message_ms = $message_header;

        $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following person has requested an account for Intel Customer Connect. <a href=\"".$domain."/manage-users\" style=\"color:#1570a6;\">Click here</a> to manage users:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>".stripslashes($fname)." ".stripslashes($lname)."</strong><br /><a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a></p>";

        if(!empty($point_of_contact_email)){
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The user has listed <strong>".stripslashes($point_of_contact)."</strong> (<a href=\"mailto:".$point_of_contact_email."\" style=\"color:#1570a6;\">".$point_of_contact_email."</a>) as their point of contact, and they have been emailed this information.</p>";
        } 

        $message_ms.=$message_footer;
            
        $mail->AddAddress($email_ms);
        $mail->Subject = 'Intel Customer Connect: New Account Request';
        $mail->MsgHTML($message_ms);
        $mail->AltBody = 'The following person has requested an account for Intel Customer Connect.';
        $mail->Send();

        //Send the message, check for errors
        //if(!$mail->Send()) {
            //echo 'Sorry, there was a problem sending a confirmation email, please <a href="mailto:support@intel.com">contact us</a> to let us know of the error.';
        //}

        //Send email to new user
        $message_user = $message_header;
        $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for requesting access to Intel Customer Connect. We have forwarded your request to the Intel Customer Connect team and you should be hearing from them shortly.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you.</p>";
        $message_user.=$message_footer;
           
        $mail2->AddAddress($email, ''.$fname.' '.$lname.'');
        $mail2->Subject = 'Intel Customer Connect: New Account Request';
        $mail2->MsgHTML($message_user);
        $mail2->AltBody = 'Thank you for requesting access to Intel Customer Connect. We have forwarded your request to the Intel Customer Connect team and you should be hearing from them shortly.';
        $mail2->Send();

        //Send email to point of contact
        if(!empty($point_of_contact_email)){
            $query_admin = "SELECT admin FROM users WHERE email = '".$point_of_contact_email."' LIMIT 1";
            $obj_admin = $mysqli->query($query_admin);
            while ($obj_ad = $obj_admin->fetch_object()) {
                if ($obj_ad->admin == 1) { 
                    $message_contact = $message_header;
                    $message_contact.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following person has requested an account for Intel Customer Connect and listed you as their point of contact. <a href=\"".$domain."/manage-users\" style=\"color:#1570a6;\">Click here</a> to approve of deny this user:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>".stripslashes($fname)." ".stripslashes($lname)."</strong><br /><a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a></p>";
                    $message_contact.=$message_footer;

                    $mail3->AddAddress($point_of_contact_email, $point_of_contact);
                    $mail3->Subject = 'Intel Customer Connect: New Account Request';
                    $mail3->MsgHTML($message_contact);
                    $mail3->AltBody = 'The following person has requested an account for Intel Customer Connect.';
                    $mail3->Send();
                }
            }
        }

        //Send the message, check for errors
        //if(!$mail2->Send()) {
        //    echo 'Sorry, there was a problem sending a confirmation email, but your account has been created. We will email you a confirmation shortly.';
        //    die();
        //}

    	echo 'Thank you for requesting access, you\'ll receive an email once you\'ve been approved. <a href="" data-dismiss="modal">Close window</a>';
    } else {
        echo 'There was a problem capturing your data. Please <a href="mailto:support@intelcustomerconnect.com">contact us</a> for support.'; 
    }

    /* close connection */
    $mysqli->close();
} else {
    echo 'There has been a problem.';  
}
?>