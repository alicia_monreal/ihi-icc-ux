<?php include('includes/validate.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Intel Customer Connect</title>
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico"/>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico"/>

    <link href="css/bootstrap.css" rel="stylesheet">

    <!--jQuery-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> <!--1.8.3-->
    <script>window.jQuery || document.write('<script src="js/jquery.min.1.9.1"><\/script>')</script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <link rel="stylesheet" href="css/calendar.css">
    <link rel="stylesheet" href="css/bootstrap-editor.css">
    <link rel="stylesheet" href="css/bootstrap-wysihtml5-0.0.2.css">
    <link rel="stylesheet" href="css/address.css">
    <link rel="stylesheet" href="css/aloha.css" type="text/css">
    <!-- IHI CSS files  -->
    <link rel="stylesheet" href="css/styles.css?ver=9">
    <link rel="stylesheet" href="css/templates.css?ver=`">
    <link rel="stylesheet" href="css/editor.css?ver=`">


    <script src="js/editor/jquery.mockjax.js"></script>
    <script src="js/editor/moment.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/editor/bootstrap-datetimepicker.js"></script>
    <script src="js/editor/bootstrap-editable.js"></script>
    <script src="js/editor/wysihtml5-0.3.0.min.js"></script>
    <script src="js/editor/bootstrap-wysihtml5-0.0.2.min.js"></script>
    <script src="js/editor/wysihtml5.js"></script>
    <script src="js/editor/address.js"></script>


    <!-- Editor Script  -->
    <script src="js/editor/editor.js"></script>
    <script type="text/javascript" src="js/editor/tinymce/tinymce.min.js"></script>

    <!-- Customize global script params -->
    <script>
        var tplEditor   = null;
        var tplInjector = null;

        $(document).ready(function() {
            tplEditor          = new templateEngine();
            tplEditor.asyncLoad();
            tplInjector        = new templateInjector();
        });
    </script>
</head>
