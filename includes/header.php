<?php
$class_active = ' class="active"';
?>
<div id="wrap">
    <div class="auxbar">
		<div class="container">
		  <div class="logo pull-left"><a href="http://www.intel.com" target="_blank" data-analytics-label="Header: Intel External"><img src="img/logo-intel-header.png" alt="Intel" id="intellogo"></a></div>
		  <div class="share pull-right"><?php if ($loc != 'login') { ?><div class="tab pull-left welcome">Welcome, <?php echo $_SESSION['fname']; ?></div><div class="tab pull-left<?php if ($loc == 'my-account') { echo ' active'; } ?>"><a href="my-account" data-analytics-label="Header: My Account">My Account</a></div><div class="tab pull-left"><a href="log-out" data-analytics-label="Header: Log Out">Log Out</a></div><?php } ?><div class="tab pull-left"><a <?php if ($loc != 'login') { echo 'href="support"'; } else { echo 'href="#contact-us" data-toggle="modal"'; } ?> data-analytics-label="Header: Support">Support</a></div></div>
		</div>
        <input class="session-user-id" type="hidden" value="<?php echo $_SESSION['user_id']?>" >
    </div>
    <div class="topnav<?php if ($loc == 'login') { echo ' nomenu'; } if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { echo ' adminmenu'; } ?>">
        <div class="container">
			<div class="logo-update-center pull-left"><a href="/home" data-analytics-label="Top Nav: Intel Home"><img src="img/logo-customer-connect.png" alt="Customer Connect"></a></div>
			<?php if ($loc != 'login') { ?><div class="links"><a href="home" data-analytics-label="Top Nav: Home"<?php if ($loc == 'home') { echo $class_active; } ?>>Home</a> <a href="solutions" data-analytics-label="Top Nav: Solutions"<?php if ($loc == 'solutions') { echo $class_active; } ?>>Solutions</a> <!--<a href="training" data-analytics-label="Top Nav: Training"<?php if ($loc == 'training') { echo $class_active; } ?>>Training</a> --><a href="resources" data-analytics-label="Top Nav: Resources"<?php if ($loc == 'resources') { echo $class_active; } ?>>Resources</a> <a href="calendar" data-analytics-label="Top Nav: Calendar"<?php if ($loc == 'calendar') { echo $class_active; } ?>>Calendar</a> <a href="request" data-analytics-label="Top Nav: Submit Request"<?php if ($loc == 'campaign-requests') { echo $class_active; } ?>>Submit Request</a><?php } ?>
			<?php if ($loc != 'login') { ?>
                <?php if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {  //Admins have second toolbar and special link up top ?>
                <a href="requests" data-analytics-label="Top Nav: My Requests"<?php if ($loc == 'requests') { echo $class_active; } ?>>My Requests</a> <a href="all-requests" data-analytics-label="Top Nav: All Requests"<?php if ($loc == 'all-requests') { echo $class_active; } ?>>All Requests</a>
                <?php } ?>
                </div>
			<?php } ?>
        </div>
        <?php if ($loc != 'login' && (isset($_SESSION['admin']) && $_SESSION['admin'] == 1)) { //Admins have second toolbar ?>
        <div class="links-admin">
        	<div class="container">
        		<!--<a href="dashboard" data-analytics-label="Admin Nav: Dashboard"<?php if ($loc == 'dashboard') { echo $class_active; } ?>>Dashboard</a> --><a href="manage-users" data-analytics-label="Admin Nav: Manage Users"<?php if ($loc == 'manage-users') { echo $class_active; } ?>>Manage Users</a> <a href="requests" data-analytics-label="Top Nav: Manage Requests"<?php if ($loc == 'requests') { echo $class_active; } ?>>Manage Requests</a> <a href="all-requests" data-analytics-label="Top Nav: All Requests"<?php if ($loc == 'all-requests') { echo $class_active; } ?>>All Requests</a> <a href="campaigns" data-analytics-label="Top Nav: Manage Campaigns"<?php if ($loc == 'campaigns') { echo $class_active; } ?>>Manage Campaigns</a> <a href="manage-resources" data-analytics-label="Top Nav: Manage Resources"<?php if ($loc == 'manage-resources') { echo $class_active; } ?>>Manage Resources</a> <a href="notification-rules" data-analytics-label="Top Nav: Notification Rules"<?php if ($loc == 'notification-rules') { echo $class_active; } ?>>Notification Rules</a>
        	</div>
        </div>
        <?php } ?>
    </div>