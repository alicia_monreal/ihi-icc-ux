<?php
session_start();
require_once('validate.php');
include('_globals.php');
include('notification_rules_email.php');

if (!empty($_POST) && !empty($_POST['action'])) {

    //STEP 0
    $user_id = $_POST['user_id'];
    $user_email = $_POST['user_email'];
    $request_type = 4;
    $campaign_id = $_SESSION['campaign_id'];

    //Step 2. Saving values to session data
    if ($_POST['step'] == 2) {

        //And now let's send this all into a series of tables.
        //$language = $_SESSION['language']; //removed req by Hubert. 0324
        $marketing_id = $_SESSION['marketing_id'];


        //STEP 2
        if ($_POST['associated_tactic'] == '' || $_POST['associated_tactic'] == 'None') {
            $associated_tactic = '';
        } else {
            $associated_tactic = $mysqli->real_escape_string($_POST['associated_tactic']);
        }
        $tag = $mysqli->real_escape_string($_POST['tag']);
        $list_request_type = $mysqli->real_escape_string($_POST['list_request_type']);
        $files_contacts = array();
        foreach ($_POST['files_contacts'] as $val_contacts) {
            array_push($files_contacts, $val_contacts);
        }
        
        //First we create a new master REQUEST and then we put it in the individual request table.

        /////////////////////////////////////////////////////////////////////////////////////
        //Create a new request
        $query_request = "INSERT INTO request VALUES (
                        NULL,
                        '".$user_id."',
                        '',
                        '',
                        '".$request_type."',
                        '".$campaign_id."',
                        NULL,
                        NULL,
                        NULL,
                        '0',
                        '1',
                        '1',
                        NULL,
                        '".$user_id."',
                        NOW(),
                        NOW(),
                        NOW(),
                        '1')";

        $mysqli->query($query_request);

        if ($mysqli->insert_id) {
            //New request ID
            $request_id = $mysqli->insert_id;
            $name = $list_request_type.' List.'.$_POST['tag'];
            $name = $mysqli->real_escape_string($name);
            $eloqua_source_id = createEloquaID($campaign_id, $name, $request_id);
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);

            /////////////////////////////////////////////////////////////////////////////////////
            //Create a new individual request
            $query = "INSERT INTO request_contactlist VALUES (
                            NULL,
                            '".$user_id."',
                            '".$request_id."',
                            '".$marketing_id."',
                            '".$eloqua_source_id."',
                            '".$associated_tactic."',
                            '".$name.".".$request_id."',
                            '".$tag."',
                            '".$list_request_type."',
                            '1',
                            '1')";

            $mysqli->query($query);


            /////////////////////////////////////////////////////////////////////////////////////
            //Input the assets

            //Contacts
            uploadAssets($files_contacts, 'Contacts', $request_id, $user_id);


            /////////////////////////////////////////////////////////////////////////////////////
            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);

            /////////////////////////////////////////////////////////////////////////////////////
            //Send email to Intel
            $name = cleanForEmail($name);
            $tag = cleanForEmail($tag, 1);

            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new contact list has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">".$name.".".$request_id."<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            //$mail->AddCC('dianex.c.schroeder@intel.com', 'Diane Schroeder'); //Special person
            //mail->AddCC('seamusx.james@intel.com', 'Seamus James'); //Special person
            //Special for Doug Marshall and Kristi
            //if ($user_id == 365) { $mail->AddCC('kristi@ironhorseinteractive.com', 'Kristi Teplitz'); }
            
            /* Send notification rules email to assignees */
            $mail = sendNotificationRulesEmail($mail,$request_type,$campaign_id, $request_id, $mysqli, $message_header,$message_footer, $domain, $mail3); 
            
            $mail->Subject = 'Intel Customer Connect: New Contact List: '.$tag.'.'.$request_id;
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/request-detail?id='.$request_id.'';
            $mail->Send();

            
            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Intel Customer Connect: Contact List Received: '.$tag.'.'.$request_id;
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();

            //Finally, let's let 'em know
            header('Location:/request-detail?id='.$request_id.'&action=success');

        }
    }

} else {
    header('Location:request?action=failure'); 
}
?>