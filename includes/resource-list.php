<?php
@session_start();
include('_globals.php');

$query = "SELECT 
                resources.*,
				DATE_FORMAT( resources.publish_date,  '%m/%d/%Y' ) AS publish_date,
                DATE_FORMAT( resources.date_created,  '%m/%d/%Y' ) AS date_created,
                resource_type.name AS resource_type_name
			FROM resources
            LEFT OUTER JOIN resource_type
            ON resources.type = resource_type.id";


if (isset($_GET['id']) && $_SESSION['admin'] == 1) {
	//SHOWING SINGLE RECORD

	$query.=" WHERE resources.id = '".$_GET['id']."' LIMIT 1";
				
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    //print_r($query);

    if ($row_cnt > 0) {
        echo '<div class="single-request single-request-resource">';

        //Show a message?
        if (isset($_GET['update'])) {
        	if ($_GET['update'] == 'success') {
        		echo '<p style="color:#ff0000;"><strong>Resource has been updated.</strong><br /></p>';
        	} else if ($_GET['update'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>We\'re sorry, there has been a problem updating this resource.</strong><br /></p>';
            }
        }
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>Resource has been created.</strong><br /></p>';
            }
        }

        while ($obj = $result->fetch_object()) {
            $current_resource_id = $_GET['id'];

        	//Admins get a form
	        if ($_SESSION['admin'] == 1) {
	        	echo '<form id="resource-update-form" action="includes/_resource-admin.php?id='.$current_resource_id.'&action=update" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="update">
					<input type="hidden" id="resource_id" name="resource_id" value="'.$current_resource_id.'">
					<input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">';
			}
        	echo '<div id="zebra">';
            echo '<div class="row"><label>Title</label> <div class="pull-left"><input type="text" id="title" name="title" placeholder="Title" value="'.$obj->title.'" class="input-xlarge required"></div></div>';
            echo '<div class="row"><label>Description</label> <div class="pull-left"><textarea id="description" name="description" placeholder="Description">'.$obj->description.'</textarea></div></div>';
            echo '<div class="row"><label>Type</label> <div class="pull-left">';
            //Type
            if ($_SESSION['admin'] == 1) {
                echo '<select name="type" id="type" class="required"><option value="">Select resource type</option>';
                    $query_type = "SELECT * FROM resource_type WHERE active = 1 ORDER BY name ASC";
                    $result_type = $mysqli->query($query_type);
                    while ($obj_type = $result_type->fetch_object()) {
                        echo '<option value="'.$obj_type->id.'"'; if ($obj->type == $obj_type->id) { echo ' selected'; } echo '>'.$obj_type->name.'</option>';
                    }
                echo '</select>';
            } else {
                $query_type = "SELECT * FROM resource_type WHERE id = $obj->type";
                $result_type = $mysqli->query($query_type);
                while ($obj_type = $result_type->fetch_object()) {
                    echo $obj_type->name;
                }
            }
            echo '</div></div>';
            echo '<div class="row"><label>Content Access Control</label> <div class="pull-left">';

            //Access Control
            $query_access = "SELECT * FROM access_control WHERE active = 1 ORDER BY id ASC";
            $result_access = $mysqli->query($query_access);
            $access_control_arr = explode(", ", $obj->access_control);
            while ($obj_access = $result_access->fetch_object()) {
                echo '<label class="checkbox inline"> <input type="checkbox" id="access_control[]" name="access_control[]" value="'.$obj_access->id.'"'; if (in_array($obj_access->id, $access_control_arr)) { echo 'checked'; } echo '> '.$obj_access->name.'</label>';
            }
            echo '</div></div>';
            //echo '<div class="row"><label>Thumbnail</label> <div class="pull-left">&nbsp;</div></div>';
            echo '<div class="row"><label>Publish Date</label> <div class="pull-left"><div id="publish_datepicker" class="input-append date" style=""><input type="text" id="publish_date" name="publish_date" class="input-medium required" value="'.$obj->publish_date.'"><span class="add-on"><i class="icon-th"></i></span></div></div></div>';
            echo '<div class="row"><label>URL</label> <div class="pull-left"><input type="text" id="link" name="link" placeholder="URL" value="'.$obj->link.'" class="input-xlarge"><br />
                <label class="radio inline"> <input type="radio" id="internal[]" name="internal" value="0"'; if ($obj->internal == 0) { echo 'checked'; } echo '> External</label>
                <label class="radio inline"> <input type="radio" id="internal[]" name="internal" value="1"'; if ($obj->internal == 1) { echo 'checked'; } echo '> Internal (select if Vimeo or local file)</label>
            </div></div>';

            echo '<div class="row"><label>Internal File</label> <div class="pull-left">
                    <div class="upload-div">
                        <div id="resource" class="fileupload fileupload-new" data-provides="fileupload">
                            <span class="btn-file btn-green add"><span class="fileupload-new">Upload New Resource File</span><input id="fileupload_resource" type="file" name="files[]"></span>
                        </div>
                        <div id="progress-resource" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 248px;width:280px;display:none;">
                            <div class="bar"></div>
                        </div>
                        <div id="shown-resource" class="files" style="clear:both;"></div>
                    </div>
            </div></div>';

            echo '<div class="row"><label>Locations</label> <div class="pull-left">';

            //Locations
            $locations_arr = explode(", ", $obj->locations);
            echo '<div class="location-cols">
                        <div class="location pull-left">
                            <strong>Homepage</strong>
                            <ul>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="1"'; if (in_array(1, $locations_arr)) { echo 'checked'; } echo '> Resources</label></li>
                                <!--<li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="2"'; if (in_array(2, $locations_arr)) { echo 'checked'; } echo '> Latest Updates</label></li>-->
                            </ul>
                        </div>
                        <div class="location pull-left">
                            <strong>Solutions</strong>
                            <ul>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="3"'; if (in_array(3, $locations_arr)) { echo 'checked'; } echo '> Webinars</label></li>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="4"'; if (in_array(4, $locations_arr)) { echo 'checked'; } echo '> Marketing Automation</label></li>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="5"'; if (in_array(5, $locations_arr)) { echo 'checked'; } echo '> Partner Sales</label></li>
                            </ul>
                        </div>
                        <div class="location pull-left">
                            <strong>Resources</strong>
                            <ul>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="6"'; if (in_array(6, $locations_arr)) { echo 'checked'; } echo '> Webinars</label></li>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="7"'; if (in_array(7, $locations_arr)) { echo 'checked'; } echo '> Marketing Automation</label></li>
                                <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="8"'; if (in_array(8, $locations_arr)) { echo 'checked'; } echo '> Partner Sales</label></li>
                            </ul>
                        </div>
                    </div>';
			echo '</div></div>';
            echo '</div>'; //End Zebra
            echo '</div>';
        }

	} else {
		echo '<p><strong>Resource not found.</p>';
	}

	echo '<p><br /><a href="manage-resources" class="btn-orange back pull-left">Back to list</a>';
	if ($_SESSION['admin'] == 1) {
		echo '<input type="submit" class="btn-green submit submit-btn pull-right" data-analytics-label="Submit Form: Update Resource" value="Update Resource"></p></form></div>';
	} else {
		echo '</p></div>';
	}

} else if (isset($_GET['action']) && $_SESSION['admin'] == 1) {
    //CREATING A NEW RESOURCE

    echo '<div class="single-request single-request-resource">';

    echo '<form id="resource-update-form" action="includes/_resource-admin.php?action=create" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="action" name="action" value="create">
        <input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">';

    echo '<div id="zebra">';
    echo '<div class="row row-border"><label>Title</label> <div class="pull-left"><input type="text" id="title" name="title" placeholder="Title" value="" class="input-xlarge required"></div></div>';
    echo '<div class="row"><label>Description</label> <div class="pull-left"><textarea id="description" name="description" placeholder="Description"></textarea></div></div>';
    echo '<div class="row"><label>Type</label> <div class="pull-left">';
    //Type
    echo '<select name="type" id="type" class="required"><option value="">Select resource type</option>';
        $query_type = "SELECT * FROM resource_type WHERE active = 1 ORDER BY name ASC";
        $result_type = $mysqli->query($query_type);
        while ($obj_type = $result_type->fetch_object()) {
            echo '<option value="'.$obj_type->id.'">'.$obj_type->name.'</option>';
        }
    echo '</select>';
    echo '</div></div>';
    echo '<div class="row"><label>Content Access Control</label> <div class="pull-left">';

    //Access Control
    $query_access = "SELECT * FROM access_control WHERE active = 1 ORDER BY id ASC";
    $result_access = $mysqli->query($query_access);
    $access_control_arr = explode(", ", $obj->access_control);
    while ($obj_access = $result_access->fetch_object()) {
        echo '<label class="checkbox inline"> <input type="checkbox" id="access_control[]" name="access_control[]" value="'.$obj_access->id.'"> '.$obj_access->name.'</label>';
    }
    echo '</div></div>';
    //echo '<div class="row"><label>Thumbnail</label> <div class="pull-left">&nbsp;</div></div>';
    echo '<div class="row"><label>Publish Date</label> <div class="pull-left"><div id="publish_datepicker" class="input-append date" style=""><input type="text" id="publish_date" name="publish_date" class="input-medium required" value=""><span class="add-on"><i class="icon-th"></i></span></div></div></div>';
    echo '<div class="row"><label>URL</label> <div class="pull-left"><input type="text" id="link" name="link" placeholder="URL" value="" class="input-xlarge"><br />
        <label class="radio inline"> <input type="radio" id="internal[]" name="internal" value="0" checked> External</label>
        <label class="radio inline"> <input type="radio" id="internal[]" name="internal" value="1"> Internal (select if Vimeo or local file)</label>
    </div></div>';
    echo '<div class="row"><label>Internal File</label> <div class="pull-left">
                    <div class="upload-div">
                        <div id="resource" class="fileupload fileupload-new" data-provides="fileupload">
                            <span class="btn-file btn-green add"><span class="fileupload-new">Upload a Resource</span><input id="fileupload_resource" type="file" name="files[]"></span>
                        </div>
                        <div id="progress-resource" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 205px;display:none;width:250px;">
                            <div class="bar"></div>
                        </div>
                        <div id="shown-resource" class="files" style="clear:both;"></div>
                    </div>
            </div></div>';

    echo '<div class="row"><label>Locations</label> <div class="pull-left">';

    //Locations
    echo '<div class="location-cols">
                <div class="location pull-left">
                    <strong>Homepage</strong>
                    <ul>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="1"> Resources</label></li>
                        <!--<li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="2"> Latest Updates</label></li>-->
                    </ul>
                </div>
                <div class="location pull-left">
                    <strong>Solutions</strong>
                    <ul>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="3"> Webinars</label></li>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="4"> Marketing Automation</label></li>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="5"> Partner Sales</label></li>
                    </ul>
                </div>
                <div class="location pull-left">
                    <strong>Resources</strong>
                    <ul>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="6"> Webinars</label></li>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="7"> Marketing Automation</label></li>
                        <li><label class="checkbox inline"> <input type="checkbox" id="locations[]" name="locations[]" value="8"> Partner Sales</label></li>
                    </ul>
                </div>
            </div>';
    echo '</div></div>';
    echo '</div>'; //End Zebra
    echo '</div>';

    echo '<p><br /><a href="manage-resources" class="btn-orange back pull-left">Back to list</a>';
    echo '<input type="submit" class="btn-green submit submit-btn pull-right" data-analytics-label="Submit Form: Create Resource" value="Create Resource"></p></form></div>';

} else {
	//SHOWING FULL LIST
    //Show a message?
    if (isset($_GET['message'])) {
        if ($_GET['message'] == 'created') {
            echo '<p style="color:#ff0000;"><strong>Resource has been created.</strong><br /></p>';
        } else if ($_GET['message'] == 'error') {
            echo '<p style="color:#ff0000;"><strong>Sorry, there has been an error. Please contact support.</strong><br /></p>';
        } else if ($_GET['message'] == 'updated') {
            echo '<p style="color:#ff0000;"><strong>Resource has been updated.</strong><br /></p>';
        } else if ($_GET['message'] == 'deleted') {
            echo '<p style="color:#ff0000;"><strong>Resource has been deleted.</strong><br /></p>';
        }
    }

	$query.=" WHERE resources.active = 1 ORDER BY resources.publish_date ASC";
				
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    //print_r($query);
    //die();

    if ($row_cnt > 0) {
    	echo '<br /><table class="table table-striped table-bordered table-hover tablesorter requests-table">';
    	echo '<thead><th>Publish Date <b class="icon-white"></b></th><th>Title <b class="icon-white"></b></th><th>Action <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th><!--<th>Location(s)</th>--><th>Admin</th></thead>';
    	echo '<tbody>';
        while ($obj = $result->fetch_object()) {
            if ($obj->active == 1) { $status = 'Active'; } else { $status = '<em style="color:#ff0000;">Inactive</em>'; }

        	echo '<tr>';
            echo '<td>'.$obj->publish_date.'</td>';
            echo '<td>'.$obj->title.'</td>';
            echo '<td>';
            switch ($obj->type) {
                case 2:
                    echo '<a href="'.$obj->link.'" target="_blank">View</a>';
                    break;
                case 3:
                    echo '<a href="'.$obj->link.'" target="_blank">Download</a>';
                    break;
                case 4:
                    echo '<a href="'.$obj->link.'" target="_blank">Download</a>';
                    break;
                case 5:
                    echo '<a href="'.$obj->link.'" target="_blank">Download</a>';
                    break;
                case 1:
                    if ($obj->internal == 1) {
                        echo '<a class="vimeo cboxElement video-link" href="'.$obj->link.'" title="'.$obj->title.'">Watch</a>';
                    } else {
                        echo '<a href="'.$obj->link.'" target="_blank">Watch</a>';
                    }
                    break;
            }
            echo '</td>';
            echo '<td>'.$obj->resource_type_name.'</td>';
            echo '<td>'.$status.'</td>';
            //echo '<td>&nbsp;</td>'; //Locations, takes up too much height
            echo '<td><a href="manage-resources?id='.$obj->id.'"><span class="icon-edit" style="margin-top:4px;"></span></a> / <a href="" onclick="deleteResource(\'includes/_resource-admin.php?id='.$obj->id.'&action=delete\');return false;"><span class="icon-remove" style="margin-top:4px;"></span></a></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
	} else {
		echo '<p><strong>There are currently no resources.</strong></p>';
	}
}

?>