<?php
session_start();
include('_globals.php');

function verifyFormToken($form) {
    if(!isset($_SESSION[$form.'_token'])) { 
        return false;
    }
    if(!isset($_POST['token'])) {
        return false;
    }
    if ($_SESSION[$form.'_token'] !== $_POST['token']) {
        return false;
    }
    
    return true;
}

if (!empty($_POST) && isset($_SESSION['user_id']) && verifyFormToken('account-update-form')) {
    $fname = $mysqli->real_escape_string($_POST['fname']);
    $lname = $mysqli->real_escape_string($_POST['lname']);
    $email = $mysqli->real_escape_string($_POST['email']);
    $title = $mysqli->real_escape_string($_POST['title']);
    $company = $mysqli->real_escape_string($_POST['company']);
    $point_of_contact = $mysqli->real_escape_string($_POST['point_of_contact']);
    $point_of_contact_email = $mysqli->real_escape_string($_POST['point_of_contact_email']);
    $password = $mysqli->real_escape_string($_POST['password']);

    $query = "UPDATE users SET
                    fname = '".$fname."',
                    lname = '".$lname."',
                    email = '".$email."',
                    title = '".$title."',
                    company = '".$company."',
                    point_of_contact = '".$point_of_contact."',
                    point_of_contact_email = '".$point_of_contact_email."',";
                    if ($password != '') {
                        $query.=" password = '".$password."',";
                    }
                    $query.=" date_updated = NOW()
                WHERE users.id = ".$_SESSION['user_id']."
                LIMIT 1";
    $result = $mysqli->query($query);

    //print_r($query);
    //die();

    if ($result) {
        $_SESSION['fname'] = $_POST['fname'];
        $_SESSION['lname'] = $_POST['lname'];
        $_SESSION['email'] = $_POST['email'];
        $_SESSION['title'] = $_POST['title'];
        $_SESSION['company'] = $_POST['company'];
        $_SESSION['point_of_contact'] = $_POST['point_of_contact'];
        $_SESSION['point_of_contact_email'] = $_POST['point_of_contact_email'];

        //Going to reset the Remember Me cookie
        /*if (strlen($_COOKIE['rememberme']) > 0) {
            //setcookie('rememberme', $_POST['email'], time()+31536000, '/');

            echo 'no';
            die();
        } else {
            //setcookie('rememberme', '', time()-3600, '/');

            echo 'ok';
            die();
        }*/

        $mysqli->close();

        header('Location:/my-account?success=true');
    } else {
        $mysqli->close();
        header('Location:/my-account?success=false-insert');
    }

} else {
    header('Location:/my-account?success=false');
    die();
}
?>