<?php
session_start();
include('_globals.php');

function verifyFormToken($form) {
    if(!isset($_SESSION[$form.'_token'])) { 
        return false;
    }
    if(!isset($_POST['token'])) {
        return false;
    }
    if ($_SESSION[$form.'_token'] !== $_POST['token']) {
        return false;
    }
    
    return true;
}

if (!empty($_POST) && isset($_SESSION['user_id']) && verifyFormToken('survey-form')) {
    $request_id = $mysqli->real_escape_string($_POST['request_id']);
    $q1 = $_POST['q1'];
    $q2 = $_POST['q2'];
    $q3 = $_POST['q3'];
    $q4 = $_POST['q4'];
    $comments = $mysqli->real_escape_string($_POST['comments']);

    //Calculate average
    $total = number_format($q1+$q2+$q3+$q4);
    $average = $total/4;

    //First, make sure this survey hasn't already been submitted
    $query_sur = "SELECT * FROM surveys WHERE request_id = '".$request_id."' LIMIT 1";
    $result_sur = $mysqli->query($query_sur);
    $row_cnt_sur = $result_sur->num_rows;

    if ($row_cnt_sur > 0) { //Survey has already been submitted
        $mysqli->close();
        header('Location:/survey?id='.$request_id.'&success=false');
        die();
    } else {
        $query = "INSERT INTO surveys (`id`, `request_id`, `user_id`, `promptness`, `timelines`, `outcome`, `overall`, `comments`, `average`, `date_created`, `date_updated`, `active`) VALUES (NULL, '".$request_id."', '".$_SESSION['user_id']."', '".$q1."', '".$q2."', '".$q3."', '".$q4."', '".$comments."', '".$average."', NOW(), NOW(), '1');";
        $result = $mysqli->query($query);

        if ($result) {
            $mysqli->close();
            header('Location:/survey?id='.$request_id.'&success=true');
            die();
        } else {
            $mysqli->close();
            header('Location:/survey?id='.$request_id.'&success=false');
            die();
        }
    }

} else {
    header('Location:/home');
    die();
}
?>