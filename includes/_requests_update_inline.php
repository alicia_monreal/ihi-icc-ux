<?php
session_start();
require_once('validate.php');
include('_globals.php');

//Debug
//header('HTTP 400 Bad Request', true, 400);

//Arrays
$arrays = array('topics_covered','vertical','geo');

//The values from the update
$pk = $_POST['pk'];
$name = $_POST['name'];

//Handle arrays
if (in_array($name, $arrays)) {
    $the_array = array();
    foreach($_POST['value'] as $val_array) {
        array_push($the_array, $val_array);
    }
    $value = implode(", ",$the_array);
} else {
    $value = $mysqli->real_escape_string($_POST['value']);
}

//What table are we editing?
$query_table = getTacticTable($_GET['request_type_id']);

//Make sure something was updated
if(!empty($value)) {
    //Fix for not being allowed to use "0"
    if ($value == 9999) { $value = 0; }

    //What table are we updating?
    if (isset($_GET['type']) && $_GET['type'] == 'notes') { //Notes
        $query_notes = "INSERT INTO notes VALUES (NULL, '".$pk."', '".$_SESSION['user_id']."', NULL, '".$value."', NOW(), '1')";
        $result_notes = $mysqli->query($query_notes);
    } else { //Catch-all
        $query = "UPDATE ".$query_table." SET ".$name." = '".$value."' WHERE request_id = '".$pk."' LIMIT 1";
        $result = $mysqli->query($query);

        $query_notes = "INSERT INTO notes VALUES (NULL, '".$pk."', '".$_SESSION['user_id']."', '".$name."', 'Value updated', NOW(), '1')";
        $result_notes = $mysqli->query($query_notes);
    }
} else {
    header('HTTP 400 Bad Request', true, 400);
    echo "This field is required";
}
?>