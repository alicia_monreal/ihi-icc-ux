<?php
//New logic for ITP and webinars
$email_from_name = getFieldValue('email_from_name', $_SESSION['clone_id'], $_SESSION['request_type'], '');
$email_from_address = getFieldValue('email_from_address', $_SESSION['clone_id'], $_SESSION['request_type'], '');
$email_reply_name = getFieldValue('email_reply_name', $_SESSION['clone_id'], $_SESSION['request_type'], '');
$email_reply_address = getFieldValue('email_reply_address', $_SESSION['clone_id'], $_SESSION['request_type'], '');

if ($_SESSION['request_type'] == 1) {
	if ($email_from_name == '' && $_SESSION['ITP'] == 1) { $email_from_name = 'Intel Webinars'; }
	if ($email_from_address == '' && $_SESSION['ITP'] == 1) { $email_from_address = 'Intel-webinars@regsvc.com'; }
	if ($email_reply_name == '' && $_SESSION['ITP'] == 1) { $email_reply_name = 'Intel Webinars'; }
	if ($email_reply_address == '' && $_SESSION['ITP'] == 1) { $email_reply_address = 'Intel-webinars@regsvc.com'; }
}
?>

<div class="control-group">
	<label for="email_from_name" class="control-label">From Name</label>
	<div class="controls"><input type="text" id="email_from_name" name="email_from_name" class="input-wide" placeholder="Will be used as the from name in all emails. Example: Intel Corporation" value="<?php echo $email_from_name; ?>"><a href="img/email-from.jpg" class="cbox-image" title="From Name"><i class="helper-icon icon-picture"></i></a></div>
</div>
<div class="control-group">
	<label for="email_from_address" class="control-label">From Address</label>
	<div class="controls"><input type="text" id="email_from_address" name="email_from_address" class="input-wide" placeholder="Will be used as the from address. Example: donotreply@intel.com" value="<?php echo $email_from_address; ?>"><a href="img/email-from-email.jpg" class="cbox-image" title="From Address"><i class="helper-icon icon-picture"></i></a></div>
</div>
<div class="control-group">
	<label for="email_reply_name" class="control-label">Reply Name</label>
	<div class="controls"><input type="text" id="email_reply_name" name="email_reply_name" class="input-wide" placeholder="Will be used as the reply name if the recipient responds to the email" value="<?php echo $email_reply_name; ?>"><a href="img/email-reply-to.jpg" class="cbox-image" title="Reply Name"><i class="helper-icon icon-picture"></i></a></div>
</div>
<div class="control-group">
	<label for="email_reply_address" class="control-label">Reply Address</label>
	<div class="controls"><input type="text" id="email_reply_address" name="email_reply_address" class="input-wide" placeholder="Will be used as the email address if the recipient responds to the email" value="<?php echo $email_reply_address; ?>"><a href="img/email-reply-to-email.jpg" class="cbox-image" title="Reply Address"><i class="helper-icon icon-picture"></i></a></div>
</div>