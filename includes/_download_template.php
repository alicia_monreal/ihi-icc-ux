<?php

session_start();
require_once('validate.php');
include('_globals.php');

// if html content exists
if (isset($_REQUEST['content'])) {

		$_REQUEST['content'] = str_replace(array('<p>','</p>'), '', $_REQUEST['content']);  // replace <p> tags with space

		if($_REQUEST['template_type'] == "page") {
			$_REQUEST['content'] = preg_replace('#<div class="button-move preview">(.*?)</div>#', '', $_REQUEST['content']);
			$_REQUEST['content'] = preg_replace('#<div class="button-remove preview">(.*?)</div>#', '', $_REQUEST['content']);
		}	

		// generating an html file
		header('Content-disposition: attachment; filename=test');
		header('Content-type: text/html');

		if($_REQUEST['template_type'] == "email") { // if template type is email
			echo '<style type="text/css">
			body { 
			    width: 100% !important;
			    -webkit-text-size-adjust: none; 
			        -ms-text-size-adjust: none; 
			    margin: 0; 
			    padding: 0;
			}	
			</style>';
		} else {  // template type is page
			echo '<link rel="stylesheet" href="http://images.plan.intel.com/Web/IntelCorporation/{051028b6-5d44-4040-bafc-bf669ebf6d73}_style-072814.css">
	
			<!--[if lte IE 9]>
				<script src="http://images.plan.intel.com/Web/IntelCorporation/{c7c4d720-f522-4609-8326-c83ac1d43a74}_html5shiv.js"></script>
			<![endif]-->

			<style type="text/css">
				.content-wrapper {
					padding:0px !important;
				}
				.hide {
				    display: none;
				}
			</style>';
		}

		echo'</head>

		<body bgcolor="#dcdcdc" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" mobile="fix">
		'.$_REQUEST['content'].'</body></html>';
		
		exit;

} else {

	header("Location:/");
	die();
}

?>