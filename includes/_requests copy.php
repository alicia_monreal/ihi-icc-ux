<?php
session_start();
include('_globals.php');

if (!empty($_POST) && !empty($_POST['action'])) {

    //Create new request
    if ($_POST['action'] == 'create') {

        //Values
        $user_id = $_POST['user_id'];
        $user_email = $_POST['user_email'];
        $title = $mysqli->real_escape_string($_POST['title']);
        $guidelines = $mysqli->real_escape_string($_POST['guidelines']);
        $request_type = $mysqli->real_escape_string($_POST['request_type']);
        $timeline = $mysqli->real_escape_string($_POST['timeline']);
        if ($timeline == 'Defined') {
            $desired_date = $mysqli->real_escape_string($_POST['desired_date']);
            $desired_date_email = $mysqli->real_escape_string($_POST['desired_date']);
        } else {
            $desired_date = NULL;
            $desired_date_email = $timeline;
        }

        $query = "INSERT INTO requests VALUES (NULL, '".$user_id."', '".$title."', '".$guidelines."', '".$request_type."', '1', NULL, NULL, '".$timeline."', '".$desired_date."', NULL, NOW(), NOW(), NULL, '1')";
        $mysqli->query($query);

        if ($mysqli->insert_id) {
            //New request ID
            $request_id = $mysqli->insert_id;

            $assets_email = '';

            //Insert docs
            if ($_POST['docs']) {
                $doc_number = count($_POST['docs']);
                $doc_counter = 1;

                $query_docs = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                foreach ($_POST['docs'] as $doc) {
                    $query_docs.="(NULL, '".$request_id."', '".$user_id."', '".$doc."', '/server/php/files/".$user_id."/".$doc."', 'doc', NOW(), '1')";
                    $doc_counter++;
                    if ($doc_counter <= $doc_number) {
                        $query_docs.=", ";
                    }
                    $assets_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$doc.'">'.$doc.'</a><br />';
                }
                $query_docs.=";";
                $mysqli->query($query_docs);
            }

            //Insert assets
            if ($_POST['assets']) {
                $asset_number = count($_POST['assets']);
                $asset_counter = 1;

                $query_asset= "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";

                foreach ($_POST['assets'] as $asset) {
                    $query_asset.="(NULL, '".$request_id."', '".$user_id."', '".$asset."', '/server/php/files/".$user_id."/".$asset."', 'asset', NOW(), '1')";
                    $asset_counter++;
                    if ($asset_counter <= $asset_number) {
                        $query_asset.=", ";
                    }
                    $assets_email.='<a href="'.$domain.'/server/php/files/'.$user_id.'/'.$asset.'">'.$asset.'</a><br />';
                }
                $query_asset.=";";
                $mysqli->query($query_asset);
            }

            //Insert external assets
            if ($_POST['external_url']) {
                foreach ($_POST['external_url'] as $key => $value) {
                    if (!empty($value)) {
                        $value = strtolower($value);
                        if (strpos($value, "http") === 0) { $value = $value; } else { $value = 'http://'.$value; }

                        $query_ext.="INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
                        $query_ext.="(NULL, '".$request_id."', '".$user_id."', '".$mysqli->real_escape_string($_POST['external_title'][$key])."', '".$mysqli->real_escape_string($value)."', 'url', NOW(), '1');";
                        $mysqli->query($query_ext);
                        $query_ext= "";

                        if ($_POST['external_title'][$key] != '') {
                            $assets_email.='<a href="'.$value.'">'.$_POST['external_title'][$key].'</a> (URL)<br />';
                        } else {
                            $assets_email.='<a href="'.$value.'">'.$value.'</a> (URL)<br />';
                        }
                    }
                }
                
            }

            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);

            //Send email to MS
            //$guidelines_email = nl2br($guidelines, false); //trying original POST rather than escaped version
            $guidelines_email = nl2br($_POST['guidelines'], false);
            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new update request has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">Request: ".$title." (#".$request_id.")<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Date</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".date('m/d/Y')."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Description</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$guidelines_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Request Type</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$request_type."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Assets Submitted</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$assets_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Status</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">New Request</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Desired Date</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$desired_date_email."</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            $mail->Subject = 'Customer Connect: New Update Request';
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/requests?id='.$request_id.'';
            $mail->Send();


            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Customer Connect: Update Request Received';
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();

            header('Location:/requests?id='.$request_id.'&action=success');
        } else {
            header('Location:/content-update-request?action=failure');
        }

        /* close connection */
        $mysqli->close();
    }
    //END Create new request



    //Update a request
    if ($_POST['action'] == 'update') {

        //Values
        $user_id = $_POST['user_id'];
        $user_email = $_POST['user_email'];
        $admin_id = $_POST['admin_id'];
        $request_id = $_POST['request_id'];
        $request_type_update = $_POST['request_type_update'];
        $status = $_POST['status_update'];
        $notes = $mysqli->real_escape_string($_POST['notes_update']);

        //Only send an email if status has been updated
        $no_status_change = false;
        $query_chk = "SELECT status FROM requests WHERE id = ".$request_id." LIMIT 1";
        $result_chk = $mysqli->query($query_chk);
        while ($obj_chk = $result_chk->fetch_object()) {
            if ($obj_chk->status == $status) { //Status isn't being updated, just something else. Which means no email and no Date Completed update.
                $no_status_change = true;
            }
        }

        $query = "UPDATE requests SET request_type = '".$request_type_update."', status = '".$status."', date_updated = NOW()";
        if ($status == 4 && $no_status_change == false) {
            $query.=", date_completed = NOW()";

            //Also add a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request completed', NOW(), '1')";
            $mysqli->query($query_note);
        }
        $query.=" WHERE id = ".$request_id."";
        $result = $mysqli->query($query);

        if ($notes != '') {
            $query_notes = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', '".$admin_id."', '".$notes."', NOW(), '1')";
            $result_notes = $mysqli->query($query_notes);
        }

        //Send email user
        $message_user = $message_header;

        //Message based on Status
        $alt = '';
        $send = false;
        switch ($status) {
            case 1: //New request
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $alt = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
                $send = false; //Don't think we need to send if status goes back to original, but just in case
                break;
            case 2: //Request in review
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. It is now being reviewed. We will contact you if we have additional questions.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $alt = 'Thank you for submitting your request. It is now being reviewed. We will contact you if we have additional questions.';
                $send = false;
                break;
            case 3: //Request scheduled
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Your request has been scheduled and will be processed based on the standard SLA.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $alt = 'Your request has been scheduled and will be processed based on the standard SLA.';
                $send = false;
                break;
            case 4: //Request complete
                //$message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Your request is complete. Thank you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/requests?id=".$request_id."\" style=\"color:#1570a6;\">".$domain."/requests?id=".$request_id."</a></p>";
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">We want to thank you for using the Windows Embedded Website Update Center for your recent request. Our records indicate we have completed the request.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">As part of our effort to drive efficiencies <i>and</i> satisfaction for users of the Update Center, we ask that you please participate in a short survey to help us learn more about your experience.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Please take a minute to complete the survey. We have five short questions for you <a href=\"".$domain."/survey?id=".$request_id."\" style=\"color:#1570a6;\">here</a>.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you again.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The Windows Embedded website team</p>";
                $alt = 'Your request is complete. Thank you.';
                $send = true;
                break;
        }

        $message_user.=$message_footer;

        $mail2->AddAddress($user_email);
        $mail2->Subject = 'Customer Connect: Update Request Status (#'.$request_id.')';
        $mail2->MsgHTML($message_user);
        $mail2->AltBody = $alt;
        if ($send == true && $no_status_change == false) {
            $mail2->Send();
        }

        if ($result) {
            header('Location:/requests?id='.$request_id.'&update=success');
        } else {
            header('Location:/requests?id='.$request_id.'&update=fail');
        }

        /* close connection */
        $mysqli->close();
    }
    //END Update a request

} else {
    header('Location:content-update-request?action=failure'); 
}
?>