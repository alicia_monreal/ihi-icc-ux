<?php
$loc = 'campaign-requests';
$step = 4;
$c_type = 'newsletter';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Newsletter Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fiveup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li>Step 3</li>
				<li class="active">Step 4</li>
				<li>Step 5</li>
			</ul>
			<div class="request-step step4">
				<div class="fieldgroup">
					<p>Please provide any necessary assets to be used with this tactic.  Assets must be provided 24 hours in advance before any asset creation period can begin.</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p>
					<h3 class="fieldgroup-header">Eloqua Templates</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_newsletter.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="4">
					<input type="hidden" id="c_type" name="c_type" value="newsletter">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="in_progress_form" name="in_progress_form" value="0">

					<div class="fieldgroup" style="border-bottom:0;">
						<div class="control-group">
							<label for="eloqua_templates" class="control-label no-padding">Do you want to use the Eloqua Templates?</label>
							<div class="controls">
								<label class="radio inline"> <input type="radio" id="eloqua_templates[]" name="eloqua_templates" value="1" <?php if (getFieldValue('eloqua_templates', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '1') { echo 'checked'; } ?>> Yes</label>
								<label class="radio inline"> <input type="radio" id="eloqua_templates[]" name="eloqua_templates" value="0" <?php if (getFieldValue('eloqua_templates', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('eloqua_templates', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo 'checked'; } ?>> No</label><br />
								<p id="eloqua-template-text" class="<?php if (getFieldValue('eloqua_templates', $_SESSION['clone_id'], $_SESSION['request_type'], '') != '1') { echo 'hide'; } ?>" style="margin-top:14px">A production support specialist will follow up with you regarding your template requirements.</p>
							</div>
						</div>
					</div>
					
					<div class="fieldgroup">
						<h3 class="fieldgroup-header">Tactic Assets</h3>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<label for="customassets" class="control-label no-padding">Tactic Assets</label>
							<div class="controls">
								<p>Please provide the html template and associated graphical assets for your newsletter. Please zip all assets into a single package and name each asset appropriately (naming should reflect asset location).</p>
								<div class="upload-div">
								    <div class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Asset Zip</span><input id="fileupload_customassets" type="file" name="files[]"></span>
									</div>
									<div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 239px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-customassets" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_customassets'])) {
								    		$filebox = '<div name="'. ($_SESSION['files_customassets']). '">';
								    		$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_customassets']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_customassets_path'].'">' . $_SESSION['files_customassets'] . '</a></p>';
						                    $filebox.= '</div>';
						                    echo $filebox;
            							}
            							?>
								    </div>
								</div>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-newsletter-3" class="btn-green back pull-left">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" style="margin-left:8px;">Next</a><a href="" id="save-and-exit" class="btn-green submit plain pull-right">Save &amp; Exit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>