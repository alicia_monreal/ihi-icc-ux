<?php
$loc = 'campaign-requests';
$step = 3;
$c_type = 'landingpage';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Landing Page Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fourup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li class="active">Step 3</li>
				<li>Step 4</li>
			</ul>
			<div class="request-step step3">


				<form id="content-update-form" class="form-horizontal" action="includes/_requests_landingpage.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="3">
					<input type="hidden" id="c_type" name="c_type" value="landingpage">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
					<input type="hidden" id="standard_template_type" name="standard_template_type" value="<?php echo getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>">

					<div class="fieldgroup" style="border-bottom:0;padding-top:10px;padding-bottom:10px;">
						<h3 class="fieldgroup-header">Choose template and layout configuration</h3>
					</div>

					<div class="fieldgroup" style="border-bottom:0;">
						<div class="control-group">
							<div class="row">
								<div id="template-standard-landingpage" class="template-selector gray-selector pull-left span6<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Standard Template') { echo ' active'; } ?>">
									<input type="radio" id="template[]" name="template" value="Standard Template"<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Standard Template') { echo ' checked'; } ?>>
									<img src="img/templates/intel-template-tmb.png" alt="Standard Template">
									<div style="width:190px;line-height:17px;" class="pull-right">
										<h4>Standard Template</h4>
										Use the approved HQ template for landing pages. You can choose a default layout, or customize one for your needs.
									</div>
								</div>
								<div id="template-custom" class="template-selector gray-selector pull-left span6<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' active'; } ?>">
									<input type="radio" id="template[]" name="template" value="Custom Template"<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' checked'; } ?> style="margin-top:51px;">
									<div style="width:388px;line-height:17px;" class="pull-right">
										<h4>Provide Your Own Template</h4>
										If your geo or group has its own template, or if your agency has already built out your assets for you, you can select this option to upload your own html content and image assets.
									</div>
								</div>
							</div>

							<div id="template-buttons" class="row<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' hide'; } ?>" style="margin-top:10px;">
								<div class="span6">
									<a href="templates-custom?type=landingpage" class="btn-green centered plain pull-left" style="width:400px;">Launch Asset Editor</a>
								</div>
							</div>

							<div id="custom-text" class="span12 row<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' hide'; } ?>" style="margin:20px 0 0 0;">
								<p>The email constructed will be built in accordance to the <a href="/files/Intel Eloqua Usage Guidelines.pdf" target="_blank">Eloqua Template Guidelines</a>. If you have any content requirements that are not being collected in this ICC form, please list them out in the Additional Instructions field at the bottom of this page.</p>
							</div>

						</div>
					</div>

					<!-- Block for custom template upload -->
					<div id="custom-assets-block" class="fieldgroup<?php if (getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || strtolower(getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '')) == 'custom' || strtolower(getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '')) == 'default') { echo ' hide'; } ?>">
						<div class="control-group">
							<label for="customassets" class="control-label no-padding">Landing Page Template Assets</label>
							<div class="controls" style="margin-left:244px;">
								<p>Please provide completed html files and any other associated assets to be included in the landing page.</p>
								
								<div class="upload-div">
								    <div class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Asset Zip</span><input id="fileupload_customassets" type="file" name="files[]"></span>
									</div>
									<div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 239px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-customassets" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_customassets'])) {
								    		echo $_SESSION['files_customassets'];
            							}
            							?>
								    </div>
								</div>
							</div>
						</div>
					</div>


					<!--Template Instructions-->
					<!--Do we not need this any longer?
					<div id="instructions" class="<?php //if (getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '') { echo ' hide'; } ?>">
						<div class="fieldgroup" style="padding-top:0px; border-bottom:2px solid #707070;">
							<h3 class="fieldgroup-header" style="margin-top:12px;">Template Instructions</h3>
						</div>

						<div class="fieldgroup" style="border-bottom:0;">
							<div class="control-group">
								<label for="cb" class="control-label">Campaign Brief</label>
								<div class="controls"><p class="input-note">Based upon your configuration, we will send you a campaign brief for you to fill in your specific content. This campaign brief will have the content attributes for your specific configuration. When you have completed this brief, edit the request and upload the xls to the Campaign Brief section.</p></div>
				            </div>
							<div class="control-group">
								<label for="template_instructions" class="control-label">Additional Template Instructions</label>
								<div class="controls"><textarea id="template_instructions" name="template_instructions" class="input-wide textarea-medium" placeholder=""><?php //echo getFieldValue('template_instructions', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
				            </div>
						</div>
					</div>-->


					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-landingpage-2" class="btn-green back pull-left">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" style="margin-left:8px;">Next</a><a href="" id="save-and-exit" class="btn-green submit plain pull-right">Save &amp; Exit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>
