<?php
@session_start();
$loc = 'templates';
$step = 3;
$t_type = 'default';
ob_start();
include('includes/_globals.php');

//If they're moving from custom to default, or vice versa, need to kill the old data
$switched = false;
if (isset($_SESSION['standard_template_type']) && !empty($_SESSION['standard_template_type'])) {
    if (strtolower($_SESSION['standard_template_type']) == 'custom') {
        unset($_SESSION['templates_data']);
        $switched = true;
    }
}

//What type of tactic are we editing (might want to move this out of the URL).
//This is saved in the session as $_SESSION['request_type'], but URL might be better for non-incremental landing
if(isset($_GET['type'])) {
    $tactic_type = $_GET['type'];
} else {
    $tactic_type = $_SESSION['request_type'];
}

//Came from Edit route
if(isset($_GET['edit']) && $_GET['edit'] === '1'){
	$bln_edit   = true;
	$bln_record = true;
	$edit_id 	= $_GET['req_id'];

	//Fetch Record
	//TODO check default and Custom types here
	$query_template  = "SELECT * FROM templates_data WHERE request_id = '".$edit_id."' LIMIT 1";
    $result_template = $mysqli->query($query_template);
    $record = (array)$result_template->fetch_object();

}
//Came from Incomplete request route
else if(isset($_SESSION['t_request_id']) && isset($_SESSION['templates_data']) && $switched == false){
    $bln_record = true;
	$bln_edit 	= false;
	$record = $_SESSION['templates_data'];
//Fresh page
} else {
	//Redirect if a Campaign ID hasn't been set
	if (!isset($_SESSION['campaign_id'])) {
	    header('Location:request?error=request-id');
	    exit();
	}

    $bln_record = false;
    $bln_edit	= false;
}

include('includes/head.php');
include('includes/header-templates.php');

/*
echo '<pre>';
echo strtolower($_SESSION['standard_template_type']);
print_r($_SESSION['templates_data']);
echo '</pre>';
*/
?>


<div class="container-fluid full-width">
    <div class="row-fluid col-wrapxx">
    	<!--Sidebar-->
        <div id="tpl-sidebar" class="span3 pre-scrollable" style="max-height:1000px;">
            <h1>Email and Registration Page Details</h1>
            <p>Enter the content that will appear across all of your emails and landing pages.</p>
            <p><em>To edit the copy, please make edits to the required fields below. Changes will be reflected in the viewing area on the right.</em></p>

            <!--Form-->
            <?php
            	//If coming from Edit route
            	if($bln_edit){
            		echo '<form id="content-edit-template-form" name="content-edit-template-form" action="email_templates/requests.php" method="POST">';
        		// Part of request form flow
            	}else{
            		echo '<form id="content-update-form" class="form-horizontal" action="includes/_requests_'.$tactic_type.'.php" method="POST" enctype="multipart/form-data">';
            	}
            ?>
            	<!-- Hidden fields -->
            	<input type="hidden" id="template" name="template" value="default">
				<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
				<input type="hidden" id="step" name="step" value="3">
				<input type="hidden" id="c_type" name="c_type" value="<?php echo $_GET['type']; ?>">
				<input type="hidden" id="request_id" name="request_id" value="">
				<?php
	            	//If coming from Edit route
	            	if($bln_edit){
	            		echo '<input type="hidden" id="action" name="action" value="edittemplate">';
	            		echo '<input type="hidden" id="req_id" name="req_id" value="' . $edit_id . '">';
	        		// Part of request form flow
	            	}else{
	            		echo '<input type="hidden" id="action" name="action" value="savetemplate">';
	            	}
            	?>


	            <div class="option-block">
	            	<h2>Global Attributes</h2>
	            	<div class="control-group">
		            	<label for="event_header">Event Header</label>
		            	<input type="text" id="event_header" name="event_header" placeholder="Used on all banners. Max characters: 72" maxlength="72" data-target=".event_header_text" class="change-text input-wide noreturn" value="<?php echo ($bln_record) ? $record['event_header']:''; ?>">
		            </div>
		            <div class="control-group">
		            	<label for="copyright_information">Copyright Information<span class="small pull-right"><a href="" class="use-default" data-default="Copyright © 2014 Intel Corporation. All rights reserved. Intel, the Intel logo, are trademarks of Intel Corporation in the U.S. and/or other countries.<br /><br />Intel is committed to protecting your privacy. For more information about Intel's privacy practices, please visit <a href='http://www.intel.com/privacy'>www.intel.com/privacy</a> or write to Intel Corporation, ATTN Privacy, Mailstop RNB4-145, 2200 Mission College Blvd., Santa Clara, CA 95054 USA<br /><br /><a href='https://www-ssl.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html'>Privacy</a> | <a href='https://www-ssl.intel.com/content/www/us/en/privacy/intel-cookie-notice.html'>Cookies</a> | <a href='#'>Unsubscribe</a> | <a href='https://www-ssl.intel.com/content/www/us/en/legal/trademarks.html'>*Trademarks</a>" data-target="copyright_information">Use Default</a></span></label>
		            	<textarea id="copyright_information"  name="copyright_information" placeholder="" data-target=".copyright_information_text" class="wys change-text input-wide textarea-medium"><?php echo ($bln_record) ? $record['copyright_information']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Invitation Email</h2>
		            <div class="control-group">
		            	<label for="invitation_body_copy">Invitation Body Copy<span class="small pull-right"><a href="" class="use-default" data-default="Invitation Email Default Content" data-target="invitation_body_copy">Use Default</a></span></label>
		            	<textarea id="invitation_body_copy" name="invitation_body_copy" placeholder="Max characters: 300" data-target=".invitation_body_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['invitation_body_copy']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Reminder Email</h2>
		            <div class="control-group">
		            	<label for="reminder_body_copy">Reminder Body Copy<span class="small pull-right"><a href="" class="use-default" data-default="This is a reminder that you are invited to {{eventName}}, which is due to start on {{dd MMM yyyy}}. Below are the event details and instructions for participating:<br /><br />When: {{dd MMM yyyy}}<br />{{eventStartTime}} {{eventTimeZone}}<br />{{EDStartTimeDescription}}<br /><br />Please click here to enter the Webinar.<br /><br />You must be pre-registered in order to access this webinar. If you have not registered for this webinar, you may do so at the following link: {{EV_registrationURL}}<br /><br />For registration or webinar entry help click here.<br /><br />Thank you,<br />The Intel Webinar Team" data-target="reminder_body_copy">Use Default</a></span></label>
		            	<textarea id="reminder_body_copy" name="reminder_body_copy" placeholder="Max characters: 300" data-target=".reminder_body_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['reminder_body_copy']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Confirmation Email</h2>
		            <div class="control-group">
		            	<label for="confirmation_body_copy">Confirmation Body Copy<span class="small pull-right"><a href="" class="use-default" data-default="You have successfully completed your registration for the following event:<br /><br />{{EventName}}<br />{{dd MMM yyyy}}<br />{{eventStartTime}}<br />{{eventTimeZone}}<br />{{EDStartTimeDescription}}<br /><br />Click here to download calendar link.<br /><br />On the day of the event please click here to enter the Webinar.<br /><br />For registration or webinar entry help click here.<br /><br />We look forward to seeing you online.<br /><br />Sincerely,<br />The Intel Webinar Team" data-target="confirmation_body_copy">Use Default</a></span></label>
		            	<textarea id="confirmation_body_copy" name="confirmation_body_copy" placeholder="Max characters: 300" data-target=".confirmation_body_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['confirmation_body_copy']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Thank You Email</h2>
		            <div class="control-group">
		            	<label for="thank_you_body_copy">Thank You Body Copy<span class="small pull-right"><a href="" class="use-default" data-default="<p>Thank you for joining our recent {{title}} webinar. We hope you enjoyed our event and found the information relevant.</p><p>Find information and upcoming events at <a href='http://www.intel.com/content/www/us/en/events/online.html?wapkw=webinars#all,all'>Intel.com</a>. We hope you will join us again soon.</p><p>Thank you,</p><p>The Intel Webinar Team</p>" data-target="thank_you_body_copy">Use Default</a></span></label>
		            	<textarea id="thank_you_body_copy" name="thank_you_body_copy" placeholder="Max characters: 300" data-target=".thank_you_body_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['thank_you_body_copy']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Sorry We Missed You Email</h2>
		            <div class="control-group">
		            	<label for="sorry_we_missed_you_body_copy">Sorry We Missed You Copy<span class="small pull-right"><a href="" class="use-default" data-default="<p>We are sorry to have missed you at our recent {{title}} webinar.</p><p>Find information and upcoming events at <a href='http://www.intel.com/content/www/us/en/events/online.html?wapkw=webinars#all,all'>Intel.com</a>. We hope to see you at a future webinar soon.</p><p>Thank you,</p><p>The Intel Webinar Team</p>" data-target="sorry_we_missed_you_body_copy">Use Default</a></span></label>
		            	<textarea id="sorry_we_missed_you_body_copy" name="sorry_we_missed_you_body_copy" placeholder="Max characters: 300" data-target=".sorry_we_missed_you_body_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['sorry_we_missed_you_body_copy']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Registration Page</h2>
	            	<div class="control-group">
		            	<label for="regbody_header">Body Header</label>
		            	<input type="text" id="regbody_header" name="regbody_header" placeholder="" data-target=".regbody_header_text" class="change-text input-wide noreturn" value="<?php echo ($bln_record) ? $record['regbody_header']:''; ?>">
		            </div>
		            <div class="control-group">
		            	<label for="registration_body_copy">Registration Body Copy<span class="small pull-right"><a href="" class="use-default" data-default="Registration body default content" data-target="registration_body_copy">Use Default</a></span></label>
		            	<textarea id="registration_body_copy" name="registration_body_copy" placeholder="Max characters: 300" data-target=".registration_body_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['registration_body_copy']:''; ?></textarea>
		            </div>
		            <div class="control-group">
		            	<label for="checkbox">Checkbox</label>
		            	<textarea id="checkbox" name="checkbox" placeholder="Max characters: 300" maxlength="300" data-target=".checkbox_text" class="wys input-wide textarea-medium required"><?php echo ($bln_record) ? $record['checkbox']:''; ?></textarea>
		            </div>
		            <div class="control-group">
		            	<label for="disclaimer_copy">Disclaimer Copy<span class="small pull-right"><a href="" class="use-default" data-default='By providing your contact information, you authorize Intel to contact you by email or telephone with information about Intel products, events, and updates for {XXX}. For more information, please review the <a href="http://www.intel.com/content/www/us/en/legal/terms-of-use.html" target="_blank">Terms of Use</a> and <a href="http://www.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" target="_blank">Intel Privacy Notice</a>.' data-target="disclaimer_copy">Use Default</a></span></label>
		            	<textarea id="disclaimer_copy" name="disclaimer_copy" placeholder="Max characters: 300" maxlength="300" data-target=".disclaimer_copy_text" class="wys input-wide textarea-medium required"><?php echo ($bln_record) ? $record['disclaimer_copy']:''; ?></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Confirmation Page</h2>
		            <div class="control-group">
		            	<label for="confirmation_page_copy">Confirmation Page Copy</label>
		            	<textarea id="confirmation_page_copy" name="confirmation_page_copy" placeholder="Max characters: 300" data-target=".confirmation_page_copy_text" maxlength="300" class="wys input-wide textarea-medium"><?php echo ($bln_record) ? $record['confirmation_page_copy']:''; ?></textarea>
		            </div>
		        </div>

		    </form>
		    <!--/Form-->
		    <p>&nbsp;</p>
		    <p>&nbsp;</p>
		    <p>&nbsp;</p>
		    <p>&nbsp;</p>
        </div>
        <!--/Sidebar-->

        <!--Body-->
        <div id="tpl-body" class="span9">
        	<div style="width:100%;margin:0 auto;">
	            <div class="template-preview-select" style="width:100%;text-align:center;">
	            	Preview Asset
	            	<select id="preview-select" name="preview-select">
	            		<option value="#invitation-email">Invitation Email</option>
	            		<option value="#reminder-email-one">Reminder Email 1</option>
	            		<option value="#reminder-email-two">Reminder Email 2</option>
	            		<option value="#confirmation-email">Confirmation Email</option>
	            		<option value="#thank-you-email">Thank You Email</option>
	            		<option value="#sorry-we-missed-you-email">Sorry We Missed You Email</option>
	            		<option value="#registration-page">Registration Page</option>
	            		<option value="#confirmation-page">Confirmation Page</option>
	            	</select>
	            </div>
	        </div>

            <div id="template-previews">
            	<div id="invitation-email" class="template-block template-email">
	            	<?php include('email_templates/headers/email-banner-f-default-invitation.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-default-invitation.php'); ?>
	            	<?php include('email_templates/footer/email-footer-a-default.php'); ?>
	            </div>

	            <div id="reminder-email-one" class="template-block template-email hide">
	            	<?php include('email_templates/headers/email-banner-f-default-reminder.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-default-reminder.php'); ?>
	            	<?php include('email_templates/footer/email-footer-a-default.php'); ?>
	            </div>

	            <div id="reminder-email-two" class="template-block template-email hide">
	            	<?php include('email_templates/headers/email-banner-f-default-reminder.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-default-reminder.php'); ?>
	            	<?php include('email_templates/footer/email-footer-a-default.php'); ?>
	            </div>

	            <div id="confirmation-email" class="template-block template-email hide">
	            	<?php include('email_templates/headers/email-banner-f-default-reminder.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-default-confirmation.php'); ?>
	            	<?php include('email_templates/footer/email-footer-a-default.php'); ?>
	            </div>

	            <div id="thank-you-email" class="template-block template-email hide">
	            	<?php include('email_templates/headers/email-banner-f-default-followup.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-thank-you.php'); ?>
	            	<?php include('email_templates/footer/email-footer-a-default.php'); ?>
	            </div>

	            <div id="sorry-we-missed-you-email" class="template-block template-email hide">
	            	<?php include('email_templates/headers/email-banner-f-default-followup.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-sorry.php'); ?>
	            	<?php include('email_templates/footer/email-footer-a-default.php'); ?>
	            </div>

	            <div id="registration-page" class="template-block template-page hide">
	            	<?php echo '<div class="content-wrapper"><div class="main-content cf">'; ?><?php include('email_templates/microsite/headerRegister.php'); ?></div></div>
	            	<?php echo '<div class="content-wrapper"><div class="main-content cf">'.file_get_contents('email_templates/microsite/register.php').'</div></div>'; ?>
	            	<?php echo '<div class="content-wrapper"><div class="main-content cf">'.file_get_contents('email_templates/microsite/footer-plain.php').'</div></div>'; ?>
	            </div>

	            <div id="confirmation-page" class="template-block template-page hide">
	            	<?php echo '<div class="content-wrapper"><div class="main-content cf">'; ?><?php include('email_templates/microsite/headerThankyou.php'); ?></div></div>
	            	<?php echo '<div class="content-wrapper"><div class="main-content cf">'.file_get_contents('email_templates/microsite/body-thank-you.php').'</div></div>'; ?>
	            	<?php echo '<div class="content-wrapper"><div class="main-content cf">'.file_get_contents('email_templates/microsite/footer-plain.php').'</div></div>'; ?>
	            </div>
            </div>
        </div>
        <!--/Body-->
    </div>

	<div class="header-row">
		<div class="container-fluid">
			<header style="width:auto;">
				<div class="header-left" style="margin-top:12px;float:left;">
					<?php
                        if($bln_edit)
                            echo '<a href="request-detail?id=' . $edit_id . '" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                        else
                            echo '<a href="request-'.$tactic_type.'-3" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                    ?>
				</div>
				<div class="header-right">
					<?php
						if($bln_edit){
							//TODO link to update script
							echo '<a href="#" id="submit-content-edit-template-form" class="btn-green submit">Save</a></div>';
						}else{
							if ($_SESSION['admin'] == 1) {
                    			echo '<a href="" id="download-html-file" class="btn-green plain default-template-webinar" style="margin-right:6px" >Generate HTML</a>';
                    		}
							echo '<a href="#" id="submit-request-form" class="btn-green submit">Save &amp; Continue</a></div>';
						}

					?>
			</header>
		</div>
	</div>

</div>

<?php include('includes/footer.php'); ?>
