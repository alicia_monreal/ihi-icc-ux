<?php 
$token = '17787327-Fw7zXvITirTSXtNTJYQGHsedotGPT4PfVlLaRytWc';
$token_secret = 'KO7POrhTflmTAnsyX9ubek7kD4XFaGmWvbHn7vE';
$consumer_key = 'Bt6MUEnals0VkXK5Zk5Bmg';
$consumer_secret = 'iSKMYGpJQwWU000WmHVTCkMxi71esRBo4qkBxqlXc';
$screen_name = 'Finisar';
$count = 3;

$host = 'api.twitter.com';
$method = 'GET';
$path = '/1.1/statuses/user_timeline.json'; // api call path

$query = array( // query parameters
    'screen_name' => $screen_name,
    'count' => $count
);

$oauth = array(
    'oauth_consumer_key' => $consumer_key,
    'oauth_token' => $token,
    'oauth_nonce' => (string)mt_rand(), // a stronger nonce is recommended
    'oauth_timestamp' => time(),
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_version' => '1.0'
);

$oauth = array_map("rawurlencode", $oauth); // must be encoded before sorting
$query = array_map("rawurlencode", $query);

$arr = array_merge($oauth, $query); // combine the values THEN sort

asort($arr); // secondary sort (value)
ksort($arr); // primary sort (key)

// http_build_query automatically encodes, but our parameters
// are already encoded, and must be by this point, so we undo
// the encoding step
$querystring = urldecode(http_build_query($arr, '', '&'));

$url = "https://$host$path";

// mash everything together for the text to hash
$base_string = $method."&".rawurlencode($url)."&".rawurlencode($querystring);

// same with the key
$key = rawurlencode($consumer_secret)."&".rawurlencode($token_secret);

// generate the hash
$signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, true)));

// this time we're using a normal GET query, and we're only encoding the query params
// (without the oauth params)
$url .= "?".http_build_query($query);
$url=str_replace("&amp;","&",$url); //Patch by @Frewuill

$oauth['oauth_signature'] = $signature; // don't want to abandon all that work!
ksort($oauth); // probably not necessary, but twitter's demo does it

// also not necessary, but twitter's demo does this too
function add_quotes($str) { return '"'.$str.'"'; }
$oauth = array_map("add_quotes", $oauth);

// this is the full value of the Authorization line
$auth = "OAuth " . urldecode(http_build_query($oauth, '', ', '));

// if you're doing post, you need to skip the GET building above
// and instead supply query parameters to CURLOPT_POSTFIELDS
$options = array( CURLOPT_HTTPHEADER => array("Authorization: $auth"),
                  //CURLOPT_POSTFIELDS => $postfields,
                  CURLOPT_HEADER => false,
                  CURLOPT_URL => $url,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_SSL_VERIFYPEER => false);
//echo $url;
// do our business
$ch = curl_init();
curl_setopt_array($ch, $options);
$json = curl_exec($ch);
curl_close($ch);
$output = '';
$tweets = json_decode($json);

foreach ($tweets as $item) {

    $output .= '<div class="slide">';
    $patterns = array(
        '#(([a-zA-Z]+://)([a-zA-Z0-9?&%.;:/=+_-]*))#',
        "/@([A-Za-z0-9_]+)\b/"

    );
    $replacements = array(
        '<a href="$1">$1</a>',
        '<a href="http://www.twitter.com/$1">@$1</a>',
        ''                
    );
    $title = preg_replace($patterns, $replacements, $item->text);
    $output .= $title;
    //$output .= '<br><span class="ago">'.relativeTime($item->created_at).'</span>';
    $output .= '</div>';


}
$twits = $output;
?>

<div class="blue-header">
	<h2>Latest Tweets</h2>
</div>
<div class="right-content">
	<?php echo $twits; ?>
</div>