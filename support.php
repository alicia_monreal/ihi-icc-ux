<?php
$loc = 'support';
include('includes/head.php');
include('includes/header.php');
?>

    <div class="container">

        <div class="row intro-body">
            <div class="intro span12">
                <h1>FAQ's</h1>
                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            Will this migration change how my OSAs qualify leads? 
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse">
                            <div class="accordion-inner">
                                No. If your OSAs qualify leads in Aprimo today, they will continue to qualify leads in Aprimo after the WW10 migration.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            What will happen to my existing web forms or microsites that feed contacts into Aprimo?
                            </a>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse">
                            <div class="accordion-inner">
                            If you have an active form or microsite and you are not already working with IT to migrate your assets to Eloqua, your form will remain active after the WW10 migration. IT is currently evaluating all remaining active web forms and microsites and will communicate the plan to address these forms, as well as the plan to ensure data is aligned between Eloqua and Aprimo, in the coming weeks. If you have additional questions, contact <a href="mailto:support@customerconnect.intel.com">support@customerconnect.intel.com</a>. 
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                            I have new campaign assets to create now - should I create them in Eloqua or Aprimo?
                            </a>
                        </div>
                        <div id="collapseThree" class="accordion-body collapse">
                            <div class="accordion-inner">
                            This may depend on the timing and details of your campaign; however, most new assets will be created in Eloqua. Follow the communicated process to submit your request via the SharePoint or Intel Customer Connect Portal. The Production Support team will determine how best to support your campaign.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                            What is Topliners and how do I get access?
                            </a>
                        </div>
                        <div id="collapseFour" class="accordion-body collapse">
                            <div class="accordion-inner">
                            Topliner is a community of Eloqua practitioners, Intel users can join the community and participate in a private user group.  See the <a href="https://soco.intel.com/groups/osg-worldwide-operations-working-session/blog/2014/02/10/eloqua-resources-toplinerseloquacom" target="_blank">Inside Blue post here</a> for instructions.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                            How can I learn more about Eloqua? 
                            </a>
                        </div>
                        <div id="collapseFive" class="accordion-body collapse">
                            <div class="accordion-inner">
                            Depending on your role and required level of detail, there are several resources in place to learn about our new Marketing Automation cloud solution, Eloqua.
                            <ul>
                                <li><a href="https://customerconnect.intel.com/">The Intel Customer Connect Portal</a></li>
                                <li><a href="http://topliners.eloqua.com/welcome" target="_blank">The Eloqua Topliners Community</a></li>
                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                            Can I get access to the Eloqua Sandbox?
                            </a>
                        </div>
                        <div id="collapseSix" class="accordion-body collapse">
                            <div class="accordion-inner">
                            Yes, Eloqua Sandbox is available on a "as need" basis.  Please submit email to <a href="mailto:don.rogers@intel.com">don.rogers@intel.com</a> to request access.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                            Can I get access to the Eloqua Production Instance?
                            </a>
                        </div>
                        <div id="collapseSeven" class="accordion-body collapse">
                            <div class="accordion-inner">
                            Yes, Eloqua Production Instance is available on a "as need" basis.  Please submit email to <a href="mailto:don.rogers@intel.com">don.rogers@intel.com</a> to request access.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
                            When will I be trained on Eloqua?
                            </a>
                        </div>
                        <div id="collapseEight" class="accordion-body collapse">
                            <div class="accordion-inner">
                            Training requirements and plans will be based on user role.  The roles are:  marketing practitioner, geo sales operations, OSAM/OSA, business unit stakeholders, agencies, and power users. There will be both 'Getting Started' and 'Recommended' training courses offered.  If you have additional questions, contact <a href="mailto:support@customerconnect.intel.com">support@customerconnect.intel.com</a>.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseNine">
                            How do I get reporting for my tactics / campaigns?
                            </a>
                        </div>
                        <div id="collapseNine" class="accordion-body collapse">
                            <div class="accordion-inner">
                            If you are looking for reporting on Leads, please go to <a href="http://mybi.intel.com" target="_blank">http://mybi.intel.com</a> and request an account.  From there you will be able to choose from 8 standard reports or generate your own.  For E2E reporting or any other reporting needs, please send an email to <a href="mailto:reporting@customerconnect.intel.com">reporting@customerconnect.intel.com</a> with your request. For more info see the <a href="http://wiki.ith.intel.com/pages/viewpage.action?pageId=264280254" target="_blank">Online Sales Playbook</a>.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTen">
                            When using Eloqua Contact List Upload Templates, what guidelines do I follow for the Source ID column?
                            </a>
                        </div>
                        <div id="collapseTen" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <p><strong>For Existing Tactics</strong><br />
                                Marketing Managers can still use Legacy Marketing Activity IDs (from Aprimo) in the Source ID column of in the Eloqua Contact Upload List Templates. Legacy Marketing Activity IDs, and the related Marketing Activity Description, was part of our data migration from Aprimo to Eloqua.</p>
     
                                <p><strong>For New Tactics</strong><br />
                                Marketing Managers can use a "friendly" name within the naming structure (for both the campaign and the tactic).</p>

                                <p align="center"><img src="img/step-1-screen-shot.png" alt="Friendly Name" style="border:1px solid #999;"></p>

                                <p>This Source ID will automatically be generated for you upon submission of the list on the Customer Connect portal (ICC):</p>
     
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;Sector -> SubSector -> Geo -> Campaign Tag -> Tactic Type -> Tactic Tag -> ICC ID</p>
                                 
                                <p>Example:<br />
                                Itdm.wa.emea.2in1.eNurture.WW25 broadcast.612</p>
                                 
                                <p>Example breakdown:<br />
                              Itdm.wa.emea.2in1 = Where’s it’s coming from.<br />
                              eNurture.WW25 broadcast.612 = What it’s for.</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEleven">
                            When do I start receiving automated reports and what do I get?
                            </a>
                        </div>
                        <div id="collapseEleven" class="accordion-body collapse">
                            <div class="accordion-inner">
                            Automated reports begin delivering EOD from when the first email is sent out daily for a week (5 business days) EST for each email.
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    
        <div class="row intro-body">
            <div class="intro span12">
                <h1>Contact Support</h1>
                <p>Please use the "Contact Us" form from the link below to contact support with your questions.</p>
                <p><a href="#contact-us" data-toggle="modal" class="btn-green">Contact Us</a></p>
            </div>
        </div>

        <div class="row intro-body">
            <div class="intro span12">
                <h1>Submit a New Requirement Request</h1>
                <p>Please use the "New Requirement Request" form from the link below to submit a new requirement or feature request for review and consideration by the Global Demand Center (GDC) Board.</p>
                <p>All submitted requests will be reviewed <span style="color:#ff0000;">within 5 business days</span> of submission. Upon review, you will hear directly from a member of the GDC Board with next steps.</p>
                <p><a href="#new-requirement-request" data-toggle="modal" class="btn-green">New Requirement Request</a></p>
            </div>
        </div>

    </div>

    

    <!-- Modals -->
    <!-- Contact Us -->
    <div id="contact-us" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="requestLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="requestLabel" class="request-form-label">Contact Us</h3>
        <h4 class="contact-form-subtitle">Use the form below to contact us.</h4>
      </div>
      <div class="modal-body">
        <form action="" id="contact-form" class="download-form" data-form="contact">
            <input type="hidden" id="name" name="name" value="<?php echo $_SESSION['fname']; ?> <?php echo $_SESSION['lname']; ?>">
            <input type="hidden" id="email_contact" name="email_contact" value="<?php echo $_SESSION['email']; ?>">
            <input type="hidden" id="fullsupport" name="fullsupport" value="1">
            <fieldset>
                <p><input type="text" name="subject" id="subject" placeholder="Subject" value=""></p>
                <?php
                /*
                <p>
                    <select id="severity" name="severity" class="required" style="width:408px;">
                        <option value="">Severity*</option>
                        <option value="Critical">Critical</option>
                        <option value="High">High</option>
                        <option value="Medium">Medium</option>
                        <option value="Low">Low</option>
                    </select>
                </p>
                */
                ?>
                <p>
                    <select id="category" name="category" class="required" style="width:408px;">
                        <option value="">Please select a reason for contacting us*</option>
                        <option value="Aprimo">Aprimo</option>
                        <option value="Data">Data</option>
                        <option value="Eloqua">Eloqua</option>
                        <option value="Feature Requests">Feature Requests</option>
                        <option value="ICC Portal">ICC Portal</option>
                        <option value="Reporting Request">Reporting Request</option>
                        <option value="Webinar">Webinar</option>
                        <option value="Other">Other</option>
                    </select>
                </p>
                <p><textarea name="message" id="message" placeholder="Your Message*" class="required" style="height:120px;"></textarea></p>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a href="" class="btn-green submit" data-analytics-label="Submit Form: Contact Us">Submit</a></p>
                <p>&nbsp;</p>
            </fieldset>
        </form>
      </div>
    </div>

    <!-- New Requirement Request -->
    <div id="new-requirement-request" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="requestLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="requestLabel" class="request-form-label">New Requirement Request</h3>
        <h4 class="new-requirement-request-form-subtitle">Use the form below to submit a new requirement or feature request for review and consideration by the Global Demand Center (GDC) Board.</h4>
      </div>
      <div class="modal-body">
        <form action="" id="new-requirement-request-form" class="download-form" data-form="contact">
            <input type="hidden" id="name" name="name" value="<?php echo $_SESSION['fname']; ?> <?php echo $_SESSION['lname']; ?>">
            <input type="hidden" id="email_contact" name="email_contact" value="<?php echo $_SESSION['email']; ?>">
            <input type="hidden" id="newrequestrequirement" name="newrequestrequirement" value="1">
            <input type="hidden" name="subject" id="subject" value="New Requirement Request">
            <fieldset>
                <p>
                    <select id="category" name="category" class="required" style="width:408px;">
                        <option value="">Please select a category*</option>
                        <option value="ICC Feature Addition">ICC Feature Addition</option>
                        <option value="Lead Management Requirement (Aprimo)">Lead Management Requirement (Aprimo)</option>
                        <option value="Eloqua Feature Enhancement">Eloqua Feature Enhancement</option>
                        <option value="Reporting Requirement">Reporting Requirement</option>
                        <option value="Webinar Feature Enhancement">Webinar Feature Enhancement</option>
                        <option value="Other">Other</option>
                    </select>
                </p>
                <p><textarea name="message" id="message" placeholder="Description*  Please provide detailed information for your requirement/feature enhancement request including any relavant use cases that support your request." class="required" style="height:120px;"></textarea></p>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a href="" class="btn-green submit" data-analytics-label="Submit Form: New Requirement Request">Submit</a></p>
                <p>&nbsp;</p>
            </fieldset>
        </form>
      </div>
    </div>

<?php include('includes/footer.php'); ?>