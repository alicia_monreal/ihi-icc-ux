<?php
session_start();
include('includes/_globals.php');
//Redirect to Home if logged in
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1) {
    header('Location:/home');
    die();
}

$loc = 'login';
include('includes/head.php');
include('includes/header.php');

function generateFormToken($form) {
    $token = md5(uniqid(microtime(), true));  
    $_SESSION[$form.'_token'] = $token; 
    
    return $token;
}
$newToken = generateFormToken('request-form'); 
?>

    <div class="container">
    	<div id="login" class="row">
    		<h1>Login</h1>
            <h4 class="login-form-subtitle"><?php if (isset($_GET['session'])) { echo '<span style="color:#ff0000;">Your session has expired.</span><br />'; } ?>Welcome to Intel Customer Connect, your destination to learn about, request, and manage webinar and marketing automation campaigns.</h4>
    		<form action="home" id="login-form" class="login-form" data-form="login">
                <fieldset>
                    <p><input type="text" name="username" id="username" placeholder="Email" value="<?php if (isset($_COOKIE['rememberme'])) { echo $_COOKIE['rememberme']; } ?>" class="required"></p>
                    <p><input type="password" name="password" id="password" placeholder="Password" value="" class="required"></p>
                    <p class="pull-left" style="margin-top:-4px;"><a href="#request" data-toggle="modal" data-analytics-label="Access Request Form">Request Access</a><br />Forgot your password? <a href="#forgot" data-toggle="modal" data-analytics-label="Forgot Password Link">Click here</a></p><p class="pull-right"><input type="submit" id="login-submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Login" value="Submit" style="width:120px;"></p>
                    <div class="clearfix"></div>
                    <div class="span2 pull-right" style="width:112px;text-align:right;clear:both;"><label class="checkbox inline" style="padding-left:2px;"><input type="checkbox" id="rememberme" name="rememberme" value="1"<?php if (isset($_COOKIE['rememberme'])) { echo ' checked="checked"'; } ?>> Remember me</label></div>
                </fieldset>
            </form>
		</div>
    </div>

    <!-- Modals -->
    <!-- Request Access -->
    <div id="request" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="requestLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="requestLabel" class="request-form-label">Request Access</h3>
        <h4 class="request-form-subtitle">Fill out form to request access to site.</h4>
      </div>
      <div class="modal-body">
        <form action="" id="request-form" class="download-form" data-form="request">
            <fieldset>
                <input type="hidden" name="token" value="<?php echo $newToken; ?>">
                <p><input type="text" name="fname" id="fname" placeholder="First Name*" value="" class="required" autocomplete="off"></p>
                <p><input type="text" name="lname" id="lname" placeholder="Last Name*" value="" class="required" autocomplete="off"></p>
                <p><input type="text" name="title" id="title" placeholder="Title" value="" autocomplete="off"></p>
                <p><input type="text" name="company" id="company" placeholder="Company*" value="" class="required" autocomplete="off"></p>
                <p><input type="text" name="email" id="email" placeholder="Email*" value="" class="required email"></p>
                <p><select name="role" id="role" class="required" style="width:407px;"><option value="">Role*</option>
                    <?php
                    $query = "SELECT * FROM roles WHERE active = 1 ORDER BY name ASC";
                    $result = $mysqli->query($query);
                    while ($obj = $result->fetch_object()) {
                        if ($obj->id != 1 && $obj->id != 6) { //Can't choose Admin
                            echo '<option value="'.$obj->id.'">'.$obj->name.'</option>';
                        }
                    }
                    ?>
                </select></p>
                <?php //<p><input type="text" name="phone" id="phone" placeholder="Phone (optional)" value=""></p> ?>
                <div id="point-of-contact-info" class="hide">
                    <p><input type="text" name="point_of_contact" id="point_of_contact" placeholder="Who is your Intel point of contact?*" value="" autocomplete="off"></p>
                    <p><input type="text" name="point_of_contact_email" id="point_of_contact_email" placeholder="Point of contact email address*" value="" class="email" autocomplete="off"></p>
                </div>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a href="" class="btn-green submit" data-analytics-label="Submit Form: Request Access">Submit</a></p>
            </fieldset>
        </form>
      </div>
    </div>

    <!-- Forgot Password -->
    <div id="forgot" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="forgotLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="forgotLabel" class="forgot-form-label">Forgot Password?</h3>
        <h4 class="forgot-form-subtitle">Enter your email below and we'll resend your password.</h4>
      </div>
      <div class="modal-body">
        <form action="" id="forgot-form" class="download-form" data-form="forgot">
            <fieldset>
                <p><input type="text" name="email_forgot" id="email_forgot" placeholder="Email" value="" class="required email"></p>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a href="" class="btn-green submit" data-analytics-label="Submit Form: Forgot Password">Submit</a></p>
            </fieldset>
        </form>
      </div>
    </div>

    <!-- Contact Us -->
    <div id="contact-us" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="requestLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="requestLabel" class="request-form-label">Contact Us</h3>
        <h4 class="contact-form-subtitle">Use the form below to contact us.</h4>
      </div>
      <div class="modal-body">
        <form action="" id="contact-form" class="download-form" data-form="contact">
            <fieldset>
                <input type="hidden" name="token" value="<?php echo $newToken; ?>">
                <p>
                    <!--<select id="reason" name="reason" class="required" style="width:408px;">
                        <option value="">Please select a reason for contacting us</option>
                        <option value="Urgent">Urgent</option>
                        <option value="Webinar issue">Webinar issue</option>
                        <option value="Email issue">Email issue</option>
                        <option value="Webinar registration issue">Webinar registration issue</option>
                        <option value="Connection issue">Connection issue</option>
                        <option value="Other">Other</option>
                    </select>-->
                    <select id="category" name="category" class="required" style="width:408px;">
                        <option value="">Please select a reason for contacting us</option>
                        <option value="Aprimo">Aprimo</option>
                        <option value="Data">Data</option>
                        <option value="Eloqua">Eloqua</option>
                        <option value="Feature Requests">Feature Requests</option>
                        <option value="ICC Portal">ICC Portal</option>
                        <option value="Reporting Request">Reporting Request</option>
                        <option value="Webinar">Webinar</option>
                        <option value="Other">Other</option>
                    </select>
                </p>
                <p><input type="text" name="name" id="name" placeholder="Name" value="" class="required"></p>
                <p><input type="text" name="email_contact" id="email_contact" placeholder="Email" value="" class="required email"></p>
                <p><textarea name="message" id="message" placeholder="Your Message" class="required" style="height:120px;"></textarea></p>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a href="" class="btn-green submit" data-analytics-label="Submit Form: Contact Us">Submit</a></p>
            </fieldset>
        </form>
      </div>
    </div>

<?php include('includes/footer.php'); ?>
