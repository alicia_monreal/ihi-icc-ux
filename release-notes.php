<?php
$loc = 'release-notes';
include('includes/head.php');
include('includes/header.php');
?>

	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
                <h1>Release Notes</h1>
            </div>
        </div>
    </div>


    <div class="container">
    	<div class="row">
    		<div id="zebra" class="single-request">

				<div class="row"><label>August 19, 2014</label> 
		        	<div class="pull-left">
		        		<p><strong>Business Process Update</strong></p>
		        		<ul class="normal">
		        			<li>ICC has been updated to now process the new Eloqua templates for the Webinar, Single Email, and Landing Page tactics;  Use of the new templates for eNurture and Newsletter tactics is available but will require requestors to work with Production Support staff to have the assets built out</li>
		        			<li>Collection of asset attributes for the templates will be done through a campaign brief that production support will send to you following tactic submission</li>
		        		</ul>

						<p><strong>Feature Updates</strong></p>
		        		<ul class="normal">
		        			<li>Capability from the requestor and assignee to communicate on specific requests through ICC</li>
		        		</ul>
		        	</div>
		        </div>

				<div class="row"><label>June 30, 2014</label> 
		        	<div class="pull-left">
		        		<p><strong>Business Process Update</strong></p>
		        		<ul class="normal">
		        			<li>With the new "duplicate request" capability, you can now easily submit similar requests (such as a multi-webinar request) without having to re-fill in attribute fields and assets</li>
		        		</ul>

						<p><strong>Feature Updates</strong></p>
		        		<ul class="normal">
		        			<li>Capability to clone existing requests so that all fields and associated files are duplicated in the new request except tactic tag and the date</li>
		        			<li>Added default image selection for webinar email notifications</li>
		        			<li>Ability to view only your personal requests in the calendar</li>
		        			<li>Processing capability for webinar On-demand capability and duration on Intel.com listing page</li>
		        			<li>Graphical tweaks to calendar components (tooltip and color changes)</li>
		        			<li>SLA listings for each request type</li>
		        			<li>Redesign on the home page to enhance prominence of training area</li>
		        			<li>Enhanced copy for reporting information</li>
		        		</ul>
 
						<p><strong>Production Support Update</strong></p>
		        		<ul class="normal">
		        			<li>Enhancements to contact list -> tactic association</li>
		        		</ul>
		        	</div>
		        </div>

    			<div class="row"><label>May 31, 2014</label> 
		        	<div class="pull-left">
		        		<p><strong>Business Process Update</strong></p>
		        		<ul class="normal">
		        			<li>Change of requirements pertaining to webinars that are non-DG or smaller than 50 attendees.  Non-DG webinars or with fewer than 50 attendees no longer require a  campaign to be created for them as they will automatically be assigned to a special campaign meant for non-DG webinars.</li>
		        		</ul>

						<p><strong>Feature Updates</strong></p>
		        		<ul class="normal">
		        			<li>Content calendar for planning and tracking of webinars, emails, newsletters, and eNurture tactics</li>
		        			<li>Capability to process non-DG webinars (these will only be created within ON24)</li>
		        			<li>Instructional and example text enhancements</li>
		        			<li>BKMs and additional directions added to the webinar presentation template</li>
		        			<li>Automatically suggested invitation and reminder email timings based upon scheduled webinar date</li>
		        			<li>Enhanced filtering capabilities for tables</li>
		        			<li>Enhanced campaign selection for tactic requests</li>
		        		</ul>
 
						<p><strong>Production Support Update</strong></p>
		        		<ul class="normal">
		        			<li>Enhancements to status dropdowns to provide more granularity</li>
		        			<li>General bug fixes</li>
		        		</ul>
		        	</div>
		        </div>

		        <div class="row"><label>April 30, 2014</label> 
		        	<div class="pull-left">
		        		<p><strong>Business Process Update</strong></p>
		        		<ul class="normal">
		        			<li>Webinars no longer require a campaign brief to be uploaded with the request submission; this has been incorporated into the web form</li>
		        		</ul>

		        		<p><strong>Feature Updates</strong></p>
		        		<ul class="normal">
		        			<li>For the webinar tactic, web form integration for invitation/registration/reminder emails and landing page configuration details</li>
		        			<li>If the requestor responds to a notification email, an email is sent to a designated production support member based upon request type</li>
		        			<li>Any time a status change occurs within a request, a notification email is sent to the requestor</li>
		        			<li>Notification emails on status changes will have the pertinent name of the request specified in the email subject line</li>
		        			<li>New “Release Notes” section on the ICC portal to detail out ICC feature updates and business process changes</li>
		        			<li>“Feature Request” is now a selectable option for the “Contact Us” form</li>
		        			<li>Widget screenshots added to webinar console configuration area</li>
		        			<li>New entry area to take special instructions for the given request</li>
		        			<li>Chinese language support for webinars</li>
		        			<li>Expanded field capture for the multi-webinar campaign brief</li>
		        			<li>For all tactics supporting eloqua driven emails, the request form now captures email attributes for From Name, From address, Reply Name, and Reply Address</li>
		        			<li>Support for webinars that are not published on Intel.com</li>
		        		</ul>

		        		<p><strong>Production Support Update</strong></p>
		        		<ul class="normal">
		        			<li>All requests now have a “Cancelled” status</li>
		        			<li>Updated naming on assets area for list upload requests</li>
		        			<li>Production support can now set the webcast environment for webinar tactic requests</li>
		        		</ul>
		        	</div>
		        </div>

		        <div class="row"><label>March 31, 2014</label> 
		        	<div class="pull-left">
		        		<p><strong>Business Process Update</strong></p>
		        		<ul class="normal">
		        			<li>Eloqua Source ID and Aprimo Marketing Activity ID moved from campaign level to the individual tactic level (includes contact/lead/prospect assessment lists)</li>
		        			<li>Updated naming conventions for all requests</li>
		        			<li>List association with a particular tactic</li>
		        			<li>Include multi-webinar intake form during webinar requests</li>
		        		</ul>

		        		<p><strong>Feature Updates</strong></p>
		        		<ul class="normal">
		        			<li>A separate Contact List Request tactic is created when a list is included as part of a tactic request; so instead of a single tactic request getting created, a tactic request and a Contact List upload request is created to allow more transparency and separate management of the Contact List request</li>
		        			<li>Eloqua Source ID auto generation based upon selected attributes</li>
		        			<li>New Modern Marketer 101 training courses on ICC</li>
		        			<li>At the campaign level, ability to search by Aprimo mID</li>
		        		</ul>

		        		<p><strong>Production Support Updates</strong></p>
		        		<ul class="normal">
		        			<li>Ability to reassign requestor</li>
		        		</ul>
		        	</div>
		        </div>
			</div>
        </div>
    </div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>

<?php include('includes/footer.php'); ?>