<?php
include('includes/_globals.php');
$loc = 'campaign-requests';
$step = 2;
$c_type = 'contactlist';
include('includes/head.php');
include('includes/header.php');

//Redirect if a Request ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Upload Contact List</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request twoup">
				<li>Step 1</li>
				<li class="active">Step 2</li>
			</ul>
			<div class="request-step step4">
				<div class="fieldgroup">
					<p>All the contact data that you upload will be purged from this portal 30 days after the upload per Intel's privacy guidelines.  When you upload your contact list to the ICC portal, we will perform cleansing and validation as well as normalization if necessary before your contacts will be available in Eloqua.</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p><p>Click on the <i class="icon-question-sign" style="margin-top: 3px;"></i> for more information and examples.</p>
					<h3 class="fieldgroup-header">Upload Contacts</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_contactlist.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="2">
					<input type="hidden" id="c_type" name="c_type" value="contactlist">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<div class="fieldgroup">
						<div class="control-group">
							<label for="associated_tactic" class="control-label">Select the tactic for the list to be associated to</label>
							<div class="controls">
								<select id="associated_tactic" name="associated_tactic" class="input-wide">
									<?php
				                    $query = "SELECT
				                    			id, request_type
				                    		FROM request
				                    		WHERE campaign_id = '".$_SESSION['campaign_id']."'
				                    		AND request_type != 4
				                    		AND request_type != 8
				                    		AND request_type != 9
				                    		AND active = 1
				                    		ORDER BY date_created DESC";
				                    $result = $mysqli->query($query);
				                    $row_cnt = $result->num_rows;
				                    if ($row_cnt > 0) {
				                    	echo '<option value="None">None</option>';
					                    while ($obj = $result->fetch_object()) {
					                        echo '<option value="'.$obj->id.'">'.getTitle($obj->id, $obj->request_type).'</option>';
					                    }
					                } else {
					                	echo '<option value="None">There are no tactics associated with the selected campaign</option>';
					                }
				                    ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label for="tag" class="control-label">List Tag</label>
							<div class="controls">
								<span class="required-mark">*</span><input type="text" id="tag" name="tag" class="input-wide required" value=""><i class="helper-icon popover-link icon-question-sign" data-container="body" data-toggle="popover" data-content="For Example: 2014.07.22 or Retail Touchpoints or Seoul World IT And Security 2013"></i>
								<p class="input-note" style="margin-left:0;">The List Tag is a friendly name for distinguishing the list. Once created, you will not be able to edit this value. Click the pop up for examples.</p>
							</div>
						</div>
						<div class="control-group">
							<label for="list_request_type" class="control-label">List Request Type</label>
							<div class="controls">
								<span class="required-mark">*</span><select id="list_request_type" name="list_request_type" class="input-wide required">
									<option value="">Select one</option>
									<option value="Contact"<?php if ($_SESSION['list_request_type'] == 'Contact') { echo ' selected'; } ?>>Contact</option>
									<option value="RAW"<?php if ($_SESSION['list_request_type'] == 'RAW') { echo ' selected'; } ?>>RAW</option>
								</select><i class="helper-icon popover-link icon-question-sign" data-container="body" data-toggle="popover" data-content="Contact list: These lists use the provided sector-specific templates to load your contacts<br /><br />RAW: These are unformated lists of contacts that do not use the standard upload templates. Do note that these lists may take longer to process and may require some back and forth. These request types also do not follow the standard SLA timing for list loads."></i>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" style="margin-top: -5px;">List Upload</label>
							<div class="controls">
								<div class="controls-minigroup">
									<p>All the contact data that you upload will be purged from this portal 30 days after the upload per Intel's privacy guidelines.</p>
									<div class="upload-div" style="margin-top:8px;">
									    <div class="fileupload fileupload-new" data-provides="fileupload">
											<span class="btn-file btn-green add"><span class="fileupload-new">Upload a List</span><input id="fileupload_contacts" type="file" name="files[]"></span>
										</div>
										<div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;">
									        <div class="bar"></div>
									    </div>
									    <div id="shown-contacts" class="files" style="clear:both;">
									    	<?php
									    	if ($_SESSION['files_contacts']) {
									    		echo '<p>'.$_SESSION['files_contacts'].'</p>';
	            							}
	            							?>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Contact List Request: Step 2">Submit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>