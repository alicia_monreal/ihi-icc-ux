<?php
$loc = 'campaigns';
include('includes/head.php');
include('includes/header.php');
require_once('includes/_globals.php');
$editable = true; //Includes the JS

$edit = false;
if (isset($_GET['edit'])) {
    $edit = true;
}
?>

   <div class="container">
        <div class="row intro-body" style="margin-top:14px;">
            <div class="intro span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
                            req.*,
                            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                            status.value AS status_value,
                            req.request_type AS request_type_id,
                            request_type.request_type AS request_type,
                            users.fname AS fname,
                            users.lname AS lname,
                            users.email AS email,
                            users.role AS role,
                            users.point_of_contact AS point_of_contact,
                            users.point_of_contact_email AS point_of_contact_email
                        FROM request AS req
                        INNER JOIN status on req.status = status.id
                        INNER JOIN request_type on req.request_type = request_type.id
                        INNER JOIN users on req.user_id = users.id
                        WHERE req.active = 1
                        AND req.id = '".$_GET['id']."' LIMIT 1";
                
                $result = $mysqli->query($query);
                $row_cnt = $result->num_rows;

                //print_r($query);

                if ($row_cnt > 0) {
                    while ($obj = $result->fetch_object()) {
                        //We need to know if this person owns this
                        if ($_SESSION['admin'] == 1 || $obj->user_id == $_SESSION['user_id']) { $owner = true; } else { $owner = false; }

                        echo '<div id="zebra" class="single-request">';
                        echo '<h1><br />'.getTitle($_GET['id'], $obj->request_type_id).' Details</h1>';

                        //Now show the details of the particular type of request
                        echo getRequestDetails($obj->id, $obj->request_type_id, $edit, 0);

                        echo '</div>'; //End single-request
                    }
                }

                echo '<p><br /><a href="'; if (isset($_SERVER['HTTP_REFERER'])) { echo $_SERVER['HTTP_REFERER']; } else { echo 'campaigns'; } echo '" class="btn-green back pull-left">Back to list</a>';

                if ($_SESSION['admin'] == 1 || $edit == 1) {
                    //echo '<a href="" id="submit-request-form-inline" class="btn-green submit pull-right" data-analytics-label="Submit Form: Request Update">Submit updates</a></p></form></div>';
                    echo '</p></form></div>';
                } else {
                    echo '</p></div>';
                }
                ?>

            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>