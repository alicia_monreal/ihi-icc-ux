<?php
@session_start();
$loc = 'templates';
$step = 3;
$t_type = 'custom';
ob_start();
include('includes/_globals.php');

//If they're moving from custom to default, or vice versa, need to kill the old data
$switched = false;
if (isset($_SESSION['standard_template_type']) && !empty($_SESSION['standard_template_type'])) {
    if (strtolower($_SESSION['standard_template_type']) == 'default') {
        unset($_SESSION['templates_data']);
        $switched = true;
    }
}
$multiple_email_count = $multiple_landingpage_count = 1; // count of number of elements on page
$email_listing = $landingpage_listing = array(); // listing to preserve data

//What type of tactic are we editing (might want to move this out of the URL).
//This is saved in the session as $_SESSION['request_type'], but URL might be better for non-incremental landing
if(isset($_GET['type'])) {
    $tactic_type = $_GET['type'];
} else {
    $tactic_type = $_SESSION['request_type'];
}

//Came from Edit route
if(isset($_GET['edit']) && $_GET['edit'] === '1'){
    $bln_edit   = true;
    $bln_record = true;
    $edit_id    = $_GET['req_id'];

    if($tactic_type == "enurture" || $tactic_type == "newsletter") { // if tactic enurture or newsletter

        $query_template  = "SELECT * FROM multiple_templates_data WHERE request_id = '".$edit_id."'";
        $result_template = $mysqli->query($query_template);

        $row_cnt = $result_template->num_rows;
        if ($row_cnt > 0) {  // if record exists 
            while ($obj = $result_template->fetch_object()) { 
                
                // user id and request id is saved
                $record['user_id'] = $obj->user_id;
                $record['request_id'] = $obj->request_id;                

                if(!empty($obj->email_config)) { //email_config is not empty
                    // decode the string 
                    $record[$tactic_type.'_email_config_'.$obj->order_number] = json_decode(stripslashes($obj->email_config));
                    
                    if($obj->order_number != 0) { // if not 0 then increment it
                        $multiple_email_count++; 
                    }

                    // fetch all form fields data and temporary saving
                    $email_listing['name'][] = $record[$tactic_type.'_email_config_'.$obj->order_number]->email_name;
                    $email_listing['email_subject'][] = $record[$tactic_type.'_email_config_'.$obj->order_number]->email_subject;
                    $email_listing['send_date'][] = $record[$tactic_type.'_email_config_'.$obj->order_number]->send_date;
                    $email_listing['send_time'][] = $record[$tactic_type.'_email_config_'.$obj->order_number]->send_time;
                }
                else
                if(!empty($obj->landing_page_config)) {  // landing_page exists
                    // decode the string
                    $record[$tactic_type.'_landingpage_config_'.$obj->order_number] = json_decode(stripslashes($obj->landing_page_config));
                    
                    if($obj->order_number != 0) // if not 0 then increment it
                        $multiple_landingpage_count++;

                    // fetch landing page name and temporary saving    
                    $landingpage_listing['name'][] = $record[$tactic_type.'_landingpage_config_'.$obj->order_number]->landingpage_name;
                }
            }
        }

    } else {
        //Fetch Record
        $query_template  = "SELECT * FROM templates_custom_data WHERE request_id = '".$edit_id."' LIMIT 1";
        $result_template = $mysqli->query($query_template);
        $record = (array)$result_template->fetch_object();

        $record['invitation_email_config']   =json_decode( stripslashes($record['invitation_email_config']));
        $record['reminder_one_email_config'] =json_decode( stripslashes($record['reminder_one_email_config']));
        $record['reminder_two_email_config'] =json_decode( stripslashes($record['reminder_two_email_config']));
        $record['confirmation_email_config'] =json_decode( stripslashes($record['confirmation_email_config']));
        $record['thank_you_email_config']    =json_decode( stripslashes($record['thank_you_email_config']));
        $record['sorry_email_config']        =json_decode( stripslashes($record['sorry_email_config']));
        $record['registration_config']       =json_decode( stripslashes($record['registration_config']));
        $record['confirmation_config']       =json_decode( stripslashes($record['confirmation_config']));
        $record['landing_page_config']       =json_decode( stripslashes($record['landing_page_config']));
        $record['single_email_config']       =json_decode( stripslashes($record['single_email_config']));
    }
    // echo '<script>dbRecord = \'' . json_encode($record) . '\'</script>';
}
//Came from Incomplete request route
else if(isset($_SESSION['t_request_id']) && isset($_SESSION['templates_data']) && $switched == false){
    $bln_record = true;
    $bln_edit   = false;
    
    $record     = $_SESSION['templates_data'];

    $record['invitation_email_config']   =json_decode( stripslashes($record['invitation_email_config']));
    $record['reminder_one_email_config'] =json_decode( stripslashes($record['reminder_one_email_config']));
    $record['reminder_two_email_config'] =json_decode( stripslashes($record['reminder_two_email_config']));
    $record['confirmation_email_config'] =json_decode( stripslashes($record['confirmation_email_config']));
    $record['thank_you_email_config']    =json_decode( stripslashes($record['thank_you_email_config']));
    $record['sorry_email_config']        =json_decode( stripslashes($record['sorry_email_config']));
    $record['registration_config']       =json_decode( stripslashes($record['registration_config']));
    $record['confirmation_config']       =json_decode( stripslashes($record['confirmation_config']));
    $record['landing_page_config']       =json_decode( stripslashes($record['landing_page_config']));
    $record['single_email_config']       =json_decode( stripslashes($record['single_email_config']));

    if($tactic_type == "enurture" || $tactic_type == "newsletter") { // if tactic is enurture or newsletter

        foreach($record as $key => $value) {
            $key_name = explode("_",$key);

            if($key_name[0] == "enurture" || $key_name[0] == "newsletter") { // if enurture or newsletter field
                if($key_name[1] == "email") { // if type email
                
                    if($key_name[3] != 0) {  // if not the first element
                        $multiple_email_count++;
                    }
                    $record[$tactic_type.'_email_config_'.$key_name[3]] = json_decode( stripslashes($record[$tactic_type.'_email_config_'.$key_name[3]]));
                    // saving all fields temporarily
                    $email_listing['name'][] = $record[$tactic_type.'_email_config_'.$key_name[3]]->email_name;
                    $email_listing['email_subject'][] = $record[$tactic_type.'_email_config_'.$key_name[3]]->email_subject;
                    $email_listing['send_date'][] = $record[$tactic_type.'_email_config_'.$key_name[3]]->send_date;
                    $email_listing['send_time'][] = $record[$tactic_type.'_email_config_'.$key_name[3]]->send_time;
                }
                else
                if($key_name[1] == "landingpage") {  // landing_page exists
                    
                    if($key_name[3] != 0) // if not the first element
                        $multiple_landingpage_count++;

                    $record[$tactic_type.'_landingpage_config_'.$key_name[3]] = json_decode( stripslashes($record[$tactic_type.'_landingpage_config_'.$key_name[3]]));
                    // saving all fields temporarily
                    $landingpage_listing['name'][] = $record[$tactic_type.'_landingpage_config_'.$key_name[3]]->landingpage_name;
                } 
            }
        }
    }
    
    // echo '<script>dbRecord = \'' . json_encode($record) . '\'</script>';


    // $invitation_email_config_mod  = $record['invitation_email_config']->modules->data;
    // $record['invitation_email_config']->modules->data      = '';

    // $reminder_one_email_config_mod  = $record['reminder_one_email_config']->modules->data;
    // $record['reminder_one_email_config']->modules->data      = '';

    // $reminder_two_email_config_mod  = $record['reminder_two_email_config']->modules->data;
    // $record['reminder_two_email_config->modules->data']      = '';

    // $thank_you_email_config_mod  = $record['thank_you_email_config']->modules->data;
    // $record['thank_you_email_config']->modules->data      = '';

    // $sorry_email_config_mod  = $record['sorry_email_config']->modules->data;
    // $record['sorry_email_config']->modules->data      = '';

    // $registration_config_mod  = $record['registration_config']->modules->data;
    // $record['registration_config']->modules->data      = '';

    // $confirmation_config_mod  = $record['confirmation_config']->modules->data;
    // $record['confirmation_config_mod']->modules->data      = '';

    // echo '<script>';
    // echo 'var reminder_one_email_config_mod = "' . $invitation_email_config_mod . '";';
    // echo 'dbRecord = \'' . json_encode($record) . '\'</script>';
//Fresh page
} else {
    //Redirect if a Campaign ID hasn't been set
    if (!isset($_SESSION['campaign_id'])) {
        header('Location:request?error=request-id');
        exit();
    }

    $bln_record = false;
    $bln_edit   = false;
}

if(empty($record['user_id'])) {
    $record['user_id'] = $_SESSION['user_id']; 
}


include('includes/head.php');
include('includes/header-templates.php');

/*
echo '<pre>';
echo strtolower($_SESSION['standard_template_type']);
print_r($_SESSION['templates_data']);
echo '</pre>';
*/
?>

<div class="container-fluid full-width">
    <div class="row-fluid col-wrap">
        <!--Sidebar-->


        <div id="tpl-sidebar" class="span3">
            <h1>Asset List</h1>
            <?php if($tactic_type != "enurture" && $tactic_type != "newsletter") { ?>
                <p>Select modules and enter content for each asset.</p>
                <p><em>To edit text, please click on the text area you wish to edit and change the text inline.</em></p>
            <?php } else { ?>
                <p><em>Add or remove multiple emails and landing pages</em></p>
            <?php } ?>

            <?php if ($tactic_type == 'webinar') { ?>
            <!-- Webinar -->
            <div class="option-block">
                <h2>Emails</h2>
                <ul class="large-menu">
                    <li><a href="#invitation-email"  class="menu-loader active">Invitation Email</a></li>
                    <li><a href="#reminder-email-one" class="menu-loader">Reminder Email 1</a></li>
                    <li><a href="#reminder-email-two" class="menu-loader">Reminder Email 2</a></li>
                    <li><a href="#confirmation-email" class="menu-loader">Confirmation Email</a></li>
                    <li><a href="#thank-you-email" class="menu-loader">Thank You Email</a></li>
                    <li><a href="#sorry-we-missed-you-email" class="menu-loader">Sorry We Missed You Email</a></li>
                </ul>
            </div>

            <div class="option-block">
                <h2>Landing Pages</h2>
                <ul class="large-menu">
                    <li><a href="#registration-page" class="menu-loader">Registration Page</a></li>
                    <li><a href="#confirmation-page" class="menu-loader">Confirmation Page</a></li>
                </ul>
            </div>
            <!-- /Webinar -->
            <?php } ?>

            <?php if ($tactic_type == 'email') { ?>
            <!-- Single Email -->
            <div class="option-block">
                <h2>Email</h2>
                <ul class="large-menu">
                    <li><a href="#single-email"  class="menu-loader active">Single Email</a></li>
                </ul>
            </div>
            <!-- /Single Email -->
            <?php } ?>

            <?php if ($tactic_type == 'landingpage') { ?>

            <!-- Landing Page -->
            <div class="option-block">
                <h2>Landing Page(s)</h2>
                <ul class="large-menu">
                    <li><a href="#landing-page" class="menu-loader active">Landing Page</a></li>
                    <li><a href="#confirmation-page" class="menu-loader">Confirmation Page</a><br /><small>(If your landing page includes a registration module)</small></li>
                </ul>
            </div>
            <!-- /Landing Page -->
            <?php } ?>

            <!-- enurture / newsletter -->
            <?php if ($tactic_type == 'enurture' || $tactic_type == 'newsletter') { ?>
                <div class="option-block">
                    <h2>Emails</h2>
                    <img id="email-loader" src="img/ajax-loader.gif" style="float:right;margin-right:90px;display:none" />
                    <button class="btn-green add module-btn" id="add_more_emails" type="button">Add more</button>
                    <ul class="large-menu">
                        <!-- default template link for email -->
                        <?php
                         $email_count = 0; // initialize to first element
                         do { ?>
                            <li><a href="#<?php echo $tactic_type; ?>-email-<?php echo $email_count; ?>"  class="menu-loader <?php if($email_count==0) echo 'active'; ?>"><?php if(!empty($email_listing['name'][$email_count])) {echo $email_listing['name'][$email_count];} else { echo "Default Email"; } ?></a>
                                <?php if($email_count != 0) { ?>
                                <div class="remove removeSocial removeAssetLink" style="margin-top:2px;margin-right:60px" href="javascript:void(0);" ></div>
                                <?php } ?>
                            </li>
                        <?php 
                         $email_count++; 
                         } while($email_count!=$multiple_email_count); ?>
                    </ul>
                </div>

                <div class="option-block">
                    <h2>Landing Pages</h2>
                    <img id="landingpage-loader" src="img/ajax-loader.gif" style="float:right;margin-right:90px;display:none" />
                    <button class="btn-green add module-btn" id="add_more_landingpages" type="button">Add more</button>
                    <ul class="large-menu">
                    <!-- default template link for landingpage -->
                        <?php
                         $landingpage_count = 0; // initialize to first element
                         do { ?>
                            <li><a href="#<?php echo $tactic_type; ?>-landingpage-<?php echo $landingpage_count; ?>" class="menu-loader"><?php if(!empty($landingpage_listing['name'][$landingpage_count])) {echo $landingpage_listing['name'][$landingpage_count];} else { echo "Default Landing Page"; } ?></a>
                                <?php if($landingpage_count != 0) { ?>
                                <div class="remove removeSocial removeAssetLink" style="margin-top:2px;margin-right:60px" href="javascript:void(0);" ></div>
                                <?php } ?>
                            </li>
                        <?php 
                         $landingpage_count++;
                         } while($landingpage_count!=$multiple_landingpage_count); ?>
                    </ul>
                </div>
            <?php } ?>

        </div>
        <div class="tpl-head"></div>

        <div id="tpl-body" class="span9 tpl-editor">

            <div class="preview-mode hide">
                <div class="template-preview-select">Preview Mode</div>
            </div>

            <div id="template-previews">
                <?php if ($tactic_type == 'webinar') { ?>
                <!-- Webinar -->
                <div id="invitation-email" data-attr="invitation_email_config" class="template-block template-email">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="reminder-email-one" data-attr="reminder_one_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="reminder-email-two" data-attr="reminder_two_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="confirmation-email" data-attr="confirmation_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="thank-you-email"  data-attr="thank_you_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="sorry-we-missed-you-email" data-attr="sorry_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="registration-page" data-attr="registration_config" class="template-block template-page hide">
                    <?php include('email_templates/package/registrationpage-webinar.php'); ?>
                </div>

                <div id="confirmation-page" data-attr="confirmation_config" class="template-block template-page hide">
                    <?php include('email_templates/package/confirmationpage-webinar.php'); ?>
                </div>
                <!-- /Webinar -->
                <?php } ?>


                <?php if ($tactic_type == 'email') { ?>
                <!-- Single Email -->
                <div id="single-email" data-attr="single_email_config" class="template-block template-email">
                    <?php include('email_templates/package/email-single-email.php'); ?>
                </div>
                <!-- /Single Email -->
                <?php } ?>


                <?php if ($tactic_type == 'landingpage') { ?>
                <!-- Landing Page -->

                <div id="landing-page" data-attr="landing_page_config" class="template-block template-page">
                    <?php include('email_templates/package/landingpage-landingpage.php'); ?>
                </div>

                <div id="confirmation-page" data-attr="confirmation_config" class="template-block template-page hide">
                    <?php include('email_templates/package/confirmationpage-landingpage.php'); ?>
                </div>
                <!-- /Landing Page -->
                <?php } ?>

                <!-- enurture / newsletter -->
                <?php if ($tactic_type == 'enurture' || $tactic_type == 'newsletter') { ?>

                    <!-- default templates kept here for enurture / newsletter -->
                    <?php
                         $email_count = 0; // initialize to first element
                         $default_template = 'single-email';
                         do { ?>
                            <div id="<?php echo $tactic_type; ?>-email-<?php echo $email_count; ?>" data-attr="<?php echo $tactic_type; ?>_email_config_<?php echo $email_count; ?>" class="template-block template-email <?php if($email_count!=0) echo 'hide'; ?>">
                                <?php
                                 if($tactic_type == "newsletter") // if newsletter then load different template
                                    $default_template = "newsletter"; 

                                    include('email_templates/package/email-'.$default_template.'.php'); 
                                ?>
                            </div>
                        <?php 
                         $email_count++;
                        } while($email_count!=$multiple_email_count); ?>

                        <?php
                         $landingpage_count = 0; // initialize to first element
                         do { ?>
                            <div id="<?php echo $tactic_type; ?>-landingpage-<?php echo $landingpage_count; ?>" data-attr="<?php echo $tactic_type; ?>_landingpage_config_<?php echo $landingpage_count; ?>" class="template-block template-page hide">
                                <?php include('email_templates/package/landingpage-landingpage.php'); ?>
                            </div>
                        <?php 
                         $landingpage_count++;
                         } while($landingpage_count!=$multiple_landingpage_count); ?>

                <?php } ?>

            </div>
        </div>
    </div>

    <!-- Form to store configs and attributes  -->
    <?php
            //If coming from Edit route
            if($bln_edit){
                echo '<form id="custom-template-form" name="content-edit-template-form" action="email_templates/requests.php" method="POST">';
            // Part of request form flow
            }else{
                echo '<form id="custom-template-form" class="form-horizontal" action="includes/_requests_'.$tactic_type.'.php" method="POST" enctype="multipart/form-data">';
            }
        ?>
    <!-- Hidden fields -->
    <input type="hidden" id="template" name="template" value="custom">
    <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
    <input type="hidden" id="step" name="step" value="3">
    <input type="hidden" id="c_type" name="c_type" value="<?php echo $_GET['type']; ?>">
    <input type="hidden" id="request_id" name="request_id" value="">
    <!-- <input type="hidden" id="head_option" name="head_option" value=""> -->
    <?php
        //If coming from Edit route
        if($bln_edit){
            echo '<input type="hidden" id="action" name="action" value="edittemplate">';
            echo '<input type="hidden" id="req_id" name="req_id" value="' . $edit_id . '">';
        // Part of request form flow
        }else{
            echo '<input type="hidden" id="action" name="action" value="savetemplate">';
        }
    ?>

    <?php
     $landingpage_count = 0; // initialize to first element
     do { ?>
        <textarea type="hidden" id="<?php echo $tactic_type; ?>_landingpage_config_<?php echo $landingpage_count; ?>" class="hide" name="<?php echo $tactic_type; ?>_landingpage_config_<?php echo $landingpage_count; ?>"></textarea>
    <?php 
     $landingpage_count++;
     } while($landingpage_count!=$multiple_landingpage_count); 

     $email_count = 0; // initialize to first element
     do { ?>
        <textarea type="hidden" id="<?php echo $tactic_type; ?>_email_config_<?php echo $email_count; ?>" class="hide" name="<?php echo $tactic_type; ?>_email_config_<?php echo $email_count; ?>"></textarea>
    <?php 
     $email_count++;
    } while($email_count!=$multiple_email_count); ?>
    <textarea type="hidden" id="landing_page_config" class="hide" name="landing_page_config"></textarea>
    <textarea type="hidden" id="single_email_config" class="hide" name="single_email_config"></textarea>
    <textarea type="hidden" id="invitation_email_config" class="hide" name="invitation_email_config"></textarea>
    <textarea type="hidden" id="reminder_one_email_config" class="hide" name="reminder_one_email_config"></textarea>
    <textarea type="hidden" id="reminder_two_email_config" class="hide" name="reminder_two_email_config"></textarea>
    <textarea type="hidden" id="confirmation_email_config" class="hide" name="confirmation_email_config"></textarea>
    <textarea type="hidden" id="thank_you_email_config" class="hide" name="thank_you_email_config"></textarea>
    <textarea type="hidden" id="sorry_email_config" class="hide" name="sorry_email_config"></textarea>
    <textarea type="hidden" id="registration_config" class="hide" name="registration_config"></textarea>
    <textarea type="hidden" id="confirmation_config" class="hide" name="confirmation_config"></textarea>

    </form>

    <div class="header-row">
        <div class="container-fluid">
            <header style="width:auto;">
                <div class="header-left" style="margin-top:12px;float:left;">
                    <?php
                        // echo '<a href="" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                        if($bln_edit)
                            echo '<a href="request-detail?id=' . $edit_id . '" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                        else
                            echo '<a href="request-'.$tactic_type.'-3" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                    ?>
                </div>
                <div class="header-right">
                    <a href="" id="tpl-preview" class="btn-green plain" style="margin-right:6px;">Preview Mode</a>
                    <?php if ($_SESSION['admin'] == 1) { ?>
                    	<a href="" id="download-html-file" class="btn-green plain" style="display:none;margin-right:6px" >Generate HTML</a>
                    <?php } ?>
                    <?php
                        // if($bln_edit){
                            //TODO link to update script
                            echo '<a href="#" id="submit-custom-temp-form" class="btn-green submit">Save &amp; Continue</a></div>';
                            // echo '<a href="#" id="cancel-custom-temp-form" class="btn-green">Cancel</a></div>';
                        // }else{
                            // echo '<a href="#" id="submit-request-form" class="btn-green submit">Save &amp; Continue</a></div>';
                        // }

                    ?>
            </header>
        </div>
    </div>

</div>

<?php include('includes/footer.php'); 
if((isset($_GET['edit']) && $_GET['edit'] === '1') ||  (isset($_SESSION['t_request_id']) && isset($_SESSION['templates_data']) && $switched == false)){
    echo '<script>dbRecord = \'' . json_encode($record) . '\'</script>';
}
?>
