<?php
$loc = 'content-update-request';
include('includes/head.php');
include('includes/header.php');
?>

	<div class="container">
		<div class="row intro-body">
			<div class="intro span12">
				<h1>Content Update Request</h1>
				<p>Please use this page to submit your request to the website team. Before you begin, be sure that you have completed a <a href="briefing-docs">briefing document</a>, including exact details of requested updates, final and approved copy and any approved assets.</p>
				<p>For guidelines about creating and submitting content, visit the <a href="files/WEB.com Program Overview User Guide.pptx" data-analytics-label="Download File: WEB.com Program Overview User Guide.pptx">User's Guide</a> and <a href="files/WEB.com Design, Editorial, Social Media guidelines.pptx" data-analytics-label="Download File: WEB.com Design, Editorial, Social Media guidelines.pptx">Style Guides</a>. For questions about submissions that are not answered in the User's Guide, <a href="mailto:webwebteam@microsoft.com">email our website team</a>. Please allow three working days for a response.</p>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<form id="content-update-form" action="includes/_requests.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" id="action" name="action" value="create">
				<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
				<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<div class="accordion" id="accordion0">
						<div class="accordion-group">
							<div class="accordion-heading"><a class="accordion-toggle notrack" data-toggle="collapse" data-parent="#accordion0" href="#collapseZero">Step 1</a></div>
							<div id="collapseZero" class="accordion-body collapse in">
								<div class="accordion-inner">
									<label for="title" class="pull-left">Title of your request?</label>
									<input type="text" id="title" name="title" value="" class="input-xlarge required">
								</div>
							</div>
						</div>
					</div>
					<div class="accordion" id="accordion1">
						<div class="accordion-group">
							<div class="accordion-heading"><a class="accordion-toggle notrack" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">Step 2</a></div>
							<div id="collapseOne" class="accordion-body collapse in">
								<div class="accordion-inner">
									<label for="request_type" class="pull-left">What's the nature of your request? <span class="note">(See User's Guide for explanations.)</span></label>
									<ul class="form-options">
										<li><label class="radio inline"> <input type="radio" id="request_type[]" name="request_type" class="timeline-radio" value="Urgent update"> <strong>Urgent</strong> (Incorrect content, branding changes, broken links, site malfunctions, etc.)</label></li>
										<li><label class="radio inline"> <input type="radio" id="request_type[]" name="request_type" class="timeline-radio" value="Content refresh" checked="checked"> <strong>Content refresh</strong> (New photos and videos, non-critical copy changes, new white papers, analyst reports or other resources, events, etc.)</label></li>
										<li><label class="radio inline"> <input type="radio" id="request_type[]" name="request_type" class="timeline-radio" value="Substantive change/new pages"> <strong>Substantive</strong> (New campaigns, forecasted product releases, wholesale updates to programs/services, modification of tools, and updates that require translation, tagging, design/development)</label></li>
										<li><label class="radio inline"> <input type="radio" id="request_type[]" name="request_type" class="timeline-radio" value="Other"> <strong>Other</strong> (Anything not included in the above; will be assessed on an individual basis)</label></li>
									</ul>
									<div class="hidden other" style="display:none;clear:both;">Time and materials required for completion will be established upon receipt of request.</div>
								</div>
							</div>
						</div>
					</div>
					<div class="accordion" id="accordion2">
						<div class="accordion-group">
							<div class="accordion-heading"><a class="accordion-toggle notrack" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Step 3</a></div>
							<div id="collapseTwo" class="accordion-body collapse in">
								<div class="accordion-inner">
									<label for="timeline" class="pull-left">What is your timeline? <span class="note">(See User's Guide for explanations. Note that requests flagged "urgent" will be assessed on a case-by-case basis; the website team will contact you to discuss.)</span></label>
									<div style="clear:both;padding-top:8px;">
										<label class="radio inline"> <input type="radio" id="timeline[]" name="timeline" class="timeline-radio" value="Urgent"> Urgent</label>
										<label class="radio inline"> <input type="radio" id="timeline[]" name="timeline" class="timeline-radio" value="General update (6 days)" checked="checked"> General update (6 days)</label>
										<label class="radio inline"> <input type="radio" id="timeline[]" name="timeline" class="timeline-radio" value="Defined"> Date defined by requestor</label>
										<div id="datetimepicker" class="input-append date" style="margin-bottom:-6px;display:none;">
											<input type="text" id="desired_date" name="desired_date" class="input-small">
											<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="accordion" id="accordion3">
						<div class="accordion-group">
							<div class="accordion-heading"><a class="accordion-toggle notrack" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">Step 4</a></div>
							<div id="collapseThree" class="accordion-body collapse in">
								<div class="accordion-inner">
									<label for="upload_doc" class="pull-left">Please upload the briefing document with your changes <span style="font-weight:normal;">(Max: 30MB)</span></label>
									<div class="span5 pull-left upload-div">
									    <div class="fileupload fileupload-new" data-provides="fileupload">
											<span class="btn btn-file"><span class="fileupload-new">Select files</span><span class="fileupload-exists">Change</span><input id="fileupload" type="file" name="files[]" multiple></span>
										</div>
										<div id="progress-files" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 108px;display:none;">
									        <div class="bar"></div>
									    </div>
									    <div id="files" class="files" style="clear:both;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="accordion" id="accordion4">
						<div class="accordion-group">
							<div class="accordion-heading"><a class="accordion-toggle notrack" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour">Step 5</a></div>
							<div id="collapseFour" class="accordion-body collapse in">
								<div class="accordion-inner">
									<label for="upload_assets" class="pull-left">Please upload your assets <span style="font-weight:normal;">(Max: 30MB)</span></label>
									<div class="span7 pull-left upload-div">
										<div class="fileupload fileupload-new" data-provides="fileupload">
											<span class="btn btn-file"><span class="fileupload-new">Select files</span><span class="fileupload-exists">Change</span><input id="assetupload" type="file" name="files[]" multiple></span>
										</div>
										<div id="progress-assets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 108px;display:none;">
									        <div class="bar"></div>
									    </div>
									    <div id="assetlist" class="files" style="clear:both;"></div>
									</div>
									<div class="clearfix"></div>
									<label for="external_title" class="pull-left">Asset URL's</label>
									<div id="eternal-assets" class="span8 pull-left external-div" style="margin-left:200px;">
										<input type="text" name="external_title[0]" value="" placeholder="Title" class="pull-left">
										<input type="text" name="external_url[0]" value="" placeholder="URL" class="pull-left" style="margin-left:12px;">
										<a href="" id="addInput" class="btn-orange add add_external pull-left" style="margin-left:12px;">Add more</a>
										<div style="clear:both;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="accordion" id="accordion5" style="margin-bottom:0;">
						<div class="accordion-group">
							<div class="accordion-heading"><a class="accordion-toggle notrack" data-toggle="collapse" data-parent="#accordion5" href="#collapseFive">Step 6</a></div>
							<div id="collapseFive" class="accordion-body collapse in">
								<div class="accordion-inner" style="padding-bottom:12px;">
									<label for="guidelines">Additional comments. Please provide any additional information you think will be helpful in processing your request.</label>
									<textarea id="guidelines" name="guidelines" rows="6" cols="45" class="input-full"></textarea>
								</div>
							</div>
						</div>
					</div>
					<p style="margin:0 0 40px 4px;"><a href="" id="submit-request-form" class="btn-green submit" data-analytics-label="Submit Form: Content Update Request">Submit</a><?php //<input type="submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Content Update Request" value="Submit"> ?></p>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>