<?php
$loc = 'campaigns';
include('includes/head.php');
include('includes/header.php');

require_once('includes/_globals.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
            	<h1>Campaigns</h1>
                <p>Need text here.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="intro span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
			                req.*,
			                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
			                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
			                status.value AS status_value,
			                request_type.request_type AS request_type,
			                request_type.id AS request_type_id,
			                users.fname AS fname,
			                users.lname AS lname,
			                users.email AS email
			            FROM request AS req
			            INNER JOIN status on req.status = status.id
			            INNER JOIN request_type on req.request_type = request_type.id
			            INNER JOIN users on req.user_id = users.id";

			    //This would be the All Requests view
			    if ($_SESSION['admin'] == 1) {
			        $query.=" WHERE req.active = 1 AND req.request_type != 8"; //IF WE WANT TO ADD CAMPAIGNS BACK HERE, CHANGE THIS
			    } else {
			        //$query.=" WHERE req.user_id = '".$_SESSION['user_id']."' AND req.request_type != 8 AND req.active = 1";
			        $query.=" WHERE req.active = 1 AND req.request_type != 8"; //I guess we're showing all campaigns for all users
			    }

			    //$query.=" WHERE req.active = 1";

			    $query.=" ORDER BY req.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			//print_r($query);

    			if ($row_cnt > 0) {
			        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
			        echo '<thead><th>Campaign Name <b class="icon-white"></b></th><th>Tactic <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th></thead>';
			        echo '<tbody>';
			        while ($obj = $result->fetch_object()) {
			            $status_trimmed = "New";
			            switch ($obj->status) {
			                case 1:
			                    $status_trimmed = "New";
			                    break;
			                case 2:
			                    $status_trimmed = "In review";
			                    break;
			                case 3:
			                    $status_trimmed = "Scheduled";
			                    break;
			                case 4:
			                    $status_trimmed = "Complete";
			                    break;
			            }

			            $query_campaign = "SELECT
			            						request_campaign.id AS campaign_id,
				            					request_campaign.name AS campaign_name
				            				FROM request_campaign
				            				WHERE request_campaign.id = ".$obj->campaign_id."
				            				LIMIT 1";
			            $result_campaign = $mysqli->query($query_campaign);

    					while ($obj_campaign = $result_campaign->fetch_object()) {
    						$campaign_link = '<a href="campaign-detail?id='.$obj_campaign->campaign_id.'" class="notrack">'.$obj_campaign->campaign_name.'</a>';
    					}

    					//print_r( $query_campaign);

			            echo '<tr>';
			            echo '<td>'.$campaign_link.'</td>';
			            echo '<td><a href="request-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
			            //echo '<td>'.$obj->fname.' '.$obj->lname.'</td>';
			            echo '<td>'.$obj->request_type.'</td>';
			            echo '<td style="white-space:nowrap;"><div style="display:none;">'.$obj->status.'</div>'.$status_trimmed.'</td>';
			            echo '</tr>';
			        }
			        echo '</tbody></table>';
			    } else {
			        echo '<p><strong>There are currently no active campaigns.</strong></p>';
			    }

            	?>
            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>