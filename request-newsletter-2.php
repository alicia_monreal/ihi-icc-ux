<?php
$loc = 'campaign-requests';
$step = 2;
$c_type = 'newsletter';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Newsletter Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fiveup">
				<li>Step 1</li>
				<li class="active">Step 2</li>
				<li>Step 3</li>
				<li>Step 4</li>
				<li>Step 5</li>
			</ul>
			<div class="request-step step2">
				<div class="fieldgroup">
					<p>Please fill out the tactic details below and a member of production support will contact you shortly to discuss your request.  The newsletter tactic will use a single email template from which the newsletter will be delivered to the target audience on a defined cadence.  If you are unsure if when the next communication is to be delivered or if you wish each newsletter design to be different, please use the single email tactic type.</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p><p>Click on the <i class="icon-question-sign" style="margin-top: 3px;"></i> for more information and examples.</p>
					<h3 class="fieldgroup-header">Tactic Details</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_newsletter.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="2">
					<input type="hidden" id="c_type" name="c_type" value="newsletter">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="in_progress_form" name="in_progress_form" value="0">

					<div class="fieldgroup">
						<?php include('includes/tactic-basics.php'); ?>
						<div class="control-group">
							<label for="start_date" class="control-label">Newsletter Start Date</label>
							<div class="controls">
								<span class="required-mark">*</span><div id="datetimepicker" class="input-append date" style=""><input type="text" id="start_date" name="start_date" class="input-medium required" style="width:134px;" value="<?php echo $_SESSION['start_date']; ?>"><span class="add-on" style="margin-right:31px;"><i class="icon-th"></i></span></div>
								<span style="font-weight: 600;">&nbsp;End Date</span><span class="required-mark" style="margin-left:5px">*</span> <div id="datetimepicker_end" class="input-append date" style=""><input type="text" id="end_date" name="end_date" class="input-medium required" style="width:134px;" value="<?php echo $_SESSION['end_date']; ?>"><span class="add-on"><i class="icon-th"></i></span></div>
							</div>
						</div>
						<div class="control-group">
							<label for="send_date" class="control-label">Newsletter Send Time</label>
							<div class="controls">
								<span class="required-mark" style="margin-right: 5px;">*</span> <select id="send_time" name="send_time" class="input-medium required" style="width:120px;">
										<option value="">Select One</option>
										<option value="01:00" <?php if ($_SESSION['send_time'] == '01:00') { echo 'selected'; } ?>>01:00</option>
										<option value="01:30" <?php if ($_SESSION['send_time'] == '01:30') { echo 'selected'; } ?>>01:30</option>
										<option value="02:00" <?php if ($_SESSION['send_time'] == '02:00') { echo 'selected'; } ?>>02:00</option>
										<option value="02:30" <?php if ($_SESSION['send_time'] == '02:30') { echo 'selected'; } ?>>02:30</option>
										<option value="03:00" <?php if ($_SESSION['send_time'] == '03:00') { echo 'selected'; } ?>>03:00</option>
										<option value="03:30" <?php if ($_SESSION['send_time'] == '03:30') { echo 'selected'; } ?>>03:30</option>
										<option value="04:00" <?php if ($_SESSION['send_time'] == '04:00') { echo 'selected'; } ?>>04:00</option>
										<option value="04:30" <?php if ($_SESSION['send_time'] == '04:30') { echo 'selected'; } ?>>04:30</option>
										<option value="05:00" <?php if ($_SESSION['send_time'] == '05:00') { echo 'selected'; } ?>>05:00</option>
										<option value="05:30" <?php if ($_SESSION['send_time'] == '05:30') { echo 'selected'; } ?>>05:30</option>
										<option value="06:00" <?php if ($_SESSION['send_time'] == '06:00') { echo 'selected'; } ?>>06:00</option>
										<option value="06:30" <?php if ($_SESSION['send_time'] == '06:30') { echo 'selected'; } ?>>06:30</option>
										<option value="07:00" <?php if ($_SESSION['send_time'] == '07:00') { echo 'selected'; } ?>>07:00</option>
										<option value="07:30" <?php if ($_SESSION['send_time'] == '07:30') { echo 'selected'; } ?>>07:30</option>
										<option value="08:00" <?php if ($_SESSION['send_time'] == '08:00') { echo 'selected'; } ?>>08:00</option>
										<option value="08:30" <?php if ($_SESSION['send_time'] == '08:30') { echo 'selected'; } ?>>08:30</option>
										<option value="09:00" <?php if ($_SESSION['send_time'] == '09:00') { echo 'selected'; } ?>>09:00</option>
										<option value="09:30" <?php if ($_SESSION['send_time'] == '09:30') { echo 'selected'; } ?>>09:30</option>
										<option value="10:00" <?php if ($_SESSION['send_time'] == '10:00') { echo 'selected'; } ?>>10:00</option>
										<option value="10:30" <?php if ($_SESSION['send_time'] == '10:30') { echo 'selected'; } ?>>10:30</option>
										<option value="11:00" <?php if ($_SESSION['send_time'] == '11:00') { echo 'selected'; } ?>>11:00</option>
										<option value="11:30" <?php if ($_SESSION['send_time'] == '11:30') { echo 'selected'; } ?>>11:30</option>
										<option value="12:00" <?php if ($_SESSION['send_time'] == '12:00') { echo 'selected'; } ?>>12:00</option>
										<option value="12:30" <?php if ($_SESSION['send_time'] == '12:30') { echo 'selected'; } ?>>12:30</option>
									</select><select id="send_ampm" name="send_ampm" style="width: 58px;"><option value="AM" <?php if (!isset($_SESSION['send_ampm']) || $_SESSION['send_ampm'] == 'AM') { echo 'selected'; } ?>>AM</option><option value="PM" <?php if ($_SESSION['send_ampm'] == 'PM') { echo 'selected'; } ?>>PM</option></select>
							</div>
						</div>
						<div class="control-group">
							<label for="time_zone" class="control-label">Time Zone</label>
							<div class="controls"><span class="required-mark">*</span><select id="time_zone" name="time_zone" class="input-medium required">
								<option value="">Select One</option>
								<?php
								reset($timezones_array);
								foreach ($timezones_array as $value_tz) {
    								echo '<option value="'.$value_tz.'"'; if ($_SESSION['time_zone'] == $value_tz) { echo ' selected'; } echo '>'.$value_tz.'</option>';
								}
								?>
							</select></div>
						</div>
						<div class="control-group">
							<label for="cadence" class="control-label">Newsletter Cadence</label>
							<div class="controls">
								<span class="required-mark">*</span><select id="cadence" name="cadence" class="required">
									<option value="">Select amount</option>
									<option value="Weekly"<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Weekly') { echo ' selected'; } ?>>Weekly</option>
									<option value="Biweekly"<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Biweekly') { echo ' selected'; } ?>>Biweekly</option>
									<option value="Monthly"<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Monthly') { echo ' selected'; } ?>>Monthly</option>
									<option value="Bimonthly"<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Bimonthly') { echo ' selected'; } ?>>Bimonthly</option>
									<option value="Quarterly"<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Quarterly') { echo ' selected'; } ?>>Quarterly</option>
									<option value="Other"<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Other') { echo ' selected'; } ?>>Other</option>
								</select>
								<input type="text" id="cadence_other" name="cadence_other" class="input-wide input-note" placeholder="Please provide your newsletter cadence" value="<?php echo getFieldValue('cadence_other', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>" style="clear:both;<?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') != 'Other') { echo 'display:none;'; } ?>" <?php if (getFieldValue('cadence', $_SESSION['clone_id'], $_SESSION['request_type'], '') != 'Other') { echo 'disabled'; } ?>>
							</div>
						</div>
						<div class="control-group">
							<label for="description" class="control-label">Description</label>
							<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="description" name="description" class="input-wide textarea-medium required"><?php echo getFieldValue('description', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
						</div>
						<?php
						//Taking out. Hubert. 03/09
						/*
						<div class="control-group">
							<label for="number_emails" class="control-label">Number of Emails</label>
							<div class="controls">
								<span class="required-mark">*</span><select id="number_emails" name="number_emails" class="required">
									<option value="">Select amount</option>
									<option value="1-5"<?php if ($_SESSION['number_emails'] == '1-5') { echo ' selected'; } ?>>1-5</option>
									<option value="6-10"<?php if ($_SESSION['number_emails'] == '6-10') { echo ' selected'; } ?>>6-10</option>
									<option value="10-15"<?php if ($_SESSION['number_emails'] == '10-15') { echo ' selected'; } ?>>10-15</option>
									<option value="16+"<?php if ($_SESSION['number_emails'] == '16+') { echo ' selected'; } ?>>16+</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label for="number_landing_pages" class="control-label">Number of Landing Pages</label>
							<div class="controls">
								<span class="required-mark">*</span><select id="number_landing_pages" name="number_landing_pages" class="required">
									<option value="">Select amount</option>
									<option value="0"<?php if ($_SESSION['number_landing_pages'] == '0') { echo ' selected'; } ?>>0</option>
									<option value="1-5"<?php if ($_SESSION['number_landing_pages'] == '1-5') { echo ' selected'; } ?>>1-5</option>
									<option value="6-10"<?php if ($_SESSION['number_landing_pages'] == '6-10') { echo ' selected'; } ?>>6-10</option>
									<option value="10-15"<?php if ($_SESSION['number_landing_pages'] == '10-15') { echo ' selected'; } ?>>10-15</option>
									<option value="16+"<?php if ($_SESSION['number_landing_pages'] == '16+') { echo ' selected'; } ?>>16+</option>
								</select>
							</div>
						</div>
						*/
						?>
						<div class="control-group">
							<label for="asset_reviewers" class="control-label">Asset Reviewers</label>
							<div class="controls"><textarea id="asset_reviewers" name="asset_reviewers" class="input-wide textarea-medium" placeholder="Provide a list of names and email addresses for the approval process."><?php echo getFieldValue('asset_reviewers', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
						</div>
					<h3 class="fieldgroup-header">Email Details</h3>
					</div>

					<div class="fieldgroup">
						<?php include('includes/email-basics.php'); ?>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request" class="btn-green back pull-left">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" style="margin-left:8px;">Next</a><a href="" id="save-and-exit" class="btn-green submit plain pull-right">Save &amp; Exit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>