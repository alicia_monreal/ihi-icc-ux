<?php
session_start();
include('../includes/_globals.php');

//--------------------------------------------------------------------------------------------------
// This script reads event data from a JSON file and outputs those events which are within the range
// supplied by the "start" and "end" GET parameters.
//
// An optional "timezone" GET parameter will force all ISO8601 date stings to a given timezone.
//
// Requires PHP 5.2.0 or higher.
//--------------------------------------------------------------------------------------------------

// Require our Event class and datetime utilities
require dirname(__FILE__) . '/utils.php';

// Short-circuit if the client did not give us a date range.
if (!isset($_GET['start']) || !isset($_GET['end'])) {
	die("Please provide a date range.");
}

// Parse the start/end parameters.
// These are assumed to be ISO8601 strings with no time nor timezone, like "2013-12-29".
// Since no timezone will be present, they will parsed as UTC.
$range_start = parseDateTime($_GET['start']);
$range_end = parseDateTime($_GET['end']);

// Parse the timezone parameter if it is present.
$timezone = null;
if (isset($_GET['timezone'])) {
	$timezone = new DateTimeZone($_GET['timezone']);
}

// Read and parse our events JSON file into an array of event data arrays.
//$json = file_get_contents(dirname(__FILE__) . '/events.json');

$json = array();

//Webinars
if (in_array('all', $_GET['tactic_type']) || in_array('webinar', $_GET['tactic_type']) || in_array('placeholder', $_GET['tactic_type'])) {
	$query = "SELECT
				request_webinar.*,
				DATE_FORMAT(request_webinar.start_date,  '%Y-%m-%d') AS date_cleaned,
				request_campaign.geo,
				request_campaign.sector,
				request.status AS request_status
			FROM request_webinar
			INNER JOIN request on request_webinar.request_id = request.id
			INNER JOIN request_campaign ON request.campaign_id = request_campaign.id
			INNER JOIN status ON request.status = status.id
			WHERE request.active = 1
			AND status.id != 11";

	//Owner
	if (in_array('mine', $_GET['owner'])) { $query.= " AND request.user_id = ".$_SESSION['user_id']; }

	if ($_GET['campaign'] != '' && $_GET['campaign'] != 'all') { $query.=" AND request.campaign_id = ".$_GET['campaign']; }
	
	if (!in_array('all', $_GET['sector'])) { //Sector has been selected
		$cnt_w = 1;
		if (count($_GET['sector']) >= 1) {
			$query.=" AND";
			foreach ($_GET['sector'] as $val) {
    			$query.=" request_campaign.sector = '".$val."'";
    			if ($cnt_w < count($_GET['sector'])) {
    				$query.=" OR";
    			}
    			$cnt_w++;
			}
		}
	}

	$result = $mysqli->query($query);
	$row_cnt = $result->num_rows;

	if ($row_cnt > 0) {
	    while ($obj = $result->fetch_object()) {
	    	
	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('webinar', $_GET['tactic_type']) 
	    		&& $obj->request_status != 14)  // if not webinar then don't show them
	    		continue;

			if(!in_array('all', $_GET['tactic_type']) && !in_array('placeholder', $_GET['tactic_type']) 
				&& $obj->request_status == 14)  // if not placeholder then don't show them
	    		continue;	    	

	    	$class_name = "cal-webinar";
	    	if($obj->request_status == 14) {
	    		$class_name = "cal-placeholder";
	    	}

	    	$geo = explode(", ", $obj->geo);

	    	if (!in_array('all', $_GET['geo'])) { //GEO or Sector have been selected
	    		if (array_intersect($_GET['geo'], $geo) === 0) {
			    	
			    } else {
			    	$json[] = array(
				        'id' => $obj->request_id,
				        'title' => $obj->tag,
				        'url' => 'calendar-detail?id='.$obj->request_id,
				        'className' => $class_name,
				        'start' => $obj->date_cleaned.' '.$obj->start_time,
				        'end' => $obj->date_cleaned.' '.$obj->end_time
				    );
			    }
		    } else { //Showing all
		    	$json[] = array(
			        'id' => $obj->request_id,
			        'title' => $obj->tag,
			        'url' => 'calendar-detail?id='.$obj->request_id,
			        'className' => $class_name,
			        'start' => $obj->date_cleaned.' '.$obj->start_time,
			        'end' => $obj->date_cleaned.' '.$obj->end_time
			    );
		    }
	    }
	}
}


//eNurture
if (in_array('all', $_GET['tactic_type']) || in_array('enurture', $_GET['tactic_type']) || in_array('placeholder', $_GET['tactic_type'])) {
	$query = 'SELECT
				request_enurture.*,
				request_campaign.geo,
				request_campaign.sector,
				request.status AS request_status
			FROM request_enurture
			INNER JOIN request on request_enurture.request_id = request.id
			INNER JOIN request_campaign ON request.campaign_id = request_campaign.id
			INNER JOIN status ON request.status = status.id
			WHERE request.active = 1
			AND status.id != 11';

	//Owner
	if (in_array('mine', $_GET['owner'])) { $query.= " AND request.user_id = ".$_SESSION['user_id']; }

	if ($_GET['campaign'] != '' && $_GET['campaign'] != 'all') { $query.=" AND request.campaign_id = ".$_GET['campaign']; }

	if (!in_array('all', $_GET['sector'])) { //Sector has been selected
		$cnt_en = 1;
		if (count($_GET['sector']) >= 1) {
			$query.=" AND";
			foreach ($_GET['sector'] as $val) {
    			$query.=" request_campaign.sector = '".$val."'";
    			if ($cnt_en < count($_GET['sector'])) {
    				$query.=" OR";
    			}
    			$cnt_en++;
			}
		}
	}

	//Owner
	if (in_array('mine', $_GET['owner'])) { $query.= "AND request.user_id = ".$_SESSION['user_id']; }

	$result = $mysqli->query($query);
	$row_cnt = $result->num_rows;

	if ($row_cnt > 0) {
	    while ($obj = $result->fetch_object()) {

	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('enurture', $_GET['tactic_type']) 
	    		&& $obj->request_status != 14) // if not enurture then don't show them
	    		continue;

	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('placeholder', $_GET['tactic_type']) 
	    		&& $obj->request_status == 14) // if not placeholder then don't show them
	    		continue;

	    	$class_name = "cal-enurture";
	    	if($obj->request_status == 14)
	    		$class_name = "cal-placeholder";

	    	$geo = explode(", ", $obj->geo);

	    	if (!in_array('all', $_GET['geo'])) { //GEO or Sector have been selected
	    		if (array_intersect($_GET['geo'], $geo) === 0) {
			    	
			    } else {
			    	$json[] = array(
				        'id' => $obj->request_id,
				        'title' => $obj->tag,
				        'url' => 'calendar-detail?id='.$obj->request_id,
				        'className' => $class_name,
				        'start' => str_replace(' 00:00:00', '', $obj->start_date),
				        'end' => str_replace(' 00:00:00', '', $obj->end_date)
				    );
			    }
		    } else { //Showing all
		    	$json[] = array(
			        'id' => $obj->request_id,
			        'title' => $obj->tag,
			        'url' => 'calendar-detail?id='.$obj->request_id,
			        'className' => $class_name,
			        'start' => str_replace(' 00:00:00', '', $obj->start_date),
			        'end' => str_replace(' 00:00:00', '', $obj->end_date)
			    );
		    }
	    }
	}
}

//Newsletter
if (in_array('all', $_GET['tactic_type']) || in_array('newsletter', $_GET['tactic_type']) || in_array('placeholder', $_GET['tactic_type'])) {
	$query = 'SELECT request_newsletter.*,
				request_campaign.geo,
				request_campaign.sector,
				request.status AS request_status
			FROM request_newsletter
			INNER JOIN request on request_newsletter.request_id = request.id
			INNER JOIN request_campaign ON request.campaign_id = request_campaign.id
			INNER JOIN status ON request.status = status.id
			WHERE request.active = 1
			AND status.id != 11';

	//Owner
	if (in_array('mine', $_GET['owner'])) { $query.= " AND request.user_id = ".$_SESSION['user_id']; }

	if ($_GET['campaign'] != '' && $_GET['campaign'] != 'all') { $query.=" AND request.campaign_id = ".$_GET['campaign']; }

	if (!in_array('all', $_GET['sector'])) { //Sector has been selected
		$cnt_n = 1;
		if (count($_GET['sector']) >= 1) {
			$query.=" AND";
			foreach ($_GET['sector'] as $val) {
    			$query.=" request_campaign.sector = '".$val."'";
    			if ($cnt_n < count($_GET['sector'])) {
    				$query.=" OR";
    			}
    			$cnt_n++;
			}
		}
	}

	$result = $mysqli->query($query);
	$row_cnt = $result->num_rows;

	if ($row_cnt > 0) {
	    while ($obj = $result->fetch_object()) {

	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('newsletter', $_GET['tactic_type']) 
	    		&& $obj->request_status != 14) // if not newsletter then don't show them
	    		continue;

	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('placeholder', $_GET['tactic_type']) 
	    		&& $obj->request_status == 14)  // if not placeholder then don't show them
	    		continue;

	    	$class_name = "cal-newsletter";
	    	if($obj->request_status == 14)
	    		$class_name = "cal-placeholder";

	    	$geo = explode(", ", $obj->geo);

	    	if (!in_array('all', $_GET['geo'])) { //GEO or Sector have been selected
	    		if (array_intersect($_GET['geo'], $geo) === 0) {
			    	
			    } else {
			    	$json[] = array(
				        'id' => $obj->request_id,
				        'title' => $obj->tag,
				        'url' => 'calendar-detail?id='.$obj->request_id,
				        'className' => $class_name,
				        'start' => str_replace(' 00:00:00', '', $obj->start_date).' '.$obj->send_time,
					    'end' => str_replace(' 00:00:00', '', $obj->end_date)
				    );
				}
		    } else { //Showing all
		    	$json[] = array(
			        'id' => $obj->request_id,
			        'title' => $obj->tag,
			        'url' => 'calendar-detail?id='.$obj->request_id,
			        'className' => $class_name,
			        'start' => str_replace(' 00:00:00', '', $obj->start_date).' '.$obj->send_time,
				    'end' => str_replace(' 00:00:00', '', $obj->end_date)
			    );
		    }
	    }
	}
}

//Emails
if (in_array('all', $_GET['tactic_type']) || in_array('email', $_GET['tactic_type']) || in_array('placeholder', $_GET['tactic_type'])) {
	$query = "SELECT request_email.*,
				DATE_FORMAT(request_email.send_date,  '%Y-%m-%d') AS date_cleaned,
				request_campaign.geo,
				request_campaign.sector,
				request.status AS request_status
			FROM request_email
			INNER JOIN request on request_email.request_id = request.id
			INNER JOIN request_campaign ON request.campaign_id = request_campaign.id
			INNER JOIN status ON request.status = status.id
			WHERE request.active = 1
			AND status.id != 11";

	//Owner
	if (in_array('mine', $_GET['owner'])) { $query.= " AND request.user_id = ".$_SESSION['user_id']; }

	if ($_GET['campaign'] != '' && $_GET['campaign'] != 'all') { $query.=" AND request.campaign_id = ".$_GET['campaign']; }

	if (!in_array('all', $_GET['sector'])) { //Sector has been selected
		$cnt_e = 1;
		if (count($_GET['sector']) >= 1) {
			$query.=" AND";
			foreach ($_GET['sector'] as $val) {
    			$query.=" request_campaign.sector = '".$val."'";
    			if ($cnt_e < count($_GET['sector'])) {
    				$query.=" OR";
    			}
    			$cnt_e++;
			}
		}
	}

	$result = $mysqli->query($query);
	$row_cnt = $result->num_rows;
	if ($row_cnt > 0) {
	    while ($obj = $result->fetch_object()) {

	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('email', $_GET['tactic_type']) 
	    		&& $obj->request_status != 14) // if not email then don't show them
	    		continue;

	    	if(!in_array('all', $_GET['tactic_type']) && !in_array('placeholder', $_GET['tactic_type']) 
	    		&& $obj->request_status == 14) // if not placeholder then don't show them
	    		continue;

	    	$class_name = "cal-email";
	    	if($obj->request_status == 14)
	    		$class_name = "cal-placeholder";

	    	$geo = explode(", ", $obj->geo);

	    	if (!in_array('all', $_GET['geo'])) { //GEO or Sector have been selected
	    		if (array_intersect($_GET['geo'], $geo) === 0) {
			    	
			    } else {
			    	$json[] = array(
				        'id' => $obj->request_id,
				        'title' => $obj->tag,
				        'url' => 'calendar-detail?id='.$obj->request_id,
				        'className' => $class_name,
				        'start' => $obj->date_cleaned.' '.$obj->send_time,
				        'end' => $obj->date_cleaned
				    );
			    }
			} else { //Showing all
				$json[] = array(
			        'id' => $obj->request_id,
			        'title' => $obj->tag,
			        'url' => 'calendar-detail?id='.$obj->request_id,
			        'className' => $class_name,
			        'start' => $obj->date_cleaned.' '.$obj->send_time,
			        'end' => $obj->date_cleaned
			    );
			}
	    }
	}
}

$json_enc = json_encode($json);
$input_arrays = json_decode($json_enc, true);


// Accumulate an output array of event data arrays.
$output_arrays = array();
foreach ($input_arrays as $array) {

	// Convert the input array into a useful Event object
	$event = new Event($array, $timezone);

	// If the event is in-bounds, add it to the output
	if ($event->isWithinDayRange($range_start, $range_end)) {
		$output_arrays[] = $event->toArray();
	}
}

// Send JSON to the client.
echo json_encode($output_arrays);