<?php
include('../includes/_globals.php');

/*
$db    = new PDO('mysql:host=localhost;dbname=intelupd_dev;charset=utf8', 'intelupd-dev', 'f19fM9lTIT3ADTVQ');
$start = $_REQUEST['from'] / 1000;
$end   = $_REQUEST['to'] / 1000;
$sql   = sprintf('SELECT * FROM trainings WHERE `date` BETWEEN %s and %s',
    $db->quote(date('Y-m-d', $start)), $db->quote(date('Y-m-d', $end)));

foreach($db->query($sql) as $row) {

	echo 'hello';

    $out[] = array(
        'id' => $row->id,
        'title' => $row->title,
        'url' => $row->id,
        'start' => strtotime($row->date) . '000'
    );
};

*/

$start = $_REQUEST['from'] / 1000;
$end   = $_REQUEST['to'] / 1000;
$from = date('Y-m-d 00:00:00', $start);
$to = date('Y-m-d 00:00:00', $end);
$out = array();

//$query = 'SELECT * FROM trainings WHERE `start` BETWEEN "'.$from.'" and "'.$to.'"';

//$query = 'SELECT * FROM request_webinar WHERE start_time BETWEEN "'.$from.'" and "'.$to.'"';

$query = 'SELECT * FROM request_webinar';
//print_r($query);
$result = $mysqli->query($query);
$row_cnt = $result->num_rows;

if ($row_cnt > 0) {
    while ($obj = $result->fetch_object()) {
    	$out[] = array(
	        'id' => $obj->id,
	        'title' => $obj->name,
	        'url' => $obj->id,
	        'class' => 'webinar',
	        'start' => strtotime($obj->start_time) . '000',
	        'end' => strtotime($obj->end_time) . '000'
	    );
    }
}



echo json_encode(array('success' => 1, 'result' => $out));
exit;
?>

{
	"success": 1,
	"result": [
		{
			"id": "293",
			"title": "This is warning class event with very long title to check how it fits to evet in day view",
			"url": "http://www.example.com/",
			"class": "event-warning",
			"start": "1362938400000",
			"end":   "1363197686300"
		},
		{
			"id": "256",
			"title": "Event that ends on timeline",
			"url": "http://www.example.com/",
			"class": "event-warning",
			"start": "1363155300000",
			"end":   "1363227600000"
		},
		{
			"id": "276",
			"title": "Short day event",
			"url": "http://www.example.com/",
			"class": "event-success",
			"start": "1363245600000",
			"end":   "1363252200000"
		},
		{
			"id": "294",
			"title": "This is information class ",
			"url": "http://www.example.com/",
			"class": "event-info",
			"start": "1363111200000",
			"end":   "1363284086400"
		},
		{
			"id": "297",
			"title": "This is success event",
			"url": "http://www.example.com/",
			"class": "event-success",
			"start": "1363234500000",
			"end":   "1363284062400"
		},
		{
			"id": "54",
			"title": "This is simple event",
			"url": "http://www.example.com/",
			"class": "",
			"start": "1363712400000",
			"end":   "1363716086400"
		},
		{
			"id": "532",
			"title": "This is inverse event",
			"url": "http://www.example.com/",
			"class": "event-inverse",
			"start": "1364407200000",
			"end":   "1364493686400"
		},
		{
			"id": "548",
			"title": "This is special event",
			"url": "http://www.example.com/",
			"class": "event-special",
			"start": "1363197600000",
			"end":   "1363629686400"
		},
		{
			"id": "295",
			"title": "Event 3",
			"url": "http://www.example.com/",
			"class": "event-important",
			"start": "1364320800000",
			"end":   "1364407286400"
		}
	]
}
