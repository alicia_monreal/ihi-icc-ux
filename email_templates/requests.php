<?php
    session_start();
    require_once('../includes/validate.php');
    include('../includes/_globals.php');

    if (!empty($_POST) && !empty($_POST['action'])) {


        $post_data = $_POST;

        if($post_data['action'] === 'edittemplate' && $post_data['template'] === 'custom'){
            
            $edit_req_id = $post_data['req_id'];
            // save_custom_template('111',$post_data);
            if($post_data['c_type'] == "enurture" || $post_data['c_type'] == "newsletter") {
                // if type is enurture or newsletter then delete existing templates data
                $query_remove = 'DELETE FROM multiple_templates_data WHERE request_id = '.$edit_req_id;
                $mysqli->query($query_remove);

                $multiple_emails = $multiple_landingpages = array();
                foreach($post_data as $key => $value) {
                    $key_name = explode("_",$key);

                    if($key_name[0] == "enurture" || $key_name[0] == "newsletter") { // if type is enurture or newsletter
                        if($key_name[1] == "email") { // if type is email
                            $multiple_emails[] = $value;
                        } else if ($key_name[1] == "landingpage") { // if type is landing page
                            $multiple_landingpages[] = $value;
                        }
                    }
                }

                if(!empty($multiple_emails)) { // if not empty multiple emails
                    // multiple email records insertion
                    foreach($multiple_emails as $key => $value) {
                     $query = 'INSERT INTO multiple_templates_data VALUES (
                                        NULL,
                                        "' .$_SESSION['user_id'] .'",
                                        "' .$edit_req_id.'",
                                        "' .$mysqli->real_escape_string($value).'",
                                        "",
                                        '.$key.')';
                        $mysqli->query($query); 
                    } 
                }

                if(!empty($multiple_landingpages)) { // if not empty multiple landing pages
                    // multiple landing pages insertion
                    foreach($multiple_landingpages as $key => $value) {
                       $query = 'INSERT INTO multiple_templates_data VALUES (
                                        NULL,
                                        "' .$_SESSION['user_id'] .'",
                                        "' .$edit_req_id.'",
                                        "",
                                        "' .$mysqli->real_escape_string($value).'",
                                        '.$key.')';
                        $mysqli->query($query);
                    }
                }
            } else {

                $query_select = "SELECT id FROM templates_custom_data WHERE request_id=".$edit_req_id;
                $exist = $mysqli->query($query_select);
                
                if($exist->num_rows === 0){
                    $query = "INSERT INTO templates_custom_data VALUES (
                                NULL,
                                '" .$_SESSION['user_id'] ."',
                                '" .$edit_req_id."',
                                '" .$post_data['invitation_email_config']."',
                                '" .$post_data['reminder_one_email_config']."',
                                '" .$post_data['reminder_two_email_config']."',
                                '" .$post_data['confirmation_email_config']."',
                                '" .$post_data['thank_you_email_config']."',
                                '" .$post_data['sorry_email_config']."',
                                '" .$post_data['registration_config']."',
                                '" .$post_data['confirmation_config']."',
                                '" .$post_data['landing_page_config']."',
                                '" .$post_data['single_email_config']."')";
                    
                    $mysqli->query($query);
                }else{

                    $email_data = $post_data;
                    
                    unset($email_data['template']);
                    unset($email_data['action']);
                    unset($email_data['req_id']);
                    unset($email_data['user_id']);
                    foreach ($email_data as $key => $value) {
                        $email_data[$key] = $mysqli->real_escape_string($value);
                    }
                    
                    $invitation_e_conf = str_replace("'", "", $post_data['invitation_email_config']);

                    $query = "UPDATE templates_custom_data SET invitation_email_config = '".$invitation_e_conf."',
                    reminder_one_email_config = '".$post_data['reminder_one_email_config']."',
                    reminder_two_email_config = '".$post_data['reminder_two_email_config']."',
                    confirmation_email_config = '".$post_data['confirmation_email_config']."',
                    thank_you_email_config = '".$post_data['thank_you_email_config']."',
                    sorry_email_config = '".$post_data['sorry_email_config']."',
                    registration_config = '".$post_data['registration_config']."',
                    confirmation_config = '".$post_data['confirmation_config']."',
                    landing_page_config = '".$post_data['landing_page_config']."',
                    single_email_config = '".$post_data['single_email_config']."' WHERE request_id = ".$edit_req_id."";

                    $mysqli->query($query);
                }
            }
            header('Location:/request-detail?id=' . $edit_req_id);

        }
        else if($post_data['action'] === 'edittemplate' && $post_data['template'] === 'default'){

            $edit_req_id = $post_data['req_id'];

            $query_select = "SELECT id FROM templates_data WHERE request_id=".$edit_req_id;
            $exist = $mysqli->query($query_select);

            if($exist->num_rows === 0){
                $query = "INSERT INTO templates_data VALUES (
                            NULL,
                            '" .$_SESSION['user_id']."',
                            '" .$edit_req_id."',
                            '" .$post_data['event_header']."',
                            '" .$post_data['copyright_information']."',
                            '" .$post_data['invitation_body_copy']."',
                            '" .$post_data['reminder_body_copy']."',
                            '" .$post_data['confirmation_body_copy']."',
                            '" .$post_data['thank_you_body_copy']."',
                            '" .$post_data['sorry_we_missed_you_body_copy']."',
                            '" .$post_data['regbody_header']."',
                            '" .$post_data['registration_body_copy']."',
                            '" .$post_data['confirmation_page_copy']."',
                            '" .$post_data['checkbox']."',
                            '" .$post_data['disclaimer_copy']."')";

                $mysqli->query($query);
            }else{

                unset($post_data['template']);
                unset($post_data['action']);
                unset($post_data['req_id']);
                unset($post_data['user_id']);

                $email_data = $post_data;
                foreach ($email_data as $key => $value) {
                    $email_data[$key] = $mysqli->real_escape_string($value);
                }

                // var_dump($email_data);

                $query = "UPDATE templates_data SET event_header = \"".$post_data['event_header'] ."\",
                    invitation_body_copy = \"".$post_data['invitation_body_copy'] ."\",
                    copyright_information = \"".$post_data['copyright_information']."\",
                    thank_you_body_copy = \"".$post_data['thank_you_body_copy']."\",
                    sorry_we_missed_you_body_copy = \"".$post_data['sorry_we_missed_you_body_copy']."\",
                    regbody_header = \"".$post_data['regbody_header']."\",
                    registration_body_copy = \"".$post_data['registration_body_copy']."\",
                    confirmation_page_copy = \"".$post_data['confirmation_page_copy']."\",
                    checkbox = \"".$post_data['checkbox']."\",
                    disclaimer_copy = \"".$post_data['disclaimer_copy']."\" WHERE request_id = ".$edit_req_id."";


                $check = $mysqli->query($query);
            }
            header('Location:/request-detail?id=' . $edit_req_id);
        }
    }else {
        header('Location:request?action=failure');
    }

?>
