
<!-- Default clone element -->
<script type="text/template" id="tpl-rp-agenda" class="tpl-rp-comp-clone"><td class="cellblock article" tpl-rp-style="0px 32px 0px 0px" style="vertical-align: top; text-align: left; width: 180px; padding: 0px 32px 0px 0px;" align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="full-width" style="border-collapse: collapse;">
        <tbody><tr>
            <td class="content-span3-left wys-inline-simple" style="font-size: 14px; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height:24px; padding: 0 0 10px 0;">Date<br>Day</td>
        </tr>
        <tr>
            <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height:20px; padding: 0;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</td>
        </tr>
    </tbody></table>
</td></script>
<!-- END Default clone element -->


<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0;">

                                <tbody><tr>
                                    <!-- Container -->

                                    <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0;">

                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">

                                            <tbody><tr>

                                                <td bgcolor="#ffffff" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                                                    <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">

                                                        <tbody><tr>

                                                            <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                                                <!-- Text Column -->



                                                                <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                                                    <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                                                    <tbody><tr>

                                                                        <td class="content-td" colspan="2" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 28px 32px 26px 32px;" align="left" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" class="content-title-icon" style="margin-bottom: 0 !important; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">

                                                                                <tbody><tr height="26">

                                                                                    <td class="title-icon" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:26px;width: 26px; height: 26px; margin: 0; padding: 0px 12px 2px 0px;" align="left" valign="top" width="26" height="26">
                                                                                        <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{d86d0759-c733-4e01-9785-98f8d9f93467}_intel_icons_b_locationorg_time.png" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block; width: 26px; height: 26px;" width="26" height="26">
                                                                                    </td>

                                                                                    <td class="title-text wys-inline" style="font-size: 24px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:26px;margin: 0; padding: 0px 0px 0px 0px;" align="left" valign="top">Agenda</td>

                                                                                    <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: right; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: bold;  line-height:27px;margin: 0; padding: 0;" align="right" valign="top"><a href="#" style="font-size: 12px; color: #00AEEF; text-decoration: none; font-weight: bold;" class="wys-inline-link">Agenda CTA &gt;</a>
                                                                                    </td>

                                                                                </tr>

                                                                            </tbody></table>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>

                                                                        <td class="content-td hide-me" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px 32px 32px 32px;" align="left" valign="top">
                                                                            <table class="full-width tpl-rp-tab" tpl-rp-data="3" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">

                                                                                <tbody><tr>
                                                                                    <!-- Schedule Item -->
                                                                                    <td class="cellblock article tpl-rp-comp" data-parent="tpl-rp-agenda" tpl-rp-style="0px 32px 0px 0px" style="vertical-align: top; text-align: left; width: 180px; padding: 0px 32px 0px 0px;" align="left" valign="top">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="full-width" style="border-collapse: collapse;">
                                                                                            <tbody><tr>
                                                                                                <td class="content-span3-left wys-inline-simple" style="font-size: 14px; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height:24px; padding: 0 0 10px 0;">Date<br>Day</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height:20px; padding: 0;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td>

                                                                                    <!-- Schedule Item -->
                                                                                    <!-- <td class="cellblock article" style="vertical-align: top; text-align: left; width: 180px; padding: 0px 32px 0px 0px;" align="left" valign="top">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="full-width" style="border-collapse: collapse;">
                                                                                            <tbody><tr>
                                                                                                <td class="content-span3-left wys-inline-simple" style="font-size: 14px; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height:24px; padding: 0 0 10px 0;">Date<br>Day</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height:20px; padding: 0;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td> -->

                                                                                    <!-- Schedule Item -->
                                                                                    <!-- <td class="cellblock article" style="vertical-align: top; text-align: left; width: 180px; padding: 0;" align="left" valign="top">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="full-width" style="border-collapse: collapse;">
                                                                                            <tbody><tr>
                                                                                                <td class="content-span3-left wys-inline-simple" style="font-size: 14px; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height:24px; padding: 0 0 10px 0;">Date<br>Day</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height:20px; padding: 0;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td> -->

                                                                                </tr>


                                                                                <!-- Start Hidden CTA -->
                                                                                <!--[if !mso]><!-->
                                                                                <tr class="hide-element tpl-rp-skip" style="display: none; width: 0; max-height: 0; overflow: hidden;">
                                                                                    <td class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;">
                                                                                        <table class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tbody><tr>
                                                                                                <td style="color: #FFFFFF; background-color#FFFFFF;">
                                                                                                    <table class="title-cta-container not-bottom" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tbody><tr class="title-cta-tr" style="font-size: 0px; color: #FFFFFF; background-color#FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 0px;padding: 0px 0px 0px 0px;">
                                                                                                            <td class="title-cta-text" style="display: none; font-size: 0; max-height: 0; line-height: 0; color:#FFFFFF; mso-hide: all; padding: 0px 0px 0px 0px;text-align: right;">
                                                                                                                <table border="0" cellpadding="0" cellspacing="0">

                                                                                                                    <tbody><tr>
                                                                                                                        <td style="font-size: 0px; line-height:0px color: #FFFFFF; background-color#FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-decoration:none;"> <a href="#" class="title-cta-text" style="font-size: 0px; line-height:0px color: #FFFFFF; background-color#FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-decoration:none;">Agenda CTA &gt;</a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>


                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>


                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!--<![endif]-->
                                                                                <!-- End Hidden CTA -->


                                                                            </tbody></table>
                                                                        </td>

                                                                    </tr>

                                                                </tbody></table>
                                                            </td>

                                                            <!-- /wrapper -->

                                                        </tr>

                                                    </tbody></table>

                                                    <!-- /row -->
                                                </td>

                                            </tr>

                                        </tbody></table>

                                    </td>

                                    <!-- /Container -->

                                </tr>

                            </tbody></table>
<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
