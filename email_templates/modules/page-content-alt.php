<style type='text/css'>
  .module-file{display:none;}
</style>
<!--[if IE 8]>
<style type='text/css'>
  .module-file{filter: alpha(opacity=0) !important; cursor:pointer !important; position: absolute !important; height: 90px !important; width: 180px !important;display:block !important; font-size: 80px; top:0; right:0;}
</style>
<![endif]-->

<?

$user_agent = $_SERVER["HTTP_USER_AGENT"];      // Get user-agent of browser

$safari_or_chrome = strpos($user_agent, 'Safari') ? true : false;     
$chrome = strpos($user_agent, 'Chrome') ? true : false;             

if($safari_or_chrome == true && $chrome == false){
    $safari = true; 
}

if($safari == true) {
    ?>
    <style type='text/css'>
  .module-file{opacity: 0 !important; cursor:pointer !important; position: absolute !important; display:block !important; font-size: 80px; top:0; right:0;}
    </style>
    <?
}     

?>

<div class="content-wrapper">
    <div class="main-content cf">
        <!-- Work With Intel -->
        <div class="module work cf">
            <div class="module-header">
                <h3 class="icon-sm blue-comments-sm content-header wys-inline-simple">Work With Intel</h3>
            </div>
            <div class="module-content">
                <div class="content">
                    <h4 class="wys-inline-simple">Planning a project? Intel can help.</h4>

                    <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veritatis laborum itaque et reprehenderit.</p>

                    <a class="mobile-cta wys-inline-link" href="">Intel can help <span class="hide">&gt;</span></a>
                </div>
            </div>
            <div style="position:relative; overflow:hidden;">
                <input type="file"  style="height: 180px !important; width: 290px !important;" id="fileupload" class="module-file"/>
                <label for="fileUpload">
                    <img class="full-width-block image-box-module" src="img/templates/default-images/placeholders/grocer-fpo-lp.png" alt="" data-custom="grocer-custom-lp.png" data-default="grocer-fpo-lp.png">
                </label>
            </div>
        </div><!-- /.module -->
    </div>
</div>
