 <!-- Default clone element -->
<script type="text/template" id="tpl-rp-help-articles" class="tpl-rp-comp-clone">
<td class="articles" data-parent="tpl-rp-help-articles" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px; margin: 0;padding:15px 0px 0px 0px;" align="left" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;width:100%;">
        <tr>
            <td class="content-span2-left wys-inline-simple" style="font-size: 18px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #007DC6; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0px 16px 15px 0px;" align="left" valign="top">Content Title</td>
        </tr>
        <tr>
            <td class="content-span2-right normal_copy_wrapper wys-inline" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0px 16px 22px 0px;" align="left" valign="top">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
            </td>
        </tr>
        <tr>
            <td class="content-span2-left mobile-help">
                <table class="mobile-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <!-- Read More Link -->
                    <tr>
                        <td class="mobile-cta" style="font-size: 12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;padding: 0px 16px 0px 0px;text-align: right;">
                            <a href="#" class="mobile-cta-text wys-inline-link" style="font-size: 12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;text-decoration:none;">Read More <span class="hideblock" style="font-size:12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 12px;text-decoration:none;"> &gt;</span></a>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
</td>
</script>
<!-- END Default clone element -->

 <!-- Begin Help Articles Module -->
<a id="Help"></a>
<table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0;">
    <tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tr>
                    <td bgcolor="#ffffff" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                            <tr>
                                <td class="wrapper" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Text Column -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">



                                        <tr>
                                            <td class="content-td" colspan="2" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 28px 32px 22px 32px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" class="content-title-icon" style="margin-bottom: 0 !important; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">
                                                    <tr>
                                                        <td class="title-icon" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; width: 38px; height: 28px; margin: 0; padding: 0;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{5d283f3e-9ffd-4226-9d24-fbbd60428058}_intel_icons_b_functions_pointing.png" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block; border:0px; width: 26px; height: 26px;">
                                                        </td>
                                                        <td class="title-text wys-inline" style="font-size: 24px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #007DC6; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 24px; margin: 0; padding: 2px 0px 10px 2px;" align="left" valign="top">Help Articles</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="content-td hide-me" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0 32px 25px 32px;" align="left" valign="top">
                                                <table class="full-width tpl-rp-tab main-content" tpl-rp-data="3" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                                                    <tr >
                                                        <!-- Begin Column -->
                                                        <td class="articles tpl-rp-comp" data-parent="tpl-rp-help-articles" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px; margin: 0;padding:15px 0px 0px 0px;" align="left" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;width:100%;">
                                                                <tr>
                                                                    <td class="content-span2-left wys-inline-simple" style="font-size: 18px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #007DC6; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0px 16px 15px 0px;" align="left" valign="top">Content Title</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="content-span2-right normal_copy_wrapper wys-inline" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0px 16px 22px 0px;" align="left" valign="top">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="content-span2-left mobile-help">
                                                                        <table class="mobile-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <!-- Read More Link -->
                                                                            <tr>
                                                                                <td class="mobile-cta" style="font-size: 12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;padding: 0px 16px 0px 0px;text-align: right;">
                                                                                    <a href="#" class="mobile-cta-text wys-inline-link" style="font-size: 12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;text-decoration:none;">Read More <span class="hideblock" style="font-size:12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 12px;text-decoration:none;"> &gt;</span></a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>

                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <!-- End Column -->
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </table>

                        <!-- /row -->
                    </td>
                </tr>
            </table>
        </td>

        <!-- /Container -->

    </tr>
</table>
<!-- End Help Articles Module -->


<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
 <!-- /SPACER -->