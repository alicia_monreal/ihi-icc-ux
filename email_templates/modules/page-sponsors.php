<style type='text/css'>
  .module-file{display:none;}
</style>
<!--[if IE 8]>
<style type='text/css'>
  .module-file{filter: alpha(opacity=0) !important; cursor:pointer !important; position: absolute !important; height: 90px !important; width: 180px !important;display:block !important; font-size: 80px; top:0; right:0;}
</style>
<![endif]-->

<?

$user_agent = $_SERVER["HTTP_USER_AGENT"];      // Get user-agent of browser

$safari_or_chrome = strpos($user_agent, 'Safari') ? true : false;     
$chrome = strpos($user_agent, 'Chrome') ? true : false;             

if($safari_or_chrome == true && $chrome == false){
    $safari = true; 
}

if($safari == true) {
    ?>
    <style type='text/css'>
  .module-file{opacity: 0 !important; cursor:pointer !important; position: absolute !important; display:block !important; font-size: 80px; top:0; right:0;}
    </style>
    <?
}     

?>

<!-- Default clone element -->
<script type="text/template" id="tpl-rp-page-sponsors" class="tpl-rp-comp-clone"><div class="sponsor">
    <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
    <label for="fileUpload">
        <img src="img/templates/default-images/placeholders/company-fpo.png" data-custom="company-custom.png" data-default="company-fpo.png" class="image-box-module" alt="">
    </label>
    <h4 class="wys-inline-simple">Company Name</h4>
</div></script>
<!-- END Default clone element -->


<!-- Sponsors -->
<div class="content-wrapper">
    <div class="main-content cf">
        <div class="module sponsors cf">
            <div class="module-header">
                <h3 class="icon-sm blue-usergroup-sm content-header">Sponsors</h3>
            </div>

            <div class="module-content">
                <div class="content cf tpl-rp-tab" tpl-rp-data="div">
                    <div class="sponsor tpl-rp-comp" data-parent="tpl-rp-page-sponsors">
                        <div style="position:relative; max-width:158px;">
                            <input type="file" id="fileupload" style="height: 100px !important; width: 150px !important;" class="module-file"/>
                            <label for="fileUpload">
                                <img src="img/templates/default-images/placeholders/company-fpo.png" data-custom="company-custom.png" data-default="company-fpo.png" class="image-box-module" alt="">
                            </label>
                        </div>
                        <h4 class="wys-inline-simple">Company Name</h4>
                    </div>

                    <!--ADD THESE DYNAMICALLY-->
                    <!--
                    <div class="sponsor middle">
                        <img src="img/templates/default-images/placeholders/company-fpo.png" alt="">
                        <h4 class="wys-inline-simple">Company Name</h4>
                    </div>

                    <div class="sponsor">
                        <img src="img/templates/default-images/placeholders/company-fpo.png" alt="">
                        <h4 class="wys-inline-simple">Company Name</h4>
                    </div>

                    <div class="sponsor">
                        <img src="img/templates/default-images/placeholders/company-fpo.png" alt="">
                        <h4 class="wys-inline-simple">Company Name</h4>
                    </div>

                    <div class="sponsor middle">
                        <img src="img/templates/default-images/placeholders/company-fpo.png" alt="">
                        <h4 class="wys-inline-simple">Company Name</h4>
                    </div>

                    <div class="sponsor">
                        <img src="img/templates/default-images/placeholders/company-fpo.png" alt="">
                        <h4 class="wys-inline-simple">Company Name</h4>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>
