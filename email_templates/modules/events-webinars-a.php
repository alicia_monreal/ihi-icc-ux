 <!-- Default clone element -->
<script type="text/template" id="tpl-rp-events-webinars" class="tpl-rp-comp-clone">
<td class="events-webinars" tpl-rp-style="0px 32px 10px 0px" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px; margin: 0; padding: 0px 32px 10px 0px;" align="left" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" class="columns" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0; padding: 0; ">
        <tr>
            <td class="content-span3-left wys-inline-simple" style="font-size: 14px; word-break: break-word; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;   line-height:24px;padding: 0px 0px 3px 0px;" align="left" valign="top">Event Name Here Event Name Event Name
            </td>
        </tr>
        <tr>
            <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left;   line-height:20px;margin: 0 0 0px; padding: 0px 0px 0px 0px;" align="left">Jan 30 - Feb 2</td>
        </tr>
        <tr>
            <td class="last normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left;   line-height:20px;margin: 0 0 0px; padding: 0px 0px 24px 0px;" align="left">
                Location, CA 95926</td>
        </tr>

        <tr>
            <td class="mobile-help" style="font-size: 12px; word-break: break-word; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" class="medium-button" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; overflow: hidden;">
                    <tr>
                        <td class="mobile-reg-cta" style="font-size: 20px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal;width: auto !important; background-color: #fdb813; margin: 0; padding: 15px 5px 15px 10px;" align="left" bgcolor="#fdb813" valign="top">
                            <table class="inner-button" border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #fdb813;">
                                <tr>
                                    <td class="button-text" style="background-color: #fdb813; color: #ffffff; font-weight: normal; font-family: arial, helvetica, sans-serif; font-size: 18px; line-height:18px; padding-right: 10px; text-align: left;padding-top:0px;"><a href="#" class="wys-inline-link" style="font-size: 18px; line-height:18px;color: #ffffff; text-decoration: none;">Register Today</a></td>

                                    <td width="12" align="left" style="font-size: 21px; line-height:21px;padding-right:15px;"><a href="#" style="font-size: 21px; line-height:21px;"><img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{049eebcb-f1ec-422e-852a-16aeb25d156c}_white-arrow.png" style="display: block;" width="12" height="21" border="0"></a>
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</td>
</script>
<!-- END Default clone element -->

<!-- Begin Events & Webinars Module -->
<a id="Events"></a>
<table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0;">
    <tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tr>
                    <td bgcolor="#ffffff" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; padding: 0;">
                            <tr>
                                <td class="wrapper" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Text Column -->

                                    <table border="0" cellpadding="0" class="full-width" cellspacing="0" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                        <tr>
                                            <td class="content-td" colspan="2" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 28px 32px 22px 32px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" class="content-title-icon" style="margin-bottom: 0 !important; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">
                                                    <tr>
                                                        <td class="title-icon" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:12px;width: 26px; height: 26px; margin: 0; padding: 0px 12px 2px 0px;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{4be5993a-26d8-4c7a-9e8f-fcfd1dd68e68}_intel_icons_b_locationorg_grid.png" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block; width: 26px; height: 26px;">
                                                        </td>
                                                        <td class="title-text wys-inline" style="font-size: 24px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:24px;margin: 0; padding: 2px 0px 10px 0px;" align="left" valign="top">Events &amp; Webinars</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="content-td hide-me mobile-event-container" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0 32px 32px 32px;" align="left" valign="top">
                                                <table class="full-width tpl-rp-tab main-content" tpl-rp-data="3" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; padding: 0;">
                                                    <tr>
                                                        <td class="events-webinars tpl-rp-comp" tpl-rp-style="0px 32px 10px 0px" data-parent="tpl-rp-events-webinars" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px; margin: 0; padding: 0px 32px 10px 0px;" align="left" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" class="columns" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0; padding: 0; ">
                                                                <tr>
                                                                    <td class="content-span3-left wys-inline-simple" style="font-size: 14px; word-break: break-word; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;   line-height:24px;padding: 0px 0px 3px 0px;" align="left" valign="top">Event Name Here Event Name Event Name
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left;   line-height:20px;margin: 0 0 0px; padding: 0px 0px 0px 0px;" align="left">Jan 30 - Feb 2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="last normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left;   line-height:20px;margin: 0 0 0px; padding: 0px 0px 24px 0px;" align="left">
                                                                        Location, CA 95926</td>
                                                                </tr>

                                                                <tr>
                                                                    <td class="mobile-help" style="font-size: 12px; word-break: break-word; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="medium-button" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; overflow: hidden;">
                                                                            <tr>
                                                                                <td class="mobile-reg-cta" style="font-size: 20px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal;width: auto !important; background-color: #fdb813; margin: 0; padding: 15px 5px 15px 10px;" align="left" bgcolor="#fdb813" valign="top">
                                                                                    <table class="inner-button" border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #fdb813;">
                                                                                        <tr>
                                                                                            <td class="button-text" style="background-color: #fdb813; color: #ffffff; font-weight: normal; font-family: arial, helvetica, sans-serif; font-size: 18px; line-height:18px; padding-right: 10px; text-align: left;padding-top:0px;"><a href="#" class="wys-inline-link" style="font-size: 18px; line-height:18px;color: #ffffff; text-decoration: none;">Register Today</a></td>

                                                                                            <td width="12" align="left" style="font-size: 21px; line-height:21px;padding-right:15px;"><a href="#" style="font-size: 21px; line-height:21px;"><img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{049eebcb-f1ec-422e-852a-16aeb25d156c}_white-arrow.png" style="display: block;" width="12" height="21" border="0"></a>
                                                                                            </td> 
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <!-- End Column -->
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </table>

                        <!-- /row -->
                    </td>
                </tr>
            </table>
        </td>

        <!-- /Container -->

    </tr>
</table>
<!-- End Events & Webinars Module -->

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->