 <!-- Default clone element -->
<script type="text/template" id="tpl-rp-help-articles-b" class="tpl-rp-comp-clone"> 
<td class="show-me tpl-rp-comp" data-parent="tpl-rp-help-articles-b" tpl-rp-style="0px 0px 0px 0px" style="border-bottom:1px #cccccc solid; padding:25px 0px 0px 0px;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
        <tbody>
        <tr>
            <td class="mobile-subhead wys-inline-simple" style="padding:0px 0px 10px 0px;font-family:Arial, Helvetica, sans-serif;font-size:14px; line-height:17px;color:#007DC6">Featured Content Article Subhead

            </td>
        </tr>
        <tr>
            <td style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:21px;padding:0px 0px 25px 0px" class="module-content wys-inline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</td>
        </tr>
        <tr>
            <td style="color:#0071C5; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 30px 0px;font-weight:bold" class="mobile-cta-container-td">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mobile-cta-container">
                    <tbody><tr>
                        <td style="font-size:14px; line-height:18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="mobile-cta">
                            <a style="font-size:14px; line-height:18px;color:#0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;text-decoration:none;" class="mobile-cta-text wys-inline-link" href="#">Read More<span style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="hide"> &gt;</span></a>


                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>

    </tbody></table>
</td>
</script>
<!-- END Default clone element -->
<table width="429" cellspacing="0" cellpadding="0" border="0" align="left" style="background-color: #FFFFFF; border-collapse: collapse;" class="content-column">
    <tbody>
           <tr> <td valign="top" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:18px; padding-top: 0px; padding-left: 25px; padding-right: 25px; text-align:left;width:100%;padding-bottom:10px" class="content-item">
                <table class="full-width tpl-rp-tab" tpl-rp-data="1" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                    <tbody>
                        <!--Content Block-->
                       <tr>
                            <td class="show-me tpl-rp-comp" data-parent="tpl-rp-help-articles-b" tpl-rp-style="0px 0px 0px 0px" style="border-bottom:1px #cccccc solid; padding:25px 0px 0px 0px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                                    <tbody><tr>
                                        <td style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:28px;padding:0px 0px 25px 0px;" class="module-header"><a style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:28px;text-decoration:none;" class="wys-inline-link" name="helparticle">Help Article</a></td>
                                    </tr>
                                    <tr>
                                        <td class="mobile-subhead wys-inline-simple" style="padding:0px 0px 10px 0px;font-family:Arial, Helvetica, sans-serif;font-size:14px; line-height:17px;color:#007DC6">Featured Content Article Subhead

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:21px;padding:0px 0px 25px 0px" class="module-content wys-inline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#0071C5; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 30px 0px;font-weight:bold" class="mobile-cta-container-td">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mobile-cta-container">
                                                <tbody><tr>
                                                    <td style="font-size:14px; line-height:18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="mobile-cta">
                                                        <a style="font-size:14px; line-height:18px;color:#0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;text-decoration:none;" class="mobile-cta-text wys-inline-link" href="#">Read More<span style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="hide"> &gt;</span></a>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>

                                </tbody></table>
                            </td>
                        </tr>
                        <!--END CONTENT BLOCK-->
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>