<style type='text/css'>
  .module-file{display:none;}
</style>
<!--[if IE 8]>
<style type='text/css'>
  .module-file{filter: alpha(opacity=0) !important; cursor:pointer !important; position: absolute !important; height: 90px !important; width: 180px !important;display:block !important; font-size: 80px; top:0; right:0;}
</style>
<![endif]-->

<?

$user_agent = $_SERVER["HTTP_USER_AGENT"];      // Get user-agent of browser

$safari_or_chrome = strpos($user_agent, 'Safari') ? true : false;     
$chrome = strpos($user_agent, 'Chrome') ? true : false;             

if($safari_or_chrome == true && $chrome == false){
    $safari = true; 
}

if($safari == true) {
    ?>
    <style type='text/css'>
  .module-file{opacity: 0 !important; cursor:pointer !important; position: absolute !important; display:block !important; font-size: 80px; top:0; right:0;}
    </style>
    <?
}     

?>
<table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->

        <td align="center" valign="top" class="center devicewidth" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">

            <table border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tbody><tr>
                    <td bgcolor="#ffffff" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="no-padding" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                            <tbody><tr>
                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Text Column -->
                                    <!-- NOTE: Change table width based on first column width, subtracted from 670 -->
                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 384px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                        <tbody><tr>
                                            <td class="content-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 32px 32px 0px 32px;" align="left" valign="top">
                                                <!-- NOTE: Specific extra padding based on Ozone template. Default is 25px all around -->
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tbody><tr>
                                                        <td style="font-size: 18px; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; line-height: 18px; padding: 0px 0px 15px 0px;" align="left" class="wys-inline-simple">Planning a project? Intel can help.</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height: 20px; padding:0px 0px 10px 0px;" align="left">Itaque earum rerum hic tenetur a sapiente delectus, ut aut ut aut reiciendis.Itaque earum sapiente delectus.</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="last">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <!-- Intel Can Help CTA -->
                                                                <tbody><tr>
                                                                    <td class="title-cta" style="font-size: 14px; color: #1588CB; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;padding: 16px 0px 16px 0px;text-align: left;">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tbody><tr>
                                                                                <td style="font-size: 14px; color: #1588CB; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 20px;text-decoration:none;"><a href="#" class="title-cta-text wys-inline-link" style="font-size: 14px; color: #1588CB; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 20px;text-decoration:none;">Intel can help &gt;</a></td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                <!-- End Intel Can Help CTA -->
                                                            </tbody></table>
                                                        </td>
                                                    </tr>

                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                                <!-- /wrapper -->

                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Image -->
                                    <!-- NOTE: Change table width based on image width -->
                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 286px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">
                                        <tbody><tr>
                                            <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                                                <div style="position:relative; max-width:286px;">
                                                    <input type="file"  style="height: 178px !important; width: 286px !important;" id="fileupload" class="module-file"/>
                                                    <label for="fileUpload" style="margin-bottom:0px;">
                                                        <img src="img/templates/default-images/placeholders/grocer-fpo.png" data-custom="grocer-custom.png" data-default="grocer-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="286" height="178" alt="Intel Can Help">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->
                    </td>
                </tr>
            </tbody></table>

        </td>

        <!-- /Container -->

    </tr>
</tbody></table>


<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
