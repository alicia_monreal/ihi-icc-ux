<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size:12px;border-spacing:0;border-collapse:collapse;vertical-align:top;text align:left;height:100%;width:100%;color:#5f5f5e;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:27px;margin:0;padding:0">
                                <tbody><tr>
                                    <!-- Container -->

                                    <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0;">
                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                                            <tbody><tr>
                                                <td bgcolor="#ffffff" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                                                    <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                                                        <tbody><tr>
                                                            <td class="cellblock" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                                                <!-- Image -->

                                                                <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 126px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;">

                                                                    <!-- NOTE: Change width based on image width -->

                                                                    <tbody><tr>
                                                                        <td class="content-td" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 25px 0px 25px 32px;" align="left" valign="top">
                                                                            <!-- Intel Badge Image -->
                                                                            <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/badge-fpo.jpg" class="" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;">
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>

                                                            <!-- /wrapper -->

                                                            <td class="cellblock" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                                                <!-- Header Text -->

                                                                <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 544px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;">

                                                                    <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                                                    <tbody><tr>
                                                                        <td class="show-me" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top;  color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 25px 25px 0px 30px;" align="left" valign="top">
                                                                            <!-- NOTE: Specific extra padding based on Ozone template. Default is 25px all around -->
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tbody><tr>
                                                                                    <td class="hideblock" align="right" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: right;  line-height:27px; padding: 0px 0px 10px 0px;"><a href="#" style="font-size: 12px;  text-align:right; color: #00AEEF; text-decoration: none; font-weight: bold;">Content CTA &gt;</a></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="headline_wrapper" style="font-size: 14px; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: 600; text-align: left; word-break: normal;  line-height: 14px;  padding: 0px 0px 10px;" align="left">Content Title</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="normal_copy_wrapper no-bottom-padding" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left;  line-height:20px;padding: 0px 0px 10px 0px;" align="left">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip.</td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="content-td" style="padding: 0px 0px 0px 0px;text-align:right">
                                                                            <!-- Mobile CTA -->
                                                                            <table class="mobile-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tbody><tr>
                                                                                    <td class="mobile-cta" style="font-size: 0px;font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 25px;">
                                                                                        <a href="#" class="mobile-cta-text" style="font-size: 0px; color: #FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 25px;text-decoration:none;text-align: right;">Content CTA<span class="hideblock" style="font-size: 0px; color: #FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 12px;text-decoration:none;"> &gt;</span></a></td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                            <!-- End Mobile CTA -->
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>

                                                            <!-- /wrapper -->

                                                        </tr>

                                                    </tbody></table>

                                                    <!-- /row -->
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>

                                    <!-- /Container -->

                                </tr>
                            </tbody></table>
<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->