<div class="content-wrapper">
    <div class="main-content cf">
        <!-- Location Module -->
        <div class="module location cf">
            <div class="module-header">
                <h3 class="icon-sm blue-location-sm content-header wys-inline-simple">Location</h3>
            </div>

            <div class="module-content">
                <div class="content cf">
                    <div class="inner-copy wys-inline">
                        <p><span class="blue">Location:</span> Moscone Center</p>
                        <p class="address"><span class="blue">Address 1:</span> 747 Howard St, San Francisco, CA 94103</p>
                        <p><span class="blue">Address 2:</span> Location</p>
                    </div>

                    <div class="image-container">
                        <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                        <label for="fileUpload">
                            <!-- <img src="img/templates/default-images/placeholders/map-fpo.png" data-custom="map-custom.png" data-default="map-fpo.png" class="location-two-column image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="292"> -->
                            <img src="img/templates/default-images/placeholders/map-fpo.png" data-custom="map-custom.png" data-default="map-fpo.png" class="image-box-module" alt="">
                        </label>
                        <!-- <a href="https://www.google.com/maps/place/747+Howard+St/@37.7839223,-122.4012128,17z/data=!3m1!4b1!4m2!3m1!1s0x8085807d89942db7:0x3cfc49ec6e33214d" target="_blank"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{98777a51-7321-4b9f-b15d-e9227a7fb234}_intel-map-337x232.jpg" alt=""></a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
