<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size:12px;border-spacing:0;border-collapse:collapse;vertical-align:top;text align:left;height:100%;width:100%;color:#5f5f5e;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:27px;margin:0;padding:0">
                                <tbody><tr>
                                    <!-- Container -->

                                    <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0;">

                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                                            <tbody><tr>
                                                <td bgcolor="#ffffff" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                                                    <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                                                        <tbody><tr>
                                                            <td class="full-width" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                                                <!-- Text Column -->

                                                                <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                                                    <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                                                    <tbody><tr>
                                                                        <td class="content-td" colspan="2" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 28px 32px 22px 32px;" align="left" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" class="content-title-icon" style="margin-bottom: 0 !important; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">
                                                                                <tbody><tr>
                                                                                    <td class="title-icon" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;width: 26px; height: 26px; margin: 0; padding: 0px 12px 2px 0px;" align="left" valign="top">
                                                                                        <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{4627df0b-40d5-4b86-97f6-adb783f84c63}_intel_icons_b_locationorg_location.png" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block; width: 26px; height: 26px;">
                                                                                    </td>
                                                                                    <td class="title-text wys-inline-simple" style="font-size: 24px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:24px;margin: 0; padding: 2px 0px 10px 0px;" align="left" valign="top">Location Two Column w/ Image</td>
                                                                                    <td class="hideblock" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: right; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: bold;  line-height:27px;margin: 0; padding: 0;" align="right" valign="top"><a href="#" style="font-size: 12px; color: #00AEEF; text-decoration: none; font-weight: bold;" class="wys-inline-link">Location CTA &gt;</a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="show-me" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0 32px 30px 32px;" align="left" valign="top">
                                                                            <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                                                                                <tbody><tr>
                                                                                    <td class="cellblock" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px 25px 0px 0px;" align="left" valign="top">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;">
                                                                                            <tbody><tr>
                                                                                                <td class="subhead_copy_wrapper wys-inline" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 0px 10px 0px;" align="left" valign="top"><strong style="color:#0071C5;">Location:</strong> Moscone Center<br /><br /><strong style="color: #0071C5;">Address 1:</strong> 747 Howard St,
                                                                                                    <br>San Francisco, CA 93104<br /><br /><strong style="color:#0071C5">Address 2:</strong> Location</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="hideblock wys-inline" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 0px 10px 0px;" align="left" valign="top">
                                                                                                    Session description consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    </td>
                                                                                    <td class="cellblock" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:27px;margin: 0; padding: 0px 0px 0px 0px;" align="left" valign="top" width="292">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="left" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;" width="292">
                                                                                            <tbody><tr>
                                                                                                <td align="center" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 0px 10px 0px;" valign="top" width="292">
                                                                                                    <!-- Map Image -->
                                                                                                    <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                                                                                                    <label for="fileUpload">
                                                                                                        <img src="img/templates/default-images/placeholders/map-fpo.png" data-custom="map-custom.png" data-default="map-fpo.png" class="location-two-column image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="292">
                                                                                                    </label>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="hideblock wys-inline-simple" align="center" style="font-size: 10px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color:#004281; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  font-style: italic; line-height:15px;margin: 0; padding: 0px 0px 0px 16px;" valign="top">
                                                                                                    Caption: Lorem ipsum dolor sit amet, consectuer adipiscing elit, sed diam nonummy nibh euismod tinnt ut laoreet dolore.</td>
                                                                                            </tr>

                                                                                            <!-- Start Hidden CTA -->
                                                                                            <!--[if !mso]><!-->
                                                                                            <tr class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;">
                                                                                                <td class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;">
                                                                                                    <table class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tbody><tr>
                                                                                                            <td class="" style="color: #FFFFFF; background-color#FFFFFF;padding: 0px 0px 0px 0px">
                                                                                                                <table class="title-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                                    <tbody><tr class="title-cta-tr" style="font-size: 0px; color: #FFFFFF; background-color#FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 0px;padding: 0px 0px 0px 0px;">
                                                                                                                        <td class="title-cta-text" style="display: none; font-size: 0; max-height: 0; line-height: 0; color:#FFFFFF; mso-hide: all; padding: 0px 0px 0px 0px;text-align: right;">
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                                                                <tbody><tr>
                                                                                                                                    <td style="font-size: 0px; line-height:0px color: #FFFFFF; background-color#FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-decoration:none;"><a href="#" class="title-cta-text" style="font-size: 0px; line-height:0px color: #FFFFFF; background-color#FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-decoration:none;">Location CTA &gt;</a>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!--<![endif]-->
                                                                                            <!-- Start Hidden CTA -->

                                                                                        </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>

                                                            <!-- /wrapper -->

                                                        </tr>
                                                    </tbody></table>

                                                    <!-- /row -->
                                                </td>
                                            </tr>
                                        </tbody></table>

                                    </td>

                                    <!-- /Container -->

                                </tr>
                            </tbody></table>
<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
