<!-- Default clone element -->
<script type="text/template" id="tpl-rp-page-resource" class="tpl-rp-comp-clone"><div class="resource" data-parent="tpl-rp-page-resource">
    <div class="resource-header">
        <h4 class="blue-document-lg wys-inline-simple">Name of Offer</h4>
        <h5 class="wys-inline-simple">Title of Author</h5>
    </div>

    <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>
    <a class="mobile-cta wys-inline-link" href="">Download <span class="hide">&gt;</span></a>
</div></script>
<!-- END Default clone element -->


<!-- Resources -->
<div class="content-wrapper">
    <div class="main-content cf">
        <div class="module resources cf">
            <div class="module-header">
                <h3 class="icon-sm blue-copy-sm content-header wys-inline-simple">Resources</h3>
            </div>

            <div class="module-content">
                <div class="content cf tpl-rp-tab" tpl-rp-data="div">
                    <div class="resource tpl-rp-comp" data-parent="tpl-rp-page-resource">
                        <div class="resource-header">
                            <h4 class="blue-document-lg wys-inline-simple">Name of Offer</h4>
                            <h5 class="wys-inline-simple">Title of Author</h5>
                        </div>

                        <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>
                        <a class="mobile-cta wys-inline-link" href="">Download <span class="hide">&gt;</span></a>
                    </div>

                    <!--ADD THESE DYNAMICALLY-->
                    <!-- <div class="resource">
                        <div class="resource-header">
                            <h4 class="blue-document-lg wys-inline-simple">Name of Offer</h4>
                            <h5 class="wys-inline-simple">Title of Author</h5>
                        </div>

                        <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>

                        <a class="mobile-cta wys-inline-link" href="">Download <span class="hide">&gt;</span></a>
                    </div>

                    <div class="resource">
                        <div class="resource-header">
                            <h4 class="blue-document-lg wys-inline-simple">Name of Offer</h4>
                            <h5 class="wys-inline-simple">Title of Author</h5>
                        </div>

                        <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>

                        <a class="mobile-cta wys-inline-link" href="">Download <span class="hide">&gt;</span></a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
