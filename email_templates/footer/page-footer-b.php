<!-- Full width footer -->
<div class="content-wrapper">
    <div class="main-content cf">
		<footer class="cf" style="height:auto;">
			<div id="footer-kink" class="kink cf">
				<div class="inner-content">
					<h5 class="wys-inline-simple">Conversations</h5>
					
					<!-- <nav id="social-media">
						<ul class="horizontal-menu cf social-list social-table">
							<li><a class="wys-inline-link" target="_blank" title="SocialIcon" onclick="javascript:return false;"><img src="../../img/icons/social16x16.png" class="social-icon" style="font-size: 12px; outline: none; text-decoration: none; width: 16px; height: 16px; border: none;display:block;" height="80" width="80"></a></li>

						</ul>
						<ul class="horizontal-menu cf" style="float:right;">
							<li><a href="" title="AddThis" onclick="addSocialIcon('social-list');return false;" class="btn-green add" style="height:27px;padding-left:13px;margin-top:6px;background-position:center right;font-size:12px;padding-right:42px;font-family: 'IntelClearBold', sans-serif;color:#333 !important;width:112px;margin-right:20px;">Add more</a></li>
						</ul>	
					</nav>		 -->		
					<!-- Begin Social Media Icons -->
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tbody><tr class="horizontal-menu cf social-table">

                            <!-- Social icon -->
                            <td class="social" style="font-size: 16px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: 600; text-align: left; line-height:15px;padding: 0 5px 0 0; " valign="top" align="left"><a class="wys-inline-link" target="_blank" onclick="javascript:return false;"><img src="../../img/icons/social16x16.png" class="social-icon" style="font-size: 12px; outline: none; text-decoration: none; width: 16px; height: 16px; border: none;display:block;" height="80" width="80"></a></td>

                        </tr>
                    </tbody></table>
                    <table class="add-social-icon" style="position:absolute; z-index:9000; right:35px;"><tbody>
                    	<tr>
                        	<!-- Add more -->
                            <td colspan="10" class="add-social-icon" style="font-size: 16px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: 600; text-align: left; line-height:15px;padding: 0px 15px 20px 0px; " valign="top" align="left"><a href="" title="AddThis" onclick="addSocialIcon('social-table');return false;" class="btn-green add" style="height:27px;padding-left:13px;margin-top:6px;background-position:center right;font-size:12px;padding-right:42px;font-family: 'IntelClearBold', sans-serif;color:#333 !important;width:120px;margin-right:20px;">Add more</a></td>
                        </tr>
                    </tbody></table>
                    <!-- End Social Media Icons -->
				</div>
			</div>
			
			<div id="footer-legal">
				<div class="wrapper">
					<div class="inner-content wys-inline" style="padding:40px 2.02128% 60px 2.02128%;width:100%;">
						<p class="copyright_information_text ">Copyright © 2014 Intel Corporation. All rights reserved. Intel, the Intel logo, are trademarks of Intel Corporation in the U.S. and/or other countries.<br /><br />Intel is committed to protecting your privacy. For more information about Intel's privacy practices, please visit <a href="http://www.intel.com/privacy">www.intel.com/privacy</a> or write to Intel Corporation, ATTN Privacy, Mailstop RNB4-145, 2200 Mission College Blvd., Santa Clara, CA 95054 USA</p>
						
						<nav id="legal">
							<ul class="horizontal-menu cf">
								<li style="margin-right:0;"><a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" title="Privacy" target="_blank">Privacy</a></li>
								<li><a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-cookie-notice.html" id="_bapw-link" title="Cookies" target="_blank">Cookies</a></li>
								<li><a href="https://www-ssl.intel.com/content/www/us/en/legal/trademarks.html" title="* Trademarks" target="_blank">* Trademarks</a></li>
							</ul>						
						</nav>
					</div>
				</div>			
			</div>
		</footer><!--/Footer-->
	</div>
</div>