
<?php global $tactic_type, $email_count, $email_listing; 

    if($tactic_type == 'enurture' || $_REQUEST['type'] == 'enurture') {  // if tactic_type is enurture 

        if(empty($tactic_type))
            $tactic_type = $_REQUEST['type'];

        if(!empty($_REQUEST['count'])) {  // if count is not empty 
            $email_record_count = $_REQUEST['count']; // get value from request count
        } else {
            $email_record_count = $email_count; // get value from global field
        }

        if(!empty($email_listing['send_time'][$email_record_count])) {  // if send time is not empty
            $send_time_value = explode(" ",$email_listing['send_time'][$email_record_count]); // get both send time and am/pm value
        } else {
            $send_time_value = array("","AM"); // set default values
        }

        if(empty($email_listing['name'][$email_record_count])) { // if empty name
            $email_listing['name'][$email_record_count] = "";
        }
        if(empty($email_listing['email_subject'][$email_record_count])) { // if empty subject
            $email_listing['email_subject'][$email_record_count] = "";
        }
        if(empty($email_listing['send_date'][$email_record_count])) { // if empty send date
            $email_listing['send_date'][$email_record_count] = "";
        }

        ?>
            <div class="thumbnail-block thumbnail-selector fourup">
                <h2 class="template-head">Email Details</h2>
                    <input type="hidden" name="<?php echo $tactic_type; ?>_number" id="<?php echo $tactic_type; ?>_number" value="<?php echo $email_record_count; ?>" />
                    <input type="text" id="email_name_field" name="email_name_field" class="input-medium-detail required" placeholder="Name of Email" value="<?php echo $email_listing['name'][$email_record_count] ?>" >
                    <input type="text" id="email_subject_field" name="email_subject_field" class="input-medium-detail required" placeholder="Email Subject" value="<?php echo $email_listing['email_subject'][$email_record_count] ?>" style="width:396px !important">
                    
                    <div class="input-append date" id="datetimepicker">
                        <input type="text" value="<?php echo $email_listing['send_date'][$email_record_count] ?>" class="input-medium-detail valid" name="email_send_date" id="email_send_date" placeholder="Send Date">
                        <span style="margin-right:2px;" class="add-on"><i class="icon-th"></i></span>
                    </div>
                    
                    <select id="email_send_time" name="email_send_time" class="input-medium-detail required" style="margin-right:2px;">
                        <option value="">Select One</option>
                        <option value="01:00" <?php if ($send_time_value[0] == '01:00') { echo 'selected'; } ?>>01:00</option>
                        <option value="01:30" <?php if ($send_time_value[0] == '01:30') { echo 'selected'; } ?>>01:30</option>
                        <option value="02:00" <?php if ($send_time_value[0] == '02:00') { echo 'selected'; } ?>>02:00</option>
                        <option value="02:30" <?php if ($send_time_value[0] == '02:30') { echo 'selected'; } ?>>02:30</option>
                        <option value="03:00" <?php if ($send_time_value[0] == '03:00') { echo 'selected'; } ?>>03:00</option>
                        <option value="03:30" <?php if ($send_time_value[0] == '03:30') { echo 'selected'; } ?>>03:30</option>
                        <option value="04:00" <?php if ($send_time_value[0] == '04:00') { echo 'selected'; } ?>>04:00</option>
                        <option value="04:30" <?php if ($send_time_value[0] == '04:30') { echo 'selected'; } ?>>04:30</option>
                        <option value="05:00" <?php if ($send_time_value[0] == '05:00') { echo 'selected'; } ?>>05:00</option>
                        <option value="05:30" <?php if ($send_time_value[0] == '05:30') { echo 'selected'; } ?>>05:30</option>
                        <option value="06:00" <?php if ($send_time_value[0] == '06:00') { echo 'selected'; } ?>>06:00</option>
                        <option value="06:30" <?php if ($send_time_value[0] == '06:30') { echo 'selected'; } ?>>06:30</option>
                        <option value="07:00" <?php if ($send_time_value[0] == '07:00') { echo 'selected'; } ?>>07:00</option>
                        <option value="07:30" <?php if ($send_time_value[0] == '07:30') { echo 'selected'; } ?>>07:30</option>
                        <option value="08:00" <?php if ($send_time_value[0] == '08:00') { echo 'selected'; } ?>>08:00</option>
                        <option value="08:30" <?php if ($send_time_value[0] == '08:30') { echo 'selected'; } ?>>08:30</option>
                        <option value="09:00" <?php if ($send_time_value[0] == '09:00') { echo 'selected'; } ?>>09:00</option>
                        <option value="09:30" <?php if ($send_time_value[0] == '09:30') { echo 'selected'; } ?>>09:30</option>
                        <option value="10:00" <?php if ($send_time_value[0] == '10:00') { echo 'selected'; } ?>>10:00</option>
                        <option value="10:30" <?php if ($send_time_value[0] == '10:30') { echo 'selected'; } ?>>10:30</option>
                        <option value="11:00" <?php if ($send_time_value[0] == '11:00') { echo 'selected'; } ?>>11:00</option>
                        <option value="11:30" <?php if ($send_time_value[0] == '11:30') { echo 'selected'; } ?>>11:30</option>
                        <option value="12:00" <?php if ($send_time_value[0] == '12:00') { echo 'selected'; } ?>>12:00</option>
                        <option value="12:30" <?php if ($send_time_value[0] == '12:30') { echo 'selected'; } ?>>12:30</option>
                    </select>
                    <select id="email_send_ampm" name="email_send_ampm" style="width: 64px;"><option value="AM" <?php if($send_time_value[1] == "AM") echo "selected" ?>>AM</option><option value="PM" <?php if($send_time_value[1] == "PM") echo "selected" ?> >PM</option></select>
            </div>
        <?php
    }
?>

<!--Banner Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a banner type</h2>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-header">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner A" data-type="header" data-path="email-banner-a"><img src="img/templates/thumbnails/email-banner-a.png" class="popover-hover" data-toggle="popover" data-content="Banner A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner B" data-type="header" data-path="email-banner-b"><img src="img/templates/thumbnails/email-banner-b.png" class="popover-hover" data-toggle="popover" data-content="Banner B"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner C" data-type="header" data-path="email-banner-c"><img src="img/templates/thumbnails/email-banner-c.png" class="popover-hover" data-toggle="popover" data-content="Banner C"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner D" data-type="header" data-path="email-banner-d"><img src="img/templates/thumbnails/email-banner-d.png" class="popover-hover" data-toggle="popover" data-content="Banner D"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner E" data-type="header" data-path="email-banner-e"><img src="img/templates/thumbnails/email-banner-e.png" class="popover-hover" data-toggle="popover" data-content="Banner E"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner F" data-type="header" data-path="email-banner-f"><img src="img/templates/thumbnails/email-banner-f.png" class="popover-hover" data-toggle="popover" data-content="Banner F"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner G" data-type="header" data-path="email-banner-g"><img src="img/templates/thumbnails/email-banner-g.png" class="popover-hover" data-toggle="popover" data-content="Banner G"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner H" data-type="header" data-path="email-banner-h"><img src="img/templates/thumbnails/email-banner-h.png" class="popover-hover" data-toggle="popover" data-content="Banner H"></a></li>
    </ul>

    <div id="" class="asset-preview asset-preview-email hide asset-header">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Body Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a body type</h2>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-content">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Body A" data-type="content" data-path="email-body-a"><img src="img/templates/thumbnails/email-body-a.png" class="popover-hover" data-toggle="popover" data-content="Email Body A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Body B" data-type="content" data-path="email-body-b"><img src="img/templates/thumbnails/email-body-b.png" class="popover-hover" data-toggle="popover" data-content="Email Body B"></a></li>
    </ul>
    <div id="" class="asset-preview asset-preview-email hide asset-content">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Additional Modules-->
<div class="thumbnail-block thumbnail-selector fourup">
    <div id="" class="asset-preview asset-preview-email hide asset-modules">
    </div>
    <h2 class="template-head">Add additional modules</h2>
    <p class="section-note">Some of the additional modules have variable column layouts which are governed by the rules in the Eloqua Template guidelines.  While entering text in the Asset Configuration, the columns will remain fixed.  To see the final output, please view the module from "Preview Mode".</p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-modules">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Speakers" data-type="modules" data-path="speakers"><img src="img/templates/thumbnails/email-module-speakers.png" class="popover-hover" data-toggle="popover" data-content="Speakers"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Location" data-type="modules" data-path="location"><img src="img/templates/thumbnails/email-module-location.png" class="popover-hover" data-toggle="popover" data-content="Location"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Agenda" data-type="modules" data-path="agenda"><img src="img/templates/thumbnails/email-module-agenda.png" class="popover-hover" data-toggle="popover" data-content="Agenda"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="CTA" data-type="modules" data-path="cta"><img src="img/templates/thumbnails/email-module-cta.png" class="popover-hover" data-toggle="popover" data-content="CTA"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="CTA Alt" data-type="modules" data-path="cta-alt"><img src="img/templates/thumbnails/email-module-cta-alt.png" class="popover-hover" data-toggle="popover" data-content="CTA Alt"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Content" data-type="modules" data-path="content"><img src="img/templates/thumbnails/email-module-content.png" class="popover-hover" data-toggle="popover" data-content="Content"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Content Alt" data-type="modules" data-path="content-alt"><img src="img/templates/thumbnails/email-module-content-alt.png" class="popover-hover" data-toggle="popover" data-content="Content Alt"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Badge" data-type="modules" data-path="badge"><img src="img/templates/thumbnails/email-module-badge.png" class="popover-hover" data-toggle="popover" data-content="Badge"></a></li>
    </ul>
    <p class="clearfix"></p>
</div>

<!--Body Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a footer type</h2>
    <p class="section-note">Footer B, which includes social links, will not visually reflect the appropriate social icons for the URLs you input until production support builds the asset in Eloqua.  They are just placeholder to indicate approximate style and location.</p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-footer" >
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer A" data-type="footer" data-path="email-footer-a"><img src="img/templates/thumbnails/email-footer-a.png" class="popover-hover" data-toggle="popover" data-content="Footer A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer B" data-type="footer" data-path="email-footer-b"><img src="img/templates/thumbnails/email-footer-b.png" class="popover-hover" data-toggle="popover" data-content="Footer B"></a></li>
    </ul>
    <div id="" class="asset-preview asset-preview-email hide asset-footer">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>
