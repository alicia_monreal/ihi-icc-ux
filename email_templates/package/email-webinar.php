<!--Banner Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a banner type</h2>
    <p class="clone-section clone hide" name="header-clone"><i class="icon-share"></i> <a href="#" onclick="javascript:return false;">Clone Header</a></p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-header">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner A" data-type="header" data-path="email-banner-a"><img src="img/templates/thumbnails/email-banner-a.png" class="popover-hover" data-toggle="popover" data-content="Banner A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner B" data-type="header" data-path="email-banner-b"><img src="img/templates/thumbnails/email-banner-b.png" class="popover-hover" data-toggle="popover" data-content="Banner B"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner C" data-type="header" data-path="email-banner-c"><img src="img/templates/thumbnails/email-banner-c.png" class="popover-hover" data-toggle="popover" data-content="Banner C"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner D" data-type="header" data-path="email-banner-d"><img src="img/templates/thumbnails/email-banner-d.png" class="popover-hover" data-toggle="popover" data-content="Banner D"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner E" data-type="header" data-path="email-banner-e"><img src="img/templates/thumbnails/email-banner-e.png" class="popover-hover" data-toggle="popover" data-content="Banner E"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner F" data-type="header" data-path="email-banner-f"><img src="img/templates/thumbnails/email-banner-f.png" class="popover-hover" data-toggle="popover" data-content="Banner F"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner G" data-type="header" data-path="email-banner-g"><img src="img/templates/thumbnails/email-banner-g.png" class="popover-hover" data-toggle="popover" data-content="Banner G"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner H" data-type="header" data-path="email-banner-h"><img src="img/templates/thumbnails/email-banner-h.png" class="popover-hover" data-toggle="popover" data-content="Banner H"></a></li>
    </ul>

    <div id="" class="asset-preview asset-preview-email hide asset-header">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Body Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a body type</h2>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-content">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Body A" data-type="content" data-path="email-body-a"><img src="img/templates/thumbnails/email-body-a.png" class="popover-hover" data-toggle="popover" data-content="Email Body A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Body B" data-type="content" data-path="email-body-b"><img src="img/templates/thumbnails/email-body-b.png" class="popover-hover" data-toggle="popover" data-content="Email Body B"></a></li>
    </ul>
    <div id="" class="asset-preview asset-preview-email hide asset-content">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Additional Modules-->
<div class="thumbnail-block thumbnail-selector fourup">
    <div id="" class="asset-preview asset-preview-email hide asset-modules">
    </div>
    <h2 class="template-head">Add additional modules</h2>
    <p class="section-note">Some of the additional modules have variable column layouts which are governed by the rules in the Eloqua Template guidelines.  While entering text in the Asset Configuration, the columns will remain fixed.  To see the final output, please view the module from "Preview Mode".</p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-modules">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Speakers" data-type="modules" data-path="speakers"><img src="img/templates/thumbnails/email-module-speakers.png" class="popover-hover" data-toggle="popover" data-content="Speakers"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="CTA" data-type="modules" data-path="cta"><img src="img/templates/thumbnails/email-module-cta.png" class="popover-hover" data-toggle="popover" data-content="CTA"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="CTA Alt" data-type="modules" data-path="cta-alt"><img src="img/templates/thumbnails/email-module-cta-alt.png" class="popover-hover" data-toggle="popover" data-content="CTA Alt"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Content" data-type="modules" data-path="content"><img src="img/templates/thumbnails/email-module-content.png" class="popover-hover" data-toggle="popover" data-content="Content"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Content Alt" data-type="modules" data-path="content-alt"><img src="img/templates/thumbnails/email-module-content-alt.png" class="popover-hover" data-toggle="popover" data-content="Content Alt"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Badge" data-type="modules" data-path="badge"><img src="img/templates/thumbnails/email-module-badge.png" class="popover-hover" data-toggle="popover" data-content="Badge"></a></li>
    </ul>
    <p class="clearfix"></p>
</div>

<!--Body Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a footer type</h2>
    <p class="section-note">Footer B, which includes social links, will not visually reflect the appropriate social icons for the URLs you input until production support builds the asset in Eloqua.  They are just placeholder to indicate approximate style and location.</p>
    <p class="clone-section clone hide" name="footer-clone"><i class="icon-share"></i> <a href="#" onclick="javascript:return false;">Clone Footer</a></p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-footer" >
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer A" data-type="footer" data-path="email-footer-a"><img src="img/templates/thumbnails/email-footer-a.png" class="popover-hover" data-toggle="popover" data-content="Footer A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer B" data-type="footer" data-path="email-footer-b"><img src="img/templates/thumbnails/email-footer-b.png" class="popover-hover" data-toggle="popover" data-content="Footer B"></a></li>
    </ul>
    <div id="" class="asset-preview asset-preview-email hide asset-footer">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn-footer"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>
