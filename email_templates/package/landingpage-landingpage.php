
<?php global $tactic_type, $landingpage_count, $landingpage_listing; 

    if($tactic_type == 'enurture' || $_REQUEST['type'] == 'enurture' 
        || $tactic_type == 'newsletter' || $_REQUEST['type'] == 'newsletter') {  // if tactic_type is enurture or landingpage
        
        if(empty($tactic_type))
            $tactic_type = $_REQUEST['type'];

        if(!empty($_REQUEST['count'])) { // if count is not empty
            $landingpage_record_count = $_REQUEST['count']; // get from request count
        } else {
            $landingpage_record_count = $landingpage_count; // get from global field
        }

        if(empty($landingpage_listing['name'][$landingpage_record_count])) { // if empty landingpage name
            $landingpage_listing['name'][$landingpage_record_count] = "";
        }

        ?>
            <div class="thumbnail-block thumbnail-selector fourup">
                <h2 class="template-head">Landing Page Details</h2>
                    <center>
                    	<input type="hidden" name="<?php echo $tactic_type; ?>_number" id="<?php echo $tactic_type; ?>_number" value="<?php echo $landingpage_record_count; ?>" />
                    	<input type="text" id="landingpage_name_field" name="landingpage_name_field" class="input-medium-detail required" placeholder="Name of Landing Page" value="<?php echo $landingpage_listing['name'][$landingpage_record_count] ?>" >
                    </center>
            </div>
        <?php
    }
?>

<!--Banner Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a banner type</h2>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-oneup thumbnail-header">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Registration Banner B" data-type="header" data-path="page-banner-b"><img src="img/templates/thumbnails/registration-banner-b.png" class="popover-hover" data-toggle="popover" data-content="Banner B"></a></li>
    </ul>

    <div id="" class="asset-preview asset-preview-page hide asset-header">

    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Additional Modules-->
<div class="thumbnail-block thumbnail-selector fourup">
    <div id="" class="asset-preview asset-preview-page hide asset-modules">

    </div>
    <h2 class="template-head">Add additional modules</h2>
    <p class="section-note">Some of the additional modules have variable column layouts which are governed by the rules in the Eloqua Template guidelines.  While entering text in the Asset Configuration, the columns will remain fixed.  To see the final output, please view the module from "Preview Mode".</p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-fiveup thumbnail-modules">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Sponsors" data-type="modules" data-path="page-sponsors"><img src="img/templates/thumbnails/page-module-sponsors.png" class="popover-hover" data-toggle="popover" data-content="Sponsors"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Registration Form" data-type="modules" data-path="registration-form"><img src="img/templates/thumbnails/registration-form.png" class="popover-hover" data-toggle="popover" data-content="Registration Form"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Speakers" data-type="modules" data-path="page-speakers"><img src="img/templates/thumbnails/page-module-speakers.png" class="popover-hover" data-toggle="popover" data-content="Speakers"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Content Alt" data-type="modules" data-path="page-content-alt"><img src="img/templates/thumbnails/page-module-content-alt.png" class="popover-hover" data-toggle="popover" data-content="Content Alt"></a></li>
        <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Agenda" data-type="modules" data-path="page-agenda"><img src="img/templates/thumbnails/page-module-agenda.png" class="popover-hover" data-toggle="popover" data-content="Agenda"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Location" data-type="modules" data-path="page-location"><img src="img/templates/thumbnails/page-module-location.png" class="popover-hover" data-toggle="popover" data-content="Location"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Resources" data-type="modules" data-path="page-resources"><img src="img/templates/thumbnails/page-module-resources.png" class="popover-hover" data-toggle="popover" data-content="Resources"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Upcoming Events" data-type="modules" data-path="page-upcoming-events"><img src="img/templates/thumbnails/page-module-upcoming-events.png" class="popover-hover" data-toggle="popover" data-content="Upcoming Events"></a></li>
    </ul>

    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Body Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a footer type</h2>
    <p class="section-note">Footer B, which includes social links, will not visually reflect the appropriate social icons for the URLs you input until production support builds the asset in Eloqua.  They are just placeholder to indicate approximate style and location.</p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-footer" >
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Page Footer A" data-type="footer" data-path="page-footer-a"><img src="img/templates/thumbnails/registration-footer-a.png" class="popover-hover" data-toggle="popover" data-content="Footer A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Page Footer B" data-type="footer" data-path="page-footer-b"><img src="img/templates/thumbnails/registration-footer-b.png" class="popover-hover" data-toggle="popover" data-content="Footer B"></a></li>
    </ul>
    <div id="" class="asset-preview asset-preview-page hide asset-footer">

    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="icon-repeat icon-white popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>
