<style type="text/css">
body {
    width: 100% !important;
    -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none;
    margin: 0;
    padding: 0;
}

@media only screen and (max-device-width: 670px) {
    /* Mobile width and full width classes */
    body[mobile] .devicewidth { width: 320px !important; }

    body[mobile] .cellblocknopadding {
        display: block !important;
        padding: 0 !important;
        width: 100% !important;
    }

    body[mobile] .cellblock {
        display: block !important;
        width: 100% !important;
    }

    body[mobile] .full-width {
        width: 100% !important;
    }

    /* General class to hide desktop elements */
    body[mobile] .hideblock { display: none !important; }

    /* Event Template Styles */
    body[mobile] td[class="ev-fup-sidebar-subhead"] {
        padding-bottom: 23px !important;
    }

    body[mobile] .article { padding-bottom: 20px !important; }

    body[mobile] td[class="subhead_copy_wrapper"] {
        font-size: 16px !important;
        line-height: 20px !important;
    }

    body[mobile] td[class="logo"] { padding-bottom: 20px !important; }

    body[mobile] td[class="normal_copy_wrapper"],
    body[mobile] a[class="normal_copy_wrapper"] {
        font-size: 14px !important;
        line-height: 170% !important;
    }

    body[mobile] td[class="normal_copy_wrapper no-padding"] {
        padding: 0px 0px 5px !important;
        font-size: 14px !important;
        line-height: 170% !important;
    }

    body[mobile] td[class="normal_copy_wrapper no-bottom-padding"] {
        padding: 0px 0px 0px !important;
        font-size: 14px !important;
        line-height: 170% !important;
    }

    body[mobile] td[class="dates_item"] {
        padding: 5px 0px !important;
        font-size: 14px !important;
        line-height: 16px !important;
    }

    body[mobile] td[class="dates_item_last"] {
        padding-bottom: 20px !important;
        padding-left: 0px !important;
        font-size: 14px !important;
        line-height: 170% !important;
    }

    body[mobile] td[class="headline_padding"],
    body[mobile] td[class="headline_wrapper headline_padding"] {
        padding-bottom: 20px!important;
        font-size: 18px !important;
        line-height: 24px !important;
    }

    body[mobile] td[class="headline_wrapper"] {
        font-size: 18px !important;
        line-height: 24px !important;
    }

    body[mobile] td[class="normal_copy_dates"] {
        font-size: 16px !important;
        line-height: 20px !important;
    }

    body[mobile] td[class="no-padding"] { padding: 0 !important; }

    body[mobile] td[class="wrapper footer-title"] {
        display: block !important;
        width: 100% !important;
        padding: 0px 0px 16px 0px!important;
    }

    body[mobile] td[class="show-me"],
    body[mobile] table[class="medium-button show-me"] {
        display: block !important;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    body[mobile] td[class="content-td sub-field"] {
        display: block !important;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
        padding-right: 25px !important;
        padding-left: 25px !important;
    }

    body[mobile] td[class="content-td"] { padding-bottom: 25px !important; }

    body[mobile] td[class="header-text-td"],
    body[mobile] td[class="content-td"],
    body[mobile] table[class="content-td show-me"] {
        padding: 25px !important;
        height: 100% !important;
    }

    body[mobile] td[class="copyright-wrapper"] {
        padding: 25px 30px 0px 30px !important;
        height: 100% !important;
    }

    body[mobile] td[class="copyright-wrapper-elqua-footer"] {
        padding: 0px 25px 25px 25px !important;
        height: 100% !important;
    }

    body[mobile] td[class="wrapper span3 block"],
    body[mobile] td[class="wrapper last span3 block"],
    body[mobile] td[class="content-section"] {
        padding-right: 0px !important;
        padding-bottom: 20px !important;
    }

    body[mobile] td[class="wrapper span4 block"] {
        display: block !important;
        width: 100% !important;
        padding-right: 0px !important;
        padding-top: 25px !important;
    }

    body[mobile] td[class="content-span2-left"],
    body[mobile] td[class="content-span3-left"] {
        padding: 0px 0px !important;
        height: auto !important;
    }
    body[mobile] td[class="content-span2-right"],
    body[mobile] td[class="content-span3-center"] { padding: 25px 0px !important; }

    body[mobile] table[class="medium-button"] {
        width: 100% !important;
    }
    body[mobile] img[class="social-icon"] {
        width: 24px !important;
        height: 24px !important;
    }
    body[mobile] td[class="social-first"] {
        padding: 20px 20px 20px 0px !important;
        width: 24px !important;
        height: 24px !important;
    }
    body[mobile] td[class="social"] {
        padding: 20px 20px 20px 20px !important;
        width: 24px !important;
        height: 24px !important;
    }
    body[mobile] td[class="copyright"] {
        padding-top: 0px !important;
        padding-bottom: 18px !important;
        font-weight: bold;
        font-size: 12px;
        line-height: 120%
    }

    body[mobile] td[class="copyright last"] {
        padding-top: 0px !important;
        font-weight: bold;
        font-size: 12px;
        line-height: 120%
    }

    body[mobile] table[class="mobile-cta-container"] {
        max-width: 100% !important;
        padding-top: 0px !important;
        width: 100% !important;
    }

    body[mobile] td[class="mobile-cta"] {
        background-color: #fdb813 !important;
        background-image: url('http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6efbdd01-db80-473a-8062-6fd338d72cef}_intel-mobile-cta-pointer.gif');
        background-position: right;
        background-repeat: no-repeat;
        font-weight: normal;
        height: 50px;
        font-family: arial, helvetica, sans-serif;
        text-align: left !important;
        font-size: 18px !important;
        line-height: 18px;
        width: 100% !important;
        padding-left: 0px !important;
    }

    body[mobile] td[class="mobile-cta-text"],
    body[mobile] a[class="mobile-cta-text"] {
        color: #FFFFFF !important;
        text-decoration: none !important;
        font-size: 18px !important;
        line-height: 18px;
        font-weight: normal !important;
        font-family: arial, helvetica, sans-serif;
        display: inline-block;
        padding: 12px 5px 13px 10px !important;
        text-align: left !important;
        margin-right: 15px !important;
    }

    body[mobile] td[class="show-logo"] {
        background-image: url('http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png');
        display: block;
        background-position: left;
        background-repeat: no-repeat;
        height: 50px !important;
        width: 79px;
        text-align: left;
        padding-bottom: 20px !important;
        padding-top: 0px !important;
        padding-top: 0px !important;
    }

    body[mobile] tr[class="show-logo-tr"] {
        height: 50px !important;
        width: 79px;
        text-align: left;
        font-size: 50px;
        line-height: 50px;
        padding-bottom: 10px !important;
        padding-top: 0px !important;
    }

    body[mobile] td[class="show-badge"] {
        background-image: url('http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/%7Bbb5b456b-9ab3-4afc-a253-677a397ad041%7D_badge-i5.png');
        display: inline-block;
        background-position: left;
        background-repeat: no-repeat;
        height: 133px;
        width: 101px;
        text-align: left;
        padding-bottom: 0px !important;
        padding-top: 0px !important;
        padding-top: 0px !important;
        line-height: 153px;
        font-size: 153px;
        vertical-align: top;
    }

    body[mobile] tr[class="show-badge-tr"] {
        height: 153px;
        width: 101px;
        text-align: left;
        padding-bottom: 20px !important;
        padding-top: 0px !important;
        line-height: 153px;
        font-size: 153px;
    }

    body[mobile] td[class="title-cta last"] {
        max-width: 100% !important;
        padding-bottom: 0px !important;
    }

    body[mobile] table[class="title-cta-container"] {
        max-width: 100% !important;
        padding-top: 25px !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
        text-align: left;
        padding-bottom: 25px !important;
        max-height: none !important;
        height: auto !important;
        line-height: 50px !important;
        height: 50px !important;
    }

    body[mobile] table[class="title-cta-container not-bottom"] {
        max-width: 100% !important;
        padding-top: 0px !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
        text-align: left;
        padding-bottom: 0px !important;
        max-height: none !important;
        height: auto !important;
        line-height: 50px !important;
        height: 50px !important;
    }

    body[mobile] tr[class="title-cta-tr"] {
        line-height: 14px !important;
        height: 14px !important;
    }

    body[mobile] table[class="title-cta-container no-top"] {
        max-width: 100% !important;
        padding-top: 0px !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
        text-align: left;
        padding-bottom: 25px !important;
        max-height: none !important;
        display: table!important;
    }

    body[mobile] td[class="title-cta"] {
        color: #00AEEF !important;
        background-position: right;
        background-repeat: no-repeat;
        font-weight: bold !important;
        font-family: arial, helvetica, sans-serif;
        text-align: left !important;
        font-size: 14px !important;
        width: 100% !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
        line-height: 18px !important;
        max-height: none !important;
        display: block !important;
    }

    body[mobile] td[class="title-cta-text"],
    body[mobile] a[class="title-cta-text"] {
        background-color: #E9E9E9 !important;
        color: #00AEEF !important;
        text-align: left;
        text-decoration: none !important;
        font-size: 14px !important;
        line-height: 18px !important;
        font-weight: bold !important;
        font-family: arial, helvetica, sans-serif;
        display: block !important;
        padding: 12px 30px 13px 15px;
    }

    body[mobile] td[class="sidebar-td-blue"] { background-color: #0071c5; }

    body[mobile] td[class="message"],
    body[mobile] tr[class="message"] {
        color: #0071C5 !important;
        font-size: 16px !important;
        line-height: 18px !important;
        height: 18px !important;
        padding-bottom: 24px !important;
        display: block !important;
        max-height: none !important;
        font-weight: bold;
    }

    body[mobile] td[class="title-speakers-td-top"] { padding-top: 24px !important; }

    body[mobile] td[class="title-speakers-td-bottom"] { padding-bottom: 20px !important; }

    body[mobile] td[class="speakers-td-bottom"],
    body[mobile] td[class="last normal_copy_wrapper speakers-td-bottom"] {
        font-size: 14px !important;
        line-height: 170% !important;
        padding-bottom: 18px !important;
    }


    body[mobile] img[class="location-two-column image-fix"] {
        width: 100% !important;
        height: auto !important;
        max-width: 292px !important;
        max-height: 202px !important;
    }

    body[mobile] .hide-element,
    body[mobile] td[class="hide-element"],
    body[mobile] table[class="hide-element"],
    body[mobile] tr[class="hide-element"] {
        display: block !important;
        width: 100% !important;
        max-height: 100% !important;
    }
}/* End Media Query */
</style>
