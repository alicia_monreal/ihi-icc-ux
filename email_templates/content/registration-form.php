<div class="content-wrapper">
    <div class="main-content cf">
        <!-- Register Form -->
        <div class="module form cf">
            <div class="module-header">
                <h3 class="icon-sm blue-bookmark-sm content-header wys-inline-simple">Register</h3>
            </div>

            <div class="module-content">
                <div class="content wys-inline-simple">
                    <h4 class="">Call to action call<br> to action call.</h4>

                    <p class="body_copy_text wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, perferendis, nam libero inventore et adipisci quibusdam quos doloremque laborum sapiente.</p>
                </div>

                <form name="register-form" id="registerForm" action="">
                    <small class="required">Required Fields (*)</small>
                    <div id="registerFields" class="register-fields" data-name="fields-form">

                        <div class="remove_first_name">
                            <div class="ui-state-default">
                                <label class="wys-inline-simple">First Name <span>*</span></label>
                                <input type="text" id="first-name">
                                <div class=" remove removeSocial" data-remove-id="first_name" alt="Remove" onclick="removeThis('.remove_first_name');"></div>
                                <div class="icon-move edit removeSocial handle"  alt="Edit"></div>
                            </div>
                        </div>

                        <div class="remove_last_name">
                            <div class="ui-state-default">
                                <label class="wys-inline-simple">Last Name <span>*</span></label>
                                <input type="text" id="last-name">
                                <div class=" remove removeSocial" data-remove-id="last_name" alt="Remove" onclick="removeThis('.remove_last_name');"></div>
                                <div class="icon-move edit removeSocial handle"  alt="Edit" ></div>
                            </div>
                        </div>
                        
                        <div class="remove_email_address">
                            <div class="ui-state-default">
                                <label class="wys-inline-simple">Email Address <span>*</span></label>
                                <input type="email" id="email-address">
                                <div class="remove removeSocial" data-remove-id="email_address" alt="Remove" onclick="removeThis('.remove_email_address');"></div>
                                <div class="icon-move edit removeSocial handle"  alt="Edit" ></div>
                            </div>
                        </div>    
                        
                        <div class="remove_company">
                            <div class="ui-state-default">
                                <label class="wys-inline-simple">Company <span>*</span></label>
                                <input type="text" id="company">
                                <div class=" remove removeSocial" data-remove-id="company" alt="Remove" onclick="removeThis('.remove_company');"></div>
                                <div class="icon-move edit removeSocial handle"  alt="Edit" ></div>
                            </div>
                        </div>

                        <div class="remove_workwithus checkbox cf">
                            <div class="ui-state-default">
                                <div class="chk-radio checkbox">
                                    <input class="styled-checkbox" id="work-with-1" type="checkbox">
                                    <label for="work-with-1" style="width:20px !important;"></label>
                                    <label class="chkLbl wys-inline-simple">Would you like to work with an Intel solutions expert?</label>
                                </div>
                                <div class="remove  removeSocial" data-remove-id="work-with-1" alt="Remove" onclick="removeThis('.remove_workwithus');"></div>
                                <div class="icon-move edit removeSocial handle"  alt="Edit" ></div>
                            </div>
                        </div>
                    </div>
                    
                    <button id="addCustomFieldsBtn" class="btn-green add" data-toggle="modal" data-target="#customField">Add additional fields</button>

                    <div class="submit-btn"><label class="wys-inline-simple">Submit</label></div>

                    <small class="form-legal disclaimer_copy_text wys-inline">By providing your contact information, you authorize Intel to contact you by email or telephone with information about Intel products, events, and updates for {XXX}. For more information, please review the <a href="http://www.intel.com/content/www/us/en/legal/terms-of-use.html" target="_blank">Terms of Use</a> and <a href="http://www.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" target="_blank">Intel Privacy Notice</a>.</small>
                </form>
            </div>
        </div><!-- /.module -->
    </div>
</div>

<!-- Edit a field -->
<div id='editFields' class='download-modal modal hide fade' tabindex="-1" role='dialog'>
    <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="customFieldLabel" class="forgot-form-label">Edit field</h3>
    </div>
    <div class="modal-body">
        <form action="" id="edit-field-form" class="download-form" data-form="custom-field">
            <fieldset>
                <p><label>Title</label>
                    <input type="text" name="title_custom_field" id="modal_field_title" value="" class="required">
                </p>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a id="editField" class="btn-green submit" data-analytics-label="Submit Form: Custom Field">Save & Close</a></p>
            </fieldset>
        </form>
    </div>
</div>

<!-- Add custom field -->
<div id="customField" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="customFieldLabel" aria-hidden="true">
  <div class="modal-header">
    <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
    <h3 id="customFieldLabel" class="forgot-form-label">Add custom field</h3>
  </div>
  <div class="modal-body">
    <form action="" id="custom-field-form" class="download-form" data-form="custom-field">
        <fieldset>
            <p><label>Type*</label>
                <select name="input_type" id="input_type">
                    <option value="">Select Type</option>
                    <option value="text">Text Field</option>
                    <option value="textarea">Text Box</option>
                    <option value="checkbox">Check Box</option>
                    <option value="radio">Radio Button</option>
                    <option value="select">Dropdown Menu</option>
                </select>
            </p>
            <p><label>Title</label><input type="text" name="title_custom_field" id="title_custom_field" placeholder="Title" value="" class="required"></p>
            
            <!-- Choices -->
            <div id="checkbox" class="custom-field-single">
                <label for="input_choices">Choices</label>
                <input type="text" name="input_choices" id="input_choices" class="input-full">
                <input type="text" name="input_choices_0" id="input_choices_0" class="input-full">
                <div id="multicheckbox-additional"></div>
                <p><a href="" class="btn-green add" id="addMultiChoice">Add Choice</a></p>
            </div>

            <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a id="saveCustomField" class="btn-green submit" data-analytics-label="Submit Form: Custom Field">Save & Close</a></p>
        </fieldset>
    </form>
  </div>
</div>

<script type="text/javascript">

    /* reorder fields functionality */
    $( "#registerFields" ).sortable(
{
    handle:'.handle',
    tolerance:'intersect'
}
        );

    /* CUSTOM FIELDS */
    $('#input_type').change(function() {
        var opt = $(this).find('option:selected').val();
        var opt_text = $(this).find('option:selected').text();
        $('.custom-field-single').hide();
        // reset the title value
        $("#title_custom_field").val("");
        if(opt == "checkbox" || opt == "radio" || opt == "select"){
            // show the choices
            var searchInput = $("#checkbox :input");
            for(var i = 0; i < searchInput.length; i++) {
                // reset the choices values
                searchInput[i].value = "";
            }
            // show the choices div
            $('#checkbox').show();
        }else{
            $('#'+opt).show();
        }
    });

    // add new choice functionality
    var multichoiceDiv = $('#multicheckbox-additional');
    var i_mc = $('#multicheckbox-additional .add-list').size() + 1;
    $('#addMultiChoice').bind('click', function() {
        $('<div class="add-list remove_'+i_mc+'"><input type="text" name="input_choices_'+i_mc+'" id="input_choices_'+i_mc+'" class="input-full"><div class="remove removeSocial" data-remove-id="'+i_mc+'" alt="Remove" onclick="removeThis(\'.remove_'+i_mc+'\');"></div></div>').appendTo(multichoiceDiv);
        i_mc++;
        return false;
    });

    // add new custom field to registration form
    $("#saveCustomField").bind('click', function(ev){
        // custom field type
        var opt = $("#input_type").val(),
        // custom field title
            title = $("#title_custom_field").val(),
        // get the registration form div
            rdiv = $("#registerFields"),
        // get the choices
            searchInput = $("#checkbox :input"),
            style="";
            editStyle='';
        // add style to the remove icon
        if(opt == "textarea"){
            style= "remove-ta";
            editStyle='edit-ta';}
        else if(opt == "checkbox")
            style= "checkbox";
        // get a random title
        var titlews = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyz'),
            remove1 = "<div class='remove_"+titlews+"'><div class=ui-state-default>",
            remove2 = "<div class='remove  "+style+" removeSocial' data-remove-id="+titlews+" alt=Remove onclick=removeThis('.remove_"+titlews+"');></div>",
            edit    = "<div class='icon-move edit removeSocial handle "+editStyle+"'  alt='Edit' data-toggle='modal' data-target='#editFields' onclick='editField(event);'></div>";

        if(opt==""){
             // empty validation
             alert("Field Title cannot be empty or select field type.");
        }else{
            // add custom label to the registration form
            switch(opt){
                case "text":
                    // add textfield
                    $(remove1+"<label class='custom-title wys-inline-simple'>"+title+"</label><input name="+titlews+" id="+titlews+" type="+opt+">"+remove2+edit).appendTo(rdiv);
                    break;
                case "textarea":
                    // add textarea
                    $(remove1+"<label class='custom-title wys-inline-simple'>"+title+"</label><"+opt+" name="+titlews+" id="+titlews+"></"+opt+">"+remove2+edit).appendTo(rdiv);
                    break;
                case "checkbox":
                case "radio":
                    var value = "", valid = false, inputs = "";
                    for(var i = 0; i < searchInput.length; i++) {
                        // get the choices value
                        value = searchInput[i].value;
                        if(value != ""){
                            // add the checkbox or radio options
                            inputs += "<div class='chk-radio "+style+"'><input class='styled-checkbox' type="+opt+" id="+titlews+"_"+i+" name="+titlews+" value="+value.replace(/ +/g, "")+"><label for="+titlews+"_"+i+" style='width:20px !important;'></label><label class=chkLbl for="+titlews+"_"+i+">"+value+"</label></div>";
                            valid = true;
                        }
                    }
                    if(!valid){
                        alert("Please enter the choices");
                        return false;
                    }
                    $(remove1+"<label class='custom-title wys-inline-simple'>"+title+"</label>"+inputs+remove2+edit).appendTo(rdiv);
                    break;
                case "select":
                    var select = "<select name="+titlews+">";
                    var value = "", valid = false;
                    for(var i = 0; i < searchInput.length; i++) {
                        value = searchInput[i].value;
                        if(value != ""){
                            // add the select options test
                            select += "<option value="+value.replace(/ +/g, "")+">"+value+"</option>";
                            valid= true
                        }
                    }
                    select += "</select>";
                    if(!valid){
                        alert("Please enter the choices");
                        return false;
                    }
                    // add the select field
                    $(remove1+"<label class='custom-title wys-inline-simple'>"+title+"</label>"+select+remove2+edit).appendTo(rdiv);
                    break;
                default:
                    break;
            }
            // hide custom field modal
            $('#customField').modal('hide');
             $( ".registerFields" ).sortable( "option", "handle", ".handle" );
            tplEditor.reinitialize();
        }
    });
    
</script>