<div class="content-wrapper">
    <div class="main-content cf">
		<!-- Thank You Module -->
		<div class="module asset cf">
			<div class="module-content">
				<div class="inner-content">
					<h4 class="content-header wys-inline-simple">Thank you.</h4>

					<p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptatum quis sapiente sed ullam repellat harum rerum recusandae dolores, accusantium suscipit eius possimus sit laborum enim eveniet, provident et maxime.</p>						
				</div>
			</div>
			
			<div class="full-width-block">
				<h4 class="sidebar-header white-document-lg wys-inline-simple">Title of offer</h4>

				<p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, doloribus.</p>

				<a class="cta wys-inline-link">Download</a>
			</div>
		</div><!-- /.module -->
	</div>
</div>