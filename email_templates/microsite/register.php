<!-- Register Form -->
<div class="module form cf">
    <div class="module-header">
        <h3 class="icon-sm blue-bookmark-sm content-header">Register</h3>
    </div>

    <div class="module-content">
        <div class="content">
            <h4 class="regbody_header_text">Call to action call<br> to action call.</h4>

            <p class="registration_body_copy_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, perferendis, nam libero inventore et adipisci quibusdam quos doloremque laborum sapiente.</p>
        </div>

        <form action="">
            <small class="required">Required Fields (*)</small>
            <label for="first-name">First Name <span>*</span></label>
            <input type="text" id="first-name">

            <label for="last-name">Last Name <span>*</span></label>
            <input type="text" id="last-name">

            <label for="email-address">Email Address <span>*</span></label>
            <input type="email" id="email-address">

            <label for="company">Company <span>*</span></label>
            <input type="text" id="company">

            <div class="checkbox cf">
                <input class="styled-checkbox" id="work-with-1" type="checkbox">
                <label for="work-with-1"></label>
                <p class="checkbox_text">Would you like to work with an Intel solutions expert?</p>
            </div>

            <div class="submit-btn"><label>Submit</labe></div>

            <small class="form-legal disclaimer_copy_text">By providing your contact information, you authorize Intel to contact you by email or telephone with information about Intel products, events, and updates for {XXX}. For more information, please review the <a href="http://www.intel.com/content/www/us/en/legal/terms-of-use.html" target="_blank">Terms of Use</a> and <a href="http://www.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" target="_blank">Intel Privacy Notice</a>.</small>
        </form>
    </div>
</div><!-- /.module -->