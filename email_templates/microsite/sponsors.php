<!-- Sponsors -->
<div class="module sponsors cf">
    <div class="module-header">
        <h3 class="icon-sm blue-usergroup-sm content-header">Sponsors</h3>
    </div>

    <div class="module-content">
        <div class="content cf">
            <div class="sponsor">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/company-fpo-1.jpg" alt="">
                <h4>Company Name</h4>
            </div>

            <div class="sponsor middle">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/company-fpo-2.jpg" alt="">
                <h4>Company Name</h4>
            </div>

            <div class="sponsor">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/company-fpo-3.jpg" alt="">
                <h4>Company Name</h4>
            </div>

            <div class="sponsor">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/company-fpo-4.jpg" alt="">
                <h4>Company Name</h4>
            </div>

            <div class="sponsor middle">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/company-fpo-5.jpg" alt="">
                <h4>Company Name</h4>
            </div>

            <div class="sponsor">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/company-fpo-6.jpg" alt="">
                <h4>Company Name</h4>
            </div>
        </div>
    </div>
</div>