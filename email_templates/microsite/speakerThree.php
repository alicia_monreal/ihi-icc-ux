<!-- Speakers - Three Col -->
<div class="module speakers-three-col cf">
    <div class="module-header">
        <h3 class="icon-sm blue-comments-sm content-header">Speakers</h3>
    </div>

    <div class="module-content">
        <div class="content cf">
            <div class="speaker">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/speaker-fpo-1.jpg" alt="">

                <h4>Name of Speaker</h4>
                <h5>Title of speaker</h5>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
            </div>

            <div class="speaker">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/speaker-fpo-2.jpg" alt="">

                <h4>Name of Speaker</h4>
                <h5>Title of speaker</h5>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
            </div>

            <div class="speaker">
                <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/speaker-fpo-3.jpg" alt="">

                <h4>Name of Speaker</h4>
                <h5>Title of speaker</h5>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
            </div>
        </div>
    </div>
</div>