<!-- Full width footer -->
<footer class="cf" style="height:auto;">
	<div id="footer-kink" class="kink cf">
		<div class="inner-content" style="display: none;">
			<h5>Conversations</h5>
			
			<nav id="social-media">
				<ul class="horizontal-menu cf">
					<li><a href="https://www.facebook.com/Intel" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://www.linkedin.com/company/intel-corporation" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="https://twitter.com/intel" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a href="http://www.intel.com/content/www/us/en/blogs-communities-social.html" title="AddThis" target="_blank"><i class="fa fa-plus"></i></a></li>
				</ul>	
			</nav>				
		</div>
	</div>
	
	<div id="footer-legal">
		<div class="wrapper">
			<div class="inner-content" style="padding:40px 2.02128% 60px 2.02128%;width:100%;">
				<p class="copyright_information_text">Copyright &copy; 2014 Intel Corporation. All rights reserved. Intel, the Intel logo, {List relevant trademarks} are trademarks of Intel Corporation in the U.S. and/or other countries.<br /><br />Intel is committed to protecting your privacy. For more information about Intel's privacy practices, please visit <a href="http://www.intel.com/privacy" target="_blank" title="Intel Online Privacy Notice Summary">www.intel.com/privacy</a> or write to Intel Corporation, ATTN Privacy, Mailstop RNB4-145, 2200 Mission College Blvd., Santa Clara, CA 95054 USA<br /><br /><a href='https://www-ssl.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html'>Privacy</a> | <a href='https://www-ssl.intel.com/content/www/us/en/privacy/intel-cookie-notice.html'>Cookies</a> | <a href='#'>Unsubscribe</a> | <a href='https://www-ssl.intel.com/content/www/us/en/legal/trademarks.html'>*Trademarks</a></p>
				
				<!--<nav id="legal">
					<ul class="horizontal-menu cf">
						<li style="margin-right:0;"><a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" title="Privacy" target="_blank">Privacy</a></li>
						<li><a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-cookie-notice.html" id="_bapw-link" title="Cookies" target="_blank">Cookies</a></li>
						<li><a href="https://www-ssl.intel.com/content/www/us/en/legal/trademarks.html" title="* Trademarks" target="_blank">* Trademarks</a></li>
					</ul>						
				</nav>-->
			</div>
		</div>			
	</div>
</footer><!--/Footer-->