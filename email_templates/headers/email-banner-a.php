<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; line-height:27px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0;font-size: 12px;">

            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0px 0px 0px 0px;" align="center">
                <tbody><tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="no-padding image-line" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; padding: 0;">
                            <tbody><tr>
                                <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Image -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 265px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;height: 413px;" bgcolor="#0071c5">

                                        <!-- NOTE: Change width based on image width -->

                                        <tbody><tr>
                                            <td style="height: 413px; width:265px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  margin: 0; padding: 0;" align="left" valign="top">
                                                <img src="/img/templates/default-images/placeholders/header-image-email-fpo-a.png" placeholder="header-image-email-fpo-a.png" class="image-fix image-box-custom" data-name="custom-image" data-attr="headerA" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="265" height="413">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Header Text -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 405px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">

                                        <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                        <tbody><tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 27px; margin: 0; padding: 27px 33px 24px 33px;height:347px;" align="left" valign="top">
                                                <!-- NOTE: Specific extra padding based on Ozone template. Default is 25px all around -->

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                    <tbody><tr>
                                                        <td align="right" style="text-align: right; font-size: 12px; border-collapse: collapse !important; vertical-align: top; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 50px; margin: 0; padding: 0px;" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png" alt="Intel" align="right" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="79" height="50">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="headline_wrapper wys-inline-simple" data-name="head-title" style="font-size: 32px; line-height: 34px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; display: block; clear: both;padding:28px 0px 15px 0px" align="left">Lorum ispsum dolor amedfa lorum ipsum.

                                                        </td>

                                                    </tr>
                                                    <!-- NOTE: Set the size/color inline or will default to default size -->
                                                    <tr>

                                                        <td class="normal_copy_wrapper wys-inline-simple" data-name="head-body" style="font-size: 14px; line-height: 17px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: left; padding: 0px 0px 20px 0px;">
                                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal_copy_dates wys-inline-simple" data-name="head-date" style="font-size: 14px; line-height: 22px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: left;  padding: 0px 0px 25px 0px;" align="left">Date: Jan 30–Feb 2<br />Time: 12:30 PST / 3:30 EST<br/>Location: Location
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px;line-height:20px; ">
                                                            <!--ORANGE BUTTON -->
                                                            <table border="0" cellpadding="0" cellspacing="0" class="medium-button" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; overflow: hidden; padding: 0px 0px 0px 0px;">
                                                                <tbody><tr>
                                                                    <td style="font-size: 20px; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 20px; width: auto !important; background-color: #fdb813; margin: 0; padding: 12px 5px 12px 20px;" align="left" bgcolor="#fdb813" valign="top">
                                                                        <table class="inner-button" border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #fdb813;">
                                                                            <tbody><tr>
                                                                                <td class="button-text wys-inline-link" style="background-color: #fdb813; color: #ffffff; font-weight: normal; font-family: arial, helvetica, sans-serif; font-size: 20px; line-height:21px; padding-right: 15px;text-align: left;"><a href="#" style="color: #ffffff; text-decoration: none;">Register Now </a>
                                                                                </td>
                                                                                <td width="12" align="left" style="font-size: 21px; line-height:21px;padding-right:15px;"><a href="#" style="font-size: 21px; line-height:21px;"><img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{049eebcb-f1ec-422e-852a-16aeb25d156c}_white-arrow.png" style="display: block;" width="12" height="21" border="0"></a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                            <!--/ORANGE BUTTON -->

                                                        </td>
                                                    </tr>

                                                </tbody></table>
                                                <!-- NOTE: Set the size/color inline or will default to default size -->

                                            </td>

                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->
                    </td>
                </tr>
            </tbody></table>
        </td>
        <!-- /Container -->
    </tr>
</tbody></table>

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
