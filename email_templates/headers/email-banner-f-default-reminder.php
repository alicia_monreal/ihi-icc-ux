<table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->
        <td align="center" valign="top" class="devicewidth" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tbody><tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; padding: 0;" align="left" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; padding: 0;">
                            <tbody><tr>
                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 520px; height:195px; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0px;" bgcolor="#0071c5">
                                        <tbody><tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: bottom; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 50px 30px 20px 25px;" align="left">
                                                <!-- NOTE: Specific extra padding based on Ozone template. Default is 25px all around -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody><tr class="show-logo-tr" style="line-height:10px;font-size:10px;">
                                                        <td class="show-logo" style="line-height:10px;font-size:10px;">&nbsp;</td>
                                                    </tr>
                                                    <!-- Headline -->
                                                    <tr>
                                                        <td class="headline_wrapper headline_padding event_header_text" style="font-size: 30px; line-height: 32px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: break-word; display: block; clear: both; padding: 0px 0px 5px 0px;" align="left">Lorum ispsum dolor amedfa lorum ipsum.</td>
                                                    </tr>

                                                    <!-- NOTE: Set the size inline or will default to default size -->

                                                    <!-- CTA -->
                                                    <tr>
                                                        <td style="padding: 15px 0px 0px 0px;font-size: 18px;font-family: Arial, Helvetica, sans-serif;  line-height: 18px;" valign="top">
                                                            <table class="mobile-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tbody><tr>
                                                                    <td class="mobile-cta" style="font-size: 18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;"><a href="#" style="font-size: 18px; color: #ffda00; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 18px;text-decoration:none;">Attend<span class="hideblock" style="font-size: 18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;color:#ffffff;"> &gt;</span></a></td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                    <!-- End CTA -->
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                                <!-- /wrapper -->
                                <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; padding: 0px;" align="left" valign="top"> <!-- Image -->
                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 150px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;height:213px;" bgcolor="#0071c5">
                                        <!-- NOTE: Change width based on image width -->
                                        <tbody><tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 12px; margin: 0; padding: 20px 23px 40px 23px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                    <tbody><tr>
                                                        <td style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 50px; height:50px; margin: 0; padding-bottom: 20px;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png" alt="Intel" align="right" class="hideblock" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block; width:79px; height: 50px;">
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="hideblock" valign="bottom" style="vertical-align: bottom !important; height: 85px; line-height: 85px; font-size: 12px; border-collapse: collapse !important; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; padding: 0;" align="left">
                                                <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{42f52c6d-4813-41c7-8257-b92216f02fcc}_tear-away-98x85.png" align="right" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;width:98px; height:85px">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->
                    </td>
                </tr>
            </tbody></table>
        </td>

        <!-- /Container -->

    </tr>
</tbody></table>

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
