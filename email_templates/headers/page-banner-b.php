<div class="content-wrapper">
    <div class="main-content cf">
        <div class="cf">
            <div id="header" class="cf">
                <div id="header-content" class="event-header no-fold ">
                    <div class=" wys-inline-simple">
                        <h1 class="event_header_text wys-inline-simple">Headline in Sans-serif Bold Font and a lot of characters</h1>
        
                        <h3 class="event-title wys-inline-simple">Name of Event<br />Thursday, July 20, 2014<br />10:00 am - 5:00 pm</h3>
                    </div>
                </div>

                <div class="image-container" style="width:501px; height:365px;">
                    <img style="max-width: 100%; width:501px; height:365px;" class="image-fix image-box-custom" src="/img/templates/default-images/placeholders/header-image-page-fpo-b.png" placeholder="header-image-page-fpo-b.png">
                </div>
            </div>
        </div>
    </div>
</div>
