<?php
$loc = 'campaign-requests';
include('includes/head.php');
include('includes/header.php');
?>

	<div class="container">
		<div class="row intro-body">
			<div class="intro span12">
				<h1>Campaign Request Form</h1>
				<p>Fill out this form to request a new campaign. At a minimum, you will need a completed campaign brief and optional registration imagery to complete the new campaign request. It is recommended that other assets be completed and on hand during creation but you may upload them later up to 72 hours prior to the event. If you don't have a campaign creative brief form, you can download it below.</p>
				<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fourup">
				<li class="active">Step 1</li>
				<li>Step 2</li>
				<li>Step 3</li>
				<li>Step 4</li>
			</ul>
			<div class="request-step step1">
				<form id="content-update-form" action="includes/_requests.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="start">
					<input type="hidden" id="step" name="step" value="1">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">

					<div class="control-group">
						<label for="geo" class="control-label">Target GEO</label>
						<div class="controls">
							<label class="checkbox inline"> <input type="checkbox" id="selectall" name="selectall" value="" class="checkall"> Select all</label>
							<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="APJ" <?php if (in_array('APJ', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> APJ</label>
							<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="EMEA" <?php if (in_array('EMEA', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> EMEA</label>
							<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="LAR" <?php if (in_array('LAR', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> LAR</label>
							<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="NAR" <?php if (in_array('NAR', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> NAR</label>
							<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="PRC" <?php if (in_array('PRC', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> PRC</label>
						</div>
					</div>
					<div class="control-group">
						<label for="sector" class="control-label">Sector</label>
						<div class="controls"><span class="required-mark">*</span><select id="sector" name="sector" class="input-wide required">
								<option value="">Select One</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="subsector" class="control-label">Sub-Sector</label>
						<div class="controls"><select id="subsector" name="subsector" class="input-wide">
								<option value="">Select Sector First</option>
							</select><br />
							<input type="text" id="subsector_other" name="subsector_other" class="input-wide" placeholder="If other, please enter the other sub-sector" value="<?php echo $_SESSION['subsector_other']; ?>" style="clear:both;margin-top:10px;margin-left:64px;" <?php if (!isset($_SESSION['subsector']) || $_SESSION['subsector'] != 'Other') { echo 'disabled'; } ?>>
						</div>
					</div>

					<div class="fieldgroup">
						<label for="type">What type of event are you planning?</label>
						<select id="type" name="type" class="input-xlarge required">
							<option value="webinar">Webinar (50+ people)</option>
						</select>
					</div>
					<div class="fieldgroup">
						<label for="email-template">Select your landing page and email template from the options below.<br /><span style="font-weight:normal;">Download the campaign brief template to see the editable fields for each template option.</span></label>
						<img src="img/templates/email-template-one.png" alt="Intel Default" style="margin-bottom:6px;"><br />
						<label class="radio inline"> <input type="radio" id="email_template" name="email_template" value="Intel Default" checked="checked"> Intel Default</label>
					</div>
					<div class="fieldgroup">
						<label for="continue">If you haven't filled out the campaign brief, select DOWNLOAD below.<br />If you do have completed templates, select CONTINUE.</label>
						<p style="margin:24px 0 40px 4px;"><a href="files/Intel Customer Connect - Intel Standard Email and Registration Content.docx" class="btn-green download" data-analytics-label="Download: Content Templates" style="margin-right:5px;">Download</a><a href="" id="submit-request-form" class="btn-green submit" data-analytics-label="Submit Form: Campaign Request Step 1" style="margin-left:5px;">Continue</a></p>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php include('includes/footer.php'); ?>

