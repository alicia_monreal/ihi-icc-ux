//Template Editor Plugin -- To be included in any page open to inline editing


 $(document).ready(function() {
    // $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.mode = 'popup';
    var templateEngine          = new templateEngine();

    templateEngine.initialize();

    function templateEngine(){
        var dirtyBit =  false;

        var initialize = function(){
            var me=  this;
            me.init_duplicates();
            me.initialize_edits();
        };

        var init_duplicates = function(){
            //Imitialize dulicates
            var ihi_modules =  $('.module');
            var ihi_module  = null;
            var oParent     = null;

            for(var i=0; i<ihi_modules.length;i++){
                ihi_module = ihi_modules[i];
                ihi_module = $(ihi_module);
                oParent    = ihi_module.parent();
                $(oParent.append(getButton()));
                var oAppend = $(oParent[oParent.length-1]);
                oAppend = $(oAppend);

                oAppend.on('click',function(elm){
                    var oParent_clk = $(elm.target).parent();
                    var oMod_clk = oParent_clk.find('.module');
                    oMod_clk = oMod_clk.clone();
                    oMod_clk.insertBefore($(elm.target));
                    templateEngine.initialize_edits();
                });
            }
        };

        var edit_made = function(){
            templateEngine.dirtyBit = true;
        };

        var initialize_edits = function(){
            //simple text
            $('.edit').editable({
                type: 'text',
                title: 'Edit',
                success: function(response, newValue) {
                    templateEngine.edit_made();
                }
            });

            //Wsywig
            $('.editw').editable({
                title: 'Edit Content',
                type:'wysihtml5',
                placement: 'right',
                success: function(response, newValue) {
                    templateEngine.edit_made();
                }
            });

            //Text area
            $('.editta').editable({
                type: "textarea",
                showbuttons: 'bottom'
            });

            $('.editd').editable({
                type:'date',
                showbuttons: 'bottom',
                viewformat: "M dd, yyyy",
                placement : 'right'
            });
        }

        function  getButton(){
            return '<button type="button" class="btn btn-primary module-btn">Add more</button>';
        }


        return {
            init_duplicates : init_duplicates,
            initialize: initialize,
            initialize_edits: initialize_edits,
            dirtyBit : dirtyBit,
            edit_made: edit_made
        }
    }


 });

