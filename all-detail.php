<?php

$loc = 'all-requests';
include('includes/head.php');
include('includes/header.php');
require_once('includes/_globals.php');
$c_type = true;
$step = 3; //This includes template.js
$editable = true; //Includes the JS

$owner = false;
$edit = false;
if (isset($_GET['edit'])) {
    $edit = false; //EDIT is always FALSE in this mode
}
?>

   <div class="container">
        <div class="row intro-body">
            <div class="span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
                            req.*,
                            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                            status.value AS status_value,
                            req.request_type AS request_type_id,
                            request_type.request_type AS request_type,
                            users.fname AS fname,
                            users.lname AS lname,
                            users.email AS email,
                            users.role AS role,
                            users.point_of_contact AS point_of_contact,
                            users.point_of_contact_email AS point_of_contact_email
                        FROM request AS req
                        INNER JOIN status on req.status = status.id
                        INNER JOIN request_type on req.request_type = request_type.id
                        INNER JOIN users on req.user_id = users.id";

                //This would be the All Requests view for every user. So losing this permission for now
                /*if ($_SESSION['admin'] == 1) {
                    $query.=" WHERE req.active = 1";
                } else {
                    $query.=" WHERE req.user_id = '".$_SESSION['user_id']."' AND req.active = 1";
                }*/

                $query.=" WHERE req.active = 1";

                $query.=" AND req.id = '".$_GET['id']."' LIMIT 1";

                $result = $mysqli->query($query);
                $row_cnt = $result->num_rows;
                $result_users = array();

                //print_r($query);

                if ($row_cnt > 0) {

                    while ($obj = $result->fetch_object()) {

                        $extra_owners = array(); 

                        if(!empty($obj->additional_owners)) // check if any additional owners exists
                            $extra_owners = unserialize($obj->additional_owners); // if yes then unserialize the array

                        //We need to know if this person owns this
                        if ($_SESSION['admin'] == 1 || $obj->user_id == $_SESSION['user_id'] || in_array($_SESSION['user_id'],$extra_owners)) { $owner = true; } else { $owner = false; }

                        echo '<h1>'.getTitle($_GET['id'], $obj->request_type_id).'</h1>';

                        //Admins/owners get a form
                        if (($_SESSION['admin'] == 1 || $edit == true) && ($owner == true)) {
                            echo '<form id="content-update-form-inline" action="includes/_requests_update.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" id="action" name="action" value="update">';
                                if ($edit) { echo '<input type="hidden" id="edit_form" name="edit_form" value="true">'; }
                                echo '<input type="hidden" id="user_id" name="user_id" value="'.$obj->user_id.'">
                                <input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">
                                <input type="hidden" id="request_id" name="request_id" value="'.$_GET['id'].'">
                                <input type="hidden" id="request_type_id" name="request_type_id" value="'.$obj->request_type_id.'">
                                <input type="hidden" id="user_email" name="user_email" value="'.getEmailAddress($obj->user_id).'">';
                        }

                        echo '<div id="zebra" class="single-request">';

                        //Grab user data for admin
                        if ($_SESSION['admin'] == 1) {
                            echo '<div class="row"><label>Requestor</label> <div class="pull-left">'.$obj->fname.' '.$obj->lname.' / <a href="mailto:'.$obj->email.'">'.$obj->email.'</a></div></div>';
                            if ($obj->point_of_contact != '') { echo '<div class="row"><label>Point of Contact</label> <div class="pull-left">'.$obj->point_of_contact; if ($obj->point_of_contact_email != '') { echo ' / <a href="mailto:'.$obj->point_of_contact_email.'">'.$obj->point_of_contact_email.'</a>'; } echo '</div></div>'; }
                        }
                        echo '<div class="row"><label>Date Created</label> <div class="pull-left">'.$obj->date_created.'</div></div>';
                        echo '<div class="row"><label>Request Type</label> <div class="pull-left">'.$obj->request_type.'</div></div>';

                        //Admin can change requestor
                        if ($_SESSION['admin'] == 1) {
                        echo '<div class="row"><label>Change Requestor</label> <div class="pull-left">';
                            $query_users = "SELECT id, fname, lname FROM users WHERE active = 1 ORDER BY fname ASC";
                            $result_users = $mysqli->query($query_users);
                            echo '<select id="new_user_id" name="new_user_id">';
                            while ($obj_users = $result_users->fetch_object()) {
                                echo '<option value="'.$obj_users->id.'"';
                                if ($obj->user_id == $obj_users->id) {
                                    echo ' selected="selected"';
                                }
                                echo '>'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                            }
                            echo '</select>';
                        echo '</div></div>';
                        } else {
                            echo '<input type="hidden" id="new_user_id" name="new_user_id" value="'.$obj->user_id.'">';
                        }

                        echo '<div class="row"><label>Status</label> <div class="pull-left">';
                        //Admin can change status
                        if ($_SESSION['admin'] == 1) {
                            $query_status = "SELECT * FROM status WHERE id != 14 ORDER BY sort_order ASC";
                            $result_status = $mysqli->query($query_status);
                            echo '<select id="status_update" name="status_update">';
                            while ($obj_s = $result_status->fetch_object()) {
                                $belongs_to = explode(',', $obj_s->belongs_to);
                                if (in_array($obj->request_type_id, $belongs_to)) {
                                    echo '<option value="'.$obj_s->id.'"';
                                    if ($obj->status == $obj_s->id) {
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$obj_s->value.'</option>';
                                }
                            }
                            echo '</select>';
                        } else {
                            echo '<input type="hidden" id="status_update" name="status_update" value="'.$obj->status.'">'; //For new Edit
                            echo $obj->status_value;
                        }
                        echo '</div></div>';
                        echo '<div class="row"><label>Assigned To</label> <div class="pull-left">';
                        //Admin can change status
                        if ($_SESSION['admin'] == 1) {
                            echo getAssignedTo($obj->assigned_to, 1);
                        } else {
                            echo '<input type="hidden" id="assigned_to" name="assigned_to" value="'.$obj->assigned_to.'">'; //For new Edit
                            echo getAssignedTo($obj->assigned_to, 0);
                        }
                        echo '</div></div>';


                        ////////////////////////////////////////////////////////////////////////////////////////
                        //START CAMPAIGN/NO-CAMPAIGN LOGIC
                        if ($obj->request_type_id != 8) {

                            // Request details box
                            echo '<h1><br />'.$obj->request_type.' Details</h1>';

                            //Now show the details of the particular type of request
                            echo getRequestDetails($obj->id, $obj->request_type_id, $edit, $owner);

                            if(in_array($obj->request_type_id,array(1,2,5,6,7))) {
                                // additional recipients and additional owners of request
                                if(!$edit) {  // if view mode
                                    $additional_username = $additional_username_owner = array();

                                    echo '<div class="row row-border"><label>Additional Notified Recipients</label>';
                                    if(!empty($obj->additional_recipients)) {  // if additional recipients are not empty
                                        $add_recipients = unserialize($obj->additional_recipients);

                                        if(!empty($add_recipients) && is_array($add_recipients)) { 

                                            foreach ($add_recipients as $user_id) {
                                                $additional_username[] = getUserName($user_id); // get name against id
                                            }

                                            if(!empty($additional_username)) { // if not empty username
                                                echo '<div class="pull-left">';
                                                echo implode(', ',$additional_username);  
                                                echo '</div>';
                                            }
                                              
                                        }
                                      
                                    }
                                    echo '</div>';  

                                    echo '<div class="row row-border"><label>Additional Users That Can View/Edit</label>';                                    
                                    if(!empty($obj->additional_owners)) { // if additional owners are not empty
                                        $add_owners = unserialize($obj->additional_owners);

                                        if(!empty($add_owners) && is_array($add_owners)) {

                                            foreach ($add_owners as $user_id) {
                                                $additional_username_owner[] = getUserName($user_id);  // get name against id
                                            }

                                            if(!empty($additional_username_owner)) { // if not empty username
                                                echo '<div class="pull-left">';
                                                echo implode(', ',$additional_username_owner);   
                                                echo '</div>';
                                            }
                                        }
                                    }
                                    echo '</div>';  
                                }
                            }

                            //Contact lists
                            if ($obj->request_type_id != 4 && $obj->request_type_id != 9) {
                                $query_contactlists = "SELECT
                                                        rcl.id,
                                                        rcl.request_id,
                                                        rcl.name
                                                    FROM request_contactlist rcl
                                                    WHERE rcl.associated_tactic = '".$obj->id."'
                                                    AND rcl.active = 1
                                                    ORDER BY rcl.id DESC";
                                $result_contactlists = $mysqli->query($query_contactlists);
                                //print_r($query_contactlists);
                                $row_cnt_contactlists = $result_contactlists->num_rows;
                                if ($row_cnt_contactlists > 0) {
                                    echo '<div class="row"><label>Contact List</label> <div class="pull-left" style="width:680px;"><ul class="notes">';
                                    while ($obj_cl = $result_contactlists->fetch_object()) {
                                        echo '<li><a href="request-detail?id='.$obj_cl->request_id.'" target="_blank">'.$obj_cl->name.' <i class="icon icon-share" style="margin-top:3px;"></i></a></li>';
                                    }
                                    echo '</ul></div></div>';
                                }
                            }


                            //Notes
                            //Grab notes
                            $notes = '<ul class="notes">';
                            $query_notes = "SELECT user_id, notes, col, DATE_FORMAT(date_created, '%m/%d/%Y %l:%i%p') AS date_created_notes FROM notes WHERE request_id = '".$obj->id."' AND active = 1 ORDER BY date_created DESC";
                            $result_notes = $mysqli->query($query_notes);
                            $row_cnt_notes = $result_notes->num_rows;
                            if ($row_cnt_notes > 0) {
                                while ($obj_n = $result_notes->fetch_object()) {
                                    $col_clean = str_replace('_', ' ', $obj_n->col);
                                    if ($obj_n->notes == 'Value updated') {
                                        $notes.='<li>'.ucwords($col_clean).' Updated | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                    } else {
                                        $notes.='<li>'.nl2br($obj_n->notes).' | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                    }
                                }
                            } else {
                                $notes.='<li>No notes</li>';
                            }
                            $notes.='</ul>';
                            echo '<div class="row"><label>Notes</label> <div class="pull-left" style="width:680px;">'.$notes;
                            //Admin can add notes
                            if ($_SESSION['admin'] == 1 && $edit == true) {
                                echo '<a href="#" id="notes" class="editable" data-type="textarea" data-pk="'.$_GET['id'].'" data-url="/includes/_requests_update_inline.php?request_type_id='.$obj->request_type_id.'&type=notes">Add a note</a>';
                                //echo '<br /><textarea id="notes_update" name="notes_update" rows="3" cols="45" class="input-full" style="width:50%;"></textarea>';
                            }
                            echo '</div></div>';

                            //Grab speakers
                            if ($obj->request_type_id == 1) {
                                $speakers = '';
                                $query_speakers = "SELECT * FROM speakers WHERE request_id = '".$_GET['id']."' AND active = 1 ORDER BY id ASC";
                                $result_speakers = $mysqli->query($query_speakers);
                                $row_cnt_speakers = $result_speakers->num_rows;
                                $speaker_cnt = 1;
                                if ($row_cnt_speakers > 0) {
                                    echo '<h1><br />Speakers</h1>';

                                    while ($obj_speakers = $result_speakers->fetch_object()) {
                                        if ($edit == 1) {
                                            $speakers.='<input type="hidden" id="speaker_id_'.$speaker_cnt.'" name="speaker_id_'.$speaker_cnt.'" value="'.$obj_speakers->id.'">
                                                            <input type="text" id="speaker_name_'.$speaker_cnt.'" name="speaker_name_'.$speaker_cnt.'" class="input-wide required" placeholder="Name (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_name.'"><br />
                                                            <input type="text" id="speaker_email_'.$speaker_cnt.'" name="speaker_email_'.$speaker_cnt.'" class="input-wide" placeholder="Email" value="'.$obj_speakers->speaker_email.'"><br />
                                                            <label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$speaker_cnt.'[]" name="speaker_type_'.$speaker_cnt.'[]" value="Speaker"'; if (strpos($obj_speakers->speaker_type, 'Speaker') !== false) { $speakers.='checked'; } $speakers.='> Speaker</label>
                                                            <label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$speaker_cnt.'[]" name="speaker_type_'.$speaker_cnt.'[]" value="Moderator"'; if (strpos($obj_speakers->speaker_type, 'Moderator') !== false) { $speakers.='checked'; } $speakers.='> Moderator</label>
                                                            <div class="bios-extra" style="display:block;">
                                                                <input type="text" id="speaker_title_'.$speaker_cnt.'" name="speaker_title_'.$speaker_cnt.'" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_title.'"><br />
                                                                <input type="text" id="speaker_company_'.$speaker_cnt.'" name="speaker_company_'.$speaker_cnt.'" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_company.'"><br />
                                                                <textarea id="speaker_bio_'.$speaker_cnt.'" name="speaker_bio_'.$speaker_cnt.'" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048">'.$obj_speakers->speaker_bio.'</textarea><br />
                                                                <div class="upload-div">
                                                                    <div id="photo_'.$speaker_cnt.'" class="fileupload fileupload-new" data-provides="fileupload">
                                                                        <span class="btn-file btn-green add"><span class="fileupload-new">Add/Replace Photo</span><input id="fileupload_photo_'.$speaker_cnt.'" class="fileupload_photo" data-number="1" type="file" name="files[]"></span>
                                                                    </div>
                                                                    <div id="progress-photo_'.$speaker_cnt.'" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 160px;width:290px;display:none;">
                                                                        <div class="bar"></div>
                                                                    </div>
                                                                    <div id="shown-photo_'.$speaker_cnt.'" class="files" style="clear:both;"> </div>
                                                                </div>
                                                            </div><label class="checkbox inline" style="color:#cc0000;"> <input type="checkbox" id="speaker_delete_'.$speaker_cnt.'" name="speaker_delete_'.$speaker_cnt.'" value="1"> Remove Speaker?</label><br /><hr>';
                                            $speaker_cnt++;
                                        } else {
                                            $speakers.='<p class="speaker-list"><strong>'.$obj_speakers->speaker_name.'</strong>';
                                            if ($obj_speakers->speaker_type != '') {
                                                $speakers.=' (<em>'.$obj_speakers->speaker_type.'</em>)';
                                            }
                                            if ($obj_speakers->speaker_title != '') {
                                                $speakers.='<br />'.$obj_speakers->speaker_title;
                                            }
                                            if ($obj_speakers->speaker_company != '') {
                                                $speakers.='<br />'.$obj_speakers->speaker_company;
                                            }
                                            if ($obj_speakers->speaker_email != '') {
                                                $speakers.='<br /><a href="mailto:'.$obj_speakers->speaker_email.'">'.$obj_speakers->speaker_email.'</a>';
                                            }
                                            if ($obj_speakers->speaker_bio != '') {
                                                $speakers.='<br /><br />'.$obj_speakers->speaker_bio;
                                            }
                                            if (strlen($obj_speakers->speaker_photo) > 22) {
                                                $speakers.='<br /><br /><img src="'.$obj_speakers->speaker_photo.'">';
                                            }
                                            $speakers.='</p><hr>';
                                        }
                                    }

                                    echo '<div class="row row-border"><label>&nbsp;</label> <div class="pull-left" style="width:680px;">'.$speakers.'</div></div>';
                                }
                            }

                            /////////////////////////////////////////////////////////////////////////////
                            //TEMPLATES
                            //Show template blocks, only for Webinar, Email (6) and Landing Page (7)
                            /*NOT in this view
                            if ($obj->request_type_id == 1 || $obj->request_type_id == 6 || $obj->request_type_id == 7) {


                                //Have to change the template query based on type
                                if ($obj->request_type_id == 1) { //Webinar  //
                                    $query_template  = "SELECT * FROM templates WHERE request_id = '".$obj->id."' ORDER BY date_created DESC LIMIT 1";
                                    $result_template = $mysqli->query($query_template);
                                    $result_template = $result_template->fetch_object();

                                    if($result_template){

                                        echo '<h1><br />Template and Layout Configuration</h1>';
                                        echo '<div class="row row-border"><label>Template Type</label><div class="pull-left">';
                                        //echo  $result_template->template_type . ' template ';
                                        //if($edit)
                                        if(strtolower($result_template->template_type) === 'custom'){
                                            echo '<a href="templates-custom?type=webinar&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                        }else{
                                            echo '<a href="templates-default?type=webinar&edit=1&req_id=' . $obj->id .  '" id="request-detail-edit-template" class="btn-green" style="">Launch Asset Editor</a>';
                                        }
                                        echo '</div></div>';
                                    }
                                    //TODO remove template block for everything : Landing page and email

                                }

                            } //End tactic check for templates
                            */

                            /////////////////////////////////////////////////////////////////////////////
                            //TEMPLATES


                            //Now show the assets from the request
                            echo '<h1><br />Request Assets</h1>';
                            echo getRequestAssets($obj->id, $obj->request_type_id, $edit, $owner, 1);

                        

                        //CAMPAIGN/NO-CAMPAIGN LOGIC
                        ///////////////////////////////////////////////////////////////////
                        } else {

                            //And then show the campaign details
                            //We're doing this inline instead of an include. Getting too crowded in there.
                            $query_camps = "SELECT
                                        req.*,
                                        DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                                        DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                                        status.value AS status_value,
                                        req.request_type AS request_type_id,
                                        request_type.request_type AS request_type,
                                        users.fname AS fname,
                                        users.lname AS lname,
                                        users.email AS email,
                                        request_campaign.id AS campaign_id
                                    FROM request AS req
                                    INNER JOIN status on req.status = status.id
                                    INNER JOIN request_type on req.request_type = request_type.id
                                    INNER JOIN users on req.user_id = users.id
                                    INNER JOIN request_campaign on req.id = request_campaign.request_id
                                    WHERE req.active = 1
                                    AND req.id = '".$_GET['id']."' LIMIT 1";
                            
                            $result_camps = $mysqli->query($query_camps);
                            $row_cnt_camps = $result_camps->num_rows;

                            if ($row_cnt_camps > 0) {
                    
                                while ($obj_camps = $result_camps->fetch_object()) {
                                    //We need to know if this person owns this
                                    if ($_SESSION['admin'] == 1 || $obj_camps->user_id == $_SESSION['user_id']) { $owner = true; } else { $owner = false; }

                                    echo '<h1><br />Campaign Details</h1>';

                                    //Now show the details of the particular type of request
                                    echo getRequestDetails($obj_camps->id, $obj_camps->request_type_id, $edit, $owner);

                                    //Notes
                                    //Grab notes
                                    $notes = '<ul class="notes">';
                                    $query_notes = "SELECT user_id, notes, col, DATE_FORMAT(date_created, '%m/%d/%Y %l:%i%p') AS date_created_notes FROM notes WHERE request_id = '".$obj_camps->id."' AND active = 1 ORDER BY date_created DESC";
                                    $result_notes = $mysqli->query($query_notes);
                                    $row_cnt_notes = $result_notes->num_rows;
                                    if ($row_cnt_notes > 0) {
                                        while ($obj_n = $result_notes->fetch_object()) {
                                            $col_clean = str_replace('_', ' ', $obj_n->col);
                                            if ($obj_n->notes == 'Value updated') {
                                                $notes.='<li>'.ucwords($col_clean).' Updated | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                            } else {
                                                $notes.='<li>'.nl2br($obj_n->notes).' | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                            }
                                        }
                                    } else {
                                        $notes.='<li>No notes</li>';
                                    }
                                    $notes.='</ul>';
                                    echo '<div class="row"><label>Notes</label> <div class="pull-left" style="width:680px;">'.$notes;
                                    //Admin can add notes
                                    if ($_SESSION['admin'] == 1 && $edit == true) {
                                        echo '<a href="#" id="notes" class="editable" data-type="textarea" data-pk="'.$_GET['id'].'" data-url="/includes/_requests_update_inline.php?request_type_id='.$obj_camps->request_type_id.'&type=notes">Add a note</a>';
                                    }
                                    echo '</div></div>';

                                    //Create this
                                    $campaign_id = $obj_camps->campaign_id;
                                }

                                //Associated tactics
                                /*
                                echo '<h1><br />Associated Campaign Tactics</h1>';

                                $query = "SELECT
                                        req.*,
                                        DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                                        DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                                        status.value AS status_value,
                                        request_type.request_type AS request_type,
                                        request_type.id AS request_type_id,
                                        users.fname AS fname,
                                        users.lname AS lname,
                                        users.email AS email
                                    FROM request AS req
                                    INNER JOIN status on req.status = status.id
                                    INNER JOIN request_type on req.request_type = request_type.id
                                    INNER JOIN users on req.user_id = users.id
                                    WHERE req.request_type != 8
                                    AND req.campaign_id = '".$campaign_id."'
                                    AND req.active = 1
                                    ORDER BY req.id DESC";
                                
                                $result = $mysqli->query($query);
                                $row_cnt = $result->num_rows;

                                if ($row_cnt > 0) {
                                    echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table" style="padding-top:20px;">';
                                    echo '<thead><th style="width:114px;">Created <b class="icon-white"></b></th><th>Tactic Name <b class="icon-white"></b></th><th>Owner <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Marketing ID <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th></thead>';
                                    echo '<tbody>';
                                    while ($obj = $result->fetch_object()) {
                                        $marketing_id = getMarketingId($obj->id, $obj->request_type_id);

                                        if ($marketing_id != '') {
                                            $marketing_id = $marketing_id;
                                        } else {
                                            $marketing_id = '--';
                                        }

                                        echo '<tr>';
                                        echo '<td>'.$obj->date_created.'</td>';
                                        echo '<td><a href="tactic-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
                                        echo '<td>'.$obj->fname.' '.$obj->lname.'</td>';
                                        echo '<td>'.$obj->request_type.'</td>';
                                        echo '<td>'.$marketing_id.'</td>';
                                        echo '<td style="white-space:nowrap;"><div style="display:none;">'.$obj->status_value.'</div>'.$obj->status_value.'</td>';
                                        echo '</tr>';
                                    }
                                    echo '</tbody></table><p>&nbsp;</p>';
                                } else {
                                    echo '<p><strong>There are currently no active tactics associated with this campaign.</strong></p>';
                                }
                                */
                            }

                        }
                        //END IF CAMPAIGN OR NOT
                        ////////////////////////////////////////////////////////////////////


                        //Message Box
                        echo '<a name="messages"></a><h1><br />Messages</h1>';

                        echo '<div class="row row-border even"><label></label> <div class="pull-left" style="width:100%;">';

                        //Fetch messages
                        // $query_messages = "SELECT messages.*,users.fname,users.lname FROM messages INNER JOIN on messages.user_id = users.id WHERE request_id = " .  $obj->id . " ORDER BY date_created";
                        $query_messages = "SELECT messages.body, messages.date_created, users.fname,users.lname,users.id FROM messages INNER JOIN users ON messages.user_id = users.id where messages.request_id = " . $_GET['id'] . " ORDER BY messages.date_created DESC";
                        echo '<div class="message-thread" style="margin-left:210px;">';
                        $result_messages = $mysqli->query($query_messages);
                        while ($obj_msg = $result_messages->fetch_object()) {
                            $attrvalue = $obj_msg->date_created;
                            date_default_timezone_set('America/Los_Angeles');
                            $attrvalue =  date('m/d/Y h:iA',strtotime("$attrvalue UTC"));
                            echo '<ul class="notes"><li>' . $obj_msg->body . '<br /><span class="user-name">'.$attrvalue.' ('.$obj_msg->fname.' '.$obj_msg->lname.')</span></li></ul><hr>';
                        }

                        echo '</div>';

                        //Message Body
                        echo '<div class="send-message">';
                        echo '<div class="control-group">';
                        echo '<div class="controls">';
                        echo '<label for="body">Send Message</label>';
                        echo '<textarea id="message-body" name="body" rows="6"  style="width:74%;" class="input-wide textarea-wide" placeholder="Send message about request"></textarea>';
                        echo '</div></div>';

                        ////recipients
                        $query_users = "SELECT id, fname, lname FROM users WHERE active = 1 ORDER BY fname ASC";
                        $result_users = $mysqli->query($query_users);

                        echo '<div class="control-group">';
                        echo '<label for="message-recipients">Recipients</label>';
                        echo '<select id="message-recipients" name="message-recipients" multiple style="width:50%;border:none !important;">';

                        $cc_list = array();
                        $cc_str = '';
                        while ($obj_users = $result_users->fetch_object()) {
                            if($obj_users->id !==  $obj->assigned_to && $obj_users->id !== $obj->user_id){
                                echo '<option value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                            }else{
                                array_push($cc_list, $obj_users->id);
                                $cc_str .=  $obj_users->fname . ' ' . $obj_users->lname . ', ';
                            }
                        }

                        $cc_str = substr($cc_str, 0, strlen($cc_str) -2);
                        echo '</select>';
                        echo '</div>';

                        echo '<div class="control-group">';
                        echo '<label for="message-cc">CC</label>';
                        echo '<p id="message-cc" data-list=\''. json_encode($cc_list) . '\'>' . $cc_str . '</p>';
                        echo '</div><br/>';

                        echo '<a href="#" id="request-detail-send-message" class="btn-green" style="margin-left: 210px;">Send Message</a>';
                        echo '</div>'; //End messages

                        echo '</div></div>';
                        // END Message Box


                        echo '</div>'; //End single-request


                    }
                }


                if (($_SESSION['admin'] == 1 || $edit == 1) && ($owner == true)) {
                    echo '</p></form></div>';
                } else {
                    echo '</p></div>';
                }


                //Message params form
                echo '<form id="request-detail-message">';
                ?>

            </div>
        </div>

        <?php
        if ($row_cnt > 0) {
            if (($_SESSION['admin'] == 1 || $owner == true)) {
                echo '
                    <div class="header-row">
                    <div class="spamm12" style="margin: 0 auto;">
                      <header>
                        <div class="header-right">';
                            if ($edit == false) {
                                echo '<a href="request-detail?id='.$_GET['id'].'&edit=1" class="btn-green plain" style="margin-right:6px;">Edit</a>';
                            } else {
                                //echo '<a href="request-detail?id='.$_GET['id'].'" class="btn-green plain finish-edit" style="margin-right:6px;">Finish Edits</a>';
                            }
                          if ($_SESSION['admin'] == 1 || $edit == true) { echo '<a href="#" id="submit-request-form-inline" class="btn-green submit">Save Changes</a>'; }
                        echo '</div>
                      </header>
                    </div>
                  </div>'; //Something to play around with
            }
        }
        ?>

    </div>

<?php include('includes/footer.php'); ?>
