<?php
$loc = 'training';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

$single = false;
$query = "SELECT *, DATE_FORMAT( date,  '%m/%d/%Y' ) AS date_cleaned FROM trainings WHERE active = 1 AND date >= CURDATE()";

if (isset($_GET['eid'])) {
	$single = true;
	$query.=" AND id = '".$_GET['eid']."'";
}
$query.=" ORDER BY date ASC";

$result = $mysqli->query($query);
$row_cnt = $result->num_rows;
?>

	<?php if ($single == false) { ?>
	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
                <h1>View All Trainings</h1>
            </div>
        </div>
    </div>
    <?php } ?>
	
	<?php
	//SINGLE RECORD
	if ($single && $row_cnt > 0) {
        while ($obj = $result->fetch_object()) {
    ?>
	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
                <h1><?php echo $obj->title; ?></h1>
            </div>
        </div>
    </div>

    <div class="container">
    	<div class="row">
    		<div id="zebra" class="single-request">
		        <?php
		        if ($obj->description != '') { echo '<div class="row"><label>Session Link</label> <div class="pull-left"><a href="'.$obj->description.'" target="_blank">'.$obj->description.'</a></div></div>'; }
		        echo '<div class="row"><label>Date</label> <div class="pull-left">'.$obj->date_cleaned.'</div></div>';
		        echo '<div class="row"><label>Time</label> <div class="pull-left">'.$obj->time.'</div></div>';
		        echo '<div class="row"><label>Bridge</label> <div class="pull-left">'.$obj->bridge.'</div></div>';
		        echo '<div class="row"><label>Organizer</label> <div class="pull-left">'.$obj->organizer.'';
		        if ($obj->organizer_phone != '') {
		        	echo '<br />'.$obj->organizer_phone;
		        }
		        if ($obj->organizer_email != '') {
		        	echo '<br /><br /><a href="mailto:'.$obj->organizer_email.'" class="btn-green">Contact Organizer</a>';
		        }
		        echo '</div></div>';
				?>
				<p style="margin-top:20px;"><a href="training" class="btn-green pull-right">View All</a></p>
			</div>
        </div>
    </div>

    <?php
    	}
    } else {
    //ALL RECORDS
    ?>

    <div class="container">
    	<div class="row">
    		<div id="zebra" class="single-request">
		        <?php
		        $cnt = 1;
		        for ($i = 0; $obj = $result->fetch_object(); ++$i) {
					echo '<div class="row"><label>Title</label> <div class="pull-left">'.$obj->title.'</div></div>';
		        	if ($obj->description != '') { echo '<div class="row"><label>Session Link</label> <div class="pull-left"><a href="'.$obj->description.'" target="_blank">'.$obj->description.'</a></div></div>'; }
		        	echo '<div class="row"><label>Date</label> <div class="pull-left">'.$obj->date_cleaned.'</div></div>';
		        	echo '<div class="row"><label></label> <div class="pull-left" style="width:400px;margin-left: 208px;margin-bottom: 16px;"><a href="training?eid='.$obj->id.'" class="btn-green">Learn More</a></div></div>';
				}
		        ?>
		    </div>
        </div>
    </div>
    <?php
	}
	?>

<?php include('includes/footer.php'); ?>