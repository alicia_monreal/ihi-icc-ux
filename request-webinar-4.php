<?php
$loc = 'campaign-requests';
$step = 4;
$c_type = 'webinar';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Webinar Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fiveup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li>Step 3</li>
				<li class="active">Step 4</li>
				<li>Step 5</li>
			</ul>
			<div class="request-step step3">
				<div class="fieldgroup">
					<p>Select options for your event.<br /><span style="font-weight:normal;">The Presentation, Q &amp; A, Media Upload, and Speakers &amp; Moderators modules are required modules.<br />However it is optional to include videos for the Media Upload as well as information for the Speakers &amp; Moderators.<br />It is highly recommended to at least provide a name for the Speakers so the audience knows who is presenting.</span></p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p>
					<h3 class="fieldgroup-header">Required Widgets</h3>
				</div>

				<form id="content-update-form" class="form-horizontal" action="includes/_requests_webinar.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="4">
					<input type="hidden" id="c_type" name="c_type" value="webinar">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="in_progress_form" name="in_progress_form" value="0">

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p><input type="checkbox" id="presentation" name="presentation" value="1" disabled="disabled" class="hide" checked></p>
								<p><img src="img/icons/presentation.png" alt="Presentation" style="margin-top:0;"></p>
							</div>
							<div class="cr-description pull-left">
								<h4>Presentation <a href="img/webinar-presentation.jpg" class="cbox-image" title="Presentation"><i class="helper-icon icon-picture"></i></a></h4>
								<p>Upload the PowerPoint for your presentation. This file should contain the main content for your webinar, and will be displayed to your audience along with polls, surveys, Flash videos, and any other content types you choose below.</p><p>Upload one .PPTX file, up to 25 MB.</p>
								<div class="upload-div">
								    <div id="presentation" class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add"><span class="fileupload-new"><?php if ($_SESSION['files_presentation']) { echo 'Change File'; } else { echo 'Add Presentation'; } ?></span><input id="fileupload_presentation" type="file" name="files[]"></span>
									</div>
									<div id="progress-presentation" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 190px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="errors-presentation" class="files" style="clear:both;color:#cc0000;"></div>
								    <div id="shown-presentation" class="files" style="clear:both;">
								    	<?php

								    	if ($_SESSION['files_presentation']) {
								    		$filebox = '<div name="'. ($_SESSION['files_presentation']). '">';
								    		$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_presentation']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_presentation_path'].'">' . $_SESSION['files_presentation'] . '</a></p>';
						                    $filebox.= '</div>';
						                    echo $filebox;
								    		// echo '<p>'.$_SESSION['files_presentation'].'</p>';
            							}
            							?>
								    </div>
								</div>
							</div>
							<div class="cr-screenshot pull-left"><p><img src="img/screenshots/presentation.jpg" alt="Presentation"></p></div>
						</div>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p><input type="checkbox" id="qa" name="qa" value="1" disabled="disabled" class="hide" checked></p>
								<p><img src="img/icons/qa.png" alt="Q&amp;A" style="margin-top:0;"></p>
							</div>
							<div class="cr-description pull-left">
								<h4>Q &amp; A <a href="img/webinar-qanda.jpg" class="cbox-image" title="Q&amp;A"><i class="helper-icon icon-picture"></i></a></h4>
								<p>The Q &amp; A widget allows your attendees to submit questions - at any time - during live or on-demand webcasts.</p>
							</div>
							<div class="cr-screenshot pull-left"><p><img src="img/screenshots/qa.jpg" alt="Q&amp;A"></p></div>
						</div>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p><input type="checkbox" id="media_player" name="media_player" value="1" disabled="disabled" class="hide" checked></p>
								<p><img src="img/icons/media-player.png" alt="Media Player" style="margin-top:0;"></p>
							</div>
							<div class="cr-description pull-left">
								<h4>Media Upload</h4>
								<p>Select this option to upload audio and video files to play during your webinar. For archived events, this widget allows attendees to pause, fast-forward, or rewind the webcast presentation. Video files will play at the same aspect ratio as your slides so please format your files accordingly. (For 16:9, recommended dimensions are 640x360, 480x270 and 320x180.)</p><p>Upload up to 20 of your FLV or MP4 files up to 25MB each.</p>
								<div class="upload-div">
								    <div id="media" class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add"><span class="fileupload-new">Add Media</span><input id="fileupload_media" type="file" name="files[]" multiple></span>
									</div>
									<div id="progress-media" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 175px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="errors-media" class="files" style="clear:both;color:#cc0000;"></div>
								    <div id="shown-media" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_media'])) {
								    		$i=0;
								    		foreach ($_SESSION['files_media'] as $val_media) {
								    			$filebox = '<div name="'. ($val_media). '">';
								    			$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($val_media) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_media_path'][$i].'">' . $val_media . '</a></p>';
							                    $filebox.= '</div>';
							                    echo $filebox;
								    			// echo '<p>'.$val_media.'</p>';
								    			//echo '<input type="hidden" name="files_media[]" value="'.$val_media.'" />';
								    			$i++;
								    		}
            							}
            							?>
								    </div>
								</div>
							</div>
							<div class="cr-screenshot pull-left"><p><img src="img/screenshots/media-player.jpg" alt="Media Player"></p></div>
						</div>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p><input type="checkbox" id="speakers" name="speakers" value="1" disabled="disabled" class="hide" checked></p>
								<p><img src="img/icons/speakers.png" alt="Speaker(s)" style="margin-top:0;"></p>
							</div>
							<div class="cr-description pull-left" style="width:800px;">
								<h4>Speakers &amp; Moderators <a href="img/webinar-bio.jpg" class="cbox-image" title="Speaker Bio"><i class="helper-icon icon-picture"></i></a></h4>
								<p>This option allows you to include introductions for your presenters within your webinar. You must include the name for at least one speaker. Bios, photos, and additional speaker introductions are optional. Speaker info will be shared with the audience to provide them with contact information for follow up questions.  Should your speakers wish to not be potentially contacted, do not provide email information.</p><p>Recommended photo size is 80px wide by 95px high.</p>
								<p><label class="radio inline"> <input type="radio" id="speaker_option[]" name="speaker_option" value="Simple" <?php if (!isset($_SESSION['speaker_option']) || $_SESSION['speaker_option'] == 'Simple') { echo 'checked'; } ?>> Simple Display - shows name and email only</label><br />
								<label class="radio inline"> <input type="radio" id="speaker_option[]" name="speaker_option" value="Detailed" <?php if ($_SESSION['speaker_option'] == 'Detailed') { echo 'checked'; } ?>> Detailed Display - shows name, email, title, company, speaker bio and image (optional)</label></p>

								<div id="bios" class="speaker-fields">
									<div class="speaker speaker_1">
										<label for="speaker_name">Speaker/Moderator 1</label>
										<input type="text" id="speaker_name_1" name="speaker_name_1" class="input-wide" placeholder="Name (120 characters max)" maxlength="120" value="<?php echo $_SESSION['speaker_name_1']; ?>"><br />
										<input type="text" id="speaker_email_1" name="speaker_email_1" class="input-wide" placeholder="Email" value="<?php echo $_SESSION['speaker_email_1']; ?>"><br />
										<label class="checkbox inline"> <input type="checkbox" id="speaker_type_1[]" name="speaker_type_1[]" value="Speaker" <?php if (in_array('Speaker', $_SESSION['speaker_type_1'])) { echo 'checked'; } ?>> Speaker</label>
										<label class="checkbox inline"> <input type="checkbox" id="speaker_type_1[]" name="speaker_type_1[]" value="Moderator" <?php if (in_array('Moderator', $_SESSION['speaker_type_1'])) { echo 'checked'; } ?>> Moderator</label>
										<div class="bios-extra"<?php if ($_SESSION['speaker_option'] == 'Detailed') { echo ' style="display:block;"'; } ?>>
											<input type="text" id="speaker_title_1" name="speaker_title_1" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value="<?php echo $_SESSION['speaker_title_1']; ?>"><br />
											<input type="text" id="speaker_company_1" name="speaker_company_1" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value="<?php echo $_SESSION['speaker_company_1']; ?>"><br />
											<textarea id="speaker_bio_1" name="speaker_bio_1" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048"><?php echo $_SESSION['speaker_bio_1']; ?></textarea><br />
											<div class="upload-div">
											    <div id="photo_1" class="fileupload fileupload-new" data-provides="fileupload">
													<span class="btn-file btn-green add"><span class="fileupload-new"><?php if ($_SESSION['files_photo_1']) { echo 'Change File'; } else { echo 'Add Photo'; } ?></span><input id="fileupload_photo_1" class="fileupload_photo" data-number="1" type="file" name="files[]"></span>
												</div>
												<div id="progress-photo_1" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 190px;display:none;">
											        <div class="bar"></div>
											    </div>
											    <div id="shown-photo_1" class="files" style="clear:both;">
											    	<?php

											    	if ($_SESSION['files_photo_1']) {
											    		$filebox = '<div name="'. ($_SESSION['files_photo_1']). '">';
							    						$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_photo_1']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="/server/php/files/'.$_SESSION['user_id'].'/'.$_SESSION['files_photo_1'].'">' . $_SESSION['files_photo_1'] . '</a></p>';
						                    			$filebox.= '</div>';
						                    			echo $filebox;
											    		// echo '<p>'.$_SESSION['files_photo_1'].'</p>';
			            							}
			            							?>
											    </div>
											</div>
										</div>
									</div>

									<?php for ($i = 2; $i <= 10; $i++) {
										echo '
										<div class="speaker speaker_'.$i.'"'; if ($_SESSION['speaker_name_'.$i]) { echo ' style="display:block;"'; } echo '>
											<label for="speaker_name_'.$i.'">Speaker/Moderator '.$i.'</label>
											<input type="text" id="speaker_name_'.$i.'" name="speaker_name_'.$i.'" class="input-wide" placeholder="Name (120 characters max)" maxlength="120" value="'.$_SESSION['speaker_name_'.$i].'"><br />
											<input type="text" id="speaker_email_'.$i.'" name="speaker_email_'.$i.'" class="input-wide" placeholder="Email" value="'.$_SESSION['speaker_email_'.$i].'"><br />
											<label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$i.'[]" name="speaker_type_'.$i.'[]" value="Speaker"'; if (in_array('Speaker', $_SESSION['speaker_type_'.$i.''])) { echo 'checked'; } echo '> Speaker</label>
											<label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$i.'[]" name="speaker_type_'.$i.'[]" value="Moderator"'; if (in_array('Moderator', $_SESSION['speaker_type_'.$i.''])) { echo 'checked'; } echo '> Moderator</label>
											<div class="bios-extra"'; if ($_SESSION['speaker_option'] == 'Detailed') { echo ' style="display:block;"'; } echo '>
												<input type="text" id="speaker_title_'.$i.'" name="speaker_title_'.$i.'" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value="'.$_SESSION['speaker_title_'.$i].'"><br />
												<input type="text" id="speaker_company_'.$i.'" name="speaker_company_'.$i.'" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value="'.$_SESSION['speaker_company_'.$i].'"><br />
												<textarea id="speaker_bio_'.$i.'" name="speaker_bio_'.$i.'" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048">'.$_SESSION['speaker_bio_'.$i].'</textarea><br />
												<div class="upload-div">
												    <div id="photo_'.$i.'" class="fileupload fileupload-new" data-provides="fileupload">
														<span class="btn-file btn-green add"><span class="fileupload-new">'; if ($_SESSION['files_photo_'.$i]) { echo 'Change File'; } else { echo 'Add Photo'; } echo '</span><input id="fileupload_photo_'.$i.'" class="fileupload_photo" data-number="'.$i.'" type="file" name="files[]"></span>
													</div>
													<div id="progress-photo_'.$i.'" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 190px;display:none;">
												        <div class="bar"></div>
												    </div>
												    <div id="shown-photo_'.$i.'" class="files" style="clear:both;">';
												    	if ($_SESSION['files_photo_'.$i]) {
												    		// echo '<p>'.$_SESSION['files_photo_'.$i].'</p>';
												    		$filebox = '<div name="'. ($_SESSION['files_photo_'.$i]). '">';
								    						$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_photo_'.$i]) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="/server/php/files/'.$_SESSION['user_id'].'/'.$_SESSION['files_photo_'.$i].'">' . $_SESSION['files_photo_'.$i] . '</a></p>';
							                    			$filebox.= '</div>';
							                    			echo $filebox;
				            							}
													echo '
												    </div>
												</div>
											</div>
										</div>';
									}
									?>
								</div>
								<p style="padding-top:20px;"><a href="" id="addSpeaker" class="btn-green add pull-left">Add a Speaker/Moderator</a><div class="pull-left small" style="margin-left:26px;margin-top:-4px;">You may add up to 10 speakers or moderators</div></p>
								<div class="upload-div" style="clear:both;display:none;">
								    <div class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add"><span class="fileupload-new">Add Photo</span><span class="fileupload-exists">Change</span><input id="fileupload" type="file" name="files[]" multiple></span>
									</div>
									<div id="progress-files" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 108px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="files" class="files" style="clear:both;"></div>
								</div>
								<div class="small" style="clear:both;display:none;"><p>Required Dimensions: 80x95 | Formats: .jpg, .jpeg, .png, or .gif<br />A larger image will be adjusted to this size to fit the photo area<br />when added. For the best picture quality we advice you to resize<br />the photo prior to uploading.</p></div>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<h3 class="fieldgroup-header">Optional Widgets</h3>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p>Enable</p>
								<p><input type="checkbox" id="url" name="url" value="1" <?php if (getFieldValue('url', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 1) { echo 'checked'; } ?>></p>
								<p><img src="img/icons/url.png" alt="URL"></p>
							</div>
							<div class="cr-description pull-left" style="width:800px;">
								<h4>URL</h4>
								<p>Use of the URL widget will open a new browser window to the URL of your choosing.</p>
								<p><input type="text" id="url_value" name="url_value" class="input-wide" placeholder="Enter URL" value="<?php echo getFieldValue('url_value', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>"></p>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p>Enable</p>
								<p><input type="checkbox" id="surveycheck" name="surveycheck" value="1" <?php if (getFieldValue('surveycheck', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 1) { echo 'checked'; } ?>></p>
								<p><img src="img/icons/survey.png" alt="Survey"></p>
							</div>
							<div class="cr-description pull-left">
								<h4>Survey <a href="img/webinar-survey.jpg" class="cbox-image" title="Survey"><i class="helper-icon icon-picture"></i></a></h4>
								<p>This option allows you to gather feedback in real time from webinar attendees.</p>
								<div class="upload-div">
								    <div id="survey" class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add"><span class="fileupload-new"><?php if ($_SESSION['files_survey']) { echo 'Change File'; } else { echo 'Add Survey File'; } ?></span><input id="fileupload_survey" type="file" name="files[]"></span>
									</div>
									<div id="progress-survey" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 179px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-survey" class="files" style="clear:both;">
								    	<?php
								    	if ($_SESSION['files_survey']) {
								    		$filebox = '<div name="'. ($_SESSION['files_survey']). '">';
								    		$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_survey']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_survey_path'].'">' . $_SESSION['files_survey'] . '</a></p>';
						                    $filebox.= '</div>';
						                    echo $filebox;
								    		// echo '<p>'.$_SESSION['files_survey'].'</p>';
            							}
            							?>
								    </div>
								</div>
							</div>
							<div class="cr-screenshot pull-left"><p><img src="img/screenshots/survey.jpg" alt="Survey"></p></div>
						</div>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p>Enable</p>
								<p><input type="checkbox" id="socialmedia" name="socialmedia" value="1" <?php if (getFieldValue('socialmedia', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 1) { echo 'checked'; } ?>></p>
								<p><img src="img/icons/social-media.png" alt="Social Media"></p>
							</div>
							<div class="cr-description pull-left" style="width:800px;">
								<h4>Social Media</h4>
								<p>Use of the Social Media widget will open a new browser window to the URL of your choosing. Please provide the URL of the social media feed you'd like to share.</p>
								<p><input type="text" id="socialmedia_url" name="socialmedia_url" class="input-wide" placeholder="Enter URL" value="<?php echo getFieldValue('socialmedia_url', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>"></p>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="cr-icon pull-left">
								<p>Enable</p>
								<p><input type="checkbox" id="resourcelist" name="resourcelist" value="1" <?php if (getFieldValue('resourcelist', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 1) { echo 'checked'; } ?>></p>
								<p><img src="img/icons/resource-list.png" alt="Resource List"></p>
							</div>
							<div class="cr-description pull-left">
								<h4>Resource List <a href="img/webinar-resourcelist.jpg" class="cbox-image" title="Resource List"><i class="helper-icon icon-picture"></i></a></h4>
								<p>The Resource List can hold any type of file (under 25MB) or URL. It is recommend to have no more than 10-11 links or assets in this section.</p>
								<p><input type="text" id="resourcelist_url" name="resourcelist_url" class="input-wide" placeholder="Enter URL" value="<?php echo getFieldValue('resourcelist_url', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>"></p>
								<div class="upload-div">
								    <div id="resource" class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add"><span class="fileupload-new">Add File(s)</span><input id="fileupload_resource" type="file" name="files[]" multiple></span>
									</div>
									<div id="progress-resource" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 175px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-resource" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_resource'])) {
								    		$x=0;
								    		foreach ($_SESSION['files_resource'] as $val_resource) {
								    			$filebox = '<div name="'. ($val_resource). '">';
								    			$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($val_resource) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_resource_path'][$x].'">' . $val_resource . '</a></p>';
							                    $filebox.= '</div>';
							                    echo $filebox;
								    			// echo '<p>'.$val_resource.'</p>';
								    			$x++;
								    		}
            							}
            							?>
								    </div>
								</div>
							</div>
							<div class="cr-screenshot pull-left"><p><img src="img/screenshots/resource-list.jpg" alt="Resource List"></p></div>
						</div>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-webinar-3" class="btn-green back pull-left">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" style="margin-left:8px;">Save &amp; Continue</a><a href="" id="save-and-exit" class="btn-green submit plain pull-right">Save &amp; Exit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>
