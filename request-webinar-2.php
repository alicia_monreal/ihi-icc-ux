<?php
$loc = 'campaign-requests';
$step = 2;
$c_type = 'webinar';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

/*
echo '<pre>';
var_dump($_SESSION);
echo '</pre>';
*/
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Webinar Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fiveup">
				<li>Step 1</li>
				<li class="active">Step 2</li>
				<li>Step 3</li>
				<li>Step 4</li>
				<li>Step 5</li>
			</ul>
			<div class="request-step step2">
				<div class="fieldgroup">
					<p>We are currently accepting requests for English-language, non-NDA webinars for 50+ users. Tell us more about your webinar:</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p><p>Click on the <i class="icon-question-sign" style="margin-top: 3px;"></i> for more information and examples.</p>
					<h3 class="fieldgroup-header">Webinar Details</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_webinar.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="2">
					<input type="hidden" id="c_type" name="c_type" value="webinar">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="in_progress_form" name="in_progress_form" value="0">

					<div class="fieldgroup">
						<?php include('includes/tactic-basics.php'); ?>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<label for="webinar_title" class="control-label">Webinar Title</label>
							<div class="controls"><span class="required-mark">*</span><input type="text" id="webinar_title" name="webinar_title" class="input-wide required" value="<?php echo getFieldValue('webinar_title', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>"></div>
						</div>
						<div class="control-group">
							<label for="start_date" class="control-label">Webinar Date</label>
							<div class="controls"><span class="required-mark">*</span><div id="datetimepicker" class="input-append date form_datetime" style=""><input type="text" id="start_date" name="start_date" class="input-medium required" value="<?php echo $_SESSION['start_date']; ?>"><span class="add-on"><i class="icon-th"></i></span></div>
							<p class="input-note">Please select the date carefully as it may not be possible to change it.</p><div id="webinar_date_span" style="input-note"></div></div>
						</div>
						<div class="control-group">
							<label for="start_time" class="control-label">Webinar Start Time</label>
							<div class="controls"><span class="required-mark">*</span><select id="start_time" name="start_time" class="input-medium required" style="width:120px;margin-right:2px;">
								<option value="">Select One</option>
								<option value="01:00" <?php if ($_SESSION['start_time'] == '01:00') { echo 'selected'; } ?>>01:00</option>
								<option value="01:30" <?php if ($_SESSION['start_time'] == '01:30') { echo 'selected'; } ?>>01:30</option>
								<option value="02:00" <?php if ($_SESSION['start_time'] == '02:00') { echo 'selected'; } ?>>02:00</option>
								<option value="02:30" <?php if ($_SESSION['start_time'] == '02:30') { echo 'selected'; } ?>>02:30</option>
								<option value="03:00" <?php if ($_SESSION['start_time'] == '03:00') { echo 'selected'; } ?>>03:00</option>
								<option value="03:30" <?php if ($_SESSION['start_time'] == '03:30') { echo 'selected'; } ?>>03:30</option>
								<option value="04:00" <?php if ($_SESSION['start_time'] == '04:00') { echo 'selected'; } ?>>04:00</option>
								<option value="04:30" <?php if ($_SESSION['start_time'] == '04:30') { echo 'selected'; } ?>>04:30</option>
								<option value="05:00" <?php if ($_SESSION['start_time'] == '05:00') { echo 'selected'; } ?>>05:00</option>
								<option value="05:30" <?php if ($_SESSION['start_time'] == '05:30') { echo 'selected'; } ?>>05:30</option>
								<option value="06:00" <?php if ($_SESSION['start_time'] == '06:00') { echo 'selected'; } ?>>06:00</option>
								<option value="06:30" <?php if ($_SESSION['start_time'] == '06:30') { echo 'selected'; } ?>>06:30</option>
								<option value="07:00" <?php if ($_SESSION['start_time'] == '07:00') { echo 'selected'; } ?>>07:00</option>
								<option value="07:30" <?php if ($_SESSION['start_time'] == '07:30') { echo 'selected'; } ?>>07:30</option>
								<option value="08:00" <?php if ($_SESSION['start_time'] == '08:00') { echo 'selected'; } ?>>08:00</option>
								<option value="08:30" <?php if ($_SESSION['start_time'] == '08:30') { echo 'selected'; } ?>>08:30</option>
								<option value="09:00" <?php if ($_SESSION['start_time'] == '09:00') { echo 'selected'; } ?>>09:00</option>
								<option value="09:30" <?php if ($_SESSION['start_time'] == '09:30') { echo 'selected'; } ?>>09:30</option>
								<option value="10:00" <?php if ($_SESSION['start_time'] == '10:00') { echo 'selected'; } ?>>10:00</option>
								<option value="10:30" <?php if ($_SESSION['start_time'] == '10:30') { echo 'selected'; } ?>>10:30</option>
								<option value="11:00" <?php if ($_SESSION['start_time'] == '11:00') { echo 'selected'; } ?>>11:00</option>
								<option value="11:30" <?php if ($_SESSION['start_time'] == '11:30') { echo 'selected'; } ?>>11:30</option>
								<option value="12:00" <?php if ($_SESSION['start_time'] == '12:00') { echo 'selected'; } ?>>12:00</option>
								<option value="12:30" <?php if ($_SESSION['start_time'] == '12:30') { echo 'selected'; } ?>>12:30</option>
							</select><select id="start_ampm" name="start_ampm" style="width: 64px;"><option value="AM" <?php if (!isset($_SESSION['start_ampm']) || $_SESSION['start_ampm'] == 'AM') { echo 'selected'; } ?>>AM</option><option value="PM" <?php if ($_SESSION['start_ampm'] == 'PM') { echo 'selected'; } ?>>PM</option></select>
							<span style="font-weight: 600;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End Time</span><span class="required-mark" style="margin-left:5px">*</span><select id="end_time" name="end_time" class="input-medium required" style="width:120px;margin-right:2px;">
								<option value="">Select One</option>
								<option value="01:00" <?php if ($_SESSION['end_time'] == '01:00') { echo 'selected'; } ?>>01:00</option>
								<option value="01:30" <?php if ($_SESSION['end_time'] == '01:30') { echo 'selected'; } ?>>01:30</option>
								<option value="02:00" <?php if ($_SESSION['end_time'] == '02:00') { echo 'selected'; } ?>>02:00</option>
								<option value="02:30" <?php if ($_SESSION['end_time'] == '02:30') { echo 'selected'; } ?>>02:30</option>
								<option value="03:00" <?php if ($_SESSION['end_time'] == '03:00') { echo 'selected'; } ?>>03:00</option>
								<option value="03:30" <?php if ($_SESSION['end_time'] == '03:30') { echo 'selected'; } ?>>03:30</option>
								<option value="04:00" <?php if ($_SESSION['end_time'] == '04:00') { echo 'selected'; } ?>>04:00</option>
								<option value="04:30" <?php if ($_SESSION['end_time'] == '04:30') { echo 'selected'; } ?>>04:30</option>
								<option value="05:00" <?php if ($_SESSION['end_time'] == '05:00') { echo 'selected'; } ?>>05:00</option>
								<option value="05:30" <?php if ($_SESSION['end_time'] == '05:30') { echo 'selected'; } ?>>05:30</option>
								<option value="06:00" <?php if ($_SESSION['end_time'] == '06:00') { echo 'selected'; } ?>>06:00</option>
								<option value="06:30" <?php if ($_SESSION['end_time'] == '06:30') { echo 'selected'; } ?>>06:30</option>
								<option value="07:00" <?php if ($_SESSION['end_time'] == '07:00') { echo 'selected'; } ?>>07:00</option>
								<option value="07:30" <?php if ($_SESSION['end_time'] == '07:30') { echo 'selected'; } ?>>07:30</option>
								<option value="08:00" <?php if ($_SESSION['end_time'] == '08:00') { echo 'selected'; } ?>>08:00</option>
								<option value="08:30" <?php if ($_SESSION['end_time'] == '08:30') { echo 'selected'; } ?>>08:30</option>
								<option value="09:00" <?php if ($_SESSION['end_time'] == '09:00') { echo 'selected'; } ?>>09:00</option>
								<option value="09:30" <?php if ($_SESSION['end_time'] == '09:30') { echo 'selected'; } ?>>09:30</option>
								<option value="10:00" <?php if ($_SESSION['end_time'] == '10:00') { echo 'selected'; } ?>>10:00</option>
								<option value="10:30" <?php if ($_SESSION['end_time'] == '10:30') { echo 'selected'; } ?>>10:30</option>
								<option value="11:00" <?php if ($_SESSION['end_time'] == '11:00') { echo 'selected'; } ?>>11:00</option>
								<option value="11:30" <?php if ($_SESSION['end_time'] == '11:30') { echo 'selected'; } ?>>11:30</option>
								<option value="12:00" <?php if ($_SESSION['end_time'] == '12:00') { echo 'selected'; } ?>>12:00</option>
								<option value="12:30" <?php if ($_SESSION['end_time'] == '12:30') { echo 'selected'; } ?>>12:30</option>
							</select><select id="end_ampm" name="end_ampm" style="width: 64px;"><option value="AM" <?php if (!isset($_SESSION['end_ampm']) || $_SESSION['end_ampm'] == 'AM') { echo 'selected'; } ?>>AM</option><option value="PM" <?php if ($_SESSION['end_ampm'] == 'PM') { echo 'selected'; } ?>>PM</option></select>
							</div>
						</div>
						<div class="wb-date-validation hide">
							<label for="time_zone" class="control-label"></label>
							<p style="margin-top:-15px;" class="error">End Time can't be before the Start Time</p>
						</div>
						<div class="control-group">
							<label for="time_zone" class="control-label">Time Zone</label>
							<div class="controls"><span class="required-mark">*</span><select id="time_zone" name="time_zone" class="input-medium required">
								<option value="">Select One</option>
								<?php
								reset($timezones_array);
								foreach ($timezones_array as $value_tz) {
    								echo '<option value="'.$value_tz.'"'; if ($_SESSION['time_zone'] == $value_tz) { echo ' selected'; } echo '>'.$value_tz.'</option>';
								}
								?>
							</select></div>
						</div>
						<div class="control-group">
							<label for="description" class="control-label">Description/Abstract</label>
							<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="description" name="description" class="input-wide textarea-medium required" placeholder="Up to 200 words"><?php echo getFieldValue('description', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea><p class="input-note">This is the publically viewed description of your event that will appear on listing pages and registration pages.</p></div>
						</div>
						<div class="control-group">
							<label for="asset_reviewers" class="control-label">Asset Reviewers</label>
							<div class="controls"><textarea id="asset_reviewers" name="asset_reviewers" class="input-wide textarea-medium" placeholder="Provide a list of names and email addresses for the approval process."><?php echo getFieldValue('asset_reviewers', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
						</div>
						<div class="control-group">
							<label for="expected_attendees" class="control-label">Expected Attendees</label>
							<div class="controls"><select id="expected_attendees" name="expected_attendees" class="input-medium">
								<option value="">Select One</option>
								<option value="50 - 100" <?php if (getFieldValue('expected_attendees', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '50 - 100') { echo 'selected'; } ?>>50 - 100</option>
								<option value="100 - 200" <?php if (getFieldValue('expected_attendees', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '100 - 200') { echo 'selected'; } ?>>100 - 200</option>
								<option value="200+" <?php if (getFieldValue('expected_attendees', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '200+') { echo 'selected'; } ?>>200+</option>
							</select><p class="input-note">This is not the recommended platform for webinars with less than 50 attendees. Please use Intel's Lync service for those events.</p></div>
						</div>
						<div class="control-group">
							<label for="topics_covered" class="control-label">Topic(s)</label>
							<div class="controls" style="margin-left:244px;">
								<p style="margin-top:5px;margin-bottom:6px;"><span class="required-mark" style="margin-right:5px;">*</span>Choose topics to associate with the webinar (these are topic filters available at Intel.com):</p>
								<div class="pull-left">
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Big Data" <?php if (in_array('Big Data', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Big Data</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Consumerization of IT" <?php if (in_array('Consumerization of IT', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Consumerization of IT</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Desktop Virtualization" <?php if (in_array('Desktop Virtualization', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Desktop Virtualization</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="High Performance Computing" <?php if (in_array('High Performance Computing', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> High Performance Computing</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Manageability" <?php if (in_array('Manageability', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Manageability</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Security" <?php if (in_array('Security', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Security</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Other" <?php if (in_array('Other', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required other-topic"> Other</label><br />
								</div>
								<div class="pull-left">
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Cloud Computing" <?php if (in_array('Cloud Computing', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Cloud Computing</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Data Center Efficiency" <?php if (in_array('Data Center Efficiency', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Data Center Efficiency</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Embedded" <?php if (in_array('Embedded', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Embedded</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Internet of Things" <?php if (in_array('Internet of Things', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Internet of Things</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Mobility" <?php if (in_array('Mobility', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Mobility</label><br />
									<label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Virtualization" <?php if (in_array('Virtualization', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Virtualization</label><br />
								</div>
								<div style="clear:both;">
									<input type="text" id="topics_covered_other" name="topics_covered_other" class="input-wide clearfix" placeholder="If Other, please enter topic(s) here" value="<?php echo getFieldValue('topics_covered_other', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>" style="width:456px;clear:both;margin-top:10px;" <?php if (!in_array('Other', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'disabled'; } ?>>
								</div>
							</div>
						</div>
						<div class="control-group">
							<label for="nda_content" class="control-label" style="padding-top:0;">Does your webinar include NDA content?</label>
							<div class="controls">
								<label class="radio inline"> <input type="radio" id="nda_content[]" name="nda_content" value="1" <?php if (getFieldValue('nda_content', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '1') { echo 'checked'; } ?>> Yes</label>
								<label class="radio inline"> <input type="radio" id="nda_content[]" name="nda_content" value="0" <?php if (getFieldValue('nda_content', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('nda_content', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo 'checked'; } ?>> No</label><br />
							</div>
						</div>
						<div class="control-group">
							<label for="green_screen_utilized" class="control-label" style="padding-top:0;">Does this webinar utilize a green screen?</label>
							<div class="controls">
								<label class="radio inline"> <input type="radio" id="green_screen_utilized[]" name="green_screen_utilized" value="1" <?php if (getFieldValue('green_screen_utilized', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '1') { echo 'checked'; } ?>> Yes</label>
								<label class="radio inline"> <input type="radio" id="green_screen_utilized[]" name="green_screen_utilized" value="0" <?php if (getFieldValue('green_screen_utilized', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('green_screen_utilized', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo 'checked'; } ?>> No</label><br />
							</div>
						</div>
						<div class="control-group">
							<label for="listed_on_intel" class="control-label" style="padding-top:0;">Do you want this webinar listed on Intel.com?</label>
							<div class="controls">
								<label class="radio inline"> <input type="radio" id="listed_on_intel[]" name="listed_on_intel" value="1" <?php if (getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '1') { echo 'checked'; } ?>> Yes</label>
								<label class="radio inline"> <input type="radio" id="listed_on_intel[]" name="listed_on_intel" value="0" <?php if (getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo 'checked'; } ?>> No</label><br />
							</div>
						</div>
						<div id="available-on-intel-end-date" class="control-group<?php if (getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo ' hide'; } ?>">
							<label for="available_on_intel_end_date" class="control-label" style="padding-top:0;">How long do you want the webinar to be available on Intel.com?</label>
							<div class="controls"><span class="required-mark">*</span>
								<div id="available_on_demand_date_picker" class="input-append date" style=""><input type="text" id="available_on_intel_end_date" name="available_on_intel_end_date" class="input-medium required" value="<?php echo $_SESSION['available_on_intel_end_date']; ?>"><span class="add-on" style="margin-right:20px;"><i class="icon-th"></i></span></div>
							</div>
						</div>
						<h3 class="fieldgroup-header">Notification Emails</h3>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<label for="webinar_initial_date" class="control-label">Webinar Initial Invitation Date</label>
							<div class="controls">
								<span class="required-mark">*</span><div id="webinar_initial_date_picker" class="input-append date" style=""><input type="text" id="webinar_initial_date" name="webinar_initial_date" class="input-medium required" value="<?php echo $_SESSION['webinar_initial_date']; ?>"><span class="add-on" style="margin-right:20px;"><i class="icon-th"></i></span></div><span style="font-weight: 600;">&nbsp;Send Time</span><span class="required-mark" style="margin-left:5px">*</span> <select id="send_time_invitation" name="send_time_invitation" class="input-medium required" style="width:120px;margin-right:2px;">
										<option value="">Select One</option>
										<option value="01:00" <?php if ($_SESSION['send_time_invitation'] == '01:00') { echo 'selected'; } ?>>01:00</option>
										<option value="01:30" <?php if ($_SESSION['send_time_invitation'] == '01:30') { echo 'selected'; } ?>>01:30</option>
										<option value="02:00" <?php if ($_SESSION['send_time_invitation'] == '02:00') { echo 'selected'; } ?>>02:00</option>
										<option value="02:30" <?php if ($_SESSION['send_time_invitation'] == '02:30') { echo 'selected'; } ?>>02:30</option>
										<option value="03:00" <?php if ($_SESSION['send_time_invitation'] == '03:00') { echo 'selected'; } ?>>03:00</option>
										<option value="03:30" <?php if ($_SESSION['send_time_invitation'] == '03:30') { echo 'selected'; } ?>>03:30</option>
										<option value="04:00" <?php if ($_SESSION['send_time_invitation'] == '04:00') { echo 'selected'; } ?>>04:00</option>
										<option value="04:30" <?php if ($_SESSION['send_time_invitation'] == '04:30') { echo 'selected'; } ?>>04:30</option>
										<option value="05:00" <?php if ($_SESSION['send_time_invitation'] == '05:00') { echo 'selected'; } ?>>05:00</option>
										<option value="05:30" <?php if ($_SESSION['send_time_invitation'] == '05:30') { echo 'selected'; } ?>>05:30</option>
										<option value="06:00" <?php if ($_SESSION['send_time_invitation'] == '06:00') { echo 'selected'; } ?>>06:00</option>
										<option value="06:30" <?php if ($_SESSION['send_time_invitation'] == '06:30') { echo 'selected'; } ?>>06:30</option>
										<option value="07:00" <?php if ($_SESSION['send_time_invitation'] == '07:00') { echo 'selected'; } ?>>07:00</option>
										<option value="07:30" <?php if ($_SESSION['send_time_invitation'] == '07:30') { echo 'selected'; } ?>>07:30</option>
										<option value="08:00" <?php if (!isset($_SESSION['send_time_invitation']) || $_SESSION['send_time_invitation'] == '08:00') { echo 'selected'; } ?>>08:00</option>
										<option value="08:30" <?php if ($_SESSION['send_time_invitation'] == '08:30') { echo 'selected'; } ?>>08:30</option>
										<option value="09:00" <?php if ($_SESSION['send_time_invitation'] == '09:00') { echo 'selected'; } ?>>09:00</option>
										<option value="09:30" <?php if ($_SESSION['send_time_invitation'] == '09:30') { echo 'selected'; } ?>>09:30</option>
										<option value="10:00" <?php if ($_SESSION['send_time_invitation'] == '10:00') { echo 'selected'; } ?>>10:00</option>
										<option value="10:30" <?php if ($_SESSION['send_time_invitation'] == '10:30') { echo 'selected'; } ?>>10:30</option>
										<option value="11:00" <?php if ($_SESSION['send_time_invitation'] == '11:00') { echo 'selected'; } ?>>11:00</option>
										<option value="11:30" <?php if ($_SESSION['send_time_invitation'] == '11:30') { echo 'selected'; } ?>>11:30</option>
										<option value="12:00" <?php if ($_SESSION['send_time_invitation'] == '12:00') { echo 'selected'; } ?>>12:00</option>
										<option value="12:30" <?php if ($_SESSION['send_time_invitation'] == '12:30') { echo 'selected'; } ?>>12:30</option>
									</select><select id="send_invitation_ampm" name="send_invitation_ampm" style="width: 64px;"><option value="AM" <?php if (!isset($_SESSION['send_invitation_ampm']) || $_SESSION['send_invitation_ampm'] == 'AM') { echo 'selected'; } ?>>AM</option><option value="PM" <?php if ($_SESSION['send_invitation_ampm'] == 'PM') { echo 'selected'; } ?>>PM</option></select><p class="input-note">This is the date the email invitation will be sent to invite attendees to your event.</p>
							</div>
						</div>
						<div class="control-group">
							<label for="webinar_reminder_1_date" class="control-label">Webinar Reminder 1 Date</label>
							<div class="controls">
								<span class="required-mark">*</span><div id="webinar_reminder_1_date_picker" class="input-append date" style=""><input type="text" id="webinar_reminder_1_date" name="webinar_reminder_1_date" class="input-medium required" value="<?php echo $_SESSION['webinar_reminder_1_date']; ?>"><span class="add-on" style="margin-right:20px;"><i class="icon-th"></i></span></div><span style="font-weight: 600;">&nbsp;Send Time</span><span class="required-mark" style="margin-left:5px">*</span> <select id="send_time_reminder_1" name="send_time_reminder_1" class="input-medium required" style="width:120px;margin-right:2px;">
										<option value="">Select One</option>
										<option value="01:00" <?php if ($_SESSION['send_time_reminder_1'] == '01:00') { echo 'selected'; } ?>>01:00</option>
										<option value="01:30" <?php if ($_SESSION['send_time_reminder_1'] == '01:30') { echo 'selected'; } ?>>01:30</option>
										<option value="02:00" <?php if ($_SESSION['send_time_reminder_1'] == '02:00') { echo 'selected'; } ?>>02:00</option>
										<option value="02:30" <?php if ($_SESSION['send_time_reminder_1'] == '02:30') { echo 'selected'; } ?>>02:30</option>
										<option value="03:00" <?php if ($_SESSION['send_time_reminder_1'] == '03:00') { echo 'selected'; } ?>>03:00</option>
										<option value="03:30" <?php if ($_SESSION['send_time_reminder_1'] == '03:30') { echo 'selected'; } ?>>03:30</option>
										<option value="04:00" <?php if ($_SESSION['send_time_reminder_1'] == '04:00') { echo 'selected'; } ?>>04:00</option>
										<option value="04:30" <?php if ($_SESSION['send_time_reminder_1'] == '04:30') { echo 'selected'; } ?>>04:30</option>
										<option value="05:00" <?php if ($_SESSION['send_time_reminder_1'] == '05:00') { echo 'selected'; } ?>>05:00</option>
										<option value="05:30" <?php if ($_SESSION['send_time_reminder_1'] == '05:30') { echo 'selected'; } ?>>05:30</option>
										<option value="06:00" <?php if ($_SESSION['send_time_reminder_1'] == '06:00') { echo 'selected'; } ?>>06:00</option>
										<option value="06:30" <?php if ($_SESSION['send_time_reminder_1'] == '06:30') { echo 'selected'; } ?>>06:30</option>
										<option value="07:00" <?php if ($_SESSION['send_time_reminder_1'] == '07:00') { echo 'selected'; } ?>>07:00</option>
										<option value="07:30" <?php if ($_SESSION['send_time_reminder_1'] == '07:30') { echo 'selected'; } ?>>07:30</option>
										<option value="08:00" <?php if (!isset($_SESSION['send_time_reminder_1']) || $_SESSION['send_time_reminder_1'] == '08:00') { echo 'selected'; } ?>>08:00</option>
										<option value="08:30" <?php if ($_SESSION['send_time_reminder_1'] == '08:30') { echo 'selected'; } ?>>08:30</option>
										<option value="09:00" <?php if ($_SESSION['send_time_reminder_1'] == '09:00') { echo 'selected'; } ?>>09:00</option>
										<option value="09:30" <?php if ($_SESSION['send_time_reminder_1'] == '09:30') { echo 'selected'; } ?>>09:30</option>
										<option value="10:00" <?php if ($_SESSION['send_time_reminder_1'] == '10:00') { echo 'selected'; } ?>>10:00</option>
										<option value="10:30" <?php if ($_SESSION['send_time_reminder_1'] == '10:30') { echo 'selected'; } ?>>10:30</option>
										<option value="11:00" <?php if ($_SESSION['send_time_reminder_1'] == '11:00') { echo 'selected'; } ?>>11:00</option>
										<option value="11:30" <?php if ($_SESSION['send_time_reminder_1'] == '11:30') { echo 'selected'; } ?>>11:30</option>
										<option value="12:00" <?php if ($_SESSION['send_time_reminder_1'] == '12:00') { echo 'selected'; } ?>>12:00</option>
										<option value="12:30" <?php if ($_SESSION['send_time_reminder_1'] == '12:30') { echo 'selected'; } ?>>12:30</option>
									</select><select id="send_reminder_1_ampm" name="send_reminder_1_ampm" style="width: 64px;"><option value="AM" <?php if (!isset($_SESSION['send_reminder_1_ampm']) || $_SESSION['send_reminder_1_ampm'] == 'AM') { echo 'selected'; } ?>>AM</option><option value="PM" <?php if ($_SESSION['send_reminder_1_ampm'] == 'PM') { echo 'selected'; } ?>>PM</option></select>
							</div>
						</div>
						<div class="control-group">
							<label for="webinar_reminder_2_date" class="control-label">Webinar Reminder 2 Date</label>
							<div class="controls">
								<span class="required-mark">*</span><div id="webinar_reminder_2_date_picker" class="input-append date" style=""><input type="text" id="webinar_reminder_2_date" name="webinar_reminder_2_date" class="input-medium required" value="<?php echo $_SESSION['webinar_reminder_2_date']; ?>"><span class="add-on" style="margin-right:20px;"><i class="icon-th"></i></span></div><span style="font-weight: 600;">&nbsp;Send Time</span><span class="required-mark" style="margin-left:5px;">*</span> <select id="send_time_reminder_2" name="send_time_reminder_2" class="input-medium required" style="width:120px;margin-right:2px;">
										<option value="">Select One</option>
										<option value="01:00" <?php if ($_SESSION['send_time_reminder_2'] == '01:00') { echo 'selected'; } ?>>01:00</option>
										<option value="01:30" <?php if ($_SESSION['send_time_reminder_2'] == '01:30') { echo 'selected'; } ?>>01:30</option>
										<option value="02:00" <?php if ($_SESSION['send_time_reminder_2'] == '02:00') { echo 'selected'; } ?>>02:00</option>
										<option value="02:30" <?php if ($_SESSION['send_time_reminder_2'] == '02:30') { echo 'selected'; } ?>>02:30</option>
										<option value="03:00" <?php if ($_SESSION['send_time_reminder_2'] == '03:00') { echo 'selected'; } ?>>03:00</option>
										<option value="03:30" <?php if ($_SESSION['send_time_reminder_2'] == '03:30') { echo 'selected'; } ?>>03:30</option>
										<option value="04:00" <?php if ($_SESSION['send_time_reminder_2'] == '04:00') { echo 'selected'; } ?>>04:00</option>
										<option value="04:30" <?php if ($_SESSION['send_time_reminder_2'] == '04:30') { echo 'selected'; } ?>>04:30</option>
										<option value="05:00" <?php if ($_SESSION['send_time_reminder_2'] == '05:00') { echo 'selected'; } ?>>05:00</option>
										<option value="05:30" <?php if ($_SESSION['send_time_reminder_2'] == '05:30') { echo 'selected'; } ?>>05:30</option>
										<option value="06:00" <?php if ($_SESSION['send_time_reminder_2'] == '06:00') { echo 'selected'; } ?>>06:00</option>
										<option value="06:30" <?php if ($_SESSION['send_time_reminder_2'] == '06:30') { echo 'selected'; } ?>>06:30</option>
										<option value="07:00" <?php if ($_SESSION['send_time_reminder_2'] == '07:00') { echo 'selected'; } ?>>07:00</option>
										<option value="07:30" <?php if ($_SESSION['send_time_reminder_2'] == '07:30') { echo 'selected'; } ?>>07:30</option>
										<option value="08:00" <?php if (!isset($_SESSION['send_time_reminder_2']) || $_SESSION['send_time_reminder_2'] == '08:00') { echo 'selected'; } ?>>08:00</option>
										<option value="08:30" <?php if ($_SESSION['send_time_reminder_2'] == '08:30') { echo 'selected'; } ?>>08:30</option>
										<option value="09:00" <?php if ($_SESSION['send_time_reminder_2'] == '09:00') { echo 'selected'; } ?>>09:00</option>
										<option value="09:30" <?php if ($_SESSION['send_time_reminder_2'] == '09:30') { echo 'selected'; } ?>>09:30</option>
										<option value="10:00" <?php if ($_SESSION['send_time_reminder_2'] == '10:00') { echo 'selected'; } ?>>10:00</option>
										<option value="10:30" <?php if ($_SESSION['send_time_reminder_2'] == '10:30') { echo 'selected'; } ?>>10:30</option>
										<option value="11:00" <?php if ($_SESSION['send_time_reminder_2'] == '11:00') { echo 'selected'; } ?>>11:00</option>
										<option value="11:30" <?php if ($_SESSION['send_time_reminder_2'] == '11:30') { echo 'selected'; } ?>>11:30</option>
										<option value="12:00" <?php if ($_SESSION['send_time_reminder_2'] == '12:00') { echo 'selected'; } ?>>12:00</option>
										<option value="12:30" <?php if ($_SESSION['send_time_reminder_2'] == '12:30') { echo 'selected'; } ?>>12:30</option>
									</select><select id="send_reminder_2_ampm" name="send_reminder_2_ampm" style="width: 64px;"><option value="AM" <?php if (!isset($_SESSION['send_reminder_2_ampm']) || $_SESSION['send_reminder_2_ampm'] == 'AM') { echo 'selected'; } ?>>AM</option><option value="PM" <?php if ($_SESSION['send_reminder_2_ampm'] == 'PM') { echo 'selected'; } ?>>PM</option></select>
							</div>
						</div>
					<h3 class="fieldgroup-header">Email Details</h3>
					</div>

					<div class="fieldgroup">
						<?php include('includes/email-basics.php'); ?>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request" class="btn-green back pull-left">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" style="margin-left:8px;">Next</a><a href="" id="save-and-exit" class="btn-green submit plain pull-right">Save &amp; Exit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>