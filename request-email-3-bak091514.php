<?php
$loc = 'campaign-requests';
$step = 3;
$c_type = 'email';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Single Email Campaign Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fourup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li class="active">Step 3</li>
				<li>Step 4</li>
			</ul>
			<div class="request-step step3">


				<form id="content-update-form" class="form-horizontal" action="includes/_requests_email.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="3">
					<input type="hidden" id="c_type" name="c_type" value="email">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="standard_template_type" name="standard_template_type" value="<?php echo getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>">

					<div class="fieldgroup" style="border-bottom:0;padding-top:10px;padding-bottom:10px;">
						<h3 class="fieldgroup-header">Choose template and layout configuration</h3>
					</div>

					<div class="fieldgroup" style="border-bottom:0;">
						<div class="control-group">
							<div class="row">
								<div id="template-standard-email" class="template-selector gray-selector pull-left span6<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Standard Template') { echo ' active'; } ?>">
									<input type="radio" id="template[]" name="template" value="Standard Template"<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Standard Template') { echo ' checked'; } ?>>
									<img src="img/templates/intel-template-tmb.png" alt="Standard Template">
									<div style="width:190px;line-height:17px;" class="pull-right">
										<h4>Standard Template</h4>
										Use the approved HQ template for emails. You can choose a default layout, or customize one for your needs.
									</div>
								</div>
								<div id="template-custom" class="template-selector gray-selector pull-left span6<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' active'; } ?>">
									<input type="radio" id="template[]" name="template" value="Custom Template"<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' checked'; } ?> style="margin-top:51px;">
									<div style="width:388px;line-height:17px;" class="pull-right">
										<h4>Provide Your Own Template</h4>
										If your geo or group has its own template, or if your agency has already built out your assets for you, you can select this option to upload your own html content and image assets.
									</div>
								</div>
							</div>
						</div>
					</div>

					<!--CONFIGURE LAYOUT-->
					<div id="configure-layout-block" class="template-option-block<?php if (getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Default') { echo ' hide'; } ?>">

						<p style="margin-bottom: 40px;">The emails constructed will be built in accordance to the <a href="/files/Intel Eloqua Usage Guidelines.pdf" target="_blank">Eloqua Template Guidelines</a>. If you have any content requirements that are not being collected in this ICC form, please list them out in the Additional Instructions field at the bottom of this page.</p>

						<!--Email Configuration-->
						<div class="fieldgroup" style="padding-top:0px; border-bottom:2px solid #707070;">
							<h3 class="fieldgroup-header" style="margin-top:12px;">Email Configuration</h3>
						</div>

						<div class="fieldgroup" style="border-bottom:0;">
							<div class="control-group">
								<div class="accordion thumbnail-selector fourup" id="accordion1">
				                    <div class="accordion-group first">
				                        <div class="accordion-heading">
				                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">(<span class="plusminus">+</span>) Select Banner <span class="normal"></span></a>
				                        </div>
				                        <div id="collapseOne" class="accordion-body collapse<?php //if (isset($_SESSION['email_banner'])) { echo ' in'; } ?>">
				                            <div class="accordion-inner">
				                                <ul>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-banner-a.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner A"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner A') { echo ' checked'; } ?>> Banner A</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-banner-b.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner B"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner B') { echo ' checked'; } ?>> Banner B</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-banner-c.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner C"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner C') { echo ' checked'; } ?>> Banner C</label>
				                                	</li>
				                                	<li class="last">
				                                		<img src="img/templates/thumbnails/email-banner-d.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner D"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner D') { echo ' checked'; } ?>> Banner D</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-banner-e.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner E"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner E') { echo ' checked'; } ?>> Banner E</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-banner-f.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner F"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner F') { echo ' checked'; } ?>> Banner F</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-banner-g.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner G"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner G') { echo ' checked'; } ?>> Banner G</label>
				                                	</li>
				                                	<li class="last">
				                                		<img src="img/templates/thumbnails/email-banner-h.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-banner[]" name="email-banner" value="Email Banner H"<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner H') { echo ' checked'; } ?>> Banner H</label>
				                                	</li>
				                                </ul>

				                                <!--Hidden image choices-->
				                                <div id="choose-images-email-banner" class="choose-images thumbnail-selector fiveup<?php if (getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner E' || getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner F' || getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner G' || getFieldValue('email_banner', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Banner H') { echo ' hide'; } ?>">
				                                	<div class="choose-images-block">
					                                	<h3>Please choose your email banner image or upload a custom image</h3>
					                                	<ul>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_15378.jpg" class="cbox-image" title="IMAL 15378"><img src="img/templates/default-images/thumbnails/IMAL_15378.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_15378.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_15378.jpg') { echo ' checked'; } ?>> A</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_16140.jpg" class="cbox-image" title="IMAL 16140"><img src="img/templates/default-images/thumbnails/IMAL_16140.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_16140.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_16140.jpg') { echo ' checked'; } ?>> B</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_17750.jpg" class="cbox-image" title="IMAL 17750"><img src="img/templates/default-images/thumbnails/IMAL_17750.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_17750.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_17750.jpg') { echo ' checked'; } ?>> C</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_18388.jpg" class="cbox-image" title="IMAL 18388"><img src="img/templates/default-images/thumbnails/IMAL_18388.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_18388.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_18388.jpg') { echo ' checked'; } ?>> D</label>
						                                	</li>
						                                	<li class="last">
						                                		<a href="img/templates/default-images/previews/IMAL_18810.jpg" class="cbox-image" title="IMAL 18810"><img src="img/templates/default-images/thumbnails/IMAL_18810.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_18810.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_18810.jpg') { echo ' checked'; } ?>> E</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_50187.jpg" class="cbox-image" title="IMAL 50187"><img src="img/templates/default-images/thumbnails/IMAL_50187.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_50187.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_50187.jpg') { echo ' checked'; } ?>> F</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_50997.jpg" class="cbox-image" title="IMAL 50997"><img src="img/templates/default-images/thumbnails/IMAL_50997.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_50997.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_50997.jpg') { echo ' checked'; } ?>> G</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_51641.jpg" class="cbox-image" title="IMAL 51641"><img src="img/templates/default-images/thumbnails/IMAL_51641.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_51641.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_51641.jpg') { echo ' checked'; } ?>> H</label>
						                                	</li>
						                                	<li class="last">
						                                		<a href="img/templates/default-images/previews/IMAL_58060.jpg" class="cbox-image" title="IMAL 58060"><img src="img/templates/default-images/thumbnails/IMAL_58060.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-banner-image[]" name="email-banner-image" value="IMAL_58060.jpg"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'IMAL_58060.jpg') { echo ' checked'; } ?>> I</label>
						                                	</li>
						                                </ul>

						                                <p class="clearfix"></p>
						                            </div>

					                                <div class="upload-div pull-left" style="margin-bottom:26px;">
													    <div class="fileupload fileupload-new" data-provides="fileupload">
															<input type="radio" id="email-banner-image[]" name="email-banner-image" value="custom"<?php if (isset($_SESSION['email_banner_image']) && $_SESSION['email_banner_image'] == 'custom') { echo ' checked'; } ?>>&nbsp;&nbsp;&nbsp;<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_emailbannerimage" type="file" name="files[]"></span>
														</div>
														<div id="progress-emailbannerimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 422px;display:none;">
													        <div class="bar"></div>
													    </div>
													    <div id="shown-emailbannerimage" class="files" style="clear:both;">
													    	<?php
													    	if (isset($_SESSION['files_emailbannerimage'])) {
													    		$filebox = '<div name="'. ($_SESSION['files_emailbannerimage']). '">';
										    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_emailbannerimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_emailbannerimage'].'</p>';
									                    		$filebox.= '</div>';
									                    		echo $filebox;
					            							}
					            							?>
													    </div>
													</div>

					                            </div>

					                            <a href="#collapseTwo" class="btn-green continue pull-right accordion-toggle" style="margin-top:18px;margin-bottom:18px;" data-toggle="collapse" data-parent="#accordion1" data-target="#collapseTwo">Continue</a>

				                            </div>
				                        </div>
				                    </div>

				                    <div class="accordion-group">
				                        <div class="accordion-heading">
				                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo">(<span class="plusminus">+</span>) Select Body</a>
				                        </div>
				                        <div id="collapseTwo" class="accordion-body collapse<?php //if (isset($_SESSION['email_body'])) { echo ' in'; } ?>">
				                            <div class="accordion-inner">
				                                <ul>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-body-a.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-body[]" name="email-body" value="Email Body A"<?php if (getFieldValue('email_body', $_SESSION['clone_id'], $_SESSION['request_type'], '')  == '' || getFieldValue('email_body', $_SESSION['clone_id'], $_SESSION['request_type'], '')  == 'Email Body A') { echo ' checked'; } ?>> A</label>
				                                	</li>
				                                	<li class="last">
				                                		<img src="img/templates/thumbnails/email-body-b.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-body[]" name="email-body" value="Email Body B"<?php if (getFieldValue('email_body', $_SESSION['clone_id'], $_SESSION['request_type'], '')  == 'Email Body B') { echo ' checked'; } ?>> B</label>
				                                	</li>
				                                </ul>

				                                <p class="clearfix" style="margin:0;"></p>

				                                 <!--Hidden image choices-->
				                                <div id="choose-images-email-body" class="choose-images thumbnail-selector fiveup<?php if (getFieldValue('email_body', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Body B') { echo ' hide'; } //Taking out first condition of an empty return only because option A is selected by default, which has these options ?>">
				                                	<div class="choose-images-block">
				                                		<h3>Please choose your email body image or upload a custom image</h3>
					                                	<ul>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_15378.jpg" class="cbox-image" title="IMAL 15378"><img src="img/templates/default-images/thumbnails/IMAL_15378.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_15378.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_15378.jpg') { echo ' checked'; } ?>> A</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_16140.jpg" class="cbox-image" title="IMAL 16140"><img src="img/templates/default-images/thumbnails/IMAL_16140.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_16140.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_16140.jpg') { echo ' checked'; } ?>> B</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_17750.jpg" class="cbox-image" title="IMAL 17750"><img src="img/templates/default-images/thumbnails/IMAL_17750.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_17750.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_17750.jpg') { echo ' checked'; } ?>> C</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_18388.jpg" class="cbox-image" title="IMAL 18388"><img src="img/templates/default-images/thumbnails/IMAL_18388.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_18388.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_18388.jpg') { echo ' checked'; } ?>> D</label>
						                                	</li>
						                                	<li class="last">
						                                		<a href="img/templates/default-images/previews/IMAL_18810.jpg" class="cbox-image" title="IMAL 18810"><img src="img/templates/default-images/thumbnails/IMAL_18810.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_18810.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_18810.jpg') { echo ' checked'; } ?>> E</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_50187.jpg" class="cbox-image" title="IMAL 50187"><img src="img/templates/default-images/thumbnails/IMAL_50187.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_50187.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_50187.jpg') { echo ' checked'; } ?>> F</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_50997.jpg" class="cbox-image" title="IMAL 50997"><img src="img/templates/default-images/thumbnails/IMAL_50997.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_50997.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_50997.jpg') { echo ' checked'; } ?>> G</label>
						                                	</li>
						                                	<li>
						                                		<a href="img/templates/default-images/previews/IMAL_51641.jpg" class="cbox-image" title="IMAL 51641"><img src="img/templates/default-images/thumbnails/IMAL_51641.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_51641.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_51641.jpg') { echo ' checked'; } ?>> H</label>
						                                	</li>
						                                	<li class="last">
						                                		<a href="img/templates/default-images/previews/IMAL_58060.jpg" class="cbox-image" title="IMAL 58060"><img src="img/templates/default-images/thumbnails/IMAL_58060.jpg"></a><br />
						                                		<label class="radio inline"><input type="radio" id="email-body-image[]" name="email-body-image" value="IMAL_58060.jpg"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'IMAL_58060.jpg') { echo ' checked'; } ?>> I</label>
						                                	</li>
						                                </ul>

						                                <p class="clearfix"></p>
						                            </div>

					                                <div class="upload-div pull-left" style="margin-bottom:26px;">
													    <div class="fileupload fileupload-new" data-provides="fileupload">
															<input type="radio" id="email-body-image[]" name="email-body-image" value="custom"<?php if (isset($_SESSION['email_body_image']) && $_SESSION['email_body_image'] == 'custom') { echo ' checked'; } ?>>&nbsp;&nbsp;&nbsp;<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_emailbodyimage" type="file" name="files[]"></span>
														</div>
														<div id="progress-emailbodyimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 422px;display:none;">
													        <div class="bar"></div>
													    </div>
													    <div id="shown-emailbodyimage" class="files" style="clear:both;">
													    	<?php
													    	if (isset($_SESSION['files_emailbodyimage'])) {
													    		$filebox = '<div name="'. ($_SESSION['files_emailbodyimage']). '">';
										    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_emailbodyimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_emailbodyimage'].'</p>';
									                    		$filebox.= '</div>';
									                    		echo $filebox;
					            							}
					            							?>
													    </div>
													</div>

												</div>

												<a href="#collapseThree" class="btn-green continue pull-right accordion-toggle" style="margin-top:18px;margin-bottom:18px;" data-toggle="collapse" data-parent="#accordion1" data-target="#collapseThree">Continue</a>

				                            </div>
				                        </div>
				                    </div>

				                	<div class="accordion-group">
				                        <div class="accordion-heading">
				                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree">(<span class="plusminus">+</span>) Select Footer</a>
				                        </div>
				                        <div id="collapseThree" class="accordion-body collapse<?php //if (isset($_SESSION['email_footer'])) { echo ' in'; } ?>">
				                            <div class="accordion-inner">
				                                <ul>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-footer-a.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-footer[]" name="email-footer" value="Email Footer A"<?php if (getFieldValue('email_footer', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('email_footer', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Footer A') { echo ' checked'; } ?>> A</label>
				                                	</li>
				                                	<li class="last">
				                                		<img src="img/templates/thumbnails/email-footer-b.png"><br />
				                                		<label class="radio inline"><input type="radio" id="email-footer[]" name="email-footer" value="Email Footer B"<?php if (getFieldValue('email_footer', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Email Footer B') { echo ' checked'; } ?>> B</label>
				                                	</li>
				                                </ul>

				                                <a href="#collapseFour" class="btn-green continue pull-right accordion-toggle clearfix" style="margin-bottom:18px;clear:both;" data-toggle="collapse" data-parent="#accordion1" data-target="#collapseFour">Continue</a>
				                            </div>
				                        </div>
				                    </div>

				                	<div class="accordion-group">
				                        <div class="accordion-heading">
				                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour">(<span class="plusminus">+</span>) Select Additional Modules</a>
				                        </div>
				                        <div id="collapseFour" class="accordion-body collapse<?php //if (isset($_SESSION['email_modules'])) { echo ' in'; } ?>">
				                            <div class="accordion-inner">
				                                <ul>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-module-speakers.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="Speakers"<?php if (in_array('Speakers', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?> class="chk-speaker-email"> Speakers</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-module-location.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="Location"<?php if (in_array('Location', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?> class="chk-location-email"> Location</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-module-agenda.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="Agenda"<?php if (in_array('Agenda', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?>> Agenda</label>
				                                	</li>
				                                	<li class="last">
				                                		<img src="img/templates/thumbnails/email-module-cta.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="CTA"<?php if (in_array('CTA', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?>> CTA</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-module-cta-alt.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="CTA Alt"<?php if (in_array('CTA Alt', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?>> CTA Alt</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-module-content.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="Content"<?php if (in_array('Content', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?>> Content</label>
				                                	</li>
				                                	<li>
				                                		<img src="img/templates/thumbnails/email-module-content-alt.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="Content Alt"<?php if (in_array('Content Alt', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?> class="chk-content-alt-email"> Content Alt</label>
				                                	</li>
				                                	<li class="last">
				                                		<img src="img/templates/thumbnails/email-module-badge.png"><br />
				                                		<label class="radio inline"><input type="checkbox" id="email-modules[]" name="email-modules[]" value="Badge"<?php if (in_array('Badge', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' checked'; } ?>> Badge</label>
				                                	</li>
				                                </ul>

				                                <p class="clearfix"></p>

				                                <div id="speaker-image-email" class="upload-div<?php if (!in_array('Speakers', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' hide'; } ?>" style="margin-bottom:26px;">
													<p><strong>Speakers Module</strong><br />Please provide appropriately sized images in a zip file. Please ensure that image files are named after the representative speaker (e.g. John Doe.jpg).</p>
												    <div class="fileupload fileupload-new" data-provides="fileupload">
														<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Speaker Zip (180x110)</span><input id="fileupload_emailspeakerimage" type="file" name="files[]"></span>
													</div>
													<div id="progress-emailspeakerimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 396px;display:none;">
												        <div class="bar"></div>
												    </div>
												    <div id="shown-emailspeakerimage" class="files" style="clear:both;">
												    	<?php
												    	if (isset($_SESSION['files_emailspeakerimage'])) {
												    		$filebox = '<div name="'. ($_SESSION['files_emailspeakerimage']). '">';
									    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_emailspeakerimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_emailspeakerimage'].'</p>';
								                    		$filebox.= '</div>';
								                    		echo $filebox;
				            							}
				            							?>
												    </div>
												</div>

				                                <div id="location-image-email" class="upload-div<?php if (!in_array('Location', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' hide'; } ?>" style="margin-bottom:26px;">
				                                	<p><strong>Location Module</strong></p>
												    <div class="fileupload fileupload-new" data-provides="fileupload">
														<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Location Image (292x202)</span><input id="fileupload_emaillocationimage" type="file" name="files[]"></span>
													</div>
													<div id="progress-emaillocationimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 396px;display:none;">
												        <div class="bar"></div>
												    </div>
												    <div id="shown-emaillocationimage" class="files" style="clear:both;">
												    	<?php
												    	if (isset($_SESSION['files_emaillocationimage'])) {
												    		$filebox = '<div name="'. ($_SESSION['files_emaillocationimage']). '">';
									    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_emaillocationimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_emaillocationimage'].'</p>';
								                    		$filebox.= '</div>';
								                    		echo $filebox;
				            							}
				            							?>
												    </div>
												</div>

												<div id="content-alt-image-email" class="upload-div<?php if (!in_array('Content Alt', getFieldValue('email_modules', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo ' hide'; } ?>" style="margin-bottom:26px;">
				                                	<p><strong>Content Alt Module</strong></p>
												    <div class="fileupload fileupload-new" data-provides="fileupload">
														<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Content Image (290x180)</span><input id="fileupload_emailcontentaltimage" type="file" name="files[]"></span>
													</div>
													<div id="progress-emailcontentaltimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 396px;display:none;">
												        <div class="bar"></div>
												    </div>
												    <div id="shown-emailcontentaltimage" class="files" style="clear:both;">
												    	<?php
												    	if (isset($_SESSION['files_emailcontentaltimage'])) {
												    		$filebox = '<div name="'. ($_SESSION['files_emailcontentaltimage']). '">';
									    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_emailcontentaltimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_emailcontentaltimage'].'</p>';
								                    		$filebox.= '</div>';
								                    		echo $filebox;
				            							}
				            							?>
												    </div>
												</div>

				                            </div>
				                        </div>
				                    </div>

				                </div>

				            </div>
						</div><!--/Email Configuration-->

					</div><!-- /CONFIGURE LAYOUT -->

					

					<!-- Block for custom template upload -->
					<div id="custom-assets-block" class="fieldgroup<?php if (getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom') { echo ' hide'; } ?>">
						<div class="control-group">
							<label for="customassets" class="control-label no-padding">Email Template Assets</label>
							<div class="controls" style="margin-left:244px;">
								<p>Please provide completed html files and any other associated assets to be included in the invitation/reminder/follow-up emails.</p>
								
								<div class="upload-div">
								    <div class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Asset Zip</span><input id="fileupload_customassets" type="file" name="files[]"></span>
									</div>
									<div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 239px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-customassets" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_customassets'])) {
								    		echo $_SESSION['files_customassets'];
            							}
            							?>
								    </div>
								</div>
							</div>
						</div>
					</div>


					<!--Template Instructions-->
					<div id="instructions" class="<?php if (getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '') { echo ' hide'; } ?>">
						<div class="fieldgroup" style="padding-top:0px; border-bottom:2px solid #707070;">
							<h3 class="fieldgroup-header" style="margin-top:12px;">Template Instructions</h3>
						</div>

						<div class="fieldgroup" style="border-bottom:0;">
							<div class="control-group">
								<label for="cb" class="control-label">Campaign Brief</label>
								<div class="controls"><p class="input-note">Based upon your configuration, we will send you a campaign brief for you to fill in your specific content. This campaign brief will have the content attributes for your specific configuration. When you have completed this brief, edit the request and upload the xls to the Campaign Brief section.</p></div>
				            </div>
							<div class="control-group">
								<label for="template_instructions" class="control-label">Additional Template Instructions</label>
								<div class="controls"><textarea id="template_instructions" name="template_instructions" class="input-wide textarea-medium" placeholder=""><?php echo getFieldValue('template_instructions', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
				            </div>
						</div><!--/Template Instructions-->
					</div>


					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-email-2" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Email Request: Step 3">Next</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>
