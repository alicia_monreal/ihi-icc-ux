<?php
require_once('includes/_globals.php');
$loc = 'requests';
include('includes/head.php');
include('includes/header.php');

//Kill some session vars
killSessionVars();
?>

   <div class="container">
        <div class="row intro-body" style="margin-bottom:30px;">
            <div class="intro span12">
            	<?php if ($_SESSION['admin'] == 1) { echo '<p class="pull-right"><a href="includes/_export.php?type=tactics" class="btn-green download">Export Data</a></p>'; } ?>
            	<h1>Requests</h1>
                <p>Use this page to manage and track your tactic requests. Click a Tactic Name to see more details.</p>
                <?php
                if (isset($_GET['inprogress']) && $_GET['inprogress'] == true) {
                	echo '<p class="error"><i class="icon-info-sign" style="margin-top: 3px;margin-right: 4px;"></i>Your request has been saved. Click the name of your request to complete or continue to work on it.</p>';
                }
                ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="intro span12" style="margin-bottom:50px;">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
			                req.*,
			                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
			                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
			                status.value AS status_value,
			                request_type.request_type AS request_type,
			                request_type.id AS request_type_id,
			                users.fname AS fname,
			                users.lname AS lname,
			                users.email AS email
			            FROM request AS req
			            INNER JOIN status on req.status = status.id
			            INNER JOIN request_type on req.request_type = request_type.id
			            INNER JOIN users on req.user_id = users.id";

			    $query.=" WHERE req.active = 1"; // All active requests fetched
			    $query.=" ORDER BY req.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			$resultant_array = array(); 

    			if ($row_cnt > 0) {

    				while ($obj = $result->fetch_object()) {

    					$extra_owners = array(); // initialize extra_owners array

                        if(!empty($obj->additional_owners)) { // check if any additional owners exists
                        	$extra_owners = unserialize($obj->additional_owners); // if yes then unserialize the array
                        }

                        // If logged-in user has got ownership rights to the request, then save it    
                        if($obj->user_id == $_SESSION['user_id'] || in_array($_SESSION['user_id'],$extra_owners) 
                        	|| $_SESSION['admin'] == 1)
    						$resultant_array[] = $obj; 
    				}

    				if(!empty($resultant_array)) { // if saved data is not empty

				        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
				        echo '<thead><th>Created <b class="icon-white"></b></th><th>Tactic Name <b class="icon-white"></b></th><th>Owner <b class="icon-white"></b></th><th>Assigned To <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th></thead>';
				        echo '<thead><tr>
									<td rowspan="1" colspan="1"><input type="text" name="search_created" placeholder="Created" class="search_init" style="width:100px;"></td>
									<td rowspan="1" colspan="1"><input type="text" name="search_tacticname" placeholder="Tactic Name" class="search_init"></td>
									<td rowspan="1" colspan="1"><input type="text" name="search_owner" placeholder="Owner" class="search_init"></td>
									<td rowspan="1" colspan="1"><input type="text" name="search_assignedto" placeholder="Assigned To" class="search_init"></td>
									<td rowspan="1" colspan="1"><input type="text" name="search_type" placeholder="Type" class="search_init"></td>
									<td rowspan="1" colspan="1"><input type="text" name="search_status" placeholder="Status" class="search_init"></td>
								</tr></thead>';
				        echo '<tbody>';

				        foreach($resultant_array as $obj) {  
				            //Rating
				            /*No survey's yet
				            $query_sur = "SELECT average FROM surveys WHERE request_id = '".$obj->id."' LIMIT 1";
				            $result_sur = $mysqli->query($query_sur);
				            $row_cnt_sur = $result_sur->num_rows;

				            if ($row_cnt_sur > 0) { //Survey has been submitted
				                while ($obj_sur = $result_sur->fetch_object()) {
				                    $survey_average = '<div style="display:none;">'.round($obj_sur->average, 0).'</div><div class="star-rating">';

				                    $cnt_sur = 1;
				                    for ($i = 1; $i <= 5; $i++) {
				                        $survey_average.='<s';
				                        if (round($obj_sur->average, 0) >= $cnt_sur) {
				                            $survey_average.=' class="rated"';
				                        }
				                        $survey_average.='>';
				                        $cnt_sur++;
				                    }

				                    //$survey_average = round($obj_sur->average, 0);
				                    $survey_average.='</s></s></s></s></s></div>';
				                }
				            } else {
				                if ($obj->status_value == "Request complete") {
				                    $survey_average = '<div style="display:none;">0</div><a href="survey?id='.$obj->id.'">Rate this</a>';
				                } else {
				                    $survey_average = '<div style="display:none;">0</div>--';
				                }
				            }
				            */

				            //Analytics URL
				            $analytics = '';
				            if ($obj->on24_analytics_url != '' || $obj->eloqua_analytics_url != '') {
				                if ($obj->on24_analytics_url != '') {
				                    $parsed_24 = parse_url($obj->on24_analytics_url);
				                    if (empty($parsed_24['scheme'])) { $url_24 = 'http://' . ltrim($obj->on24_analytics_url, '/'); } else { $url_24 = $obj->on24_analytics_url; }
				                    $analytics.='<a href="'.$url_24.'" target="_blank">On24</a><br />';
				                }
				                if ($obj->eloqua_analytics_url != '') {
				                    $parsed_elo = parse_url($obj->eloqua_analytics_url);
				                    if (empty($parsed_elo['scheme'])) { $url_elo = 'http://' . ltrim($obj->eloqua_analytics_url, '/'); } else { $url_elo = $obj->eloqua_analytics_url; }
				                    $analytics.='<a href="'.$url_elo.'" target="_blank">Eloqua</a><br />';
				                }
				                if ($obj->regpage_url != '') {
				                    $parsed_regpage = parse_url($obj->regpage_url);
				                    if (empty($parsed_regpage['scheme'])) { $url_regpage = 'http://' . ltrim($obj->regpage_url, '/'); } else { $url_regpage = $obj->regpage_url; }
				                    $analytics.='<a href="'.$url_regpage.'" target="_blank">Reg Page</a>';
				                }
				            } else {
				                $analytics.='None';
				            }

				            echo '<tr>';
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>'.$obj->date_created.'</td>';
				            //In progress
				            if ($obj->status == 14) {
				            	echo '<td class="in-progress"><a href="includes/_requests_route?in_progress_id='.$obj->id.'&in_progress_type='.$obj->request_type_id.'&campaign_id='.$obj->campaign_id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
					        } else {
					            if ($obj->request_type_id == 8) {
					            	echo '<td><a href="campaign-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
					            } else {
					            	echo '<td><a href="request-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
					            }
					        }
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>'.$obj->fname.' '.$obj->lname.'</td>'; //Who owns it, the person that made it, or the Owner value
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>';
				            if ($_SESSION['admin'] == 1) {
				            	echo getAssignedTo($obj->assigned_to, 0);
				            } else {
				            	echo getAssignedTo($obj->assigned_to, 0);
				            }
				            echo '</td>';

				            $placeholder_status = "";
				            if ($obj->status == 14) $placeholder_status = " (Placeholder)";
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>'.$obj->request_type.$placeholder_status.'</td>';
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>';
				            if ($obj->status == 14) { echo 'User In Progress'; } else { echo getStatus($obj->status); }
				            echo '</td>';
				            echo '</tr>';
				        }
				        
				        echo '</tbody></table>';
			    	} else {
			    		echo '<p><strong>You currently have no active requests.</strong></p>';	
			    	}
			    } else {
			        echo '<p><strong>You currently have no active requests.</strong></p>';
			    }

            	?>
            </div>
        </div>
    </div>

    <?php if ($_SESSION['admin'] == 1) { ?>
    <div class="container">
        <div class="row intro-body" style="margin:20px 0 0 -20px;">
            <div class="intro span12">
            	<h1>At a Glance</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="intro span12" style="margin-bottom:50px;">

            	<div class="row span4" style="margin-left:0;">
	            	<h3 style="font-size:20px;">Total Requests</h3>
	            	<table class="table table-striped table-bordered table-hover requests-table"><thead><th colspan="2">Request Type</th></thead><tbody>
					<?php
					$result = $mysqli->query("SELECT * FROM request_type ORDER BY sort_order ASC");
					while ($obj = $result->fetch_object()) {
					    echo '<tr><td width="200">'.$obj->request_type.'</td><td>'.getTotalRecords('request',$obj->id,'','').'</td></tr>';
					}
					echo '<tr><td><strong>Total</strong></td><td>'.getTotalRecords('request','','','').'</td></tr>';
					?>
					</tbody></table>
				</div>

				<div class="row span4">
					<h3 style="font-size:20px;">Completed</h3>
	            	<table class="table table-striped table-bordered table-hover requests-table"><thead><th colspan="2">Request Type</th></thead><tbody>
					<?php
					$result = $mysqli->query("SELECT * FROM request_type ORDER BY sort_order ASC");
					while ($obj = $result->fetch_object()) {
					    echo '<tr><td width="200">'.$obj->request_type.'</td><td>'.getTotalRecords('request',$obj->id,'4','').'</td></tr>';
					}
					echo '<tr><td><strong>Total</strong></td><td>'.getTotalRecords('request','','4','').'</td></tr>';
					?>
					</tbody></table>
				</div>

				<div class="row span4">
					<h3 style="font-size:20px;">Rejected</h3>
	            	<table class="table table-striped table-bordered table-hover requests-table"><thead><th colspan="2">Request Type</th></thead><tbody>
					<?php
					$result = $mysqli->query("SELECT * FROM request_type ORDER BY sort_order ASC");
					while ($obj = $result->fetch_object()) {
					    echo '<tr><td width="200">'.$obj->request_type.'</td><td>'.getTotalRecords('request',$obj->id,'10','').'</td></tr>';
					}
					echo '<tr><td><strong>Total</strong></td><td>'.getTotalRecords('request','','10','').'</td></tr>';
					?>
					</tbody></table>
				</div>

            </div>
        </div>
    </div>
    <?php } //End if admin ?>

<?php include('includes/footer.php'); ?>