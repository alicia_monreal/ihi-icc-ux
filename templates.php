<?php
$loc = 'templates';
$step = 3.1;
$t_type = 'default';
ob_start();

include('includes/head.php');
include('includes/header-templates.php');
include('includes/_globals.php');

//Just send them to the correct page if they're in progress
if (isset($_SESSION['in_progress']) && $_SESSION['in_progress'] == 1) {
	if (isset($_SESSION['standard_template_type']) && !empty($_SESSION['standard_template_type'])) {
		$url = '/templates-'.strtolower($_SESSION['standard_template_type']).'?type=webinar';
		header('Location:'.$url);
		exit();
	}
}

//Redirect if a Campaign ID hasn't been set
// if (!isset($_SESSION['campaign_id'])) {
    // header('Location:request?error=request-id');
    // echo "Initiate a request";
    // exit();
//}
	$bln_default = (isset($_GET['type']) && $_GET['type'] === 'default') ? true: false;
	$bln_custom  = (isset($_GET['type']) && $_GET['type'] === 'custom') ? true: false;
	$bln_edit	 = (isset($_GET['req_id'])) ? $_GET['req_id']:false;

/*
echo '<pre>';
var_dump($_SESSION);
echo '</pre>';
*/
?>


<div class="container">

	<!--Form-->
    <form id="content-update-form" class="form-horizontal" action="includes/_requests_webinar.php" method="POST" enctype="multipart/form-data">
		<input type="hidden" id="action" name="action" value="create">
		<input type="hidden" id="step" name="step" value="3.1">
		<input type="hidden" id="c_type" name="c_type" value="<?php echo $_GET['type']; ?>">
		<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
		<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
		<input type="hidden" id="standard_template_type" name="standard_template_type" value="<?php if (getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '') != '') { echo getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], ''); } else { echo 'Default'; } ?>">

		<div class="fieldgroup" style="border-bottom:0;padding-top:10px;padding-bottom:10px;">
			<h3 class="blue-header" style="margin-top: 60px;margin-bottom:0;">Select how you would like to configure your assets</h3>
		</div>

		<div class="fieldgroup" style="border-bottom:0;">
			<div class="control-group">
				<div class="row">
					<div id="template-default" class="template-type-selector gray-selector pull-left span6<?php if($bln_edit && $bln_default){ echo ' active'; }else if (strtolower(getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '')) == 'default') { echo ' active'; } ?>">
						<div style="line-height:19px;font-size:15px;">
							<h4>Default</h4>
							The default configuration will have preselected modules and condensed field entry to simplify the asset creation process. Choose this option if you want a quick, simple setup.
						</div>
					</div>
					<div id="template-custom" class="template-type-selector gray-selector pull-left span6<?php if($bln_edit && $bln_custom){ echo ' active'; }else if (strtolower(getFieldValue('standard_template_type', $_SESSION['clone_id'], $_SESSION['request_type'], '')) == 'custom') { echo ' active'; } ?>">
						<div style="line-height:19px;font-size:15px;">
							<h4>Custom</h4>
							Select the custom configuration if you wish to fully configure the setup and layout of your tactic's assets. Choose this option if you want full control over all aspects of your emails and landing pages.
						</div>
					</div>
				</div>

				<div class="row">
					<p style="font-size:18px;color:#666;text-align:center;margin-top:40px;">Note: Changing between Default and Custom modes will reset your configuration.</p>
				</div>
			</div>
		</div>

	</form>

</div>
	
	<?php /*
		<div class="header-row">
			<div class="container-fluid">
				<header style="width:auto;">
					<div class="header-right">
						<?php if($bln_edit && $bln_default){
							echo '<a href="templates-default?type=webinar&edit=1&req_id=' . $bln_edit .  '" id="submit-template-type-form" class="btn-green submit">Save &amp; Continue</a>';
						}else if($bln_edit && $bln_custom){
							echo '<a href="templates-custom?type=webinar&edit=1&req_id=' . $bln_edit .  '" id="submit-template-type-form" class="btn-green submit">Save &amp; Continue</a>';
						}else{
							echo '<a href="#" id="submit-request-form" class="btn-green submit">Save &amp; Continue</a>';
						} ?>
					</div>
				</header>
			</div>
		</div>
	*/ ?>

<?php include('includes/footer.php'); ?>
