<?php
$loc = 'solutions';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
                <h1>Brightcove Test</h1>
            </div>
        </div>
    </div>

    <div class="container">
    	<div class="row">
            <div class="intro span12">
                <!-- Start of Brightcove Player --><div style="display:none">Testing a solution for Marketing Automation. Mike L B, Oct 2014. </div><!--By use of this code snippet, I agree to the Brightcove Publisher T and C found at https://accounts.brightcove.com/en/terms-and-conditions/. --><script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script><object id="myExperience" class="BrightcoveExperience">  <param name="bgcolor" value="#FFFFFF" />  <param name="width" value="588" />  <param name="height" value="410" />  <param name="playerID" value="3830470165001" />  <param name="playerKey" value="AQ~~,AAAAqwZd9wk~,X1Exj3sUi-0vO060dbbtoulpTmgnAzJC" />  <param name="isVid" value="true" />  <param name="isUI" value="true" />  <param name="dynamicStreaming" value="true" />  </object><!-- This script tag will cause the Brightcove Players defined above it to be created as soonas the line is read by the browser. If you wish to have the player instantiated only afterthe rest of the HTML is processed and the page load is complete, remove the line.--><script type="text/javascript">brightcove.createExperiences();</script><!-- End of Brightcove Player -->
            </div>
        </div>
    </div>
    
<?php include('includes/footer.php'); ?>