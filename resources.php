<?php
$loc = 'resources';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

/*
Resource Type's
1 = Video
2 = Link
3 = PDF
4 = Template
*/

$query = "SELECT resources.*,
                        DATE_FORMAT(resources.publish_date, '%m/%d/%Y') AS date_cleaned,
                        resource_type.name AS resource_type_name
                    FROM resources
                    LEFT OUTER JOIN resource_type
                    ON resources.type = resource_type.id
                    WHERE resources.active = 1 ORDER BY resources.publish_date ASC";
$result = $mysqli->query($query);
$result_a = $mysqli->query($query);
$row_cnt = $result->num_rows;

$query_webinar = "SELECT resources.*,
                        DATE_FORMAT(resources.publish_date, '%m/%d/%Y') AS date_cleaned,
                        resource_type.name AS resource_type_name
                    FROM resources
                    LEFT OUTER JOIN resource_type
                    ON resources.type = resource_type.id
                    WHERE resources.active = 1 AND resources.audience = 'Webinar' ORDER BY resources.publish_date ASC";
$result_webinar = $mysqli->query($query_webinar);
$row_cnt_webinar = $result_webinar->num_rows;

$query_automation = "SELECT resources.*,
                        DATE_FORMAT(resources.publish_date, '%m/%d/%Y') AS date_cleaned,
                        resource_type.name AS resource_type_name
                    FROM resources
                    LEFT OUTER JOIN resource_type
                    ON resources.type = resource_type.id
                    WHERE resources.active = 1 AND resources.locations IN ('7') ORDER BY resources.publish_date ASC";
$result_automation = $mysqli->query($query_automation);
$row_cnt_automation = $result_automation->num_rows;
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
                <h1 class="tabbed-header">Resources:&nbsp;&nbsp;&nbsp;<?php if(in_array(1, $_SESSION['access_control'])) { ?><a href="#tabbed-webinars" class="active">Webinars</a><?php if (count($_SESSION['access_control']) > 1) { ?> <span>|</span> <?php } ?><?php } ?><?php if(in_array(2, $_SESSION['access_control'])) { ?><a href="#tabbed-marketing">Marketing Automation</a><?php } ?></h1>
                <p>Use the resources below to help you create successful webinars and marketing automation campaigns.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
        	<div class="span12">
        		<h2 class="tabbed-header" style="margin-bottom:16px;"><span>All Resources</span></h2>
        	</div>

            <?php if(in_array(1, $_SESSION['access_control'])) { ?>

            <div id="tabbed-webinars" class="tabbed-contents span12" style="margin-bottom:60px;">
            	<table class="table table-striped table-bordered table-hover tablesorter requests-table">
            		<thead>
            			<th>Publish Date <b class="icon-white"></b></th>
                        <th>Title <b class="icon-white"></b></th>
                        <th>Action <b class="icon-white"></b></th>
                        <th>Type <b class="icon-white"></b></th>
            		</thead>
            		<tbody>
                        <?php
                        if ($row_cnt > 0) {
                            while ($obj = $result->fetch_object()) {
                                $locations = explode(", ", $obj->locations);

                                if (in_array(6, $locations)) {
                                    //Clean the name for popup
                                    $title_clean = preg_replace('/\s*/', '', $obj->title);
                                    $title_clean = strtolower($title_clean);

                                    echo '<tr>
                                        <td>'.$obj->date_cleaned.'</td>
                                        <td>'.$obj->title.'</td>
                                        <td>';
                                        switch ($obj->type) {
                                            case 2:
                                                //echo '<a href="'.$obj->link.'"'; if ($obj->internal != 1) { echo ' target="_blank"'; } echo '>View</a>';
                                                echo '<a href="'.$obj->link.'" target="_blank">View</a>';
                                                break;
                                            case 3:
                                                echo '<a href="'.$obj->link.'" target="_blank">Download</a>';
                                                break;
                                            case 4:
                                                echo '<a href="'.$obj->link.'" target="_blank">Download</a>';
                                                break;
                                            case 5:
                                                echo '<a href="'.$obj->link.'" target="_blank">Download</a>';
                                                break;
                                            case 'Video_old':
                                                echo '<a href="'.$obj->link.'" target="_blank">Watch</a>';
                                                break;
                                            case 1:
                                                if ($obj->internal == 1) {
                                                    echo '<a class="vimeo cboxElement video-link" href="'.$obj->link.'" title="'.$obj->title.'">Watch</a>';
                                                } else {
                                                    echo '<a href="'.$obj->link.'" target="_blank">Watch</a>';
                                                }
                                                break;
                                            case 'Video_future':
                                                echo '<a href="#'.$title_clean.'" data-toggle="modal">Watch</a>';
                                                echo '<script type="text/javascript">
                                                        $(".modal-backdrop, #'.$title_clean.' .close, #'.$title_clean.' .btn").live("click", function() {
                                                            $("#'.$title_clean.' iframe").attr("src", $("#'.$title_clean.' iframe").attr("src"));
                                                        });
                                                        </script>';
                                                echo '<div id="'.$title_clean.'" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" style="width:900px;"><div class="modal-body"><iframe src="//player.vimeo.com/video/47534696?title=0&amp;byline=0&amp;portrait=0&amp;color=020303" width="900" height="506" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></div>';
                                                break;
                                        }
                                        echo '</td>
                                        <td>'.$obj->resource_type_name.'</td>
                                    </tr>';
                                }
                            }
                        }
                        ?>
            		</tbody>
            	</table>
            </div>

            <?php } ?>

            <?php if(in_array(2, $_SESSION['access_control'])) { ?>

            <div id="tabbed-marketing" class="tabbed-contents span12" style="margin-bottom:60px;">
            	<table class="table table-striped table-bordered table-hover tablesorter requests-table">
                    <thead>
                        <th>Publish Date <b class="icon-white"></b></th>
                        <th>Title <b class="icon-white"></b></th>
                        <th>Action <b class="icon-white"></b></th>
                        <th>Type <b class="icon-white"></b></th>
                    </thead>
                    <tbody>
                        <?php
                        if ($row_cnt > 0) {
                            while ($obj_a = $result_a->fetch_object()) {
                                $locations_a = explode(", ", $obj_a->locations);

                                if (in_array(7, $locations_a)) {
                                    echo '<tr>
                                        <td>'.$obj_a->date_cleaned.'</td>
                                        <td>'.$obj_a->title.'</td>
                                        <td>';
                                        switch ($obj_a->type) {
                                            case 2:
                                                echo '<a href="'.$obj_a->link.'" target="_blank">View</a>';
                                                break;
                                            case 3:
                                                echo '<a href="'.$obj_a->link.'" target="_blank">Download</a>';
                                                break;
                                            case 4:
                                                echo '<a href="'.$obj_a->link.'" target="_blank">Download</a>';
                                                break;
                                            case 5:
                                                echo '<a href="'.$obj_a->link.'" target="_blank">Download</a>';
                                                break;
                                            case 1:
                                                if ($obj_a->internal == 1) {
                                                    echo '<a class="vimeo cboxElement video-link" href="'.$obj_a->link.'" title="'.$obj_a->title.'">Watch</a>';
                                                } else {
                                                    echo '<a href="'.$obj_a->link.'" target="_blank">Watch</a>';
                                                }
                                                break;
                                        }
                                        echo '</td>
                                        <td>'.$obj_a->resource_type_name.'</td>
                                    </tr>';
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <?php } ?>

        </div>
    </div>

<?php include('includes/footer.php'); ?>