<?php
$loc = 'campaigns';
include('includes/head.php');
include('includes/header.php');

require_once('includes/_globals.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
            	<h1>Campaigns</h1>
                <p>Need text here.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="intro span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
			                req.*,
			                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
			                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
			                status.value AS status_value,
			                request_type.request_type AS request_type,
			                request_type.id AS request_type_id,
			                users.fname AS fname,
			                users.lname AS lname,
			                users.email AS email,
			                request_campaign.name AS campaign_name,
			                DATE_FORMAT( request_campaign.start_date,  '%m/%d/%Y' ) AS start_date,
			                DATE_FORMAT( request_campaign.end_date,  '%m/%d/%Y' ) AS end_date
			            FROM request AS req
			            INNER JOIN status on req.status = status.id
			            INNER JOIN request_type on req.request_type = request_type.id
			            INNER JOIN users on req.user_id = users.id
			            INNER JOIN request_campaign on req.id = request_campaign.request_id
			            WHERE req.request_type = 8
			            AND req.active = 1
			            AND request_campaign.approved = 1
			            ORDER BY req.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			//print_r($query);

    			if ($row_cnt > 0) {
			        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
			        echo '<thead><th style="width:114px;">Campaign Name <b class="icon-white"></b></th><th>Tactics <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th></thead>';
			        echo '<tbody>';
			        while ($obj = $result->fetch_object()) {
			            $status_trimmed = "New";
			            switch ($obj->status) {
			                case 1:
			                    $status_trimmed = "New";
			                    break;
			                case 2:
			                    $status_trimmed = "In review";
			                    break;
			                case 3:
			                    $status_trimmed = "Scheduled";
			                    break;
			                case 4:
			                    $status_trimmed = "Complete";
			                    break;
			            }

			            //Let's get all of our related tactics
			            /*
			            webinar = 1
			            enurture = 2
			            newsletter = 5
			            email = 6
			            microsite = 7
			            */
			            $tactics = '<table width="100%"><tbody>';

			            $query_microsite = "SELECT
				            					request_microsite.name AS microsite_name,
				            					DATE_FORMAT(request_microsite.start_date, '%m/%d/%Y') AS microsite_date
				            				FROM request AS req
				            				INNER JOIN request_microsite on req.id = request_microsite.request_id
				            				WHERE req.campaign_id = ".$obj->id."
				            				AND req.active = 1
				            				ORDER BY req.date_created DESC";
			            $result_microsite = $mysqli->query($query_microsite);
    					$row_cnt_microsite = $result_microsite->num_rows;

    					//print_r($query_microsite);

    					if ($row_cnt_microsite > 0) {
    						while ($obj_tactics = $result_microsite->fetch_object()) {
    							$tactics.='<tr><td>'.$obj_tactics->microsite_name.'</td><td>'.$obj_tactics->microsite_date.'</td></tr>';
    						}
    					} else {
    						$tactics.='<tr><td>nothing</td></tr>';
    					}

    					$tactics.='</tbody></table>';

			            echo '<tr>';
			            echo '<td><a href="campaign-detail?id='.$obj->id.'" class="notrack">'.$obj->campaign_name.' (#'.$obj->id.')</a></td>';
			            echo '<td></td>';
			            //echo '<td>'.$obj->fname.' '.$obj->lname.'</td>';
			            echo '<td>'.$obj->request_type.'</td>';
			            echo '<td style="white-space:nowrap;"><div style="display:none;">'.$obj->status.'</div>'.$status_trimmed.'</td>';
			            echo '</tr>';
			        }
			        echo '</tbody></table>';
			    } else {
			        echo '<p><strong>There are currently no active campaigns.</strong></p>';
			    }

            	?>
            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>