<?php
include('../includes/_globals.php');

//STEP 1. Grab all the current Contact/Lead Lists that are active and are > 30 days old.
$query = "SELECT *
		FROM assets
		WHERE active = 1
		AND (type = 'Contacts' OR type = 'Leads')
		AND DATE(date_created) < DATE_SUB(NOW(), INTERVAL 1 MONTH)";
$result = $mysqli->query($query);
$row_cnt = $result->num_rows;

//File names for the email
$filenames = 'testing';

if ($row_cnt > 0) {
	while ($obj = $result->fetch_object()) {
		echo $obj->date_created.': '.$obj->date_created.'<br />';

		$filenames.=$obj->name.'<br>';

		/*

		//Make the asset inactive and delete the file
		$query_inactive = "UPDATE assets SET active = '0' WHERE id = ".$obj->id;
        $mysqli->query($query_inactive);
        unlink($abspath.$obj->path); //Delete file

        //Adding a special record to show what was deleted (primarily for legal notice on frontend)
		$query_delete = "INSERT INTO assets_deleted (id, asset_id, request_id, name, path, type, date_deleted) VALUES (NULL, '".$obj->id."', '".$obj->request_id."', '".$obj->name."', '".$obj->path."', '".$obj->type."', NOW())";
        $mysqli->query($query_delete);
		
		*/

	}

	//Let's send the admin an email
	$message = $message_header;

    $message.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following Contact/Lead lists have been removed from the Intel Customer Connect portal in compliance with Intel privacy rules:<br><br>".$filenames."</p>";

    $message.=$message_footer;

    $mail->AddAddress($email_ms);
    $mail->Subject = 'Intel Customer Connect: Contact/Lead lists have been removed';
    $mail->MsgHTML($message);
    if ($filenames != '') {
        $mail->Send();
    }
}

?>