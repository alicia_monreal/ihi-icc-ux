//Fix for <IE9 not supporting indexOf
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
      if ( this === undefined || this === null ) {
        throw new TypeError( '"this" is null or not defined' );
      }

      var length = this.length >>> 0; // Hack to convert object.length to a UInt32

      fromIndex = +fromIndex || 0;

      if (Math.abs(fromIndex) === Infinity) {
        fromIndex = 0;
      }

      if (fromIndex < 0) {
        fromIndex += length;
        if (fromIndex < 0) {
          fromIndex = 0;
        }
      }

      for (;fromIndex < length; fromIndex++) {
        if (this[fromIndex] === searchElement) {
          return fromIndex;
        }
      }

      return -1;
    };
  }

$(document).ready(function() {
	var theUrl = '/api/get-events.php';
	var theTacticType = ["all"];
	var theGeo = ["all"];
	var theSector = ["all"];
	var theCampaign = 'all';
	var theOwner = ["all"];

	//When you click a filter, we update the url
	/*$('ul.filter-list li').click(function(event){
		if ($(this).hasClass('active')) {
		    $(this).removeClass('active');
		} else {
		    $(this).addClass('active');
		}

		if ($(this).hasClass('all')) {
		    $(this).addClass('active');
		} else {
		    $('li:first-child', $(this).parents('ul')).removeClass('active');
		}

	});
	*/

	//Tactic Type
	$('ul#tactic_type li a').click(function(event){
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
		var type = $(this).attr('data-type');
		var all = theTacticType.indexOf('all');
		var index = theTacticType.indexOf(type);

		if (type == 'all' || theTacticType.length == 0) {
			theTacticType = ["all"];
			$('ul#tactic_type li').removeClass('active');
			$('ul#tactic_type li.all').addClass('active');
		} else {
			//Remove All
			if (all > -1) { theTacticType.splice(all, 1); }

			//Either remove or add
			if (index > -1) { //Remove
				theTacticType.splice(index, 1);
				$(this).parent().removeClass('active');
				if (theTacticType.length == 0) {
					theTacticType = ["all"];
					$('ul#tactic_type li').removeClass('active');
					$('ul#tactic_type li.all').addClass('active');
					//alert('no more');
				}
			} else {
				theTacticType.push(type); //Add
				$(this).parent().addClass('active');
				$('ul#tactic_type li.all').removeClass('active');
			}
		}

		var oCal = $("#calendar").fullCalendar('getDate');
		$('#calendar').fullCalendar('destroy');
		renderCalendar(oCal.format("YYYY-MM-DDTHH:mm:ssZZ"));
	});
	//GEO
	$('ul#geo li a').click(function(event){
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
		var geo = $(this).attr('data-geo');
		var all = theGeo.indexOf('all');
		var index = theGeo.indexOf(geo);

		if (geo == 'all' || theGeo.length == 0) {
			theGeo = ["all"];
			$('ul#geo li').removeClass('active');
			$('ul#geo li.all').addClass('active');
		} else {
			//Remove All
			if (all > -1) { theGeo.splice(all, 1); }

			//Either remove or add
			if (index > -1) { //Remove
				theGeo.splice(index, 1);
				$(this).parent().removeClass('active');
				if (theGeo.length == 0) {
					theGeo = ["all"];
					$('ul#geo li').removeClass('active');
					$('ul#geo li.all').addClass('active');
				}
			} else {
				theGeo.push(geo); //Add
				$(this).parent().addClass('active');
				$('ul#geo li.all').removeClass('active');
			}
		}

		//alert(theGeo);
		var oCal = $("#calendar").fullCalendar('getDate');
		$('#calendar').fullCalendar('destroy');
		renderCalendar(oCal.format("YYYY-MM-DDTHH:mm:ssZZ"));

	});
	//Sector
	$('ul#sector li a').click(function(event){
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
		var sector = $(this).attr('data-sector');
		var all = theSector.indexOf('all');
		var index = theSector.indexOf(sector);

		if (sector == 'all' || theSector.length == 0) {
			theSector = ["all"];
			$('ul#sector li').removeClass('active');
			$('ul#sector li.all').addClass('active');
		} else {
			//Remove All
			if (all > -1) { theSector.splice(all, 1); }

			//Either remove or add
			if (index > -1) { //Remove
				theSector.splice(index, 1);
				$(this).parent().removeClass('active');
				if (theSector.length == 0) {
					theSector = ["all"];
					$('ul#sector li').removeClass('active');
					$('ul#sector li.all').addClass('active');
				}
			} else {
				theSector.push(sector); //Add
				$(this).parent().addClass('active');
				$('ul#sector li.all').removeClass('active');
			}
		}

		var oCal = $("#calendar").fullCalendar('getDate');
		$('#calendar').fullCalendar('destroy');
		renderCalendar(oCal.format("YYYY-MM-DDTHH:mm:ssZZ"));
	});
	//Owner
	$('ul#owner li a').click(function(event){
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
		var owner = $(this).attr('data-owner');
		var all = theOwner.indexOf('all');
		var index = theOwner.indexOf(owner);

		if (owner == 'all' || theOwner.length == 0) {
			theOwner = ["all"];
			$('ul#owner li').removeClass('active');
			$('ul#owner li.all').addClass('active');
		} else {
			//Remove All
			if (all > -1) { theOwner.splice(all, 1); }

			//Either remove or add
			if (index > -1) { //Remove
				theOwner.splice(index, 1);
				$(this).parent().removeClass('active');
				if (theOwner.length == 0) {
					theOwner = ["all"];
					$('ul#owner li').removeClass('active');
					$('ul#owner li.all').addClass('active');
				}
			} else {
				theOwner.push(owner); //Add
				$(this).parent().addClass('active');
				$('ul#owner li.all').removeClass('active');
			}
		}

		var oCal = $("#calendar").fullCalendar('getDate');
		$('#calendar').fullCalendar('destroy');
		renderCalendar(oCal.format("YYYY-MM-DDTHH:mm:ssZZ"));
	});
	//Possible drop down
	$('#campaign_id').on('change', function() {
		theCampaign = this.value || false;

		var oCal = $("#calendar").fullCalendar('getDate');
		$('#calendar').fullCalendar('destroy');
		renderCalendar(oCal.format("YYYY-MM-DDTHH:mm:ssZZ"));
	});

	function renderCalendar(dtrDate) {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: (dtrDate !=undefined) ? dtrDate : moment().format("YYYY-MM-DDTHH:mm:ssZZ"),
			editable: false,
			events: {
				url: theUrl,
				type: 'GET',
				data: {
		            tactic_type: theTacticType,
		            geo: theGeo,
		            sector: theSector,
		            campaign: theCampaign,
		            owner: theOwner
		        }
			},
			loading: function(bool) {
				//$('#loading').toggle(bool);
			},
			eventAfterAllRender: function(){

				$('.popover-calendar').popover({
					'html':false,
			        'trigger': 'hover',
			        'placement': 'top',
			        'delay': { show: 300, hide: 100 }
			    });
			}
		});
	}

	renderCalendar();

});
