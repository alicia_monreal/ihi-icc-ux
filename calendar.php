<?php
$loc = 'calendar';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>
	
	<div class="container" style="border-bottom: 2px solid #dddddd;">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
            	<a href="" id="filter-btn" class="btn-green add">Filter</a>
                <a href="javascript:void(0)" id="placeholder-btn" class="btn-green plain pull-right">Add Placeholder Tactic</a>
                <div id="filters" class="hide">
                	<div class="row">
                		<div class="span3">
                			<h5>Tactic Type</h5>
                			<ul id="tactic_type" class="filter-list">
                				<li class="active all"><a href="" data-type="all">All</a></li>
                				<li><a href="" data-type="webinar">Webinar</a></li>
                				<li><a href="" data-type="newsletter">Newsletter</a></li>
								<li><a href="" data-type="enurture">eNurture</a></li>
                				<li><a href="" data-type="email">Single Email</a></li>
                			</ul>
                		</div>
                		<div class="span3">
                			<h5>GEO</h5>
                			<ul id="geo" class="filter-list">
                				<li class="active all"><a href="" data-geo="all">All</a></li>
                				<li><a href="" data-geo="APJ">APJ</a></li>
                				<li><a href="" data-geo="EMEA">EMEA</a></li>
								<li><a href="" data-geo="LAR">LAR</a></li>
                				<li><a href="" data-geo="NAR">NAR</a></li>
                				<li><a href="" data-geo="PRC">PRC</a></li>
                			</ul>
                		</div>
                		<div class="span3">
                			<h5>Sector</h5>
                			<ul id="sector" class="filter-list">
                				<li class="active all"><a href="" data-sector="all">All</a></li>
                				<li><a href="" data-sector="embedded">Embedded</a></li>
                				<li><a href="" data-sector="itdm">ITDM</a></li>
								<li><a href="" data-sector="channel">Channel</a></li>
                			</ul>
                		</div>
                		<div class="span3">
                			<h5>Campaign</h5>
                			<select id="campaign_id" name="campaign_id" class="input-wide required">
								<option value="all">All campaigns</option>
								<?php
								$campaign_array = array();
			                    $query = "SELECT request_campaign.id, request_campaign.name FROM request_campaign INNER JOIN request on request_campaign.request_id = request.id WHERE request.active = 1 AND DATE(end_date) > DATE(NOW()) AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
			                    $result = $mysqli->query($query);
			                    while ($obj = $result->fetch_object()) {
			                        echo '<option value="'.$obj->id.'">'.$obj->name.'</option>';
			                        $campaign_array[] = $obj->name;
			                    }
			                    ?>
							</select>

                            <h5>My Requests</h5>
                            <ul id="owner" class="filter-list">
                                <li class="active all"><a href="" data-owner="all">All requests</a></li>
                                <li><a href="" data-owner="mine">Show only my requests</a></li>
                            </ul>
                		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>

    <p>&nbsp;</p>

    <div class="container">
        <div class="row" style="margin-top:0px;margin-bottom:20px;">
            <div class="intro span3">
                <div class="cal-legend"><div class="cal-legend-block cal-webinar"></div> Webinar</div>
            </div>
            <div class="intro span3">
                <div class="cal-legend"><div class="cal-legend-block cal-newsletter"></div> Newsletter</div>
            </div>
            <div class="intro span3">
                <div class="cal-legend"><div class="cal-legend-block cal-enurture"></div> eNurture</div>
            </div>
            <div class="intro span3">
                <div class="cal-legend"><div class="cal-legend-block cal-email"></div> Single Email</div>
            </div>
        </div>
    </div>

	<div class="container">
        <div class="row intro-body" style="margin-top:0px;margin-bottom:20px;">
            <div class="intro span12">
            	<div id='loading' style="display:none;">loading...</div>

                <h1></h1>
                <div id="calendar" style="margin-top:32px;"></div>
            </div>
        </div>
    </div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>

	<!--Modals-->
	<div class="modal hide fade" id="modal">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3 style="font-size:26px;">Event Detail</h3>
	    </div>
	    <div id="calendar-detail-body" class="modal-body" style="height: 400px">
	    </div>
	    <div class="modal-footer">
	        <a href="#" data-dismiss="modal" class="btn">Close</a>
	    </div>
	</div>
    <?php  
        /* limited campaigns were shown by default in calendar page, thatswhy a different array to contain all
           the active campaigns is created 
        */
        $campaign_array_popup = array();
        $query = "SELECT request_campaign.id, request_campaign.name 
                  FROM request_campaign 
                  INNER JOIN 
                  request ON request_campaign.request_id = request.id 
                  WHERE request.active = 1 AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
        
        $result = $mysqli->query($query);
        while ($obj = $result->fetch_object()) {
            $campaign_array_popup[] = $obj->name; // all campaigns loaded into campaign_array_popup from calendar page
        }
    ?>

<?php include('includes/footer.php'); ?>