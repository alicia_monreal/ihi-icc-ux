<?php
$loc = 'requests';
include('includes/head.php');
include('includes/header.php');

require_once('includes/_globals.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
            	<h1>Requests</h1>
                <p>Use this page to manage and track your campaign requests. Click a request ID to see more details. After your campaign is complete, click analytics to see real-time statistics about your campaign.</p>
                <?php if ($_SESSION['admin'] != 1) { echo '<p><a href="requests?showall=true">+ View everybody\'s requests</a></p>'; } ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="intro span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
			                req.*,
			                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
			                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
			                status.value AS status_value,
			                request_type.request_type AS request_type,
			                request_type.id AS request_type_id,
			                users.fname AS fname,
			                users.lname AS lname,
			                users.email AS email
			            FROM request AS req
			            INNER JOIN status on req.status = status.id
			            INNER JOIN request_type on req.request_type = request_type.id
			            INNER JOIN users on req.user_id = users.id";

			    //This would be the All Requests view for every user. So losing this permission for now
			    if ($_SESSION['admin'] == 1 || isset($_GET['showall'])) {
			        $query.=" WHERE req.active = 1";
			    } else {
			        $query.=" WHERE req.user_id = '".$_SESSION['user_id']."' AND req.active = 1";
			    }

			    //$query.=" WHERE req.active = 1";

			    $query.=" ORDER BY req.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			//print_r($query);

    			if ($row_cnt > 0) {
			        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
			        echo '<thead><th style="width:114px;">Request Created <b class="icon-white"></b></th><th>Request Title <b class="icon-white"></b></th><th>Owner <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th><th style="width:66px;">Analytics</th><!--<th>Rating <b class="icon-white"></b></th>--></thead>';
			        echo '<tbody>';
			        while ($obj = $result->fetch_object()) {
			            //Rating
			            $query_sur = "SELECT average FROM surveys WHERE request_id = '".$obj->id."' LIMIT 1";
			            $result_sur = $mysqli->query($query_sur);
			            $row_cnt_sur = $result_sur->num_rows;

			            if ($row_cnt_sur > 0) { //Survey has been submitted
			                while ($obj_sur = $result_sur->fetch_object()) {
			                    $survey_average = '<div style="display:none;">'.round($obj_sur->average, 0).'</div><div class="star-rating">';

			                    $cnt_sur = 1;
			                    for ($i = 1; $i <= 5; $i++) {
			                        $survey_average.='<s';
			                        if (round($obj_sur->average, 0) >= $cnt_sur) {
			                            $survey_average.=' class="rated"';
			                        }
			                        $survey_average.='>';
			                        $cnt_sur++;
			                    }

			                    //$survey_average = round($obj_sur->average, 0);
			                    $survey_average.='</s></s></s></s></s></div>';
			                }
			            } else {
			                if ($obj->status_value == "Request complete") {
			                    $survey_average = '<div style="display:none;">0</div><a href="survey?id='.$obj->id.'">Rate this</a>';
			                } else {
			                    $survey_average = '<div style="display:none;">0</div>--';
			                }
			            }

			            $status_trimmed = "New";
			            switch ($obj->status) {
			                case 1:
			                    $status_trimmed = "New";
			                    break;
			                case 2:
			                    $status_trimmed = "In review";
			                    break;
			                case 3:
			                    $status_trimmed = "Scheduled";
			                    break;
			                case 4:
			                    $status_trimmed = "Complete";
			                    break;
			            }

			            //Analytics URL
			            $analytics = '';
			            if ($obj->on24_analytics_url != '' || $obj->eloqua_analytics_url != '') {
			                if ($obj->on24_analytics_url != '') {
			                    $parsed_24 = parse_url($obj->on24_analytics_url);
			                    if (empty($parsed_24['scheme'])) { $url_24 = 'http://' . ltrim($obj->on24_analytics_url, '/'); } else { $url_24 = $obj->on24_analytics_url; }
			                    $analytics.='<a href="'.$url_24.'" target="_blank">On24</a><br />';
			                }
			                if ($obj->eloqua_analytics_url != '') {
			                    $parsed_elo = parse_url($obj->eloqua_analytics_url);
			                    if (empty($parsed_elo['scheme'])) { $url_elo = 'http://' . ltrim($obj->eloqua_analytics_url, '/'); } else { $url_elo = $obj->eloqua_analytics_url; }
			                    $analytics.='<a href="'.$url_elo.'" target="_blank">Eloqua</a><br />';
			                }
			                if ($obj->regpage_url != '') {
			                    $parsed_regpage = parse_url($obj->regpage_url);
			                    if (empty($parsed_regpage['scheme'])) { $url_regpage = 'http://' . ltrim($obj->regpage_url, '/'); } else { $url_regpage = $obj->regpage_url; }
			                    $analytics.='<a href="'.$url_regpage.'" target="_blank">Reg Page</a>';
			                }
			            } else {
			                $analytics.='None';
			            }

			            //NEED TITLE LOGIC SINCE THEY'RE ALL DIFFERENT
			            $title = '';
			            if ($obj->request_type_id != 8) { //Anything but a Campaign ID... for now
			            	//Webinar
			            	if ($obj->request_type_id == 7) {
			            		$query_title = "SELECT microsite_name FROM request_microsite WHERE request_id = '".$obj->id."' LIMIT 1";
			            		$result_title = $mysqli->query($query_title);
							    while ($obj_title = $result_title->fetch_object()) { $title = $obj_title->microsite_name; }
								$result_title->close();
							}

			            }

			            echo '<tr>';
			            echo '<td>'.$obj->date_created.'</td>';
			            echo '<td><a href="request-detail?id='.$obj->id.'" class="notrack">'.$title.' (#'.$obj->id.')</a></td>';
			            echo '<td>'.$obj->fname.' '.$obj->lname.'</td>';
			            echo '<td>'.$obj->request_type.'</td>';
			            echo '<td style="white-space:nowrap;"><div style="display:none;">'.$obj->status.'</div>'.$status_trimmed.'</td>';
			            echo '<td>'.$analytics.'</td>';
			            //echo '<td>'.$survey_average.'</td>';
			            echo '</tr>';
			        }
			        echo '</tbody></table>';
			    } else {
			        echo '<p><strong>You currently have no active requests.</strong></p>';
			    }

            	?>
            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>