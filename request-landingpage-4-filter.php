<?php
$loc = 'campaign-requests';
$step = 4;
$c_type = 'landingpage';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Landing Page Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fourup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li>Step 3</li>
				<li class="active">Step 4</li>
			</ul>
			<div class="request-step step4">
				<div class="fieldgroup">
					<p>Please provide your target audience segmentation by uploading a list and/or describing a segmentation from existing users.</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p><p>Click on the <i class="icon-question-sign" style="margin-top: 3px;"></i> for more information and examples.</p>
					<h3 class="fieldgroup-header">Audience Member Details</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_landingpage.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="4">
					<input type="hidden" id="c_type" name="c_type" value="landingpage">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">

					<div class="fieldgroup">
						<div class="control-group">
							<label for="find_existing_contact_list" class="control-label">Find an Existing Contact List</label>
							<div class="controls">
								<?php
				                $query = "SELECT
							                req.*,
							                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
							                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
							                status.value AS status_value,
							                request_type.request_type AS request_type,
							                request_type.id AS request_type_id,
							                users.fname AS fname,
							                users.lname AS lname,
							                users.email AS email,
							                request_campaign.geo,
							                request_campaign.sector,
							                request_campaign.subsector,
							                request_campaign.subsector_other
							            FROM request AS req
							            INNER JOIN status on req.status = status.id
							            INNER JOIN request_type on req.request_type = request_type.id
							            INNER JOIN users on req.user_id = users.id
							            INNER JOIN request_campaign on req.campaign_id = request_campaign.id
							            WHERE req.active = 1
							            AND req.request_type = 4 OR req.request_type = 9
							            ORDER BY req.id DESC";
				                
				    			$result = $mysqli->query($query);
				    			$row_cnt = $result->num_rows;

				    			if ($row_cnt > 0) {
				    				/*
							        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
							        echo '<thead><th>&nbsp;</th><th>Eloqua Source ID <b class="icon-white"></b></th><th>GEO <b class="icon-white"></b></th><th>Sector <b class="icon-white"></b></th><th>Subsector <b class="icon-white"></b></th></thead>';
							        echo '<thead><tr>
												<td rowspan="1" colspan="1">&nbsp;</td>
												<td rowspan="1" colspan="1"><input type="text" name="search_eloqua" placeholder="Eloqua Source ID" class="search_init"></td>
												<td rowspan="1" colspan="1"><input type="text" name="search_geo" placeholder="GEO" class="search_init"></td>
												<td rowspan="1" colspan="1"><input type="text" name="search_sector" placeholder="Sector" class="search_init"></td>
												<td rowspan="1" colspan="1"><input type="text" name="search_subsector" placeholder="Subsector" class="search_init"></td>
											</tr></thead>';
							        echo '<tbody>';
							        while ($obj = $result->fetch_object()) {
							            echo '<tr>';
							            echo '<td><input type="checkbox" name="contactlists[]" value="'.$obj->id.'"></td>';
							            echo '<td>'.getEloquaSourceId($obj->id, $obj->request_type_id).'</td>';
							            echo '<td>'.$obj->geo.'</td>';
							            echo '<td>'.$obj->sector.'</td>';
							            echo '<td>'.$obj->subsector.'</td>';
							            echo '</tr>';
							        }
							        
							        echo '</tbody></table>';
							        */

							        

							        echo '<div id="contact-list-filterable-placeholder"></div>';
							        
							        echo '<ul id="contact-list-filterable" class="filterable-list">';
							        while ($obj = $result->fetch_object()) {
							        	if ($obj->user_id == $_SESSION['user_id']) { $owner = 'Myself'; } else { $owner = 'Other'; }

							            echo '<li data-subsector="'.$obj->subsector.'" data-sector="'.$obj->sector.'" data-geo="'.$obj->geo.'" data-owner="'.$owner.'"><label class="checkbox inline"> <input type="checkbox" name="contactlists[]" value="'.getEloquaSourceId($obj->id, $obj->request_type_id).'" class="contactlist-checkbox">'.getEloquaSourceId($obj->id, $obj->request_type_id).'</label></li>';
							        }
							        echo '</ul>';
									
									/*
							        echo '<select id="contact-list-filterable" name="contactlists[]" multiple>';
							        while ($obj = $result->fetch_object()) {
							            echo '<option value="'.$obj->id.'" data-subsector="'.$obj->subsector.'" data-sector="'.$obj->sector.'" data-geo="'.$obj->geo.'">'.getEloquaSourceId($obj->id, $obj->request_type_id).'</option>';
							        }
							        echo '</select>';
							        */
							    }
				            	?>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<?php include('includes/contactlist-basics.php'); ?>
					</div>
					<?php include('includes/notified_users.php'); ?>
					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-landingpage-3" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Landing Page Request: Step 4">Submit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>