<?php

$loc = 'templates';
$step = 1;
$t_type = 'custom';


// if(isset($_GET['legacy']))
// else
    // include('includes/editorhead.php');
include('includes/head.php');
include('includes/header-templates.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
// if (!isset($_SESSION['campaign_id'])) {
    // header('Location:request?error=request-id');
    // echo "Initiate a request";
    // exit();
?>

<div class="container-fluid full-width">
    <div class="row-fluid">
        <!--Sidebar-->
        <div id="tpl-sidebar" class="span3">
            <br/><h4>Newsletter</h4>
            <div class="tpl-micro-select">
                <label><input id="checka" type="checkbox" class="tpl-news-checkbox" data-attr="a"> A</label>
                <label><input id="checkb" type="checkbox" class="tpl-news-checkbox" data-attr="b"> B</label>
            </div>
            <br/><h4>Microsite</h4>
            <div class="tpl-micro-select">
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="header"> Header</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="headerThankyou"> Header Thankyou</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="headerFulfillment"> Header Fulfillment</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="agendaOne"> Agenda One Column</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="agendaTwo"> Agenda Two Column</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="events"> Events Module</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="location"> Location Module</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="register"> Register Module</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="resources"> Resources Module</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="sponsors"> Sponsors Module</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="speakerThree"> Speakers Three Column</label>
                <label><input type="checkbox" class="tpl-micro-checkbox" data-attr="speakerTwo"> Speakers Two Column</label>
            </div>

            <br/><h4>Headers to show</h4>
            <div class="tpl-head-select">
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerA"> Header A</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerB"> Header B</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerC"> Header C</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerD"> Header D</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerE"> Header E</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerF"> Header F</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerG"> Header G</label>
                <label><input type="checkbox" class="tpl-head-checkbox" data-attr="headerH"> Header H</label>
            </div>

            <br/><h4>Modules to show</h4>
            <div class="tpl-head-select">
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="image_sidebar"> Image in Sidebar</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="location_sidebar"> Location in Sidebar</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="offers"> Offers in Content</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="standard_content"> Standard Content</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="standard_cta"> Standard Content with CTA</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="three_speaker"> Speaker Module (3 column)</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="two_speaker"> Speaker Module (2 column)</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="three_agenda"> Agenda Module (3 column)</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="two_agenda"> Agenda Module (2 column)</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="one_agenda"> Agenda Module (standard)</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="location"> Location Module (2 column)</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="event_summary"> Event Summary</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="no_content"> No Content</label>
                <label><input type="checkbox" class="tpl-cont-checkbox" data-attr="intel_badge"> Intel Badge Module</label>
            </div>

            <br/><h4>Select Footer</h4>
            <div class="tpl-head-select">
                <label><input type="checkbox" class="tpl-foot-checkbox" data-attr="standard_foot"> Standard</label>
                <label><input type="checkbox" class="tpl-foot-checkbox" data-attr="badge_foot"> Badge</label>
            </div>
        </div>
        <div id="tpl-body" class="span9 tpl-editor">
            <div id="template-previews-news">
            </div>
            <div id="template-previews-large">
            </div>
            <div id="template-previews">
                <div class="tpl-head"></div>

                <div class="tpl-modules"></div>

                <div class="tpl-foot"></div>
            </div>
        </div>
    </div>

    <div class="header-row">
        <div class="container-fluid">
            <header style="width:auto;">
                <div class="header-right"><a href="" id="tpl-cancel" class="btn-green plain" style="margin-right:6px;">Cancel</a><a href="#" id="tpl-save-continue" class="btn-green submit">Save &amp; Continue</a></div>
            </header>
        </div>
    </div>

</div>






