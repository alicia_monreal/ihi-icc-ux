<?php
$loc = 'survey';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

$ownsthis = false;
$already = false;
$succeed = false;

//If survey doesn't have a request ID attached to it, redirect elsewhere
if (!isset($_GET['id'])) {
	header('Location:/home');
} else {
	//Make sure the current user owns this request and that the request is complete
	$query = "SELECT title FROM requests WHERE id = '".$_GET['id']."' AND status = 4 AND active = 1";
	if ($_SESSION['admin'] != 1) {
		$query.=" AND user_id = '".$_SESSION['user_id']."'"; //Admin sees all
	}
	$query.=" LIMIT 1";
	$result = $mysqli->query($query);
	$row_cnt = $result->num_rows;

	if ($row_cnt > 0) { //User owns this
		$ownsthis = true;
		while ($obj = $result->fetch_object()) {
			$req_title = $obj->title;
		}
	}
}

function generateFormToken($form) {
    $token = md5(uniqid(microtime(), true));  
    $_SESSION[$form.'_token'] = $token; 
    
    return $token;
}
$newToken = generateFormToken('survey-form'); 
?>

	<?php if ($ownsthis == false) { //User doesn't own this request survey ?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:15px;">
			<div class="intro span12">
				<h1>Post-request Completion Survey</h1>
				<h4 class="account-update-form" style="color:#ff0000;">We're sorry, this survey isn't related to a request that you've made or the request is not complete yet.</h4>
			</div>
		</div>
	</div>

	<?php } else { //User DOES own this survey, or this is an admin ?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:15px;">
			<div class="intro span12">
				<h1>Post-request Completion Survey</h1>
				<h4>For your request: <?php echo $req_title.' (#'.$_GET['id'].')'; ?></h4>
				<?php
				if (isset($_GET['success'])) {
					echo '<h4 class="survey-form" style="color:#ff0000;">';
					if ($_GET['success'] == 'true') {
						echo 'Thank you for taking the time to fill out our survey, the results have been sent to our team for review.';
						$succeed = true;
					} else { 
						echo 'Sorry, there was a problem collecting your results. Please try again or contact us for support.';
					}
					echo '</h4>';
				} else {
					//Now make sure this survey hasn't already been submitted
					$query_sur = "SELECT * FROM surveys WHERE request_id = '".$_GET['id']."' LIMIT 1";
					$result_sur = $mysqli->query($query_sur);
					$row_cnt_sur = $result_sur->num_rows;

					if ($row_cnt_sur > 0) { //Survey has already been submitted
						echo '<h4 class="account-update-form" style="color:#ff0000;">The survey for this request has already been submitted. Thank you for your feedback.</h4>';
						$succeed = true;
					} else {
						echo '<p>Thank you for agreeing to take a short survey of your experience with the Customer Connect website. Please answer the following questions, using a 1-5 scale with 1 representing "highly unsatisfied" and 5 representing "highly satisfied".</p>';
					}
				}
				?>
			</div>
		</div>
	</div>

	<?php if ($succeed == false) { ?>

	<div class="container">
		<div id="survey" class="row">
			<form id="survey-form" action="includes/_survey.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
				<input type="hidden" id="request_id" name="request_id" value="<?php echo $_GET['id']; ?>">
				<input type="hidden" name="token" value="<?php echo $newToken; ?>">

				<div class="span12">
					<table class="survey-table">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th width="100">1:<br />Highly unsatisfied</th>
								<th width="100">2:<br />Unsatisfied</th>
								<th width="100">3:<br />Neutral</th>
								<th width="100">4:<br />Satisfied</th>
								<th width="100">5:<br />Highly satisfied</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong>Question 1</strong><br />How satisfied are you with the promptness of our response to your initial request?</td>
								<td align="center"><input type="radio" id="q1[]" name="q1" value="1"></td>
								<td align="center"><input type="radio" id="q1[]" name="q1" value="2"></td>
								<td align="center"><input type="radio" id="q1[]" name="q1" value="3" checked></td>
								<td align="center"><input type="radio" id="q1[]" name="q1" value="4"></td>
								<td align="center"><input type="radio" id="q1[]" name="q1" value="5"></td>
							</tr>
							<tr>
								<td><strong>Question 2</strong><br />How satisfied are you with the timeliness of the published update to the Customer Connect website?</td>
								<td align="center"><input type="radio" id="q2[]" name="q2" value="1"></td>
								<td align="center"><input type="radio" id="q2[]" name="q2" value="2"></td>
								<td align="center"><input type="radio" id="q2[]" name="q2" value="3" checked></td>
								<td align="center"><input type="radio" id="q2[]" name="q2" value="4"></td>
								<td align="center"><input type="radio" id="q2[]" name="q2" value="5"></td>
							</tr>
							<tr>
								<td><strong>Question 3</strong><br />How satisfied are you with the final outcome on the Customer Connect website?</td>
								<td align="center"><input type="radio" id="q3[]" name="q3" value="1"></td>
								<td align="center"><input type="radio" id="q3[]" name="q3" value="2"></td>
								<td align="center"><input type="radio" id="q3[]" name="q3" value="3" checked></td>
								<td align="center"><input type="radio" id="q3[]" name="q3" value="4"></td>
								<td align="center"><input type="radio" id="q3[]" name="q3" value="5"></td>
							</tr>
							<tr>
								<td><strong>Question 4</strong><br />How satisfied are you overall with your experience utilizing Customer Connect to submit and manage requests?</td>
								<td align="center"><input type="radio" id="q4[]" name="q4" value="1"></td>
								<td align="center"><input type="radio" id="q4[]" name="q4" value="2"></td>
								<td align="center"><input type="radio" id="q4[]" name="q4" value="3" checked></td>
								<td align="center"><input type="radio" id="q4[]" name="q4" value="4"></td>
								<td align="center"><input type="radio" id="q4[]" name="q4" value="5"></td>
							</tr>
							<tr>
								<td valign="top"><strong>Question 5</strong><br />Do you have any other comments or suggestions? </td>
								<td colspan="5"><textarea id="comments" name="comments" rows="5" cols="45" class="input-full" style="width:98%;"></textarea></td>
							</tr>
						</tbody>
					</table>
					
					<p class="pull-right" style="margin:0 0 40px 0;"><input type="submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Survey" value="Submit"></p>
				</div>
			</form>
		</div>
	</div>

	<?php } //End Succeed ?>

	<?php } //End ownership ?>

<?php include('includes/footer.php'); ?>