<?php
require_once('includes/_globals.php');
$loc = 'all-requests';
include('includes/head.php');
include('includes/header.php');

//Kill some session vars
killSessionVars();
?>

   <div class="container">
        <div class="row intro-body" style="margin-bottom:30px;">
            <div class="intro span12">
            	<h1>All Requests</h1>
                <p>NEED TEXT</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="intro span12" style="margin-bottom:50px;">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
			                req.*,
			                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
			                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
			                status.value AS status_value,
			                request_type.request_type AS request_type,
			                request_type.id AS request_type_id,
			                users.fname AS fname,
			                users.lname AS lname,
			                users.email AS email
			            FROM request AS req
			            INNER JOIN status on req.status = status.id
			            INNER JOIN request_type on req.request_type = request_type.id
			            INNER JOIN users on req.user_id = users.id
			            WHERE req.active = 1
			            AND req.status IN (1,2,4,5,7,8,9,12)
			            ORDER BY req.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			if ($row_cnt > 0) {
			        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
			        echo '<thead><th>Created <b class="icon-white"></b></th><th>Name <b class="icon-white"></b></th><th>Owner <b class="icon-white"></b></th><th>GEO(s) <b class="icon-white"></b></th><th>Sector <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th></thead>';
			        echo '<thead><tr>
								<td rowspan="1" colspan="1"><input type="text" name="search_created" placeholder="Created" class="search_init" style="width:100px;"></td>
								<td rowspan="1" colspan="1"><input type="text" name="search_name" placeholder="Name" class="search_init"></td>
								<td rowspan="1" colspan="1"><input type="text" name="search_owner" placeholder="Owner" class="search_init"></td>
								<td rowspan="1" colspan="1"><input type="text" name="search_geos" placeholder="GEO(s)" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_sector" placeholder="Sector" class="search_init"></td>
								<td rowspan="1" colspan="1"><input type="text" name="search_type" placeholder="Type" class="search_init"></td>
							</tr></thead>';
			        echo '<tbody>';
			        while ($obj = $result->fetch_object()) {

			            echo '<tr>';
			            echo '<td>'.$obj->date_created.'</td>';
			            if ($obj->request_type_id == 8) {
			            	echo '<td><a href="all-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
			            } else {
			            	echo '<td><a href="all-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
			            }
			            echo '<td>'.$obj->fname.' '.$obj->lname.'</td>'; //Who owns it, the person that made it, or the Owner value
			            if ($obj->request_type_id == 8) {
				            echo '<td>'.getGeoForCampaign($obj->id).'</td>';
	                        echo '<td>'.getSectorForCampaign($obj->id).'</td>';
	                    } else {
	                    	echo '<td>'.getGeoNonArray($obj->id).'</td>';
	                        echo '<td>'.getSector($obj->id).'</td>';
	                    }
			            echo '<td>'.$obj->request_type.'</td>';
			            echo '</tr>';
			        }
			        
			        echo '</tbody></table>';
			    } else {
			        echo '<p><strong>You currently have no active requests.</strong></p>';
			    }

            	?>
            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>