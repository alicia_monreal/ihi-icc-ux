<?php
$loc = 'campaign-requests';
$step = 2;
$c_type = 'leadform';
include('includes/head.php');
include('includes/header.php');
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Create a Lead Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request twoup">
				<li>Step 1</li>
				<li class="active">Step 2</li>
			</ul>
			<div class="request-step step4">
				<div class="fieldgroup">
					<p>We currently do not have Lead Form Requests developed at the moment. Please fill out the contact us form below and production support will reach out to you regarding your request.</p>
					<h3 class="fieldgroup-header">Lead Form Details</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_leadform.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="2">
					<input type="hidden" id="c_type" name="c_type" value="leadform">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<div class="fieldgroup">
						<div class="control-group">
							<label for="tag" class="control-label">What are your requirements?</label>
							<div class="controls">
								<textarea id="message" name="message" class="input-wide textarea-medium required" style="width:660px;height:250px;"></textarea>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Lead Form Request: Step 2">Send Email</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>