<?php
$loc = 'home';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

    <!-- Hero -->
	<div class="hero">
		<div class="container" style="position: relative;">
            <div id="slideshow">
                <div class="slideshow-container">
                    <div class="left">
                        <h1><span style="font-size:32px;">Welcome to</span><br />Intel Customer Connect</h1>
                        <p style="font-size:17px;">Learn about, engage and manage all of your webinars in one place.</p>
                        <p><a href="solutions" class="btn-green" title="Get Started">Get Started</a></p>
                    </div>
                    <div class="right" style="margin-top: 38px;">
                        <img src="img/hero-devices.png" alt="Welcome to the Intel Online Sales Center">
                    </div>
                </div>
                <div class="slideshow-container">
                    <div class="left">
                        <h1 style="margin-top:5px;line-height:38px;"><span style="font-size:28px;">Introducing <strong>NEW</strong> GDC Office Hours</h1>
                        <p style="font-size:16px;color:#464646;line-height:18px;">Learn how to get started creating<br />integrated marketing campaigns.</p>
                        <p style="font-size:24px;color:#464646;">Every Wednesday</p>
                        <div class="pull-left" style="margin-right: 20px;width:130px;">
                            <p style="font-size:20px;margin-bottom: 5px;line-height: 17px;"><strong>ASMO / EMEA</strong><br /><span style="font-size:11px;color:#464646;">8:30-9:00 AM PST</span></p>
                        </div>
                        <div class="pull-left">
                            <p style="margin-top:0px;"><a href="files/Intel  GDC Office Hours AM.ICS" class="btn-green add" title="Add Meeting">Add Meeting</a></p>
                        </div>
                        <div class="pull-left" style="margin-right: 20px;width:130px;clear:both;">
                            <p style="font-size:20px;margin-bottom: 5px;line-height: 17px;"><strong>APAC / PRC</strong><br /><span style="font-size:11px;color:#464646;">4:30-5:00 PM PST</span></p>
                        </div>
                        <div class="pull-left">
                            <p style="margin-top:0px;"><a href="files/Intel  GDC Office Hours PM.ICS" class="btn-green add" title="Add Meeting">Add Meeting</a></p>
                        </div>
                    </div>
                    <div class="right" style="width: 710px;margin-top: 6px;right: -24px;">
                        <img src="img/hero-team.png" alt="Introducing NEW GDC Office Hours">
                    </div>
                </div>
                <div class="slideshow-container">
                    <div class="left">
                        <h1 style="margin-top:64px;line-height:38px;"><span style="font-size:34px;">New Eloqua templates available<br />to use in your campaigns</h1>
                        <p style="margin-top:24px;"><a href="files/Intel Eloqua Usage Guidelines.pdf" class="btn-green" title="Download Guidelines">Download Guidelines</a></p>
                    </div>
                    <div class="right" style="width: 427px;margin-top: 0px;right: 10px;">
                        <img src="img/hero-guidelines.png" alt="New Eloqua templates available to use in your campaigns">
                    </div>
                </div>
                <div class="slideshow-container">
                    <div class="left">
                        <h1 style="margin-top:64px;line-height:38px;"><span style="font-size:36px;">Check out <strong>what's new</strong><br />to Intel Customer Connect</h1>
                        <p style="margin-top:24px;"><a href="release-notes" class="btn-green" title="Check out what's new to Intel Customer Connect">View Release Notes</a></p>
                    </div>
                    <div class="right" style="width: 571px;margin-top: 3px;">
                        <img src="img/hero-updates.png" alt="Check out what's new to Intel Customer Connect">
                    </div>
                </div>
                <!--<div class="slideshow-container">
                    <div class="left">
                        <h1><span style="font-size:32px;">Next On24 Webinar Training is</span><br />December 4th</h1>
                        <p style="font-size:17px;">Learn how to get started creating integrated webinar campaigns.</p>
                        <p><a href="training" data-analytics-label="Get started"><img src="img/btn-get-started.gif" alt="Get started"></a></p>
                    </div>
                    <div class="right" style="margin-top:21px;">
                        <img src="img/hero-on24.png" alt="Next On24 Webinar Training is December 4th">
                    </div>
                </div>-->
            </div>
		</div>
	</div>

    <div class="container">
        <div class="row intro-body">
            <div class="intro span7">
                <p><strong>This site contains everything you need to set up engaging, relevant, and connected webinar and e-nurture campaigns.</strong></p>
                <p>Visit <a href="solutions">Solutions</a> to learn more about what you can do here. Then check out <a href="resources">Resources</a> for training and information about your selected solution.</p>
                <p>When you're ready to set up a campaign, visit <a href="request">Submit Request</a> and fill out and upload the related forms &ndash; and we'll do the rest!</p>

                <?php
                /*
                <div class="trainings">
                    <h1>Upcoming Training</h1>
                    <p>Learn how to use Intel Online Sales resources to your best advantage.</p>
                    <ul>
                        <li><a href="training?eid=5">3/20/14 - Eloqua Onboarding Training</a></li>
                        <li><a href="training?eid=5">3/20/14 - Eloqua Onboarding Training</a></li>
                        <li><a href="training?eid=5">3/20/14 - Eloqua Onboarding Training</a></li>
                    </ul>
                    <a href="training" class="btn-green">View All</a>
                </div>
                */
                ?>

                <div class="trainings">
                    <h1>On-Demand Training</h1>
                    <p>Learn how to use Intel Online Sales resources to your best advantage.</p>
                    <ul>
                        <li><a class="vimeo cboxElement video-link" href="//www.youtube.com/embed/0nyl9OKul8I?rel=0" title="Marketing Automation Onboarding Video">Marketing Automation Onboarding Video</a></li>
                        <li><a class="vimeo cboxElement video-link" href="//www.youtube.com/embed/GD67IItX9TY?rel=0" title="Webinar Hands-on Training Session 5/20">Webinar Hands-on Training Session 5/20</a></li>
                    </ul>
                    <?php /*<a href="training" class="btn-green">View All</a> */ ?>
                    <p>&nbsp;</p>
                </div>
            </div>

            <div class="accordion-menu span4 pull-right">
                <div class="accordion-head"><strong>Resources</strong> - Download templates, take training, and more.<p><a href="resources" class="learn-more-arrow">Learn More</a></p></div>
                <div class="accordion" id="accordion2">
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                        Resources
                      </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                        <?php

                            // home page resources loaded dynamically
                            $query = "SELECT resources.link,resources.title,resources.locations
                                      FROM resources
                                      WHERE resources.active = 1 ORDER BY resources.publish_date ASC";
                            $result = $mysqli->query($query);

                            while ($obj = $result->fetch_object()) {
                                $locations = explode(", ", $obj->locations);

                                if(in_array(1,$locations)) { // location 1 represents home page
                                    echo '<div class="accordion-inner">
                                            <a href="'.$obj->link.'" alt="'.$obj->title.'">'.$obj->title.'</a>
                                          </div>';
                                }
                            }
                        ?>
                      
                    </div>
                  </div>
                </div>

                <div class="capabilities">
                    <h4>Capabilities Roadmap</h4>
                    <p>Please view the capabilities roadmap to see what's coming.</p>
                    <ul>
                        <li><a href="files/Webinar Phases.pptx" target="_blank">Webinar Roadmap</a></li>
                        <li><a href="release-notes">Release Notes</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php
    /*
    <div class="row odd">
    	<div class="container" style="padding-bottom:30px;">
    		<div class="span5 pull-left calendar">
    			<h1>Upcoming Training</h1>
                <p>Learn how to use Intel Online Sales resources to your best advantage.</p>
                <!--<ul>
                    <li><a href="training?eid=5">3/20/14 - Eloqua Onboarding Training</a></li>
                </ul>
                <a href="training" class="btn-green">View All</a>-->
    		</div>
    		<div class="span5 pull-right calendar">
                <h1>Capabilities Roadmap</h1>
                <p>Please view the capabilities roadmap to see what's coming.</p>
                <ul>
                    <li><a href="files/Webinar Phases.pptx" target="_blank">Webinar Roadmap</a></li>
                    <li><a href="release-notes">Release Notes</a></li>
                </ul>
            </div>
    	</div>
    </div>
    */
    ?>

    <?php
    /*
    <div class="row dark-blue">
        <div class="container">
            <div class="span12">
                <div class="span7 pull-left">
                    <h4>Featured Update</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mauris lorem, feugiat et hendrerit a, convallis id urna. Aliquam fringilla urna in neque suscipit ullamcorper eu vitae augue. Quisque dapibus rutrum erat nec euismod. Aliquam eu lacus sit amet erat tempor feugiat. Vivamus eget arcu lorem. Nam eros justo, pretium eu tristique id, sodales ac nisi. Sed aliquet lectus magna, at aliquam ligula. Ut tincidunt tincidunt ultricies.</p>
                    <p><a href="" class="btn-green" data-analytics-label="Featured Update: Learn more">Learn more</a></p>
                </div>
                <div class="pull-right">
                    <a href="" data-analytics-label="Featured Update: Thumbnail"><img src="img/fpo-338.png" alt="Featured Update" class="first"></a>
                </div>
            </div>
        </div>
    </div>
    */
    ?>

<?php include('includes/footer.php'); ?>