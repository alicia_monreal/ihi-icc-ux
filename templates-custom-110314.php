<?php
$loc = 'templates';
$step = 3;
$t_type = 'custom';
ob_start();
include('includes/_globals.php');

//If they're moving from custom to default, or vice versa, need to kill the old data
$switched = false;
if (isset($_SESSION['standard_template_type']) && !empty($_SESSION['standard_template_type'])) {
    if (strtolower($_SESSION['standard_template_type']) == 'default') {
        unset($_SESSION['templates_data']);
        $switched = true;
    }
}

//What type of tactic are we editing (might want to move this out of the URL).
//This is saved in the session as $_SESSION['request_type'], but URL might be better for non-incremental landing
if(isset($_GET['type'])) {
    $tactic_type = $_GET['type'];
} else {
    $tactic_type = $_SESSION['request_type'];
}

//Came from Edit route
if(isset($_GET['edit']) && $_GET['edit'] === '1'){
    $bln_edit   = true;
    $bln_record = true;
    $edit_id    = $_GET['req_id'];

    //Fetch Record
    $query_template  = "SELECT * FROM templates_custom_data WHERE request_id = '".$edit_id."' LIMIT 1";
    $result_template = $mysqli->query($query_template);
    $record = (array)$result_template->fetch_object();

    $record['invitation_email_config']   =json_decode( stripslashes($record['invitation_email_config']));
    $record['reminder_one_email_config'] =json_decode( stripslashes($record['reminder_one_email_config']));
    $record['reminder_two_email_config'] =json_decode( stripslashes($record['reminder_two_email_config']));
    $record['confirmation_email_config'] =json_decode( stripslashes($record['confirmation_email_config']));
    $record['thank_you_email_config']    =json_decode( stripslashes($record['thank_you_email_config']));
    $record['sorry_email_config']        =json_decode( stripslashes($record['sorry_email_config']));
    $record['registration_config']       =json_decode( stripslashes($record['registration_config']));
    $record['confirmation_config']       =json_decode( stripslashes($record['confirmation_config']));
    $record['landing_page_config']       =json_decode( stripslashes($record['landing_page_config']));
    $record['single_email_config']       =json_decode( stripslashes($record['single_email_config']));
    echo '<script>dbRecord = \'' . json_encode($record) . '\'</script>';
}
//Came from Incomplete request route
else if(isset($_SESSION['t_request_id']) && isset($_SESSION['templates_data']) && $switched == false){
    $bln_record = true;
    $bln_edit   = false;

    $record     = $_SESSION['templates_data'];

    $record['invitation_email_config']   =json_decode( stripslashes($record['invitation_email_config']));
    $record['reminder_one_email_config'] =json_decode( stripslashes($record['reminder_one_email_config']));
    $record['reminder_two_email_config'] =json_decode( stripslashes($record['reminder_two_email_config']));
    $record['confirmation_email_config'] =json_decode( stripslashes($record['confirmation_email_config']));
    $record['thank_you_email_config']    =json_decode( stripslashes($record['thank_you_email_config']));
    $record['sorry_email_config']        =json_decode( stripslashes($record['sorry_email_config']));
    $record['registration_config']       =json_decode( stripslashes($record['registration_config']));
    $record['confirmation_config']       =json_decode( stripslashes($record['confirmation_config']));
    $record['landing_page_config']       =json_decode( stripslashes($record['landing_page_config']));
    $record['single_email_config']       =json_decode( stripslashes($record['single_email_config']));
    echo '<script>dbRecord = \'' . json_encode($record) . '\'</script>';


    // $invitation_email_config_mod  = $record['invitation_email_config']->modules->data;
    // $record['invitation_email_config']->modules->data      = '';

    // $reminder_one_email_config_mod  = $record['reminder_one_email_config']->modules->data;
    // $record['reminder_one_email_config']->modules->data      = '';

    // $reminder_two_email_config_mod  = $record['reminder_two_email_config']->modules->data;
    // $record['reminder_two_email_config->modules->data']      = '';

    // $thank_you_email_config_mod  = $record['thank_you_email_config']->modules->data;
    // $record['thank_you_email_config']->modules->data      = '';

    // $sorry_email_config_mod  = $record['sorry_email_config']->modules->data;
    // $record['sorry_email_config']->modules->data      = '';

    // $registration_config_mod  = $record['registration_config']->modules->data;
    // $record['registration_config']->modules->data      = '';

    // $confirmation_config_mod  = $record['confirmation_config']->modules->data;
    // $record['confirmation_config_mod']->modules->data      = '';

    // echo '<script>';
    // echo 'var reminder_one_email_config_mod = "' . $invitation_email_config_mod . '";';
    // echo 'dbRecord = \'' . json_encode($record) . '\'</script>';
//Fresh page
} else {
    //Redirect if a Campaign ID hasn't been set
    if (!isset($_SESSION['campaign_id'])) {
        header('Location:request?error=request-id');
        exit();
    }

    $bln_record = false;
    $bln_edit   = false;
}

include('includes/head.php');
include('includes/header-templates.php');

/*
echo '<pre>';
echo strtolower($_SESSION['standard_template_type']);
print_r($_SESSION['templates_data']);
echo '</pre>';
*/
?>

<div class="container-fluid full-width">
    <div class="row-fluid col-wrap">
        <!--Sidebar-->


        <div id="tpl-sidebar" class="span3">
            <h1>Asset List</h1>
            <p>Select modules and enter content for each asset.</p>
            <p><em>To edit text, please click on the text area you wish to edit and change the text inline.</em></p>

            <?php if ($tactic_type == 'webinar') { ?>
            <!-- Webinar -->
            <div class="option-block">
                <h2>Emails</h2>
                <ul class="large-menu">
                    <li><a href="#invitation-email"  class="menu-loader active">Invitation Email</a></li>
                    <li><a href="#reminder-email-one" class="menu-loader">Reminder Email 1</a></li>
                    <li><a href="#reminder-email-two" class="menu-loader">Reminder Email 2</a></li>
                    <li><a href="#confirmation-email" class="menu-loader">Confirmation Email</a></li>
                    <li><a href="#thank-you-email" class="menu-loader">Thank You Email</a></li>
                    <li><a href="#sorry-we-missed-you-email" class="menu-loader">Sorry We Missed You Email</a></li>
                </ul>
            </div>

            <div class="option-block">
                <h2>Landing Pages</h2>
                <ul class="large-menu">
                    <li><a href="#registration-page" class="menu-loader">Registration Page</a></li>
                    <li><a href="#confirmation-page" class="menu-loader">Confirmation Page</a></li>
                </ul>
            </div>
            <!-- /Webinar -->
            <?php } ?>

            <?php if ($tactic_type == 'email') { ?>
            <!-- Single Email -->
            <div class="option-block">
                <h2>Email</h2>
                <ul class="large-menu">
                    <li><a href="#single-email"  class="menu-loader active">Single Email</a></li>
                </ul>
            </div>
            <!-- /Single Email -->
            <?php } ?>

            <?php if ($tactic_type == 'landingpage') { ?>
            <!-- Landing Page -->
            <div class="option-block">
                <h2>Landing Page(s)</h2>
                <ul class="large-menu">
                    <li><a href="#landing-page" class="menu-loader active">Landing Page</a></li>
                    <li><a href="#confirmation-page" class="menu-loader">Confirmation Page</a><br /><small>(If your landing page includes a registration module)</small></li>
                </ul>
            </div>
            <!-- /Landing Page -->
            <?php } ?>

        </div>
        <div class="tpl-head"></div>

        <div id="tpl-body" class="span9 tpl-editor">

            <div class="preview-mode hide">
                <div class="template-preview-select">Preview Mode</div>
            </div>

            <div id="template-previews">
                <?php if ($tactic_type == 'webinar') { ?>
                <!-- Webinar -->
                <div id="invitation-email" data-attr="invitation_email_config" class="template-block template-email">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="reminder-email-one" data-attr="reminder_one_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="reminder-email-two" data-attr="reminder_two_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="confirmation-email" data-attr="confirmation_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="thank-you-email"  data-attr="thank_you_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="sorry-we-missed-you-email" data-attr="sorry_email_config" class="template-block template-email hide">
                    <?php include('email_templates/package/email-webinar.php'); ?>
                </div>

                <div id="registration-page" data-attr="registration_config" class="template-block template-page hide">
                    <?php include('email_templates/package/registrationpage-webinar.php'); ?>
                </div>

                <div id="confirmation-page" data-attr="confirmation_config" class="template-block template-page hide">
                    <?php include('email_templates/package/confirmationpage-webinar.php'); ?>
                </div>
                <!-- /Webinar -->
                <?php } ?>


                <?php if ($tactic_type == 'email') { ?>
                <!-- Single Email -->
                <div id="single-email" data-attr="single_email_config" class="template-block template-email">
                    <?php include('email_templates/package/email-single-email.php'); ?>
                </div>
                <!-- /Single Email -->
                <?php } ?>


                <?php if ($tactic_type == 'landingpage') { ?>
                <!-- Landing Page -->
                <div id="landing-page" data-attr="landing_page_config" class="template-block template-page">
                    <?php include('email_templates/package/landingpage-landingpage.php'); ?>
                </div>

                <div id="confirmation-page" data-attr="confirmation_config" class="template-block template-page hide">
                    <?php include('email_templates/package/confirmationpage-landingpage.php'); ?>
                </div>
                <!-- /Landing Page -->
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Form to store configs and attributes  -->
    <?php
            //If coming from Edit route
            if($bln_edit){
                echo '<form id="custom-template-form" name="content-edit-template-form" action="email_templates/requests.php" method="POST">';
            // Part of request form flow
            }else{
                echo '<form id="custom-template-form" class="form-horizontal" action="includes/_requests_'.$tactic_type.'.php" method="POST" enctype="multipart/form-data">';
            }
        ?>
    <!-- Hidden fields -->
    <input type="hidden" id="template" name="template" value="custom">
    <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
    <input type="hidden" id="step" name="step" value="3">
    <input type="hidden" id="c_type" name="c_type" value="<?php echo $_GET['type']; ?>">
    <input type="hidden" id="request_id" name="request_id" value="">
    <!-- <input type="hidden" id="head_option" name="head_option" value=""> -->
    <?php
        //If coming from Edit route
        if($bln_edit){
            echo '<input type="hidden" id="action" name="action" value="edittemplate">';
            echo '<input type="hidden" id="req_id" name="req_id" value="' . $edit_id . '">';
        // Part of request form flow
        }else{
            echo '<input type="hidden" id="action" name="action" value="savetemplate">';
        }
    ?>

    <textarea type="hidden" id="landing_page_config" class="hide" name="landing_page_config"></textarea>
    <textarea type="hidden" id="single_email_config" class="hide" name="single_email_config"></textarea>
    <textarea type="hidden" id="invitation_email_config" class="hide" name="invitation_email_config"></textarea>
    <textarea type="hidden" id="reminder_one_email_config" class="hide" name="reminder_one_email_config"></textarea>
    <textarea type="hidden" id="reminder_two_email_config" class="hide" name="reminder_two_email_config"></textarea>
    <textarea type="hidden" id="confirmation_email_config" class="hide" name="confirmation_email_config"></textarea>
    <textarea type="hidden" id="thank_you_email_config" class="hide" name="thank_you_email_config"></textarea>
    <textarea type="hidden" id="sorry_email_config" class="hide" name="sorry_email_config"></textarea>
    <textarea type="hidden" id="registration_config" class="hide" name="registration_config"></textarea>
    <textarea type="hidden" id="confirmation_config" class="hide" name="confirmation_config"></textarea>


    </form>

    <div class="header-row">
        <div class="container-fluid">
            <header style="width:auto;">
                <div class="header-left" style="margin-top:12px;float:left;">
                    <?php
                        // echo '<a href="" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                        if($bln_edit)
                            echo '<a href="request-detail?id=' . $edit_id . '" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                        else
                            echo '<a href="request-'.$tactic_type.'-3" id="tpl-cancel" class="btn-green plain pull-left" style="margin-right:6px;">Cancel</a>';
                    ?>
                </div>
                <div class="header-right"><a href="" id="tpl-preview" class="btn-green plain" style="margin-right:6px;">Preview Mode</a>
                    <?php
                        // if($bln_edit){
                            //TODO link to update script
                            echo '<a href="#" id="submit-custom-temp-form" class="btn-green submit">Save &amp; Continue</a></div>';
                            // echo '<a href="#" id="cancel-custom-temp-form" class="btn-green">Cancel</a></div>';
                        // }else{
                            // echo '<a href="#" id="submit-request-form" class="btn-green submit">Save &amp; Continue</a></div>';
                        // }

                    ?>
            </header>
        </div>
    </div>

</div>

<?php include('includes/footer.php'); ?>
