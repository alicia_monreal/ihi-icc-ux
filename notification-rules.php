<?php
session_start();
//Redirect to Home if not admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/home');
    die();
}

$loc = 'notification-rules';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
            	<?php if (isset($_GET['action']) && $_GET['action'] ==='create' && !isset($_GET['default'])) { 
                    echo '<h1>Create a New Rule</h1><p>&nbsp;</p>'; 
                } else if(isset($_GET['action']) && $_GET['action'] === 'create' && isset($_GET['default'])){
                    echo '<h1>Create a Default Rule</h1><p>&nbsp;</p>'; 
                } else if($_GET['action'] !== "create" && isset($_GET['default'])){
                    echo '<h1>Default Rule</h1><p>&nbsp;</p>'; 
                } else { 
                    echo '<h1>Notification Rules</h1>'; 
                } 
                ?>
            	
                <?php if (!isset($_GET['id']) && !isset($_GET['action'])) { ?><p><a href="notification-rules?action=create" class="btn-green add">Create a New Rule</a></a></p><?php } ?>
                <?php include('includes/rules-list.php'); ?>
            </div>
            
        </div>
    </div>
    <script>
    function deleteResource(url) {
        if (window.confirm("Are you sure you want to delete this notification rule?")) { 
            window.location.href = url;
        }
    }

    function removeThisUser(removeDiv) {   
        /* this function is used in the registration form functionality */
        // remove the input choice
        $(removeDiv).remove();
        return false;
    }

    </script>

<?php include('includes/footer.php'); ?>