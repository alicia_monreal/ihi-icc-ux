<?php
$loc = 'training';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
                <h1>Training</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div id="tabbed-solutions" class="tabbed-contents span12" style="margin-bottom:60px;">
            	<ul class="nav nav-tabs" id="tabs-solutions">
					<li class="active"><a href="#webinars">Webinars</a></li>
					<li><a href="#marketing-automation">Marketing Automation</a></li>
					<!--<li><a href="#partner-sales">Partner Sales</a></li>-->
				</ul>
				 
				<div id="video-grid" class="tab-content">
					<div class="tab-pane active" id="webinars">
						<div class="row">
							<div class="span6">
								<h3>Launch Announcement</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Overview</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Get Started</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>How to Request a Webinar</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
						</div>
					</div>
					<div class="tab-pane" id="marketing-automation">
						<div class="row">
							<div class="span6">
								<h3>How to Request a Webinar</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Launch Announcement</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Overview</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Get Started</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
						</div>
					</div>
					<div class="tab-pane" id="partner-sales">
						<div class="row">
							<div class="span6">
								<h3>Overview</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Get Started</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>How to Request a Webinar</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
							<div class="span6">
								<h3>Launch Announcement</h3>
								<img src="img/fpo-video.gif" alt="">
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>