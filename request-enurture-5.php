<?php
$loc = 'campaign-requests';
$step = 5;
$c_type = 'enurture';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>eNurture Campaign Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fiveup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li>Step 3</li>
				<li>Step 4</li>
				<li class="active">Step 5</li>
			</ul>
			<div class="request-step step5">
				<div class="fieldgroup">
					<p>Please provide your target audience segmentation by uploading a list and/or describing a segmentation from existing users.</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p><p>Click on the <i class="icon-question-sign" style="margin-top: 3px;"></i> for more information and examples.</p>
					<h3 class="fieldgroup-header">Audience Member Details</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_enurture.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="5">
					<input type="hidden" id="c_type" name="c_type" value="enurture">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<div class="fieldgroup">
						<?php include('includes/contactlist-basics.php'); ?>
					</div>
					<?php include('includes/notified_users.php'); ?>
					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-enurture-4" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: eNurture Request: Step 4">Submit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>