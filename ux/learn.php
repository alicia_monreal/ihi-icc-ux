<?php
$loc = 'learn';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

<!--Video Popup start-->

<div class="modal fade video-popup" id="videoPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="text-align:center">
        
        <span class="icon-close close" data-dismiss="modal" aria-hidden="true"></span>
        <h2 id="requestLabel" class="request-form-label">Webinar Best Practices in 80 Seconds</h2>
      </div>
      <div class="modal-body">
      <div class="videoWrapper">
          <iframe src="//player.vimeo.com/video/59658465?color=ff6d33&title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
        
      </div>
     
    </div>
  </div>
</div>
<!--Video Popup start-->

<!--<a class="support-fixed" href="support" data-analytics-label="Header: Support">
    <h3>Support</h3>
</a>-->

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="request-header">
						<h2>Learn</h2>
				</div>
			</div>
		</div>
	</div>


    <!-- Main Containter Div -->
    <div id="learn-page" class='container-wide container-fluid padding-none'>
        
        <div class="panel panel-default help-for-learn secondary-background-color">
          <div class="panel-body">
            <ul>
                <li><strong>Need Help Getting Started?</strong> Here are some critical resources to help you with requesting your first webinar:</li>
                <li><a href="javascript:;"><i class="icon2 icon-webinar-training"></i>Webinar Onboarding Training.pptx <span class="caret"></span></a></li>
                <li><a href="javascript:;"><i class="icon2 icon-template"></i>Eloqua Template Guidelines.pdf <span class="caret"></span></a></li>
            </ul>
          </div>
        </div>
        <!--Help for learn end-->


<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs learn-page-tabs nav-tabs-simple" role="tablist">
    <li role="presentation" class="active"><a href="#webinar" aria-controls="home" role="tab" data-toggle="tab">
        <i class="icon2 icon-webinar"></i> Webinars
    </a></li>
    <li role="presentation"><a href="#newsletters" aria-controls="profile" role="tab" data-toggle="tab">
        <i class="icon no-bg icon-book-open"></i> Newsletters
    </a></li>
    <li role="presentation"><a href="#enurture" aria-controls="messages" role="tab" data-toggle="tab">
        <i class="icon no-bg icon-graduation"></i> eNurture
    </a></li>
    <li role="presentation"><a href="#emails" aria-controls="settings" role="tab" data-toggle="tab">
        <i class="icon no-bg icon-envelope-letter"></i> Emails
    </a></li>
    <li role="presentation"><a href="#landingPage" aria-controls="settings" role="tab" data-toggle="tab">
        <i class="icon2 icon-landingpage"></i> Landing Pages
    </a></li>
    <li role="presentation"><a href="#lists" aria-controls="settings" role="tab" data-toggle="tab">
        <i class="icon no-bg icon-list icon-lists"></i> Lists
    </a></li>
    <li role="presentation"><a href="#campaigns" aria-controls="settings" role="tab" data-toggle="tab">
        <i class="glyphicon glyphicon-edit"></i> Campaigns
    </a></li>
  </ul>

<?php
$table_header = '<div class="table-border-wrapper">
					<table class="table table-striped table-bordered table-hover tablesorter requests-table learn-table">
			        <thead>
			        	<th style="width:14%;">Publish Date <b class="icon-white"></b></th>
                        <th>Title <b class="icon-white"></b></th>
                        <th>Action <b class="icon-white"></b></th>
                        <th>Type <b class="icon-white"></b></th>
			        </thead>
			        <tbody>';
$table_footer = '</tbody></table></div>';
?>


  <!-- Tab panes -->
    <div class="tab-content">
    <span class="spinner rotating" style="display:none"></span>
      <div role="tabpanel" class="tab-pane fade in active" id="webinar">
        <div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('9'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div> 
      </div>
      <div role="tabpanel" class="tab-pane fade" id="newsletters">
    	<div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('10'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="enurture">
  		<div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('11'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="emails">
        <div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('12'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="landingPage">
        <div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('13'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="lists">
        <div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('14'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="campaigns">
        <div class="row filter-and-search margin-top-40px margin-right-none margin-left-none">
            <div class="col-xs-8 pull-right learn-filter-right padding-right-none">
                <span class="spinner rotating" style="display:none"></span>
                <ul class="nav nav-pills custom-filter pull-right">
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Best Practices</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Training</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Analytics</a></li>
                  <li role="presentation"><a href="javascript:;" class="tab-filter">Templates</a></li>
                  <li role="presentation" class="active"><a href="javascript:;" class="tab-filter">All</a></li>
                </ul>
            </div>
        </div>
        <!--Filter and search end-->
        <div class="row margin-none">
            <div class="col-xs-12 padding-none">
                <div class="table-wrapper-learn">
                	<?php echo $table_header; ?>
                	<?php echo getResourcesTable('15'); ?>
                	<?php echo $table_footer; ?>
                </div>
            </div>
        </div>
      </div>
    </div>

</div>
        <!--Tabs end-->

    </div>
    <!-- Main Containter End -->
	
   

<?php include('includes/footer.php'); ?>