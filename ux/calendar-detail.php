<?php
$loc = 'calendar-detail';
//include('includes/head.php');
//include('includes/header.php');
require_once('includes/_globals.php');
?>

   <div class="container" style="width:460px">
        <div class="row intro-body" style="margin-top:0;margin-bottom:0;">
            <div class="intro">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
                            req.*,
                            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                            status.value AS status_value,
                            req.request_type AS request_type_id,
                            request_type.request_type AS request_type,
                            users.fname AS fname,
                            users.lname AS lname,
                            users.email AS email,
                            users.role AS role,
                            users.point_of_contact AS point_of_contact,
                            users.point_of_contact_email AS point_of_contact_email
                        FROM request AS req
                        INNER JOIN status on req.status = status.id
                        INNER JOIN request_type on req.request_type = request_type.id
                        INNER JOIN users on req.user_id = users.id";

                $query.=" WHERE req.active = 1";

                $query.=" AND req.id = '".$_GET['id']."' LIMIT 1";
                
                $result = $mysqli->query($query);
                $row_cnt = $result->num_rows;

                //print_r($query);

                if ($row_cnt > 0) {
        
                    while ($obj = $result->fetch_object()) {
                        $owner = false;

                        $placeholder_text = "";
                        if($obj->status == 14)
                            $placeholder_text = " (Placeholder)";

                        echo '<h1 style="font-size:21px;">'.getTitle($_GET['id'], $obj->request_type_id).$placeholder_text.'</h1>';

                        echo '<div id="zebra" class="single-request calendar-detail-request">';

                        //Now show the details of the particular type of request
                        echo getRequestDetails($obj->id, $obj->request_type_id, $edit, $owner);

                        //And then show the campaign details
                        echo '<h1 style="font-size:21px;"><br />Campaign Details</h1>';
                        echo getCampaignDetails($obj->campaign_id, 8, $edit, $owner);

                        echo '</div>'; //End single-request
                    }
                } else {
                    echo 'Sorry, there is no additional information for this event or it is no longer active.';
                }
                ?>

            </div>
        </div>

    </div>

<?php //include('includes/footer.php'); ?>