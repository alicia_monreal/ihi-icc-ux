<?php
session_start();
//Redirect to Home if not admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/home');
    die();
}

$loc = 'manage-users';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">

        <div class='row'>
            <div class='col-md-12'>
                <div class='request-header'>
                    <h2>Manage Users</h2>
                    <h4>Use this page to manage Intel Customer Connect users. Click their name to see more details.</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php include('includes/user-list.php'); ?>
            </div>
        </div>

    </div>
    <script>
    function approveUser(url) {
    	if (window.confirm("Are you sure you want to approve this user?")) { 
			window.location.href = url;
		}
    }
    function denyUser(url) {
    	if (window.confirm("Are you sure you want to mark this user as inactive?")) { 
			window.location.href = url;
		}
    }

    </script>

<div class="push"></div>
<?php include('includes/footer.php'); ?>