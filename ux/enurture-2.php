<?php
$loc = 'request';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

$tactic_name="enurture";

unset($_SESSION['in_progress_id']);
unset($_SESSION['tactic_request_id']);
?>

<div class='container'>

	<div class='row'>
		<div class='col-md-12'>
			<div class='request-header'>
				<h2>Create a eNurture</h2>
				<h4>Follow the steps to build and launch your enurture.</h4>
			</div>
		</div>
	</div>
<div class='row row-centered'>

	<div class='col-md-3 col-centered box-button-container'>
		<a href='#campaign-setup' data-toggle='modal' data-target='#campaign-setup' data-backdrop="static" class="lnk existing-campaign-setup">
			<div class='box-button icon-small' val='campaign-setup'>
				<div class='circled'>
					<div class="icon campaign-setup" style='height:28px !important; margin-top:36%;'></div>
				</div>
				<h3 class='spaced'>Campaign Setup</h3>
				<div class='step-number'><i class='fa fa-2x'>1</i></div>
			</div>
		</a>
	</div>

	<div class='col-md-3 col-centered box-button-container'>
		<a href='#enurture-setup' data-target='#enurture-setup' data-backdrop="static" class="lnk box-button-setup">
			<div class='box-button icon-small inactive' val='setup'>
				<div class='circled'>
					<div class="icon setup"></div>
				</div>
				<h3 class='spaced'>eNurture Setup</h3>
				<div class='step-number'><i class='fa fa-2x'>2</i></div>
			</div>
		</a>
	</div>

	<div class='col-md-3 col-centered box-button-container'>
		<a href='#email-content' data-target='#email-content' data-backdrop="static" class="lnk box-button-link">
			<div class='box-button icon-small inactive' val='email-content'>
				<div class='circled'>
					<div class="icon webinar-content"></div>
				</div>
				<h3 class='spaced'>Email Content</h3>
				<div class='step-number'><i class='fa fa-2x'>3</i></div>
			</div>
		</a>
	</div>

</div>

<div class='row row-centered' style='margin-bottom:100px'>

	<div class='col-md-3 col-centered box-button-container'>
		<a href='#webinar-audience' data-target='#webinar-audience' data-backdrop="static" class="lnk box-button-link">
			<div class='box-button icon-small inactive' val='audience'>
				<div class='circled'>
					<div class="icon audience"></div>
				</div>
				<h3 class='spaced'>Audience</h3>
				<div class='step-number'><i class='fa fa-2x'>4</i></div>
			</div>
		</a>
	</div>

	<div class='col-md-3 col-centered box-button-container'>
		<a href='#additional_information' data-target='#additional_information' data-backdrop="static" class="lnk box-button-link">
			<div class='box-button inactive' val='additional-lg'>
				<div class='circled'>
					<div class="icon additional-lg" style="margin-top:21%;margin-right:11px"></div>
				</div>
				<h3 class='spaced'>Additional Information</h3>
				<div class='step-number'><i class='fa fa-2x'>5</i></div>
			</div>
		</a>
	</div>
		
</div>

<?php include('includes/campaign-details.php'); ?>
<?php include('includes/enurture-setup.php'); ?>
<?php include('includes/email-content.php'); ?>
<?php include('includes/webinar-audience.php'); ?>
<?php include('includes/additional_information.php'); ?>

</div>

<div class="push"></div>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">	
</script>