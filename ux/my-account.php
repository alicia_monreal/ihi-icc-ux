<?php
$loc = 'my-account';
include('includes/head.php');
include('includes/header.php');

function generateFormToken($form) {
    $token = md5(uniqid(microtime(), true));  
    $_SESSION[$form.'_token'] = $token; 
    
    return $token;
}
$newToken = generateFormToken('account-update-form'); 
?>	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="request-header">
					<h2>My Account</h2>
					<?php
					if (isset($_GET['success'])) {
						echo '<h4 class="account-update-form" style="color:#ff0000;">';
						if ($_GET['success'] == 'true') {
							echo 'Your changes have been saved.';
						} else { 
							echo 'Sorry, there was a problem updating your account. Please try again or contact us for support.';
						}
						echo '</h4>';
					}
					?>
					<?php
					if (isset($_GET['new'])) {
						echo '<h4>Thank you for creating an account with Intel Customer Connect. Please create a new password below.</h4>';
					} else { 
						echo '<h4>Please use this page to make changes to your account information.</h4>';
					}
					?>
				</div>
			</div>
		</div>

		<div class="row">
			<div id="account-update" class="col-md-12">
				<form id="account-update-form" action="includes/_user-account.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" name="token" value="<?php echo $newToken; ?>">

					<div class="row form-group">
  						<div class="col-md-4">
							<input type="text" id="fname" name="fname" placeholder="First Name" value="<?php echo $_SESSION['fname']; ?>" class="form-control required" autocomplete="off">
						</div>
						<div class="col-md-4">
							<input type="text" id="lname" name="lname" placeholder="Last Name" value="<?php echo $_SESSION['lname']; ?>" class="form-control required" autocomplete="off">
						</div>
						<div class="col-md-4">
							<input type="text" id="email" name="email" placeholder="Email" value="<?php echo $_SESSION['email']; ?>" class="form-control email required" autocomplete="off">
						</div>
					</div>
					<div class="row form-group">
  						<div class="col-md-4">
							<input type="text" id="title" name="title" placeholder="Title" value="<?php echo $_SESSION['title']; ?>" class="form-control" autocomplete="off">
						</div>
  						<div class="col-md-4">
							<input type="text" id="company" name="company" placeholder="Company" value="<?php echo $_SESSION['company']; ?>" class="form-control required" autocomplete="off">
						</div>
					</div>
						
					<?php if ($_SESSION['role'] == 2) { ?>
					<div class="row form-group">
						<div class="col-md-4">
							<input type="text" id="point_of_contact" name="point_of_contact" placeholder="Who is your Intel point of contact?" value="<?php echo $_SESSION['point_of_contact']; ?>" class="form-control" autocomplete="off">
						</div>
						<div class="col-md-4">
							<input type="text" id="point_of_contact_email" name="point_of_contact_email" placeholder="Point of contact email address" value="<?php echo $_SESSION['point_of_contact_email']; ?>" class="form-control" autocomplete="off">
						</div>
					</div>
					<?php } ?>

					<div class="row form-group">
						<div class="col-md-12">
							<label for="password">Create a new password. It is recommended that you not use your Intel password for this portal.</label>
						</div>
  						<div class="col-md-4">
							<input type="password" id="password" placeholder="New Password" name="password" value="" class="form-control" autocomplete="off">
						</div>
						<div class="col-md-4">
							<input type="password" id="password_confirm" placeholder="Password Confirm" name="password_confirm" value="" class="form-control" autocomplete="off">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<input type="submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Update User Account" value="Update">
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
<div class="push"></div>
<?php include('includes/footer.php'); ?>