<?php
session_start();
unset($_SESSION['loggedin']);
unset($_SESSION['user_id']);
unset($_SESSION['admin']);
unset($_SESSION['fname']);
unset($_SESSION['lname']);
unset($_SESSION['email']);
unset($_SESSION['access_control']);
unset($_SESSION['access_blocked']);
unset($_SESSION['timeout']);
unset($_SESSION['linkback']);
session_destroy();

if (isset($_GET['session'])) {
	header('Location:/ux/?session=timeout');
	die();
} else {
	header('Location:/ux/');
	die();
}
exit();
?>