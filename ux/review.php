<?php
$loc = 'review';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

<div class='container'>

	<div class='row'>
		<div class='col-md-12'>
			<div class='request-header'>
				<?php if ($_SESSION['admin'] == 1) { echo '<div class="header-btn"><a href="includes/_export.php?type=tactics" class="btn-green">Export Data &nbsp;&nbsp;<i class="fa fa-download"></i></a></div>'; } ?>
				<h2>Review</h2>
				<h4>Use this page to manage and track your requests. Click request name to see more details.</h4>
				<?php
                if (isset($_GET['inprogress']) && $_GET['inprogress'] == true) {
                	echo '<p class="error"><i class="icon-info-sign" style="margin-top: 3px;margin-right: 4px;"></i>Your request has been saved. Click the name of your request to complete or continue to work on it.</p>';
                }
                ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php
                $query = "SELECT
			                req.*,
			                DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
			                DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
			                status.value AS status_value,
			                request_type.request_type AS request_type,
			                request_type.id AS request_type_id,
			                users.fname AS fname,
			                users.lname AS lname,
			                users.email AS email
			            FROM request AS req
			            INNER JOIN status on req.status = status.id
			            INNER JOIN request_type on req.request_type = request_type.id
			            INNER JOIN users on req.user_id = users.id";

			    $query.=" WHERE req.active = 1"; // All active requests fetched
			    $query.=" ORDER BY req.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			$resultant_array = array(); 

    			if ($row_cnt > 0) {

    				while ($obj = $result->fetch_object()) {

    					$extra_owners = array(); // initialize extra_owners array

                        if(!empty($obj->additional_owners)) { // check if any additional owners exists
                        	$extra_owners = unserialize($obj->additional_owners); // if yes then unserialize the array
                        }

                        // If logged-in user has ownership rights to the request, then save it    
                        if ($obj->user_id == $_SESSION['user_id'] || in_array($_SESSION['user_id'],$extra_owners) || $_SESSION['admin'] == 1) {
    						$resultant_array[] = $obj; 
    					}
    				}

    				if(!empty($resultant_array)) { // if saved data is not empty

				        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
				        echo '<thead><th>Created <b class="icon-white"></b></th><th>ICC ID <b class="icon-white"></b></th><th>Name <b class="icon-white"></b></th><th>Owner <b class="icon-white"></b></th><th>Assigned To <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th><th></th></thead>';
				        echo '<tbody>';

				        foreach($resultant_array as $obj) {  
				            echo '<tr>';
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>'.$obj->date_created.'</td>';
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>'.$obj->id.'</td>';
				            //In progress
				            if ($obj->status == 14) {
				            	echo '<td class="in-progress"><a href="includes/_requests_route?in_progress_id='.$obj->id.'&in_progress_type='.$obj->request_type_id.'&campaign_id='.$obj->campaign_id.'" class="notrack">'.getName($obj->id, $obj->request_type_id).'</a></td>';
				            	//Edit/View icons
				            	$editviewicon = '<a href="includes/_requests_route?in_progress_id='.$obj->id.'&in_progress_type='.$obj->request_type_id.'&campaign_id='.$obj->campaign_id.'" class="notrack action-icon icon-pencil popover-hover" data-toggle="popover" data-content="Edit"></a>';
					        } else {
					            if ($obj->request_type_id == 8) {
					            	echo '<td><a href="campaign-detail?id='.$obj->id.'" class="notrack popover-hover" data-toggle="popover" data-content="'.getEloquaSourceId($obj->id, $obj->request_type_id).'">'.getName($obj->id, $obj->request_type_id).'</a></td>';
					            	//Edit/View icons
				            		$editviewicon = '<a href="campaign-detail?id='.$obj->id.'" class="notrack action-icon icon-eye popover-hover" data-toggle="popover" data-content="View Details"></a>';
					            } else {
					            	echo '<td><a href="request-detail?id='.$obj->id.'" class="notrack popover-hover" data-toggle="popover" data-content="'.getEloquaSourceId($obj->id, $obj->request_type_id).'">'.getName($obj->id, $obj->request_type_id).'</a></td>';
					            	//Edit/View icons
				            		$editviewicon = '<a href="request-detail?id='.$obj->id.'" class="notrack action-icon icon-eye popover-hover" data-toggle="popover" data-content="View Details"></a>';
					            }
					        }
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo ' style="white-space:nowrap;">'.$obj->fname.' '.$obj->lname.'</td>'; //Who owns it, the person that made it, or the Owner value
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo ' style="white-space:nowrap;">'.getAssignedTo($obj->assigned_to, 0).'</td>';
				            $placeholder_status = "";
				            //if ($obj->status == 14) { $placeholder_status = " (Placeholder)"; }
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>'.$obj->request_type.$placeholder_status.'</td>';
				            echo '<td'; if ($obj->status == 14) { echo ' class="in-progress"'; } echo '>';
				            if ($obj->status == 14) { echo 'User In Progress'; } else { echo getStatus($obj->status); }
				            echo '</td>';
				            echo '<td style="text-align:center;'; if ($obj->status == 14) { echo 'background-color: #fdfbf0 !important;'; } echo '">'.$editviewicon;
				            echo '<a href="" class="action-icon icon-docs popover-hover" data-toggle="popover" data-content="Duplicate"></a><a href="" class="action-icon icon-control-pause popover-hover" data-toggle="popover" data-content="Pause"></a>';
				            if ($_SESSION['admin'] == 1) {
				            	echo '<a href="" class="action-icon icon-close popover-hover" data-toggle="popover" data-content="Delete"></a>';
				            }
				            echo '</td>';
				            echo '</tr>';
				        }
				        
				        echo '</tbody></table>';
			    	} else {
			    		echo '<p><strong>You currently have no active requests.</strong></p>';	
			    	}
			    } else {
			        echo '<p><strong>You currently have no active requests.</strong></p>';
			    }

            	?>
		</div>

	</div> <!--end ROW-->
		
</div> <!-- end CONTAINER-->

</div>

<div class="push"></div>
<?php include('includes/footer.php'); ?>