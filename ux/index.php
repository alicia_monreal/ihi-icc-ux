    <?php
    session_start();
    include('includes/_globals.php');
    //Redirect to Home if logged in
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1) {
        header('Location:/home');
        die();
    }

    $loc = 'login';
    include('includes/head.php');
    include('includes/header.php');

    function generateFormToken($form) {
        $token = md5(uniqid(microtime(), true));  
        $_SESSION[$form.'_token'] = $token; 
        
        return $token;
    }
    $newToken = generateFormToken('request-form'); 
    ?>
<div class='row' stlye='margin-right:0'>
  <div  id="slideshow" class='col-md-12' style='padding-right:0;height:240px'>
    <div class='container'>
          <div  class="cycle-slideshow slideshow" data-cycle-manual-speed="200" data-cycle-fx="fade" data-cycle-speed='1500' data-cycle-timeout="4000" data-cycle-pager='.cycle-pager' data-cycle-slides="> .slideshow-container" style='height:240px'>
         
          <div class='slideshow-container' >
        <div class=' col-sm-6 col-md-5' style="padding-left:0">
        <div style="display:table-cell;vertical-align:middle;height:240px">
            <h1>Plan, request, and monitor marketing activities</h1>
        </div>
        </div>
        <div class='col-sm-6 col-md-7' style="text-align:center">
            <img  style='height:240px;' src="img/hero-team.png" alt="Welcome to the Intel Online Sales Center">
        </div>
        </div>

        <div class='slideshow-container'>
        <div class=' col-sm-6 col-md-5' style="padding-left:0">
        <div style="display:table-cell;vertical-align:middle;height:240px">
            <h1>Learn about tactics from videos, BKMs, and other training documentation</h1>
        </div>
        </div>
        <div class=' col-sm-6 col-md-7' style="text-align:center">
            <img style='height:240px;' src="img/hero-team.png" alt="Welcome to the Intel Online Sales Center">
        </div>
        </div>
         <div class='slideshow-container' >
        <div class=' col-sm-6 col-md-5' style="padding-left:0">
        <div style="display:table-cell;vertical-align:middle;height:240px">
            <h1>Engage with production support to help fulfill your marketing needs</h1>
        </div>
        </div>
        <div class=' col-sm-6 col-md-7' style="text-align:center">
            <img style='height:240px;' src="img/hero-team.png" alt="Welcome to the Intel Online Sales Center">
        </div>
        </div>
        
    </div>
     <div id='nav' class="cycle-pager"></div>
</div>


  </div>
</div>
    <div class='container'>
        <div class='row' style='margin-top:30px'>

            <div class='col-md-3'>
                <h1>Welcome</h1>
                <h4 class="login-form-subtitle"><?php if (isset($_GET['session'])) { echo '<span style="color:#ff0000;">Your session has expired.</span><br />'; } ?>Welcome to Intel Customer Connect, your destination to learn about, request, and manage webinar and marketing automation campaigns.</h4>
            </div>
            <div class='col-md-8 col-md-offset-1'>
                <h1>Login</h1>
                <form action="home" id="login-form" class="login-form" data-form="login">
                  <div class="form-group">
                    <input type="text" name="username" id="username" placeholder="Email*" value="<?php if (isset($_COOKIE['rememberme'])) { echo $_COOKIE['rememberme']; } ?>" class="form-control required">
                </div>
                <div class="form-group">
                    <input  type="password" name="password" id="password" placeholder="Password*" value="" class="form-control required">
                </div>

                <button type="submit" id="login-submit" class="btn-green" data-analytics-label="Submit Form: Login" value="Submit">Submit &nbsp;&nbsp;<i class="fa fa-caret-right"></i></button>
               <input type="button"  class="btn-gray" data-toggle="modal" data-target='#request' value="Request Access">
                <div class="checkbox rememberme" style="left:15px">
                    <label>
                        <input type="checkbox" id="rememberme" name="rememberme" value="1"<?php if (isset($_COOKIE['rememberme'])) { echo ' checked="checked"'; } ?>> Remember me
                  </label>
              </div>
             <p> Forgot your password? <a href="#forgot" data-toggle="modal" data-target="#forgot">Click here</a></p>
          </form>

      </div>
  </div>
</div>

<div class="container" style='display:none'>
 <div id="login" class="row">
  <h1>Login</h1>
  <h4 class="login-form-subtitle"><?php if (isset($_GET['session'])) { echo '<span style="color:#ff0000;">Your session has expired.</span><br />'; } ?>Welcome to Intel Customer Connect, your destination to learn about, request, and manage webinar and marketing automation campaigns.</h4>
  <form action="home" id="login-form" class="login-form" data-form="login">
    <fieldset>
        <p><input type="text" name="username" id="username" placeholder="Email" value="<?php if (isset($_COOKIE['rememberme'])) { echo $_COOKIE['rememberme']; } ?>" class="required"></p>
        <p><input type="password" name="password" id="password" placeholder="Password" value="" class="required"></p>
        <p class="pull-left" style="margin-top:-4px;"><a href="#request" data-toggle="modal" data-analytics-label="Access Request Form">Request Access</a><br />Forgot your password? <a href="#forgot" data-toggle="modal" data-target="#forgot" data-analytics-label="Forgot Password Link">Click here</a></p><p class="pull-right"><input type="submit" id="login-submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Login" value="Submit" style="width:120px;"></p>
        <div class="clearfix"></div>
        <div class="span2 pull-right" style="width:112px;text-align:right;clear:both;"><label class="checkbox inline" style="padding-left:2px;"><input type="checkbox" id="rememberme" name="rememberme" value="1"<?php if (isset($_COOKIE['rememberme'])) { echo ' checked="checked"'; } ?>> Remember me</label></div>
    </fieldset>
</form>
</div>
</div>

<!-- Modals -->
<!-- Request Access -->
<div id='request' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h1 id="requestLabel" class="request-form-label">Request Access</h1>
        <h4 class="request-form-subtitle">Fill out form to request access to site.</h4>
    </div>

    <div class="modal-body">
      <form action="" id="request-form" class="download-form" data-form="request">

        <input type="hidden" name="token" value="<?php echo $newToken; ?>">

        <div class='form-group'>
            <input type="text"  name="fname" id="fname" placeholder="First Name*" value="" class="form-control required" autocomplete="off">
        </div>

        <div class='form-group'>
            <input type="text"  name="lname" id="lname" placeholder="Last Name*" value="" class="form-control required" autocomplete="off">
        </div>
        <div class='form-group'>
            <input type="text"  name="title" id="title" placeholder="Title" value="" class="form-control" autocomplete="off">
        </div>
        <div class='form-group'>
            <input type="text"  name="company" id="company" placeholder="Company*" value="" class="form-control required" autocomplete="off">
        </div>
        <div class='form-group'>
          <input type="text"  name="email" id="email" placeholder="Email*" value="" class="form-control required email">
      </div>

      <div class="form-group">
        <select name="role" id="role" class="form-control required"><option value="">Role*</option>
            <?php
            $query = "SELECT * FROM roles WHERE active = 1 ORDER BY name ASC";
            $result = $mysqli->query($query);
            while ($obj = $result->fetch_object()) {
                            if ($obj->id != 1 && $obj->id != 6) { //Can't choose Admin
                            echo '<option value="'.$obj->id.'">'.$obj->name.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group request-form-group">
                <label for="geo" class="control-label request-label col-md-1"> GEO </label> <!-- Target -->
                <div class="controls col-md-11" style="margin-bottom: 15px;">
                    <label class="checkbox-inline first-checkbox request-checkbox"> <input type="checkbox" id="selectall" name="selectall" value="" class="required checkall"> World Wide</label>
                    <label class="checkbox-inline request-checkbox" > <input type="checkbox" name="geo[]" value="APJ" class="required check-geo"> APJ</label>
                    <label class="checkbox-inline request-checkbox"> <input type="checkbox" name="geo[]" value="EMEA" class="required check-geo"> EMEA</label>
                    <label class="checkbox-inline request-checkbox"> <input type="checkbox" name="geo[]" value="LAR" class="required check-geo"> LAR</label>
                    <label class="checkbox-inline request-checkbox"> <input type="checkbox" name="geo[]" value="NAR" class="required check-geo"> NAR</label>
                    <label class="checkbox-inline request-checkbox"> <input type="checkbox" name="geo[]" value="PRC" class="required check-geo"> PRC</label>
                    
                </div>
            </div>
            <div class="form-group">
                <select name="sector" id="sector" class="form-control"><option value="">Sector</option></select>            
            </div>
            <div class="form-group">
                <select name="subsector" id="subsector" class="form-control"><option value="">Sub-Sector</option></select>
            </div>
            <div class="form-group">             
                <input type="text" name="subsector_other" id="subsector_other" placeholder="Please provide a specific sub-sector" value="" class="form-control" autocomplete="off" style="display:none;">
            </div>

            <div class="modal-footer" style="text-align:center">
                <a href="" class="lnk" style="font-size:16px;padding-top:10px" data-dismiss="modal">Cancel &nbsp;</a>
                <a href="" class="btn-green submit" data-analytics-label="Submit Form: Request Access">Submit &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
            </div>
    
    </form>
</div>


</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Forgot Password -->
<div id="forgot" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
                <h1 id="requestLabel" class="forgot-form-label">Forgot Password?</h1>
                <h4 class="forgot-form-subtitle">Enter your email below and we'll resend your password.</h4>
            </div>

            <div class="modal-body">
                <form action="" id="forgot-form" class="download-form" data-form="forgot">
                    <div class="form-group">
                        <input type="text" name="email_forgot" id="email_forgot" placeholder="Email" value="" class="form-control required email">
                    </div>
                </form>
            </div>

            <div class="forgot-form-footer modal-footer" style="text-align:center">
                <a href="" class="lnk" style="font-size:16px;padding-top:10px" data-dismiss="modal">Cancel &nbsp;</a>
                <a href="" id="forgot-password" class="btn-green submit">Submit &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
            </div>
        </div>
    </div>
</div>


<!-- Contact Us -->
<div id="contact-us" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="requestLabel" aria-hidden="true">
  <div class="modal-header">
    <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
    <h3 id="requestLabel" class="request-form-label">Contact Us</h3>
    <h4 class="contact-form-subtitle">Use the form below to contact us.</h4>
</div>
<div class="modal-body">
    <form action="" id="contact-form" class="download-form" data-form="contact">
        <fieldset>
            <input type="hidden" name="token" value="<?php echo $newToken; ?>">
            <p>
                    <select id="category" name="category" class="required" style="width:408px;">
                        <option value="">Please select a reason for contacting us</option>
                        <option value="Aprimo">Aprimo</option>
                        <option value="Data">Data</option>
                        <option value="Eloqua">Eloqua</option>
                        <option value="Feature Requests">Feature Requests</option>
                        <option value="ICC Portal">ICC Portal</option>
                        <option value="Reporting Request">Reporting Request</option>
                        <option value="Webinar">Webinar</option>
                        <option value="Other">Other</option>
                    </select>
                </p>
                <p><input type="text" name="name" id="name" placeholder="Name" value="" class="required"></p>
                <p><input type="text" name="email_contact" id="email_contact" placeholder="Email" value="" class="required email"></p>
                <p><textarea name="message" id="message" placeholder="Your Message" class="required" style="height:120px;"></textarea></p>
                <p class="pull-right"><a href="" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;<a href="" class="btn-green submit" data-analytics-label="Submit Form: Contact Us">Submit</a></p>
            </fieldset>
        </form>
    </div>
</div>
<div class="push"></div>
    <?php include('includes/footer.php'); ?>
