
    		<div class="form-group">
    			<label for="campaign_name" class="col-md-3 control-label">Campaign Name</label>
    			<div class="col-md-8">
    			<input type="text" name='campaign_name' class="form-control required" id="campaign_name" placeholder="Enter Campaign Name *" value="" maxlength="40">
    			</div>
    		</div>

    		<!-- <div class="form-group"> ---- this field was not in Campaign's ux form fields 
    			<label for="tag" class="col-md-3 control-label">Campaign Tag</label>
    			<div class="col-md-8">
    			<input type="text" class="form-control" id="tag" placeholder="Enter Tag" value="<?php echo $_SESSION['tag']; ?>" maxlength="40">
    			</div>
    		</div> -->
			
    		<div class="form-group">
				<label for="geo" class="control-label col-md-3">Target GEO</label>
				<div class="controls col-md-8">
					<!-- <label class="checkbox-inline first-checkbox" style="margin-right:13px;"> <input type="checkbox" id="selectall" name="selectall" value="" class="checkall"> World Wide</label>
					<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="APJ"  checked class="check-geo required"> APJ</label>
					<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="EMEA"  class="check-geo required"> EMEA</label>
					<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="LAR"   class="check-geo required"> LAR</label>
					<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="NAR"   class="check-geo required"> NAR</label>
					<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="PRC" 	 class="check-geo required"> PRC</label> -->
				
					<?php
						$geo_checkboxes = array("APJ", "EMEA","LAR","NAR","PRC",); $geo_checkboxes_size = sizeof($geo_checkboxes);

			            $query = "SELECT * FROM users WHERE id = ".$_SESSION['user_id']." ";
			            $result = $mysqli->query($query);
			            while ($obj = $result->fetch_object()) { $result_geo = $obj->geo; }
		                $geo_array = explode(", ", $result_geo); $total_geo = sizeof($geo_array);

		                if($total_geo === $geo_checkboxes_size)
		                	echo '<label class="checkbox-inline first-checkbox" style="margin-right:13px;"> <input type="checkbox" id="selectall" name="selectall" value="" class="checkall" checked> World Wide</label>';
		                else
		                	echo '<label class="checkbox-inline first-checkbox" style="margin-right:13px;"> <input type="checkbox" id="selectall" name="selectall" value="" class="checkall"> World Wide</label>';                

		                foreach ($geo_checkboxes as &$geo_checkbox) {
		                	$geo_flag = 0;
		                    foreach ($geo_array as &$geo) {                    	
		                    	if($geo_checkbox===$geo)
		                    		$geo_flag = 1;                    	
		                    }
		                    if($geo_flag){ echo '<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="'.$geo_checkbox.'" class="check-geo required" checked> '.$geo_checkbox.'</label>';	}
		                	else{ echo '<label class="checkbox-inline"> <input type="checkbox"  name="geo[]" value="'.$geo_checkbox.'" class="check-geo required"> '.$geo_checkbox.'</label>'; }                    
		                }
		            ?>
	            </div>
			</div>

    		<div class="form-group">
						<label for="sector" class="col-md-3 control-label">Sector</label>
						<div class="col-md-8">
						<select id="sector" name="sector" class="form-control required">
								<option value="">Select One *</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="subsector" class="col-md-3 control-label">Sub-Sector</label>
						<div class="col-md-8">
						<select id="subsector" name="subsector" class="form-control required">
								<option value="">Select Sector First *</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="subsector" class="col-md-3 control-label"></label>
						<div class="col-md-8">
						<input type="text" id="subsector_other" name="subsector_other" class="form-control input-note" placeholder="Please provide a specific sub-sector" value="" style="display:none;">
						</div>
					</div>

					<div class="form-group">
					<label for="add-addicional-info-lnk" class="col-md-3 control-label"></label>
					<div class="col-md-9" style="padding-left:0">
						<a class="lnk col-md-8"  id="add-addicional-info-lnk" href="javascript:void(0);"><i class="fa fa-plus"></i> Add addicional information</a>
						</div>
					</div>

					<div id='campaign_addicional_details' style="display:none">
					<div class="form-group">
							<label for="campaign_cta" class="control-label col-md-3">Campaign Call to Action(s)/Offer(s)</label>
							<div class='col-md-8'>
							<div class="controls"><textarea id="campaign_cta" name="campaign_cta" class=" form-control textarea-medium" placeholder="What is the customer responding to, what was being offered to drive lead to the site, why are they coming to intel.com?  If event, webinar, etc. where a list was gathered and input, what was committed when capturing name?"><?php echo getFieldValue('target_audience', isset($_SESSION['clone_campaign_id']) ? $_SESSION['clone_campaign_id'] : 0, 8, 'textarea'); ?></textarea><i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="Example: We incorporated a number of conversion opportunities throughout the site, titled 'We're here to help, Ask an Expert.' Linked to a new Aprimo form."></i></div>
						</div>
						</div>

					<div class="form-group">
							<label for="target_audience" class="control-label col-md-3">Target Audience</label>
							<div class='col-md-8'>
							<div class="controls"><textarea id="target_audience" name="target_audience" class="form-control textarea-medium" placeholder="List all. Primary and secondary."></textarea><i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="Example: The tactic is targeted for all Education audiences: Education IT Managers, Educators/Teachers, Government Officials/MOE"></i></div>
							</div>
						</div>

						<div class="form-group">
							<label for="start_date" class="control-label col-md-3">Campaign Launch</label>
							<div class="controls col-md-3">
								<div id="start_date" class="input-group margin-bottom-sm" style="">
								<input type="text" id="datetimepicker" name="start_date" class="form-control" value="">
								<span class="input-group-addon" style="margin-right:2px;"><i class=" fa fa-th"></i></span>
								</div>
								
							</div>
						</div>



						<div class="form-group">
							<label for="end_date" class="control-label col-md-3">Campaign End</label>
							<div class="controls col-md-3">
						<div id="end_date" class="input-group margin-bottom-sm">
						<input type="text" id="datetimepicker_end" name="end_date" class="input-medium"value="">
						<span class="input-group-addon"><i class=" fa fa-th"></i></span>
						</div>
								
							</div>
						</div>

						<div class="form-group">
							<label for="generate_leads" class="control-label col-md-3">Is this campaign meant to generate leads?</label>
							<div class="controls col-md-8">
								<label class="radio-inline"> <input type="radio" id='generate-leads-true'  name="generate_leads[]" val="1" >Yes</label>
								<label class="radio-inline"> <input type="radio" id='generate-leads-false' name="generate_leads[]" val="0" checked> No</label><br />
							</div>
						</div>
						<div id="generate-leads-fields" style='display:none'>
							<div class="form-group">
								<label for="campaign_objectives" class="control-label col-md-3">Campaign Objectives</label>
								<div class="controls col-md-8"><textarea id="campaign_objectives" name="campaign_objectives" class="form-control textarea-medium" placeholder="Provide any details about the campaign objectives that are not already captured by the Geo, Sector, and Target Audience fields."></textarea><i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="Example:<br />-Provide a holistic, integrated, world-class user experience that establishes Intel as an education solutions and technology thought-leader.<br />-Engage customers throughout their journey by guiding them through awareness and preference to decision &amp; purchase."></i></div>
							</div>
							<div class="form-group">
								<label for="talking_points" class="control-label col-md-3">Lead Talking Points</label>
								<div class="controls col-md-8"><textarea id="talking_points" name="talking_points" class="form-control textarea-medium" placeholder="Provide any specific questions or resources that might help the OSA or OSAM engage with the customer in a follow-up call or email."></textarea><i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="Example:<br />-Which of the following challenges are you experiencing?<br />-Which group/office is most responsible for influencing the purchase of technologies for the classroom?<br />-In considering purchase decisions, which features are most important for your IT environment."></i></div>
							</div>
							<div class="form-group">
								<label for="reference_material" class="control-label col-md-3">Reference Material</label>
								<div class="controls col-md-8"><textarea id="reference_material" name="reference_material" class="form-control textarea-medium" placeholder="Provide links to any additional collateral, reference materials, or campaign assets existing outside of Eloqua that might assist the OSA or OSAM when following up on a lead."></textarea><i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="Example:<br />Prospect initiated contact request based on: Referring page - http://sample.URL.com.<br /><br />Page content: Explore teaching and learning hardware and software from Intel and education partners.<br /><br />Technology teachers are using: Interactive Board."></i></div>
							</div>
						</div>

</div>


