<?php
$clone_campaign_aviable=false;
$loc = 'request';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>
<div class='container'>

	<div class='row'>
		<div class='col-md-12'>
			<div class='request-header'>
				<h2>Submit a Request</h2>
				<h4>
					Choose the type of request you wish to submit, and a<br/>
					production support team will assist in fulfilling your request.
				</h4>
			</div>
		</div>
	</div>
	
	<div class='row row-centered'>

		<div class='col-md-3 col-centered box-button-container'>
		<a href='#campaign-setup' data-toggle='modal' data-backdrop="static" data-target='#campaign-setup' class="lnk new-campaign-setup">
			<div class='box-button' val='campaign'>
				<div class='circled'>
					<div class="icon campaign"></div>
				</div>
				<h3>Create a<br> <span class='highlight'>Campaign</span></h3>
			</div>
		</a>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<a href='list' class="lnk">
				<div class='box-button' val='list-lg'>
					<div class='circled'>
						<div class="icon list-lg" style='height:34px !important'></div>
					</div>
					<h3>Upload a<br> <span class='highlight'>List</span></h3>
				</div>
			</a>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<a href='email' class="lnk">
				<div class='box-button' val='mail-lg'>
					<div class='circled'>
						<div class="icon mail-lg"></div>
					</div>
					<h3>Create a<br> <span class='highlight'>Single Email</span></h3>
				</div>
			</a>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<a href='webinar' class="lnk">
				<div class='box-button' val='webinar-lg'>
					<div class='circled'>
					<div class="icon webinar-lg"></div>
					</div>
					<h3>Create a<br> <span class='highlight'>Webinar</span></h3>
				</div>
			</a>
		</div>		
	</div>

	<div class='row row-centered'  style='margin-bottom:100px'>
		<div class='col-md-3 col-centered box-button-container'>
			<a href='newsletter' class="lnk">
				<div class='box-button' val='newsletter-lg'>
					<div class='circled'>
					<div class="icon newsletter-lg"></div>
				</div>
					<h3>Create a<br> <span class='highlight'>Newsletter</span></h3>
				</div>
			</a>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<a href='landingpage' class="lnk">
				<div class='box-button' val='page-lg'>
					<div class='circled'>
					<div class="icon page-lg"></div>
				</div>
					<h3>Create a<br> <span class='highlight'>Landing Page/Form</span></h3>
				
				</div>
			</a>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<a href='enurture' class="lnk">
				<div class='box-button' val='nurture-lg'>
					<div class='circled'>
					<div class="icon nurture-lg"></div>
				</div>
					<h3 class='logo'>Create a<br> <span class='highlight'>Nurture Series</span></h3>
				</div>
			</a>
		</div>
</div>

	<?php include('includes/campaign-details.php'); ?>	
 
</div>



<div class="push"></div>
<?php include('includes/footer.php'); ?>

 <?php if($clone_campaign_aviable==false){ ?>
<script type="text/javascript">
	$('#new-campaign-type').css('display','none');
	$('#existing-campaign').hide();
    $('#campaign-form-fields').show();
</script>

<?php  }?>