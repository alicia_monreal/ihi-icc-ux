<style type='text/css'>
  .module-file{display:none;}
</style>
<!--[if IE 8]>
<style type='text/css'>
  .module-file{filter: alpha(opacity=0) !important; cursor:pointer !important; position: absolute !important; height: 90px !important; width: 180px !important;display:block !important; font-size: 80px; top:0; right:0;}
</style>
<![endif]-->

<?

$user_agent = $_SERVER["HTTP_USER_AGENT"];      // Get user-agent of browser

$safari_or_chrome = strpos($user_agent, 'Safari') ? true : false;     
$chrome = strpos($user_agent, 'Chrome') ? true : false;             

if($safari_or_chrome == true && $chrome == false){
    $safari = true; 
}

if($safari == true) {
    ?>
    <style type='text/css'>
  .module-file{opacity: 0 !important; cursor:pointer !important; position: absolute !important; display:block !important; font-size: 80px; top:0; right:0;}
    </style>
    <?
}     

?>
<!-- Default clone element -->
<script type="text/template" id="tpl-rp-page-events" class="tpl-rp-comp-clone"><div style="position:relative;clear:both;margin-top:20px;"><div class="inner-copy" data-parent="tpl-rp-page-events">
    <h4 class="wys-inline-simple">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

    <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque porro similique voluptate.</p>

    <a class="mobile-cta wys-inline-link" href="">Read More <span class="hide">&gt;</span></a>
</div>
<div class="image-container">
    <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
    <label for="fileUpload">
        <img src="img/templates/default-images/placeholders/events-fpo.png" data-custom="events-custom.png" data-default="events-fpo.png" class="image-box-module" alt="">
    </label>
</div>
</div></script>
<!-- END Default clone element -->

<!-- Upcoming Events -->
<div class="content-wrapper">
    <div class="main-content cf">
        <div class="module upcoming-events cf">
            <div class="module-header">
                <h3 class="icon-sm blue-location-sm content-header wys-inline-simple">Upcoming Events</h3>
            </div>

            <div class="module-content">
                <div class="content cf tpl-rp-tab" tpl-rp-data="div">
                    <div style="position:relative;clear:both;">
                        <div class="inner-copy tpl-rp-comp" data-parent="tpl-rp-page-events">
                            <h4 class="wys-inline-simple">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

                            <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque porro similique voluptate.</p>

                            
                            <a class="mobile-cta wys-inline-link" href="">Read More <span class="hide">&gt;</span></a>
                        </div>
                        <div class="image-container">
                            <input type="file"  style="height: 178px !important; width: 336px !important;" id="fileupload" class="module-file"/>
                            <label for="fileUpload">
                                <img src="img/templates/default-images/placeholders/events-fpo.png" data-custom="events-custom.png" data-default="events-fpo.png" class="image-box-module" alt="">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
