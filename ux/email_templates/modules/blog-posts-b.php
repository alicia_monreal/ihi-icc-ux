 <!-- Default clone element -->
<script type="text/template" id="tpl-rp-blog-posts-b" class="tpl-rp-comp-clone"> 
<td class="show-me tpl-rp-comp" data-parent="tpl-rp-blog-posts-b" tpl-rp-style="0px 0px 0px 0px" style="border-bottom:1px #cccccc solid; padding:25px 0px 0px 0px;" class="no-border">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
        <tbody>

        <tr>
            <td style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 20px 0px;" class="mobile-subhead wys-inline-simple">Featured Content Article Subhead</td>
        </tr>
        <tr>
            <td valign="top" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px;padding:0px 0px 25px 0px;height:116px" class="module-content">
                <table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                    <tbody><tr>
                    <td align="top" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 10px 0px 0px;" valign="top" width="150" height="100">
                        <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                        <label for="fileUpload">
                            <img src="img/templates/default-images/placeholders/company-fpo.png" data-custom="company-fpo.png" data-default="events-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="150" height="100">
                        </label>

                    </td>
                        <td valign="top" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:21px;" class="module-content block wys-inline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</td>

                    </tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td style="color:#0071C5; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 0px 0px;font-weight:bold" class="mobile-cta-container-td">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mobile-cta-container last">
                    <tbody><tr>
                        <td style="font-size:14px; line-height:18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="mobile-cta">
                            <a style="font-size:14px; line-height:18px;color:#0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;text-decoration:none;" class="mobile-cta-text wys-inline-link" href="#">Read More<span style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="hide"> &gt;</span></a>


                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>

    </tbody></table>
</td>
</script>
<table width="429" cellspacing="0" cellpadding="0" border="0" align="left" style="background-color: #FFFFFF; border-collapse: collapse;" class="content-column">
    <tbody>
           <tr> <td valign="top" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:18px; padding-top: 0px; padding-left: 25px; padding-right: 25px; text-align:left;width:100%;padding-bottom:10px" class="content-item">
                <table class="full-width tpl-rp-tab" tpl-rp-data="1" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                    <tbody>
                        <!--Content Block-->
                        <tr>
                            <td class="show-me tpl-rp-comp" data-parent="tpl-rp-blog-posts-b" tpl-rp-style="0px 0px 0px 0px" style="border-bottom:1px #cccccc solid; padding:25px 0px 30px 0px;" class="no-border">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                                    <tbody><tr>
                                        <td style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:28px;padding:0px 0px 20px 0px;" class="module-header"><a style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:28px;text-decoration:none;" class="wys-inline-link" name="blog">Blog</a></td>
                                    </tr>

                                    <tr>
                                        <td style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 20px 0px;" class="mobile-subhead wys-inline-simple">Featured Content Article Subhead</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px;padding:0px 0px 25px 0px;height:116px" class="module-content">
                                            <table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                                                <tbody><tr>
                                                <td align="top" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 10px 0px 0px;" valign="top" width="150" height="100">
                                                    <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                                                    <label for="fileUpload">
                                                        <img src="img/templates/default-images/placeholders/company-fpo.png" data-custom="company-fpo.png" data-default="events-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="150" height="100">
                                                    </label>

                                                </td>
                                                    <td valign="top" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:21px;" class="module-content block wys-inline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</td>

                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color:#0071C5; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 0px 0px;font-weight:bold" class="mobile-cta-container-td">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mobile-cta-container last">
                                                <tbody><tr>
                                                    <td style="font-size:14px; line-height:18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="mobile-cta">
                                                        <a style="font-size:14px; line-height:18px;color:#0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;text-decoration:none;" class="mobile-cta-text wys-inline-link" href="#">Read More<span style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;" class="hide"> &gt;</span></a>


                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>

                                </tbody></table>
                            </td>
                        </tr>
                        <!--END CONTENT BLOCK-->
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>