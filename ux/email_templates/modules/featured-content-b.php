 <!-- Default clone element -->
<script type="text/template" id="tpl-rp-featured-content-b" class="tpl-rp-comp-clone"> 
<td class="show-me tpl-rp-comp" data-parent="tpl-rp-featured-content-b" tpl-rp-style="0px 0px 0px 0px" style="border-bottom:1px #cccccc solid; padding:25px 0px 0px 0px;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
        <tbody>
        <tr>
            <td class='hide' style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 20px 0px;">Featured Content Article Subhead</td>
        </tr>
        <!--[if !mso]><!-->

        <tr class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;">
            <td class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;color: #FFFFFF; background-color#FFFFFF;">
                <img src="http://preview.ozoneonline.com/creative/INT_v22/images/1.png" style="display:block;border:0px;width:0px; height:0px;" class="module-img" alt="">
            </td>


        </tr>

        <!--<![endif]-->

        <tr>
            <td valign="top" class="module-content" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px;padding:0px 0px 25px 0px;height:116px">
                <table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                    <tbody><tr>
                        <td valign="top" class="module-content block wys-inline" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:21px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</td>
                        <td align="center" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 0px 10px 0px;" valign="top" width="150" height="100">
                            <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                            <label for="fileUpload">
                                <img src="img/templates/default-images/placeholders/company-fpo.png" data-custom="company-fpo.png" data-default="events-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="150" height="100">
                            </label>

                        </td>
                    </tr>

                </tbody></table>
            </td>
        </tr>
        <tr>
            <td class="mobile-cta-container-td" style="color:#0071C5; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 30px 0px;font-weight:bold">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mobile-cta-container">
                    <tbody><tr>
                        <td class="mobile-cta" style="font-size:14px; line-height:18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
                            <a href="#" class="mobile-cta-text wys-inline-link" style="font-size:14px; line-height:18px;color:#0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;text-decoration:none;">Read More<span class="hide" style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;"> &gt;</span></a>


                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>

    </tbody></table>
</td>
</script>
<!-- END Default clone element -->

<table width="429" cellspacing="0" cellpadding="0" border="0" align="left" class="content-column" style="background-color: #FFFFFF; border-collapse: collapse;">
<tbody>
       <tr> <td valign="top" class="content-item" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:18px; padding-top: 0px; padding-left: 25px; padding-right: 32px; padding-bottom:10px; text-align:left;width:100%;">
            <table width="100%" class="full-width tpl-rp-tab" tpl-rp-data="1" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                <tbody>
                    <!--Content Block-->
                    <tr >
                        <td class="show-me tpl-rp-comp" data-parent="tpl-rp-featured-content-b" tpl-rp-style="0px 0px 0px 0px" style="border-bottom:1px #cccccc solid; padding:25px 0px 0px 0px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                                <tbody><tr>
                                    <td class="module-header" style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:28px;padding:0px 0px 20px 0px;"><a name="featuredcontent" class="wys-inline-link" style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:28px;;text-decoration:none;">Featured Content</a></td>
                                </tr>

                                <tr>
                                    <td class='hide' style="color:#007DC6; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 20px 0px;">Featured Content Article Subhead</td>
                                </tr>
                                <!--[if !mso]><!-->

                                <tr class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;">
                                    <td class="hide-element" style="display: none; width: 0; max-height: 0; overflow: hidden;color: #FFFFFF; background-color#FFFFFF;">
                                        <img src="http://preview.ozoneonline.com/creative/INT_v22/images/1.png" style="display:block;border:0px;width:0px; height:0px;" class="module-img" alt="">
                                    </td>


                                </tr>

                                <!--<![endif]-->

                                <tr>
                                    <td valign="top" class="module-content" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px;padding:0px 0px 25px 0px;height:116px">
                                        <table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                                            <tbody><tr>
                                                <td valign="top" class="module-content block wys-inline" style="color:#5F5F5E; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:21px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</td>
                                                <td align="center" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 0px 10px 0px;" valign="top" width="150" height="100">
                                                    <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                                                    <label for="fileUpload">
                                                        <img src="img/templates/default-images/placeholders/company-fpo.png" data-custom="company-fpo.png" data-default="events-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="150" height="100">
                                                    </label>

                                                </td>
                                            </tr>

                                        </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mobile-cta-container-td" style="color:#0071C5; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px;padding:0px 0px 30px 0px;font-weight:bold">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mobile-cta-container">
                                            <tbody><tr>
                                                <td class="mobile-cta" style="font-size:14px; line-height:18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
                                                    <a href="#" class="mobile-cta-text wys-inline-link" style="font-size:14px; line-height:18px;color:#0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;text-decoration:none;">Read More<span class="hide" style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;"> &gt;</span></a>


                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>

                            </tbody></table>
                        </td>
                    </tr>
                    <!--END CONTENT BLOCK-->
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>