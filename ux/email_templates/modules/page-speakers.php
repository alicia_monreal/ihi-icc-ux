<style type='text/css'>
  .module-file{display:none;}
</style>
<!--[if IE 8]>
<style type='text/css'>
  .module-file{filter: alpha(opacity=0) !important; cursor:pointer !important; position: absolute !important; height: 90px !important; width: 180px !important;display:block !important; font-size: 80px; top:0; right:0;}
</style>
<![endif]-->

<?

$user_agent = $_SERVER["HTTP_USER_AGENT"];      // Get user-agent of browser

$safari_or_chrome = strpos($user_agent, 'Safari') ? true : false;     
$chrome = strpos($user_agent, 'Chrome') ? true : false;             

if($safari_or_chrome == true && $chrome == false){
    $safari = true; 
}

if($safari == true) {
    ?>
    <style type='text/css'>
  .module-file{opacity: 0 !important; cursor:pointer !important; position: absolute !important; display:block !important; font-size: 80px; top:0; right:0;}
    </style>
    <?
}     

?>

<!-- Default clone element -->
<script type="text/template" id="tpl-rp-page-speakers" class="tpl-rp-comp-clone"><div class="speaker">
    <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
    <label for="fileUpload">
        <img src="img/templates/default-images/placeholders/speaker-fpo.png" data-custom="speaker-custom.png" data-default="speaker-fpo.png" class="image-box-module" alt="">
    </label>
    <h4 class="wys-inline-simple">Name of Speaker</h4>
    <h5 class="wys-inline-simple">Title of speaker</h5>

    <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
</div></script>
<!-- END Default clone element -->


<!-- Speakers - Three Col -->
<div class="content-wrapper">
    <div class="main-content cf">
        <div class="module speakers-three-col cf">
            <div class="module-header">
                <h3 class="icon-sm blue-comments-sm content-header wys-inline-simple">Speakers</h3>
            </div>

            <div class="module-content">
                <div class="content cf tpl-rp-tab" tpl-rp-data="div">
                    <div class="speaker tpl-rp-comp" data-parent="tpl-rp-page-speakers">
                        <div style="position:relative;display:inline-block; max-width:180px;">
                            <input type="file" id="fileupload" style="height: 110px !important; width: 180px !important;" class="module-file"/>
                            <label for="fileUpload">
                                <img src="img/templates/default-images/placeholders/speaker-fpo.png" data-custom="speaker-custom.png" data-default="speaker-fpo.png" class="image-box-module" alt="">
                            </label>
                        </div>
                        <h4 class="wys-inline-simple">Name of Speaker</h4>
                        <h5 class="wys-inline-simple">Title of speaker</h5>

                        <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
                    </div>

                    <!--ADD THESE DYNAMICALLY-->
                    <!--<div class="speaker">
                        <img src="img/templates/default-images/placeholders/speaker-fpo.png" alt="">

                        <h4 class="wys-inline-simple">Name of Speaker</h4>
                        <h5 class="wys-inline-simple">Title of speaker</h5>

                        <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
                    </div>

                    <div class="speaker last">
                        <img src="img/templates/default-images/placeholders/speaker-fpo.png" alt="">

                        <h4 class="wys-inline-simple">Name of Speaker</h4>
                        <h5 class="wys-inline-simple">Title of speaker</h5>

                        <p class="wys-inline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta sapiente, quasi rerum.</p>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
