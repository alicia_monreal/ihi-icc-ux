<!-- Default clone element -->

<style type='text/css'>
  .module-file{display:none;}
</style>
<!--[if IE 8]>
<style type='text/css'>
  .module-file{filter: alpha(opacity=0) !important; cursor:pointer !important; position: absolute !important; height: 90px !important; width: 180px !important;display:block !important; font-size: 80px; top:0; right:0;}
</style>
<![endif]-->

<?

$user_agent = $_SERVER["HTTP_USER_AGENT"];      // Get user-agent of browser

$safari_or_chrome = strpos($user_agent, 'Safari') ? true : false;     
$chrome = strpos($user_agent, 'Chrome') ? true : false;             

if($safari_or_chrome == true && $chrome == false){
    $safari = true; 
}

if($safari == true) {
    ?>
    <style type='text/css'>
  .module-file{opacity: 0 !important; cursor:pointer !important; position: absolute !important; display:block !important; font-size: 80px; top:0; right:0;}
    </style>
    <?
}     

?>

<script type="text/template" id="tpl-rp-speakers" class="tpl-rp-comp-clone"><td class="cellblock" tpl-rp-style="0px 32px 0px 0px" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px; margin: 0; padding: 0px 32px 0px 0px;" align="left" valign="top">
    <!-- Start Speaker Column -->
    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;">
        <tbody><tr>
            <td class="content-span3-left" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px 0px 10px 0px;" align="left" valign="top">
                <div style="position:relative;display:inline-block; max-width:180px;">
                        <input type="file" id="fileupload" class="module-file"/>
                    <label for="fileUpload">
                        <img src="img/templates/default-images/placeholders/speaker-fpo.png" data-custom="speaker-custom.png" data-default="speaker-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="180" height="110">
                    </label>
                </div>
                <a href="javascript:void(0)" id="filename" style="cursor:pointer;margin-top:15px;text-decoration:none;"></a>                
            </td>
        </tr>
        <tr>
            <td class="title-speakers-td-top wys-inline-simple" style="font-size: 14px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;   line-height:16px;margin: 0; padding: 10px 0px 8px 0px;" align="left" valign="top">Name of Speaker</td>
        </tr>
        <tr>
            <td class="title-speakers-td-bottom wys-inline-simple" style="font-size: 14px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal;line-height:16px;margin: 0; padding: 0px 0px 22px 0px;" align="left" valign="top">Title of Speaker
            </td>
        </tr>
        <tr>
            <td class="last normal_copy_wrapper speakers-td-bottom wys-inline" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;line-height:20px;margin: 0; padding: 0px 0px 00px 0px;" align="left" valign="top">
                Amet conse ctetu er adipiscing emet conse ctetu er adipiscing adipiscing elilit.</td>
        </tr>
    </tbody></table>
    <!-- End Speaker Column -->
</td></script>
<!-- END Default clone element -->


<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0;">
<tbody><tr>
    <!-- Container -->

    <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0;">

        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
            <tbody><tr>
                <td bgcolor="#ffffff" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                    <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                        <tbody><tr>
                            <td class="full-width" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                <!-- Text Column -->

                                <table border="0" cellpadding="0" class="full-width" cellspacing="0" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                    <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                    <tbody><tr>
                                        <td class="content-td" colspan="2" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 28px 32px 22px 32px;" align="left" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" class="content-title-icon" style="margin-bottom: 0 !important; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">
                                                <tbody><tr>
                                                    <td class="title-icon" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;width: 26px; height: 26px; margin: 0; padding: 0px 12px 2px 0px;" align="left" valign="top">
                                                        <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{10b6672c-d126-4063-a48c-003ac908d7a0}_intel_icons_b_interactpay_comments.png" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block; width: 26px; height: 26px;">
                                                    </td>
                                                    <td class="title-text wys-inline-simple" style="font-size: 24px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:24px;margin: 0; padding: 2px 0px 10px 0px;" align="left" valign="top">Speakers</td>
                                                    <td class="hideblock" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: right; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: 600;   line-height:27px;margin: 0; padding: 0;" align="right" valign="top"><a href="#" style="font-size: 12px; color: #00AEEF; text-decoration: none; font-weight: 600;" class="wys-inline-link">Speakers CTA &gt;</a>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="show-me" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px 32px 32px 32px;" align="left" valign="top">
                                            <table class="full-width tpl-rp-tab" tpl-rp-data="3" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                                                <tbody><tr>
                                                    <td class="cellblock tpl-rp-comp" data-parent="tpl-rp-speakers" tpl-rp-style="0px 32px 0px 0px" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px; margin: 0; padding: 0px 32px 0px 0px;" align="left" valign="top">
                                                        <!-- Start Speaker Column -->
                                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;width:100%;">
                                                            <tbody><tr>
                                                                <td class="content-span3-left" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;   line-height:27px;margin: 0; padding: 0px 0px 10px 0px;" align="left" valign="top">
                                                                    <div style="position:relative;display:inline-block; max-width:180px;">
                                                                        <input type="file" id="fileupload" style="height: 110px !important; width: 180px !important;" class="module-file" />
                                                                        <label for="fileUpload">
                                                                            <img src="img/templates/default-images/placeholders/speaker-fpo.png" data-custom="speaker-custom.png" data-default="speaker-fpo.png" class="image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="180" height="110">
                                                                        </label>
                                                                    </div>
                                                                    <a href="javascript:void(0)" id="filename" style="cursor:pointer;margin-top:15px;text-decoration:none;"></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="title-speakers-td-top wys-inline-simple" style="font-size: 14px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold;   line-height:16px;margin: 0; padding: 10px 0px 8px 0px;" align="left" valign="top">Name of Speaker</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="title-speakers-td-bottom wys-inline-simple" style="font-size: 14px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal;line-height:16px;margin: 0; padding: 0px 0px 22px 0px;" align="left" valign="top">Title of Speaker
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="last normal_copy_wrapper speakers-td-bottom wys-inline" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;line-height:20px;margin: 0; padding: 0px 0px 00px 0px;" align="left" valign="top">
                                                                    Amet conse ctetu er adipiscing emet conse ctetu er adipiscing adipiscing elilit.</td>
                                                            </tr>
                                                        </tbody></table>
                                                        <!-- End Speaker Column -->
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>

                            <!-- /wrapper -->

                        </tr>
                    </tbody></table>

                    <!-- /row -->
                </td>
            </tr>
        </tbody></table>

    </td>

    <!-- /Container -->

</tr>
</tbody></table>
<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
