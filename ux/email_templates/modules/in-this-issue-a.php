
<!-- In This Issue Module -->
<table border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; width: 100%;">
    <tr>
        <!-- Container -->
        <td align="center" valign="top" class="devicewidth" style="width: 670px; vertical-align: top; text-align: center;">

            <table border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width: 670px; border-collapse: collapse;" align="center">
                <tr>
                    <td bgcolor="#ffffff" style="vertical-align: top; text-align: left;" align="left" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" class="no-padding" style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td class="cellblock" style="vertical-align: top; text-align: left;" align="left" valign="top">
                                    <!-- Text Column -->

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                        <tr>
                                            <td class="content-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 30px 0px 32px 30px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" class="container anchor-links-container" style="width: 100%; border-right-color: #d5d5d5; border-right-width: 1px; border-right-style: solid; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">
                                                    <tr>
                                                        <td class="anchor-links-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: right; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0 20px 34px 0;" align="right" valign="top">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <!-- Start In This Issue Anchor Links -->
                                                                <tr>
                                                                    <td class="anchor-links-td wys-inline-simple" style="font-size: 30px; border-collapse: collapse !important; vertical-align: top; text-align: right; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: right; font-weight: normal; line-height: 30px; margin: 0; padding: 0 20px 30px 0;" align="right" valign="top">In This Issue</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="anchor-links" style="font-size: 16px; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: right; line-height: 170%; margin: 0; padding: 0;" align="right"><a href="#HotTopic" style="font-size: 16px; color: #0071C5; text-decoration: none;" class="wys-inline-link">Hot Topic Article &gt;</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="anchor-links" style="font-size: 16px; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: right; line-height: 170%; margin: 0; padding: 0;" align="right"><a href="#Featured" style="font-size: 16px; color: #0071C5; text-decoration: none;" class="wys-inline-link">Featured Content &gt;</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="anchor-links" style="font-size: 16px; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: right; line-height: 170%; margin: 0; padding: 0;" align="right"><a href="#Editorial" style="font-size: 16px; color: #0071C5; text-decoration: none;" class="wys-inline-link">Editorial Article &gt;</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="anchor-links" style="font-size: 16px; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: right; line-height: 170%; margin: 0; padding: 0;" align="right"><a href="#Help" style="font-size: 16px; color: #0071C5; text-decoration: none;" class="wys-inline-link">Help Article &gt;</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="anchor-links" style="font-size: 16px; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: right; line-height: 170%; margin: 0; padding: 0;" align="right"><a href="#Events" style="font-size: 16px; color: #0071C5; text-decoration: none;" class="wys-inline-link">Events &amp; Webinars &gt;</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="anchor-links no-border" style="font-size: 16px; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: right; line-height: 170%; margin: 0; padding: 0 0 50px 0;" align="right"><a href="#Blog" style="font-size: 16px; color: #0071C5; text-decoration: none;" class="wys-inline-link">Blog &gt;</a></td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <!-- /wrapper -->

                                <td class="cellblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Sidebar Column -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 416px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">


                                        <a id="HotTopic"></a>
                                        <!-- Begin Hot Topic Article -->
                                        <tr>
                                            <td class="content-td sub-field" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 30px 60px 25px 38px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" class="class">
                                                    <tr>
                                                        <td class="hot-topic wys-inline-simple" style="font-size: 18px; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; line-height: 18px; padding: 20px 0px 14px 0px;" align="left">Hot Topic Article
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <!-- <td style="font-size: 18px; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; width:320px; height:143px;">
                                                            <img src="http://www.ironhorseinteractive.com/eloqua-templates/img/hot-topic-fpo-2.jpg" class="hot-topic image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;width:320px; height:143px;">
                                                        </td> -->

                                                        <td align="center" style="font-size: 12px;  border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height:20px;margin: 0; padding: 0px 0px 10px 0px;" valign="top" width="286" height="178">
                                                            <!-- Map Image -->
                                                            <input type="file"  style="display:none;" id="fileupload" class="module-file"/>
                                                            <label for="fileUpload">
                                                                <img src="img/templates/default-images/placeholders/grocer-fpo.png" data-custom="grocer-fpo.png" data-default="grocer-fpo.png" class="hot-topic image-fix image-box-module" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="286" height="178">
                                                            </label>

                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="last normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height:22px;padding: 21px 0px 0px 0px;" align="left">Itaque earum rerum hic tenetur a sapiente delectus, ut aut ut aut reiciendis.Itaque earum rerum hic tenetur a sapiente delectus, ut aut ut aut reiciendis.</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>

                                        <!-- Read More Link -->
                                        <tr>
                                            <td class="content-td sub-field" style="padding: 0px 30px 25px 0px;">
                                                <table class="mobile-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="mobile-cta" style="font-size: 12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;padding: 0px 16px 0px 0px;text-align: right;">
                                                            <a class="mobile-cta-text wys-inline-link" style="font-size: 12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;text-decoration:none;">Read More <span class="hideblock" style="font-size:12px; color: #00AEEF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 12px;text-decoration:none;"> &gt;</span></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>

                                    </table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </table>

                        <!-- /row -->
                    </td>
                </tr>
            </table>

        </td>

        <!-- /Container -->

    </tr>
</table>
<!-- End In This Issue Module -->

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
