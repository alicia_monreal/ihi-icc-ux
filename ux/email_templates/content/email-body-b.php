<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tbody><tr>
                    <td bgcolor="#ffffff" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                            <tbody><tr>
                                <td class="cellblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Text Column -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 384px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                        <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                        <tbody><tr>
                                            <td class="content-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 40px 32px 20px 32px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tbody><tr>
                                                        <td class="headline_wrapper" style="font-size: 18px; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; line-height: 18px; padding: 0px 0px 18px 0px;" align="left">First Name,</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal_copy_wrapper wys-inline" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height: 20px; padding: 0px 0px 0px 0px;" align="left" data-name="body"><p>Amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>
                                                            <ul>
                                                                <li>Ut wisi enim ad minim veniam</li>
                                                                <li>Ut wisi enim ad minim veniam</li>
                                                                <li>Ut wisi enim ad minim veniam</li>
                                                                <li>Ut wisi enim ad minim veniam</li>
                                                            </ul>
                                                            <p>Amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.</p>
                                                        </td>
                                                    </tr>
                                                </tbody></table>

                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                                <td class="cellblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; background-color: #0071c5; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Sidebar Column -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 286px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">

                                        <!-- NOTE: Change width based on image/content width -->

                                        <tbody><tr>
                                            <td class="sidebar-td-blue" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;" width="100%">
                                                    <tbody><tr>
                                                        <td class="content-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 61px 40px 80px 40px;" align="left" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody><tr>
                                                                    <td style="font-size:18px;line-height:18px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; padding: 0px 0px 12px 0px;" align="left" class="wys-inline-simple">Location<br /><br />Location<br /><br />Date<br /><br />Time</td>
                                                                </tr>
                                                                <tr>

                                                                    <td style="width: 100%; padding: 0px 0px 0px 0px;">

                                                                        <!-- CTA -->
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="medium-button" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; overflow: hidden;">
                                                                            <tbody><tr>
                                                                                <td style="font-size: 20px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal;width: auto !important; background-color: #fdb813; margin: 0; padding: 14px 10px 14px 20px;" align="left" bgcolor="#fdb813" valign="top">
                                                                                    <table class="inner-button" border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #fdb813;">
                                                                                        <tbody><tr>
                                                                                            <td class="button-text wys-inline-link" style="background-color: #fdb813; color: #ffffff; font-weight: normal; font-family: arial, helvetica, sans-serif; font-size: 20px; line-height:20px; padding-right: 20px; text-align: left;padding-top:0px;"><a href="#" style="font-size:20px; line-height:20px;  color: #ffffff; text-decoration: none;">CTA</a>
                                                                                            </td>
                                                                                            <td width="12" align="left" style="font-size: 21px; line-height:21px;padding-right:15px;"><a href="#" style="font-size: 21px; line-height:21px;"><img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{049eebcb-f1ec-422e-852a-16aeb25d156c}_white-arrow.png" style="display: block;" width="12" height="21" border="0"></a></td>
                                                                                        </tr>
                                                                                    </tbody></table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                        <!-- END CTA -->
                                                                    </td>

                                                                </tr>
                                                            </tbody></table>

                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->
                    </td>
                </tr>
            </tbody></table>
        </td>

        <!-- /Container -->

    </tr>
</tbody></table>
<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->