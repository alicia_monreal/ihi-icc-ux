<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
<tbody><tr>
    <!-- Container -->

    <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
            <tbody><tr>
                <td bgcolor="#ffffff" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                    <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                        <tbody><tr>
                            <td class="cellblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                <!-- Text Column -->

                                <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 384px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">

                                    <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                    <tbody><tr>
                                        <td class="content-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 41px 31px 20px 43px;" align="left" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tbody><tr>
                                                    <td style="font-size: 18px; color: #0071C5; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; line-height: 18px; padding: 0px 0px 18px 0px; " align="left">First Name,
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal_copy_wrapper" style="font-size: 12px; line-height: 20px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; padding: 0px 0px 31px 0px;" align="left">Amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim.
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:100%;font-size12px;font-family: Arial, Helvetica, sans-serif;">
                                                        <!-- Start Asset -->
                                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 100%; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">
                                                            <tbody><tr>
                                                                <td valign="top" style="vertical-align: top !important; width: 51px; font-size: 12px; border-collapse: collapse !important; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left">
                                                                    <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{2342ab18-0a5e-4caa-8e1f-3c472d4b58b6}_intel_icons_b_docs_features_new_document36x36.png" class="image-block" style="font-size: 12px; outline: none; text-decoration: none;   max-width: 100%; display:block; border:0px;">
                                                                </td>
                                                                <td style="font-size: 14px; vertical-align: top; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 14px;" align="left" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody><tr>
                                                                            <td class="subhead_copy_wrapper" style="font-size: 14px;  vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 16px; margin: 0; padding: 0px 0px 8px 0px;">Title of offer
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="normal_copy_wrapper no-bottom-padding" style="font-size: 14px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 16px; margin: 0; padding: 0px 0px 10px 0px;">Title of speaker
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="title-cta last" style="font-size: 16px; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 16px; margin: 0; padding: 0px 0px 30px 0px;">
                                                                                <table class="title-cta-container no-top" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tbody><tr>
                                                                                        <td class="title-cta" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;padding: 0px 32px 0px 0px;text-align: right;">
                                                                                            <a href="#" class="title-cta-text" style="font-weight: bold; font-size: 16px; color: #5F5F5E; text-decoration: none;">Download &gt;</a>


                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                            </td>


                                                                        </tr>

                                                                    </tbody></table>

                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                        <!-- End Asset -->
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td style="width:100%;font-size12px;font-family: Arial, Helvetica, sans-serif;">
                                                        <!-- Start Asset -->
                                                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 100%; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">
                                                            <tbody><tr>
                                                                <td valign="top" style="vertical-align: top !important; width: 51px; font-size: 12px; border-collapse: collapse !important; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left">
                                                                    <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{2342ab18-0a5e-4caa-8e1f-3c472d4b58b6}_intel_icons_b_docs_features_new_document36x36.png" class="image-block" style="font-size: 12px; outline: none; text-decoration: none;   max-width: 100%; display:block; border:0px;">
                                                                </td>
                                                                <td style="font-size: 14px; vertical-align: top; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 14px;" align="left" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody><tr>
                                                                            <td class="subhead_copy_wrapper" style="font-size: 14px;  vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 16px; margin: 0; padding: 0px 0px 8px 0px;">Title of offer
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="normal_copy_wrapper no-bottom-padding" style="font-size: 14px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0071C5; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 16px; margin: 0; padding: 0px 0px 10px 0px;">Title of speaker
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="title-cta" style="font-size: 16px; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 16px; margin: 0; padding: 0px 0px 30px 0px;">
                                                                                <table class="title-cta-container no-top" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tbody><tr>
                                                                                        <td class="title-cta" style="font-size: 12px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;padding: 0px 32px 0px 0px;text-align: right;">
                                                                                            <a href="#" class="title-cta-text" style="font-weight: bold; font-size: 16px; color: #5F5F5E; text-decoration: none;">Download &gt;</a>


                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                            </td>


                                                                        </tr>
                                                                    </tbody></table>

                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                        <!-- End Asset -->
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="normal_copy_wrapper" style="font-size: 12px; line-height: 20px;  color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left;  padding: 0;" align="left">Amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.

                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>

                            <!-- /wrapper -->

                            <td class="cellblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;background-color:#0071c5" align="left" valign="top">
                                <!-- Sidebar Column -->

                                <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 286px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">

                                    <!-- NOTE: Change width based on image/content width -->

                                    <tbody><tr>
                                        <td class="sidebar-td-blue" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                <tbody><tr>
                                                    <td class="content-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 61px 42px 60px 43px;" align="left" valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tbody><tr>
                                                                <td style="font-size: 20px; line-height: 120%; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; padding: 0px 0px 32px 0px;" align="left">Lorum ipsum dolor amed lorum ipsum fateed.

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-size: 16px; color: #ffffff; font-weight: bold; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; text-align: left; word-break: normal; line-height: 20px; padding: 0px 0px 28px 0px; " align="left">Lorum ipsum dolor

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="normal_copy_wrapper" style="color: #ffffff; line-height: 22px; font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; padding: 0px 0px 30px 0px;" align="left">Itaque earum rerum hic tenetur a sapiente delectus, ut aut ut aut reiciendis. Itaque earum sapiente.

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100%; padding: 0px 0px 0px 0px;" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0" class="medium-button" style="font-size: 20px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; overflow: hidden;">
                                                                        <tbody><tr>
                                                                            <td style="font-size: 20px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal;width: auto !important; background-color: #fdb813; margin: 0; padding: 14px 10px 14px 20px;" align="left" bgcolor="#fdb813" valign="top">
                                                                                <!-- CTA -->
                                                                                <table class="inner-button" border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #fdb813;">
                                                                                    <tbody><tr>
                                                                                        <td class="button-text" style="background-color: #fdb813; color: #ffffff; font-weight: normal; font-family: arial, helvetica, sans-serif; font-size: 20px; line-height:20px; padding-right: 25px; text-align: left;padding-top:0px;"><a href="#" style="font-size:20px; line-height:20px;  color: #ffffff; text-decoration: none;">CTA&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a></td>
                                                                                        <td width="12" align="left" style="font-size: 21px; line-height:21px;padding-right:15px;"><a href="#" style="font-size: 21px; line-height:21px;"><img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{049eebcb-f1ec-422e-852a-16aeb25d156c}_white-arrow.png" style="display: block;" width="12" height="21" border="0"></a></td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                                <!-- End CTA -->
                                                                            </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>

                                                            </tr>
                                                        </tbody></table>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>

                            <!-- /wrapper -->

                        </tr>
                    </tbody></table>

                    <!-- /row -->
                </td>
            </tr>
        </tbody></table>
    </td>

    <!-- /Container -->

</tr>
</tbody></table>
<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->