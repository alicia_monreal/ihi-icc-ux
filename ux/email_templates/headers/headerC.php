<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tbody><tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; padding: 0;">
                            <tbody><tr>
                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Header Text -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 100%; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">
                                        <tbody><tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 25px 32px 0px 32px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                    <tbody><tr>
                                                        <td class="logo" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png" alt="Intel" align="left" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;">
                                                        </td>
                                                    </tr>
                                                    <!-- NOTE: Set the size/color inline or will default to default size -->

                                                    <tr>
                                                        <td class="headline_padding wys-inline-simple" style="font-size: 38px; line-height: 40px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; display: block; clear: both; padding: 20px 0px 18px 0px; " align="left">
                                                            Lorum ispsum dolor amed lorum fateed.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="subhead_copy_wrapper wys-inline" style="font-size: 24px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; line-height: 26px;padding: 0px 0px 24px 0px;" align="left">
                                                            Subhead here in sans-serif regular
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="mobile-cta-container" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tbody><tr>
                                                                    <td class="mobile-cta" style="font-size: 16px;font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;">
                                                                        <a href="#" class="mobile-cta-text wys-inline-simple"  style="font-size: 16px; color: #ffda00; font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;text-decoration:none;">CTA<span class="hideblock" style="font-size: 16px;font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;color:#ffffff;"> &gt;</span></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>

                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>


                                    <!-- /wrapper -->

                                </td>
                            </tr>

                            <tr>
                                <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Image -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 100%; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0; height: 233px;" bgcolor="#0071c5">
                                        <tbody><tr>
                                            <td class="hideblock" style="font-size: 1px; line-height: 0px !important; height: 233px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; padding: 0;" align="left" valign="top">
                                                <img src="/img/templates/default-images/placeholders/header-image-email-fpo-c.png" class="image-fix logo image-box-custom" data-attr="headerC" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->

                    </td>
                </tr>
            </tbody></table>


            <!-- /Container -->

        </td>
    </tr>
</tbody></table>

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
