<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tbody><tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; padding: 0;">
                            <tbody><tr>
                                <td class="full-width" style="border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Header Text -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 410px;  border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">

                                        <!-- NOTE: Change width based on first column width, subtracted from 670 -->

                                        <tbody><tr>
                                            <td class="header-text-td" style=" border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 40px 0px 20px 44px;height: 381px;" align="left" valign="top">
                                                <!-- NOTE: Specific extra padding based on Ozone template. Default is 25px all around -->

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                    <tbody><tr>
                                                        <td style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0px;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png" alt="Intel" align="left" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="headline_wrapper wys-inline-simple" style="font-size: 40px; line-height: 50px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; padding: 35px 0px 0px 0px;" align="left">Lorum ispsum dolor amed lorum fateed.</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal_copy_dates wys-inline-simple"  style="line-height: 30px; font-size: 20px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: bold; text-align: left; padding: 30px 0px 25px 0px;" align="left">Name of Event<br />Thursday, January 4th, 2014<br />10:00 am - 5:00 pm</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 20px; line-height:21px;">
                                                            <table border="0" cellpadding="0" cellspacing="0" class="medium-button" style="font-size: 20px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; overflow: hidden; padding: 6px 0px 0px 0px;">
                                                                <tbody><tr>
                                                                    <td style="font-size: 20px; line-height:21px; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 100% !important; width: auto !important; background-color: #fdb813; margin: 0; padding: 12px 5px 12px 20px;" align="left" bgcolor="#fdb813" valign="top">
                                                                        <table class="inner-button" border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #fdb813;">
                                                                            <tbody><tr>
                                                                                <td class="button-text wys-inline-link" style="background-color: #fdb813; color: #ffffff; font-weight: normal; font-family: arial, helvetica, sans-serif; font-size: 20px; line-height:21px; padding-right: 15px;text-align: left;"><a href="#" style="color: #ffffff; text-decoration: none;">Register Now </a></td>
                                                                                <td width="12" align="left" style="font-size: 21px; line-height:21px;padding-right:15px;"><a href="#" style="font-size: 21px; line-height:21px;"><img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{049eebcb-f1ec-422e-852a-16aeb25d156c}_white-arrow.png" style="display: block;" width="12" height="21" border="0"></a></td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>

                                                        </td>
                                                    </tr>
                                                </tbody></table>

                                            </td>
                                        </tr>

                                        <!-- NOTE: Set the size/color inline or will default to default size -->

                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                                <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  line-height: 20px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Image -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 260px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;height: 441px;" bgcolor="#0071c5">

                                        <!-- NOTE: Change width based on image width -->

                                        <tbody><tr>
                                            <td class="hideblock" style="font-size: 1px; line-height: 0px !important; height: 441px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal;  margin: 0; padding: 0;" align="left" valign="top">
                                                <img src="/img/templates/default-images/placeholders/header-image-email-fpo-b.png" placeholder="header-image-email-fpo-b.png" class="image-fix image-box-custom" data-attr="headerB" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" height="441">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->
                    </td>
                </tr>
            </tbody></table>
        </td>

        <!-- /Container -->

    </tr>
</tbody></table>

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
