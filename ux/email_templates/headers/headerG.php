<table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
    <tbody><tr>
        <!-- Container -->

        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tbody><tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                            <tbody><tr>
                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px; height:160px" align="left" valign="top">
                                    <!-- Header Text -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 520px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5;" bgcolor="#0071c5">
                                        <tbody><tr>
                                            <td class="header-text-td" style="width:100%;background-color: #0071c5;;padding-left:28px;height:16px;">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <!-- NOTE: Change width based on first column width, subtracted from 670 -->
                                                    <tbody><tr class="show-logo-tr">
                                                        <td class="show-logo" height="50" style="line-height:50px; font-size:50px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="headline_wrapper headline_padding wys-inline" style="font-size: 30px; line-height: 32px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0;padding: 0px 0px 40px 0px;" align="left" valign="top">
                                                            Lorum ispsum dolor amed lorum ipsum amed fateed.


                                                            <!-- NOTE: Set the size inline or will default to default size -->
                                                        </td>

                                                    </tr>

                                                </tbody></table>

                                            </td>

                                        </tr>

                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                                <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top" height="171">
                                    <!-- Image -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 150px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">

                                        <!-- NOTE: Change width based on image width -->

                                        <tbody><tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 19px 30px 17px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                    <tbody><tr>
                                                        <td style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top" width="79" height="50">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png" alt="Intel" align="right" class="hideblock" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="79" height="50">
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="hideblock" valign="bottom" style="vertical-align: bottom !important; font-size: 12px; border-collapse: collapse !important; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" width="98" height="85">
                                                <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{42f52c6d-4813-41c7-8257-b92216f02fcc}_tear-away-98x85.png" align="right" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;" width="98" height="85">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </tbody></table>

                        <!-- /row -->
                    </td>
                </tr>
            </tbody></table>
        </td>

        <!-- /Container -->

    </tr>
</tbody></table>

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td style="height:10px;"><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
<!-- /SPACER -->
