<!-- Start Header -->
<table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">
    <tr>
        <!-- Container -->
        <td align="center" valign="top" class="center" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;">

            <table border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                            <tr>
                                <td class="wrapper" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Header Text -->
                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 520px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">
                                        
                                        <tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px 90px 0px 25px;height:155px;" align="left" valign="top">
                                                
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="show-logo-tr">
                                                        <td class="show-logo" height="60" style="line-height:60px; font-size:50px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>

                                                    <!-- Newsletter Headline -->
                                                    <tr style="font-size: 30px; line-height: 32px">
                                                        <td class="headline_wrapper headline_padding wys-inline-simple" style="font-size: 30px; line-height: 32px; color: #ffffff; font-family: Arial, Helvetica, 'Lucida Grande', sans-serif; font-weight: normal; text-align: left; word-break: normal; display: block; clear: both; padding: 12px 0 10px 0px;" align="left">Lorum ispsum dolor amedfa lorum ipsum.</td>
                                                    </tr>

                                                    <!-- Newsletter Dateline -->
                                                    <tr style="line-height: 19px; font-size: 18px; color: #ffffff; font-family: Arial, Helvetica, sans-serif;">
                                                        <td class="last dateline wys-inline-simple" style="font-size: 18px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: Normal; text-align: left; line-height: 18px; padding: 15px 0px 0px 0px;" align="left" valign="top">Issue, Month</td>
                                                    </tr>
                                                </table>
                                                

                                            </td>
                                            <td class="expander" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0;" align="left" valign="top"></td>
                                        </tr>
                                    </table>
                                </td>
                                <!-- /wrapper -->
                                <td class="hideblock" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Image -->
                                    <table border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 150px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; margin: 0 auto; padding: 0;" bgcolor="#0071c5">
                                        <!-- NOTE: Change width based on image width -->
                                        <tr>
                                            <td class="header-text-td" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 27px; margin: 0; padding: 20px 25px 35px 25px;" align="left" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;">
                                                    <tr>
                                                        <td style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; height:50px; margin: 0; padding: 0px;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{544b5a0b-edb2-4c65-8b35-0954de999446}_logo-wht-79x50.png" alt="Intel" align="right" class="hideblock" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="hideblock" valign="bottom" style="vertical-align: bottom !important; height: 85px; line-height: 85px; font-size: 12px; border-collapse: collapse !important; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; padding-top: 30px;" align="left">
                                                <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{42f52c6d-4813-41c7-8257-b92216f02fcc}_tear-away-98x85.png" align="right" class="image-fix" style="font-size: 12px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; display: block;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <!-- /wrapper -->
                            </tr>
                        </table>
                        <!-- /row -->
                    </td>
                </tr>
            </table>
        </td>
        <!-- /Container -->
    </tr>
</table>
<!-- /End Header -->

<!-- SPACER (for display only) -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dcdcdc; border-collapse: collapse;">
    <tr>
        <td><img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{6c105f02-ae73-4103-a1f3-3c74f0c57426}_spacer.gif" width="10" height="10" border="0" style="display: block;"></td>
    </tr>
</table>
 <!-- /SPACER -->