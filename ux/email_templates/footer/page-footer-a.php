<!-- Full width footer -->
<div class="content-wrapper">
    <div class="main-content cf">
		<footer class="cf" style="height:auto;">
			<div id="footer-kink" class="kink cf">
				<div class="inner-content"></div>
			</div>
			
			<div id="footer-legal">
				<div class="wrapper">
					<div class="inner-content wys-inline" style="padding:40px 2.02128% 60px 2.02128%;width:100%;">
						<p class="copyright_information_text">Copyright © 2014 Intel Corporation. All rights reserved. Intel, the Intel logo, are trademarks of Intel Corporation in the U.S. and/or other countries.<br /><br />Intel is committed to protecting your privacy. For more information about Intel's privacy practices, please visit <a href="http://www.intel.com/privacy">www.intel.com/privacy</a> or write to Intel Corporation, ATTN Privacy, Mailstop RNB4-145, 2200 Mission College Blvd., Santa Clara, CA 95054 USA</p>
						
						<nav id="legal">
							<ul class="horizontal-menu cf">
								<li style="margin-right:0;"><a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-online-privacy-notice-summary.html" title="Privacy" target="_blank">Privacy</a></li>
								<li><a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-cookie-notice.html" id="_bapw-link" title="Cookies" target="_blank">Cookies</a></li>
								<li><a href="https://www-ssl.intel.com/content/www/us/en/legal/trademarks.html" title="* Trademarks" target="_blank">* Trademarks</a></li>
							</ul>						
						</nav>
					</div>
				</div>			
			</div>
		</footer><!--/Footer-->
	</div>
</div>