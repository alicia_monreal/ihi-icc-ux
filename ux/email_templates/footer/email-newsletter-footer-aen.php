<!-- Start Standard Footer -->
<table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0;">
    <tr>
        <!-- Container -->

        <td align="center" valign="top" class="center devicewidth" style="width: 670px; font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;" align="center">
                <tr>
                    <td bgcolor="#0071c5" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                        <table class="full-width" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0;">
                            <tr>
                                <td class="full-width" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0px;" align="left" valign="top">
                                    <!-- Text Column -->

                                    <table border="0" cellpadding="0" cellspacing="0" class="devicewidth" bgcolor="#0071c5" style="width: 670px; font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; margin: 0 auto; padding: 0;">
                                        <tr>
                                            <td class="copyright-wrapper" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0;" align="left" valign="top">

                                                <table border="0" cellpadding="0" cellspacing="0" align="left" class="footer-table" style="font-size: 12px; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; background-color: #0071c5; padding: 0;" bgcolor="#0071c5">
                                                    <tr class="hideblock">
                                                        <!-- KINK. Will hide on mobile -->
                                                        <td class="kink" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0;" align="left" valign="top">
                                                            <img src="http://img03.en25.com/EloquaImages/clients/IntelSandbox2013/{a940d952-223a-44ae-8471-1d3ec8d1013f}_lower-kink-dc.png" class="hideblock" style="font-size: 12px; outline: none; text-decoration: none; max-width: 100%; display: block;">
                                                        </td>
                                                        <!-- END KINK -->
                                                    </tr>
                                                    <tr>
                                                        <td class="cellblocknopadding" style="font-size: 12px; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #5F5F5E; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height:27px;margin: 0; padding: 0px 30px 0px 30px;" align="left" valign="top">

                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="copyright wys-inline" style="font-size: 10px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; text-align: left; line-height:14px;padding: 20px 0px 10px 0px; " align="left">Copyright &copy; 2014 Intel Corporation. All rights reserved. Intel, the Intel logo, {List relevant trademarks} are trademarks of Intel Corporation in the U.S. and/or other countries.</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <!-- /wrapper -->

                            </tr>
                        </table>

                        <!-- /row -->
                    </td>
                </tr>
            </table>
        </td>

        <!-- /Container -->

    </tr>
</table>
<!-- / End Standard Footer -->