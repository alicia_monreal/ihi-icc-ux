<!-- Agenda - Two Column -->
<div class="module agenda-two-col cf">
    <div class="module-header">
        <h3 class="icon-sm blue-time-sm content-header">Agenda</h3>
    </div>

    <div class="module-content">
        <div class="content cf">
            <div class="schedule">
                <h4>Date<br>Day</h4>
                <h5>9:00 - 10:30</h5>
                <h6>Title of Session</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quaerat ducimus quisquam optio incidunt a reprehenderit nisi.</p>

                <div class="no-info">
                    <h5>11:00 - 12:30</h5>
                    <h6>Title of Session</h6>
                </div>

                <div class="no-info">
                    <h5>1:00 - 2:30</h5>
                    <h6>Title of Session</h6>
                </div>

                <div class="schedule hidden-schedule">
                    <div class="no-info">
                        <h5>3:00 - 3:30</h5>
                        <h6>Title of Session</h6>
                    </div>

                    <div class="no-info">
                        <h5>4:00 - 5:30</h5>
                        <h6>Title of Session</h6>
                    </div>
                </div>

                <a class="mobile-cta gray" href="">Day 1 full schedule <span class="hide">&gt;</span></a>
            </div>


            <div class="schedule">
                <h4>Date<br>Day</h4>
                <h5>9:00 - 10:30</h5>
                <h6>Title of Session</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quaerat ducimus quisquam optio incidunt a reprehenderit nisi.</p>

                <div class="no-info">
                    <h5>11:00 - 12:30</h5>
                    <h6>Title of Session</h6>
                </div>

                <div class="no-info">
                    <h5>1:00 - 2:30</h5>
                    <h6>Title of Session</h6>
                </div>

                <div class="schedule hidden-schedule">
                    <div class="no-info">
                        <h5>3:00 - 3:30</h5>
                        <h6>Title of Session</h6>
                    </div>

                    <div class="no-info">
                        <h5>4:00 - 5:30</h5>
                        <h6>Title of Session</h6>
                    </div>
                </div>

                <a class="mobile-cta gray" href="">Day 2 full schedule <span class="hide">&gt;</span></a>
            </div>
        </div>
    </div>
</div>