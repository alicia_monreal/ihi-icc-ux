<div class="content-wrapper">
    <div id="main" class="main-content cf">
        <!-- Event Header - Thank You -->
        <div id="header" class="cf">
            <div id="header-content" class="event-header ty no-fold">
                <h1 class="event_header_text">Thank you<br> for registering.</h1>

                <h3 class="event-title"><?php if (isset($_SESSION['webinar_title']) && $_SESSION['webinar_title'] != '') { echo $_SESSION['webinar_title']; } else { echo 'Name of Event'; } ?></h3>
                <h3><?php if (isset($_SESSION['start_date']) && $_SESSION['start_date'] != '') { echo date('l, F j Y', strtotime($_SESSION['start_date'])); } else { echo 'Thursday, July 20 2014'; } ?></h3>
                <h3><?php if (isset($_SESSION['start_time_db']) && $_SESSION['start_time_db'] != '') { echo $_SESSION['start_time_db']; } ?><?php if (isset($_SESSION['end_time_db']) && $_SESSION['end_time_db'] != '') { echo ' - '.$_SESSION['end_time_db']; } ?></h3>

                <div class="event-links">
                    <a href="" class="icon-event calendar">Add to your calendar <span class="white">&gt;</span></a>
                    <a href="javascript:window.print()" class="icon-event printer" style="width:auto;">Print event information <span class="white">&gt;</span></a>
                </div>
            </div>

            <div class="image-container ie8-image-container">
                <img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{3f6c64f3-108d-432c-84d6-a2e60e9933b5}_intel-header-hero-image-501-365.jpg" alt="Intel">
            </div>
        </div><!-- / End Event Header - Thank You -->
    </div>
</div>
