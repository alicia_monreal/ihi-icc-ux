<div class="content-wrapper">
    <div id="main" class="main-content cf">
		<!-- Thank You Module -->
		<div class="module asset cf">
		    <div class="module-content" style="width:100%;">
		        <div class="inner-content">
		            <h4 class="content-header">Thank you.</h4>
		            <p class="confirmation_page_copy_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptatum quis sapiente sed ullam repellat harum rerum recusandae dolores, accusantium suscipit eius possimus sit laborum enim eveniet, provident et maxime.</p>                        
		        </div>
		    </div>
		</div><!-- /.module -->
	</div>
</div>