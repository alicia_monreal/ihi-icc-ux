<!-- Upcoming Events -->
<div class="module upcoming-events cf">
    <div class="module-header">
        <h3 class="icon-sm blue-location-sm content-header">Upcoming Events</h3>
    </div>

    <div class="module-content">
        <div class="content">
            <div class="inner-copy">
                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque porro similique voluptate.</p>

                <div class="image-container"><img src="http://www.ironhorseinteractive.com/eloqua-templates/img/events-fpo.jpg" alt=""></div>

                <a class="mobile-cta" href="">Read More <span class="hide">&gt;</span></a>
            </div>
        </div>
    </div>
</div>