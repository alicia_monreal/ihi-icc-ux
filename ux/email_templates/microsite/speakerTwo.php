<!-- Speakers - Two Column -->
<div class="module speakers-two-col cf">
    <div class="module-header">
        <h3 class="icon-sm blue-comments-sm content-header">Speakers (alt)</h3>
    </div>

    <div class="module-content">
        <div class="content cf">
            <div class="t_row first cf">
                <div class="speaker">
                    <img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{487ffe90-dcaf-4be8-bb9c-d9f5081561e7}_speaker-fpo-two-col.jpg" alt="">

                    <h4>Name of Speaker</h4>
                    <h5>Title of speaker</h5>

                    <p>Lorem ipsum dolor sit amet, consectetur asdfefh. Lorem ipsum dolor sit amet, consectetur.</p>
                </div>

                <div class="speaker">
                    <img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{487ffe90-dcaf-4be8-bb9c-d9f5081561e7}_speaker-fpo-two-col.jpg" alt="">

                    <h4>Name of Speaker</h4>
                    <h5>Title of speaker</h5>

                    <p>Lorem ipsum dolor sit amet, consectetur asdfefh. Lorem ipsum dolor sit amet, consectetur.</p>
                </div>
            </div>

            <div class="t_row cf">
                <div class="speaker">
                    <img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{487ffe90-dcaf-4be8-bb9c-d9f5081561e7}_speaker-fpo-two-col.jpg" alt="">

                    <h4>Name of Speaker</h4>
                    <h5>Title of speaker</h5>

                    <p>Lorem ipsum dolor sit amet, consectetur asdfefh. Lorem ipsum dolor sit amet, consectetur.</p>
                </div>

                <div class="speaker">
                    <img src="http://images.plan.intel.com/EloquaImages/clients/IntelCorporation/{487ffe90-dcaf-4be8-bb9c-d9f5081561e7}_speaker-fpo-two-col.jpg" alt="">

                    <h4>Name of Speaker</h4>
                    <h5>Title of speaker</h5>

                    <p>Lorem ipsum dolor sit amet, consectetur asdfefh. Lorem ipsum dolor sit amet, consectetur.</p>
                </div>
            </div>
        </div>
    </div>
</div>