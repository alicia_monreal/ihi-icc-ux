<!-- Resources -->
<div class="module resources cf">
    <div class="module-header">
        <h3 class="icon-sm blue-copy-sm content-header">Resources</h3>
    </div>

    <div class="module-content">
        <div class="content cf">
            <div class="resource">
                <div class="resource-header">
                    <h4 class="blue-document-lg">Name of Offer</h4>
                    <h5>Title of Author</h5>
                </div>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>

                <a class="mobile-cta" href="">Download <span class="hide">&gt;</span></a>
            </div>

            <div class="resource">
                <div class="resource-header">
                    <h4 class="blue-document-lg">Name of Offer</h4>
                    <h5>Title of Author</h5>
                </div>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>

                <a class="mobile-cta" href="">Download <span class="hide">&gt;</span></a>
            </div>

            <div class="resource">
                <div class="resource-header">
                    <h4 class="blue-document-lg">Name of Offer</h4>
                    <h5>Title of Author</h5>
                </div>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eaque impedit facilis.</p>

                <a class="mobile-cta" href="">Download <span class="hide">&gt;</span></a>
            </div>
        </div>
    </div>
</div>