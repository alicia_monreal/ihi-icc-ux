<!-- Agenda - One Column -->
<div class="module agenda-one-col cf">
    <div class="module-header">
        <h3 class="icon-sm blue-time-sm content-header">Agenda (alt)</h3>
    </div>

    <div class="module-content">
        <div class="content cf">
            <div class="schedule">
                <h4>Date<br>Day</h4>
                <h5>9:00 - 10:30</h5>
                <h6>Title of Session</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad reiciendis fugit eveniet incidunt quibusdam amet architecto pariatur quidem culpa, libero mollitia quas debitis nemo aliquam, eos expedita ducimus, nobis non. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus nam minus nobis magnam ut incidunt nihil.</p>

                <div class="no-info">
                    <h5>11:00 - 12:30</h5>
                    <h6>Title of Session</h6>
                </div>

                <div class="no-info">
                    <h5>1:00 - 2:30</h5>
                    <h6>Title of Session</h6>
                </div>

                <div class="schedule hidden-schedule">
                    <div class="no-info">
                        <h5>3:00 - 4:30</h5>
                        <h6>Title of Session</h6>
                    </div>

                    <div class="no-info">
                        <h5>5:00 - 6:30</h5>
                        <h6>Title of Session</h6>
                    </div>
                </div>

                <a class="mobile-cta gray" href="">Full schedule <span class="hide">&gt;</span></a>
            </div>
        </div>
    </div>
</div>