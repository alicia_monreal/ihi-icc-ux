<style>
.thumbnail-block.thumbnail-selector.fourup li {
    width: auto !important;
}
</style>

<?php global $tactic_type, $email_count, $email_listing; 
    
    if($tactic_type == 'newsletter' || $_REQUEST['type'] == 'newsletter') {  // if tactic_type is newsletter 

        if(empty($tactic_type)) // if empty global tactic_type value
            $tactic_type = $_REQUEST['type'];

        if(!empty($_REQUEST['count'])) {  // if count is not empty 
            $email_record_count = $_REQUEST['count']; // get value from request count
        } else {
            $email_record_count = $email_count; // get value from global field
        }

        if(!empty($email_listing['send_time'][$email_record_count])) {  // if send time is not empty
            $send_time_value = explode(" ",$email_listing['send_time'][$email_record_count]); // get both send time and am/pm value
        } else {
            $send_time_value = array("","AM"); // set default values
        }

        if(empty($email_listing['name'][$email_record_count])) { // if empty name
            $email_listing['name'][$email_record_count] = "";
        }
        if(empty($email_listing['email_subject'][$email_record_count])) { // if empty subject
            $email_listing['email_subject'][$email_record_count] = "";
        }
        if(empty($email_listing['send_date'][$email_record_count])) { // if empty send date
            $email_listing['send_date'][$email_record_count] = "";
        }

        ?>
            <div class="thumbnail-block thumbnail-selector fourup">
                <h2 class="template-head">Email Details</h2>
                    <input type="hidden" name="<?php echo $tactic_type; ?>_number" id="<?php echo $tactic_type; ?>_number" value="<?php echo $email_record_count; ?>" />
                    <input type="text" id="email_name_field" name="email_name_field" class="input-medium-detail required" placeholder="Name of Email" value="<?php echo $email_listing['name'][$email_record_count] ?>" >
                    <input type="text" id="email_subject_field" name="email_subject_field" class="input-medium-detail required" placeholder="Email Subject" value="<?php echo $email_listing['email_subject'][$email_record_count] ?>" style="width:396px !important">
                    
                    <div class="input-append date" id="datetimepicker">
                        <input type="text" value="<?php echo $email_listing['send_date'][$email_record_count] ?>" class="input-medium-detail valid" name="email_send_date" id="email_send_date" placeholder="Send Date">
                        <span style="margin-right:2px;" class="add-on"><i class="icon-th"></i></span>
                    </div>
                    
                    <select id="email_send_time" name="email_send_time" class="input-medium-detail required" style="margin-right:2px;">
                        <option value="">Select One</option>
                        <option value="01:00" <?php if ($send_time_value[0] == '01:00') { echo 'selected'; } ?>>01:00</option>
                        <option value="01:30" <?php if ($send_time_value[0] == '01:30') { echo 'selected'; } ?>>01:30</option>
                        <option value="02:00" <?php if ($send_time_value[0] == '02:00') { echo 'selected'; } ?>>02:00</option>
                        <option value="02:30" <?php if ($send_time_value[0] == '02:30') { echo 'selected'; } ?>>02:30</option>
                        <option value="03:00" <?php if ($send_time_value[0] == '03:00') { echo 'selected'; } ?>>03:00</option>
                        <option value="03:30" <?php if ($send_time_value[0] == '03:30') { echo 'selected'; } ?>>03:30</option>
                        <option value="04:00" <?php if ($send_time_value[0] == '04:00') { echo 'selected'; } ?>>04:00</option>
                        <option value="04:30" <?php if ($send_time_value[0] == '04:30') { echo 'selected'; } ?>>04:30</option>
                        <option value="05:00" <?php if ($send_time_value[0] == '05:00') { echo 'selected'; } ?>>05:00</option>
                        <option value="05:30" <?php if ($send_time_value[0] == '05:30') { echo 'selected'; } ?>>05:30</option>
                        <option value="06:00" <?php if ($send_time_value[0] == '06:00') { echo 'selected'; } ?>>06:00</option>
                        <option value="06:30" <?php if ($send_time_value[0] == '06:30') { echo 'selected'; } ?>>06:30</option>
                        <option value="07:00" <?php if ($send_time_value[0] == '07:00') { echo 'selected'; } ?>>07:00</option>
                        <option value="07:30" <?php if ($send_time_value[0] == '07:30') { echo 'selected'; } ?>>07:30</option>
                        <option value="08:00" <?php if ($send_time_value[0] == '08:00') { echo 'selected'; } ?>>08:00</option>
                        <option value="08:30" <?php if ($send_time_value[0] == '08:30') { echo 'selected'; } ?>>08:30</option>
                        <option value="09:00" <?php if ($send_time_value[0] == '09:00') { echo 'selected'; } ?>>09:00</option>
                        <option value="09:30" <?php if ($send_time_value[0] == '09:30') { echo 'selected'; } ?>>09:30</option>
                        <option value="10:00" <?php if ($send_time_value[0] == '10:00') { echo 'selected'; } ?>>10:00</option>
                        <option value="10:30" <?php if ($send_time_value[0] == '10:30') { echo 'selected'; } ?>>10:30</option>
                        <option value="11:00" <?php if ($send_time_value[0] == '11:00') { echo 'selected'; } ?>>11:00</option>
                        <option value="11:30" <?php if ($send_time_value[0] == '11:30') { echo 'selected'; } ?>>11:30</option>
                        <option value="12:00" <?php if ($send_time_value[0] == '12:00') { echo 'selected'; } ?>>12:00</option>
                        <option value="12:30" <?php if ($send_time_value[0] == '12:30') { echo 'selected'; } ?>>12:30</option>
                    </select>
                    <select id="email_send_ampm" name="email_send_ampm" style="width: 64px;"><option value="AM" <?php if($send_time_value[1] == "AM") echo "selected" ?>>AM</option><option value="PM" <?php if($send_time_value[1] == "PM") echo "selected" ?> >PM</option></select>
            </div>
        <?php
    }
?>

<!--Banner Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a banner type</h2>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-oneup thumbnail-header">
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner A" data-type="header" data-path="email-newsletter-banner-a"><img src="img/templates/thumbnails/email-banner-f.png" class="popover-hover" data-toggle="popover" data-content="Banner A"></a></li>
    </ul>

    <div id="" class="asset-preview asset-preview-email hide asset-header">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="glyphicon glyphicon-refresh popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>

<!--Additional Modules-->
<div class="thumbnail-block thumbnail-selector fourup" style="padding:0px">
    <div class="asset-preview asset-preview-email hide asset-modules" style="padding: 0px 20px 0px"></div>
    <div class="asset-preview asset-preview-email hide asset-modules asset-modules-leftbar" style="float:left;width:429px;padding: 0px 20px 0px"></div>
    <div class="asset-preview asset-preview-email hide asset-modules asset-modules-rightbar" style="float:right;width:240px"></div>
    <p class="clearfix"></p>
    <h2 class="template-head">Add additional modules</h2>
    <p class="section-note">Some of the additional modules have variable column layouts which are governed by the rules in the Eloqua Template guidelines.  While entering text in the Asset Configuration, the columns will remain fixed.  To see the final output, please view the module from "Preview Mode".</p>
    <p class="section-note"><b>You need to select one of the modules mode between A and B</b></p>
        <ul class="newsletter-modules" style="margin-left:165px">
            <li class="newsletter-mod-a" style="height:225px">
                <span><b>A</b></span>
                <a href="javascript:void(0)">
                    <img class="popover-hover" src="img/templates/thumbnails/newsletter-modules-a.png" data-content="A set" style="height:160px">
                </a>
            </li>                    
            <li class="newsletter-mod-b" style="height:225px">
                <span><b>B</b></span>
                <a href="javascript:void(0)">
                    <img class="popover-hover" src="img/templates/thumbnails/newsletter-modules-b.png" data-content="B set" style="height:160px">
                </a>
            </li>               
        </ul>

        <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-modules newsletter-modules-a" style="display:none">
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="In This Issue" data-type="modules" data-path="in-this-issue-a"><img src="img/templates/thumbnails/email-module-in-this-issue.png" class="popover-hover" data-toggle="popover" data-content="In This Issue"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Featured Content" data-type="modules" data-path="featured-content-a"><img src="img/templates/thumbnails/email-module-featured-content.png" class="popover-hover" data-toggle="popover" data-content="Featured Content"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Editorial Content" data-type="modules" data-path="editorial-content-a"><img src="img/templates/thumbnails/email-module-editorial-content.png" class="popover-hover" data-toggle="popover" data-content="Editorial Content"></a></li>
            <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Help Articles" data-type="modules" data-path="help-articles-a"><img src="img/templates/thumbnails/email-module-help-articles.png" class="popover-hover" data-toggle="popover" data-content="Help Articles"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Events & Webinars" data-type="modules" data-path="events-webinars-a"><img src="img/templates/thumbnails/email-module-events-webinars.png" class="popover-hover" data-toggle="popover" data-content="Events & Webinars"></a></li>
            <li class="last" ><a href="" class="thumbnail-selector thumbnail-load" data-attr="Blog Posts" data-type="modules" data-path="blog-posts-a"><img src="img/templates/thumbnails/email-module-blog-posts.png" class="popover-hover" data-toggle="popover" data-content="Blog Posts"></a></li>
        </ul>

        <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-modules newsletter-modules-b" style="display:none">
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Hot Topic" data-type="modules" data-path="hot-topic-b"><img src="img/templates/thumbnails/email-module-in-this-issue.png" class="popover-hover" data-toggle="popover" data-content="Hot Topic"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Featured Content" data-type="modules" data-path="featured-content-b"><img src="img/templates/thumbnails/email-module-featured-content.png" class="popover-hover" data-toggle="popover" data-content="Featured Content"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Editorial Content" data-type="modules" data-path="editorial-content-b"><img src="img/templates/thumbnails/email-module-editorial-content.png" class="popover-hover" data-toggle="popover" data-content="Editorial Content"></a></li>
            <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Help Articles" data-type="modules" data-path="help-articles-b"><img src="img/templates/thumbnails/email-module-help-articles.png" class="popover-hover" data-toggle="popover" data-content="Help Articles"></a></li>
            <li ><a href="" class="thumbnail-selector thumbnail-load" data-attr="Blog Posts" data-type="modules" data-path="blog-posts-b"><img src="img/templates/thumbnails/email-module-blog-posts.png" class="popover-hover" data-toggle="popover" data-content="Blog Posts"></a></li>
            <li ><a href="" class="thumbnail-selector thumbnail-load" data-attr="Sidebar - In This Issue" data-type="modules" data-path="sidebar-in-this-issue-b"><img src="img/templates/thumbnails/email-module-blog-posts.png" class="popover-hover" data-toggle="popover" data-content="Sidebar -In This Issue"></a></li>
            <li ><a href="" class="thumbnail-selector thumbnail-load" data-attr="Sidebar - Events" data-type="modules" data-path="sidebar-events-b"><img src="img/templates/thumbnails/email-module-blog-posts.png" class="popover-hover" data-toggle="popover" data-content="Sidebar - Events"></a></li>
            <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Sidebar - Webinar" data-type="modules" data-path="sidebar-webinar-b"><img src="img/templates/thumbnails/email-module-blog-posts.png" class="popover-hover" data-toggle="popover" data-content="Sidebar - Webinars"></a></li>
            <li ><a href="" class="thumbnail-selector thumbnail-load" data-attr="Sidebar - Contact Us" data-type="modules" data-path="sidebar-contact-us-b"><img src="img/templates/thumbnails/email-module-blog-posts.png" class="popover-hover" data-toggle="popover" data-content="Sidebar - Contact Us"></a></li>
        </ul>        
    <p class="clearfix"></p>
</div>

<!--Body Type-->
<div class="thumbnail-block thumbnail-selector fourup">
    <h2 class="template-head">Select a footer type</h2>
    <p class="section-note">Footer B, which includes social links, will not visually reflect the appropriate social icons for the URLs you input until production support builds the asset in Eloqua.  They are just placeholder to indicate approximate style and location.</p>
    <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-footer" >
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer A" data-type="footer" data-path="email-newsletter-footer-aen"><img src="img/templates/thumbnails/email-footer-a.png" class="popover-hover" data-toggle="popover" data-content="Footer A"></a></li>
        <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer B" data-type="footer" data-path="email-newsletter-footer-ben"><img src="img/templates/thumbnails/email-footer-b.png" class="popover-hover" data-toggle="popover" data-content="Footer B"></a></li>
    </ul>
    <div id="" class="asset-preview asset-preview-email hide asset-footer">
    </div>
    <div class="button-refresh hide">
        <button type="button" class="btn btn-primary refresh-btn"><i class="glyphicon glyphicon-refresh popover-hover" data-toggle="popover" data-content="Reset"></i></button>
    </div>
    <p class="clearfix"></p>
</div>
