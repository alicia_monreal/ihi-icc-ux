<div>
    <p class="asset-title">Select the button below to use the default configuration in your email and registration asset for queicker and easier setup.</p>
    <div id="default-configuration" class="btn-green custom-html default-config-box"><p class="default-config-icon" val="0"></p>Use Default Configuration</div>
</div>
<div class="global-line"></div>


<div class="build-header-footer">
    <p class="asset-title" style="margin-top: 60px; width: 100%;">Build a header and footer below that can be replicated in your other assets.</p>
    <!--Header Type-->
    <div class="thumbnail-block thumbnail-selector fourup">
        <h2 class="template-head">Select a header type</h2>
        <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-header">
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner A" data-type="header" data-path="email-banner-a"><img src="img/templates/thumbnails/email-banner-a.png" class="popover-hover" data-toggle="popover" data-content="Banner A"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner B" data-type="header" data-path="email-banner-b"><img src="img/templates/thumbnails/email-banner-b.png" class="popover-hover" data-toggle="popover" data-content="Banner B"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner C" data-type="header" data-path="email-banner-c"><img src="img/templates/thumbnails/email-banner-c.png" class="popover-hover" data-toggle="popover" data-content="Banner C"></a></li>
            <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner D" data-type="header" data-path="email-banner-d"><img src="img/templates/thumbnails/email-banner-d.png" class="popover-hover" data-toggle="popover" data-content="Banner D"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner E" data-type="header" data-path="email-banner-e"><img src="img/templates/thumbnails/email-banner-e.png" class="popover-hover" data-toggle="popover" data-content="Banner E"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner F" data-type="header" data-path="email-banner-f"><img src="img/templates/thumbnails/email-banner-f.png" class="popover-hover" data-toggle="popover" data-content="Banner F"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner G" data-type="header" data-path="email-banner-g"><img src="img/templates/thumbnails/email-banner-g.png" class="popover-hover" data-toggle="popover" data-content="Banner G"></a></li>
            <li class="last"><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Banner H" data-type="header" data-path="email-banner-h"><img src="img/templates/thumbnails/email-banner-h.png" class="popover-hover" data-toggle="popover" data-content="Banner H"></a></li>
        </ul>

        <div id="" class="asset-preview asset-preview-email hide asset-header">
        </div>
        <div class="button-refresh hide">
            <button type="button" class="btn btn-primary refresh-btn"><i class="glyphicon glyphicon-refresh popover-hover" data-toggle="popover" data-content="Reset"></i></button>
        </div>
        <p class="clearfix"></p>
    </div>

    <!--Footer Type-->
    <div class="thumbnail-block thumbnail-selector fourup">
        <h2 class="template-head">Select a footer type</h2>
        <ul id="thumbnail-banner" class="thumbnail-ul thumbnail-twoup thumbnail-footer" >
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer A" data-type="footer" data-path="email-footer-a"><img src="img/templates/thumbnails/email-footer-a.png" class="popover-hover" data-toggle="popover" data-content="Footer A"></a></li>
            <li><a href="" class="thumbnail-selector thumbnail-load" data-attr="Email Footer B" data-type="footer" data-path="email-footer-b"><img src="img/templates/thumbnails/email-footer-b.png" class="popover-hover" data-toggle="popover" data-content="Footer B"></a></li>
        </ul>
        <div id="" class="asset-preview asset-preview-email hide asset-footer">
        </div>
        <div class="button-refresh hide">
            <button type="button" class="btn btn-primary refresh-btn-footer"><i class="glyphicon glyphicon-refresh popover-hover" data-toggle="popover" data-content="Reset"></i></button>
        </div>
        <p class="clearfix"></p>
    </div>
</div>
