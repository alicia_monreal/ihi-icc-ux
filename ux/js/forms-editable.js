$(function(){
	//Yes/No options here
	$('#surveycheck, #socialmedia, #resourcelist, #contactlist_optin, #list_optin, #mql_criteria_notify, #generate_leads, #green_screen_utilized, #listed_on_intel').editable({
		//value: 0,   
        source: [
          {value: '', text: 'Please select'},
          {value: 1, text: 'Yes'},
          {value: 9999, text: 'No'}
        ]
    });

    //Other selects
    $('#contact_list_request_type, #list_request_type').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'Contact', text: 'Contact'},
          {value: 'Lead', text: 'Lead'},
          //{value: 'Contact and Lead', text: 'Contact and Lead'},
          {value: 'Prospect Assessment', text: 'Prospect Assessment'},
          {value: 'RAW', text: 'RAW'}
        ]
    });
    $('#time_zone').editable({ 
        source: [
          {value: '(GMT -12:00) Eniwetok, Kwajalein', text: '(GMT -12:00) Eniwetok, Kwajalein'},
          {value: '(GMT -11:00) Midway Island, Samoa', text: '(GMT -11:00) Midway Island, Samoa'},
          {value: '(GMT -10:00) Hawaii', text: '(GMT -10:00) Hawaii'},
          {value: '(GMT -9:00) Alaska', text: '(GMT -9:00) Alaska'},
          {value: '(GMT -8:00) Pacific Time', text: '(GMT -8:00) Pacific Time'},
          {value: '(GMT -7:00) Mountain Time', text: '(GMT -7:00) Mountain Time'},
          {value: '(GMT -6:00) Central Time, Mexico City', text: '(GMT -6:00) Central Time, Mexico City'},
          {value: '(GMT -5:00) Eastern Time, Bogota, Lima', text: '(GMT -5:00) Eastern Time, Bogota, Lima'},
          {value: '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz', text: '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz'},
          {value: '(GMT -3:30) Newfoundland', text: '(GMT -3:30) Newfoundland'},
          {value: '(GMT -3:00) Brazil, Buenos Aires, Georgetown', text: '(GMT -3:00) Brazil, Buenos Aires, Georgetown'},
          {value: '(GMT -2:00) Mid-Atlantic', text: '(GMT -2:00) Mid-Atlantic'},
          {value: '(GMT) Western Europe Time, London, Lisbon, Casablanca', text: '(GMT) Western Europe Time, London, Lisbon, Casablanca'},
          {value: '(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris', text: '(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris'},
          {value: '(GMT +2:00) Kaliningrad, South Africa', text: '(GMT +2:00) Kaliningrad, South Africa'},
          {value: '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg', text: '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg'},
          {value: '(GMT +3:30) Tehran', text: '(GMT +3:30) Tehran'},
          {value: '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi', text: '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi'},
          {value: '(GMT +4:30) Kabul', text: '(GMT +4:30) Kabul'},
          {value: '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent', text: '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent'},
          {value: '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi', text: '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi'},
          {value: '(GMT +5:45) Kathmandu', text: '(GMT +5:45) Kathmandu'},
          {value: '(GMT +6:00) Almaty, Dhaka, Colombo', text: '(GMT +6:00) Almaty, Dhaka, Colombo'},
          {value: '(GMT +7:00) Bangkok, Hanoi, Jakarta', text: '(GMT +7:00) Bangkok, Hanoi, Jakarta'},
          {value: '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong', text: '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong'},
          {value: '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk', text: '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk'},
          {value: '(GMT +9:30) Adelaide, Darwin', text: '(GMT +9:30) Adelaide, Darwin'},
          {value: '(GMT +10:00) Eastern Australia, Guam, Vladivostok', text: '(GMT +10:00) Eastern Australia, Guam, Vladivostok'},
          {value: '(GMT +11:00) Magadan, Solomon Islands, New Caledonia', text: '(GMT +11:00) Magadan, Solomon Islands, New Caledonia'},
          {value: '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka', text: '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'}
        ]
  });
  $('#webcast_environment').editable({ 
        source: [
          {value: 'Intel Webinar Events', text: 'Intel Webinar Events'},
          {value: 'Intel Webinar Reg Events', text: 'Intel Webinar Reg Events'},
          {value: 'Intel Webinar Events-CH', text: 'Intel Webinar Events-CH'},
          {value: 'Intel Technology Providers Webinar Invite', text: 'Intel Technology Providers Webinar Invite'},
          {value: 'Internet of Things BB', text: 'Internet of Things BB'},
          {value: 'Internet of Things Disti', text: 'Internet of Things Disti'},
          {value: 'Internet of Things ISA', text: 'Internet of Things ISA'}
        ]
  });
	$('#expected_attendees').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: '50 - 100', text: '50 - 100'},
          {value: '100 - 200', text: '100 - 200'},
          {value: '200+', text: '200+'}
        ]
    });
    $('#sector').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'Embedded', text: 'Embedded'},
          {value: 'ITDM', text: 'ITDM'},
          {value: 'Channel', text: 'Channel'},
          {value: 'Educator', text: 'Educator'}
        ]
    });
    $('#subsector').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'None', text: 'None'},
          {value: 'ITDM-ESS', text: 'ITDM-ESS'},
          {value: 'ITDM-WA', text: 'ITDM-WA'},
          {value: 'ITDM-WA', text: 'ITDM-WA'},
          {value: 'Intel Technology Provider (ITP)', text: 'Intel Technology Provider (ITP)'},
          {value: 'Retail Ecosystem', text: 'Retail Ecosystem'},
          {value: 'Intel Education', text: 'Intel Education'},
          {value: 'Other', text: 'Other'}
        ]
    });
    $('#mmi').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'Need MMI', text: 'Need MMI'},
          {value: 'Need MMI', text: 'Need MMI'}
        ]
    });
    $('#program').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'Industrial Nurture', text: 'Industrial Nurture'},
          {value: 'Cloud', text: 'Cloud'},
          {value: 'HPC', text: 'HPC'},
          {value: 'EnterpriseIT', text: 'EnterpriseIT'},
          {value: 'Broad Market', text: 'Broad Market'}
        ]
    });
    /*Hard coding these instead
    $('#template').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'Standard Template', text: 'Standard Template'},
          {value: 'Custom Template', text: 'Custom Template'}
        ]
    });
    $('#standard_template_type').editable({ 
        source: [
          {value: '', text: 'Select one'},
          {value: 'Custom', text: 'Custom'},
          {value: 'Default', text: 'Default'}
        ]
    });
    */

    //Checklists
    $('#topics_covered').editable({ 
        source: [
          {value: 'Big Data', text: 'Big Data'},
          {value: 'Cloud Computing', text: 'Cloud Computing'},
          {value: 'Consumerization of IT', text: 'Consumerization of IT'},
          {value: 'Data Center Efficiency', text: 'Data Center Efficiency'},
          {value: 'Desktop Virtualization', text: 'Desktop Virtualization'},
          {value: 'Embedded', text: 'Embedded'},
          {value: 'High Performance Computing', text: 'High Performance Computing'},
          {value: 'Internet of Things', text: 'Internet of Things'},
          {value: 'Manageability', text: 'Manageability'},
          {value: 'Mobility', text: 'Mobility'},
          {value: 'Security', text: 'Security'},
          {value: 'Virtualization', text: 'Virtualization'},
          {value: 'Other', text: 'Other'}
        ]
    });
    $('#vertical').editable({ 
        source: [
          {value: 'Healthcare', text: 'Healthcare'},
          {value: 'Retail', text: 'Retail'},
          {value: 'Lorum ipsum', text: 'Lorum ipsum'}
        ]
    });
    $('#geo').editable({ 
        source: [
          {value: 'APJ', text: 'APJ'},
          {value: 'EMEA', text: 'EMEA'},
          {value: 'LAR', text: 'LAR'},
          {value: 'NAR', text: 'NAR'},
          {value: 'PRC', text: 'PRC'}
        ]
    });

	//All of em. Special cases below, then additional in footer.php for dynamic content.
    $('.editable').editable();

	

});