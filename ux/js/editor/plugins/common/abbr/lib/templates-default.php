<?php
$loc = 'templates';
$step = 1;
$t_type = 'default';

include('includes/head.php');
include('includes/header-templates.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
// if (!isset($_SESSION['campaign_id'])) {
    // header('Location:request?error=request-id');
    // echo "Initiate a request";
    // exit();
//}
?>


<div class="container-fluid full-width">
    <div class="row-fluid">
    	<!--Sidebar-->
        <div id="tpl-sidebar" class="span4">
            <h1>Email and Registration Page Details</h1>
            <p>Enter the content that will appear across all of your emails and landing pages.</p>

            <!--Form-->
            <form id="tpl-default-options" name="tpl-default-options" action="templates-default" method="post">
	            <div class="option-block">
	            	<h2>Global Attributes</h2>
	            	<div class="control-group">
		            	<label for="event_header">Event Header</label>
		            	<input type="text" id="event_header" name="event_header" placeholder="Used on all banners. Max characters: 72" maxlength="72" data-target=".event_header_text" class="change-text input-wide required">
		            </div>
		            <div class="control-group">
		            	<label for="body_copy">Body Copy</label>
		            	<textarea id="body_copy" class="ckeditor" name="body_copy" placeholder="Max characters: 400" maxlength="400"  data-target=".body_copy_text" class="change-text input-wide textarea-medium required"></textarea>
		            </div>
		            <div class="control-group">
		            	<label for="copyright_information">Copyright Information</label>
		            	<textarea id="copyright_information" class="ckeditor" name="copyright_information" placeholder="" data-target=".copyright_information_text" class="change-text input-wide textarea-medium required"></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Thank You Email</h2>
		            <div class="control-group">
		            	<label for="thank_you_body_copy">Thank You Body Copy</label>
		            	<textarea id="thank_you_body_copy" class="ckeditor" name="thank_you_body_copy" placeholder="Max characters: 300" maxlength="300" class="input-wide textarea-medium required"></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Sorry We Missed You Email</h2>
		            <div class="control-group">
		            	<label for="sorry_we_missed_you_body_copy">Sorry We Missed You Body Copy</label>
		            	<textarea id="sorry_we_missed_you_body_copy" class="ckeditor" name="sorry_we_missed_you_body_copy" placeholder="Max characters: 300" maxlength="300" class="input-wide textarea-medium required"></textarea>
		            </div>
		        </div>

		        <div class="option-block">
	            	<h2>Registration/Confirmation Page</h2>
		            <div class="control-group">
		            	<label for="confirmation_page_copy">Confirmation Page Copy</label>
		            	<textarea id="confirmation_page_copy" class="ckeditor" name="confirmation_page_copy" placeholder="Max characters: 300" maxlength="300" class="input-wide textarea-medium required"></textarea>
		            </div>
		            <div class="control-group">
		            	<label for="checkbox">Checkbox</label>
		            	<textarea id="checkbox" name="checkbox" class="ckeditor" placeholder="Max characters: 300" maxlength="300" class="input-wide textarea-medium required"></textarea>
		            </div>
		            <div class="control-group">
		            	<label for="legal_copy">Legal Copy</label>
		            	<textarea id="legal_copy" name="legal_copy" class="ckeditor" placeholder="Max characters: 300" maxlength="300" class="input-wide textarea-medium required"></textarea>
		            </div>
		        </div>

		    </form>
		    <!--/Form-->

        </div>
        <!--/Sidebar-->

        <!--Body-->
        <div id="tpl-body" class="span8">
        	<div style="width:670px;margin:0 auto;">
	            <div class="template-preview-select pull-right">
	            	Preview Asset
	            	<select id="preview-select" name="preview-select">
	            		<option value="#invitation-email">Invitation Email</option>
	            		<option value="#reminder-email-one">Reminder Email 1</option>
	            		<option value="#reminder-email-two">Reminder Email 2</option>
	            		<option value="#thank-you-email">Thank You Email</option>
	            		<option value="#sorry-we-missed-you-email">Sorry We Missed You Email</option>
	            		<option value="#registration-page">Registration Page</option>
	            		<option value="#confirmation-page">Confirmation Page</option>
	            	</select>
	            </div>
	        </div>

            <div id="template-previews">
            	<div id="invitation-email" class="template-block">
	            	<?php include('email_templates/headers/headerF-default.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-default.php'); ?>
	            	<?php include('email_templates/footer/standard_foot-default.php'); ?>
	            </div>

	            <div id="reminder-email-one" class="template-block hide">
	            	<?php include('email_templates/headers/headerF-default.php'); ?>
	            	<?php include('email_templates/content/image_sidebar-default.php'); ?>
	            	<?php include('email_templates/footer/standard_foot-default.php'); ?>
	            </div>
            </div>
        </div>
        <!--/Body-->
    </div>

	<div class="header-row">
		<div class="container-fluid">
			<header style="width:auto;">
				<div class="header-right"><a href="" id="tpl-cancel" class="btn-green plain" style="margin-right:6px;">Cancel</a><a href="#" id="tpl-save-continue" class="btn-green submit">Save &amp; Continue</a></div>
			</header>
		</div>
	</div>

</div>

<?php include('includes/footer.php'); ?>
