//Template Editor Plugin -- To be included in any page open to inline editing
    //Static class to keep and inject HTML elements.


    // *********TEMPLATE INJECTOR CLASS Responsible for
    // *********Injecting and loading selected templates -
    // *********Duplicate chunks of template modules

    $.ajaxSetup({ cache: false });
    var templateInjector = function(){
        var temp_elm        = null;
        var temp_name       = null;
        var temp_type       = null;
        var temp_selector   = null;
        var temp_data       = null;
        var temp_load_step  = null;
        var temp_load_callback = null;


        var injectAddmore= function(handle, mark){
            if(mark === 'div'){
                var ohtml = '<div id="' + handle +  '-btn-row" class="tpl-add-btn-row"><div class="tpl-rp-add-btn"><button type="button" id="' + handle +  '-btn" tpl-rp-data="' +  handle + '" class="btn-green add module-btn">Add more</button>';
                ohtml += '</div></div>';
            }else{
                var ohtml = '<tr id="' + handle +  '-btn-row" class="tpl-add-btn-row"><td><div class="tpl-rp-add-btn"><button type="button" id="' + handle +  '-btn" tpl-rp-data="' +  handle + '" class="btn-green add module-btn">Add more</button>';
                ohtml += '</td></div></tr>';
            }
            return ohtml;
        }

        var initialize = function(){
            initLegacyCheckboxes();
        }


        var initLegacyCheckboxes = function(){

            $(".tpl-news-checkbox").change(function() {
                var oElm  = $(this);
                var oData = oElm.attr('data-attr');
                if(this.checked) {
                    // $('#template-previews').removeClass('small');
                    // $('#template-previews').addClass('large');
                    var sInvert = (oData === 'a') ? 'b':'a';
                    tplInjector.loadTemplate2(oData, 'news', $('#template-previews-news'));
                    tplInjector.removeTemplate(sInvert);
                    $('#check'+sInvert).attr('checked','false');
                }else{
                    tplInjector.removeTemplate(oData);
                }
            });

            $(".tpl-micro-checkbox").change(function() {
                var oElm  = $(this);
                var oData = oElm.attr('data-attr');
                if(this.checked) {
                    // $('#template-previews').removeClass('small');
                    // $('#template-previews').addClass('large');
                    tplInjector.loadTemplate2(oData, 'micro', $('#template-previews-large'));
                }else{
                    tplInjector.removeTemplate(oData);
                }
            });

            $(".tpl-head-checkbox").change(function() {
                var oElm  = $(this);
                var oData = oElm.attr('data-attr');
                if(this.checked) {
                    $('#template-previews').removeClass('large');
                    $('#template-previews').addClass('small');
                    tplInjector.loadTemplate2(oData, 'head', tplEditor.tplHead);
                }else{
                    tplInjector.removeTemplate(oData);
                }
            });

            $(".tpl-cont-checkbox").change(function() {
                var oElm  = $(this);
                var oData = oElm.attr('data-attr');
                if(this.checked) {
                    tplInjector.loadTemplate2(oData, 'content', tplEditor.tplModules);
                }else{
                    tplInjector.removeTemplate(oData);
                }
            });

            $(".tpl-foot-checkbox").change(function() {
                var oElm  = $(this);
                var oData = oElm.attr('data-attr');
                if(this.checked) {
                    tplInjector.loadTemplate2(oData, 'foot', tplEditor.tplFoot);
                }else{
                    tplInjector.removeTemplate(oData);
                }
            });

        }

        var injectImageSelector = function(temp_name, path){
            var ohtml = '<div class="image-selector" id="image-selector-' + temp_name + '" data-attr="' + temp_name + '">';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_15378" data-attr="IMAL_15378" data-path="/img/templates/default-images/placeholders/' + path + '-15378.jpg"><img src="img/templates/default-images/thumbnails/IMAL_15378.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_16140" data-attr="IMAL_16140" data-path="/img/templates/default-images/placeholders/' + path + '-16140.jpg"><img src="img/templates/default-images/thumbnails/IMAL_16140.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_17750" data-attr="IMAL_17750" data-path="/img/templates/default-images/placeholders/' + path + '-17750.jpg"><img src="img/templates/default-images/thumbnails/IMAL_17750.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_18388" data-attr="IMAL_18388" data-path="/img/templates/default-images/placeholders/' + path + '-18388.jpg"><img src="img/templates/default-images/thumbnails/IMAL_18388.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_18810" data-attr="IMAL_18810" data-path="/img/templates/default-images/placeholders/' + path + '-18810.jpg"><img src="img/templates/default-images/thumbnails/IMAL_18810.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_50187" data-attr="IMAL_50187" data-path="/img/templates/default-images/placeholders/' + path + '-50187.jpg"><img src="img/templates/default-images/thumbnails/IMAL_50187.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_50997" data-attr="IMAL_50997" data-path="/img/templates/default-images/placeholders/' + path + '-50997.jpg"><img src="img/templates/default-images/thumbnails/IMAL_50997.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_51641" data-attr="IMAL_51641" data-path="/img/templates/default-images/placeholders/' + path + '-51641.jpg"><img src="img/templates/default-images/thumbnails/IMAL_51641.jpg"></a>';
            ohtml += '<a href="#" class="image-selector-thumb IMAL_58060" data-attr="IMAL_58060" data-path="/img/templates/default-images/placeholders/' + path + '-58060.jpg"><img src="img/templates/default-images/thumbnails/IMAL_58060.jpg"></a>';
            ohtml += '</div>';

            //Custom image upload
            ohtml += '<div class="upload-div pull-left" style="margin-top:11px;">';
            ohtml += '<div class="fileupload fileupload-new" data-provides="fileupload">';
            ohtml += '<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_' + temp_name + '" data-attr="' + temp_name + '" type="file" name="files[]"></span>';
            ohtml += '</div>';
            ohtml += '<div id="progress-' + temp_name + '" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 400px;min-height:11px;display:none;"><div class="bar"></div></div>';
            ohtml += '<div id="shown-' + temp_name + '" class="files" style="clear:both;"></div>';
            ohtml += '</div>';
            return ohtml;
        }

        var removeTemplate = function(handle){
            $('#' + handle).remove();
            $('#space_' + handle).remove();
        }

        var loadTemplate2 = function(fileName,type, oObj, oSelector){

            tplInjector.temp_elm      = oObj;
            tplInjector.temp_name     = fileName;
            tplInjector.temp_type     = type;
            tplInjector.temp_selector = oSelector;
            $.ajax({

                url: 'email_templates/loader?file=' + fileName + '&type=' + type,
                type: 'GET',
                success: function(result) {
                    var oCont = (tplInjector.temp_elm);
                    // tplInjector.injectSpace(oCont,tplInjector.temp_name);
                    oCont.append('<div class="tpl-container" id="'  + tplInjector.temp_name + '">' + result + '</div>');
                    // $(tplInjector.temp_elm).html(result);
                    oCont.removeClass('hide');

                    tplInjector.temp_elm  = null;
                    tplInjector.temp_name = null;
                    tplInjector.temp_type = null;
                    tplInjector.temp_selector = null;
                    tplEditor.initialize();
                }
            });
        }



        var loadTemplate = function(fileName,type, oObj, oSelector, oData, oParams){

            tplInjector.temp_elm      = oObj;
            tplInjector.temp_name     = fileName;
            tplInjector.temp_type     = type;
            tplInjector.temp_selector = oSelector;
            tplInjector.temp_data     = oData;
            
            if(tplInjector.temp_data !==undefined)
                tplInjector.temp_data['params'] = oParams;

            //If Orhan call with no data
            if(tplInjector.temp_name === undefined && oData['data-0'] ==undefined){
                tplInjector.temp_load_step = tplInjector.temp_load_step +1;
                tplEditor.renderCallback(tplInjector.temp_load_step);
                return;
            }

            $.ajaxSetup({ cache: false });

            $.ajax({

                url: 'email_templates/loader?file=' + fileName + '&type=' + type + '&dc=' + (new Date().getTime()),
                type: 'GET',
                cache: false,
                success: function(result) {
                    var oCont        = (tplInjector.temp_elm);
                    var txtSelector  = $(tplEditor.emailStub).attr('id') + '-' +  tplInjector.temp_name;
                    
                    templateCheck = 0; sideBarStyle = "";

                    if( (tplEditor.tactic_type === "newsletter" || tplEditor.tactic_type === "5") && type === 'modules' ) { // tactic is newsletter

                        sideBarCheck = tplInjector.temp_name.split('-');
                        if(sideBarCheck[0] == "sidebar" && sideBarCheck[sideBarCheck.length-1] == "b") { // if sidebar module
                            if(!$(oCont).hasClass('asset-modules-rightbar') && $(oCont).hasClass('asset-modules-leftbar'))
                                oCont = $(oCont).next();
                            else if(!$(oCont).hasClass('asset-modules-rightbar') && !$(oCont).hasClass('asset-modules-leftbar'))
                                oCont = $(oCont).next().next();

                        } 
                        else if(sideBarCheck[sideBarCheck.length-1] == "b") { // if template b modules 
                            if(!$(oCont).hasClass('asset-modules-leftbar'))
                                oCont = $(oCont).next();
                        } 

                        if(sideBarCheck[sideBarCheck.length-1] == "b") // if template b modules
                            templateCheck = 1;
                    }

                    if(oCont === null) return;

                    if(tplInjector.temp_type !=='modules') {
                        oCont.html('');
                        oCont.append('<div class="tpl-container" id="'  + txtSelector + '" data-attr="' +  tplInjector.temp_name + '">' + result + '</div>');
                    } else {
                        // move and remove buttons
                        extraButtons = '<div class="button-move" '+sideBarStyle+'><button type="button" class="btn btn-primary move-btn"><i class="glyphicon glyphicon-move popover-hover" data-toggle="popover" data-content="Move" data-original-title="" title=""></i></button></div><div class="button-remove" '+sideBarStyle+'><button type="button" class="btn btn-primary remove-btn"><i class="glyphicon glyphicon-remove popover-hover" data-toggle="popover" data-content="Remove" data-original-title="" title=""></i></button></div></div>';
                        
                        if(templateCheck == 1) {  // if newsletter 2nd template
                            sideBarStyle = "style='float:left;position:static'";
                            extraButtons = '<div class="button-remove" '+sideBarStyle+'><button type="button" class="btn btn-primary remove-btn"><i class="glyphicon glyphicon-remove popover-hover" data-toggle="popover" data-content="Remove" data-original-title="" title=""></i></button></div></div>';
                        }

                        oCont.append('<div class="tpl-container" id="'  + txtSelector + '" data-attr="' +  tplInjector.temp_name + '">' + result + extraButtons);
                    }

                    oCont.removeClass('hide');

                    var oAppendElm = $('#' + txtSelector);
                    var oImage = (oAppendElm.find('.image-box-custom'));
                    if(oImage.length){
                        var oImage_Selector = tplInjector.temp_elm.after(tplInjector.injectImageSelector(txtSelector, tplInjector.temp_name));
                        oImage.attr('id', 'image-box-custom-' + txtSelector);
                        oImage.attr('data-thumb-sel', txtSelector);
                        oImage.attr('data-parent', tplInjector.temp_name);
                        tplInjector.initImageSelector(txtSelector);
                    }

                    if(tplInjector.temp_selector && tplInjector.temp_type !== 'modules'){
                        tplInjector.temp_selector.addClass('hide');
                        tplInjector.temp_selector.prev().addClass('hide');
                        if(tplInjector.temp_type === "footer"){
                            tplInjector.temp_selector.prev().prev().addClass('hide');
                            tplInjector.temp_selector.prev().prev().prev().addClass('hide');
                        }
                        if(tplInjector.temp_type === "header")
                            tplInjector.temp_selector.prev().prev().addClass('hide');
                    }
                    $(oCont.parent().find('.button-refresh')[0]).removeClass('hide');

                    tplInjector.temp_elm        = null;
                    tplInjector.temp_name       = null;
                    tplInjector.temp_selector   = null;
                    //IF data is passed inject it

                    if(tplInjector.temp_data !==null && tplInjector.temp_data !== undefined){
                        tplInjector.temp_load_step  = tplInjector.temp_load_step +1;

                        if(tplInjector.temp_type !== 'modules'){
                            tplInjector.applyDatanNames(oAppendElm);
                            injectData(tplInjector.temp_data, oAppendElm);
                            tplInjector.temp_load_callback(tplInjector.temp_load_step);
                            // tplEditor.renderCallback(tplInjector.temp_load_step);

                        }else{
                            
                            tplEditor.init_duplicates();
                            if(tplInjector.temp_data.params['duplicates'] > 0){
                                for (var i = tplInjector.temp_data.params.duplicates - 1; i >= 0; i--) {
                                    tplEditor.injectModuleClone($(oAppendElm.find('.module-btn')[0]));
                                };
                            }
                            tplInjector.applyDatanNames(oAppendElm);
                            injectData(tplInjector.temp_data, oAppendElm);
                            tplEditor.modCount = tplEditor.modCount+1;
                            tplEditor.renderModules();
                        }
                    }else{
                        tplInjector.applyDatanNames(oAppendElm);
                    }

                    tplEditor.reinitialize();

                }
            });
        }

        var injectData = function(oData, oElm){
            injectDataTypes(oData, '.wys-inline' , oElm);
            injectDataTypes(oData, '.wys-inline-simple', oElm);
            injectDataTypes(oData, '.wys-inline-link', oElm);
            injectDataTypes(oData, '.wys-inline-color', oElm);
            injectDataTypes(oData, '.social-icon', oElm);
            injectDataTypes(oData, '.image-box-custom', oElm);
            injectDataTypes(oData, '.image-box-module', oElm);
            injectDataTypes(oData, '.register-fields', oElm);
        }

        var injectDataTypes = function(oData, type, oElm){
            var oArr = oElm.find(type);
            var otObj = null;
            for (var i = oArr.length - 1; i >= 0; i--) {
                otObj=  $(oArr[i]);
                if(type === '.image-box-custom' || type=== '.image-box-module'){
                    var oImgdata =  oData[otObj.attr('data-name')];
                    if(typeof oImgdata !== 'object') continue;

                    if(oImgdata.type === 'default'){
                        $('#image-selector-' + otObj.attr('data-thumb-sel')).find('.' + oImgdata.name).addClass('active');
                        otObj.attr('src', oImgdata.src);
                        otObj.attr('data-img', oImgdata.name);
                        otObj.attr('data-type', 'default');
                    }else if(oImgdata.type === 'custom'){
                        otObj.attr('src', 'img/templates/default-images/placeholders/' + otObj.attr('data-parent') + '-custom.png');
                        otObj.attr('data-img', oImgdata.name);
                        otObj.attr('data-type', 'custom');
                        var tSelector = otObj.attr('data-thumb-sel');
                        $('#shown-' + tSelector).empty();
                        var imageName = oImgdata.name.split("/");
                        var filebox = '<div name="' + (oImgdata.name) + '">';                         
                        filebox += '<p><a class="btn btn-danger delete-file" data="' + (oImgdata.name) + '" data-attr="'+ otObj.attr('data-parent') + '" href="#"><i class="icon-trash icon-white"></i></a><a target=_blank href="'+oImgdata.name+'">' + imageName[imageName.length-1] + '</a></p>';
                        filebox += '</div>';
                        $(filebox).appendTo('#shown-' + tSelector);
                        $('.delete-file').on('click',tplInjector.deleteImageCallback);

                    }else if(oImgdata.type === 'inline'){
                        otObj.attr('src', oImgdata.src);
                        otObj.attr('data-img', oImgdata.name);
                        otObj.attr('data-type', oImgdata.type);
                    }
                }
                else if(type === ".social-icon"){
                    $(".social-table .wys-inline-link").parent().remove();
                    if(oData["data-icons"])
                        var items = oData["data-icons"].split("</a>");
                        for(var z=0; z< items.length; z++){
                            if(items[z] !== "")
                                $(".social-table").prepend("<td class='social-icons-saved'>"+tplEditor.unespaceHtml(items[z])+"</td>");    
                        }
                }else if(type === ".register-fields"){
                    // remove the default fields
                    var formFields = document.getElementById("registerFields");
                    
                    while (formFields.hasChildNodes()) {
                        formFields.removeChild(formFields.lastChild);
                    }
                    // append the saved fields
                    if(oData["fields-form"])
                        var savedFields = tplEditor.unespaceHtml(oData["fields-form"]);
                        $(".register-fields").html(savedFields);    
                }
                else{
                    otObj.html( tplEditor.unespaceHtml(oData[otObj.attr('data-name')]));
                }
            };
        }

        var applyDatanNames = function(oSelector){
            var t_count = 0;
            t_count = applyDataCount(oSelector.find('.wys-inline') ,t_count);
            t_count = applyDataCount(oSelector.find('.wys-inline-simple') ,t_count);
            t_count = applyDataCount(oSelector.find('.wys-inline-link') ,t_count);
            t_count = applyDataCount(oSelector.find('.wys-inline-color') ,t_count);
            t_count = applyDataCount(oSelector.find('.image-box-custom') ,t_count);
            t_count = applyDataCount(oSelector.find('.image-box-module') ,t_count);
        }

        var applyDataCount =  function(oData, t_count){
            var otObj   = null;
            for (var i = oData.length - 1; i >= 0; i--) {
                otObj = oData[i];
                $(otObj).attr('data-name', 'data-' + t_count);
                t_count++;
            };
            return t_count;
        }

        var deleteImageCallback = function(e){
            e.preventDefault();
            var otObj       = $(this);
            var txtSelector = $(tplEditor.emailStub).attr('id') + '-' + otObj.attr('data-attr');
            var oNode       = $('#image-box-custom-' + txtSelector );
            var oProgress   = $('#progress-' + txtSelector);
            otObj.parent().parent().remove();
            oProgress.find('.bar').css('width', '0%');
            oProgress.css('display','none');
            oNode.removeAttr('data-img');
            oNode.removeAttr('data-type');
            oNode.attr('src',  'img/templates/default-images/placeholders/' + oNode.attr('placeholder'));
        }

        var initImageSelector = function(temp_name){

            //Handler for image carosul click : change image
            $('.image-selector-thumb').off();
            $('.image-selector-thumb').on('click',function(e){
                e.preventDefault();
                //Check if Custom image was present
                var oThumb = $(this);
                var oData  = oThumb.parent().attr('data-attr');
                var oNode  = $('#image-box-custom-' + oData);                
                if(oNode.attr('data-type') === 'custom'){
                    var txtSelector =  oNode.attr('data-thumb-sel');
                    var oProgress   = $('#progress-' + txtSelector);
                    oProgress.find('.bar').css('width', '0%');
                    oProgress.css('display','none');
                    $('#shown-' + txtSelector).html('');
                    oNode.removeAttr('data-img');
                    oNode.removeAttr('data-type');
                }

                oNode.attr('src', oThumb.attr('data-path'));
                oNode.attr('data-img', oThumb.attr('data-attr'));
                oNode.attr('data-type', 'default');

                $(oThumb).closest('.image-selector').find('.image-selector-thumb').removeClass('active');
                $(oThumb).addClass('active');
                //$('#image-selector-' + oData).addClass('hide');
            });
            
            $('#fileupload_' + temp_name).off();
            $('#fileupload_' + temp_name).fileupload({
                url: 'server/php/',
                dataType: 'json',
                processalways: function (e, data) {
                    var index = data.index,
                        file = data.files[index],
                        node = $(data.context.children()[index]);
                    if (file.preview) {
                        node
                            .prepend('<br>')
                            .prepend(file.preview);
                    }
                    if (file.error) {
                        node
                            .append('<br>')
                            .append(file.error);
                    }
                    if (index + 1 === data.files.length) {
                        data.context.find('button')
                            .text('Upload')
                            .prop('disabled', !!data.files.error);
                    }
                },
                done: function (e, data) {
                    var txtSelector = $(this).attr('data-attr');
                    $.each(data.result.files, function (index, file) {
                        $('#shown-' + txtSelector).empty();

                        var otObj = $('#image-box-custom-'+txtSelector);
                        // oNode.attr('src', file.url);
                        // oNode.attr('data-img', file.name);
                        // oNode.attr('data-type', 'custom');

                        // var filebox = '<div name="' + (file.name) + '">';
                        // filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" data-attr="' + oNode.attr('data-parent') + '" href="#"><i class="icon-trash icon-white"></i></a> ' + file.name + '</p>';
                        // filebox += '</div>';
                        // $(filebox).appendTo('#shown-' + txtSelector);

                        // $('#' + txtSelector).find('.image-selector-thumb').removeClass('active');
                        // $('.delete-file').on('click',tplInjector.deleteImageCallback);
                        var userId = $(".session-user-id").val(); 
                        // if(tplEditor.sessionUserId !== null && tplEditor.sessionUserId !== userId)
                        //     userId = tplEditor.sessionUserId;

                        otObj.attr('src', 'img/templates/default-images/placeholders/' + otObj.attr('data-parent') + '-custom.png');
                        otObj.attr('data-img', '/server/php/files/'+userId+'/'+file.name);
                        otObj.attr('data-type', 'custom');
                        var tSelector = otObj.attr('data-thumb-sel');
                        $('#shown-' + tSelector).empty();
                        var filebox = '<div name="' + (file.name) + '">';
                        filebox += '<p><a class="btn btn-danger delete-file" data="' + (file.name) + '" data-attr="'+ otObj.attr('data-parent') + '" href="#"><i class="icon-trash icon-white"></i></a><a target=_blank href="/server/php/files/'+userId+'/'+file.name+'">' + file.name + '</a></p>';
                        filebox += '</div>';
                        $(filebox).appendTo('#shown-' + tSelector);
                        $('.delete-file').on('click',tplInjector.deleteImageCallback);
                    });
                },

                progressall: function (e, data) {
                    var txtSelector = $(this).attr('data-attr');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress-' + txtSelector).show();
                    $('#progress-' +  txtSelector + ' .bar').css(
                        'width',
                        progress + '%'
                    );
                },
                fail: function (e, data) {
                    var txtSelector = $(this).attr('data-attr');
                    $.each(data.result.files, function (index, file) {
                        var error = $('<span/>').text(file.error);
                        $(error).appendTo('#shown-' + txtSelector);
                    });
                }
            });
            // ohtml += '<div class="upload-div pull-left" style="margin-top:11px;">';
            // ohtml += '<div class="fileupload fileupload-new" data-provides="fileupload">';
            // ohtml += '<span class="btn-file btn-green btn-fileupload-med"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_' + temp_name + '" data-attr="' + temp_name + '" type="file" name="files[]"></span>';
            // ohtml += '</div>';
            // ohtml += '<div id="progress-' + temp_name + '" class="progress progress-success progress-striped pull-left span3" style="margin-top: -30px;margin-left: 422px;display:none;"><div class="bar"></div></div>';
            // ohtml += '<div id="shown-' + temp_name + '" class="files" style="clear:both;"></div>';
            // ohtml += '</div>';
        }

        var refreshModule = function(oElm){
            var oCont = oElm.parent().parent();
            $(oCont.find('.thumbnail-ul')[0]).removeClass('hide');
            $(oCont.find('.template-head')[0]).removeClass('hide');
            $(oCont.find('.section-note')[0]).removeClass('hide');
            $(oCont.find('.image-selector')).remove();
            $(oCont.find('.button-refresh')[0]).addClass('hide');
            $(oCont.find('.upload-div')).remove();
            oCont = $(oCont.find('.asset-preview')[0]);
            oCont.html('');
            oCont.addClass('hide');
            // show clone option
            $(oCont.parent().find('.clone')[0]).removeClass('hide');

        }



        return {
            injectImageSelector : injectImageSelector,
            initImageSelector : initImageSelector,
            injectAddmore: injectAddmore,
            loadTemplate: loadTemplate,
            loadTemplate2: loadTemplate2,
            removeTemplate: removeTemplate,
            applyDatanNames:applyDatanNames,
            temp_elm: temp_elm,
            temp_name: temp_name,
            temp_type: temp_type,
            temp_selector: temp_selector,
            temp_load_callback:temp_load_callback,
            temp_data: temp_data,
            refreshModule: refreshModule,
            initialize: initialize,
            deleteImageCallback: deleteImageCallback
        }
    }

    // *********TEMPLATE ENGINE CLASS Responsible for
    // *********Editor functions - edit and sync templates
    var templateEngine = function(){
        var dirtyBit =  false;
        // var tplInjector = null;
        var tplHead = null;  // Head reference
        var tplModules = null;  // modules reference
        var tplFoot = null;  // Foot reference
        var tplActiveLink = null;
        var tpl_rp_count =  0;
        var tpl_wsy_count=  0;
        var modCount     =  0;
        var emailStub =  null; //Defines which type of template to load
        var tactic_type = null;
        var blnHeadOption  = 'off';
        var blnFooterOption  = 'off';
        var temp_load_callback = null;
        var sessionUserId = null;

        var initialize = function(emailStub){

            //Initialize params and refrences
            var me = this;
            $.fn.editable.defaults.mode = 'inline';
            me.tplHead     =    $('.tpl-head');
            me.tplModules  =    $('.tpl-modules');
            me.tplFoot     =    $('.tpl-foot');

            me.tactic_type   = (getParameterByName('type'));
            // me.blnHeadOption = 'off';
            //initilize Injector
            tplInjector        = new templateInjector();
            tplInjector.initialize();
            tplEditor.emailStub = (emailStub === undefined) ? getEmailStub() : emailStub;

            $("html, body").animate({ scrollTop: 0 }, "fast");


            /* USE DEFAULT CONFIGURATION */
            $("#default-configuration").on("click", function(){
                var defaultConf = $(this).children();
                var activeV = defaultConf.attr("val");
                if(activeV === "0"){
                    bootbox.confirm("Changing to a default configuration will cause the changes you made to your custom configuration to be lost.", function(result) {
                      if(result){
                        defaultConf.css("background-color", "#ffffff"); 
                        defaultConf.attr("val", "1");
                        $("input#template").val("default");
                        $('.build-header-footer').hide();
                      }else{
                        return;
                      }
                    });
                }else{
                    bootbox.confirm("Changing to a custom configuration will cause the changes you made to your default configuration to be lost.", function(result) {
                      if(result){
                        defaultConf.css("background-color", "transparent"); 
                        defaultConf.attr("val", "0");
                        $("input#template").val("custom");
                        $('.build-header-footer').show();
                      }else{
                        return;
                      }
                    });
                }
            });

            /* ASSET LIST CLICK */
            $('.asset-list').bxSlider({
                // slideWidth: 450,
                minSlides: 1,
                maxSlides: 12,
                moveSlides: 1,
                slideMargin: 10
            });

            $('.asset-list a').on("click", function(){
                var hrefV = $(this).attr("href");
                var defaultConf = $("#default-configuration").children().attr("val");
                $('.asset-list a .slide').removeClass("active-asset");
                $(this).children().addClass("active-asset");

                // SHOW MAIN DIV
                $(".template-block").hide();    // hide all pages
                if(defaultConf === "1" && hrefV !== "#global-elements"){
                    // GET DEFAULT TEMPLATES (header, body and footer)
                    var headerTemplate = "", bodyTemplate = "", footerTemplate = "";
                    var headerName = "", bodyName = "", footerName = "";
                    switch(hrefV){
                        case "#registration-page":
                            headerTemplate = "email_templates/microsite/headerRegister.php";
                            headerName = "headerRegister";
                            bodyTemplate = "email_templates/microsite/register.php";
                            bodyName = "register";
                            footerTemplate = "email_templates/microsite/footer-plain.php"
                            footerName = "footer-plain";
                            break;
                        case "#confirmation-page": 
                            headerTemplate = "email_templates/microsite/headerThankyou.php";
                            headerName = "headerThankyou";
                            bodyTemplate = "email_templates/microsite/body-thank-you.php";
                            bodyName = "body-thank-you";
                            footerTemplate = "email_templates/microsite/footer-plain.php"
                            footerName = "footer-plain";
                            break;
                        default:
                            headerTemplate = "email_templates/headers/headerF-default.php";
                            headerName = "headerF-default";
                            bodyTemplate = "email_templates/content/image_sidebar-default.php";
                            bodyName = "image_sidebar-default";
                            footerTemplate = "email_templates/footer/standard_foot-default.php"
                            footerName = "standard_foot-default";
                            break;
                    }

                    // LOAD DEFAULT CONFIGURATION
                    var data = "";
                    var divId = hrefV.replace('#', '')
                    $.ajax({
                       url: headerTemplate,
                       success: function(html) {
                            // add header
                            data = "<div class='asset-preview asset-header'><div class='tpl-container' id='"+divId+"-"+headerName+"' data-attr='"+headerName+"'>'"+html+"</div></div>";
                            $.ajax({
                                url: bodyTemplate,
                                success: function(html) {
                                    // add body
                                    data += "<div class='asset-preview asset-content'><div class='tpl-container' id='"+divId+"-"+bodyName+"' data-attr='"+bodyName+"'>'"+html+"</div></div>";
                                    $.ajax({
                                        url: footerTemplate,
                                        success: function(html) {
                                            // add footer
                                            data += "<div class='asset-preview asset-footer'><div class='tpl-container' id='"+divId+"-"+footerName+"' data-attr='"+footerName+"'>'"+html+"</div></div>";
                                            // get button and link texts
                                            var textVal = "Register";
                                            switch(hrefV){
                                                case '#reminder-email-one':
                                                case '#reminder-email-two':
                                                case '#confirmation-email':
                                                    textVal = "Attend";
                                                    break;
                                                case '#thank-you-email':
                                                case '#sorry-we-missed-you-email':
                                                    textVal = "Watch Replay";
                                                    break;
                                                default:
                                                    textVal = "Register";
                                                    break;
                                            }
                                            if($("div"+hrefV+"-default").length){
                                                // if the page already exist, just show it
                                                $("div"+hrefV+"-default").show();
                                            }else{
                                                // append new page to main div
                                                $("#template-previews").append('<div id="'+divId+'-default" data-attr="" class="template-block ">'+data+'</div>');
                                                tplInjector.applyDatanNames($("#"+divId+"-"+headerName));
                                                tplInjector.applyDatanNames($("#"+divId+"-"+bodyName));
                                                tplInjector.applyDatanNames($("#"+divId+"-"+footerName));
                                            }
                                           // update button and link texts
                                           $(".button-text a").text(textVal);
                                           $(".mobile-cta a").text(textVal);
                                           tplEditor.reinitialize();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }else if(defaultConf === "0" && hrefV !== "#global-elements"){
                    // custom configuration selected
                    $("div"+hrefV).removeClass("hide");
                    $("div"+hrefV+"-default").hide();
                    $("div"+hrefV).show();
                }else{
                    // show global elements page 
                    $("div"+hrefV).removeClass("hide");
                    $("div"+hrefV).show();
                }
            });


            // Duplicate headers/footers links
            $("p[class*='clone']").off();
            $("p[class*='clone']").on('click', me.toggleDefaultHeaderFooter);

            /* clone header/footer new functionality */
            // if(me.emailStub !== "#invitation-email"){
                var header = $($(me.emailStub).find('.asset-header')); 
                var footer = $($(me.emailStub).find('.asset-footer')); 
                // show headed clone text
                if(header.css('display') === "none")
                    $(header.parent().find('.clone')).removeClass('hide');
                // show footer clone text
                if(footer.css('display') === "none")
                    $(footer.parent().find('.clone')).removeClass('hide');
            // }
            /******/

            //me.initHeaderFooterBtns();

            var dataFilled = $(tplEditor.emailStub).attr('data-state');
            if(dbRecord !==undefined && dbRecord && dbRecord !== "" && dataFilled !== '1'){             
                setEditRecord();
                renderTemplate();
            } else if(tplEditor.isReplicateHeader() || tplEditor.isReplicateFooter()){
                if(tplEditor.isReplicateHeader()){
                    me.replicateHeader();
                }
                if(tplEditor.isReplicateFooter()){
                    me.replicateFooter();
                }
            }
            
            me.reinitialize();
        };


        var reinitialize = function(){
            var me = this;
            initHandlers();
            me.initialize_edits();
            me.init_duplicates();
            me.init_module_boxes();

        }

        var getEmailStub = function(){
            var eType = tplEditor.tactic_type;
            if(eType === 'webinar' || eType === '1')
                return '#invitation-email';
            else if(eType === 'landingpage' || eType === '7')
                return '#landing-page';
            else if(eType === 'email' || eType === '6')
                return '#single-email';
            else if(eType === 'enurture' || eType === '2')
                return '#enurture-email-0';
            else if(eType === 'newsletter' || eType === '5')
                return '#newsletter-email-0';
        }

        var toggleDefaultHeaderFooter = function(ev){
            var oToggle    = $(this);
            var opt = oToggle.attr("name");
            var oContainer = "";
            var cId = oToggle.parent().parent()[0].id;
            if(opt === "header-clone"){
                if($("#global-elements").find(".asset-header").css('display') !== "none"){
                    /* clone header */
                    oContainer = oToggle.parent().find('.asset-header');
                    $("#global-elements .asset-header .tpl-container").clone().appendTo(oContainer);

                    var pageId = oContainer.parent().parent()[0].id;
                    var bannerName = oContainer.find('.tpl-container').attr("data-attr");
                    var txtSelector = pageId+"-"+bannerName;
                    // change banner id
                    oContainer.find('.tpl-container').attr("id", pageId+"-"+bannerName);
                    // change image-box-custom attributes
                    oContainer.find(".tpl-container").find(".image-box-custom").attr("id","image-box-custom-"+pageId+"-"+bannerName);
                    oContainer.find(".tpl-container").find(".image-box-custom").attr("data-thumb-sel",pageId+"-"+bannerName);
                    // inject image-selector and upload-div
                    oContainer.after(tplInjector.injectImageSelector(txtSelector, bannerName));
                    // this function call is necessary because the events are not launched after the elements are dynamically loaded
                    tplInjector.initImageSelector(txtSelector);

                    // copy the custom image name
                    if(oContainer.find(".tpl-container").find(".image-box-custom").attr("data-type") === "custom"){
                        var cImage = $("#global-elements .files").html();
                        $("#shown-"+pageId+"-"+bannerName).html(cImage);
                        $('.delete-file').on('click',tplInjector.deleteImageCallback);
                    }

                    // hide thumbnail options
                    oToggle.parent().find(".thumbnail-header").addClass("hide");
                    // show cloned header
                    oToggle.parent().find(".asset-header").toggleClass("hide");
                    // show refresh button
                    oToggle.parent().find(".button-refresh").toggleClass("hide");
                    // hide header text
                    oToggle.parent().find(".template-head").toggleClass("hide");
                    // hide clone option
                    oToggle.parent().find(".clone").toggleClass("hide");
                }
            }else{
                if($("#global-elements").find(".asset-footer").css('display') !== "none"){
                    /* clone footer */
                    oContainer = oToggle.parent().find('.asset-footer');
                    $("#global-elements .asset-footer .tpl-container").clone().appendTo(oContainer);
                    // hide thumbnail options
                    oToggle.parent().find(".thumbnail-footer").addClass("hide");
                    // show cloned footer
                    oToggle.parent().find(".asset-footer").toggleClass("hide");
                    // show refresh button
                    oToggle.parent().find(".button-refresh").toggleClass("hide");
                    // hide footer text
                    oToggle.parent().find(".template-head").toggleClass("hide");
                    oToggle.parent().find(".section-note").toggleClass("hide");
                    // hide clone option
                    oToggle.parent().find(".clone").toggleClass("hide");
                    // clone social icons
                    var findSocialIcons = oContainer.find('.social-icon').parent();
                    if(findSocialIcons.length > 0){
                        for(var x=0; x< findSocialIcons.length; x++){
                            aIcon = findSocialIcons[x];
                            $(".social-icon").parent().addClass('wys-inline-link');
                        }
                    }
                } 
            }
            tplEditor.reinitialize();
        }


        var initHeaderFooterBtns =  function(){
            var me = this;
            if(me.tactic_type === 'webinar' && (me.emailStub === '#registration-page' || me.emailStub === '#confirmation-page'))
                $('.duplicate-header-footer-btns').hide();
            else
                $('.duplicate-header-footer-btns').show();
        }

        //Logic for checking if replication is needed
        var isReplicateHeader =  function(){
            var me = this;
            if(me.blnHeadOption === 'on' && me.tactic_type === 'webinar'){
                if(me.emailStub === '#registration-page' || me.emailStub === '#confirmation-page'){
                    return false;
                }else if(me.emailStub !== me.getEmailStub())
                    return true;
            }
            else if(me.blnHeadOption === 'on' && me.emailStub !== me.getEmailStub())
                return true;
            return false;
        }

        //Logic for checking if replication is needed
        var isReplicateFooter =  function(){
            var me = this;
            if(me.blnFooterOption === 'on' && me.tactic_type === 'webinar'){
                if(me.emailStub === '#registration-page' || me.emailStub === '#confirmation-page'){
                    return false;
                }else if(me.emailStub !== me.getEmailStub())
                    return true;
            }
            else if(me.blnFooterOption === 'on' && me.emailStub !== me.getEmailStub())
                return true;
            return false;
        }


        var replicateHeaderFooterCallback = function(step){
            if(step == 2){
                var txtStub   = tplEditor.getEmailStub(tplEditor.tactic_type);
                var oEmailObj = $(txtStub);
                var oConfig   = tplEditor.getTacticConfig(txtStub);

                if(oConfig.footer['value']){
                    var oSelector = $($(tplEditor.emailStub +' .thumbnail-footer')[0]);

                    // do not hide the refresh button in Invitation Email section
                    if(oSelector.parent().parent().attr("id") != "invitation-email")
                        oSelector.parent().find('.refresh-btn-footer').hide();

                    $($(tplEditor.emailStub +' .asset-footer')[0]).html('');
                    tplInjector.loadTemplate(oConfig.footer.value, 'footer', oSelector.next(), oSelector, oConfig.footer.data);
                }
            }
        }

        var replicateHeader = function(){
            //Check for Webinar Swap types
            var txtStub   = tplEditor.getEmailStub(tplEditor.tactic_type);
            var oEmailObj = $(txtStub);
            var oConfig   = tplEditor.getTacticConfig(txtStub);
            if(oConfig.header['value']){
                $(tplEditor.emailStub +  ' .image-selector').remove();
                $(tplEditor.emailStub +  ' .upload-div').remove();
                var oSelector = $($(tplEditor.emailStub +' .thumbnail-header')[0]);

                // do not hide the refresh button in "invitation email"
                if(oSelector.parent().parent().attr("id") != "invitation-email")
                    oSelector.parent().find('.refresh-btn').hide();

                tplInjector.temp_load_callback = tplEditor.replicateHeaderFooterCallback;
                tplInjector.temp_load_step     = 0;
                $($(tplEditor.emailStub +' .asset-header')[0]).html('');
                tplInjector.loadTemplate(oConfig.header.value, 'header', oSelector.next(), oSelector, oConfig.header.data);
            }
        }

        var replicateFooter = function(){
            //Check for Webinar Swap types
            tplInjector.temp_load_callback = tplEditor.replicateHeaderFooterCallback;
            tplInjector.temp_load_step     = 1;

            var txtStub   = tplEditor.getEmailStub(tplEditor.tactic_type);
            var oEmailObj = $(txtStub);
            var oConfig   = tplEditor.getTacticConfig(txtStub);
            if(tplEditor.blnHeadOption != "on"){
                tplEditor.replicateHeaderFooterCallback(2);
            }
        }

        var setEditRecord  = function(){
            editRecord     = JSON.parse(espapeSpecialCharacters(dbRecord));
            // set template owner
            tplEditor.sessionUserId = editRecord.user_id;
            var oStubObj   = $(tplEditor.emailStub);
            editRecord     = editRecord[ oStubObj.attr('data-attr') ];
            if(editRecord === null || editRecord === undefined || editRecord === ''){
                //console.log("Record Not present");
                return false;
            }
            
            return editRecord;
        }

        var renderTemplate = function(){
            tplInjector.temp_load_step = 1;
            tplInjector.temp_load_callback = tplEditor.renderCallback;

            $(tplEditor.emailStub +  ' .image-selector').remove();
            $(tplEditor.emailStub +  ' .upload-div').remove();
            var oSelector = $($(tplEditor.emailStub +' .thumbnail-header')[0]);

            if(editRecord)
                tplInjector.loadTemplate(editRecord.header.value, 'header', oSelector.next(), oSelector, editRecord.header.data);
        }

        var renderCallback = function(step){
            if(step === 2){
                oSelector = $($(tplEditor.emailStub +' .thumbnail-content')[0]);
                tplInjector.loadTemplate(editRecord.content.value, 'content', oSelector.next(), oSelector, editRecord.content.data);
            }else if(step === 3){
                tplEditor.modCount = 0;
                renderModules();
                tplEditor.modCount = 0;
            }else if(step === 4){
                if(tplEditor.isReplicateHeader() || tplEditor.isReplicateFooter()){
                    if(tplEditor.isReplicateHeader())
                        me.replicateHeader();

                    if(tplEditor.isReplicateFooter())
                        me.replicateFooter();
                }else{
                    oSelector = $($(tplEditor.emailStub +' .thumbnail-footer')[0]);
                    tplInjector.loadTemplate(editRecord.footer.value, 'footer', oSelector.next(), oSelector, editRecord.footer.data);
                    tplInjector.temp_load_step = null;
                    $(tplEditor.emailStub).attr('data-state', '1');
                }
            }
        }

        var renderModules = function(){
            var oSelector,oCont;
            if(editRecord.modules.length > 0 && editRecord.modules[tplEditor.modCount] !== undefined){
                oSelector = $($(tplEditor.emailStub+ ' .thumbnail-modules')[0]);
                oCont     = $($(tplEditor.emailStub+ ' .asset-modules')[0]);
                tplInjector.loadTemplate(editRecord.modules[tplEditor.modCount].value, 'modules', oCont, oSelector, editRecord.modules[tplEditor.modCount].data, editRecord.modules[tplEditor.modCount]);
            }else{
                renderCallback(4);
            }
        }

        var getParameterByName = function(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results == null ? false : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        var espapeSpecialCharacters = function(myJSONString){
            var myEscapedJSONString = myJSONString.replace(/\\n/g, "\\n")
                                                  .replace(/\\'/g, "\\'")
                                                  .replace(/\\"/g, '\\"')
                                                  .replace(/\\&/g, "\\&")
                                                  .replace(/\\r/g, "\\r")
                                                  .replace(/\\t/g, "\\t")
                                                  .replace(/\\b/g, "\\b")
                                                  .replace(/\\f/g, "\\f");
                                                  return myEscapedJSONString;
        }


        var initHandlers= function(){
            $('.thumbnail-load').off();
            $(".thumbnail-load").click(function(e){
                e.preventDefault();
                var oElm  = $(this);
                var oData = oElm.attr('data-path');
                if(oData === undefined){
                    return false;
                }

                assetPreview = 0;
                // tactic is newsletter and type is modules
                if( (tplEditor.tactic_type === "newsletter" || tplEditor.tactic_type === "5") && oElm.attr('data-type') === 'modules' ) { 
                    sideBarCheck = oData.split('-');
                    if(sideBarCheck[0] == "sidebar" && sideBarCheck[sideBarCheck.length-1] == "b") { // if sidebar module
                        assetPreview = 2;  
                    }
                    else
                    if (sideBarCheck[sideBarCheck.length-1] == "b") {  // normal template b module
                        assetPreview = 1;   
                    }
                }

                //Search container relevent to thumb
                var oSelector   = $(oElm.parent().parent());
                var oCont       = $(oSelector.parent().find('.asset-preview')[assetPreview])
                var strDataType = oElm.attr('data-type');
                if(strDataType !=='modules')
                    oCont.html('');
                tplInjector.loadTemplate(oData,strDataType , oCont, oSelector);
            });


            $('.refresh-btn').off();
            $('.refresh-btn').on('click',function(e){
                e.preventDefault();
                tplInjector.refreshModule($(this));
            });

            $('.refresh-btn-footer').off();
            $('.refresh-btn-footer').on('click',function(e){
                e.preventDefault();
                tplInjector.refreshModule($(this));
            });

            //Submit Form handler
            $('#submit-custom-temp-form').off();
            $('#submit-custom-temp-form, #template-save-exit').on('click',function(e){
                e.preventDefault();
                var fullData;
                var tactic_type = $(this).attr("val");
                if($("#default-configuration").children().attr("val") === "0"){
                    // save templates custom data                    
                    fullData = tplEditor.saveTemplate();
                }else{
                    // save templates default data
                    fullData = tplEditor.saveDefaultTemplate();
                }

                if($(this).attr("id") === "template-save-exit")
                    requestTactic(true, fullData, tactic_type);
                else
                    requestTactic(false, fullData, tactic_type, '#email-content', '#webinar-audience', 'email-md');
            });

            $('.button-move').off();
            $('.button-move').mousedown(function(e){
                //console.log('check');
                e.preventDefault();
                $( tplEditor.emailStub + ' .asset-modules').sortable({
                    // containment: 'parent',
                    handle: 'button',
                    cancel: '',
                    cursor: 'move',
                    helper: 'clone',
                    opacity: '0.8',
                    revert: 'invalid',
                    stop :  function(){
                        $( tplEditor.emailStub + ' .asset-modules' ).sortable( "destroy" );
                    }
                });
            });

            $('.button-remove').off();
            $('.button-remove').on('click',function(e){
                e.preventDefault();
                $(this).parent().remove();
            });

            $('a#tpl-cancel, .template-type-links a').off();
            $('a#tpl-cancel, .template-type-links a').on('click',function(e){
                e.preventDefault();
                var path = $(this).attr('href');

                bootbox.confirm("Are you sure you want to exit the template configuration? All unsaved data will be lost.", function(result) {
                  if(result){
                    document.location = path;
                  }else{
                    return;
                  }
                });
            });
        }


        var saveTactic = function(txtTactic){
            var oElm  = $('#' + txtTactic);
            var dataFilled = (oElm).attr('data-state');
            var configAttr = oElm.attr('data-attr');
            var oData    = {};
            
            if(oElm.length ===0){
                $('#' + $('#' + txtTactic).attr('data-attr')).val(JSON.stringify(oData));
                return;
            }

            if(oElm.length !== 0){
                ($('.asset-header .tpl-container'))  && copyModule(oData, $(oElm.find('.asset-header')[0])  ,   'header'  , (oElm.find('.asset-header .tpl-container')));
                ($('.asset-header .tpl-container'))  && copyModule(oData, $(oElm.find('.asset-content')[0]) ,  'content'  , (oElm.find('.asset-content .tpl-container')));
                ($('.asset-modules .tpl-container')) && copyModule(oData, $(oElm.find('.asset-modules')[0]) ,  'modules'  , (oElm.find('.asset-modules .tpl-container')));
                ($('.asset-header .tpl-container'))  && copyModule(oData, $(oElm.find('.asset-footer')[0])  ,   'footer'  , (oElm.find('.asset-footer .tpl-container')));
            }else if(dbRecord !==undefined && dbRecord && dbRecord !== "" && dataFilled !== '1'){
                var tempRecord     = JSON.parse(espapeSpecialCharacters(dbRecord));
                oData     = tempRecord[ configAttr ];
                //IF headers and footers are to be cloned
                if(tplEditor.blnHeadOption === 'on'){
                    var oConfig = tplEditor.getTacticConfig( tplEditor.getEmailStub() );
                    oData.header = oConfig.header;
                    oData.footer = oConfig.footer;
                }
            }

            var eType = tplEditor.tactic_type; 
            if(eType == 'enurture' || eType == 'newsletter') { // tactic type is enurture or newsletter
                field_type = txtTactic.split('-'); 

                if(field_type[1] == "email") {  // if email template
                    // fetch form fields
                    oData.email_name = oElm.find('#email_name_field').val(); 
                    oData.email_subject = oElm.find('#email_subject_field').val();
                    oData.send_date = oElm.find('#email_send_date').val();

                    // if send date is empty then set its value as empty string
                    if(oElm.find('#email_send_time').val() == "")
                        send_date = "";
                    else // else set time along with AM/PM
                        send_date = oElm.find('#email_send_time').val() + " " +oElm.find('#email_send_ampm').val();
                    
                    oData.send_time = send_date;

                } else { // else landing page
                    oData.landingpage_name = oElm.find('#landingpage_name_field').val();
                }
            } 
            $('#' + $('#' + txtTactic).attr('data-attr')).val(JSON.stringify(oData));
            return oData;
        }

        var getTacticConfig = function(txtTactic){
            var oElm  = $(txtTactic);
            var oData    = {};
            ($('.asset-header .tpl-container'))  && copyModule(oData, $(oElm.find('.asset-header')[0])  ,   'header'  , (oElm.find('.asset-header .tpl-container')));
            ($('.asset-header .tpl-container'))  && copyModule(oData, $(oElm.find('.asset-content')[0]) ,  'content'  , (oElm.find('.asset-content .tpl-container')));
            ($('.asset-modules .tpl-container')) && copyModule(oData, $(oElm.find('.asset-modules')[0]) ,  'modules'  , (oElm.find('.asset-modules .tpl-container')));
            ($('.asset-header .tpl-container'))  && copyModule(oData, $(oElm.find('.asset-footer')[0])  ,   'footer'  , (oElm.find('.asset-footer .tpl-container')));
            return oData;
        }

        var saveTemplate= function(e){
            
            var invitationEData = tplEditor.saveTactic('invitation-email');
            var emailOData = tplEditor.saveTactic('reminder-email-one');
            var emailTData = tplEditor.saveTactic('reminder-email-two');
            var confirmationEData = tplEditor.saveTactic('confirmation-email');
            var thankYData = tplEditor.saveTactic('thank-you-email');
            var sorryEData = tplEditor.saveTactic('sorry-we-missed-you-email');
            var registrationPData = tplEditor.saveTactic('registration-page');
            var confirmationPData = tplEditor.saveTactic('confirmation-page');
            var landingPData = tplEditor.saveTactic('landing-page');
            var singlEData = tplEditor.saveTactic('single-email');

            var eType = tplEditor.tactic_type; 
            if(eType == 'enurture' || eType == 'newsletter') { // if enurture or newsletter
                var TacticElements = $("#template-previews").children(); // get all preview childrens
                $.each(TacticElements, function( index, value ) {
                    TacticEle = $(value).attr("id");  // get id from id
                    tplEditor.saveTactic(TacticEle); // save all dynamic tactics 
                });
            }

            var formData = {};
            // get hidden fields values
            var a = $("#custom-template-form").serializeArray();
            $.each(a, function() {
                if (formData[this.name] !== undefined) {
                    if (!formData[this.name].push) {
                        formData[this.name] = [formData[this.name]];
                    }
                    formData[this.name].push(this.value || '');
                } else {
                    formData[this.name] = this.value || '';
                }
            });

            tacticData = {'invitation_email_config':JSON.stringify(invitationEData), 
                            'reminder_one_email_config':JSON.stringify(emailOData), 
                            'reminder_two_email_config':JSON.stringify(emailTData),
                            'confirmation_email_config':JSON.stringify(confirmationEData),
                            'thank_you_email_config':JSON.stringify(thankYData),
                            'sorry_email_config':JSON.stringify(sorryEData),
                            'registration_config':JSON.stringify(registrationPData),
                            'confirmation_config':JSON.stringify(confirmationPData),
                            'landing_page_config':JSON.stringify(landingPData),
                            'single_email_config':JSON.stringify(singlEData),
                        };

            var oFullData = $.extend({}, tacticData, formData);
            return oFullData;

        }

        var saveDefaultTemplate= function(e){
            
            var invitationEData = tplEditor.saveTactic('invitation-email-default');
            var emailOData = tplEditor.saveTactic('reminder-email-one-default');
            var emailTData = tplEditor.saveTactic('reminder-email-two-default');
            var confirmationEData = tplEditor.saveTactic('confirmation-email-default');
            var thankYData = tplEditor.saveTactic('thank-you-email-default');
            var sorryEData = tplEditor.saveTactic('sorry-we-missed-you-email-default');
            var registrationPData = tplEditor.saveTactic('registration-page-default');
            var confirmationPData = tplEditor.saveTactic('confirmation-page-default');
            var landingPData = tplEditor.saveTactic('landing-page-default');
            var singlEData = tplEditor.saveTactic('single-email-default');

            var formData = {};
            // get hidden fields values
            var a = $("#custom-template-form").serializeArray();
            $.each(a, function() {
                if (formData[this.name] !== undefined) {
                    if (!formData[this.name].push) {
                        formData[this.name] = [formData[this.name]];
                    }
                    formData[this.name].push(this.value || '');
                } else {
                    formData[this.name] = this.value || '';
                }
            });

            tacticData = {'invitation_email_config':JSON.stringify(invitationEData), 
                            'reminder_one_email_config':JSON.stringify(emailOData), 
                            'reminder_two_email_config':JSON.stringify(emailTData),
                            'confirmation_email_config':JSON.stringify(confirmationEData),
                            'thank_you_email_config':JSON.stringify(thankYData),
                            'sorry_email_config':JSON.stringify(sorryEData),
                            'registration_config':JSON.stringify(registrationPData),
                            'confirmation_config':JSON.stringify(confirmationPData),
                            'landing_page_config':JSON.stringify(landingPData),
                            'single_email_config':JSON.stringify(singlEData),
                        };

            var oFullData = $.extend({}, tacticData, formData);
            return oFullData;

        }

        /* functions used in save tactic process */ 
        var requestTactic =  function(exit, data, t_type, modalHide, modalShow, val){
          if(exit){
            data['redirect'] = 1;
          }
          $.ajax({
                type: "POST",
                url:'includes/_requests_'+t_type+'.php',
                data: data,
                cache: false,
                dataType: 'JSON',
                success: function(data) 
                { 
                  var response = null, newRequest = null;
                  if(data){  
                    if(data.moduleResponse){
                      response = data.moduleResponse;              
                      newRequest = response.insert_id;              
                    }
                  }

                  if(response === null || newRequest !== null){
                    if(exit){
                      window.location.href='review.php';
                    }else{
                      $('.box-button[val="'+val+'"]').addClass('complete');
                      updateIcons();
                      $(modalHide).modal('hide');
                      $(modalShow).modal({ backdrop: 'static' });
                      $(modalShow).modal('show');
                      if(val === "setup"){
                        enableBoxButtons();
                      }
                    }
                  }
               },
               error: function(data)
               {
                  console.log("error", data);
               }
          });
        }

        function enableBoxButtons(){
            $('.box-button-link').attr('data-toggle','modal');
            $('.inactive').removeClass('inactive');
        }

        function updateIcons(){
            $(".box-button").each(function(i,v){
                if($(v).hasClass('incomplete')|| $(v).hasClass('complete') ){
                    var menuType = $(v).attr("val");
                    $(v).children().find('.icon').removeClass(menuType);
                    $(v).children().find('.icon').addClass(menuType+"-active");
                }
            });
        }
        /* end functions used in save tactic process */ 

        var copyModule = function(oData, oSelector, key, oCont){
                oData[key] = {};
                if(key === 'modules'){
                    oData[key] = new Array();
                    var otObj     =  null;
                    var oClones   =  null;
                    var oJsonTail = null;
                    for (var i = oCont.length - 1; i >= 0; i--) {
                        otObj =  $(oCont[i]);
                        tplInjector.applyDatanNames(otObj);
                        oData[key][i] = {};
                        oData[key][i]['data']  = {};
                        oData[key][i]['value']  = otObj.attr('data-attr');
                        oData[key][i]['count']  = i;
                        
                        oData[key] = copyProperties(oData[key], otObj.find('.wys-inline') , i , true);
                        oData[key] = copyProperties(oData[key], otObj.find('.wys-inline-simple') , i , true);
                        oData[key] = copyProperties(oData[key], otObj.find('.wys-inline-link') , i , true);
                        oData[key] = copyProperties(oData[key], otObj.find('.wys-inline-color') , i , true);
                        oData[key] = copyProperties(oData[key], otObj.find('.image-box-custom') , i , true);
                        oData[key] = copyProperties(oData[key], otObj.find('.image-box-module') , i , false);
                        oData[key] = copyProperties(oData[key], otObj.find('.register-fields'), i, true);


                        //Copy Custom images and module inner Clones
                        oClones = otObj.find('.tpl-rp-clone');
                        oData[key][i]['duplicates']  = oClones.length;

                    };
                }
                else{
                    oData[key]['value'] = oCont.attr('data-attr');
                    oData[key]['data']  = {};
                    if(key === "footer")
                        oData[key]['data']["data-icons"]  = "";

                    oData = copyProperties(oData, oSelector.find('.wys-inline') , key , true);
                    oData = copyProperties(oData, oSelector.find('.wys-inline-simple') , key , true);
                    //oData = copyProperties(oData, oSelector.find('.wys-inline-link') , key , true);
                    oData = copyProperties(oData, oSelector.find('.social-icon').parent(), key, true);
                    oData = copyProperties(oData, oSelector.find('.wys-inline-color') , key , true);
                    oData = copyProperties(oData, oSelector.find('.image-box-custom') , key , false);
                    oData = copyProperties(oData, oSelector.find('.register-fields'), key, true);
                }
            return oData;
        }

        var copyProperties = function(oData, oEditables , key,  blnHtml){
            var oObj =  null;          
            for (var i = oEditables.length - 1; i >= 0; i--) {
                oObj = $(oEditables[i]);
                if(key==="footer" && oObj.get(0).tagName === "A"){
                    oObj.addClass("wys-inline-link");
                    oData[key]['data']["data-icons"] += (blnHtml) ? espaceHtml(oObj.parent().html()) : imageNode(oObj);
                }else{
                    if(oObj.attr('data-name') === "fields-form"){
                        // registration form
                        oData[key]['data'][oObj.attr('data-name')] = espaceHtml(oObj.html().replace(/^\s+|\n+/g, '').replace(/\>\s+\</g, '><'));
                    }else{
                        oData[key]['data'][oObj.attr('data-name')] = (blnHtml) ? espaceHtml(oObj.html()) : imageNode(oObj);
                    }
                }
            }
            return oData;
        }

        var imageNode  = function(oObj,type){
            var rtObj = {};
            rtObj.name = oObj.attr('data-img');
            rtObj.src  = oObj.attr('src');
            rtObj.type = oObj.attr('data-type');
            return rtObj;
        }


        var espaceHtml = function(str){
            var find = "\"";
            str = str.replace(new RegExp(find, 'g'),"&quot;");
            find = "'";
            str = str.replace(new RegExp(find, 'g'),"&#39;");
            // find = "/";
            // str = str.replace(new RegExp(find, 'g'),"&#47;");
            // find = "&";
            // str = str.replace(new RegExp(find, 'g'),"&amp;");
            // find = "<";
            // str = str.replace(new RegExp(find, 'g'),"&lt;");
            // find = ">";
            // str = str.replace(new RegExp(find, 'g'),"&gt;");

            return str;

        }

        var unespaceHtml = function(str){
            if(str){
                var find = "&quot;";
                str= str.replace(new RegExp(find, 'g'),"\"");
                find = "&#39;"
                str = str.replace(new RegExp(find, 'g'),"'");
                // find = "&#47;";
                // str = str.replace(new RegExp(find, 'g'),"/");
                // find = "&amp;";
                // str = str.replace(new RegExp(find, 'g'),"&");
                // find = "&lt;";
                // str = str.replace(new RegExp(find, 'g'),"<");
                // find = "&gt;";
                // str = str.replace(new RegExp(find, 'g'),">");
            }
            return str;
        }

        var init_duplicates= function(){
            //First remove all add mores
            $('.tpl-add-btn-row').remove();
            $('.tpl-rp-add-btn').remove();

            var me = this;
            var oTables =  $('.tpl-rp-tab');
            for (var i = oTables.length - 1; i >= 0; i--) {
                me.tpl_rp_count ++;
                var oTable = $(oTables[i]);
                var colCnt = oTable.attr('tpl-rp-data');
                var oTBody = (colCnt === 'div') ? oTable  : $(oTable.children()[0]);

                var ohandle= 'tpl-rp-' + me.tpl_rp_count;
                var oRows   = oTBody.children();
                var oRemoveBtn = oTBody.find('.button-mod-remove');
                if(oRemoveBtn.length > 0){
                    oRemoveBtn.off();
                    oRemoveBtn.attr('data-attr', ohandle);
                    oRemoveBtn.on('click', tplEditor.removeModuleClone);
                }
                oTable.attr('id',ohandle);

                //Choose Final TR / or Div
                var oFinal = null;
                for (var j = oRows.length - 1; j >= 0; j--) {
                    if ($(oRows[j]).hasClass('tpl-rp-skip'))
                        continue;
                    oFinal = $(oRows[j]);
                    break;
                };
                oFinal.attr('id',ohandle+'-final');
                // $(oRows[oRows.length-1]).attr('id',ohandle+'-final');

                var oAddBtn  = (colCnt === 'div') ?  oTBody.parent().append(tplInjector.injectAddmore(ohandle, 'div')) : oTBody.append(tplInjector.injectAddmore(ohandle));

                //Delegate Click Handler
                $('#' + ohandle + '-btn').on('click', tplEditor.addMoreClick);

            };
        }

        var getRepeatClone = function(oComp){
            var oComp  = $('#' + oComp.attr('data-parent'));
            var oClone = $.parseHTML(oComp.html());
            oClone     = $(oClone).addClass('tpl-rp-clone');
            oClone     = oClone.css('display','');
            return oClone;
        }

        var injectCloneBtns   = function(oBtn, ohandle){
            //oBtn.append('<div class="button-mod-remove"><button type="button" class="btn btn-danger ui-sortable-handle"><i class="glyphicon glyphicon-remove"></i></button></div>');
            oBtn.append('<div class="button-mod-remove"><a href="#" class="btn btn-danger delete-file" style="padding:4px 2px 4px 0;" onclick="return false;"><i class="icon-trash icon-white"></i></a> Remove</div>');
            var otObj = $(oBtn.find('.button-mod-remove')[0]);
            otObj.attr('data-attr', ohandle);
            otObj.on('click', tplEditor.removeModuleClone);
        }

        var removeModuleClone =  function(e){
            tplEditor.remove_edits();
            var otObj = $(this);
            var ohandle = otObj.attr('data-attr');

            var oTable  = $('#' + ohandle);
            var colCnt = (oTable.attr('tpl-rp-data'));
            var oTBody = (colCnt === 'div') ? oTable  : $(oTable.children()[0]);
            colCnt     = (colCnt === 'div') ? colCnt  : Number(colCnt);

            $('#' + ohandle +'-btn-row').remove();
            otObj.parent().remove();

            var oRows   = oTBody.children();
            var tArr;
            var blnSkipRow = false;

            if(colCnt !== 'div'){

                var oAdjustables = new Array();
                var activeRow;
                for (var i = oRows.length - 1; i >= 0; i--) {
                    activeRow = $(oRows[i]);
                    if (activeRow.hasClass('tpl-rp-skip')){
                        blnSkipRow = true;
                        continue;
                    }
                    tArr = activeRow.children().clone();
                    for (var j = tArr.length - 1; j >= 0; j--) {
                        oAdjustables.push(tArr[j]);
                    };
                    activeRow.children().remove();
                };


                //Adjust rows..
                if(blnSkipRow){
                    blnSkipRow =  oTBody.find('.tpl-rp-skip').clone();
                }
                oRows.remove();
                var rCount = Math.ceil((oAdjustables.length/colCnt));

                for (var i = 0; i < rCount; i++) {
                    if(i=== rCount -1)
                        oTBody.append('<tr id="' + ohandle +'-final"></tr>');
                    else
                        oTBody.append('<tr id="' + ohandle +'xf"></tr>');
                };
                rCount = 0;

                oRows = oTBody.children();
                for (var i = oAdjustables.length - 1; i >= 0; i--) {
                    if( $(oRows[rCount]).children().length !== colCnt){
                        $(oRows[rCount]).append(oAdjustables[i]);
                    }else{
                        rCount ++;
                        $(oRows[rCount]).append(oAdjustables[i]);
                    }

                    //If Last Object in row
                    if($(oRows[rCount]).children().length === colCnt)
                        $(oAdjustables[i]).css('padding-right','0px');
                    else
                        $(oAdjustables[i]).css('padding', $(oAdjustables[i]).attr('tpl-rp-style'));

                    tArr = $(oAdjustables[i]).find('.button-mod-remove');
                    if(tArr.length >0){
                        $(tArr[0]).on('click', tplEditor.removeModuleClone);
                    }
                };

                (blnSkipRow) && oTBody.append(blnSkipRow);
            }
            var oAddBtn  = (colCnt === 'div') ?  oTBody.parent().append(tplInjector.injectAddmore(ohandle, 'div')) : oTBody.append(tplInjector.injectAddmore(ohandle));
            $('#' + ohandle + '-btn').on('click', tplEditor.addMoreClick);
            tplEditor.init_module_boxes();
            tplEditor.initialize_edits();
        }

        var injectModuleClone = function(oBtn,e){

            tplEditor.remove_edits();
            (e!== undefined) && e.preventDefault();

            var ohandle = oBtn.attr('tpl-rp-data');

            var oTable  = $('#' + ohandle);
            var colCnt = (oTable.attr('tpl-rp-data'));
            var oTBody = (colCnt === 'div') ? oTable  : $(oTable.children()[0]);
            colCnt     = (colCnt === 'div') ? colCnt  : Number(colCnt);
            var oComp   = $(oTable.find('.tpl-rp-comp'));
            var oRows   = oTBody.children();

            var oFinal  = $('#' + ohandle + '-final'); //(oRows[oRows.length-2])
            var oBtnRow    = $('#' + ohandle +'-btn-row');

            if( colCnt === 'div'){
                var oClone = tplEditor.getRepeatClone(oComp);
                oTBody.append(oClone);
            }
            else if(oFinal.children().length !== colCnt){
                var oClone = (oFinal.children().length === colCnt-1) ? tplEditor.getRepeatClone(oComp).css('padding-right','0px') : tplEditor.getRepeatClone(oComp);
                oFinal.append(oClone);
            }else{
                //Creat new Row
                oFinal.attr('id', ohandle + 'xf');
                oBtnRow.remove();
                oTBody.append('<tr id="' + ohandle + '-final"></tr>');
                oFinal = $('#' + ohandle +'-final');

                var oClone = tplEditor.getRepeatClone(oComp);
                $('#' + ohandle +'-final').append(oClone);
                oTBody.append(tplInjector.injectAddmore(ohandle));
                $('#' + ohandle + '-btn').on('click', tplEditor.addMoreClick);
            }

            tplEditor.injectCloneBtns(oClone,ohandle);

            // tplEditor.reinitialize();
            tplEditor.init_module_boxes();
            tplEditor.initialize_edits();
        }


        var addMoreClick = function(e){
            var oBtn    = $(this);
            tplEditor.injectModuleClone(oBtn,e);
        }

        var init_duplicatesx = function(){
            //Imitialize dulicates
            var ihi_modules =  $('.module');
            var ihi_module  = null;
            var oParent     = null;

            for(var i=0; i<ihi_modules.length;i++){
                ihi_module = ihi_modules[i];
                ihi_module = $(ihi_module);
                oParent    = ihi_module.parent();
                $(oParent.append(getButton()));
                var oAppend = $(oParent[oParent.length-1]);
                oAppend = $(oAppend);

                oAppend.on('click',function(elm){
                    var oParent_clk = $(elm.target).parent();
                    var oMod_clk = oParent_clk.find('.module');
                    oMod_clk = oMod_clk.clone();
                    oMod_clk.insertBefore($(elm.target));
                    tplEditor.initialize_edits();
                });
            }
        };

        var edit_made = function(){
            templateEngine.dirtyBit = true;
        };

        var remove_edits     = function(){
            //Remove All first
            $('.wys').removeAttr('id');
            $('.wys-inline').removeAttr('id');
            $('.wys-inline-simple').removeAttr('id');
            $('.wys-inline-link').removeAttr('id');


            tinymce.remove(".wys");
            tinymce.remove(".wys-inline");
            tinymce.remove(".wys-inline-simple");
            tinymce.remove(".wys-inline-link");
        }

        var initialize_edits = function(){

            //simple text
            var me = this;
            tplEditor.remove_edits();
            me.tpl_wsy_count = 0;

            //X-editable for edit
            // $('.edit').editable({
            //     type: 'text',
            //     title: 'Edit',
            //     success: function(response, newValue) {
            //         tplEditor.edit_made();
            //     }
            // });

            //Wsywig for default View templates
            tinymce.init({
                selector: ".wys",
                plugins: [
                    "autolink lists link"
                ],
                toolbar: "bold italic | bullist numlist | link",
                menubar: false,
                statusbar : false,
                setup : function(ed) {
                      ed.on('keyup', function(e) {
                         var oT= $(ed.targetElm).attr('data-target');
                         $(oT).html(ed.getContent());
                         /* show edited default template */
                         var showme = "";
                         switch(oT){
                            case ".reminder_body_copy_text":
                                showme = "#reminder-email-one";
                                break;
                            case ".confirmation_body_copy_text":
                                showme = "#confirmation-email";
                                break;
                            case ".thank_you_body_copy_text":
                                showme = "#thank-you-email";
                                break;
                            case ".sorry_we_missed_you_body_copy_text":
                                showme = "#sorry-we-missed-you-email";
                                break;
                            case ".disclaimer_copy_text":
                            case ".registration_body_copy_text":
                            case ".checkbox_text":
                                showme = "#registration-page";
                                break;
                            case ".confirmation_page_copy_text":
                                showme = "#confirmation-page";
                                break;
                            case ".invitation_body_copy_text":
                                showme = "#invitation-email";
                                break;
                            default:
                                break;
                         } 
                         if(showme !== ""){
                            showEditedDefaultTemplate(showme);
                        }
                      });

                      ed.on('LoadContent', function(e) {
                         var oT= $(ed.targetElm).attr('data-target');
                         var edContent = ed.getContent();
                         if(edContent !==null && edContent !=='' && edContent !== undefined){
                            $(oT).html(edContent);
                         }
                      });

                      ed.on('focus', function(e) {
                        $(ed.targetElm).children().attr("contenteditable","true");
                      });
               }
             });


            //WsyWigs for Inline
            tinymce.init({
                selector: ".wys-inline",
                inline: true,
                plugins: [
                    "autolink lists link"
                ],
                toolbar: "bold italic | bullist numlist | undo redo | link",
                menubar: false,
                statusbar : false,
                setup : function(ed) {
                      ed.on('focus', function(e) {
                        $(ed.targetElm).children().attr("contenteditable","true");
                      });
               }
             });
            tinymce.init({
                selector: ".wys-inline-simple",
                inline: true,
                toolbar: "undo redo",
                menubar: false,
                statusbar : false,
                setup : function(ed) {
                      ed.on('focus', function(e) {
                        $(ed.targetElm).children().attr("contenteditable","true");
                      });
               }
             });
            tinymce.init({
                selector: ".wys-inline-link",
                inline: true,
                plugins: [
                    "autolink link"
                ],
                toolbar: "undo redo | link",
                menubar: false,
                statusbar : false,
                setup : function(ed) {
                      ed.on('focus', function(e) {
                        $(ed.targetElm).children().attr("contenteditable","true");
                      });
               }
             });

            //Wsywig for color picker
            tinymce.init({
                selector: ".wys-inline-color",
                inline: true,
                plugins: [
                    "autolink lists link textcolor"
                ],
                toolbar: "bold italic | bullist numlist | undo redo | link | forecolor",
                theme_advanced_text_colors : "436EB2,3CB54B,3b2315,ffffff,000000",
                menubar: false,
                statusbar : false,
                setup : function(ed) {
                      ed.on('focus', function(e) {
                        $(ed.targetElm).children().attr("contenteditable","true");
                      });
               }
            });
        }

        var injectInlineFileControls = function(ohandle,fileName){
            var imageName = fileName.split("/");
            var ohtml = '<div class="fup-overlay" id="'  + ohandle  + '-fup-overlay">';
            ohtml += '<a href="#" class="btn btn-danger delete-file" data-attr="' + ohandle + '" onclick="return false;">';
            ohtml += '<i class="icon-trash icon-white"></i></a><a target=_blank href="'+fileName+'">' + imageName[imageName.length-1] +  '</a></div>';
            return ohtml;
        }

        var inlineFileDelete = function(e){
            e.preventDefault();
            var otObj   = $(this);
            var ohandle = otObj.attr('data-attr');
            var oNode   = $(ohandle);

            otObj.parent().remove();
            oNode.attr('src' , 'img/templates/default-images/placeholders/' + oNode.attr('data-default'));
            oNode.removeAttr('data-img');
            oNode.removeAttr('data-type');
        }

        var init_module_boxes = function(){
            var otObj, ofObj;
            var oModuleImages = $(tplEditor.emailStub + ' .image-box-module');
            for (var i = oModuleImages.length - 1; i >= 0; i--) {
                otObj =  $(oModuleImages[i]);
                ofObj =  otObj.parent().prev();

                var ohandle = tplEditor.emailStub.substr(1) + '-fup-img-' + tplEditor.tpl_wsy_count;

                otObj.attr('id' , ohandle );
                ofObj.attr('for', ohandle );

                ofObj.attr('id' , tplEditor.emailStub.substr(1) +  '-fup-input-' + tplEditor.tpl_wsy_count );
                otObj.parent().attr('for', tplEditor.emailStub.substr(1) + '-fup-input-' + tplEditor.tpl_wsy_count );

                //Check if Filename creation is necessary
               // $('.fup-overlay .delete-file').off();
                if(otObj.parent().parent().find('.fup-overlay').length >0){
                    var oFupOverlay = $(otObj.parent().parent().find('.fup-overlay')[0]);
                    oFupOverlay.attr('id', '#' + ohandle +  '-fup-overlay');
                    $(oFupOverlay.children()[0]).attr('data-attr','#' + ohandle );
                    $(oFupOverlay.children()[0]).on('click',tplEditor.inlineFileDelete);
                }
                else if(otObj.attr('data-type') === 'inline' && otObj.attr('data-img') !== '' && otObj.attr('data-img') !== undefined ){
                    var oFupOverlay = $(tplEditor.injectInlineFileControls('#' + ohandle ,otObj.attr('data-img')));
                    otObj.parent().after(oFupOverlay);
                    $(oFupOverlay.find('.delete-file')[0]).on('click', tplEditor.inlineFileDelete);
                }

                tplEditor.tpl_wsy_count = tplEditor.tpl_wsy_count +1;
                ofObj.fileupload({
                    url: 'server/php/',
                    dataType: 'json',
                    processalways: function (e, data) {
                        var index = data.index,
                            file = data.files[index],
                            node = $(data.context.children()[index]);
                        if (file.preview) {
                            node
                                .prepend('<br>')
                                .prepend(file.preview);
                        }
                        if (file.error) {
                            node
                                .append('<br>')
                                .append(file.error);
                        }
                        if (index + 1 === data.files.length) {
                            data.context.find('button')
                                .text('Upload')
                                .prop('disabled', !!data.files.error);
                        }
                    },
                    done: function (e, data) {
                        var txtSelector = '#' + $(this).attr('for');
                        var userId = $(".session-user-id").val();
                        $.each(data.result.files, function (index, file) {

                            var oNode = $(txtSelector);
                            oNode.attr('src', 'img/templates/default-images/placeholders/' + oNode.attr('data-custom'));
                            oNode.attr('data-img', '/server/php/files/'+userId+'/'+file.name);
                            oNode.attr('data-type', 'inline');

                            // get and remove the previous fup-overlay element if it exists
                            var element = document.getElementById(txtSelector+"-fup-overlay");
                            if(element != null){
                                element.remove();
                            }

                            var oFupOverlay = $(tplEditor.injectInlineFileControls(txtSelector,'/server/php/files/'+userId+'/'+file.name));
                            oNode.parent().after(oFupOverlay);
                            $(oFupOverlay.find('.delete-file')[0]).on('click', tplEditor.inlineFileDelete);
                        });
                    },
                    fail: function (e, data) {
                        //console.log('fileupload failed');
                    }
                });
            };

        }

        var initialize_link_edits = function(){
            //Links
            $('#tpl-body a').popover({
                html : true,
                content: function() {
                    //console.log('popover event fired');
                    return getLinksEditable();
                }
            });


            //Stop all template links
            $('#tpl-body a').on('click',function(e){
                e.preventDefault();
                var me = tplEditor.tplActiveLink =  this;
                //console.log('Click event fired');

                $('.tpl-link-editable .editable-submit').on('click',function(e){
                    e.preventDefault();
                    var l_href  = $('.editable-input input[name="href"]').val();
                    var l_title = $('.editable-input input[name="title"]').val();

                    if(tplEditor.tplActiveLink){
                        var oObj = $(tplEditor.tplActiveLink);
                        tplEditor.tplActiveLink.href = l_href;
                        oObj.html(l_title + tplEditor.getLinkSuffix());
                        $(tplEditor.tplActiveLink).popover('hide');
                        tplEditor.tplActiveLink = null;

                    }
                    tplEditor.tplActiveLink = null;
                });

                $('.tpl-link-editable .editable-cancel').on('click', function(){
                    $(tplEditor.tplActiveLink).popover('hide');
                    tplEditor.tplActiveLink = null;
                });
            });

        }

        function getLinkSuffix(){
            return '<span class="hideblock" style="font-size: 18px;font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 18px;color:#ffffff;"> &gt;</span>';
        }

        function getLinksEditable(){
            var edt_html = '<div class="popover-content tpl-link-editable">';
            edt_html+= '<div><div class="editableform-loading" style="display: none;"></div>';
            edt_html+= '<form class="form-inline editableform" style="">';
            edt_html+= '<div class="control-group"><div><div class="editable-input"><label for="href">Href:</label><input name="href" type="text" class="input-medium" style="padding-right: 24px;"></input></div>';
            edt_html+= '<div class="control-group"><div><div class="editable-input"><label for="title">Link Title:</label><input name="title" type="text" class="input-medium" style="padding-right: 24px;"></input></div>';
            edt_html+= '<div class="editable-buttons editable-buttons-bottom"><button type="submit" class="btn btn-primary editable-submit"><i class="icon-ok icon-white"></i></button><button type="button" class="btn editable-cancel"><i class="glyphicon glyphicon-remove"></i></button></div></div><div class="editable-error-block help-block" style="display: none;"></div></div></form></div></div>';
            return edt_html;
        }


        function getEditableButtons(){
            return '<div class="editable-buttons editable-buttons-bottom"><button type="submit" class="btn btn-primary editable-submit"><i class="icon-ok icon-white"></i></button><button type="button" class="btn editable-cancel"><i class="glyphicon glyphicon-remove"></i></button></div>';
        }

        function  getButton(){
            return '<button type="button" class="btn-green add module-btn">Add more</button>';
        }


        return {
            init_duplicates : init_duplicates,
            initialize: initialize,
            reinitialize: reinitialize,
            initialize_edits: initialize_edits,
            saveTemplate: saveTemplate,
            saveDefaultTemplate: saveDefaultTemplate,
            requestTactic: requestTactic,
            enableBoxButtons: enableBoxButtons,
            updateIcons: updateIcons,
            saveTactic:saveTactic,
            dirtyBit : dirtyBit,
            tpl_rp_count: tpl_rp_count,
            tpl_wsy_count: tpl_wsy_count,
            modCount:modCount,
            tplHead: tplHead,
            emailStub: emailStub,
            tplModules: tplModules,
            tplFoot: tplFoot,
            edit_made: edit_made,
            getLinkSuffix: getLinkSuffix,
            unespaceHtml:unespaceHtml,
            // asyncLoad: asyncLoad,
            initialize_link_edits:initialize_link_edits,
            addMoreClick:addMoreClick,
            renderCallback: renderCallback,
            renderModules:renderModules,
            init_module_boxes:init_module_boxes,
            getRepeatClone:getRepeatClone,
            injectModuleClone:injectModuleClone,
            tactic_type:tactic_type,
            remove_edits:remove_edits,
            injectCloneBtns:injectCloneBtns,
            removeModuleClone:removeModuleClone,
            injectInlineFileControls:injectInlineFileControls,
            inlineFileDelete:inlineFileDelete,
            toggleDefaultHeaderFooter:toggleDefaultHeaderFooter,
            blnHeadOption:blnHeadOption,
            blnFooterOption:blnFooterOption,
            replicateHeader:replicateHeader,
            replicateFooter:replicateFooter,
            getTacticConfig:getTacticConfig,
            getEmailStub:getEmailStub,
            replicateHeaderFooterCallback:replicateHeaderFooterCallback,
            isReplicateHeader:isReplicateHeader,
            isReplicateFooter:isReplicateFooter,
            initHeaderFooterBtns:initHeaderFooterBtns,
            sessionUserId: sessionUserId
        }
    };