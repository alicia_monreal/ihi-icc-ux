var asInitVals = new Array(); //Datatables below

$(document).ready(function() {

   /* Change learn submenu icons on hover */
   $('#campaign-lnk').on({
      'click':function() {
           $('#submit-existing-campaign').hide();
              $('#submit-new-campaign').show();
      }
   });
    
  $(".learn-submenu li").on('mouseover', function(){
      activeIcon(this, 'menu');
  });
  $(".learn-submenu li").on('mouseout', function(){
      inactiveIcon(this, 'menu');
  });

  /* Change action area icons on hover */
  $(".menu-list li").on('mouseover', function(){
      activeIcon(this, 'action');
  });
  $(".menu-list li").on('mouseout', function(){
      inactiveIcon(this, 'action');
  });

  $(".inactive").parents('a').css("cursor", "not-allowed");

  tplEditor.updateIcons();

  $(".box-button").on('mouseover', function(){
      activeIcon(this, 'box-button');
  });
  $(".box-button").on('mouseout', function(){
      inactiveIcon(this, 'box-button');
  });

  function activeIcon(item, area){
      var menuType = $(item).attr("val");

      if(area == "menu"){
          $(item).children().removeClass(menuType);
          $(item).children().addClass(menuType+"-active");
      }else if(area == 'action'){
          $(item).children().find(".icon").removeClass(menuType);
          $(item).children().find(".icon").addClass(menuType+"-active");
      }else{
          if(!($(item).hasClass('complete')|| $(item).hasClass('incomplete'))){
          $(item).children().find('.icon').removeClass(menuType);
          $(item).children().find('.icon').addClass(menuType+"-active");
           
      }
      $(item).children().first().css('border','2px solid #1273c4');
      }
  }

  function inactiveIcon(item, area){
      var menuType = $(item).attr("val");
      if(area === "menu"){
          $(item).children().addClass(menuType);
          $(item).children().removeClass(menuType+"-active");
      }else if(area=='action'){
          $(item).children().find(".icon").addClass(menuType);
          $(item).children().find(".icon").removeClass(menuType+"-active");
      }else{
           if(!($(item).hasClass('complete')|| $(item).hasClass('incomplete'))){
          $(item).children().find('.icon').addClass(menuType);
          $(item).children().find('.icon').removeClass(menuType+"-active");
         
      }
       $(item).children().first().css('border','2px solid transparent');
      }
  }
  $('#generate-leads-true').click(function () {
          $('#generate-leads-fields').show();
      
   });

  $('#generate-leads-false').click(function () {    
          $('#generate-leads-fields').hide();
   });



  $('#add-addicional-info-lnk').on({
      'click':function(){
                    
        if(!$('#campaign_addicional_details').is(':visible')){
            $('#campaign_addicional_details').slideToggle('fast');
            $('#add-addicional-info-lnk').html('<i class="fa fa-minus"></i> Hide additional information');
        }else{
         
          $('#campaign_addicional_details').slideToggle('fast',function(){
              $('#add-addicional-info-lnk').html('<i class="fa fa-plus"></i> Add additional information');
              $('#generate-leads-false').prop('checked',true);
              $('#generate-leads-fields').hide();
             
          });
        
        }
      
      }
  });

/***** SAVE TACTIC FUNCTIONALITY ******/

  $(".new-campaign-setup").on({
    'click':function(){
      $('#submit-existing-campaign').hide();
      $('#submit-new-campaign').show();
    }
  })
  $(".existing-campaign-setup").on({
    'click':function(){
      $('#submit-existing-campaign').show();
      $('#submit-new-campaign').hide();
    }
  })
  

  /*  Campaign Setup options */
  $('#new-campaign-radio').on({
      'click':function(){
          $('#existing-campaign').hide();
          $('#campaign-form-fields').show();
          resetModal();
          $('#new-campaign-radio').prop('checked',true);
          $('#submit-existing-campaign').hide();
          $('#submit-new-campaign').show()

          
      }
  })
  $('#existing-campaign-radio').on({
      'click':function(){
          $('#existing-campaign').show();
          $('#campaign-form-fields').hide();
          $('.modal-backdrop').css('height','100%');
          $('#submit-new-campaign').hide() 
          $('#submit-existing-campaign').show();
      }
  })
  /******/

  // $('#submit-existing-campaign').hide();
  $('#submit-new-campaign').on({
      'click':function(){
       var tactic_type = $(this).attr("val");
       var formString= $('#content-update-form').serialize();
       if($('#content-update-form').valid())
        $.ajax({
              type: "POST",
              url:'includes/_requests_campaign.php',
              data: formString,
              cache: false,
              dataType: 'json',
              success: function(data) 
              { 
                // enable tactic setup modal             
                $('.box-button-setup').attr('data-toggle','modal');
                $('.box-button-setup .inactive').removeClass('inactive');

                $('.box-button[val="campaign-setup"]').removeClass('incomplete');
                $('.box-button[val="campaign-setup"]').addClass('complete');
                $('#campaign-setup').modal('hide');
                $('#'+tactic_type+'-setup').modal({ backdrop: 'static' });
                $('#'+tactic_type+'-setup').modal('show');
              },
             error: function(data)
             {}
        });
      }
  });
 $('#submit-existing-campaign').on({
    'click':function(){
     var tactic_type = $(this).attr("val");
     var request_search_campaign = $('#request_search_campaign').val();
     if($('#content-update-form').valid())
        $.ajax({
              type: "POST",
              url:'includes/_requests_campaign.php',
              data: {campaign_id: request_search_campaign,action:'setCampaign'},
              cache: false,
              dataType: 'json',
              success: function(data) 
              { 
                // enable tactic setup modal             
                $('.box-button-setup').attr('data-toggle','modal');
                $('.box-button-setup .inactive').removeClass('inactive');
                
                $('.box-button[val="campaign-setup"]').removeClass('incomplete');
                $('.box-button[val="campaign-setup"]').addClass('complete');
                $('#campaign-setup').modal('hide');
                $('#'+tactic_type+'-setup').modal({ backdrop: 'static' });
                $('#'+tactic_type+'-setup').modal('show');

             },
             error: function(data)
             {

            }
        });
    }
});


/* Form validation */
$('#webinar-setup-details-form').validate({
   ignore: ".ignore"
});
$('#webinar-setup-schedule-form').validate({
   ignore: ".ignore"
});
$('#content-update-form').validate({
  
});
$('#webinar-audience-form-1').validate();
$('#webinar-audience-form-2').validate();
$('#webinar-publishing-form').validate();
$('#email-setup-details-form').validate({
   ignore: ".ignore"
});
$('#email-setup-schedule-form').validate({
   ignore: ".ignore"
});
$('#landingpage-setup-details-form').validate({
   ignore: ".ignore"
});
$('#landingpage-setup-schedule-form').validate({
   ignore: ".ignore"
});
$('#newsletter-setup-details-form').validate({
   ignore: ".ignore"
});
$('#newsletter-setup-schedule-form').validate({
   ignore: ".ignore"
});
$('#enurture-setup-details-form').validate({
   ignore: ".ignore"
});
$('#enurture-setup-schedule-form').validate({
   ignore: ".ignore"
});
$('#upload-list-form').validate({
   ignore: ".ignore"
});


/* Save and continue */
$('#webinar-setup-save').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var webinar_setup = $('#webinar-setup-details-form,#webinar-setup-schedule-form').serialize();
      // if($('#webinar-setup-details-form').valid() && $('#webinar-setup-schedule-form').valid())
      // tplEditor.requestTactic(false, webinar_setup, tactic_type, '#webinar-setup', '#webinar-content', 'setup');
      var setup_forms = {detailsForm:$('#webinar-setup-details-form'), scheduleForm:$('#webinar-setup-schedule-form')}
          save_params = {tactic_type:tactic_type, setup_id:'#webinar-setup', next_module:'#webinar-content', setup:'setup'};
      setup_save_validation(setup_forms, webinar_setup, save_params);
    }
});

$('#webinar-content-save').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#default-widgets-form,#optional-widgets-form').serialize();
      tplEditor.requestTactic(false, data, tactic_type, '#webinar-content', '#webinar-audience', 'webinar-content');
    }
});

$('#audience-save').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var nextModal = "#webinar-publishing";
      var data = $('#webinar-audience-form-1,#webinar-audience-form-2').serialize();
      if(tactic_type == "email" || tactic_type == "landingpage" || tactic_type == "newsletter" || tactic_type == "enurture" ){
        nextModal = "#additional_information";
      }
      if($('#webinar-audience-form-1').valid())
        tplEditor.requestTactic(false, data, tactic_type, "#webinar-audience", nextModal, "audience");
    }
});

$('#publishing-save-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#webinar-publishing-form').serialize();
      checkbox_validation(data, 'topics_covered');
      if($('#webinar-publishing-form').valid())
        tplEditor.requestTactic(false,data, tactic_type, "#webinar-publishing", "#additional_information","publishing");
     }
});


$('#upload-list-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#upload-list-form').serialize();
      if($('#upload-list-form').valid())
        tplEditor.requestTactic(true, data, tactic_type);
     }
});

$('#email-setup-save').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var email_setup = $('#email-setup-details-form,#email-setup-schedule-form').serialize();
      // if($('#email-setup-details-form').valid() && $('#email-setup-schedule-form').valid())
      //     tplEditor.requestTactic(false, email_setup, tactic_type, '#email-setup', '#webinar-audience', 'setup');
      var setup_forms = {detailsForm:$('#email-setup-details-form'), scheduleForm:$('#email-setup-schedule-form')}
          save_params = {tactic_type:tactic_type, setup_id:'#email-setup', next_module:'#webinar-audience', setup:'setup'};
      setup_save_validation(setup_forms, email_setup, save_params);
    }    
});


/* Save and exit */
$('#webinar-setup-save-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var webinar_setup = $('#webinar-setup-details-form,#webinar-setup-schedule-form').serialize();
      // if($('#webinar-setup-details-form').valid() && $('#webinar-setup-schedule-form').valid())
      //   tplEditor.requestTactic(true, webinar_setup, tactic_type);
      var setup_forms = {detailsForm:$('#webinar-setup-details-form'), scheduleForm:$('#webinar-setup-schedule-form')}
          save_params = {tactic_type:tactic_type},
          validation_response = setup_save_validation(setup_forms, webinar_setup, save_params, 'save-exit');
      if(validation_response==='submit'){ 
          tplEditor.requestTactic(true, webinar_setup, tactic_type);
      }
    }
});
$('#webinar-content-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#default-widgets-form,#optional-widgets-form').serialize();
        tplEditor.requestTactic(true, data, tactic_type);
    }
});
$('#audience-save-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#webinar-audience-form-1,#webinar-audience-form-2').serialize();
      if($('#webinar-audience-form-1').valid())
        tplEditor.requestTactic(true, data, tactic_type);
     }
 });

$('#publishing-exit-btn').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#webinar-publishing-form').serialize();
      checkbox_validation(data, 'topics_covered');
      if($('#webinar-publishing-form').valid())
        tplEditor.requestTactic(true, data, tactic_type);
     }
});

$('#additional-info-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var data = $('#additional-information-form').serialize();
      tplEditor.requestTactic(true, data, tactic_type);
     }
});

$('#email-setup-save-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var email_setup = $('#email-setup-details-form,#email-setup-schedule-form').serialize();
      // if($('#email-setup-details-form').valid() && $('#email-setup-schedule-form').valid())
      //     tplEditor.requestTactic(true, email_setup, tactic_type);      
      var setup_forms = {detailsForm:$('#email-setup-details-form'), scheduleForm:$('#email-setup-schedule-form')}
          save_params = {tactic_type:tactic_type},
          validation_response = setup_save_validation(setup_forms, email_setup, save_params, 'save-exit');
      if(validation_response==='submit'){ 
          tplEditor.requestTactic(true, email_setup, tactic_type);
      }    
    }
});

/*Save and continue -  Landing page*/
$('#landingpage-setup-save-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var landing_setup = $('#landingpage-setup-details-form,#landingpage-setup-schedule-form').serialize();
      // if($('#landingpage-setup-details-form').valid() && $('#landingpage-setup-schedule-form').valid())
      //   tplEditor.requestTactic(true, landing_setup, tactic_type);
      var setup_forms = {detailsForm:$('#landingpage-setup-details-form'), scheduleForm:$('#landingpage-setup-schedule-form')}
          save_params = {tactic_type:tactic_type},
          validation_response = setup_save_validation(setup_forms, landing_setup, save_params, 'save-exit');
      if(validation_response==='submit'){ 
          tplEditor.requestTactic(true, landing_setup, tactic_type);
      }
    }
});
$('#landingpage-setup-save').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var landing_setup = $('#landingpage-setup-details-form,#landingpage-setup-schedule-form').serialize();
      // if($('#landingpage-setup-details-form').valid() && $('#landingpage-setup-schedule-form').valid())
      //     tplEditor.requestTactic(false, landing_setup, tactic_type, '#landingpage-setup', '#webinar-audience', 'setup');
      var setup_forms = {detailsForm:$('#landingpage-setup-details-form'), scheduleForm:$('#landingpage-setup-schedule-form')}
          save_params = {tactic_type:tactic_type, setup_id:'#landingpage-setup', next_module:'#webinar-audience', setup:'setup'};
      setup_save_validation(setup_forms, landing_setup, save_params);
    }
});

/*Save and continue -  Newsletter*/
$('#newsletter-setup-save-exit').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var newsletter_setup = $('#newsletter-setup-details-form,#newsletter-setup-schedule-form').serialize();
      // if($('#newsletter-setup-details-form').valid() && $('#newsletter-setup-schedule-form').valid())
      //    tplEditor.requestTactic(true, newsletter_setup, tactic_type);
      var setup_forms = {detailsForm:$('#newsletter-setup-details-form'), scheduleForm:$('#newsletter-setup-schedule-form')}
          save_params = {tactic_type:tactic_type},
          validation_response = setup_save_validation(setup_forms, newsletter_setup, save_params, 'save-exit');
      if(validation_response==='submit'){ 
          tplEditor.requestTactic(true, newsletter_setup, tactic_type);
      }
    }
});
$('#newsletter-setup-save').on({
    'click':function(){
      var tactic_type = $(this).attr("val");
      var newsletter_setup = $('#newsletter-setup-details-form,#newsletter-setup-schedule-form').serialize();
      // if($('#newsletter-setup-details-form').valid() && $('#newsletter-setup-schedule-form').valid())
      //     tplEditor.requestTactic(false, newsletter_setup, tactic_type, '#newsletter-setup', '#webinar-audience', 'setup');
      var setup_forms = {detailsForm:$('#newsletter-setup-details-form'), scheduleForm:$('#newsletter-setup-schedule-form')}
          save_params = {tactic_type:tactic_type, setup_id:'#newsletter-setup', next_module:'#webinar-audience', setup:'setup'};
      setup_save_validation(setup_forms, newsletter_setup, save_params);      
    }
});

/*Save and continue -  eNurture*/
$('#enurture-setup-save-exit').on({
    'click':function(){      
      var tactic_type = $(this).attr("val");
      var enurture_setup = $('#enurture-setup-details-form,#enurture-setup-schedule-form').serialize();
      // if($('#enurture-setup-details-form').valid() && $('#enurture-setup-schedule-form').valid())
      //   tplEditor.requestTactic(true, enurture_setup, tactic_type);       
      // checkbox_validation(enurture_setup, 'topics_covered');
      var setup_forms = {detailsForm:$('#enurture-setup-details-form'), scheduleForm:$('#enurture-setup-schedule-form')}
          save_params = {tactic_type:tactic_type, checkbox_validation:'yes'},
          validation_response = setup_save_validation(setup_forms, enurture_setup, save_params, 'save-exit');
      if(validation_response==='submit'){ 
          tplEditor.requestTactic(true, enurture_setup, tactic_type);
      }     
    }
});
$('#enurture-setup-save').on({
    'click':function(){      
      var tactic_type = $(this).attr("val");
      var enurture_setup = $('#enurture-setup-details-form,#enurture-setup-schedule-form').serialize();      
      var setup_forms = {detailsForm:$('#enurture-setup-details-form'), scheduleForm:$('#enurture-setup-schedule-form')}
          save_params = {tactic_type:tactic_type, setup_id:'#enurture-setup', next_module:'#webinar-audience', setup:'setup', checkbox_validation:'yes'};
      // checkbox_validation(enurture_setup, 'topics_covered');
      setup_save_validation(setup_forms, enurture_setup, save_params);
    }
});

$('#choose-template-save').on({
    'click':function(){      
      var tactic_type = $(this).attr("val");
      var choose_template = $('#choose-template-form').serialize(); 
      if(choose_template !== ""){
        var next_modal = "#custom-template";
        if(choose_template === "template=Standard+Template")
          next_modal = "#email-content";

        $("#choose-template").modal("hide");
        $(next_modal).modal({ backdrop: 'static' });
        $(next_modal).modal("show");
      }   
    }
});

function setup_save_validation(forms, serialize_forms, save_params, save_action){
  var detailsForm = forms.detailsForm,
      scheduleForm = forms.scheduleForm;

  if($("#details.tab-pane.active").length > 0 && detailsForm.valid()){
    $("#details-setup-tab").removeClass('active');
    $("#details.tab-pane").removeClass('active');                
    $("#schedule.tab-pane").addClass('active');
    $("#schedule.tab-pane").css('opacity', '1');
    $("#schedule-setup-tab").addClass('active');    
  }
  else if($("#schedule.tab-pane.active").length > 0){
    if(save_params.checkbox_validation){ checkbox_validation(serialize_forms, 'topics_covered'); }
    if(detailsForm.valid() && scheduleForm.valid()){
      if(save_action==='save-exit'){  
        var response = 'submit';
        return response;
      }
      else  
        tplEditor.requestTactic(false, serialize_forms, save_params.tactic_type, save_params.setup_id, save_params.next_module, save_params.setup);
    }
    else if(scheduleForm.valid()){
      $("#schedule-setup-tab").removeClass('active');
      $("#schedule.tab-pane").removeClass('active');
      $("#details-setup-tab").addClass('active');
      $("#details-setup-tab").css('opacity', '1');
      $("#details.tab-pane").addClass('active');
    }
  }
}

function checkbox_validation(form_serialize, checkbox_container){
  var checkbox_var = form_serialize.indexOf(checkbox_container);      
  if( checkbox_var==-1)
    $('.'+checkbox_container).css("border","1px solid #cc0000");
  else
    $('.'+topics_covered).css("border","none");
}

/***** END SAVE WEBINAR TACTIC FUNCTIONALITY ******/


// Upload List - List request type, select options: show the corresponding file.
$( "select#list_request_type" ).change(function(e) {
  var test = $('#list_request_type option:selected').val();
  $('.file_name').html('<a href="http://intel.com"> '+$('#list_request_type option:selected').text()+'.pdf</a>');
  $(".box_file_link ").show();
});

    // });
function resetModal(){
    $('#content-update-form').trigger('reset');
    $('textarea').text('');
    $('#generate_leads_false').prop('checked',false);
    $('#generate-leads-fields').hide();
    $('#campaign_addicional_details').hide();
    $('.modal-backdrop').css('height','100%');
    $('#add-addicional-info-lnk').html('<i class="fa fa-plus"></i> Add additional information');
}

    /* Show/hide learn submenu */
    $("#learn-menu").on('mouseover', function(){
        $(".learn-submenu").toggle();
    });
    $("#learn-menu").on('mouseout', function(){
        $(".learn-submenu").toggle();
    });
    $(".learn-submenu").on('mouseover', function(){
        $(this).show();
    });
    $(".learn-submenu").on('mouseout', function(){
        $(this).hide();
    });

    /* Show/hide my-account menu */
    $(".kt-icon").on("mouseover", function(e){
        showKtMenu();
    });
    $(".kt-icon").on("mouseout", function(e){
        hideKtMenu();
    });
    $(".kt-menu-ctn").on("mouseover", function(e){
        showKtMenu();
    });
    $(".kt-menu-ctn").on("mouseout", function(e){
        hideKtMenu();
    });
    // show my-account menu
    function showKtMenu(){
        $(".kt-icon").attr("src","img/kt-active.png")
        $('.kt-menu-ctn').show();
        $(".kt-icon").css("background", "#007dc6");
        $(".kt-icon").css("color", "#ffffff");
    }
    // hide my-account menu
    function hideKtMenu(){
        $(".kt-icon").attr("src","img/kt-icon.png")
        $('.kt-menu-ctn').hide(); 
        $(".kt-icon").css("background", "transparent");
        $(".kt-icon").css("color", "#007dc6");
    }

    /* Show/hide manage menu */
    $(".manage-menu, .manage-menu-ctn").on("mouseover", function(e){
        $('.manage-menu-ctn').show();
    });
    $(".manage-menu, .manage-menu-ctn").on("mouseout", function(e){
        $('.manage-menu-ctn').hide();
    });

    /* notification rules */
    $('#rules-recipients').chosen();
    $("#rules-recipients").change(function(){
        // show recipients list
        str = "";
        $(".chosen-recipients ul").empty();
        $( "#rules-recipients option:selected" ).each(function() {
          str = $( this ).text() + " ";
          email = $( this ).attr("val") + " ";
          $(".chosen-recipients ul").append("<li>"+str+" - "+email+"</li>");
        });
        
    });
    $('#default-assignee').chosen();
    /* end notification rules */

    // additional notifiers are applied jquery chosen
    $('#additional-notifiers').chosen(); 
    $("#additional-notifiers").change(function(){
        str = "";
        $(".chosen-notifiers ul").empty(); 
        $( "#additional-notifiers option:selected" ).each(function() {
          str = $( this ).text() + " ";
          email = $( this ).attr("val") + " ";
          $(".chosen-notifiers ul").append("<li>"+str+" - "+email+"</li>");
        });
        
    });

    // additional grant users are applied jquery chosen
    $('#additional-rights-users').chosen();
    $("#additional-rights-users").change(function(){
        str = "";
        $(".chosen-rights-users ul").empty();
        $( "#additional-rights-users option:selected" ).each(function() {
          str = $( this ).text() + " ";
          email = $( this ).attr("val") + " ";
          $(".chosen-rights-users ul").append("<li>"+str+" - "+email+"</li>");
        });
    });

    $("#placeholder-btn").click(function(){  // placeholder button clicked
        var url = '/ux/includes/placeholder-form.php';
        $("#message-notify").empty();
        $("#message-notify").css({'color':'red'});

        $.ajax({
            "url" : url,   // placeholder form load request
            "type" : "post",
            "dataType" : "text",
            "success" : function(data) {
                $("#handle-placeholder").html(data);
                $("#placeholder-loaded").click();

                $("#submit-request-form").on('click', function(){
                    $("#message-notify").empty();

                    start_date = $("#request-start-date").is(":visible");
                    end_date = $("#request-end-date").is(":visible");
                    start_time = $("#request-start-time").is(":visible");
                    end_time = $("#request-end-time").is(":visible");
                    campaign_search = $("#campaign-search-popup").is(":visible");

                    // if any of the required field is empty then error
                    if($("#name").val() == "" || $("#description").val() == "" || (start_date == 1 && $("#start_date").val() == "" )
                        || (end_date == 1 && $("#end_date").val() == "" ) || (start_time == 1 && $("#start_time").val() == "" ) 
                        || (end_time == 1 && $("#end_time").val() == "" ) || (campaign_search == 1 && $("#campaign_search_popup").val() == "" )) {  
                        $("#message-notify").css({'color':'red'});
                        $("#message-notify").html("Please enter the required fields");
                    }
                    else {
                        // serialize form data
                        var serialize_data = $("#placeholder-request-form").serialize();

                        $.ajax({
                            "url" : '/ux/includes/_placeholder-process.php', // form processing request
                            "type" : "post",
                            "dataType" : "html",
                            "data" : serialize_data,
                            "success" : function(data) {
                                if(data == "success") {
                                    $("#message-notify").css({'color':'green'});
                                    $("#message-notify").html("<b>Success!<b>");
                                    location.reload();  // page refresh to show the change in calendar
                                } else {
                                    console.log(data);
                                    $("#message-notify").css({'color':'red'});
                                    $("#message-notify").html(data);
                                }
                            }
                        });
                    }

                    return false;
                });
                
                var substringMatcher = function(strs) {
                return function findMatches(q, cb) {
                var matches, substringRegex;
                matches = [];
                substrRegex = new RegExp(q, 'i');

                $.each(strs, function(i, str) {
                  if (substrRegex.test(str)) {
                    matches.push({ value: str });
                  }
                });

                cb(matches);
                  };
                };

                // campaigns search 
                $('#campaign_search_popup').typeahead({
                  hint: true,
                  highlight: true,
                  minLength: 1
                },
                {
                  name: 'campaigns',
                  displayKey: 'value',
                  source: substringMatcher(campaign_popup)
                });

                $('#request-type-select').on('change', function (e) {
                    var optionSelected = $(this).find("option:selected");
                     var valueSelected  = optionSelected.val();

                     switch (valueSelected) {
                        case '1': // webinar
                            $("#request-end-date").hide();
                            $("#request-start-date").show();
                            $("#request-start-time").show();
                            $("#request-end-time").show();
                            $("#campaign-search-popup").show();
                            $("#additional-user-rights").show();
                            break;
                        case '5': // newsletter
                            $("#request-end-date").show();
                            $("#request-start-date").show();
                            $("#request-start-time").show();
                            $("#request-end-time").hide();
                            $("#campaign-search-popup").show();
                            $("#additional-user-rights").show();
                            break;
                        case '6':  // single email
                            $("#request-end-date").hide();
                            $("#request-start-date").show();
                            $("#request-start-time").show();
                            $("#request-end-time").hide();
                            $("#campaign-search-popup").show();
                            $("#additional-user-rights").show();
                            break;   
                        case '2': // eNurture   
                        case '8': // campaign    
                        case '7':  // landing page
                            $("#campaign-search-popup").show();
                            $("#additional-user-rights").show();

                            if(valueSelected == '8') {
                                $("#campaign-search-popup").hide();
                                $("#additional-user-rights").hide();
                            }

                            $("#request-end-date").show();
                            $("#request-start-date").show();
                            $("#request-start-time").hide();
                            $("#request-end-time").hide();
                            break;            

                        default:
                            break;    
                     }
                });
                
                // additional grant users are applied jquery chosen
                $('#additional-rights-users').chosen();
                $("#additional-rights-users").change(function(){
                    str = "";
                    $(".chosen-rights-users ul").empty();
                    $( "#additional-rights-users option:selected" ).each(function() {
                      str = $( this ).text() + " ";
                      email = $( this ).attr("val") + " ";
                      $(".chosen-rights-users ul").append("<li>"+str+" - "+email+"</li>");
                    });
                });
                
                // default btn classes are removed from bootbox dialog buttons
                $(".btn.btn-green.submit").removeClass('btn');
                $(".btn.btn-green.plain").removeClass('btn');

                $(".btn-green.submit").css({
                    'margin-right':'10px'
                });


                // call to datepicker after its loaded
                $('#datetimepicker, #datetimepicker_end').datepicker({
                    format: 'mm/dd/yyyy',
                    pickTime: false,
                    autoclose: true,
                    startDate: new Date()
                });
            }
        });
    }); 

    $("#rules-update-form").validate();
    /* end notification rules */

    //Placeholder Fix
    //$('input[placeholder]').placeholder();

    //New placeholder fix for modal issue
    $('input, textarea').placeholder();

    //Even and odd rows
    $(".zebra > div.row:odd").addClass("odd");
    $(".zebra > div.row:not(.even)").addClass("even");

    //Colorbox
    $(".various").colorbox({
        iframe:true,
        innerWidth:853,
        innerHeight:480,
        close:''
    });
    $(".video-link").colorbox({
        iframe:true,
        innerWidth:853,
        innerHeight:480,
        close:''
    });
    $(".cbox-image").colorbox({
        close:''
    });

    $('body').on('hidden.modal', '#modal', function () {
        $(this).removeData('modal');
        $(this).find('.modal-body').html('');
    });

    //Login Form
    //$('#login-submit').click(function(e){ //Using a submit input so Return works
    $('#login-form').submit(function(event) {        
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
        //e.preventDefault();

        var form_id = 'login-form';
        var url = 'includes/_login.php';
        var form_name = '#'+form_id;

        if ($(form_name).valid()) {
            $.ajax({
                "url" : url,
                "type" : "post",
                "dataType" : "text",
                "data" : $(form_name).serialize(),
                "success" : function(data) {
                    var data_trim = data.substring(0,1);
                    if (data_trim != '<') {
                        window.location = data;
                    } else {
                        $('h4.'+form_id+'-subtitle').html(data);
                    }
                }
            });
        }

        return false;
    });

    //Create Account Form
    $('form#request-form a.submit').click(function(event){
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8

        var form_id = $(this).parents('form:first').attr('id');
        var url = '/ux/includes/_create-account.php';
        var form_name = '#'+form_id;

        if ($(form_name).valid()) {
            $.ajax({
                "url" : url,
                "type" : "post",
                "dataType" : "text",
                "data" : $(form_name).serialize(),
                "success" : function(data) {
                    if (data == 'failure') {
                        var error_msg = 'That email account is already in use. If you\'ve forgotten your password, please use the Forgot Password link to retrieve it, or enter a different email. <a href="" data-dismiss="modal">Close window</a>';
                        $('h4.'+form_id+'-subtitle').html(error_msg);
                    } else {
                        $('h4.'+form_id+'-subtitle').html(data);
                        $(form_name).slideUp('fast');
                        $(form_name).css('min-height','50px');
                    }
                }
            });
        }
        
    });

    //Create account Role change
    $('select#role').on('change', function() {
        if (this.value == 2 && this.value != '') { //Only Agency selects see it
            $('#point_of_contact').addClass("required");
            $('#point_of_contact_email').addClass("required");
            $('#point-of-contact-info').show();
        } else {
            $('#point_of_contact').removeClass("required");
            $('#point_of_contact_email').removeClass("required");
            $('#point-of-contact-info').hide();
        }
    });

    //Forgot Password Form
    $('a#forgot-password').click(function(event){
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8

        var url = '/ux/includes/_forgot-password.php';
        var form_name = '#forgot-form';

        if ($(form_name).valid()) {
            $.ajax({
                "url" : url,
                "type" : "post",
                "dataType" : "text",
                "data" : $(form_name).serialize(),
                "success" : function(data) {
                    $('h4.forgot-form-subtitle').html(data);
                }
            });
            $(form_name).slideUp('fast');
            $(form_name).css('min-height','50px');
            $('.forgot-form-footer').slideUp('fast');
            $('.forgot-form-footer').css('min-height','50px');
            
        }
        
    });

    //Update Account Form
    $("#account-update-form").validate({
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: false,
                minlength: 6
            },
            password_confirm: {
                required: false,
                minlength: 6,
                equalTo: "#password"
            }
        },
        messages: {
            fname: {
                required: "This field is required"
            },
            lname: {
                required: "This field is required"
            },
            email: {
                required: "Valid email is required"
            },
            password: {
                minlength: "Please enter at least 6 chars"
            },
            password_confirm: {
                equalTo: "Passwords do not match"
            }
        }
    });

    //Contact Us Form
    $('form#contact-form a.submit').click(function(event){
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
        //e.preventDefault();

        var form_id = $(this).parents('form:first').attr('id');
        var url = '/includes/_contact-us.php';
        var form_name = '#'+form_id;

        if ($(form_name).valid()) {
            $.ajax({
                "url" : url,
                "type" : "post",
                "dataType" : "text",
                "data" : $(form_name).serialize(),
                "success" : function(data) {
                    if (data == 'failure') {
                        var error_msg = 'Uh oh. We had a problem sending your message. Please try again. <a href="" data-dismiss="modal">Close window</a>';
                        $('h4.'+form_id+'-subtitle').html(error_msg);
                    } else {
                        $('h4.'+form_id+'-subtitle').html(data);
                        $(form_name).slideUp('fast');
                        $(form_name).css('min-height','50px');
                    }
                }
            });
        }
        
    });

    //New Requirement Request Form
    $('form#new-requirement-request-form a.submit').click(function(event){
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8
        //e.preventDefault();

        var form_id = $(this).parents('form:first').attr('id');
        var url = '/includes/_contact-us.php';
        var form_name = '#'+form_id;

        if ($(form_name).valid()) {
            $.ajax({
                "url" : url,
                "type" : "post",
                "dataType" : "text",
                "data" : $(form_name).serialize(),
                "success" : function(data) {
                    if (data == 'failure') {
                        var error_msg = 'Uh oh. We had a problem sending your message. Please try again. <a href="" data-dismiss="modal">Close window</a>';
                        $('h4.'+form_id+'-subtitle').html(error_msg);
                    } else {
                        $('h4.'+form_id+'-subtitle').html(data);
                        $(form_name).slideUp('fast');
                        $(form_name).css('min-height','50px');
                    }
                }
            });
        }
        
    });

    //Submit form, upload files
    $('#submit-request-form').click(function() {
        $('#in_progress_form').val('0');
        $('#content-update-form').submit();
        return false;
    });
    $('#save-and-exit').click(function() {
        $('#in_progress_form').val('1');
        $('#content-update-form').submit();
        return false;
    });
    $('#submit-request-form-inline, #submit-request-form-inline-files, #submit-request-form-inline-speakers, .finish-edit').click(function() {
        $('#content-update-form-inline').submit();
        return false;
    });

    //Update request validate
    // $("#content-update-form").validate();
    $("#resource-update-form").validate();

    /* Webinar dte/time validation */
    $.validator.addMethod("validDate", function(value, element) {
        var start_date = $("#start_date").val();
        var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
        var start_ampm = $("#start_ampm").val();
        var end_ampm = $("#end_ampm").val();
        var newStartTime = new Date(start_date +" "+ start_time);
        var newEndTime = new Date(start_date +" "+ end_time) 
        
        if(start_date && start_time && end_time){
            if(start_ampm === "PM" && start_time !== "12:00" && start_time !== "12:30"){
                newStartTime = newStartTime.addHours(12);
            }else if(start_ampm === "AM" && (start_time === "12:00" || start_time === "12:30")){
                newStartTime = newStartTime.addHours(12);
                newStartTime = new Date(newStartTime.getTime()-(1000*60*60*24));
            }
            if(end_ampm === "PM" && end_time !== "12:00" && end_time !== "12:30"){
                newEndTime = newEndTime.addHours(12);
            }else if(end_ampm === "AM" && (end_time === "12:00" || end_time === "12:30")){
                newEndTime = newEndTime.addHours(12);
                newEndTime = new Date(newEndTime.getTime()-(1000*60*60*24));
            }                   
            
            var diff = ( newEndTime - newStartTime );
            if(diff < 0){
                $("#start_time").removeClass("valid");              
                $("#start_time").addClass("error");
                $("#end_time").removeClass("valid");
                $("#end_time").addClass("error");
                $(".wb-date-validation").removeClass("hide");
                return false;
            }else{
                $("#start_time").removeClass("error");              
                $("#start_time").addClass("valid");
                $("#end_time").removeClass("error");
                $("#end_time").addClass("valid");
                $(".wb-date-validation").addClass("hide");
                return true;
            }
        }
        return true;
    }, "* End Date can't be before the Start Date");


    //Not allowing special or foreign chars for Tag
    /*ONE METHOD, but disables pasting
    $("#tag").on("keypress", function(event) {
        var englishAlphabetDigitsAndWhiteSpace = /[A-Za-z0-9 ]/g;
        var key = String.fromCharCode(event.which);
        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetDigitsAndWhiteSpace.test(key)) {
            return true;
        }

        return false;
    });
    $('#tag').on("paste",function(e){
        e.preventDefault();
    });
    */
    $('#tag').on('keyup paste',function(event){
        oldtxt = $(this).val();
        var key = String.fromCharCode(event.which);
        if (event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 39) {
            newtxt = oldtxt.replace(/[^a-zA-Z0-9 _.,!"'+=-]/g, '');
            $(this).val(newtxt);
        }
    });

    //Form whatnots
    $(".timeline-radio").click(function(){
        if ($(this).val() == 'Defined') {
            $("#datetimepicker").show();
        } else {
            $("#datetimepicker").hide();
        }
    });
    $('#request_type').change(function() {
        $('.other').hide();
        var vall = $(this).find('option:selected').attr('value');
        if (vall == "Other") {
            $('.other').show();
        }
    });
    $('#webinar_date').change(function() {
        var val_date = $(this).attr('value');

        $.ajax({
            "url"  : "/includes/_date-span-check.php",
            "dataType" : "text",
            "data" : "date="+val_date,
            "success" : function(data) {
                if (data == 'failure') {
                    var error_msg = 'Failure';
                    $('#webinar_date_span').html(error_msg);
                } else {
                    $('#webinar_date_span').html(data);
                }
            }
        });
    });

    //Tablesorter
    var oTable = $("table.tablesorter").dataTable({
        "aLengthMenu": [
            [15, 25, 50, 100, 200, -1],
            [15, 25, 50, 100, 200, "All"]
        ],
        'iDisplayLength': 15,
        "aaSorting": [
            [ 0, "desc" ]
        ],
        "oLanguage": {
            "sLengthMenu": "_MENU_"
        }
        //"sPaginationType": "full_numbers"
    });
    $("thead input").keyup( function () {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter( this.value, $("thead input").index(this) );
    } );

    $("a.tab-filter").click(function() {
        //var char = $(this).text().toLowerCase();
        var char = $(this).text();
        $("a.tab-filter").parent().removeClass('active');
        $(this).parent().addClass('active');
 
        if (char === 'All') { // if they click 'all', show everything
             oTable.fnFilter('');
        } else {
             oTable.fnFilter('' + char + '');
        }
    });
     
     
     
    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    $("thead input").each( function (i) {
        asInitVals[i] = this.value;
    } );
     
    $("thead input").focus( function () {
        if ( this.className == "search_init" )
        {
            this.className = "";
            this.value = "";
        }
    } );
     
    $("thead input").blur( function (i) {
        if ( this.value == "" )
        {
            this.className = "search_init";
            this.value = asInitVals[$("thead input").index(this)];
        }
    } );
    /*
    $("table.tablesorter").dataTable({
        "aLengthMenu": [
            [15, 25, 50, 100, 200, -1],
            [15, 25, 50, 100, 200, "All"]
        ],
        'iDisplayLength': 15,
        "aaSorting": [
            [ 0, "desc" ]
        ],
        "oLanguage": {
            "sLengthMenu": "_MENU_"
        },
        "bStateSave": true
        //"sPaginationType": "full_numbers"
    });
    */
    $("table.campaign-table").dataTable({
        "aLengthMenu": [
            [15, 25, 50, 100, 200, -1],
            [15, 25, 50, 100, 200, "All"]
        ],
        'iDisplayLength': 15,
        "aaSorting": [
            [ 4, "desc" ]
        ],
        "oLanguage": {
            "sLengthMenu": "_MENU_"
        },
        "bStateSave": true
        //"sPaginationType": "full_numbers"
    });

    //Tabs
    $('#tabs-solutions a, a.tab-link').click(function (e) {
        e.preventDefault();
        $(this).tab('show');

        //Video stop
        var video_one = $("#video-one-source").attr("src");
        $("#video-one-source").attr("src","");
        $("#video-one-source").attr("src",video_one);

        var video_two = $("#video-two-source").attr("src");
        $("#video-two-source").attr("src","");
        $("#video-two-source").attr("src",video_two);
    });
    $('#tabs-contacts a, #tabs-leads a, #tabs-webinars a, #tabs-enurture a, #tabs-newsletter a, #tabs-email a, #tabs-landingpage a, #tabs-events a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    //Parent tab links
    //TABS ref
    $('h1.tabbed-header, h2.tabbed-header').each(function(){
        var $active, $content, $links = $(this).find('a');

        $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active.attr('href'));

        $links.not($active).each(function () {
            $($(this).attr('href')).hide();
        });

        $(this).on('click', 'a', function(e){
            $active.removeClass('active');
            $content.hide();

            $active = $(this);
            $content = $($(this).attr('href'));

            $active.addClass('active');
            $content.show();

            e.preventDefault();
        });
    });

    //Popovers
    $('.popover-link').popover({
        'html': true,
        'trigger': 'click',
        'placement': 'top'
    });
    $('.popover-calendar').popover({
        'html': false,
        'trigger': 'hover',
        'placement': 'top',
        'delay': { show: 500, hide: 100 }
    });
    $('.popover-hover').popover({
        'html': true,
        'trigger': 'hover',
        'placement': 'top',
        'container': 'body'
    });
    $('.popover-link, .popover-calendar, .popover-hover').on('click', function (e) {
        $('.popover-link').not(this).popover('hide');

    });

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });


    //Random form handlers
    $('.checkall').on('click', function (e) {
        $('.check-geo').prop('checked', this.checked);
    });

    $('input:radio[name=series_based]').click(function () {
        var checkval = $('input:radio[name=series_based]:checked').val();   
        if (checkval == 'Yes') {
            $('#series_based_name').removeAttr("disabled","disabled");
            $('#series_based_name').addClass("required");
        } else {
            $('#series_based_name').attr("disabled","disabled");
            $('#series_based_name').removeClass("required");
        }
     });

    $('input:radio[name=extending_campaign]').click(function () {
        var checkval = $('input:radio[name=extending_campaign]:checked').val();   
        if (checkval == 'Yes') {
            $('#extending_campaign_aprimo').removeAttr("disabled","disabled");
            $('#extending_campaign_aprimo').addClass("required");
        } else {
            $('#extending_campaign_aprimo').attr("disabled","disabled");
            $('#extending_campaign_aprimo').removeClass("required");
        }
     });

    $('input:radio[name=email_rules]').click(function () {
        var checkval = $('input:radio[name=email_rules]:checked').val();   
        if (checkval == 'Yes') {
            $('#email_rules_aprimo').removeAttr("disabled","disabled");
            $('#email_rules_aprimo').addClass("required");
        } else {
            $('#email_rules_aprimo').attr("disabled","disabled");
            $('#email_rules_aprimo').removeClass("required");
        }
     });

    $('input.check-topics').click(function () {
        if ($('input.other-topic').is(':checked')) {
            $('#topics_covered_other').removeAttr("disabled","disabled");
            $('#topics_covered_other').addClass("required");
        } else {
            $('#topics_covered_other').val("");
            $('#topics_covered_other').attr("disabled","disabled");
            $('#topics_covered_other').removeClass("required");
        }
     });

  
    $('input:radio[name=generate_leads_q]').click(function () {
        var checkval = $('input:radio[name=generate_leads_q]:checked').val();   
        if (checkval == '1') {
            $('#aprimo-marketing-id').show();
            $('input:radio[name=existing_mid]')[0].checked = true;
            $('input:radio[name=itp_audience]')[1].checked = true;
            $('#existing-no').hide();
            $('#existing-yes').show();
            $('#geography-select').hide();
            if ($('input:radio[name=request_type]:checked').val() == 1) {
                $('#eloqua-no').hide();
                $('#eloqua-yes').show();
                $('#itp-question').hide();
            }
        } else {
            $('#aprimo-marketing-id').hide();
            $('input:radio[name=existing_mid]')[0].checked = true;
            $('input:radio[name=itp_audience]')[1].checked = true;
            $('#existing-no').hide();
            $('#existing-yes').show();
            if ($('input:radio[name=request_type]:checked').val() == 1) {
                $('#eloqua-yes').hide();
                $('#eloqua-no').show();
                $('#itp-question').show();
            }
        }
     });
    $('input:radio[name=itp_audience]').click(function () {
        var checkval = $('input:radio[name=itp_audience]:checked').val();   
        if (checkval == '1') {
            $('#geography-select').show();
            $('span#preselect-campaign').html('CHANNEL.ITP.APJ.RCO Training.366');
        } else {
            $('#geography-select').hide();
            $('span#preselect-campaign').html('WW.Non-Eloqua Webinar.76');
        }
     });
    $('select#target_geography').on('change', function() {
        text = 'WW.Non-Eloqua Webinar.76';

        switch (this.value) {
            case 'APJ':
                text = 'CHANNEL.ITP.APJ.RCO Training.366';
                break;
            case 'ASMO':
                text = 'CHANNEL.ITP.LAR NAR.RCO Training.369';
                break;
            case 'EMEA':
                text = 'CHANNEL.ITP.EMEA.RCO Training.367';
                break;
            case 'LAR':
                text = 'CHANNEL.ITP.LAR.RCO Training.368';
                break;
            case 'PRC':
                text = 'CHANNEL.ITP.PRC.RCO Training.370';
                break;
        }
        $('span#preselect-campaign').html(text);
    });
    $('input:radio[name=existing_mid]').click(function () {
        var checkval = $('input:radio[name=existing_mid]:checked').val();   
        if (checkval == '1') {
            $('#existing-no').hide();
            $('#existing-yes').show();
        } else {
            $('#existing-yes').hide();
            $('#existing-no').show();
        }
     });
    $('input:radio[name=eloqua_integrate]').click(function () {
        var checkval = $('input:radio[name=eloqua_integrate]:checked').val();   
        if (checkval == '1') {
            $('#eloqua-no').hide();
            $('#eloqua-yes').show();
            $('#existing-question').show();
            $('#aprimo-marketing-id').show();
            //$('#marketing_id').addClass('required');
        } else {
            $('#eloqua-yes').hide();
            $('#eloqua-no').show();
            $('#existing-question').hide();
            $('#aprimo-marketing-id').hide();
            //$('#marketing_id').removeClass('required');
        }
     });
    $('input:radio[name=cloned]').click(function () {
        var checkval = $('input:radio[name=cloned]:checked').val();   
        if (checkval == '1') {
            //$('#campaign-search').hide();
            //$('#aprimo-marketing-id').hide();
            $('#request-search').show();
        } else {
            $('#request-search').hide();
            //$('#campaign-search').show();
            //$('#aprimo-marketing-id').show();
            $('#request_search').typeahead('destroy');
            $('#request_search').val('');
        }
     });
    $('input:radio[name=cloned_campaign]').click(function () {
        var checkval = $('input:radio[name=cloned_campaign]:checked').val();   
        if (checkval == '1') {
            $('#request-search-campaign').show();
        } else {
            $('#request-search-campaign').hide();
            $('#request_search_campaign').typeahead('destroy');
            $('#request_search_campaign').val('');
        }
     });
    $('input:radio[name=create_contactlist]').click(function () {
        var checkval = $('input:radio[name=create_contactlist]:checked').val();   
        if (checkval == '1') {
            $('#contactlist-fields').show();
            $('#list_tag').addClass('required');
            $('#contact_list_request_type').addClass('required');
        } else {
            $('#contactlist-fields').hide();
            $('#list_tag').removeClass('required');
            $('#contact_list_request_type').removeClass('required');
        }
     });

    $('input:radio[name=email-featured-image]').click(function () {
        var checkval = $('input:radio[name=email-featured-image]:checked').val();   
        if (checkval != 'custom') {
            $('#shown-emailfeaturedimage').empty();
            $('input[name=files_emailfeaturedimage]').remove();
        }
     });
    $('input:radio[name=registration-page-image]').click(function () {
        var checkval = $('input:radio[name=registration-page-image]:checked').val();   
        if (checkval != 'custom') {
            $('#shown-registrationpageimage').empty();
            $('input[name=files_registrationpageimage]').remove();
        }
     });
    $('input:radio[name=confirmation-page-image]').click(function () {
        var checkval = $('input:radio[name=confirmation-page-image]:checked').val();   
        if (checkval != 'custom') {
            $('#shown-confirmationpageimage').empty();
            $('input[name=files_confirmationpageimage]').remove();
        }
     });
    $('input:radio[name=listed_on_intel]').click(function () {
        var checkval = $('input:radio[name=listed_on_intel]:checked').val();   
        if (checkval == '1') {
            $('#available-on-intel-end-date').show();
            $("#available_on_intel_end_date").addClass("required");
        } else {
            $('#available-on-intel-end-date').hide();
            $('#available_on_intel_end_date').val('');
            $("#available_on_intel_end_date").removeClass("required");
        }
     });
    $('input:radio[name=eloqua_templates]').click(function () {
        var checkval = $('input:radio[name=eloqua_templates]:checked').val();   
        if (checkval == '1') {
            $('#eloqua-template-text').show();
        } else {
            $('#eloqua-template-text').hide();
        }
     });


    //Campaign Step 0
    var type_data = [ // The data
        ['webinar', [
            'None'
        ]],
        ['event', [
            'Industry', 'Roadshow', 'Partner'
        ]],
        ['enurture', [
            'Standard', 'Newsletter'
        ]],
        ['adhoc', [
            'Email', 'Landing Page'
        ]]
    ];

    /*
    $('div.cr-description').click(function () {
        $('input:radio[name=request_type]').prop('checked', true);
    });
    */

    $('input:radio[name=request_type]').click(function () {
        $('.tabbed-contents').hide();
        $('#additional-details').hide();
        $('a#submit-request-form').addClass('submit-disabled');
        $('div.filedgroup-header-class').hide();
        $('div.request-type-group').hide();
        $(this).closest('div.request-type-group').show();
        $('#show_all_types').show();
        $('div#select-request-content, div#add-contacts-content, div#add-tactics-content').addClass('no-padding');

        var checkval_typeof = $('input:radio[name=request_type]:checked').val();   
        if (checkval_typeof != 8 && checkval_typeof != 999999) { //999999 = temp lead form
            if (checkval_typeof == 4) { //Contacts
                $('div#add-contacts').show();
                $('div#add-contacts-content').removeClass('no-padding');
                $('#cloned-container-campaign').hide();
                $('#campaign-search').show();
                $('#existing-question').hide();
                $('#leads-question').hide();
                $('#aprimo-marketing-id').hide();
                //$('#marketing_id').removeClass('required');
            } else if (checkval_typeof == 9) { //Leads
                $('div#add-contacts').show();
                $('div#add-contacts-content').removeClass('no-padding');
                $('#cloned-container-campaign').hide();
                $('#campaign-search').show();
                $('#existing-question').show();
                $('#leads-question').hide();
                $('#aprimo-marketing-id').show();
                //$('#marketing_id').addClass('required');
            } else {
                $('div#add-tactics').show();
                $('div#add-tactics-content').removeClass('no-padding');
                $('#cloned-container').show();
                $('#cloned-container-campaign').hide();
                $('#campaign-search').show();
                //$('#existing-question').hide();
                //$('#aprimo-marketing-id').show();
                //$('#marketing_id').removeClass('required');
                $('#existing-question').show();
                $('#leads-question').show();
                $('#aprimo-marketing-id').show();
                //$('#marketing_id').addClass('required');
            }
            if (checkval_typeof == 1) { //Show language for Webinars
                $('#language-select').show();
                //$('#eloqua-integrate').show();
                $('#leads-question-webinar').show();
            } else {
                $('#language-select').hide();
                //$('#eloqua-integrate').hide();
                $('#leads-question-webinar').hide();
            }
            $('#additional-details').show();
            $('a#submit-request-form').removeClass('submit-disabled');
        } else {
            $('#additional-details').hide();
            $('#cloned-container-campaign').show();
            $('div#select-request').show();
            $('div#select-request-content').removeClass('no-padding');
            $('a#submit-request-form').removeClass('submit-disabled');
        }

        if (checkval_typeof == 1) { $('#tabbed-webinars').show(); }
        if (checkval_typeof == 2) { $('#tabbed-enurture').show(); }
        if (checkval_typeof == 3) { $('#tabbed-event').show(); }
        if (checkval_typeof == 4) { $('#tabbed-contacts').show(); }
        if (checkval_typeof == 5) { $('#tabbed-newsletter').show(); }
        if (checkval_typeof == 6) { $('#tabbed-email').show(); }
        if (checkval_typeof == 7) { $('#tabbed-landingpage').show(); }
        if (checkval_typeof == 9) { $('#tabbed-leads').show(); }
     });

    $('a#show_all_types_link').click(function (e) {
        e.preventDefault();
        $('input:radio[name=request_type]:checked').prop('checked', false); 
        $('div.filedgroup-header-class').show();
        $('div.request-type-group').show();
        $('div#select-request-content, div#add-contacts-content, div#add-tactics-content').removeClass('no-padding');
        $('input:radio[name=cloned]')[0].checked = true;
        //$('input:radio[name=eloqua_integrate]')[0].checked = true;
        $('input:radio[name=generate_leads_q]')[0].checked = true;
        $('input:radio[name=existing_mid]')[0].checked = true;
        $('input:radio[name=itp_audience]')[0].checked = true;
        $('#show_all_types').hide();
        $('#additional-details').hide();
        $('div.tabbed-contents').hide();
        $('#eloqua-no').hide();
        $('#eloqua-yes').show();
        $('#request-search').hide();
        $('#leads-question').hide();
        $('#leads-question-webinar').hide();
        $('#geography-select').hide();
        $('#aprimo-marketing-id').hide();
        $('#request_search').typeahead('destroy');
        $('#request_search').val('');
        $('#cloned-container-campaign').hide();
        $('#request_search_campaign').val('');
        $('#campaign_search').val('');
    });

    $('#marketing_team').on('change', function() {
        //$('#marketing_team_other').hide();

        if (this.value == 'Other') {
            $('#marketing_team_other').removeAttr("disabled","disabled");
            $('#marketing_team_other').show();
            $('#marketing_team_other').addClass("required");
        } else {
            $('#marketing_team_other').attr("disabled","disabled");
            $('#marketing_team_other').removeClass("required");
            $('#marketing_team_other').hide();
        }
    });

    $('#cadence').on('change', function() {
        if (this.value == 'Other') {
            $('#cadence_other').removeAttr("disabled","disabled");
            $('#cadence_other').show();
            $('#cadence_other').addClass("required");
        } else {
            $('#cadence_other').attr("disabled","disabled");
            $('#cadence_other').val("");
            $('#cadence_other').removeClass("required");
            $('#cadence_other').hide();
        }
    });

    $('#type_of_webinar').on('change', function() {
        if (this.value == 'single') {
            $('#multi-request-text').hide();
            $('#campaign-brief-fields').show();
        } else if (this.value == 'multi') {
            $('#campaign-brief-fields').hide();
            $('#multi-request-text').show();
        } else {
            $('#campaign-brief-fields').hide();
            $('#multi-request-text').hide();
        }
    });

    $(".filterable-list input").click(function(){
        $("#existing_contact_list").text('');
         $(".filterable-list :checked").each(function(){
              $("#existing_contact_list").append( $(this).val() + "\n");
         });
    });

    //Video player control
    $("a.yt-link").click(function(e) {
        e.preventDefault();
        var data_target = $(this).attr('data-target');
 
        $('#'+data_target+' iframe').attr('src', $(this).attr('href'));
        $('#'+data_target+' h3').text($(this).text());
    });


    //Filters
    $('a#filter-btn').click(function(event){
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false; //IE8

        if ($('#filters').is(':visible')) {
            $('#filters').slideUp();
            $(this).removeClass('minus');
            $(this).addClass('add');
        } else {
            $('#filters').slideDown();
            $(this).removeClass('add');
            $(this).addClass('minus');
        }
        
    });


    //Sector+Subsector drop downs
    var data = [ // The data
        ['Embedded', [
            'None','Other'
        ]],
        ['ITDM', [
            'ITDM-ESS', 'ITDM-WA', 'None', 'Other'
        ]],
        ['Channel', [
            'Intel Technology Provider (ITP)', 'Retail Resellers', 'Retail Ecosystem', 'Other'
        ]],
        ['Educator', [
            'Intel Education', 'Other'
        ]]
    ];

    $a = $('#sector'); // The dropdowns
    $b = $('#subsector');

    for(var i = 0; i < data.length; i++) {
        var first = data[i][0];
        $a.append($("<option>"). // Add options
           attr("value",first).
           data("sel", i).
           text(first));
    }
    if($a.attr("val") === "sector-rules"){
        // this code is just for the sector drop down in notification rules section
        $a.append($("<option>"). // Add options
               attr("value","Any").
               text("Any"));
    }

    $a.on('change', function() {
        var index = $(this).children('option:selected').data('sel');
        if (index != undefined) {
            var second = data[index][1]; // The second-choice data
        } else {
            var second = '';
        }

        $b.html(''); // Clear existing options in second dropdown

        for(var j = 0; j < second.length; j++) {
            if($b.attr("val") === "subsector-rules" && second[j] === "Other"){
                // this validation is to hide the 'other' option in notification rules section
            }else{
                $b.append($("<option>"). // Add options
                   attr("value",second[j]).
                   data("sel", j).
                   text(second[j]));
            }
        }

        if($b.attr("val") === "subsector-rules" && $a.val() !== ""){
            // this code is just for the subsector drop down in notification rules section
            $b.append($("<option>"). // Add options
                   attr("value","Any").
                   text("Any"));
        }

        //Open up Other if Embedded is chosen
        /*if (this.value == 'Embedded') {
            $('#subsector_other').removeAttr("disabled","disabled");
            $('#subsector_other').show();
        } else {
            $('#subsector_other').attr("disabled","disabled");
            $('#subsector_other').hide();
        }*/

        $b.on('change', function() {
            if (this.value == 'Other') {
                $('#subsector_other').removeAttr("disabled","disabled");
                $('#subsector_other').show();
            } else {
                $('#subsector_other').attr("disabled","disabled");
                $('#subsector_other').hide();
            }
        });

    }).change(); // Trigger once to add options at load of first choice

	
	/*=======
    Scripts by cygnismedia
    ======*/
    // calander menu accordion
    $('.accord-flip').click(function(){
        $(this).siblings().slideToggle("slow");
        $(this).children().toggleClass("open");
    }); 

    // Radio selection
    $(".select-media").click(function(){
        var id = $(this).attr('data-id');
        var childOf = $(this).attr('child-of');
        $(".select-media").removeClass('active');   
        $(".select-media[data-id='"+id+"']").addClass('active');
        $(".selected-media").fadeOut('fast');
        $(".selected-media-tab"+id).delay(300).fadeIn('slow');
    });
    
    // tool tip intiated
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    // review page filter toggle 
    $('.filter-btn').click(function(){
      $(this).children('.custom-caret').toggleClass('close');
      $('.filter-accordion').slideToggle();
    });

    
    /*=======
    Scripts by cygnismedia
    ======*/
	
    //Don't put anything after this line

});