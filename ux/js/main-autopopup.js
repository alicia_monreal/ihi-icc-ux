function openModal() {
    setTimeout( function() { $('#download-link').trigger('click'); },15000);
    $.cookie('visited', 'yes', { expires: 30 });
}

$(document).ready(function() {
    //Cookie
    var visited = $.cookie('visited');
    if (visited == 'yes') {
        //return false;
    } else if (!$(".various").data().fancybox) {
        openModal();
    }

    //Analytics
    $('a').bind('click',function(){
        if ($(this).attr('data-analytics-label')) {
            mixpanel.track($(this).attr('data-analytics-label'));
        } else {
            mixpanel.track('Link: '+$(this).text());
        }
    });

    //Fancybox
    $(".various").fancybox({
        maxWidth    : 1024,
        maxHeight   : 610,
        fitToView   : false,
        width       : '90%',
        height      : '70%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'elastic',
        closeEffect : 'none',
        padding     : 0,
        margin      : 0
    });

    //Slideshow
    $('#slideshow').after('<div id="nav" class="slideshow-nav">').cycle({
        speed:  400,
        timeout: 8500,
        cleartype: true,
        cleartypeNoBg: true,
        filter:'',
        pager:  '#nav'
    });

    if ($('#fancybox-overlay:empty').length > 0) {
         //console.log('fancy box loaded');
    } else {
         //console.log('fancy box NOT loaded');
    }


});


if (!$(".various").data().fancybox) {
    //console.log('fancy box NOT loaded, dude');
}

//console.log($('#fancybox-overlay:empty').length);