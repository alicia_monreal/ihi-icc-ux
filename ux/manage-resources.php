<?php
session_start();
//Redirect to Home if not admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:home');
    die();
}

$loc = 'manage-resources';
include('includes/head.php');
include('includes/header.php');
?>

    <div class="container">
        <div class="row">
            <div class="col-med-12">
                <div class='request-header'>
                    <?php if (!isset($_GET['id']) && !isset($_GET['action'])) { echo '<div class="header-btn"><a href="manage-resources?action=create" class="btn-green">Create a New Resource &nbsp;&nbsp;<i class="fa fa-plus"></i></a></div>'; } ?>
                	<?php if (isset($_GET['action']) && $_GET['action'] == 'create') { echo '<h2>Create a New Resource</h2><h4>Use the form below to create a new resource and add it to different locations on the site.</h4>'; } else { echo '<h2>Manage Resources</h2><h4>Create and manage resources.</h4>'; } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-med-12">
                <?php include('includes/resource-list.php'); ?>
            </div>
        </div>
    </div>
    <script>
    function deleteResource(url) {
        if (window.confirm("Are you sure you want to delete this resource?")) { 
            window.location.href = url;
        }
    }
    </script>

<div class="push"></div>
<?php include('includes/footer.php'); ?>