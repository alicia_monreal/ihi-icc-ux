<?php
$loc = 'home';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

<!-- Hero -->

<div class="container">
    <div class='row' style='margin-top:30px'>
        <div class='col-md-8'>
            <div id="slideshow"  class="cycle-slideshow slideshow" data-cycle-manual-speed="200" data-cycle-fx="fade" data-cycle-speed='1500' data-cycle-timeout="4000" data-cycle-pager='.cycle-pager' data-cycle-slides="> .slideshow-container" style='height:280px'>
         
              <div class='slideshow-container' style='padding-left:2%'>
                <div class=' col-sm-6 col-md-7'>
                  <div style="display:table-cell;vertical-align:middle;height:280px">
                  <h1>New templates aviable to use in your campaigns</h1>
                    <a  class="btn-green" data-toggle="modal" data-target='#request'>Download Guidelines &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
                  </div>
                </div>
                <div class='col-sm-7 col-md-5' style="text-align:center">
                 <div style="display:table-cell;vertical-align:middle;height:280px">
                  <img style='height:200px;position:relative;right:50px' src="img/hero-guidelines.png" alt="Welcome to the Intel Online Sales Center">
                </div>
              </div>
            </div>

      
         <div class='slideshow-container' style='padding-left:2%'>
        <div class=' col-sm-6 col-md-7'>
        <div style="display:table-cell;vertical-align:middle;height:280px">
            <h1>Test</h1>
        </div>
        </div>
        <div class='col-sm-7 col-md-5' style="text-align:center">
           <div style="display:table-cell;vertical-align:middle;height:280px">
            <img style='height:200px;position:relative;right:50px' src="img/hero-guidelines.png" alt="Welcome to the Intel Online Sales Center">
            </div>
        </div>
        </div>
        
    </div>
    <div id='nav' class="nav2 cycle-pager pager-lower"></div>
            </div>
            <div class='col-md-4'>
                <div class='menu-list'>
                    <ul>

                      <li val="list-sm">
                          <div class='item-container'>
                            <div class="icon submenu list-sm" style="margin-top:14px;"></div><h4 class='item-text'>Upload a List</h4>
                          </div>
                      </li>
                      <a class='lnk' href='email'>
                        <li val="mail-sm">
                          <div class='item-container'>
                            <div class="icon submenu mail-sm"></div><h4 class='item-text'>Create a Single Email</h4>
                          </div>
                        </li>
                      </a>
                      <a class='lnk' href='webinar'>
                          <li val="webinar-sm">
                              <div class='item-container' >
                                  <div class="icon submenu webinar-sm" style="margin-top:10px;"></div><h4 class='item-text'>Create a Webinar</h4>
                              </div>
                          </li>
                      </a>
                      <a class='lnk' href='newsletter'>
                        <li val="newsletter-sm">
                            <div class='item-container'>
                                <div class="icon submenu newsletter-sm"></div><h4 class='item-text'>Create a Newsletter</h4>
                            </div>
                        </li>
                      </a>
                      <a class='lnk' href='landing-page'>
                        <li val="page-sm">
                          <div class='item-container'>
                            <div class="icon submenu page-sm"></div><h4 class='item-text'>Create a Landing Page/Form</h4>
                          </div>
                        </li>
                      </a>
                      <a class='lnk' href='enurture'>
                        <li val="nurture-sm">
                            <div class='item-container'>
                              <div class="icon submenu nurture-sm"></div><h4 class='item-text'>Build a Nurture Series</h4>
                            </div>
                        </li>
                      </a>
                     
                    <!-- <li val="event">
                      <div class='item-container'>
                         <h4 class='item-text'><div class="icon icon-event"></div>Create an Event</h4>
                      </div>
                    </li> -->
                        
                       <!-- <li>
                        <div class='item-container'>
                            <h4 class='item-text'>Create a Campaign</h4>
                        </div>
                        </li>-->

             </ul>
         </div>
     </div>
 </div>

 <div class='row' style='margin-top:30px;margin-bottom:81px'>
    <!-- <div class='col-md-6'> ---------------hidden until logic is added
       <div class='table-empty'>
       <div class='table-title'>
          <h1>Upcoming Milestones</h1>
      </div>
      <div class='table-content'>
      <h4>You don't have any upcoming milestones. let's create some!</h4>
       <input type="submit"  class="btn-green" data-toggle="modal" data-target='#request' value="Get Started">
      </div>
  </div>
</div>
<div class='col-md-6'>
       <div class='table-empty'>
       <div class='table-title'>
          <h1>My Request</h1>
      </div>
      <div class='table-content'>
            <h4>You haven't created any requests. Create your first one now!</h4>
       <input type="submit"  class="btn-green" data-toggle="modal" data-target='#request' value="Get Started">
      </div>
  </div>
</div> -->

<div class='col-md-6'>
    <div class='custom-table'>
     <table width="100%;" style=' border:#acacac 1px solid;border-top:none'>
      <caption class='table-title' style="padding:0"><h1>Upcoming Milestones<span class='top-lnk pull-right'><a href='review'>View All &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a></span></h1></caption>
         <thead>
             <tr>
                 <th></th>
                 <th></th>
                 <th></th>
                 <th></th>
             </tr>
         </thead>
         <tbody class='table-content'>
            <tr class='odd'>
           <td class='col-md-4 highlight'>Email Campaign 2</td>
           <td class='col-md-4'>Begins</td>
           <td class='col-md-3'>Dec 14, 2014</td>
           <td class='col-md-1'></td>
           </tr> 
           <tr>
           <td class='col-md-4 highlight'>Webinar Series</td>
           <td class='col-md-4'>Registration Deadline</td>
           <td class='col-md-3'>Dec 22, 2014</td>
           <td class='col-md-1'></td>
           </tr>
             <tr class='odd'>
           <td class='col-md-4 highlight'>Email Campaign 2</td>
           <td class='col-md-4'>Begins</td>
           <td class='col-md-3'>Dec 14, 2014</td>
           <td class='col-md-1'></td>
           </tr> 
           <tr>
           <td class='col-md-4 highlight'>Webinar Series</td>
           <td class='col-md-4'>Registration Deadline</td>
           <td class='col-md-3'>Dec 22, 2014</td>
           <td class='col-md-1'></td>
           </tr>
             <tr class='odd'>
           <td class='col-md-4 highlight'>Email Campaign 2</td>
           <td class='col-md-4'>Begins</td>
           <td class='col-md-3'>Dec 14, 2014</td>
           <td class='col-md-1'></td>
           </tr> 
           
           
         </tbody>
     </table>
 </div>
</div>

<div class='col-md-6'>
    <div class='custom-table'>
     <table width="100%" class="list-table-home gray-links">
        <?php
		//My Requests query
		$query = "SELECT
		          req.*,
		          DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
		          DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
		          status.value AS status_value,
		          request_type.request_type AS request_type,
		          request_type.id AS request_type_id,
		          users.fname AS fname,
		          users.lname AS lname,
		          users.email AS email
		      FROM request AS req
		      INNER JOIN status on req.status = status.id
		      INNER JOIN request_type on req.request_type = request_type.id
		      INNER JOIN users on req.user_id = users.id";

		$query.=" WHERE req.active = 1"; // All active requests fetched
		$query.=" ORDER BY req.id DESC";
		//$query.=" LIMIT 5";
		    
		$result = $mysqli->query($query);
		$row_cnt = $result->num_rows;

		$resultant_array = array(); 

		if ($row_cnt > 0) {
			echo '<caption class="table-title" style="padding:0"><h1>My Requests<span class="top-lnk pull-right"><a href="review">View All &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a></span></h1></caption>
			         <thead>
			             <tr>
			                 <th></th>
			                 <th></th>
			                 <th></th>
			             </tr>
			         </thead>
			         <tbody class="table-content">';

            while ($obj = $result->fetch_object()) {
				$extra_owners = array(); // initialize extra_owners array

				if(!empty($obj->additional_owners)) { // check if any additional owners exists
					$extra_owners = unserialize($obj->additional_owners); // if yes then unserialize the array
				}

				// If logged-in user has ownership rights to the request, then save it    
				if($obj->user_id == $_SESSION['user_id'] || in_array($_SESSION['user_id'],$extra_owners) || $_SESSION['admin'] == 1) {
					$resultant_array[] = $obj; 
				}

        		if(!empty($resultant_array)) { // if saved data is not empty
        			$i == 0;
        			$c == true;
        			foreach($resultant_array as $obj) {
        				if ($i == 5) break;
        				echo '<tr'.(($c = !$c)?' class="odd"':'').'>
        						<td class="col-md-6 highlight"><a href="request-detail?id='.$obj->id.'" class="notrack popover-hover" data-toggle="popover" data-content="'.getEloquaSourceId($obj->id, $obj->request_type_id).'">'.getTitle($obj->id, $obj->request_type_id).'</a></td>
           						<td class="col-md-4">';
           						if ($obj->status == 14) { echo 'User In Progress'; } else { echo getStatus($obj->status); }
           						echo '</td>
           						<td class="col-md-2">'.$obj->date_created.'</td>
           					</tr>';
           				++$i;
           			}
           		}
           	}
        } else {
        	echo '<caption class="table-title" style="padding:0"><h1>My Requests</h1></caption>
			         <thead>
			             <tr>
			                 <th></th>
			                 <th></th>
			                 <th></th>
			                 <th></th>
			             </tr>
			         </thead>
			         <tbody class="table-content">
						<tr class="odd">
							<td class="col-md-12 highlight" colspan="3"><h4 style="padding-left:0;margin-bottom:20px;">You haven\'t created any requests. Create your first one now!</h4><a href="request" class="btn-green">Get Started</a></td>
							<td class="col-md-1"></td>
						</tr>
						<tr class="odd"><td class="col-md-11 highlight" colspan="3">&nbsp;</td><td class="col-md-1"></td></tr>';
		}
        ?>
         </tbody>
     </table>
 </div>
</div>


</div>
<div class="push"></div>
<?php include('includes/footer.php'); ?>


<?php
    /*
    <div class="row odd">
    	<div class="container" style="padding-bottom:30px;">
    		<div class="span5 pull-left calendar">
    			<h1>Upcoming Training</h1>
                <p>Learn how to use Intel Online Sales resources to your best advantage.</p>
                <!--<ul>
                    <li><a href="training?eid=5">3/20/14 - Eloqua Onboarding Training</a></li>
                </ul>
                <a href="training" class="btn-green">View All</a>-->
    		</div>
    		<div class="span5 pull-right calendar">
                <h1>Capabilities Roadmap</h1>
                <p>Please view the capabilities roadmap to see what's coming.</p>
                <ul>
                    <li><a href="files/Webinar Phases.pptx" target="_blank">Webinar Roadmap</a></li>
                    <li><a href="release-notes">Release Notes</a></li>
                </ul>
            </div>
    	</div>
    </div>
    */
    ?>

    <?php
    /*
    <div class="row dark-blue">
        <div class="container">
            <div class="span12">
                <div class="span7 pull-left">
                    <h4>Featured Update</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mauris lorem, feugiat et hendrerit a, convallis id urna. Aliquam fringilla urna in neque suscipit ullamcorper eu vitae augue. Quisque dapibus rutrum erat nec euismod. Aliquam eu lacus sit amet erat tempor feugiat. Vivamus eget arcu lorem. Nam eros justo, pretium eu tristique id, sodales ac nisi. Sed aliquet lectus magna, at aliquam ligula. Ut tincidunt tincidunt ultricies.</p>
                    <p><a href="" class="btn-green" data-analytics-label="Featured Update: Learn more">Learn more</a></p>
                </div>
                <div class="pull-right">
                    <a href="" data-analytics-label="Featured Update: Thumbnail"><img src="img/fpo-338.png" alt="Featured Update" class="first"></a>
                </div>
            </div>
        </div>
    </div>
    */
    ?>
