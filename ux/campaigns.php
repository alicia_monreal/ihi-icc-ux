<?php
$loc = 'campaigns';
include('includes/head.php');
include('includes/header.php');

require_once('includes/_globals.php');
?>

   <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class='request-header'>
                    <?php if ($_SESSION['admin'] == 1) { echo '<div class="header-btn"><a href="includes/_export.php?type=campaigns" class="btn-green">Export Data &nbsp;&nbsp;<i class="fa fa-download"></i></a></div>'; } ?>
            	    <h2>Campaigns</h2>
                    <h4>You can view the list of available campaigns here. Use the "Search" box to filter down the list by keywords.</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query = "SELECT
        						request_campaign.*,
        						DATE_FORMAT( request_campaign.start_date,  '%m/%d/%Y' ) AS start_date,
			                	DATE_FORMAT( request_campaign.end_date,  '%m/%d/%Y' ) AS end_date,
                                request.status
            				FROM request_campaign
                            INNER JOIN request on request_campaign.request_id = request.id
            				WHERE request.active = 1
            				ORDER BY request_campaign.id DESC";
                
    			$result = $mysqli->query($query);
    			$row_cnt = $result->num_rows;

    			//print_r($query);

    			if ($row_cnt > 0) {
			        echo '<table class="table table-striped table-bordered table-hover requests-table tablesorter">'; //was campaign-table, changed to tablesorter class for column search. throws off sort order, will need to rewire that
			        echo '<thead><th>ICC ID <b class="icon-white"></b></th><th>Campaign Name <b class="icon-white"></b></th><th>Requestor <b class="icon-white"></b></th><th>GEO(s) <b class="icon-white"></b><th>Sector <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th><!--<th>Tactic <b class="icon-white"></b></th>--><!--<th>Type <b class="icon-white"></b></th>--><th>Start Date <b class="icon-white"></b></th><th>End Date <b class="icon-white"></b></th></thead>';
                    /*echo '<thead><tr>
                                <td rowspan="1" colspan="1"><input type="text" name="search_id" placeholder="ICC ID" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_name" placeholder="Name" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_requestor" placeholder="Requestor" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_geos" placeholder="GEO(s)" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_sector" placeholder="Sector" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_status" placeholder="Status" class="search_init"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_start" placeholder="Start" class="search_init" style="width:100px;"></td>
                                <td rowspan="1" colspan="1"><input type="text" name="search_end" placeholder="End" class="search_init" style="width:100px;"></td>
                            </tr></thead>';*/
			        echo '<tbody>';
			        while ($obj = $result->fetch_object()) {
                        //Get user info
                        $query_user = "SELECT fname, lname, email FROM users WHERE users.id = ".$obj->user_id." LIMIT 1";
                        $result_user = $mysqli->query($query_user);
                        while ($obj_user = $result_user->fetch_object()) {
                            $user = '<a href="mailto:'.$obj_user->email.'">'.$obj_user->fname.' '.$obj_user->lname.'</a>';
                        }

                        if ($obj->start_date == "0000-00-00 00:00:00" || $obj->start_date == "00/00/0000") { $start_date = ''; } else { $start_date = $obj->start_date; }
                        if ($obj->end_date == "0000-00-00 00:00:00" || $obj->end_date == "00/00/0000") { $end_date = ''; } else { $end_date = $obj->end_date; }

			            echo '<tr>';
                        echo '<td>'.$obj->id.'</td>';
			            echo '<td><a href="campaign-detail?id='.$obj->request_id.'" class="notrack popover-hover" data-toggle="popover" data-content="'.getTitle($obj->id, 8).'">'.$obj->tag.'</td>';
                        echo '<td>'.$user.'</td>';
                        echo '<td>'.$obj->geo.'</td>';
                        echo '<td>'.$obj->sector.'</td>';
                        echo '<td>'.getStatus($obj->status).'</td>';
			            echo '<td>'.$start_date.'</td>';
			            echo '<td>'.$end_date;

                        //Hidden mID's for search
                        $query_mid = "SELECT id, request_type FROM request WHERE request_type != 8 AND request_type != 4 AND campaign_id = '".$obj->request_id."' AND active = 1";                
                        $result_mid = $mysqli->query($query_mid);
                        $row_cnt_mid = $result_mid->num_rows;
                        if ($row_cnt_mid > 0) {
                            while ($obj_mid = $result_mid->fetch_object()) {
                                echo '&nbsp;<span style="display:none;">'.getMarketingId($obj_mid->id, $obj_mid->request_type).'</span>';
                            }
                        }

                        echo '</td>';
			            echo '</tr>';
			        }
			        echo '</tbody></table><div class="clearfix"></div>';
			    } else {
			        echo '<p><strong>There are currently no active campaigns.</strong></p>';
			    }

            	?>
            </div>
        </div>
    
    </div>
<div class="push"></div>
<?php include('includes/footer.php'); ?>