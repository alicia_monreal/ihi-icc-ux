<?php
$loc = 'plan';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

    
    <a id="placeholder-loaded" href='#placeholder-setup' data-toggle='modal' data-target='#placeholder-setup' class="lnk">
    </a>


    <div id='handle-placeholder'></div>

    <a href="support" data-analytics-label="Header: Support" style="position:absolute;right:0px;z-index:100">
        <img src="img/support.png" />    
    </a>
    <!-- Plan Heading -->
    <div class='row' stlye='margin-right:0'>
        <div class='col-md-12' style='padding-right:0;text-align:center;top:35px'>
            <h3>Plan</h3>
        </div>
    </div>

    <!-- Main Containter Div -->
    <div class='container-wide'>
        <div class='row' style='margin-top:30px'>
            <div class='col-md-3'>
              <a href="javascript:void(0)" id="placeholder-btn">
                  <h1 class='placeholder_button'>Add Placeholder Event</h1>
                  <img id="placeholder-btn" src="img/placeholder_button.png" />
              </a>
                <div id="filters" style="width:280px">
                    <div class="calendar_menu_tabs">
                        <div class="span3">
                            <h1 class='calendar-menu'>Tactic Type</h1>
                            <img class="calendar_menu_tab" src="img/tab_close.png" />
                            <ul id="tactic_type" class="filter-list" style="display:none">
                                <li class="active all"><a href="" data-type="all">All</a></li>
                                <li><a href="" data-type="webinar" >Webinar</a></li>
                                <li><a href="" data-type="newsletter">Newsletter</a></li>
                                <li><a href="" data-type="enurture">eNurture</a></li>
                                <li><a href="" data-type="email">Single Email</a></li>
                            </ul>
                        </div>
                        <div class="span3">
                            <h1 class='calendar-menu'>Geo</h1>
                            <img class="calendar_menu_tab" src="img/tab_close.png" />
                            <ul id="geo" class="filter-list" style="display:none">
                                <li class="active all"><a href="" data-geo="all">All</a></li>
                                <li><a href="" data-geo="APJ">APJ</a></li>
                                <li><a href="" data-geo="EMEA">EMEA</a></li>
                                <li><a href="" data-geo="LAR">LAR</a></li>
                                <li><a href="" data-geo="NAR">NAR</a></li>
                                <li><a href="" data-geo="PRC">PRC</a></li>
                            </ul>
                        </div>
                        <div class="span3">
                            <h1 class='calendar-menu'>Sector</h1>
                            <img class="calendar_menu_tab" src="img/tab_close.png" />
                            <ul id="sector" class="filter-list" style="display:none">
                                <li class="active all"><a href="" data-sector="all">All</a></li>
                                <li><a href="" data-sector="embedded">Embedded</a></li>
                                <li><a href="" data-sector="itdm">ITDM</a></li>
                                <li><a href="" data-sector="channel">Channel</a></li>
                            </ul>
                        </div>
                        <div class="span3">
                            <h1 class='calendar-menu'>Campaign</h1>
                            <img class="calendar_menu_tab" src="img/tab_close.png" />
                            <select id="campaign_id" name="campaign_id" class="input-wide required" style="width:279px;display:none">
                                <option value="all">All campaigns</option>
                                <?php
                                $campaign_array = array();
                                $query = "SELECT request_campaign.id, request_campaign.name FROM request_campaign INNER JOIN request on request_campaign.request_id = request.id WHERE request.active = 1 AND DATE(end_date) > DATE(NOW()) AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
                                $result = $mysqli->query($query);
                                while ($obj = $result->fetch_object()) {
                                    echo '<option value="'.$obj->id.'">'.$obj->name.'</option>';
                                    $campaign_array[] = $obj->name;
                                }
                                ?>
                            </select>
                        </div>
                        <div class="span3">
                            <h1 class='calendar-menu'>Requests</h1>
                            <img class="calendar_menu_tab" src="img/tab_close.png" />
                            <ul id="owner" class="filter-list" style="display:none">
                                <li class="active all"><a href="" data-owner="all">All requests</a></li>
                                <li><a href="" data-owner="mine">Show only my requests</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Calendar Tiles -->
            <div class='col-md-8' style="margin-left:40px;margin-top:50px">
                <div >
                    <div class="row" style="margin-top:0px;">
                        <div class="intro span2" style="float:left">
                            <div class="cal-legend"><div class="icon webinar-sm" style="margin-right:10px;margin-top:-2px"></div>Webinar<div class="cal-legend-block cal-webinar"></div> </div>
                        </div>
                        <div class="intro span2" style="float:left;margin-left:17px">
                            <div class="cal-legend"><div class="icon newsletter-sm" style="margin-right:10px;margin-top:-2px"></div>Newsletter<div class="cal-legend-block cal-newsletter"></div> </div>
                        </div>
                        <div class="intro span2" style="float:left;margin-left:17px">
                            <div class="cal-legend"><div class="icon nurture-sm" style="margin-right:10px;margin-top:-2px"></div>eNurture<div class="cal-legend-block cal-enurture"></div> </div>
                        </div>
                        <div class="intro span2" style="float:left;margin-left:17px">
                            <div class="cal-legend"><div class="icon mail-sm" style="margin-right:10px;margin-top:-2px"></div>Email<div class="cal-legend-block cal-email"></div> </div>
                        </div>
                        <div class="intro span2" style="float:left;margin-left:17px">
                            <div class="cal-legend"><div class="icon event-sm" style="margin-right:10px;margin-top:-2px"></div>Event<div class="cal-legend-block cal-event"></div> </div>
                        </div>
                    </div>
                </div>

                <!--Calendar Loaded Over Here -->
                <div>
                    <div class="row intro-body" style="margin-top:0px;margin-bottom:20px;">
                        <div class="intro">
                            <div id='loading' style="display:none;">loading...</div>

                            <h1></h1>
                            <div id="calendar" style="margin-top:20px;"></div>
                        </div>
                    </div>
                </div>

                <p>&nbsp;</p>
                <p>&nbsp;</p>
                
                <!-- Modal - Not working probably because of bootstrap js error -->
                <div class="modal hide fade" id="modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 style="font-size:26px;">Event Detail</h3>
                    </div>
                    <div id="calendar-detail-body" class="modal-body" style="height: 400px">
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn">Close</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
	
    <?php  
        /* limited campaigns were shown by default in calendar page, thatswhy a different array to contain all
           the active campaigns is created 
        */
        $campaign_array_popup = array();
        $query = "SELECT request_campaign.id, request_campaign.name 
                  FROM request_campaign 
                  INNER JOIN 
                  request ON request_campaign.request_id = request.id 
                  WHERE request.active = 1 AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
        
        $result = $mysqli->query($query);
        while ($obj = $result->fetch_object()) {
            $campaign_array_popup[] = $obj->name; // all campaigns loaded into campaign_array_popup from calendar page
        }
    ?>

<script>

$(".calendar_menu_tab").click(function(){
    if($(this).next().is(":visible")) {
        $(this).next().hide();
        $(this).attr("src","img/tab_close.png");
    } else {
        $(this).next().show();
        $(this).attr("src","img/tab_open.png");
    }
    

});
</script>

<?php include('includes/footer.php'); ?>