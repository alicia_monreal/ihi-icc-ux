<?php  
    if($_SESSION['available_on_intel_end_date']===null){                                
      $today = date("m/d/Y");
      $aYearLater = strtotime ( '+1 year' , strtotime ( $today ) ) ;
      $_SESSION['a_later_year_on_intel_end_date'] = date ( 'm/d/Y' , $aYearLater );
    }
?>
<div id='webinar-publishing' class="modal fade">
  <div class="modal-dialog" style="width:75%;max-width:1000px">
    <div class="modal-content" style="padding-left:5%;padding-right:5%">
        <div class="modal-header modal-tabs" style="border-bottom:none;">
          <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
          <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Publishing</h2>
        </div>

        <div class="modal-body" style="padding-bottom:90px;padding-top:40px;margin:0 10% 0 0%">
         <div role="tabpanel">
         <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="intel-com">
            <form id="webinar-publishing-form" class="form-horizontal">
              <input type="hidden" id="action" name="action" value="create">
              <input type="hidden" id="step" name="step" value="6">
              <input type="hidden" id="c_type" name="c_type" value="webinar">
              <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
              <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
              <input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
              <div class="col-md-12">
                <div class='form-group' style="margin-bottom:75px;margin-left:12%">
                  <label class="radio-inline modal-label col-md-2" style="font-family:'IntelClearBold'">Topic(s)</label>
                  <div class='col-md-10 topics_covered'>
                    <p style="margin:8px 0 15px 0; width:310px; font-size:15px;">*Choose topics to associate with the webinar (these are topic filters available at Intel.com):</p>
                    <div class="pull-left" style="margin-left:45px;">
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Big Data" <?php if (in_array('Big Data', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Big Data</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Consumerization of IT" <?php if (in_array('Consumerization of IT', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Consumerization of IT</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Desktop Virtualization" <?php if (in_array('Desktop Virtualization', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Desktop Virtualization</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="High Performance Computing" <?php if (in_array('High Performance Computing', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> High Performance Computing</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Manageability" <?php if (in_array('Manageability', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Manageability</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Security" <?php if (in_array('Security', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Security</label><br />                    
                    </div>
                    <div class="pull-left" style="margin-left: 45px;">
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Cloud Computing" <?php if (in_array('Cloud Computing', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Cloud Computing</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Data Center Efficiency" <?php if (in_array('Data Center Efficiency', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Data Center Efficiency</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Embedded" <?php if (in_array('Embedded', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Embedded</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Internet of Things" <?php if (in_array('Internet of Things', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Internet of Things</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Mobility" <?php if (in_array('Mobility', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Mobility</label><br />
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Virtualization" <?php if (in_array('Virtualization', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required"> Virtualization</label><br />
                    </div>
                    <div style="clear:both;margin-left: 45px;">
                      <label class="checkbox inline required"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Other" <?php if (in_array('Other', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'checked'; } ?> class="check-topics required other-topic"></label> <input type="text" id="topics_covered_other" name="topics_covered_other" class="input-wide clearfix form-control" placeholder="Other: please enter topic(s) here*" value="<?php echo getFieldValue('topics_covered_other', $_SESSION['clone_id'], $_SESSION['request_type'], ''); ?>" style="clear:both;" <?php if (!in_array('Other', getFieldValue('topics_covered', $_SESSION['clone_id'], $_SESSION['request_type'], 'array'))) { echo 'disabled'; } ?>><br />
                    </div>                  
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group" style="margin-top:35px;">
                  <label for="listed_on_intel" class="control-label modal-label col-lg-6" style="padding:0; margin-left:-8px;">Do you want this webinar listed on Intel.com?</label>
                  <div class="controls col-lg-6" style="padding-left:45px;">
                    <label class="radio inline modal-label col-lg-6" style="padding: 7px 0;"> <input type="radio" id="listed_on_intel[]" name="listed_on_intel" value="1" <?php if (getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '1') { echo 'checked'; } ?>> Yes</label>
                    <label class="radio inline modal-label"> <input type="radio" id="listed_on_intel[]" name="listed_on_intel" value="0" <?php if (getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo 'checked'; } ?>> No</label><br />
                  </div>
                </div>
              </div> 
              <div class="col-md-12">
                <div class="form-group">
                  <div id="available-on-intel-end-date" class="control-group<?php if (getFieldValue('listed_on_intel', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo ' hide'; } ?>">
                    <label for="available_on_intel_end_date" class="control-label modal-label col-lg-6" style="padding-top:0;">How long do you want the webinar to be available on Intel.com?</label>
                    <div class="controls col-lg-6">
                      <div id="available_on_demand_date_picker" class="input-group input-append date" style="width:235px;"><input type="text" id="available_on_intel_end_date" name="available_on_intel_end_date" class="input-medium required" placeholder="Select End Date*" value="<?php echo $_SESSION['a_later_year_on_intel_end_date']; ?>"><span class="input-group-addon" style="margin-right:20px;"><i class="fa fa-th"></i></span></div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
    <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="publishing-exit-btn" class="btn-blue" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 6" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0);" id="publishing-save-exit" class="btn-green submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 6" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
  </div>

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->