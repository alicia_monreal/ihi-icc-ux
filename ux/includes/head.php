<?php include('includes/validate.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Intel Customer Connect</title>
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="">
	<meta name="keywords" content="">

	<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico"/>
	<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico"/>

	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="css/colorbox/colorbox.css">
	<link rel="stylesheet" href="js/bootstrap-fileupload.min.css">
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="css/datepicker.css">
	<link rel="stylesheet" href="css/calendar.css">
	<link rel="stylesheet" href="css/bootstrap-editable.css">
    <link rel="stylesheet" href="css/simple-line-icons.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">

    <!--jQuery-->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <!-- Bootstrap -->
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap-editable.min.js"></script>

	<!-- Calendar -->
    <?php if ($loc == 'plan') { ?>
    <link href='js/fullcalendar/fullcalendar.css' rel='stylesheet' />
	<link href='js/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
	<script src='js/moment.min.js'></script>
	<script src='js/fullcalendar/fullcalendar.js'></script>
	<script src='js/calendar-icc.js'></script>
    <?php } ?>

    <script src="js/bootbox.min.js"></script>
    <link rel="stylesheet" href="css/chosen.css">
   	<link rel="stylesheet" href="css/styles.css?ver=<?php echo rand(); ?>">
    <link rel="stylesheet" href="css/style-ux.css?ver=<?php echo rand(); ?>">
   	<link rel="stylesheet" href="css/templates.css?ver=<?php echo rand(); ?>">

   	<?php if ($loc == 'templates') { ?>
    <script type="text/javascript" src="js/shCore.js"></script>
    <script type="text/javascript" src="js/shBrushJScript.js"></script>
    <link type="text/css" rel="stylesheet" href="css/shCoreDefault.css"/>

    <link rel="stylesheet" href="email_templates/microStyles.css?ver=<?php echo rand(); ?>">

    <script src="js/editor/jquery.mockjax.js"></script>
    <script src="js/jquery.ui.js"></script>
    <script src="js/editor/moment.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/editor/bootstrap-datetimepicker.js"></script>
    <script src="js/editor/bootstrap-editable.js"></script>
    <script src="js/editor/wysihtml5-0.3.0.min.js"></script>
    <script src="js/editor/bootstrap-wysihtml5-0.0.2.min.js"></script>
    <script src="js/editor/wysihtml5.js"></script>
    <script src="js/editor/address.js"></script>
    <?php } ?>
    <link rel="stylesheet" href="email_templates/microStyles.css?ver=<?php echo rand(); ?>">
    <!-- Editor Script  -->
    <script src="js/editor/editor.js?ver=<?php echo rand(); ?>"></script>
    <script type="text/javascript" src="js/editor/tinymce/tinymce.min.js"></script>
    
    <!-- Customize global script params -->
    <script>
        var tplEditor   = null;
        var tplInjector = null;
        var dbRecord, editRecord  = null;

        $(document).ready(function() {
            tplEditor          = new templateEngine();
            tplEditor.initialize();
            // tplInjector        = new templateInjector();
            // tplInjector.initialize();
        });

        function autoIframeResize(id){
            var newheight;
            var newwidth;
            // var id = 'micro_' + tplInjector.temp_name;
            if(document.getElementById){
                newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
                newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth;
            }

            document.getElementById(id).height= (newheight) + "px";
            // document.getElementById(id).width= (newwidth) + "px";

        }
    </script>
    

   	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="js/html5shiv.js"></script>
	<![endif]-->
</head>
<!--[if lt IE 7]>      <body class="no-js ie7 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <body class="no-js ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <body class="no-js ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <body class="no-js"> <!--<![endif]-->
