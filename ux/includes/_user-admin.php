<?php
session_start();
include('_globals.php');

//Redirect to Home if not admin
if (!isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/');
    die();
}

if (!empty($_GET['action']) && isset($_GET['id'])) {

    //Approve a user
    if ($_GET['action'] == 'approve') {

        $query = "UPDATE users SET suspended = '0', active = '1', date_updated = NOW() WHERE users.id = ".$_GET['id']."";
        $result = $mysqli->query($query);

        $query_user = "SELECT email, password FROM users WHERE users.id = ".$_GET['id']." LIMIT 1";
        $result_user = $mysqli->query($query_user);
        $row_cnt = $result_user->num_rows;

        //Create their own dir (nevermind, upload script does it)
        //mkdir("/public_html/server/php/files/".$_GET['id']."", 0777);

        if ($row_cnt > 0) {
            while ($obj = $result_user->fetch_object()) {
                $email = $obj->email;
                $password = $obj->password;

                //Send email to approved user
                $message_user = $message_header;
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"> Welcome to Intel Customer Connect, your destination to learn about, request, and manage webinar and marketing automation campaigns. Please visit <a href=\"".$domain."\" style=\"color:#1570a6;\">customerconnect.intel.com</a> to submit your campaign requests.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>Username:</strong> <a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a><br /><strong>Password:</strong> ".$password."</p><p><br /><a href=\"".$domain."\"><img src=\"".$domain."/img/btn-get-started.gif\" alt=\"Get started\"></a></p>";
                $message_user.=$message_footer;

                $mail2->AddAddress($email);
                $mail2->Subject = 'Intel Customer Connect Access';
                $mail2->MsgHTML($message_user);
                $mail2->AltBody = 'Your request for site access is approved. '.$domain.'';
                $mail2->Send();
            }
            header('Location:/manage-users?message=approved');
        }
    }

    //Deny a user
    if ($_GET['action'] == 'deny') {

        $query = "UPDATE users SET active = '0', suspended = '1', date_updated = NOW() WHERE users.id = ".$_GET['id']."";
        $result = $mysqli->query($query);

        $query_user = "SELECT email, password FROM users WHERE users.id = ".$_GET['id']." LIMIT 1";
        $result_user = $mysqli->query($query_user);
        $row_cnt = $result_user->num_rows;

        //Create their own dir (nevermind, upload script does it)
        //mkdir("/public_html/server/php/files/".$_GET['id']."", 0777);

        if ($row_cnt > 0) {
            while ($obj = $result_user->fetch_object()) {
                $email = $obj->email;
                $password = $obj->password;

                //Send email to approved user
                $message_user = $message_header;
                $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Your request for access to Intel Customer Connect has been denied.</p>";
                $message_user.=$message_footer;

                $mail2->AddAddress($email);
                $mail2->Subject = 'Intel Customer Connect Access';
                $mail2->MsgHTML($message_user);
                $mail2->AltBody = 'Your request for access to Intel Customer Connect has been denied.';
                $mail2->Send();
            }
            header('Location:/manage-users?message=denied');
        }
    }


    //Update a user
    if ($_GET['action'] == 'update') {

        $fname = $mysqli->real_escape_string($_POST['fname']);
        $lname = $mysqli->real_escape_string($_POST['lname']);
        $email = $mysqli->real_escape_string($_POST['email']);
        $title = $mysqli->real_escape_string($_POST['title']);
        $company = $mysqli->real_escape_string($_POST['company']);
        $point_of_contact = $mysqli->real_escape_string($_POST['point_of_contact']);
        $point_of_contact_email = $mysqli->real_escape_string($_POST['point_of_contact_email']);
        $role = $_POST['role'];
        if ($role == 1 || $role == 6) {
            $admin = 1;
        } else {
            $admin = 0;
        }
        $access_control_array = array();
        foreach($_POST['access_control'] as $val_access_control) {
            array_push($access_control_array, $val_access_control);
        }
        $access_control_db = implode(", ",$access_control_array); //array

        $query = "UPDATE users SET
                        fname = '".$fname."',
                        lname = '".$lname."',
                        email = '".$email."',
                        title = '".$title."',
                        company = '".$company."',
                        point_of_contact = '".$point_of_contact."',
                        point_of_contact_email = '".$point_of_contact_email."',
                        role = '".$role."',
                        admin = '".$admin."',
                        access_control = '".$access_control_db."',
                        date_updated = NOW()
                    WHERE users.id = ".$_GET['id']."
                    LIMIT 1";
        $result = $mysqli->query($query);

        header('Location:/manage-users?message=updated');
    }


    /* close connection */
    $mysqli->close();
} else {
    header('Location:/manage-users'); //No action
}

?>