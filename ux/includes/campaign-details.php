<?php
    
    $campaign_array = array();
    $query = "SELECT request_campaign.id, request_campaign.name FROM request_campaign INNER JOIN request on request_campaign.request_id = request.id WHERE request.active = 1 AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
    //AND DATE(end_date) > DATE(NOW()) 
    $result = $mysqli->query($query);
    while ($obj = $result->fetch_object()) {
        $campaign_array[] = $obj->name;
    }
?>
<!-- Campaign Setup Modal -->
<div id='campaign-setup' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content" style="padding-left:5%;padding-right:5%">
      <div class="modal-header" style="text-align:center">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
        <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Campaign Details</h2>
        <h4 class="request-form-subtitle" style='padding: 0 30px 0 30px;'>
            In order to provide more detail in our reporting, we need you to associate your request with a campaign. 
            Campaigns are one or more marketing activities over a specified timeframe.
        </h4>
    </div>

    <div class="modal-body" style="padding-bottom:60px">
    <form class="form-horizontal" id="content-update-form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="action" name="action" value="create">
                    <input type="hidden" id="step" name="step" value="2">
                    <input type="hidden" id="c_type" name="c_type" value="campaign">
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                    <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">

        <div class='form-group' id='new-campaign-type' style="margin-bottom:30px">
            <label class="radio-inline col-md-3" style="font-family:'IntelClearBold'">Campaign Association</label>
            <div class='col-md-9'>
            <label class="radio-inline" style="font-family:'IntelClearBold';margin-right:40px">
                <input type="radio" name="inlineRadioOptions" id="new-campaign-radio" value="0" checked> New Campaign
            </label>
            <label class="radio-inline" style="font-family:'IntelClearBold'">
                <input type="radio" name="inlineRadioOptions" id="existing-campaign-radio" value="1" checked=""> Existing Campaign
            </label>
        </div>
        </div>

        
        <div id="existing-campaign" style="display:visible">
            <div id="request-search-campaign" class="form-group">
                <label for="campaign_search_popup" class="col-md-3 control-label">Campaign</label>
                <div class="col-md-8">
                    <input type="text" id="request_search_campaign" name="request_search_campaign" class="form-control text-modal required" placeholder="Start typing the GEOs or keywords from the campaign name" value="" >
                </div>
            </div>
            


        </div>
        <div id='campaign-form-fields' style='display:none'>
        <?php include('request-campaign.php'); ?>
        </div>
        </form>
    </div>
    <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
    <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel &nbsp;</a><a href="javascript:void(0);" id="submit-existing-campaign" class="btn-green" style="font-size: 16px !important;" data-analytics-label="Submit Form: Campaign ID Request: Step 1" val="<?php echo $tactic_name ?>">Continue</a><a href="javascript:void(0);" id="submit-new-campaign" class="btn-green" style="font-size: 16px !important; display:none" data-analytics-label="Submit Form: Campaign ID Request: Step 1" val="<?php echo $tactic_name ?>">Continue</a>
    </div>

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- End - Campaign Setup Modal -->



