<div id='custom-template' class="modal fade">
  <div class="modal-dialog icc-modal-dialog">
    <div class="modal-content" style="padding-left:5%;padding-right:5%;">
            <div class="modal-header modal-tabs">
              <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
              <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Custom Template</h2>
            </div>

        <div class="modal-body" style="padding-top:30px;margin: 0 4% 0 4%;">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="details" style="display:block !important; visibility: visible !important;">
                  
                  <form id="custom-asset-form" class="content-update-form form-horizontal">
                    <input type="hidden" id="template" name="template" value="Custom Template">
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                    <input type="hidden" id="step" name="step" value="4">
                    <input type="hidden" id="c_type" name="c_type" value="<?php echo $_GET['type']; ?>">
                    <input type="hidden" id="request_id" name="request_id" value="">
                    <input type="hidden" id="action" name="action" value="savetemplate">
                    <div class="fieldgroup" style="border-bottom:0;">
                      <div class="control-group custom-template-div">
                        <h4>Custom Email & Landing Page Template Assets</h4>
                        <p>Please provide completed html files and any other associated assets to be included in the invitation/reminder/follow-up emails.</p>

                        <div class="upload-div">
                          <div class="fileupload fileupload-new" data-provides="fileupload">
                            <span class="btn-file btn-green" style="width:172px; font-size:16px;"><span class="fileupload-new">Upload Asset Zip &nbsp;&nbsp;<i class="fa fa-caret-right"></i></span><input id="fileupload_customassets" type="file" name="files[]"></span>
                          </div>
                          <div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: 10px;margin-left: 118px;display:none;">
                              <div class="progress-bar bar"></div>
                          </div>
                          <div id="shown-customassets" class="files" style="clear:both;">
                            <?php
                            if (isset($_SESSION['files_customassets'])) {
                              $filebox = '<div name="'. ($_SESSION['files_customassets']). '">';
                              $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_customassets']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_customassets_path'].'">' . $_SESSION['files_customassets'] . '</a></p>';
                                      $filebox.= '</div>';
                                      echo $filebox;
                                }
                                ?>
                          </div>
                        </div>

                      </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
        <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
          <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="custom-template-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Choose Template and Layout Configuration Request: Step 3" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0);" id="custom-template-save" class="btn-green submit" style="font-size: 16px !important;" data-analytics-label="Choose Template and Layout Configuration Request: Step 3" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
        </div>
        <!-- <div class="modal-footer" style="position: absolute;bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
             <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="upload-list-setup-save-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0)" id="upload-list-setup-save" class="btn-green" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
        </div> -->
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- End - Campaign Setup Modal -->