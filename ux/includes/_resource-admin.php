<?php
session_start();
include('_globals.php');

//Redirect to Home if not admin
if (!isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/');
    die();
}

if (!empty($_GET['action'])) {

    //Update a resource
    if ($_GET['action'] == 'update' && isset($_GET['id'])) {

        $title = $mysqli->real_escape_string($_POST['title']);
        $description = $mysqli->real_escape_string($_POST['description']);
        $type = $_POST['type'];
        $access_control_array = array();
        foreach($_POST['access_control'] as $val_access_control) {
            array_push($access_control_array, $val_access_control);
        }
        $access_control_db = implode(", ",$access_control_array); //array

        //Format date
        $thedate = DateTime::createFromFormat('m/d/Y', $_POST['publish_date']);
        $publish_date = $thedate->format('Y-m-d');

        $link = $mysqli->real_escape_string($_POST['link']);
        $internal = $_POST['internal'];
        $locations_array = array();
        foreach($_POST['locations'] as $val_locations) {
            array_push($locations_array, $val_locations);
        }
        $locations_db = implode(", ",$locations_array); //array
        $tags_array = array();
        foreach($_POST['tags'] as $val_tags) {
            array_push($tags_array, $val_tags);
        }
        $tags_db = implode(", ",$tags_array); //array

        $query = "UPDATE resources SET
                        user_id = ".$_SESSION['user_id'].",
                        title = '".$title."',
                        description = '".$description."',
                        link = '".$link."',
                        type = '".$type."',
                        access_control = '".$access_control_db."',
                        locations = '".$locations_db."',
                        tags = '".$tags_db."',
                        internal = '".$internal."',
                        publish_date = '".$publish_date."',
                        date_updated = NOW()
                    WHERE resources.id = ".$_GET['id']."
                    LIMIT 1";
        $result = $mysqli->query($query);

        header('Location:/ux/manage-resources?message=updated');
    }


    //Delete a resource
    if ($_GET['action'] == 'delete' && isset($_GET['id'])) {

        $query = "UPDATE resources SET active = '0' WHERE resources.id = ".$_GET['id']." LIMIT 1";
        $result = $mysqli->query($query);

        header('Location:/ux/manage-resources?message=deleted');
    }


    //Create a new resource
    if ($_GET['action'] == 'create') {

        $title = $mysqli->real_escape_string($_POST['title']);
        $description = $mysqli->real_escape_string($_POST['description']);
        $type = $_POST['type'];
        $access_control_array = array();
        foreach($_POST['access_control'] as $val_access_control) {
            array_push($access_control_array, $val_access_control);
        }
        $access_control_db = implode(", ",$access_control_array); //array

        //Format date
        $thedate = DateTime::createFromFormat('m/d/Y', $_POST['publish_date']);
        $publish_date = $thedate->format('Y-m-d');

        $link = $mysqli->real_escape_string($_POST['link']);
        $internal = $_POST['internal'];
        $locations_array = array();
        foreach($_POST['locations'] as $val_locations) {
            array_push($locations_array, $val_locations);
        }
        $locations_db = implode(", ",$locations_array); //array
        $tags_array = array();
        foreach($_POST['tags'] as $val_tags) {
            array_push($tags_array, $val_tags);
        }
        $tags_db = implode(", ",$tags_array); //array

        $query = "INSERT INTO resources VALUES (
                        NULL,
                        ".$_SESSION['user_id'].",
                        '".$title."',
                        '".$description."',
                        '".$link."',
                        '".$type."',
                        '".$access_control_db."',
                        '".$locations_db."',
                        '".$tags_db."',
                        '',
                        '".$internal."',
                        '".$publish_date."',
                        NOW(),
                        NOW(),
                        '1')";
        $result = $mysqli->query($query);

        header('Location:/ux/manage-resources?message=created');
    }


    /* close connection */
    $mysqli->close();
} else {
    header('Location:/manage-resources'); //No action
}

?>