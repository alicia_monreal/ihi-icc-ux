<?php
session_start();
include('_globals.php');

//Redirect to Home if not admin
if (!isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/');
    die();
}

if (!empty($_GET['action'])) {

    //Update a resource
    if ($_GET['action'] == 'update' && isset($_GET['id'])) {

        
        $recipients = array();
        foreach ($_POST['recipients-list'] as $value) {
            if($value !== "")
                array_push($recipients, $value);   
        }
        foreach($_POST['rules-recipients'] as $val_rec) {
            array_push($recipients, $val_rec);
        }
        $rec_list = implode(", ",$recipients);

        $query = "UPDATE notification_rules SET                        
                        notification_user_ids = '".$rec_list."',
                        default_assignee = '".$_POST['default-assignee']."',
                        last_updated = NOW()
                    WHERE id = ".$_GET['id']."
                    LIMIT 1";

        $result = $mysqli->query($query);

        header('Location:/notification-rules?message=updated');
    }


    //Delete a resource
    if ($_GET['action'] == 'delete' && isset($_GET['id'])) {

        $query = "UPDATE notification_rules SET active = '0' WHERE id = ".$_GET['id']." LIMIT 1";
        $result = $mysqli->query($query);

        header('Location:/notification-rules?message=deleted');
    }


    //Create a new resource
    if ($_GET['action'] == 'create') {

        $default_rule = 0;
        if($_GET['default']){
            $default_rule = 1;
        }

        $geo_array = array();
        foreach($_POST['geo'] as $val_geo) {
            array_push($geo_array, $val_geo);
        }
        $geo_db = implode(", ",$geo_array);
        $recipients = array();
        foreach($_POST['rules-recipients'] as $val_rec) {
            array_push($recipients, $val_rec);
        }
        $rec_list = implode(", ",$recipients);

        if($default_rule){
            // validate if the default rule already exists
            $query_exist = "SELECT id FROM notification_rules 
                        WHERE request_type=".$_POST['request_type']." AND active = 1 ";
        }else{
            // validate if the custom rule already exists
            $query_exist = "SELECT id FROM notification_rules 
                        WHERE request_type=".$_POST['request_type']." 
                        AND geo = '".$geo_db."' AND sector = '".$_POST['sector']."' 
                        AND subsector = '".$_POST['subsector']."' AND active = 1";
        }
        $exist = $mysqli->query($query_exist);

        if($exist->num_rows === 0){
            $query = "INSERT INTO notification_rules VALUES (
                            NULL,
                            ".$_POST['request_type'].",
                            '".$geo_db."',
                            '".$_POST['sector']."',
                            '".$_POST['subsector']."',
                            '".$rec_list."',
                            '".$_POST['default-assignee']."',
                            '".$_POST['nickname']."',
                            '".$_SESSION['user_id']."',
                            NOW(),
                            NOW(),
                            1,
                            ".$default_rule.")";

            $result = $mysqli->query($query);

            header('Location:/notification-rules?message=created');
        }else{
            if($default_rule)
                // send a message to default rules
                header('Location:/notification-rules?action=create&default=true&message=exist');    
            else
                // send a message to custom rules
                header('Location:/notification-rules?action=create&message=exist');
        }
    }


    /* close connection */
    $mysqli->close();
} else {
    header('Location:/notification-rules'); //No action
}

?>