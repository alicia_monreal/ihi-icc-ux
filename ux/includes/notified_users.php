<style type="text/css">
.notification-recipients {
    padding: 10px 0 0 !important;
}
ul {
    list-style: none;
    margin: 0 0 10px 4px;
}
</style>

<script>
function removeThisUser(removeDiv) {   
    /* this function is used in the registration form functionality */
    // remove the input choice
    $(removeDiv).remove();
    return false;
}
</script>

<?php
if(!empty($_SESSION['in_progress_id'])) {
    $query_owners = "SELECT additional_owners FROM request WHERE id = ".intval($_SESSION['in_progress_id']);
    $add_owners_result = $mysqli->query($query_owners);
    $owners_result = $add_owners_result->fetch_object();
};
?>
<h3 class="fieldgroup-header">Select Additional Notified Recipients</h3>
<div class="fieldgroup">
	<div class="control-group">
		<label for="additional-notifiers" class="control-label">You may add additional users to be notified whenever changes are made to this request</label>
        	<div class="controls" style="margin-top:8px">
            <select id="additional-notifiers" name="additional-notifiers[]" data-placeholder="Choose a user..." multiple style="width:50%;border:none !important;" class="required">
         	<?php   
         	$users_string = "";
            $query_users = "SELECT id, fname, lname, email FROM users WHERE active = 1 ORDER BY fname ASC";
            $result_users = $mysqli->query($query_users);
            
            while ($obj_users = $result_users->fetch_object()) {
                 $users_string .= '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
            }
            echo $users_string;
            ?>
		    </select>
            <div class="chosen-notifiers notification-recipients"><ul></ul></div>
            </div>
	</div>
</div>

<h3 class="fieldgroup-header">Select Additional Users That Can View/Edit This Request</h3>
<div class="fieldgroup">
	<div class="control-group">
		<label for="additional-rights-users" class="control-label">You may grant additional users the same editing permissions as you</label>
			<div class="controls" style="margin-top:8px">
            <select id="additional-rights-users" name="additional-rights-users[]" data-placeholder="Choose a user..." multiple style="width:50%;border:none !important;" class="required">
         	<?php 

            echo $users_string;                            
            echo '</select>';

            if(!empty($owners_result->additional_owners)) {  // if additional onwers already exists for current request
                $userId = unserialize($owners_result->additional_owners);
                echo '<div class="notification-recipients"><ul>';
                // display the already saved additional recipients
                foreach ($userId as $user) {
                    if($user != ""){
                        $query_users = "SELECT id, fname, lname, email FROM users WHERE id =".$user;
                        $result_users = $mysqli->query($query_users);
                        while ($obj_users = $result_users->fetch_object()) {
                            echo '<li class="user-'.$obj_users->id.'"><div class="remove removeUserRule" data-remove-id="first_name" alt="Remove" onclick="removeThisUser(\'.user-'.$obj_users->id.'\');"></div>'.$obj_users->fname.' '.$obj_users->lname.' - '.$obj_users->email.'<input type="hidden" id="add-owners" name="add-owners[]" value="'.$obj_users->id.'"></li>';
                        }
                    }
                }
                echo '</ul></div>';  
            }

            ?>
            <div class="chosen-rights-users notification-recipients"><ul></ul></div>
            </div>
	</div>
</div>
