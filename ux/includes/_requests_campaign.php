<?php
session_start();
require_once('validate.php');
include('_globals.php');
include('notification_rules_email.php');
if (!empty($_POST) && !empty($_POST['action'])) {

// When using an existing campaign 
if($_POST['action']=='setCampaign'){
    $campaignId=getCampaignIdFromName($_POST['campaign_id']);
    $_SESSION['campaign_id']=$campaignId;
    exit();
}

    //Step 2. Saving values to session data
    if ($_POST['step'] == 2) {

        // $request_id = $_SESSION['in_progress_id']; // request id fetched from session ------ Commented 4 now  - JY

        //Only one step here, so we're going to send this content to the db
        //$mmi = $_POST['mmi']; //Taking out for time being. Hubert. 0311
        $user_id=100;
        $request_type;
        $mmi = '';
        //$program = $_POST['program']; //Taking out for time being. Hubert/JM. 0314
        $tag='';
        $eloqua_source_id='';
        $program = '';
        $campaign_cta = $mysqli->real_escape_string($_POST['campaign_cta']);
        $campaign_name=$_POST['campaign_name'];
        $geo_array = array();
        foreach($_POST['geo'] as $val_geo) {
            array_push($geo_array, $val_geo);
        }
        $geo_db = implode(", ",$geo_array);
        $sector = $_POST['sector'];
        $subsector = $_POST['subsector'];
        /*if ($_POST['subsector_other'] == 'None') {
            $subsector_other = '';
        } else {
            $subsector_other = $mysqli->real_escape_string($_POST['subsector_other']);
        }*/
        $subsector_other = $mysqli->real_escape_string($_POST['subsector_other']);
        $target_audience = $mysqli->real_escape_string($_POST['target_audience']);
        if ($_POST['start_date'] != '') {
            $start_date = date('Y-m-d 00:00:00', strtotime($_POST['start_date'])); //date format
        } else {
            $start_date = NULL;
        }
        if ($_POST['end_date'] != '') {
            $end_date = date('Y-m-d 00:00:00', strtotime($_POST['end_date'])); //date format
        } else {
            $end_date = NULL;
        }
        //$marketing_id = $mysqli->real_escape_string($_POST['marketing_id']); //moved to tactic level. 0317. hubert
        $generate_leads = $_POST['generate_leads'];
        $generate_leads= implode(' ', $generate_leads);
        if($generate_leads=='on'){
            $generate_leads=1;
        }else{
            $generate_leads=0;
        }
        $campaign_objectives = $mysqli->real_escape_string($_POST['campaign_objectives']);
        $talking_points = $mysqli->real_escape_string($_POST['talking_points']);
        $reference_material = $mysqli->real_escape_string($_POST['reference_material']);

        //Sorting out the Campaign Name here
        if (count($geo_array) >= 5) { //Selected all
            $geo_name = 'WW';
        } else {
            $geo_name = implode(" ",$geo_array);
        }
        $start_date_month = date('m', strtotime($start_date));
        if ($start_date_month == '01' || $start_date_month == '02' || $start_date_month == '03') {
            $quarter = 'Q1';
        } else if ($start_date_month == '04' || $start_date_month == '05' || $start_date_month == '06') {
            $quarter = 'Q2';
        } else if ($start_date_month == '07' || $start_date_month == '08' || $start_date_month == '09') {
            $quarter = 'Q3';
        } else {
            $quarter = 'Q4';
        }
        $start_date_year = date('y', strtotime($start_date));
        if ($sector == 'Embedded') {
            $sector_name = 'EMB';
        } else {
            $sector_name = $sector;
        }
        if ($subsector == 'ITDM-ESS') {
            $subsector_name = 'ESS';
        } else if ($subsector == 'ITDM-WA') {
            $subsector_name = 'WA';
        } else if ($subsector == 'Intel Technology Provider (ITP)') {
            $subsector_name = 'ITP';
        } else {
            $subsector_name = $subsector;
        }
        //$campaign_name = $geo_name.'_'.$program.'_'.$quarter.''.$start_date_year; //OLD NAME. Changing per Hubert/JM. 0314
        if (strtoupper($subsector_name) != 'NONE' && strtoupper($subsector_name) != 'OTHER') {
            $eloqua_source_id = strtoupper($sector_name).'.'.strtoupper($subsector_name).'.'.$geo_name.'.'.$_POST['campaign_name'];
        } else {
            $eloqua_source_id = strtoupper($sector_name).'.'.$geo_name.'.'.$_POST['campaign_name'];
        }

        if (empty($_SESSION['in_progress_id'])) { // If campaign in progress then don't check campaign name
            //We need to see if this campaign already exists before we continue
            $query_check = "SELECT request_id FROM request_campaign WHERE eloqua_source_id = '".$eloqua_source_id."' LIMIT 1";
            $result_check = $mysqli->query($query_check);
            $row_cnt_check = $result_check->num_rows;

            if ($row_cnt_check > 0) {
                //Whoops, there's a campaign already created! Let's send them back with a link to it, but also save all their content
                while ($obj_check = $result_check->fetch_object()) {
                    $c_id = $obj_check->request_id;
                }
                $_SESSION['eloqua_source_id']=$eloqua_source_id;
                $_SESSION['name'] = $_POST['campaign_name'];
                $_SESSION['mmi'] = $_POST['mmi'];
                $_SESSION['program'] = $_POST['program'];
                $_SESSION['campaign_cta'] = $_POST['campaign_cta'];
                $_SESSION['geo'] = $geo_array;
                $_SESSION['sector'] = $_POST['sector'];
                $_SESSION['subsector'] = $_POST['subsector'];
                $_SESSION['subsector_other'] = $_POST['subsector_other'];
                $_SESSION['target_audience'] = $_POST['target_audience'];
                $_SESSION['start_date'] = $_POST['start_date'];
                $_SESSION['end_date'] = $_POST['end_date'];
                //$_SESSION['marketing_id'] = $_POST['marketing_id'];
                $_SESSION['generate_leads'] = $_POST['generate_leads'];
                $_SESSION['campaign_objectives'] = $_POST['campaign_objectives'];
                $_SESSION['talking_points'] = $_POST['talking_points'];
                $_SESSION['reference_material'] = $_POST['reference_material'];

                //Ok, send them back
                header('Location:/request-campaign-2.php?error=name&c_id='.$c_id);
                die();
            }
        }


        //And now let's send this all into a series of tables.
        //First we create a new master REQUEST and then we put it in the individual request table.

        /////////////////////////////////////////////////////////////////////////////////////
        //Create a new request
        $user_id = $_POST['user_id'];
        $user_email = $_POST['user_email'];
        $request_type = 8;

        // if campaign in progress then update the request_campaign table
       if (isset($_SESSION['in_progress_id']) && $_SESSION['in_progress_id'] != NULL) { 

            $query = "UPDATE request_campaign SET
                            name = '".$campaign_name."',
                            tag = '".$tag."',
                            mmi = '".$mmi."',
                            program = '".$program."',
                            NULL,
                            campaign_cta = '".$campaign_cta."',
                            geo = '".$geo_db."',
                            sector = '".$sector."',
                            subsector = '".$subsector."',
                            subsector_other = '".$subsector_other."',
                            target_audience = '".$target_audience."',
                            start_date = '".$start_date."',
                            end_date = '".$end_date."',
                            generate_leads = '".$generate_leads."',
                            campaign_objectives = '".$campaign_objectives."',
                            talking_points = '".$talking_points."',
                            reference_material = '".$reference_material."',
                            approved = '1',
                            approved_by = '0',
                            revision = '1',
                            active = '1'
                        WHERE request_id = ".$request_id;
            $mysqli->query($query);
            $campaign_id = getTacticIdFromRequestId($_SESSION['in_progress_id'], $request_type); // get tactic id
      
        } else { // else insert into request and request_campaign tables
            $query_request = "INSERT INTO request VALUES (
                            NULL,
                            ".$user_id.",
                            '',
                            '',
                            ".$request_type.",
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            '0',
                            '1',
                            '1',
                            NULL,
                            '".$user_id."',
                            NOW(),
                            NOW(),
                            NOW(),
                            '1')";
            
            $mysqli->query($query_request);
            //New request ID
            $request_id = $mysqli->insert_id;
    
            //Adding ID to Campaign Name
            $eloqua_source_id = $eloqua_source_id.'.'.$request_id;
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);
            /////////////////////////////////////////////////////////////////////////////////////
            //Create a new individual request
            $query = "INSERT INTO request_campaign VALUES (
                            NULL,
                            '".$user_id."',
                            '".$request_id."',
                            NULL,
                            '".$eloqua_source_id."',
                            '".$campaign_name."',
                            '".$tag."',
                            '".$mmi."',
                            '".$program."',
                            NULL,
                            '".$campaign_cta."',
                            '".$geo_db."',
                            '".$sector."',
                            '".$subsector."',
                            '".$subsector_other."',
                            '".$target_audience."',
                            '".$start_date."',
                            '".$end_date."',
                            '".$generate_leads."',
                            '".$campaign_objectives."',
                            '".$talking_points."',
                            '".$reference_material."',
                            '1',
                            '0',
                            '1',
                            '1')";

        $mysqli->query($query);
        
            $campaign_id = $mysqli->insert_id; // save tactic id
        }

        $query_final = "UPDATE request SET status = 1,campaign_id='".$campaign_id."' WHERE id = ".$request_id." LIMIT 1";
        $final = $mysqli->query($query_final);
        $_SESSION['campaign_id']= $campaign_id;
        if($final) {  // if status updated successfully then proceed
            /////////////////////////////////////////////////////////////////////////////////////
            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);


            /////////////////////////////////////////////////////////////////////////////////////
            //Send email to Intel
            $campaign_name_subject = stripslashes(cleanForEmail($campaign_name, 1)); //Adding stripslashes again due to real_escape
            $campaign_name = stripslashes(cleanForEmail($campaign_name));

            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new campaign request has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">".$campaign_name."<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Campaign Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$campaign_name."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Target GEO</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$geo_db."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Sector</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$sector."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Sub-Sector</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">"; if ($subsector == 'Other' || $subsector_other != '') { $message_ms.=$subsector_other; } else { $message_ms.=$subsector; } $message_ms.="</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/campaign-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            //$mail->AddCC('seamusx.james@intel.com', 'Seamus James'); //Special person
            //Special for Doug Marshall and Kristi
            //if ($user_id == 365) { $mail->AddCC('kristi@ironhorseinteractive.com', 'Kristi Teplitz'); }

            /* Send notification rules email to assignees */
            $mail = sendNotificationRulesEmail($mail,$request_type,$campaign_id, $request_id, $mysqli, $message_header,$message_footer, $domain, $mail3); 

            $mail->Subject = 'Intel Customer Connect: New Campaign: '.$campaign_name_subject;
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/campaign-detail?id='.$request_id.'';
            $mail->Send();
            
            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/campaign-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Intel Customer Connect: Campaign Received: '.$campaign_name_subject;
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();

            //We need to kill all these session variables. Oy!
            unset($_SESSION['request_type']);
            unset($_SESSION['campaign_id_type']);
            unset($_SESSION['campaign_name_id']);
            unset($_SESSION['language']);

            //Finally, let's let 'em know
            // header('Location:/campaign-detail?id='.$request_id.'&action=success');
        }

    }

} else {
    header('Location:request?action=failure'); 
}
?>