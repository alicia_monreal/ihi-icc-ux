<?php
session_start();
include('_globals.php');

if (!empty($_POST) && !empty($_POST['email_contact'])) {

    //Values
    $name = $mysqli->real_escape_string($_POST['name']);
    $email = $mysqli->real_escape_string($_POST['email_contact']);
    $category = $mysqli->real_escape_string($_POST['category']);
    $message = $mysqli->real_escape_string($_POST['message']);

    //Send email to support
    $message_user = $message_header;
    //Sending email to full support if they're coming from support page
    if (isset($_POST['fullsupport'])) {
        $subject = $mysqli->real_escape_string($_POST['subject']);
        //$severity = $mysqli->real_escape_string($_POST['severity']);

        $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following was sent from the Intel Customer Connect contact form:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>Name:</strong> ".$name."<br /><strong>Email:</strong> <a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a><br /><strong>Subject:</strong> ".stripcslashes($subject)."<br /><strong>Category:</strong> ".$category."<br /><br />".stripcslashes($message)."</p>";
    
    // NEW REQUEST REQUIREMENT
    } else if (isset($_POST['newrequestrequirement'])) {
        $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following was sent from the Intel Customer Connect New Request Requirement form:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>Name:</strong> ".$name."<br /><strong>Email:</strong> <a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a><br /><strong>Category:</strong> ".$category."<br /><br />".stripcslashes($message)."</p>";
    } else {
        $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following was sent from the Intel Customer Connect contact form:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>Name:</strong> ".$name."<br /><strong>Email:</strong> <a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a><br /><strong>Category:</strong> ".$category."<br /><br />".stripcslashes($message)."</p>";
    }
    $message_user.=$message_footer;

    //$email_support = 'support@customerconnect.intel.com';
    $email_support = 'ezra@ironhorseinteractive.com';

    $mail2->AddAddress($email_support);
    if (isset($_POST['newrequestrequirement'])) {
        $mail2->Subject = 'Intel Customer Connect New Request Requirement Form: '.$category.'';
    } else {
        $mail2->Subject = 'Intel Customer Connect Contact Form: '.$category.'';
    }
    $mail2->MsgHTML($message_user);
    $mail2->AltBody = $message;
    $mail2->AddReplyTo($email,$name);

    //Send the message, check for errors
    if(!$mail2->Send()) {
        echo 'Sorry, there was a problem sending your email. Please <a href="mailto:'.$email_support.'">contact us</a>.';
        die();
    }
    echo 'Your message has been received. We\'ll follow up with you shortly. <a href="" data-dismiss="modal">Close window</a><br /><br />';

    /* close connection */
    $mysqli->close();
} else {
    echo 'There has been a problem.';  
}
?>