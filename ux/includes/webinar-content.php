<?php 
$loc = 'campaign-requests';
$step = 3;
$c_type = 'webinar';
?>
<div id='webinar-content' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content" style="padding-left:5%;padding-right:5%">
        <div class="modal-header modal-tabs">
          <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
          <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Webinar Content - Console Setup</h2>
          <ul class="nav nav-tabs" role="tablist" style='display: block;width: 56%;margin: 0 auto; margin-top:20px'>
            <li role="presentation" class="active col-md-6" style='padding:0 5px 0 5px'><a href="#default-widgets" style="padding-left:3px;padding-right:3px" aria-controls="home" role="tab" data-toggle="tab">Default Widgets</a></li>
            <li role="presentation" class='col-md-6' style='padding:0 5px 0 5px'><a href="#optional-widgets" style="padding-left:3px;padding-right:3px" aria-controls="profile" role="tab" data-toggle="tab">Optional Widgets</a></li>
          </ul>
        </div>

        <div class="modal-body" style="padding-bottom:90px;padding-top:40px;margin:0 5% 0 20%">

         <div role="tabpanel">
         <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="default-widgets">

            <form id="default-widgets-form" class="content-update-form form-horizontal" method="POST" enctype="multipart/form-data">
              <input type="hidden" id="action" name="action" value="create">
              <input type="hidden" id="step" name="step" value="3">
              <input type="hidden" id="c_type" name="c_type" value="webinar">
              <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
              <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
              <input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
                <div class="form-group" style="position:relative">
                <label class="control-label" style='margin-bottom:10px'>Presentation</label>
                <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="This file should contain the main presentation content for your webinar, and will be displayed to your audience along with polls, surveys, Flash videos, and any other content types you choose below."></i>
                <div class="row">
                  <div class='col-md-12'>
                    <div class="upload-div col-lg-5" style="padding:0px;">
                        <div id="presentation" class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn-file btn-green iconned-btn"><span class="fileupload-new"><?php if ($_SESSION['files_presentation']) { echo 'Change File'; } else { echo 'Add Presentation'; } ?></span><span class="icon-plus-media"><i class='fa fa-plus'></i></span><input id="fileupload_presentation" type="file" name="files[]"></span>
                      </div>  
                    
                    <div id="progress-presentation" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                          <div class="progress-bar bar"></div>
                      </div>
                      <div id="errors-presentation" class="files" style="clear:both;color:#cc0000;"></div>
                      <div id="shown-presentation" class="files" style="clear:both;">
                        <?php

                        if ($_SESSION['files_presentation']) {
                          $filebox = '<div name="'. ($_SESSION['files_presentation']). '">';
                          $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_presentation']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_presentation_path'].'">' . $_SESSION['files_presentation'] . '</a></p>';
                                  $filebox.= '</div>';
                                  echo $filebox;
                            }
                            ?>
                      </div>
                    </div>
                    <span class="col-lg-7 small" for=''>Upload one .PPTX file up to 25MB.<br/>Cannot include transtion animations.</span>
                  </div>
                </div>
                <div class="icon presentation" style='position: absolute;top: -3px;left: -48px;'></div>
              </div>

              <div class="form-group" style="position:relative">
                <label style='margin-bottom:10px'>Media Upload</label>
                <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="You can upload audio and video files to play during your webinar. For archived events, this widget allows attendees to pause, fast-forward, or rewind the webcast presentation. Video files will play at the same aspect ratio as your slides so please format your files accordingly. (For 16:9, recommended dimensions are 640x360, 480x270 and 320x180.)"></i>
                <div class="row">
                  <div class='col-md-12'>
                    <div class="upload-div col-lg-5" style="padding:0px;">
                        <div id="media" class="fileupload fileupload-new" data-provides="fileupload">
                          <span class="btn-file btn-green iconned-btn"><span class="fileupload-new">Add Media<span class="icon-plus-media"><i class='fa fa-plus'></i></span></span><input id="fileupload_media" type="file" name="files[]"></span>
                        </div>
                          <div id="progress-media" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                              <div class="progress-bar bar"></div>
                          </div>
                          <div id="errors-media" class="files" style="clear:both;color:#cc0000;"></div>
                          <div id="shown-media" class="files" style="clear:both;">
                           <?php
                              if (isset($_SESSION['files_media'])) {
                                $i=0;
                                foreach ($_SESSION['files_media'] as $val_media) {
                                  $filebox = '<div name="'. ($val_media). '">';
                                  $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($val_media) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_media_path'][$i].'">' . $val_media . '</a></p>';
                                          $filebox.= '</div>';
                                          echo $filebox;
                                  // echo '<p>'.$val_media.'</p>';
                                  //echo '<input type="hidden" name="files_media[]" value="'.$val_media.'" />';
                                  $i++;
                                }
                              }
                            ?>
                          </div>
                        </div>
                      <span class="col-lg-7 small" for='placeholder-btn'>Upload up to 20 FLV or MP4 files, up to 25MB each.</span>
                  </div>

                  </div>
                   <div class="icon media_upload" style='position: absolute;top: -3px;left: -48px;'></div>
              </div>
          

                  <div class="form-group" style="margin-top:30px;position:relative">
                    <label>Speakers & Moderators</label>
                    <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="You can include introductions for your presenters within your webinar. You must include the name for at least one speaker. Bios, photos, and additional speaker introductions are optional. Speaker info will be shared with the audience to provide them with contact information for follow up questions. Should your speakers wish to not be potentially contacted, do not provide email information. Recommended photo size is 80px wide by 95px high."></i>
                    <div class="icon speakers" style='position: absolute;top: -3px;left: -48px;'></div>
                  </div>

                  <div class="form-group">
                    <label style="font-family:'IntelClearRegular';font-weight:100"> <input style='margin-right:20px' type="radio" id="speaker_option[]" name="speaker_option" value="Simple" class="required" checked><b>Simple Display</b> - Shows name and email only</label>
                    <label style="font-family:'IntelClearRegular';font-weight:100"> <input style='margin-right:20px' type="radio" id="speaker_option[]" name="speaker_option" value="Detailed" class="required"><b>Detailed Display</b> - shows name, email, title company, speaker bio and image (optional)</label>
                  </div>

                  <div class="speaker speaker_1 form-group col-md-12">
                    <div class="form-group">
                      <div style="">
                        <label class="control-label" style="padding-right: 30px;">Speaker/Moderator 1</label>                        
                        <label class="checkbox-inline" style="padding-top:0;"> <input type="checkbox" id="speaker_type_1[]" name="speaker_type_1[]" value="Speaker" <?php if (in_array('Speaker', $_SESSION['speaker_type_1'])) { echo 'checked'; } ?>> Speaker</label>
                        <label class="checkbox-inline" style="padding-top:0;"> <input type="checkbox" id="speaker_type_1[]" name="speaker_type_1[]" value="Moderator" <?php if (in_array('Moderator', $_SESSION['speaker_type_1'])) { echo 'checked'; } ?>> Moderator</label>
                      </div>
                    </div>

                    <div class="form-group">
                      <input type="text" id="speaker_name_1" name="speaker_name_1" class="form-control" placeholder="Name (120 characters max)" value="" maxlength="120">
                    </div>
                    <div class="form-group">
                      <input type="text" id="speaker_email_1" name="speaker_email_1" class="form-control" placeholder="Email" value="" maxlength="40">
                    </div>

                    <!-- Extra fields -->
                    <div class="bios-extra">
                      <div class="form-group">
                        <input type="text" id="speaker_title_1" name="speaker_title_1" class="form-control" placeholder="Title (120 characters max)" value="" maxlength="120">
                      </div>
                      <div class="form-group">
                        <input type="text" id="speaker_company_1" name="speaker_company_1" class="form-control" placeholder="Company (120 characters max)" value="" maxlength="120">
                      </div>
                      <div class="form-group">
                        <textarea id="speaker_bio_1" name="speaker_bio_1" class="input-wide form-control" placeholder="Enter bio (2048 characters max)" maxlength="2048"></textarea>
                      </div>
                      <div class="upload-div form-group">
                            <div id="photo_1" class="fileupload fileupload-new" data-provides="fileupload">
                              <span class="btn-file btn-green iconned-btn" style="min-width: 140px !important; font-size: 15px;"><span class="fileupload-new"><?php if ($_SESSION['files_photo_'.$i]) { echo 'Change File'; } else { echo 'Add Photo'; } ?></span><span class="icon-plus-media"><i class='fa fa-plus'></i></span><input id="fileupload_photo_1" data-number="1" type="file" name="files[]"></span>
                            </div>
                            <div id="progress-photo_1" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                                <div class="progress-bar bar"></div>
                            </div>
                            <div id="shown-photo_1" class="files" style="clear:both;">
                              <?php if ($_SESSION['files_photo_'.$i]) {
                                // echo '<p>'.$_SESSION['files_photo_'.$i].'</p>';
                                $filebox = '<div name="'. ($_SESSION['files_photo_'.$i]). '">';
                                $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_photo_'.$i]) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="/server/php/files/'.$_SESSION['user_id'].'/'.$_SESSION['files_photo_'.$i].'">' . $_SESSION['files_photo_'.$i] . '</a></p>';
                                        $filebox.= '</div>';
                                        echo $filebox;
                                  }
                              ?>
                            </div>
                        </div>
                    </div>
                  </div>

                  <!-- Add more speakers -->
                  <?php for ($i = 2; $i <= 10; $i++) {
                      echo '
                      <div class="form-group speaker speaker_'.$i.' col-md-12"'; if ($_SESSION['speaker_name_'.$i]) { echo ' style="display:block;"'; } echo '>
                        <div style="margin-bottom:10px;margin-left:-15px;">
                          <label class="control-label" style="padding-right: 30px;">Speaker/Moderator '.$i.'</label>
                          <label class="checkbox-inline" style="padding-top:0;"> <input type="checkbox" id="speaker_type_'.$i.'[]" name="speaker_type_'.$i.'[]" value="Speaker"'; if (in_array('Speaker', $_SESSION['speaker_type_'.$i.''])) { echo 'checked'; } echo '> Speaker</label>
                          <label class="checkbox-inline" style="padding-top:0;"> <input type="checkbox" id="speaker_type_'.$i.'[]" name="speaker_type_'.$i.'[]" value="Moderator"'; if (in_array('Moderator', $_SESSION['speaker_type_'.$i.''])) { echo 'checked'; } echo '> Moderator</label>
                        </div>
                        <div class="form-group">
                          <input type="text" id="speaker_name_'.$i.'" name="speaker_name_'.$i.'" class="form-control" placeholder="Name (120 characters max)" value="" maxlength="120">
                        </div>
                        <div class="form-group">
                          <input type="text" id="speaker_email_'.$i.'" name="speaker_email_'.$i.'" class="form-control" placeholder="Email" value="" maxlength="40">
                        </div>


                        <div class="bios-extra form-group"'; if ($_SESSION['speaker_option'] == 'Detailed') { echo ' style="display:block;"'; } echo '>
                          <input type="text" name="speaker_title_'.$i.'" class="form-control" id="speaker_title_'.$i.'" placeholder="Title (120 characters max)" value="" maxlength="120" value="'.$_SESSION['speaker_title_'.$i].'"><br />
                          <input type="text" name="speaker_company_'.$i.'" class="form-control" id="speaker_company_'.$i.'" placeholder="Company (120 characters max)" value="" maxlength="120" value="'.$_SESSION['speaker_company_'.$i].'"><br />
                          <textarea id="speaker_bio_'.$i.'" name="speaker_bio_'.$i.'" class="input-wide form-control" placeholder="Enter bio (2048 characters max)" maxlength="2048">'.$_SESSION['speaker_bio_'.$i].'</textarea><br />

                          <div class="upload-div">
                                <div id="photo_'.$i.'" class="fileupload fileupload-new" data-provides="fileupload">
                                  <span class="btn-file btn-green iconned-btn" style="min-width: 140px !important; font-size: 15px;"><span class="fileupload-new">'; if ($_SESSION['files_photo_'.$i]) { echo 'Change File'; } else { echo 'Add Photo'; } echo '</span><span class="icon-plus-media"><i class="fa fa-plus"></i></span><input id="fileupload_photo_'.$i.'" class="fileupload_photo" data-number="'.$i.'" type="file" name="files[]"></span>
                                </div>
                                <div id="progress-photo_'.$i.'" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 190px;display:none;">
                                    <div class="progress-bar bar"></div>
                                </div>
                                <div id="shown-photo_'.$i.'" class="files" style="clear:both;">';
                                  if ($_SESSION['files_photo_'.$i]) {
                                    // echo '<p>'.$_SESSION['files_photo_'.$i].'</p>';
                                    $filebox = '<div name="'. ($_SESSION['files_photo_'.$i]). '">';
                                    $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_photo_'.$i]) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="/server/php/files/'.$_SESSION['user_id'].'/'.$_SESSION['files_photo_'.$i].'">' . $_SESSION['files_photo_'.$i] . '</a></p>';
                                            $filebox.= '</div>';
                                            echo $filebox;
                                      }
                                  echo '
                                </div>
                            </div>
                        </div>
                      </div>';
                    }
                    ?>

                    <div class="form-group">
                      <div class="row">
                        <div class='col-md-12'>
                          <p style="padding:0px;" class="col-lg-7"><a href="" id="addSpeaker" class="btn-green pull-left iconned-btn" style="width:300px;">Add a Speaker/Moderator<span class="icon-plus-media"><i class='fa fa-plus'></i></span></span></a>
                          <div class="pull-left small col-lg-5" style="padding:0;">You may add up to 10 speakers or moderators</div></p>
                        </div>
                        
                          
                        </div>
                    </div>
                  </form>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="optional-widgets">
            <form id="optional-widgets-form" class="optional-upload-form form-horizontal" method="POST" enctype="multipart/form-data">

              <div class="form-group" style="position:relative">
                <label for='url' class="control-label">URL</label>
                <div class="input-group">
                <input type="text" name='url_value' class="form-control" id="url_value" placeholder="Enter URL" value="">
               <span class='input-group-addon'> <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="Use of the URL widget will open a new browser window to the URL of your choosing." data-original-title="" title=""></i></span>
               </div>
               <div class="" style="position:absolute;top:-11px;left:-79px">
                 <label class="checkbox-inline">
                    <input type="checkbox" name="url" id="url" value="1">
                </label>
               </div>
                <div class="icon url" style="position:absolute;top: 2px;left: -35px;"></div>
              </div>

              <div class="form-group" style="position:relative">
              <label style='margin-bottom:10px'>Survey</label>
              <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="This option allows you to gather feedback in real time from webinar attendees." data-original-title="" title=""></i>
              <div class="row">
                <div class='col-md-12'>
                  <div class="upload-div col-md-5" style="padding: 0;">
                    <div id="survey" class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="btn-file btn-green iconned-btn col-md-12" style="min-width: 195px;"><span class="fileupload-new"><?php if ($_SESSION['files_survey']) { echo 'Change File'; } else { echo 'Add Survey File'; } ?></span><span class="icon-plus-media"><i class='fa fa-plus'></i></span><input id="fileupload_survey" type="file" name="files[]"></span>
                    </div>
                    <div id="progress-survey" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                          <div class="progress-bar bar"></div>
                      </div>
                      <div id="shown-survey" class="files" style="clear:both;">
                        <?php
                        if ($_SESSION['files_survey']) {
                          $filebox = '<div name="'. ($_SESSION['files_survey']). '">';
                          $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_survey']) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_survey_path'].'">' . $_SESSION['files_survey'] . '</a></p>';
                                  $filebox.= '</div>';
                                  echo $filebox;
                          // echo '<p>'.$_SESSION['files_survey'].'</p>';
                            }
                            ?>
                      </div>
                  </div>
                  <!-- <span class="col-md-7" style='padding-top:9px' for='placeholder-btn'><i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="This option allows you to gather feedback in real time from webinar attendees." data-original-title="" title=""></i></span> -->
                </div>

                </div>
                <div class="" style="position:absolute;top:-11px;left:-79px">
                 <label class="checkbox-inline">
                    <input type="checkbox" name="surveycheck" id="surveycheck" value="1">
                </label>
               </div>
                <div class="icon survey" style="position:absolute;top: 2px;left: -35px;"></div>
            </div>

             <div class="form-group" style="position:relative">
                <label for='social_media' class="control-label">Social Media</label>
                <div class="input-group">
                <input type="text" name='socialmedia_url' class="form-control" id="socialmedia_url" placeholder="Enter URL" value="">
               <span class='input-group-addon'> <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="Use of the Social Media widget will open a new browser window to the URL of your choosing. Please provide the URL of the social media feed you'd like to share." data-original-title="" title=""></i></span>
               </div>
               <div class="" style="position:absolute;top:-11px;left:-79px">
                 <label class="checkbox-inline">
                    <input type="checkbox" name="socialmedia" id="socialmedia" value="1">
                </label>
               </div>
               <div class="icon social_media" style="position:absolute;top: 2px;left: -35px;"></div>
              </div>

               <div class="form-group" style="position:relative">
                  <label for='resource_list' class="control-label">Resource List</label>
                  <div class="input-group">
                  <input type="text" name='resourcelist_url' class="form-control" id="resourcelist_url" placeholder="Enter URL" value="">
                 <span class='input-group-addon'> <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="The Resource List can hold any type of file (under 25MB) or URL. It is recommend to have no more than 10-11 links or assets in this section." data-original-title="" title=""></i></span>
                 </div>
                 <div class="" style="position:absolute;top:-11px;left:-79px">
                   <label class="checkbox-inline">
                      <input type="checkbox" name="resourcelist" id="resourcelist" value="1">
                  </label>
                 </div>
                 <div class="icon resource_list" style="position:absolute;top: 2px;left: -35px;"></div>
              </div>

               <div class="form-group">
                <div class="row">
                  <div class='col-md-12'>
                    <div class="upload-div col-md-5" style="padding: 0;">
                        <div id="resource" class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn-file btn-green iconned-btn col-md-12" style="min-width: 195px;"><span class="fileupload-new">Add File(s)</span><span class="icon-plus-media"><i class='fa fa-plus'></i></span><input id="fileupload_resource" type="file" name="files[]" multiple></span>
                      </div>
                      <div id="progress-resource" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                            <div class="progress-bar bar"></div>
                        </div>
                        <div id="shown-resource" class="files" style="clear:both;">
                          <?php
                          if (isset($_SESSION['files_resource'])) {
                            $x=0;
                            foreach ($_SESSION['files_resource'] as $val_resource) {
                              $filebox = '<div name="'. ($val_resource). '">';
                              $filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($val_resource) . '" href="#"><i class="icon-trash icon-white"></i></a> <a target=_blank href="'.$_SESSION['files_resource_path'][$x].'">' . $val_resource . '</a></p>';
                                      $filebox.= '</div>';
                                      echo $filebox;
                              // echo '<p>'.$val_resource.'</p>';
                              $x++;
                            }
                              }
                              ?>
                        </div>
                    </div>
                    <span class="col-lg-7" style='position:absolute;padding-top:9px' for='placeholder-btn'></span>
                  </div>

                  </div>
              </div>

              </form>
          </div>
        </div>

      </div>
    </div>
  <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
    <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="" id="webinar-content-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request : Step 3" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0)" id="webinar-content-save" class="btn-green submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request : Step 3" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
  </div>

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->