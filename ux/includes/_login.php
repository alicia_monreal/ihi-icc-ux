<?php
session_start();
include('_globals.php');

if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])) {

    //Values
    $username = $_POST['username'];
    //$password = md5($_POST['password']); //Not using md5.
    $password = $_POST['password'];

    $query = "SELECT * FROM users WHERE email = '".$username."' AND password = '".$password."' AND active = 1 LIMIT 1";
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    if ($row_cnt > 0) {
        $_SESSION['loggedin'] = 1;
        $_SESSION['timeout'] = time();

        //Set a Remember Me cookie
        if (isset($_POST['rememberme'])) {
            setcookie('rememberme', $username, time()+31536000, '/');
        } elseif (!isset($_POST['rememberme'])) {
            if (isset($_COOKIE['rememberme'])) {
                setcookie('rememberme', '', time()-3600, '/');
            }
        }

        while ($obj = $result->fetch_object()) {
            $_SESSION['user_id'] = $obj->id;
            $_SESSION['admin'] = $obj->admin;
            $_SESSION['role'] = $obj->role;
            $_SESSION['fname'] = $obj->fname;
            $_SESSION['lname'] = $obj->lname;
            $_SESSION['email'] = $obj->email;
            $_SESSION['title'] = $obj->title;
            $_SESSION['company'] = $obj->company;
            $_SESSION['point_of_contact'] = $obj->point_of_contact;
            $_SESSION['point_of_contact_email'] = $obj->point_of_contact_email;
            $_SESSION['geo'] = $obj->geo;
            $_SESSION['sector'] = $obj->sector;
            $_SESSION['subsector'] = $obj->subsector;
            $_SESSION['subsector_other'] = $obj->subsector_other;
            

            //Access control array
            $_SESSION['access_control'] = explode(", ", $obj->access_control);
            $access_control_arr = explode(", ", $obj->access_control);
            if (empty($access_control_arr)) {
                $_SESSION['access_blocked'] = true;
            } else {
                $_SESSION['access_blocked'] = false;
            }

            //Is this the first time they've logged in?
            $query_chk = "SELECT * FROM log_users WHERE user_id = '".$obj->id."'";
            $result_chk = $mysqli->query($query_chk);
            $row_cnt_chk = $result_chk->num_rows;

            if ($row_cnt_chk > 0) { $firsttime = false; } else { $firsttime = true; }

            //Log log in
            $query_log = "INSERT INTO log_users VALUES (NULL, '".$obj->id."', NOW())";
            $mysqli->query($query_log);
        }

        if ($firsttime == true) {
            echo '/my-account?new=true';
        } else {
            if (isset($_SESSION['linkback'])) {
                echo $_SESSION['linkback'];
            } else {
                echo '/ux/home';
            }
        }
    } else {
        echo '<span style="color:#ff0000;">Sorry, that login did not work or your account is not active yet. Please double check and try again.</span>'; 
    }

    /* close connection */
    $mysqli->close();
} else {
    echo 'There has been an error.';  
}
?>