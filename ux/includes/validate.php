<?php
@session_start();

if (!isset($_SESSION['loggedin']) && $loc != 'login') {
	$location = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$_SESSION['linkback'] = str_replace('.php', '', $location);
	header('Location: /ux/');
	die();
}
if (isset($_SESSION['timeout']) && $_SESSION['timeout'] + (120 * 60) < time()) { //Session has timed out
	if ($loc != 'login') {
		header('Location: /ux/log-out.php?session=timeout');
		die();
	}
} else {
	$_SESSION['timeout'] = time();
}
?>