<?php
$class_active = ' class="active"';

if (isset($_GET['edit']) && $_GET['edit'] === '1') { $edit_id = $_GET['req_id']; }
?>
<div id="wrap">
    <div class="auxbar">
		<div class="container-fluid">
		  <div class="logo pull-left"><a href="http://www.intel.com" target="_blank" data-analytics-label="Header: Intel External"><img src="img/logo-intel-header.png" alt="Intel" id="intellogo"></a></div>
		  <div class="share pull-right"><?php if ($loc != 'login') { ?><div class="tab pull-left welcome">Welcome, <?php echo $_SESSION['fname']; ?></div><div class="tab pull-left<?php if ($loc == 'my-account') { echo ' active'; } ?>"><a href="my-account" data-analytics-label="Header: My Account">My Account</a></div><div class="tab pull-left"><a href="log-out" data-analytics-label="Header: Log Out">Log Out</a></div><?php } ?><div class="tab pull-left"><a <?php if ($loc != 'login') { echo 'href="support"'; } else { echo 'href="#contact-us" data-toggle="modal"'; } ?> data-analytics-label="Header: Support">Support</a></div></div>
		</div>
        <input class="session-user-id" type="hidden" value="<?php echo $_SESSION['user_id']?>" >
    </div>
    <div class="topnav-templates">
        <div class="container-fluid">
			<div class="logo-update-center pull-left"><img src="img/logo-customer-connect-templates.png" alt="Customer Connect"></div>
            <?php if ((isset($step) && $step == 3) && (isset($_GET['type']) && $_GET['type'] == 'webinar')) { ?>
            <div class="template-type-links pull-right">Configuration: 
                <?php if (isset($t_type) && $t_type == 'default') {
                    echo 'Default';
                } else {
                    if (isset($_GET['edit']) && $_GET['edit'] === '1') {
                        echo '<a href="templates-default?type=webinar&edit=1&req_id='.$_GET['req_id'].'">Default</a>';
                    } else {
                        echo '<a href="templates-default?type=webinar">Default</a>';
                    }
                }
                echo ' | ';
                if (isset($t_type) && $t_type == 'custom') {
                    echo 'Custom';
                } else {
                    if (isset($_GET['edit']) && $_GET['edit'] === '1') {
                        echo '<a href="templates-custom?type=webinar&edit=1&req_id='.$_GET['req_id'].'">Custom</a>';
                    } else {
                        echo '<a href="templates-custom?type=webinar">Custom</a>';
                    }
                }
                ?></div>
            <?php } ?>
        </div>
    </div>
