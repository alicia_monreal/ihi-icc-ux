<?php
session_start();
require_once('validate.php');
include('_globals.php');
include('notification_rules_email.php');


if (!empty($_POST) && !empty($_POST['action'])) {

    //Some globals
    $user_id = $_POST['user_id'];
    $user_email = $_POST['user_email'];
    $request_type = 1;
    $campaign_id = $_SESSION['webinar_campaign_id'];
    
    //Step 2. Saving values to session data
    if ($_POST['step'] == '2') {
        
        $request_id = $_SESSION['in_progress_id'];

        //All our session values
        $_SESSION['name'] = $_POST['name'];
        $_SESSION['owner'] = $_POST['owner'];
        $_SESSION['owner_email'] = $_POST['owner_email'];
        $_SESSION['vertical'] = ''; //Removing per Hubert. 0314
        // $_SESSION['name'] = createTacticName('Webinar', $_POST['tag']);
        $_SESSION['webinar_title'] = $_POST['webinar_title'];
        $_SESSION['start_date'] = $_POST['start_date'];
        $_SESSION['start_time'] = $_POST['start_time'];
        $_SESSION['start_ampm'] = $_POST['start_ampm'];
        $_SESSION['end_time'] = $_POST['end_time'];
        $_SESSION['end_ampm'] = $_POST['end_ampm'];
        $_SESSION['time_zone'] = $_POST['time_zone'];
        $_SESSION['description'] = $_POST['description'];
      //  $_SESSION['asset_reviewers'] = $_POST['asset_reviewers'];
        //$_SESSION['expected_attendees'] = $_POST['expected_attendees'];

        // $_SESSION['nda_content'] = $_POST['nda_content'];
        // $_SESSION['green_screen_utilized'] = $_POST['green_screen_utilized'];

        // $_SESSION['webinar_initial_date'] = $_POST['webinar_initial_date'];
        // $_SESSION['send_time_invitation'] = $_POST['send_time_invitation'];
        // $_SESSION['send_invitation_ampm'] = $_POST['send_invitation_ampm'];

        // $_SESSION['webinar_reminder_1_date'] = $_POST['webinar_reminder_1_date'];
        // $_SESSION['send_time_reminder_1'] = $_POST['send_time_reminder_1'];
        // $_SESSION['send_reminder_1_ampm'] = $_POST['send_reminder_1_ampm'];

        // $_SESSION['webinar_reminder_2_date'] = $_POST['webinar_reminder_2_date'];
        // $_SESSION['send_time_reminder_2'] = $_POST['send_time_reminder_2'];
        // $_SESSION['send_reminder_2_ampm'] = $_POST['send_reminder_2_ampm'];

        // //$_SESSION['email_subject_line'] = $_POST['email_subject_line'];
        // $_SESSION['email_from_name'] = $_POST['email_from_name'];
        // $_SESSION['email_from_address'] = $_POST['email_from_address'];
        // $_SESSION['email_reply_name'] = $_POST['email_reply_name'];
        // $_SESSION['email_reply_address'] = $_POST['email_reply_address'];

        //Create a new individual request
        // $language = $_SESSION['language'];
        // $marketing_id = $_SESSION['marketing_id'];

        $name= $_SESSION['name'];
        // $tag = $mysqli->real_escape_string($_POST['tag']);
        $owner = $mysqli->real_escape_string($_POST['owner']);
        $owner_email = $mysqli->real_escape_string($_POST['owner_email']);
        $vertical_db = '';
        $webinar_title = $mysqli->real_escape_string($_POST['webinar_title']);
        $start_date = date('Y-m-d 00:00:00', strtotime($_POST['start_date'])); //date format
        $start_time_db = $_POST['start_time'].' '.$_POST['start_ampm'];
        $_SESSION['start_time_db'] = $start_time_db;
        $end_time_db = $_POST['end_time'].' '.$_POST['end_ampm'];
        $_SESSION['end_time_db'] = $end_time_db;
        $time_zone = $mysqli->real_escape_string($_POST['time_zone']);
        $description = $mysqli->real_escape_string($_POST['description']);
       // $asset_reviewers = $mysqli->real_escape_string($_POST['asset_reviewers']);
        // $expected_attendees = $mysqli->real_escape_string($_POST['expected_attendees']);
        
        // $nda_content = $_POST['nda_content'];
        // $green_screen_utilized = $_POST['green_screen_utilized'];
        // $webinar_initial_date = date('Y-m-d 00:00:00', strtotime($_POST['webinar_initial_date'])); //date format
        // $send_time_initial_db = $_POST['send_time_invitation'].' '.$_POST['send_invitation_ampm'];

        // $webinar_reminder_1_date = date('Y-m-d 00:00:00', strtotime($_POST['webinar_reminder_1_date'])); //date format
        // $send_time_reminder_1_db = $_POST['send_time_reminder_1'].' '.$_POST['send_reminder_1_ampm'];

        // $webinar_reminder_2_date = date('Y-m-d 00:00:00', strtotime($_POST['webinar_reminder_2_date'])); //date format
        // if ($_POST['send_time_reminder_2'] != '') {
        //     $send_time_reminder_2_db = $_POST['send_time_reminder_2'].' '.$_POST['send_reminder_2_ampm'];
        // } else {
        //     $send_time_reminder_2_db = '';
        // }

        // //$email_subject_line = $mysqli->real_escape_string($_POST['email_subject_line']);
        // $email_from_name = $mysqli->real_escape_string($_POST['email_from_name']);
        // $email_from_address = $mysqli->real_escape_string($_POST['email_from_address']);
        // $email_reply_name = $mysqli->real_escape_string($_POST['email_reply_name']);
        // $email_reply_address = $mysqli->real_escape_string($_POST['email_reply_address']);

        // //MID
        // $source_activity_name = $mysqli->real_escape_string($_SESSION['source_activity_name']);
        // $source_description = $mysqli->real_escape_string($_SESSION['source_description']);
        // $source_program = $_SESSION['source_program'];
        // $source_activity_type = $_SESSION['source_activity_type'];
        // $source_offer_type = $_SESSION['source_offer_type'];
        // $marketing_team = $_SESSION['marketing_team'];

        //IN PROGRESS
        //This is in progress
        if (isset($_SESSION['in_progress_id']) && $_SESSION['in_progress_id'] != NULL) {
            $eloqua_source_id = createEloquaID($campaign_id, $_SESSION['name'], $_SESSION['in_progress_id']);
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);

            //Insert UPDATE statement instead
            $query = "UPDATE request_webinar SET
                            eloqua_source_id = '".$eloqua_source_id."',
                            name = '".$name.".".$request_id."',
                            owner = '".$owner."',
                            owner_email = '".$owner_email."',
                            webinar_title = '".$webinar_title."',
                            start_date = '".$start_date."',
                            start_time = '".$start_time_db."',
                            end_time = '".$end_time_db."',
                            time_zone = '".$time_zone."',
                            description = '".$description."',
                            nda_content = '".$nda_content."',
                            green_screen_utilized = '".$green_screen_utilized."',
                            listed_on_intel = '".$listed_on_intel."',
                            available_on_intel_end_date = '".$available_on_intel_end_date."',
                            webinar_initial_date = '".$webinar_initial_date."',
                            webinar_initial_time = '".$send_time_initial_db."',
                            webinar_reminder_1_date = '".$webinar_reminder_1_date."',
                            webinar_reminder_1_time = '".$send_time_reminder_1_db."',
                            webinar_reminder_2_date = '".$webinar_reminder_2_date."',
                            webinar_reminder_2_time = '".$send_time_reminder_2_db."',
                            email_from_name = '".$email_from_name."',
                            email_from_address = '".$email_from_address."',
                            email_reply_name = '".$email_reply_name."',
                            email_reply_address = '".$email_reply_address."'
                        WHERE request_id = ".$request_id;
            $mysqli->query($query);
            $_SESSION['tactic_request_id'] = getTacticIdFromRequestId($_SESSION['in_progress_id'], $request_type);

        } else {
            //Create a new request
            $request_id = createInProgressRequest($user_id, $request_type, $campaign_id);

            $eloqua_source_id = createEloquaID($campaign_id, $_SESSION['name'], $request_id);
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);

            $query = "INSERT INTO request_webinar VALUES (
                            NULL,
                            '".$user_id."',
                            '".$request_id."',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '".$eloqua_source_id."',
                            '',
                            '".$name."',
                            '',
                            '".$owner."',
                            '".$owner_email."',
                            '".$vertical_db."',
                            '".$webinar_title."',
                            '".$start_date."',
                            '".$start_time_db."',
                            '".$end_time_db."',
                            '".$time_zone."',
                            '".$description."',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '1',
                            '1')";

            $mysqli->query($query);
            $_SESSION['in_progress_id'] = $request_id;
            $_SESSION['tactic_request_id'] = $mysqli->insert_id;
        }
        if (isset($_POST['redirect'])){
            $arr = array(
              'redirect'=>true
            );
            echo json_encode($arr);
        }else{
            $return['moduleResponse'] = array('status'=>200, 'request_id'=> $_SESSION['in_progress_id'], 'insert_id'=> $_SESSION['tactic_request_id'], 'message'=>'OK');
            echo json_encode($return);
        }
    }

    //Step 3. Saving values to session data
    if ($_POST['step'] == '3') {

        $request_id = $_SESSION['in_progress_id'];

        /////////////////////////////////////////////////////////////////////////////////////
        //Remove all files and speakers related to the request, going to be entering in fresh so we don't get duplicates
        $query_remove_assets = "UPDATE assets SET active = 0 WHERE (type = 'Survey' OR type = 'Resource' OR type = 'Presentation' OR type = 'Media') AND request_id = ".$request_id;
        $mysqli->query($query_remove_assets);

        $query_remove_speakers = "UPDATE speakers SET active = 0, date_updated = NOW() WHERE request_id = ".$request_id;
        $mysqli->query($query_remove_speakers);


        /////////////////////////////////////////////////////////////////////////////////////
        //Default file path
        $path = '/ux/server/php/files/'.$user_id.'/';

        //Presentation file
        if (isset($_SESSION['files_presentation']) && !isset($_POST['files_presentation'])) {
            $_SESSION['files_presentation'] = $_SESSION['files_presentation'];
        } else {
            $_SESSION['files_presentation'] = $_POST['files_presentation'];
        }
        $files_presentation = $mysqli->real_escape_string($_SESSION['files_presentation']);
        if (!empty($files_presentation)) {
            /* don't change the session user id if it exists */
            $presentationPath = $path.$files_presentation;
            if(isset($_SESSION['files_presentation_path']) && !empty($_SESSION['files_presentation_path'])){
                $files_presentation_path = $_SESSION['files_presentation_path'];    
                $fpPath = explode("/", $files_presentation_path);
                $fpPath[sizeof($fpPath)-1] = $files_presentation;
                $presentationPath = implode("/", $fpPath);
            }
            $query_presentation = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_presentation."', '".$presentationPath."', 'Presentation', NOW(), '1')";
            $mysqli->query($query_presentation);
        }

        //Media files
        if (isset($_SESSION['files_media']) && !isset($_POST['files_media'])) {
            $_SESSION['files_media'] = $_SESSION['files_media'];
        } else {
            $media_array = array();
            foreach ($_POST['files_media'] as $val_media) {
                array_push($media_array, $val_media);
                $_SESSION['files_media'][] = $val_media;
            }
            // $_SESSION['files_media'] = $media_array;
        }
        $files_media = $_SESSION['files_media'];
        if (!empty($files_media)) {
            $media_number = count($files_media);
            $media_counter = 1;

            $query_media = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
            $z = 0;
            foreach ($files_media as $media) {
                /* don't change the session user id if it exists */
                $mediaPath = $path.$media;
                if(isset($_SESSION['files_media_path'][$z]) && !empty($_SESSION['files_media_path'][$z])){
                    $files_media_path = $_SESSION['files_media_path'][$z];    
                    $fmPath = explode("/", $files_media_path);
                    $fmPath[sizeof($fmPath)-1] = $media;
                    $mediaPath = implode("/", $fmPath);
                }
                $query_media.="(NULL, '".$request_id."', '".$user_id."', '".$media."', '".$mediaPath."', 'Media', NOW(), '1')";
                $media_counter++;
                if ($media_counter <= $media_number) {
                    $query_media.=", ";
                }
                $z++;
            }

            $query_media.=";";
            $mysqli->query($query_media);
        }

        //URL
        $_SESSION['url'] = $_POST['url'];
        $_SESSION['url_value'] = $_POST['url_value'];
        if (!empty($_POST['url_value'])) { $_SESSION['url'] = 1; } else { $_SESSION['url'] = 0; }

        //Survey file
        if (isset($_POST['surveycheck']) && $_POST['surveycheck'] == 1) { $_SESSION['surveycheck'] = 1; } else { $_SESSION['surveycheck'] = 0; }

        if (isset($_SESSION['files_survey']) && !isset($_POST['files_survey'])) {
            $_SESSION['files_survey'] = $_SESSION['files_survey'];
        } else {
            $_SESSION['files_survey'] = $_POST['files_survey'];
        }
        $files_survey = $mysqli->real_escape_string($_SESSION['files_survey']);
        if (!empty($files_survey)) {
            /* don't change the session user id if it exists */
            $surveyPath = $path.$files_survey;
            if(isset($_SESSION['files_survey_path']) && !empty($_SESSION['files_survey_path'])){
                $files_survey_path = $_SESSION['files_survey_path'];    
                $fsPath = explode("/", $files_survey_path);
                $fsPath[sizeof($fsPath)-1] = $files_survey;
                $surveyPath = implode("/", $fsPath);
            }
            $query_survey = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_survey."', '".$surveyPath."', 'Survey', NOW(), '1')";
            $mysqli->query($query_survey);
        }

        //Social Media
        $_SESSION['socialmedia'] = $_POST['socialmedia'];
        $_SESSION['socialmedia_url'] = $_POST['socialmedia_url'];
        if (!empty($_POST['socialmedia_url'])) { $_SESSION['socialmedia'] = 1; } else { $_SESSION['socialmedia'] = 0; }

        //Resource files
        $_SESSION['resourcelist'] = $_POST['resourcelist'];
        $_SESSION['resourcelist_url'] = $_POST['resourcelist_url'];
        if (!empty($_POST['resourcelist_url'])) { $_SESSION['resourcelist'] = 1; } else { $_SESSION['resourcelist'] = 0; }

        if (isset($_SESSION['files_resource']) && !isset($_POST['files_resource'])) {
            $_SESSION['files_resource'] = $_SESSION['files_resource'];
        } else {
            $resource_array = array();
            foreach ($_POST['files_resource'] as $val_resource) {
                array_push($resource_array, $val_resource);
                $_SESSION['files_resource'][] = $val_resource;
            }
            // $_SESSION['files_resource'] = $resource_array;
        }
        $files_resource = $_SESSION['files_resource']; //array
        if (!empty($files_resource)) {
            $resource_number = count($files_resource);
            $resource_counter = 1;

            $query_resource = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES ";
            $z=0;
            foreach ($files_resource as $resource) {
                /* don't change the session user id if it exists */
                $resourcePath = $path.$resource;
                if(isset($_SESSION['files_resource_path'][$z]) && !empty($_SESSION['files_resource_path'][$z])){
                    $files_resource_path = $_SESSION['files_resource_path'][$z];    
                    $frPath = explode("/", $files_resource_path);
                    $frPath[sizeof($frPath)-1] = $resource;
                    $resourcePath = implode("/", $frPath);
                }
                $query_resource.="(NULL, '".$request_id."', '".$user_id."', '".$resource."', '".$resourcePath."', 'Resource', NOW(), '1')";
                $resource_counter++;
                if ($resource_counter <= $resource_number) {
                    $query_resource.=", ";
                }
                $z++;
            }
            $query_resource.=";";
            $mysqli->query($query_resource);
        }

        $url = $_SESSION['url'];
        $url_value = $mysqli->real_escape_string($_SESSION['url_value']);
        $surveycheck = $_SESSION['surveycheck'];
        $socialmedia = $_SESSION['socialmedia'];
        $socialmedia_url = $mysqli->real_escape_string($_SESSION['socialmedia_url']);
        $resourcelist = $_SESSION['resourcelist'];
        $resourcelist_url = $mysqli->real_escape_string($_SESSION['resourcelist_url']);

        $_SESSION['speaker_option'] = $_POST['speaker_option'];
        $speaker_option = $mysqli->real_escape_string($_SESSION['speaker_option']);

        //Insert UPDATE statement
        $query_4 = "UPDATE request_webinar SET speaker_option = '".$speaker_option."', url = '".$url."', url_value = '".$url_value."', surveycheck = '".$surveycheck."', socialmedia = '".$socialmedia."', socialmedia_url = '".$socialmedia_url."', resourcelist = '".$resourcelist."', resourcelist_url = '".$resourcelist_url."' WHERE id = ".$_SESSION['tactic_request_id'];
        $mysqli->query($query_4);


        //Speakers
        //Breaking down individually. Not the best way to do it, but short on time
        $_SESSION['speaker_name_1'] = $_POST['speaker_name_1'];
        $speaker_type_1_array = array();
        if (!empty($_POST['speaker_type_1'])) {
            foreach($_POST['speaker_type_1'] as $val_speaker_type_1) {
                array_push($speaker_type_1_array, $val_speaker_type_1);
            }
        }
        $_SESSION['speaker_type_1'] = $speaker_type_1_array;
        $_SESSION['speaker_title_1'] = $_POST['speaker_title_1'];
        $_SESSION['speaker_company_1'] = $_POST['speaker_company_1'];
        $_SESSION['speaker_email_1'] = $_POST['speaker_email_1'];
        $_SESSION['speaker_bio_1'] = $_POST['speaker_bio_1'];
        if (isset($_SESSION['files_photo_1']) && !isset($_POST['files_photo_1'])) { $_SESSION['files_photo_1'] = $_SESSION['files_photo_1']; } else { $_SESSION['files_photo_1'] = $_POST['files_photo_1']; }
        $speaker_name_1 = $mysqli->real_escape_string($_SESSION['speaker_name_1']);
        $speaker_type_db_1 = implode(", ",$_SESSION['speaker_type_1']); //array
        $speaker_title_1 = $mysqli->real_escape_string($_SESSION['speaker_title_1']);
        $speaker_company_1 = $mysqli->real_escape_string($_SESSION['speaker_company_1']);
        $speaker_email_1 = $mysqli->real_escape_string($_SESSION['speaker_email_1']);
        $speaker_bio_1 = $mysqli->real_escape_string($_SESSION['speaker_bio_1']);
        $files_photo_1 = $mysqli->real_escape_string($_SESSION['files_photo_1']);

        $_SESSION['speaker_name_2'] = $_POST['speaker_name_2'];
        $speaker_type_2_array = array();
        if (!empty($_POST['speaker_type_2'])) {
            foreach($_POST['speaker_type_2'] as $val_speaker_type_2) {
                array_push($speaker_type_2_array, $val_speaker_type_2);
            }
        }
        $_SESSION['speaker_type_2'] = $speaker_type_2_array;
        $_SESSION['speaker_title_2'] = $_POST['speaker_title_2'];
        $_SESSION['speaker_company_2'] = $_POST['speaker_company_2'];
        $_SESSION['speaker_email_2'] = $_POST['speaker_email_2'];
        $_SESSION['speaker_bio_2'] = $_POST['speaker_bio_2'];
        if (isset($_SESSION['files_photo_2']) && !isset($_POST['files_photo_2'])) { $_SESSION['files_photo_2'] = $_SESSION['files_photo_2']; } else { $_SESSION['files_photo_2'] = $_POST['files_photo_2']; }
        $speaker_name_2 = $mysqli->real_escape_string($_SESSION['speaker_name_2']);
        $speaker_type_db_2 = implode(", ",$_SESSION['speaker_type_2']); //array
        $speaker_title_2 = $mysqli->real_escape_string($_SESSION['speaker_title_2']);
        $speaker_company_2 = $mysqli->real_escape_string($_SESSION['speaker_company_2']);
        $speaker_email_2 = $mysqli->real_escape_string($_SESSION['speaker_email_2']);
        $speaker_bio_2 = $mysqli->real_escape_string($_SESSION['speaker_bio_2']);
        $files_photo_2 = $mysqli->real_escape_string($_SESSION['files_photo_2']);

        $_SESSION['speaker_name_3'] = $_POST['speaker_name_3'];
        $speaker_type_3_array = array();
        if (!empty($_POST['speaker_type_3'])) {
            foreach($_POST['speaker_type_3'] as $val_speaker_type_3) {
                array_push($speaker_type_3_array, $val_speaker_type_3);
            }
        }
        $_SESSION['speaker_type_3'] = $speaker_type_3_array;
        $_SESSION['speaker_title_3'] = $_POST['speaker_title_3'];
        $_SESSION['speaker_company_3'] = $_POST['speaker_company_3'];
        $_SESSION['speaker_email_3'] = $_POST['speaker_email_3'];
        $_SESSION['speaker_bio_3'] = $_POST['speaker_bio_3'];
        if (isset($_SESSION['files_photo_3']) && !isset($_POST['files_photo_3'])) { $_SESSION['files_photo_3'] = $_SESSION['files_photo_3']; } else { $_SESSION['files_photo_3'] = $_POST['files_photo_3']; }
        $speaker_name_3 = $mysqli->real_escape_string($_SESSION['speaker_name_3']);
        $speaker_type_db_3 = implode(", ",$_SESSION['speaker_type_3']); //array
        $speaker_title_3 = $mysqli->real_escape_string($_SESSION['speaker_title_3']);
        $speaker_company_3 = $mysqli->real_escape_string($_SESSION['speaker_company_3']);
        $speaker_email_3 = $mysqli->real_escape_string($_SESSION['speaker_email_3']);
        $speaker_bio_3 = $mysqli->real_escape_string($_SESSION['speaker_bio_3']);
        $files_photo_3 = $mysqli->real_escape_string($_SESSION['files_photo_3']);

        $_SESSION['speaker_name_4'] = $_POST['speaker_name_4'];
        $speaker_type_4_array = array();
        if (!empty($_POST['speaker_type_4'])) {
            foreach($_POST['speaker_type_4'] as $val_speaker_type_4) {
                array_push($speaker_type_4_array, $val_speaker_type_4);
            }
        }
        $_SESSION['speaker_type_4'] = $speaker_type_4_array;
        $_SESSION['speaker_title_4'] = $_POST['speaker_title_4'];
        $_SESSION['speaker_company_4'] = $_POST['speaker_company_4'];
        $_SESSION['speaker_email_4'] = $_POST['speaker_email_4'];
        $_SESSION['speaker_bio_4'] = $_POST['speaker_bio_4'];
        if (isset($_SESSION['files_photo_4']) && !isset($_POST['files_photo_4'])) { $_SESSION['files_photo_4'] = $_SESSION['files_photo_4']; } else { $_SESSION['files_photo_4'] = $_POST['files_photo_4']; }
        $speaker_name_4 = $mysqli->real_escape_string($_SESSION['speaker_name_4']);
        $speaker_type_db_4 = implode(", ",$_SESSION['speaker_type_4']); //array
        $speaker_title_4 = $mysqli->real_escape_string($_SESSION['speaker_title_4']);
        $speaker_company_4 = $mysqli->real_escape_string($_SESSION['speaker_company_4']);
        $speaker_email_4 = $mysqli->real_escape_string($_SESSION['speaker_email_4']);
        $speaker_bio_4 = $mysqli->real_escape_string($_SESSION['speaker_bio_4']);
        $files_photo_4 = $mysqli->real_escape_string($_SESSION['files_photo_4']);

        $_SESSION['speaker_name_5'] = $_POST['speaker_name_5'];
        $speaker_type_5_array = array();
        if (!empty($_POST['speaker_type_5'])) {
            foreach($_POST['speaker_type_5'] as $val_speaker_type_5) {
                array_push($speaker_type_5_array, $val_speaker_type_5);
            }
        }
        $_SESSION['speaker_type_5'] = $speaker_type_5_array;
        $_SESSION['speaker_title_5'] = $_POST['speaker_title_5'];
        $_SESSION['speaker_company_5'] = $_POST['speaker_company_5'];
        $_SESSION['speaker_email_5'] = $_POST['speaker_email_5'];
        $_SESSION['speaker_bio_5'] = $_POST['speaker_bio_5'];
        if (isset($_SESSION['files_photo_5']) && !isset($_POST['files_photo_5'])) { $_SESSION['files_photo_5'] = $_SESSION['files_photo_5']; } else { $_SESSION['files_photo_5'] = $_POST['files_photo_5']; }
        $speaker_name_5 = $mysqli->real_escape_string($_SESSION['speaker_name_5']);
        $speaker_type_db_5 = implode(", ",$_SESSION['speaker_type_5']); //array
        $speaker_title_5 = $mysqli->real_escape_string($_SESSION['speaker_title_5']);
        $speaker_company_5 = $mysqli->real_escape_string($_SESSION['speaker_company_5']);
        $speaker_email_5 = $mysqli->real_escape_string($_SESSION['speaker_email_5']);
        $speaker_bio_5 = $mysqli->real_escape_string($_SESSION['speaker_bio_5']);
        $files_photo_5 = $mysqli->real_escape_string($_SESSION['files_photo_5']);

        $_SESSION['speaker_name_6'] = $_POST['speaker_name_6'];
        $speaker_type_6_array = array();
        if (!empty($_POST['speaker_type_6'])) {
            foreach($_POST['speaker_type_6'] as $val_speaker_type_6) {
                array_push($speaker_type_6_array, $val_speaker_type_6);
            }
        }
        $_SESSION['speaker_type_6'] = $speaker_type_6_array;
        $_SESSION['speaker_title_6'] = $_POST['speaker_title_6'];
        $_SESSION['speaker_company_6'] = $_POST['speaker_company_6'];
        $_SESSION['speaker_email_6'] = $_POST['speaker_email_6'];
        $_SESSION['speaker_bio_6'] = $_POST['speaker_bio_6'];
        if (isset($_SESSION['files_photo_6']) && !isset($_POST['files_photo_6'])) { $_SESSION['files_photo_6'] = $_SESSION['files_photo_6']; } else { $_SESSION['files_photo_6'] = $_POST['files_photo_6']; }
        $speaker_name_6 = $mysqli->real_escape_string($_SESSION['speaker_name_6']);
        $speaker_type_db_6 = implode(", ",$_SESSION['speaker_type_6']); //array
        $speaker_title_6 = $mysqli->real_escape_string($_SESSION['speaker_title_6']);
        $speaker_company_6 = $mysqli->real_escape_string($_SESSION['speaker_company_6']);
        $speaker_email_6 = $mysqli->real_escape_string($_SESSION['speaker_email_6']);
        $speaker_bio_6 = $mysqli->real_escape_string($_SESSION['speaker_bio_6']);
        $files_photo_6 = $mysqli->real_escape_string($_SESSION['files_photo_6']);

        $_SESSION['speaker_name_7'] = $_POST['speaker_name_7'];
        $speaker_type_7_array = array();
        if (!empty($_POST['speaker_type_7'])) {
            foreach($_POST['speaker_type_7'] as $val_speaker_type_7) {
                array_push($speaker_type_7_array, $val_speaker_type_7);
            }
        }
        $_SESSION['speaker_type_7'] = $speaker_type_7_array;
        $_SESSION['speaker_title_7'] = $_POST['speaker_title_7'];
        $_SESSION['speaker_company_7'] = $_POST['speaker_company_7'];
        $_SESSION['speaker_email_7'] = $_POST['speaker_email_7'];
        $_SESSION['speaker_bio_7'] = $_POST['speaker_bio_7'];
        if (isset($_SESSION['files_photo_7']) && !isset($_POST['files_photo_7'])) { $_SESSION['files_photo_7'] = $_SESSION['files_photo_7']; } else { $_SESSION['files_photo_7'] = $_POST['files_photo_7']; }
        $speaker_name_7 = $mysqli->real_escape_string($_SESSION['speaker_name_7']);
        $speaker_type_db_7 = implode(", ",$_SESSION['speaker_type_7']); //array
        $speaker_title_7 = $mysqli->real_escape_string($_SESSION['speaker_title_7']);
        $speaker_company_7 = $mysqli->real_escape_string($_SESSION['speaker_company_7']);
        $speaker_email_7 = $mysqli->real_escape_string($_SESSION['speaker_email_7']);
        $speaker_bio_7 = $mysqli->real_escape_string($_SESSION['speaker_bio_7']);
        $files_photo_7 = $mysqli->real_escape_string($_SESSION['files_photo_7']);

        $_SESSION['speaker_name_8'] = $_POST['speaker_name_8'];
        $speaker_type_8_array = array();
        if (!empty($_POST['speaker_type_8'])) {
            foreach($_POST['speaker_type_8'] as $val_speaker_type_8) {
                array_push($speaker_type_8_array, $val_speaker_type_8);
            }
        }
        $_SESSION['speaker_type_8'] = $speaker_type_8_array;
        $_SESSION['speaker_title_8'] = $_POST['speaker_title_8'];
        $_SESSION['speaker_company_8'] = $_POST['speaker_company_8'];
        $_SESSION['speaker_email_8'] = $_POST['speaker_email_8'];
        $_SESSION['speaker_bio_8'] = $_POST['speaker_bio_8'];
        if (isset($_SESSION['files_photo_8']) && !isset($_POST['files_photo_8'])) { $_SESSION['files_photo_8'] = $_SESSION['files_photo_8']; } else { $_SESSION['files_photo_8'] = $_POST['files_photo_8']; }
        $speaker_name_8 = $mysqli->real_escape_string($_SESSION['speaker_name_8']);
        $speaker_type_db_8 = implode(", ",$_SESSION['speaker_type_8']); //array
        $speaker_title_8 = $mysqli->real_escape_string($_SESSION['speaker_title_8']);
        $speaker_company_8 = $mysqli->real_escape_string($_SESSION['speaker_company_8']);
        $speaker_email_8 = $mysqli->real_escape_string($_SESSION['speaker_email_8']);
        $speaker_bio_8 = $mysqli->real_escape_string($_SESSION['speaker_bio_8']);
        $files_photo_8 = $mysqli->real_escape_string($_SESSION['files_photo_8']);

        $_SESSION['speaker_name_9'] = $_POST['speaker_name_9'];
        $speaker_type_9_array = array();
        if (!empty($_POST['speaker_type_9'])) {
            foreach($_POST['speaker_type_9'] as $val_speaker_type_9) {
                array_push($speaker_type_9_array, $val_speaker_type_9);
            }
        }
        $_SESSION['speaker_type_9'] = $speaker_type_9_array;
        $_SESSION['speaker_title_9'] = $_POST['speaker_title_9'];
        $_SESSION['speaker_company_9'] = $_POST['speaker_company_9'];
        $_SESSION['speaker_email_9'] = $_POST['speaker_email_9'];
        $_SESSION['speaker_bio_9'] = $_POST['speaker_bio_9'];
        if (isset($_SESSION['files_photo_9']) && !isset($_POST['files_photo_9'])) { $_SESSION['files_photo_9'] = $_SESSION['files_photo_9']; } else { $_SESSION['files_photo_9'] = $_POST['files_photo_9']; }
        $speaker_name_9 = $mysqli->real_escape_string($_SESSION['speaker_name_9']);
        $speaker_type_db_9 = implode(", ",$_SESSION['speaker_type_9']); //array
        $speaker_title_9 = $mysqli->real_escape_string($_SESSION['speaker_title_9']);
        $speaker_company_9 = $mysqli->real_escape_string($_SESSION['speaker_company_9']);
        $speaker_email_9 = $mysqli->real_escape_string($_SESSION['speaker_email_9']);
        $speaker_bio_9 = $mysqli->real_escape_string($_SESSION['speaker_bio_9']);
        $files_photo_9 = $mysqli->real_escape_string($_SESSION['files_photo_9']);

        $_SESSION['speaker_name_10'] = $_POST['speaker_name_10'];
        $speaker_type_10_array = array();
        if (!empty($_POST['speaker_type_10'])) {
            foreach($_POST['speaker_type_10'] as $val_speaker_type_10) {
                array_push($speaker_type_10_array, $val_speaker_type_10);
            }
        }
        $_SESSION['speaker_type_10'] = $speaker_type_10_array;
        $_SESSION['speaker_title_10'] = $_POST['speaker_title_10'];
        $_SESSION['speaker_company_10'] = $_POST['speaker_company_10'];
        $_SESSION['speaker_email_10'] = $_POST['speaker_email_10'];
        $_SESSION['speaker_bio_10'] = $_POST['speaker_bio_10'];
        if (isset($_SESSION['files_photo_10']) && !isset($_POST['files_photo_10'])) { $_SESSION['files_photo_10'] = $_SESSION['files_photo_10']; } else { $_SESSION['files_photo_10'] = $_POST['files_photo_10']; }
        $speaker_name_10 = $mysqli->real_escape_string($_SESSION['speaker_name_10']);
        $speaker_type_db_10 = implode(", ",$_SESSION['speaker_type_10']); //array
        $speaker_title_10 = $mysqli->real_escape_string($_SESSION['speaker_title_10']);
        $speaker_company_10 = $mysqli->real_escape_string($_SESSION['speaker_company_10']);
        $speaker_email_10 = $mysqli->real_escape_string($_SESSION['speaker_email_10']);
        $speaker_bio_10 = $mysqli->real_escape_string($_SESSION['speaker_bio_10']);
        $files_photo_10 = $mysqli->real_escape_string($_SESSION['files_photo_10']);

        /////////////////////////////////////////////////////////////////////////////////////
        //Input the speakers
        $query_speakers = "INSERT INTO speakers (id, request_id, user_id, speaker_type, speaker_name, speaker_title, speaker_company, speaker_email, speaker_bio, speaker_photo, date_created, date_updated, active) VALUES ";
        if (!empty($speaker_name_1)) { $query_speakers.="(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_1."', '".$speaker_name_1."', '".$speaker_title_1."', '".$speaker_company_1."', '".$speaker_email_1."', '".$speaker_bio_1."', '/server/php/files/".$user_id."/".$files_photo_1."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_2)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_2."', '".$speaker_name_2."', '".$speaker_title_2."', '".$speaker_company_2."', '".$speaker_email_2."', '".$speaker_bio_2."', '/server/php/files/".$user_id."/".$files_photo_2."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_3)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_3."', '".$speaker_name_3."', '".$speaker_title_3."', '".$speaker_company_3."', '".$speaker_email_3."', '".$speaker_bio_3."', '/server/php/files/".$user_id."/".$files_photo_3."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_4)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_4."', '".$speaker_name_4."', '".$speaker_title_4."', '".$speaker_company_4."', '".$speaker_email_4."', '".$speaker_bio_4."', '/server/php/files/".$user_id."/".$files_photo_4."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_5)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_5."', '".$speaker_name_5."', '".$speaker_title_5."', '".$speaker_company_5."', '".$speaker_email_5."', '".$speaker_bio_5."', '/server/php/files/".$user_id."/".$files_photo_5."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_6)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_6."', '".$speaker_name_6."', '".$speaker_title_6."', '".$speaker_company_6."', '".$speaker_email_6."', '".$speaker_bio_6."', '/server/php/files/".$user_id."/".$files_photo_6."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_7)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_7."', '".$speaker_name_7."', '".$speaker_title_7."', '".$speaker_company_7."', '".$speaker_email_7."', '".$speaker_bio_7."', '/server/php/files/".$user_id."/".$files_photo_7."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_8)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_8."', '".$speaker_name_8."', '".$speaker_title_8."', '".$speaker_company_8."', '".$speaker_email_8."', '".$speaker_bio_8."', '/server/php/files/".$user_id."/".$files_photo_8."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_9)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_9."', '".$speaker_name_9."', '".$speaker_title_9."', '".$speaker_company_9."', '".$speaker_email_9."', '".$speaker_bio_9."', '/server/php/files/".$user_id."/".$files_photo_9."', NOW(), NOW(), '1')"; }
        if (!empty($speaker_name_10)) { $query_speakers.=",(NULL, '".$request_id."', '".$user_id."', '".$speaker_type_db_10."', '".$speaker_name_10."', '".$speaker_title_10."', '".$speaker_company_10."', '".$speaker_email_10."', '".$speaker_bio_10."', '/server/php/files/".$user_id."/".$files_photo_10."', NOW(), NOW(), '1')"; }

        //Insert speakers into db
        $query_speakers.=";";
        if (!empty($speaker_name_1)) {
            $mysqli->query($query_speakers);
        }

        if (isset($_POST['redirect'])){
            $arr = array(
              'redirect'=>true
            );
            echo json_encode($arr);
        }
    }

    //Step 4. Saving values to session data
    if ($_POST['step'] == 4) {

        if(isset($_POST['template']) && $_POST['template'] !== ""){

            $request_id = $_SESSION['in_progress_id'];
            $template = '';
            $standard_template_type = '';

            if($_POST['action'] === 'savetemplate' && ($_POST['template'] === 'custom' || $_POST['template'] === 'default')){
                $template = 'Standard Template';
                $standard_template_type = $_POST['template'];

                $email_data = $_POST;
                unset($email_data['template']);
                unset($email_data['action']);
                unset($email_data['step']);
                unset($email_data['c_type']);

                //Encode characters
                foreach ($email_data as $key => $value) {
                    $email_data[$key] = $mysqli->real_escape_string($value);
                }

                //Create temporary request ID
                //$_SESSION['t_request_id']           =  $email_data['request_id'] = time(); //replaced wih line below. EJH
                $_SESSION['t_request_id']           =  $request_id; //Shahzad
                $_SESSION['templates_data']         =  $email_data;

                //Shahzad, we need to remove previously entered data, or add an active field to tables, otherwise it'll be a new db entry
                $query_remove = 'DELETE FROM templates_custom_data WHERE request_id = '.$request_id;
                $mysqli->query($query_remove);

                $query = 'INSERT INTO templates_custom_data VALUES (
                                NULL,
                                "' .$email_data['user_id'] .'",
                                "' .$request_id.'",
                                "' .$email_data['invitation_email_config'].'",
                                "' .$email_data['reminder_one_email_config'].'",
                                "' .$email_data['reminder_two_email_config'].'",
                                "' .$email_data['confirmation_email_config'].'",
                                "' .$email_data['thank_you_email_config'].'",
                                "' .$email_data['sorry_email_config'].'",
                                "' .$email_data['registration_config'].'",
                                "' .$email_data['confirmation_config'].'",
                                "' .$email_data['landing_page_config'].'",
                                "' .$email_data['single_email_config'].'")';

                $mysqli->query($query);
                $email_data_id = $mysqli->insert_id;
            }
            //Custom template, no fields
            else if ($_POST['template'] == 'Custom Template') {
                $template = 'Custom Template';
                $standard_template_type = '';

                if (!isset($_SESSION['files_customassets']) && !isset($_POST['files_customassets'])) {
                    // no uploaded file 
                    return;
                }
                
                //Custom assets
                if (isset($_SESSION['files_customassets']) && !isset($_POST['files_customassets'])) {
                    $_SESSION['files_customassets'] = $_SESSION['files_customassets'];
                } else {
                    $_SESSION['files_customassets'] = $_POST['files_customassets'];
                }
                $files_customassets = $_SESSION['files_customassets'];

                //Custom assets
                if (!empty($_SESSION['files_customassets'])) {
                    $path = "/server/php/files/".$user_id."/".$files_customassets;
                    if (isset($_SESSION['files_customassets_path']) && $_SESSION['files_customassets_path'] != '') {
                        $path_exisiting = $abspath."/server/php/files/".$user_id."/".$files_customassets;
                        if (!file_exists($path_exisiting)) { //This means they didn't upload something themselves even if there's a path set in session (i.e. overwritten file)
                            $path = $_SESSION['files_customassets_path'];
                        }
                    }
                    //First remove old assets
                    $query_customassets_remove = "UPDATE assets SET active = 0 WHERE request_id = '".$request_id."' AND type = 'Custom Assets'";
                    $mysqli->query($query_customassets_remove);

                    $query_customassets = "INSERT INTO assets (id, request_id, user_id, name, path, type, date_created, active) VALUES (NULL, '".$request_id."', '".$user_id."', '".$files_customassets."', '".$path."', 'Custom Assets', NOW(), '1')";
                    $mysqli->query($query_customassets);
                }
            }

            $_SESSION['template'] = $template;
            $_SESSION['standard_template_type'] = $standard_template_type;

            //Insert UPDATE statement
            $query_3 = "UPDATE request_webinar SET template = '".$template."', standard_template_type = '".$standard_template_type."' WHERE id = ".$_SESSION['tactic_request_id'];
            $mysqli->query($query_3);

            //Only insert template data if they've selected a template type
            if ($template == 'Standard Template' && !empty($standard_template_type)) {
                $query_template = "INSERT INTO templates VALUES (NULL, '".$user_id."', '".$request_id."', '".$standard_template_type."', NOW())";
                $mysqli->query($query_template);
                $template_id = $mysqli->insert_id;
            }

        }
        if (isset($_POST['redirect'])){
            $arr = array(
              'redirect'=>true
            );
            echo json_encode($arr);
        }

    }

    //Step 5. Saving values to session data, AND writing to the database
    if ($_POST['step'] == 5) {

        $request_id = $_SESSION['in_progress_id'];

        //STEP 5
        $create_contactlist = $_POST['create_contactlist'];
        $list_tag = $mysqli->real_escape_string($_POST['list_tag']);
        $contact_list_request_type = $mysqli->real_escape_string($_POST['contact_list_request_type']);
        $files_contacts = array();
        foreach ($_POST['files_contacts'] as $val_contacts) {
            array_push($files_contacts, $val_contacts);
        }
        $existing_contact_list = $mysqli->real_escape_string($_POST['existing_contact_list']);

        /////////////////////////////////////////////////////////////////////////////////////
        //Create a new Contact List tactic if they chose to do so
        if ($create_contactlist == 1) {
            include('_requests_create_contactlist.php');
        }

        //Insert UPDATE statement
        $query_4 = "UPDATE request_webinar SET existing_contact_list = '".$existing_contact_list."' WHERE id = ".$_SESSION['tactic_request_id'];
        $mysqli->query($query_4);

        //Finally, let's let 'em know
        // header('Location:/request-detail?id='.$request_id.'&action=success');
        if (isset($_POST['redirect'])){
            $arr = array(
              'redirect'=>true
            );
            echo json_encode($arr);
        }

    }

     //Step 6. Saving values to session data
    if ($_POST['step'] == 6) {
         $request_id = $_SESSION['in_progress_id'];

        $topics_array = array();
        foreach($_POST['topics_covered'] as $val_topics) {
            array_push($topics_array, $val_topics);
        }
        $_SESSION['topics_covered'] = $topics_array;
        $_SESSION['topics_covered_other'] = $_POST['topics_covered_other'];
        $_SESSION['listed_on_intel'] = $_POST['listed_on_intel'];
        $_SESSION['available_on_intel_end_date'] = $_POST['available_on_intel_end_date'];
        $topics_covered_db = implode(", ",$topics_array); //array
        $topics_covered_other = $mysqli->real_escape_string($_POST['topics_covered_other']);
        $listed_on_intel = $_POST['listed_on_intel'];
        $available_on_intel_end_date = date('Y-m-d 00:00:00', strtotime($_POST['available_on_intel_end_date'])); //date format

        //Insert UPDATE statement instead
        $query = "UPDATE request_webinar SET
                            topics_covered = '".$topics_covered_db."',
                            topics_covered_other = '".$topics_covered_other."',
                            listed_on_intel = '".$listed_on_intel."',
                            available_on_intel_end_date = '".$available_on_intel_end_date."'
                        WHERE request_id = ".$request_id;
        $mysqli->query($query);
  
        $_SESSION['tactic_request_id'] = getTacticIdFromRequestId($_SESSION['in_progress_id'], $request_type);

        if (isset($_POST['redirect'])){
            $arr = array(
              'redirect'=>true
            );
            echo json_encode($arr);
        }

    }

     //Step 7. Saving values to session data
    if ($_POST['step'] == 7) {
        $request_id = $_SESSION['in_progress_id'];
        $_SESSION['additional_instructions'] = $_POST['additional_instructions'];
        $additional_instructions = $mysqli->real_escape_string($_POST['additional_instructions']);
        $additional_recipients = $_POST['additional-notifiers'];
        $additional_owners = $_POST['additional-rights-users'];

        if(!empty($additional_recipients) && is_array($additional_recipients)) {
            $additional_rec = $mysqli->real_escape_string(serialize($additional_recipients)); // serialized string 
            $add_recipients = ",additional_recipients = \"$additional_rec\"";
        }

        if(!empty($additional_owners) && is_array($additional_owners)) {
            $additional_own = $mysqli->real_escape_string(serialize(array_unique($additional_owners))); // serialized sring
            $adding_owners = ",additional_owners = \"$additional_own\"";
        }

        //Insert UPDATE statement
        // $query_5 = "UPDATE request_webinar SET existing_contact_list = '".$existing_contact_list."', additional_instructions = '".$additional_instructions."' WHERE id = ".$_SESSION['tactic_request_id'];
        $query_5 = "UPDATE request_webinar SET additional_instructions = '".$additional_instructions."' WHERE request_id = ".$request_id;
        $mysqli->query($query_5);

        /////////////////////////////////////////////////////////////////////////////////////
        //Update the status of the request
        $query_final = "UPDATE request SET status = 1 $add_recipients $adding_owners WHERE id = ".$request_id." LIMIT 1";
        $final = $mysqli->query($query_final);

        //SUCCESS
        if ($final) {
            /////////////////////////////////////////////////////////////////////////////////////
            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);


            /////////////////////////////////////////////////////////////////////////////////////
            //Send email to Intel
            $name = cleanForEmail($_SESSION['name']);
            $tag = cleanForEmail($_SESSION['tag'], 1);
            $description_email = cleanForEmail($_SESSION['description']);

            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new webinar tactic request has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">".$name.".".$request_id."<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$name.".".$request_id."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Owner</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['owner']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Date</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['start_date']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"top\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Webinar Time</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['start_time_db']." - ".$_SESSION['end_time_db']." (".$_SESSION['time_zone'].")</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Description</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$description_email."</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            
            //$mail->AddCC('jm@ironhorseinteractive.com', 'JM Caballero'); //Special person
            //$mail->AddCC('ryan.rijken@sureshotmedia.com', 'Ryan Rijken'); //Special person
            //$mail->AddCC('nate.miller@sureshotmedia.com', 'Nate Miller'); //Special person
            //$mail->AddCC('monica.l.wellington@intel.com', 'Monica Wellington'); //Special person
            //Special for Doug Marshall and Kristi
            //if ($user_id == 365) { $mail->AddCC('kristi@ironhorseinteractive.com', 'Kristi Teplitz'); }
            
            /*
            //ITP logic
            if ($campaign_id == 337 || $campaign_id == 340) { $mail->AddCC('abrock@opuseventsagency.com', 'Amy Brock'); }
            if ($campaign_id == 338 || $campaign_id == 339) { $mail->AddCC('jrussell-x@opuseventsagency.com', 'Jodi Russell'); }
            */

            /* Send notification rules email to assignees */
            $mail = addAddtionalRecipients($mail,$additional_recipients,$email_ms);
            $mail = sendNotificationRulesEmail($mail,$request_type,$campaign_id, $request_id, $mysqli, $message_header,$message_footer, $domain, $mail3); 

            $mail->Subject = 'Intel Customer Connect: New Webinar: '.$tag.'.'.$request_id;
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/request-detail?id='.$request_id.'';
            $mail->Send();


            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Automated reports begin delivering EOD from when the first email is sent out daily for a week (5 business days) EST for each email. These are basic Eloqua reporting emails that have key metrics like: opens, clickthrough rate, click to open rate, possible email forwards, unique open rate.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Intel Customer Connect: Webinar Request Received: '.$tag.'.'.$request_id;
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();
            
            //Kill some session vars
            killSessionVars();
        }
        if (isset($_POST['redirect'])){
            $arr = array(
              'redirect'=>true
            );
            echo json_encode($arr);
        }

    }


} else {
    header('Location:request?action=failure');
}
?>
