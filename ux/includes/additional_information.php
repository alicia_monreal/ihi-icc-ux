<style type="text/css">
.notification-recipients {
    padding: 10px 0 0 !important;
}
ul {
    list-style: none;
    margin: 0 0 10px 4px;
}
</style>

<script>
function removeThisUser(removeDiv) {   
    /* this function is used in the registration form functionality */
    // remove the input choice
    $(removeDiv).remove();
    return false;
}
</script>
<div id='additional_information' class="modal fade">
  <div class="modal-dialog" style="width: 70%;max-width: 1000px;margin: 0 auto;margin-top: 5%;">
    <div class="modal-content" style="padding-left:5%;padding-right:5%">
        <div class="modal-header" style="text-align:center;text-align: center;padding-bottom: 1px;">
          <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
          <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Additional Information</h2>
        </div>

        <div class="modal-body" style="padding-bottom:90px;padding-top:40px;margin:0 10% 0 10%">
        <form class="form-horizontal" id="additional-information-form">
          <input type="hidden" id="action" name="action" value="create">
          <input type="hidden" id="step" name="step" value="7">
          <input type="hidden" id="c_type" name="c_type" value="webinar">
          <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
          <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
          <input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
          <div class="form-group">
            <label for="additional_instructions" class="control-label col-md-4" style="padding-top:0">Additional Instructions</label>
            <div class='col-md-8'>
             <textarea id="additional_instructions" name="additional_instructions" class=" form-control textarea-medium" placeholder="Please provide any additional instructions you'd like to add to your request."><?php echo getFieldValue('target_audience', isset($_SESSION['clone_campaign_id']) ? $_SESSION['clone_campaign_id'] : 0, 8, 'textarea'); ?></textarea>
            </div>
          </div>


          <div class="form-group">
            <label for="additional-notifiers" class="col-md-4 control-label" style="padding-top:0">You may add additional users to be notified whenever changes are made to this request</label>
            <div class="col-md-8">

              <select id="additional-notifiers" name="additional-notifiers[]" data-placeholder="Choose a user..." multiple class="required form-control">
              <?php   
              $users_string = "";
                $query_users = "SELECT id, fname, lname, email FROM users WHERE active = 1 ORDER BY fname ASC";
                $result_users = $mysqli->query($query_users);
                
                while ($obj_users = $result_users->fetch_object()) {
                     $users_string .= '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                }
                echo $users_string;
                ?>
              </select>
              <div class="chosen-notifiers notification-recipients"><ul></ul></div>
            </div>
          </div>

          <div class="form-group">
            <label for="additional-rights-users" class=" col-md-4 control-label" style="padding-top:0">You may grant additional users the same editing permissions as you</label>
              <div class="col-md-8">

                <select id="additional-rights-users" name="additional-rights-users[]" data-placeholder="Choose a user..." multiple class="form-control required">
                  <?php 

                    echo $users_string;                            
                    echo '</select>';

                    if(!empty($owners_result->additional_owners)) {  // if additional onwers already exists for current request
                        $userId = unserialize($owners_result->additional_owners);
                        echo '<div class="notification-recipients"><ul>';
                        // display the already saved additional recipients
                        foreach ($userId as $user) {
                            if($user != ""){
                                $query_users = "SELECT id, fname, lname, email FROM users WHERE id =".$user;
                                $result_users = $mysqli->query($query_users);
                                while ($obj_users = $result_users->fetch_object()) {
                                    echo '<li class="user-'.$obj_users->id.'"><div class="remove removeUserRule" data-remove-id="first_name" alt="Remove" onclick="removeThisUser(\'.user-'.$obj_users->id.'\');"></div>'.$obj_users->fname.' '.$obj_users->lname.' - '.$obj_users->email.'<input type="hidden" id="add-owners" name="add-owners[]" value="'.$obj_users->id.'"></li>';
                                }
                            }
                        }
                        echo '</ul></div>';  
                    }

                    ?>
                  </select>
                  <div class="chosen-rights-users notification-recipients"><ul></ul></div>
                </div>
            </div>
    </form>
    </div>
  <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
    <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="additional-info-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 7" val="<?php echo $tactic_name ?>">Save and Exit</a>
  </div>

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->