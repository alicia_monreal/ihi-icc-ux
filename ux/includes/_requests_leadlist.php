<?php
session_start();
header('Content-type: application/json');
require_once('validate.php');
include('_globals.php');
include('notification_rules_email.php');

if (!empty($_POST) && !empty($_POST['action'])) {

    //Step 2. Saving values to session data
    if ($_POST['step'] == 2) {        

        //And now let's send this all into a series of tables.
        
        //STEP 0
        $user_id = $_POST['user_id'];
        $user_email = $_POST['user_email'];
        $request_type = 9;
        $campaign_id = $_SESSION['campaign_id'];

        //$language = $_SESSION['language']; //removed req by Hubert. 0324
        // $marketing_id = $_SESSION['marketing_id'];

        // $source_activity_name = $mysqli->real_escape_string($_SESSION['source_activity_name']);
        // $source_description = $mysqli->real_escape_string($_SESSION['source_description']);
        // $source_program = $_SESSION['source_program'];
        // $source_activity_type = $_SESSION['source_activity_type'];
        // $source_offer_type = $_SESSION['source_offer_type'];
        // $marketing_team = $_SESSION['marketing_team'];


        //STEP 2
        $name = $mysqli->real_escape_string($_POST['tag']);
        $list_request_type = $mysqli->real_escape_string($_POST['list_request_type']);
        $list_raw = $mysqli->real_escape_string($_POST['list_raw']);
        $files_contacts = array();
        foreach ($_POST['files_contacts'] as $val_contacts) {
            array_push($files_contacts, $val_contacts);
        }

        //First we create a new master REQUEST and then we put it in the individual request table.

        /////////////////////////////////////////////////////////////////////////////////////
        //Create a new request
        $query_request = "INSERT INTO request VALUES (
                        NULL,
                        '".$user_id."',
                        '',
                        '',
                        '".$request_type."',
                        '".$campaign_id."',
                        NULL,
                        NULL,
                        NULL,
                        '0',
                        '1',
                        '1',
                        NULL,
                        '".$user_id."',
                        NOW(),
                        NOW(),
                        NOW(),
                        '1')";

        $mysqli->query($query_request);

        if ($mysqli->insert_id) {
            //New request ID
            $request_id = $mysqli->insert_id;
            // $name = $list_request_type.' List.'.$_POST['tag'];
            // $name_db = $mysqli->real_escape_string($name);
            $eloqua_source_id = createEloquaID($campaign_id, $name, $request_id);
            $eloqua_source_id = $mysqli->real_escape_string($eloqua_source_id);

            /////////////////////////////////////////////////////////////////////////////////////
            //Create a new individual request
            if($list_request_type !== "Contact"){
                $query = "INSERT INTO request_leadlist VALUES (
                                NULL,
                                '".$user_id."',
                                '".$request_id."',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '".$eloqua_source_id."',
                                '".$name."',
                                '',
                                '".$list_request_type."',
                                '1',
                                '1',
                                '".$list_raw."')";
            }else{
                $query = "INSERT INTO request_contactlist VALUES (
                            NULL,
                            '".$user_id."',
                            '".$request_id."',
                            '',
                            '".$eloqua_source_id."',
                            '',
                            '".$name."',
                            '',
                            '".$list_request_type."',
                            '1',
                            '1',
                            '".$list_raw."')";
            }
            $mysqli->query($query);


            /////////////////////////////////////////////////////////////////////////////////////
            //Input the assets

            //Contacts
            uploadAssets($files_contacts, 'Leads', $request_id, $user_id);


            /////////////////////////////////////////////////////////////////////////////////////
            //Create a note
            $query_note = "INSERT INTO notes VALUES (NULL, '".$request_id."', '".$user_id."', NULL, 'Request created', NOW(), '1')";
            $mysqli->query($query_note);


            /////////////////////////////////////////////////////////////////////////////////////
            //Send email to Intel
            $name = cleanForEmail($name);
            $tag = cleanForEmail($name, 1);

            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new lead list has been submitted.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;font-weight:bold;\">".$name.".".$request_id."<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$_SESSION['fname']." ".$_SESSION['lname']."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                </table>
            <p><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";

            $message_ms.=$message_footer;

            $mail->AddAddress($email_ms);
            //$mail->AddCC('dianex.c.schroeder@intel.com', 'Diane Schroeder'); //Special person
            //Special for Doug Marshall and Kristi
            //if ($user_id == 365) { $mail->AddCC('kristi@ironhorseinteractive.com', 'Kristi Teplitz'); }

            /* Send notification rules email to assignees */
            $mail = sendNotificationRulesEmail($mail,$request_type,$campaign_id, $request_id, $mysqli, $message_header,$message_footer, $domain, $mail3); 
            
            $mail->Subject = 'Intel Customer Connect: New Lead List: '.$name.'.'.$request_id;
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new update request has been submitted. '.$domain.'/request-detail?id='.$request_id.'';
            $mail->Send();

            //Send email user
            $message_user = $message_header;
            $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you for submitting your request. We have received it and will review it shortly and get back to you.</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><a href=\"".$domain."/request-detail?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
            $message_user.=$message_footer;

            $mail2->AddAddress($user_email);
            $mail2->Subject = 'Intel Customer Connect: Lead List Received: '.$name.'.'.$request_id;
            $mail2->MsgHTML($message_user);
            $mail2->AltBody = 'Thank you for submitting your request. We have received it and will review it shortly and get back to you.';
            $mail2->Send();

            //Finally, let's let 'em know
            // header('Location:/request-detail?id='.$request_id.'&action=success');

            if (isset($_POST['redirect'])){
                $arr = array(
                  'redirect'=>true
                );
                echo json_encode($arr);
            }else{
                $return['moduleResponse'] = array('status'=>200, 'message'=>'OK');
                echo json_encode($return);
            }


        }
    }

} else {
    header('Location:request?action=failure'); 
}
?>