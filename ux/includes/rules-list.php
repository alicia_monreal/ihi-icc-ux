<?php
@session_start();
include('_globals.php');

$query = "SELECT 
                notification_rules.*, request_type.request_type as request_name,
                DATE_FORMAT( notification_rules.created_on,  '%m/%d/%Y' ) AS created_on,
                users.fname, users.lname
			FROM notification_rules
            LEFT OUTER JOIN request_type
            ON notification_rules.request_type = request_type.id
            LEFT OUTER JOIN users
            ON notification_rules.created_by = users.id";

 $default_rule = false;
if(isset($_GET['default'])){
    $default_rule = true;
}

if (isset($_GET['id']) && $_SESSION['admin'] == 1) {
	//SHOWING SINGLE RECORD

	$query.=" WHERE notification_rules.id = ".$_GET['id']." LIMIT 1";
				
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    if ($row_cnt > 0) {
        echo '<div class="single-request single-request-resource">';

        //Show a message?
        if (isset($_GET['update'])) {
        	if ($_GET['update'] == 'success') {
        		echo '<p style="color:#ff0000;"><strong>Notification rule has been updated.</strong><br /></p>';
        	} else if ($_GET['update'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>We\'re sorry, there has been a problem updating this notification rule.</strong><br /></p>';
            }
        }
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>Notification rule has been created.</strong><br /></p>';
            }
        }

        while ($obj = $result->fetch_object()) {
            $current_id = $_GET['id'];

        	//Admins get a form
        	echo '<form id="resource-update-form" action="includes/_notification-rules.php?id='.$current_id.'&action=update" method="POST" enctype="multipart/form-data">
				<input type="hidden" id="action" name="action" value="update">
				<input type="hidden" id="resource_id" name="resource_id" value="'.$current_id.'">
				<input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">';
        	echo '<div class="zebra">';

            echo '<div class="row"><label>Request Type</label> <div class="pull-left">';
                // Request Type
                $req_type = getTacticType($obj->request_type);
            echo "<span>".$req_type."</span>"; 
            echo '</div></div>';
            if(!$default_rule){
                $geo_arr = explode(", ", $obj->geo);
                echo '<div class="row row-border"><label for="geo" class="control-label">Target GEO</label>
                    <div class="controls pull-left">';
                    $i=0;
                    foreach ($geo_arr as $geo_val) {
                        if($i === sizeof($geo_arr)-1)
                            echo $geo_val;
                        else
                            echo $geo_val.", ";
                        $i++;
                    }

                echo '</div></div>';
                echo '<div class="row row-border"><label for="sector" class="control-label">Sector</label>
                        <div class="controls pull-left"><span>'.$obj->sector.'</span></div></div>';
                echo '<div class="row row-border"><label for="subsector" class="control-label">Sub-Sector</label>
                        <div class="controls pull-left"><span>'.$obj->subsector.'</span></div></div>'; 
            }
            echo '<div class="row row-border"><label for="subsector" class="control-label">Recipients</label>
                    <div class="controls pull-left"><span class="required-mark">*</span>
                    <select id="rules-recipients" name="rules-recipients[]" data-placeholder="Choose a user..." multiple style="width:50%;border:none !important;" class="required">';
                     ////recipients list
                    $nuids = $obj->notification_user_ids;
                    $userId = explode(",", $nuids);
                    $query_users = "SELECT id, fname, lname, email FROM users WHERE active = 1 ORDER BY fname ASC";
                    $result_users = $mysqli->query($query_users);
                    $cc_list = array();
                    $cc_str = '';
                    while ($obj_users = $result_users->fetch_object()) {
                        if($obj_users->id !== $_SESSION['user_id']){
                            echo '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                        }
                    }

            echo '</select>
                    <div class="notification-recipients" style="padding-left:0;"><ul>';
                    // display the saved recipients
                    foreach ($userId as $user) {
                        if($user != ""){
                            $query_users = "SELECT id, fname, lname, email FROM users WHERE id =".$user;
                            $result_users = $mysqli->query($query_users);
                            while ($obj_users = $result_users->fetch_object()) {
                                echo '<li class="user-'.$obj_users->id.'"><div class="remove removeUserRule" data-remove-id="first_name" alt="Remove" onclick="removeThisUser(\'.user-'.$obj_users->id.'\');"></div>'.$obj_users->fname.' '.$obj_users->lname.' - '.$obj_users->email.'<input type="hidden" id="recipients-list" name="recipients-list[]" value="'.$obj_users->id.'"></li>';
                            }
                        }
                    }
            echo '</ul></div>
                    <div class="chosen-recipients notification-recipients" style="padding-left:0;"><ul></ul></div>
                    </div></div>';
                    
            echo '<div class="row row-border"><label for="subsector" class="control-label">Default Assignee</label>
                    <div class="controls pull-left"><span class="required-mark">*</span>
                    <select id="default-assignee" name="default-assignee" data-placeholder="Choose a user..." style="width:50%;border:none !important;" class="required">
                    <option value="">Choose a user...</option>';
                     ////recipients
                    $query_users = "SELECT id, fname, lname, email FROM users WHERE role = 6 AND active = 1 ORDER BY fname ASC";
                    $result_users = $mysqli->query($query_users);
                    $cc_list = array();
                    $cc_str = '';
                    while ($obj_users = $result_users->fetch_object()) {
                            if($obj->default_assignee === $obj_users->id)
                                echo '<option value="'.$obj_users->id.'" selected=selected>'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                            else
                                echo '<option value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                    }
            echo '</select>
                    </div></div>';

            echo '<div class="row row-border"><label for="nickname" class="control-label">Nickname of rule</label>
                    <div class="controls pull-left"><span>'.$obj->nickname.'</span> 
                    </div></div>';
            echo '</div>'; //End Zebra
            echo '</div>';
        }

	} else {
		echo '<p><strong>Notification rule not found.</p>';
	}

	echo '<p><br /><a href="notification-rules" class="btn-orange back pull-left">Back to list</a>';
	if ($_SESSION['admin'] == 1) {
		echo '<input type="submit" class="btn-green submit submit-btn pull-right" data-analytics-label="Submit Form: Update Rule" value="Update Rule"></p></form></div>';
	} else {
		echo '</p></div>';
	}

} else if (isset($_GET['action']) && $_SESSION['admin'] == 1) {
    if (isset($_GET['message'])) {
            if ($_GET['message'] == 'exist') {
                echo '<p style="color:#ff0000;"><strong>The notification rule already exists.</strong><br /></p>';
            } 
    }
    //CREATING A NEW RULE
    echo '<div class="single-request single-request-resource">';

    echo '<form id="rules-update-form" action="includes/_notification-rules.php?action=create&default='.$default_rule.'" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="action" name="action" value="create">
        <input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">';

    echo '<div class="zebra">';
    echo '<div class="row row-border"><label>Request type</label> <div class="pull-left"><span class="required-mark">*</span>
            <select id="request_type" name="request_type" class="form-control required"><option value="">Select a request type</option>';
            $query_type = "SELECT * FROM request_type ORDER BY sort_order ASC";
            $result_type = $mysqli->query($query_type);
            while ($obj_type = $result_type->fetch_object()) {
                if($obj_type->request_type !== "Placeholder" && $obj_type->request_type !== 'Event')
                    echo '<option value="'.$obj_type->id.'">'.$obj_type->request_type.'</option>';
            }
    echo '</select></div></div>';
    if(!$default_rule){
        echo '<div class="row row-border"><label for="geo" class="control-label">Target GEO</label>
                <div class="controls pull-left"><span class="required-mark">*</span>
                    <label class="checkbox-inline first-checkbox" style="margin-right:13px;"> <input type="checkbox" id="selectall" name="selectall" value="" class="checkall"> World Wide</label>
                    <label class="checkbox-inline" style="margin-right:13px;"> <input type="checkbox" id="geo[]" name="geo[]" value="APJ" class="check-geo required"> APJ</label>
                    <label class="checkbox-inline" style="margin-right:13px;"> <input type="checkbox" id="geo[]" name="geo[]" value="EMEA" class="check-geo required"> EMEA</label>
                    <label class="checkbox-inline" style="margin-right:13px;"> <input type="checkbox" id="geo[]" name="geo[]" value="LAR" class="check-geo required"> LAR</label>
                    <label class="checkbox-inline" style="margin-right:13px;"> <input type="checkbox" id="geo[]" name="geo[]" value="NAR" class="check-geo required"> NAR</label>
                    <label class="checkbox-inline" style="margin-right:13px;"> <input type="checkbox" id="geo[]" name="geo[]" value="PRC" class="check-geo required"> PRC</label>
                </div></div>';
        echo '<div class="row row-border"><label for="sector" class="control-label">Sector</label>
                <div class="controls pull-left"><select id="sector" name="sector" class="form-control" val="sector-rules">
                        <option value="">Select One</option>
                    </select>
                </div></div>';
        echo '<div class="row row-border"><label for="subsector" class="control-label">Sub-Sector</label>
                <div class="controls pull-left"><select id="subsector" name="subsector" class="form-control" val="subsector-rules">
                        <option value="">Select Sector First</option>
                    </select>
                </div></div>';
    }
    echo '<div class="row row-border"><label for="subsector" class="control-label">Recipients</label>
            <div class="controls pull-left"><span class="required-mark">*</span>
            <select id="rules-recipients" name="rules-recipients[]" data-placeholder="Choose a user..." multiple style="width:50%;border:none !important;" class="form-control required">';
             ////recipients
            $query_users = "SELECT id, fname, lname, email FROM users WHERE active = 1 ORDER BY fname ASC";
            $result_users = $mysqli->query($query_users);
            $cc_list = array();
            $cc_str = '';
            while ($obj_users = $result_users->fetch_object()) {
                // if($obj_users->id !== $_SESSION['user_id']){
                    echo '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                // }
            }

    echo '</select>
            <div class="chosen-recipients notification-recipients" style="padding-left:0px;"><ul></ul></div>
            </div></div>';

    echo '<div class="row row-border"><label for="subsector" class="control-label">Default Assignee</label>
            <div class="controls pull-left"><span class="required-mark">*</span>
            <select id="default-assignee" name="default-assignee" data-placeholder="Choose a user..." style="width:50%;border:none !important;" class="form-control required">
            <option value="">Choose a user...</option>';
             ////recipients
            $query_users = "SELECT id, fname, lname, email FROM users WHERE role = 6 AND active = 1 ORDER BY fname ASC";
            $result_users = $mysqli->query($query_users);
            $cc_list = array();
            $cc_str = '';
            while ($obj_users = $result_users->fetch_object()) {
                    echo '<option val="'.$obj_users->email.'" value="'.$obj_users->id.'">'.$obj_users->fname.' '.$obj_users->lname.'</option>';
            }
    echo '</select>
            </div></div>';

    echo '<div class="row row-border"><label for="subsector" class="control-label">Nickname of rule</label>
            <div class="controls pull-left"><span class="required-mark">*</span><input type="text" id="nickname" name="nickname" value="" class="form-control required"> 
            </div></div>';
    echo '</div>'; //End Zebra
    echo '</div>';

    echo '<p><br /><a href="notification-rules" class="btn-orange back pull-left">Back to list</a>';
    echo '<input type="submit" class="btn-green submit submit-btn pull-right" data-analytics-label="Submit Form: Create Rule" value="Create Rule"></p></form></div>';

} else {

	$nquery = $query." WHERE notification_rules.active = 1 AND default_rule = 0 ORDER BY notification_rules.created_on ASC";
				
    $result = $mysqli->query($nquery);
    $row_cnt = $result->num_rows;

    // print_r($nquery);
    // die();

    if ($row_cnt > 0) {
    	echo '<br /><table class="table table-striped table-bordered table-hover tablesorter requests-table">';
    	echo '<thead><th>Create Date <b class="icon-white"></b></th><th>Nickname <b class="icon-white"></b></th><th>Request type <b class="icon-white"></b></th><th>Geo <b class="icon-white"></b></th><th>Sector <b class="icon-white"></b></th><th>Assigned To<b class="icon-white"></b></th><th>Created By <b class="icon-white"></b></th><th style=width:45px !important;></th></thead>';
    	echo '<tbody>';
        while ($obj = $result->fetch_object()) {
            $assigned_to = "";
            // get assigned to
            if($obj->default_assignee !== ""){
                $query_rec = "SELECT fname, lname FROM users WHERE id = $obj->default_assignee";

                $result_user = $mysqli->query($query_rec);
                while($obj_us = $result_user->fetch_object()){
                    $assigned_to = $obj_us->fname." ".$obj_us->lname;
                }
            }
        	echo '<tr>';
            echo '<td>'.$obj->created_on.'</td>';
            echo '<td>'.$obj->nickname.'</td>';
            echo '<td>'.$obj->request_name.'</td>';
            echo '<td>'.$obj->geo.'</td>';
            echo '<td>'.$obj->sector.'</td>';
            echo '<td>'.$assigned_to.'</td>';
            echo '<td>'.$obj->fname." ".$obj->lname.'</td>';
            echo '<td class="action-icons"><a href="notification-rules?id='.$obj->id.'" class="action-icon icon-pencil popover-hover" data-toggle="popover" data-content="Edit"></a><a href="" onclick="deleteResource(\'includes/_notification-rules.php?id='.$obj->id.'&action=delete\');return false;" class="action-icon icon-close popover-hover" data-toggle="popover" data-content="Delete"></a></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';

	} else {
		echo '<p><strong>There are currently no notification rules.</strong></p>';
	}

    /* DEFAULT RULES LIST */
    echo "<div class='row'><div class='col-md-12 default-rules-list'><div class='request-header'>";
    echo '<div class="header-btn"><a href="notification-rules?action=create&default=true" class="btn-green">Create a New Rule &nbsp;&nbsp;<i class="fa fa-plus"></i></a></div>';
    echo '<h2>Default Rules</h2><h4>Use these rules for default system-wide rules.</h4>';
    echo '</div>';
    $dquery =$query." WHERE notification_rules.active = 1 AND default_rule = 1 ORDER BY notification_rules.created_on ASC";
                
    $result = $mysqli->query($dquery);
    $row_cnt = $result->num_rows;

    // print_r($dquery);
    // die();

    if ($row_cnt > 0) {
        echo '<br /><table class="table table-striped table-bordered table-hover tablesorter requests-table">';
        echo '<thead><th>Create Date <b class="icon-white"></b></th><th>Request type <b class="icon-white"></b></th><th>Assigned To <b class="icon-white"></b></th><th>Recipients<b class="icon-white"></b></th><th>Created By <b class="icon-white"></b></th><th></th></thead>';
        echo '<tbody>';
        while ($obj = $result->fetch_object()) {
            $users = "";
            $assigned_to = "";
            // get recipients
            if($obj->notification_user_ids !== ""){
                $not_users = explode(",", $obj->notification_user_ids);
                $query_rec = "SELECT fname, lname FROM users WHERE id IN ($obj->notification_user_ids)";

                $result_user = $mysqli->query($query_rec);
                $i = 0;
                while($obj_us = $result_user->fetch_object()){
                    if($i === sizeof($not_users)-1)
                        $users .= $obj_us->fname." ".$obj_us->lname;
                    else
                        $users .= $obj_us->fname." ".$obj_us->lname.", ";
                    $i++;
                }
            }
            // get assigned to
            if($obj->default_assignee !== ""){
                $query_rec = "SELECT fname, lname FROM users WHERE id = $obj->default_assignee";

                $result_user = $mysqli->query($query_rec);
                while($obj_us = $result_user->fetch_object()){
                    $assigned_to = $obj_us->fname." ".$obj_us->lname;
                }
            }

            echo '<tr>';
            echo '<td>'.$obj->created_on.'</td>';
            echo '<td>'.$obj->request_name.'</td>';
            echo '<td>'.$assigned_to.'</td>';
            echo '<td>'.$users.'</td>';
            echo '<td>'.$obj->fname." ".$obj->lname.'</td>';
            echo '<td class="action-icons"><a href="notification-rules?id='.$obj->id.'&default=true" class="action-icon icon-pencil popover-hover" data-toggle="popover" data-content="Edit"></a><a href="" onclick="deleteResource(\'includes/_notification-rules.php?id='.$obj->id.'&action=delete\');return false;" class="action-icon icon-close popover-hover" data-toggle="popover" data-content="Delete"></a></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';

    } else {
        echo '<p><strong>There are currently no default rules.</strong></p>';
    }
    echo "</div></div>";
}

?>