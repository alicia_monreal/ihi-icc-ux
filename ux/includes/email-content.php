<?php 
$loc = 'templates';
$step = 4;
$c_type = 'webinar';
$t_type = 'custom';
$tactic_type = "webinar";
?>
<div id='email-content' class="modal fade">
  <div class="modal-dialog" style="width:98%;">
    <div class="modal-content">
        <div class="modal-header modal-tabs" style="border-bottom:none;">
          <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
          <div style="margin-top:35px;">
            <label id="requestLabel" class="request-form-label" style="margin-bottom:0;font-family: 'IntelClearLight', sans-serif; font-weight: normal; color: rgb(0,121,192);font-size:28px; float:left; ">Asset configuration</label>

            <div class="template-type-links pull-right">
              <a href="" class="btn-green custom-html">Provide Custom HTML</a>
            </div>

          </div>
        </div>

        <div class="modal-body" style="padding-bottom:90px;padding-top:40px;">
         <div role="tabpanel" style="border: 2px solid #dbdbdb;">
          <form id="custom-template-form" class="form-horizontal" action="includes/_requests_<?php echo $tactic_type; ?>.php" method="POST" enctype="multipart/form-data">
            <input type="hidden" id="template" name="template" value="custom">
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
            <input type="hidden" id="step" name="step" value="4">
            <input type="hidden" id="c_type" name="c_type" value="<?php echo $_GET['type']; ?>">
            <input type="hidden" id="request_id" name="request_id" value="">
            <input type="hidden" id="action" name="action" value="savetemplate">

            <!-- Assets list -->
            <div class="asset-list">
              <a href="#global-elements"><div class="slide bx-clone active-asset">Global Elements</div></a>
              <?php if ($tactic_name == "webinar") { ?>
                <a href="#invitation-email"><div class="slide">Invitation Email</div></a>
                <a href="#reminder-email-one"><div class="slide">Reminder Email 1</div></a>
                <a href="#reminder-email-two"><div class="slide">Reminder Email 2</div></a>
                <a href="#confirmation-email"><div class="slide">Confirmation Email</div></a>
                <a href="#thank-you-email"><div class="slide">Thank You Email</div></a>
                <a href="#sorry-we-missed-you-email"><div class="slide">Sorry We Missed You Email</div></a>
                <a href="#registration-page"><div class="slide">Registration Page</div></a>
                <a href="#confirmation-page"><div class="slide">Confirmation Page</div></a>
              <?php } ?>
            </div>

            <?php if ($tactic_name == "newsletter" || $tactic_name == "enurture") { ?>
              <p class="add-asset-link"><a href="#">Add a Landing Page +</a> &nbsp;&nbsp;<a href="#">Add an asset +</a></p>
            <?php } ?>
            <!-- End assets list -->

           <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="intel-com">
              <div id="tpl-body" class="span9 tpl-editor">

                <div class="preview-mode hide">
                    <div class="template-preview-select">Preview Mode</div>
                </div>

                <div id="template-previews">
                  <!-- Global Elements -->
                  <div id="global-elements" data-attr="global_elements_config" class="template-block template-email">
                      <?php include('email_templates/package/global-elements.php'); ?>
                  </div>

                  <?php if ($tactic_name == "webinar") { ?>
                    <!-- Webinar -->
                    <div id="invitation-email" data-attr="invitation_email_config" class="template-block template-email hide ">
                        <?php include('email_templates/package/email-webinar.php'); ?>
                    </div>

                    <div id="reminder-email-one" data-attr="reminder_one_email_config" class="template-block template-email hide">
                        <?php include('email_templates/package/email-webinar.php'); ?>
                    </div>

                    <div id="reminder-email-two" data-attr="reminder_two_email_config" class="template-block template-email hide">
                        <?php include('email_templates/package/email-webinar.php'); ?>
                    </div>

                    <div id="confirmation-email" data-attr="confirmation_email_config" class="template-block template-email hide">
                        <?php include('email_templates/package/email-webinar.php'); ?>
                    </div>

                    <div id="thank-you-email"  data-attr="thank_you_email_config" class="template-block template-email hide">
                        <?php include('email_templates/package/email-webinar.php'); ?>
                    </div>

                    <div id="sorry-we-missed-you-email" data-attr="sorry_email_config" class="template-block template-email hide">
                        <?php include('email_templates/package/email-webinar.php'); ?>
                    </div>

                    <div id="registration-page" data-attr="registration_config" class="template-block template-page hide">
                        <?php include('email_templates/package/registrationpage-webinar.php'); ?>
                    </div>

                    <div id="confirmation-page" data-attr="confirmation_config" class="template-block template-page hide">
                        <?php include('email_templates/package/confirmationpage-webinar.php'); ?>
                    </div>
                    <!-- /Webinar -->
                    <?php } ?>


                    <?php if ($tactic_type == 'email') { ?>
                    <!-- Single Email -->
                    <div id="single-email" data-attr="single_email_config" class="template-block template-email">
                        <?php include('email_templates/package/email-single-email.php'); ?>
                    </div>
                    <!-- /Single Email -->
                    <?php } ?>


                    <?php if ($tactic_type == 'landingpage') { ?>
                    <!-- Landing Page -->

                    <div id="landing-page" data-attr="landing_page_config" class="template-block template-page">
                        <?php include('email_templates/package/landingpage-landingpage.php'); ?>
                    </div>

                    <div id="confirmation-page" data-attr="confirmation_config" class="template-block template-page hide">
                        <?php include('email_templates/package/confirmationpage-landingpage.php'); ?>
                    </div>
                    <!-- /Landing Page -->
                    <?php } ?>

                    <!-- enurture / newsletter -->
                    <?php if ($tactic_type == 'enurture' || $tactic_type == 'newsletter') { ?>

                        <!-- default templates kept here for enurture / newsletter -->
                        <?php
                             $email_count = 0; // initialize to first element
                             $default_template = 'single-email';
                             do { ?>
                                <div id="<?php echo $tactic_type; ?>-email-<?php echo $email_count; ?>" data-attr="<?php echo $tactic_type; ?>_email_config_<?php echo $email_count; ?>" class="template-block template-email <?php if($email_count!=0) echo 'hide'; ?>">
                                    <?php
                                     if($tactic_type == "newsletter") // if newsletter then load different template
                                        $default_template = "newsletter"; 

                                        include('email_templates/package/email-'.$default_template.'.php'); 
                                    ?>
                                </div>
                            <?php 
                             $email_count++;
                            } while($email_count!=$multiple_email_count); ?>

                            <?php
                             $landingpage_count = 0; // initialize to first element
                             do { ?>
                                <div id="<?php echo $tactic_type; ?>-landingpage-<?php echo $landingpage_count; ?>" data-attr="<?php echo $tactic_type; ?>_landingpage_config_<?php echo $landingpage_count; ?>" class="template-block template-page hide">
                                    <?php include('email_templates/package/landingpage-landingpage.php'); ?>
                                </div>
                            <?php 
                             $landingpage_count++;
                             } while($landingpage_count!=$multiple_landingpage_count); ?>

                    <?php } ?>
                </div>
              </div>
            </div>
          </div>
      </form>
      </div>
    </div>
    <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
      <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="template-save-exit" class="btn-blue" style="font-size: 16px !important;" data-analytics-label="Submit Form: Campaign ID Request: Step 2" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0);" id="submit-custom-temp-form" class="btn-green submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 3" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->