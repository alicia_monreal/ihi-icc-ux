<div id='leadlist-setup' class="modal fade">
  <div class="modal-dialog icc-modal-dialog">
    <div class="modal-content" style="padding-left:5%;padding-right:5%;">
            <div class="modal-header modal-tabs">
              <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
              <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Upload List</h2>
            </div>

        <div class="modal-body" style="padding-top:30px;margin: 0 4% 0 4%;">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="details">
                  
                  <form id="upload-list-form" class="content-update-form form-horizontal">
                    <input type="hidden" id="action" name="action" value="create">
                    <input type="hidden" id="step" name="step" value="2">
                    <input type="hidden" id="c_type" name="c_type" value="webinar">
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                    <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
                    <input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
                  <div id='list-fields'>
                    <div class="form-group col-md-12 col-sm-12">
                      <label for='list-tag' class="col-md-4 col-sm-4">List Name</label>
                      <div class="col-md-8 col-sm-8">
                        <input type="text" name='tag' class="form-control required" id="tag" placeholder="Enter name *" value="" maxlength="120">                        
                      </div>
                    </div>

                    <div class="form-group col-md-12 col-sm-12">
                      <label for='list-request-type' class="col-md-4 col-sm-4">List Request Type</label>
                      <div class="col-md-8 col-sm-8">
                        <select id="list_request_type" name="list_request_type" class="form-control required">
                          <option value="">Select one *</option>
                          <option value="Contact"<?php if (isset($_SESSION['list_request_type']) && $_SESSION['list_request_type'] == 'Contact') { echo ' selected'; } ?>>Contact</option>
                          <option value="Lead"<?php if ( isset($_SESSION['list_request_type']) && $_SESSION['list_request_type'] == 'Lead') { echo ' selected'; } ?>>Lead</option>
                          <option value="ProspectAssessment"<?php if ( isset($_SESSION['list_request_type']) && $_SESSION['list_request_type'] == 'ProspectAssessment') { echo ' selected'; } ?>>Prospect Assessment</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group col-md-12 col-sm-12">
                      <label for="raw-list" class="col-md-4 col-sm-4">Is this a RAW list?</label>
                      <div class="col-md-8 col-sm-8">
                        <label class="radio inline modal-label col-md-2 col-sm-2"> <input type="radio" id="list_raw[]" name="list_raw" value="1" <?php if (getFieldValue('list_raw', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('list_raw', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '1') { echo 'checked'; } ?>> Yes</label>
                        <label class="radio inline modal-label col-md-2 col-sm-2"> <input type="radio" id="list_raw[]" name="list_raw" value="0" <?php if (getFieldValue('list_raw', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '0') { echo 'checked'; } ?>> No</label><br />
                      </div>
                    </div>

                    <div class="form-group col-md-12 col-sm-12">
                       <label class="col-md-4 col-sm-4">List Upload</label>
                      <div class='col-md-8 col-sm-8'>
                        <div class="upload-div col-md-7 col-sm-7" style="margin-left:-5%;">
                          <div id="contacts" class="fileupload fileupload-new" data-provides="fileupload" style="">
                            <span class="btn-file btn-green iconned-btn"><span class="fileupload-new">Upload a List<span class="icon-plus-media"><i class='fa fa-plus'></i></span></span><input id="fileupload_contacts" type="file" name="files_contacts[]" class=""></span>
                          </div>
                          <div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                              <div class="progress-bar bar"></div>
                          </div>
                          <div id="errors-contacts" class="files" style="clear:both;color:#cc0000;"></div>
                          <div id="shown-contacts" class="files" style="clear:both;"></div>
                        </div>
                        <span class="col-md-5 col-sm-5" style='font-size:12px; padding:0; text-align:left;' for='placeholder-btn'>All the data that you upload will be purged from this portal 30 days after the upload per Intel's privacy guidelines</span>
                        <div class="box_file_link col-md-8 col-sm-8" style="padding:0px; margin-top: 15px;">                    
                          <div class="col-md-6 col-sm-6" style="padding: 10px 0px;">
                            <p style="margin:0px"> Based on your selection, we recommend this template: </p>
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <!--File icom--> <div class="icon-file"></div>
                            <!--File name (link)--> <div class="file_name"></div>
                            <!--File arrow--> <div class="icon-arrow"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
        </div>
        <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
          <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="upload-list-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Upload a List: Step 2" val="<?php echo $tactic_name ?>">Save and Exit</a>
        </div>
        <!-- <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
             <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="upload-list-setup-save-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0)" id="upload-list-setup-save" class="btn-green" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
        </div> -->
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- End - Campaign Setup Modal -->