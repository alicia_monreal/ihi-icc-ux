<?php
echo '<h1>Thank you.</h1><p>The details of the request would follow...</p>';
exit();
?>

<?php

/*

if (isset($_GET['id'])) {
    //SHOWING SINGLE RECORD

    if ($_SESSION['admin'] == 1) { //Show requests even if not active
        $query.=" WHERE req.id = '".$_GET['id']."' LIMIT 1";
    } else {
        $query.=" WHERE req.id = '".$_GET['id']."' AND req.user_id = '".$_SESSION['user_id']."' AND req.active = 1 LIMIT 1";
    }
                
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    //print_r($query);

    if ($row_cnt > 0) {

        //Show a message?
        if (isset($_GET['update'])) {
            if ($_GET['update'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>Request has been updated.</strong><br /></p>';
            } else if ($_GET['update'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>We\'re sorry, there has been a problem updating this request.</strong><br /></p>';
            }
        }
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>Your request has been submitted.</strong><br /></p>';
            }
        }

        while ($obj = $result->fetch_object()) {
            if (!$edit) { echo '<div class="pull-right"><a href="request-detail-enurture?id='.$_GET['id'].'&edit=1" class="btn-green download">Make changes</a></div>'; }
            echo '<h1>Request: '.$obj->campaign_name.' (#'.$_GET['id'].')</h1>';
            echo '<div class="single-request">';

            //Admins get a form
            if ($_SESSION['admin'] == 1 || $edit) {
                echo '<form id="content-update-form" action="includes/_requests_enurture.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="action" name="action" value="update">';
                    if ($edit) { echo '<input type="hidden" id="edit_form" name="edit_form" value="true">'; }
                    echo '<input type="hidden" id="user_id" name="user_id" value="'.$obj->user_id.'">
                    <input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">
                    <input type="hidden" id="request_id" name="request_id" value="'.$_GET['id'].'">';

                $query_email = "SELECT fname, lname, email FROM users WHERE id = '".$obj->user_id."' LIMIT 1";
                $result_email = $mysqli->query($query_email);
                while ($obj_e = $result_email->fetch_object()) {
                    //echo '<input type="hidden" id="user_name" name="user_name" value="'.$obj_e->fname.' '.$obj_e->lname.'">'; //Don't need yet, but maybe later
                    echo '<input type="hidden" id="user_email" name="user_email" value="'.$obj_e->email.'">';
                }
            }

            //Grab speakers
            $speakers = '';
            $query_speakers = "SELECT * FROM speakers WHERE request_id = '".$obj->id."' AND active = 1 ORDER BY id ASC";
            $result_speakers = $mysqli->query($query_speakers);
            $row_cnt_speakers = $result_speakers->num_rows;
            $speaker_cnt = 1;
            if ($row_cnt_speakers > 0) {
                while ($obj_speakers = $result_speakers->fetch_object()) {
                    if ($edit) {
                        $speakers.='<input type="hidden" id="speaker_id_'.$speaker_cnt.'" name="speaker_id_'.$speaker_cnt.'" value="'.$obj_speakers->id.'">
                                        <input type="text" id="speaker_name_'.$speaker_cnt.'" name="speaker_name_'.$speaker_cnt.'" class="input-wide required" placeholder="Name (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_name.'"><br />
                                        <input type="text" id="speaker_email_'.$speaker_cnt.'" name="speaker_email_'.$speaker_cnt.'" class="input-wide" placeholder="Email" value="'.$obj_speakers->speaker_email.'"><br />
                                        <label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$speaker_cnt.'[]" name="speaker_type_'.$speaker_cnt.'[]" value="Speaker"'; if (strpos($obj_speakers->speaker_type, 'Speaker') !== false) { $speakers.='checked'; } $speakers.='> Speaker</label>
                                        <label class="checkbox inline"> <input type="checkbox" id="speaker_type_'.$speaker_cnt.'[]" name="speaker_type_'.$speaker_cnt.'[]" value="Moderator"'; if (strpos($obj_speakers->speaker_type, 'Moderator') !== false) { $speakers.='checked'; } $speakers.='> Moderator</label>
                                        <div class="bios-extra" style="display:block;">
                                            <input type="text" id="speaker_title_'.$speaker_cnt.'" name="speaker_title_'.$speaker_cnt.'" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_title.'"><br />
                                            <input type="text" id="speaker_company_'.$speaker_cnt.'" name="speaker_company_'.$speaker_cnt.'" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value="'.$obj_speakers->speaker_company.'"><br />
                                            <textarea id="speaker_bio_'.$speaker_cnt.'" name="speaker_bio_'.$speaker_cnt.'" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048">'.$obj_speakers->speaker_bio.'</textarea><br />
                                            <div class="upload-div">
                                                <div id="photo_'.$speaker_cnt.'" class="fileupload fileupload-new" data-provides="fileupload">
                                                    <span class="btn-file btn-green add"><span class="fileupload-new">Add/Replace Photo</span><input id="fileupload_photo_'.$speaker_cnt.'" class="fileupload_photo" data-number="1" type="file" name="files[]"></span>
                                                </div>
                                                <div id="progress-photo_'.$speaker_cnt.'" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 190px;display:none;">
                                                    <div class="bar"></div>
                                                </div>
                                                <div id="shown-photo_'.$speaker_cnt.'" class="files" style="clear:both;"> </div>
                                            </div>
                                        </div><label class="checkbox inline" style="color:#cc0000;"> <input type="checkbox" id="speaker_delete_'.$speaker_cnt.'" name="speaker_delete_'.$speaker_cnt.'" value="1"> Remove Speaker?</label><br /><hr>';
                        $speaker_cnt++;
                    } else {
                        $speakers.='<p class="speaker-list"'; if ($row_cnt_speakers > 1) { $speakers.=' style="padding-bottom:20px;"'; } $speakers.='><strong>'.$obj_speakers->speaker_name.'</strong>';
                        if ($obj_speakers->speaker_type != '') {
                            $speakers.=' (<em>'.$obj_speakers->speaker_type.'</em>)';
                        }
                        if ($obj_speakers->speaker_title != '') {
                            $speakers.='<br />'.$obj_speakers->speaker_title;
                        }
                        if ($obj_speakers->speaker_company != '') {
                            $speakers.='<br />'.$obj_speakers->speaker_company;
                        }
                        if ($obj_speakers->speaker_email != '') {
                            $speakers.='<br /><a href="mailto:'.$obj_speakers->speaker_email.'">'.$obj_speakers->speaker_email.'</a>';
                        }
                        if ($obj_speakers->speaker_bio != '') {
                            $speakers.='<br /><br />'.$obj_speakers->speaker_bio;
                        }
                        if (strlen($obj_speakers->speaker_photo) > 22) {
                            $speakers.='<br /><br /><img src="'.$obj_speakers->speaker_photo.'">';
                        }
                        $speakers.='</p><hr>';
                    }
                }

                //Add blank fields for new speaker
                if ($edit && $row_cnt_speakers < 10) {
                    $speakers.='<label style="width:auto;">Add another Speaker/Moderator</label><br /><input type="hidden" id="speaker_id_10" name="speaker_id_10" value="new">
                                    <input type="text" id="speaker_name_10" name="speaker_name_10" class="input-wide" placeholder="Name (120 characters max)" maxlength="120" value=""><br />
                                    <input type="text" id="speaker_email_10" name="speaker_email_10" class="input-wide" placeholder="Email" value=""><br />
                                    <label class="checkbox inline"> <input type="checkbox" id="speaker_type_10[]" name="speaker_type_10[]" value="Speaker"> Speaker</label>
                                    <label class="checkbox inline"> <input type="checkbox" id="speaker_type_10[]" name="speaker_type_10[]" value="Moderator"> Moderator</label>
                                    <div class="bios-extra" style="display:block;">
                                        <input type="text" id="speaker_title_10" name="speaker_title_10" class="input-wide" placeholder="Title (120 characters max)" maxlength="120" value=""><br />
                                        <input type="text" id="speaker_company_10" name="speaker_company_10" class="input-wide" placeholder="Company (120 characters max)" maxlength="120" value=""><br />
                                        <textarea id="speaker_bio_10" name="speaker_bio_10" class="input-wide" placeholder="Enter bio (2048 characters max)" maxlength="2048"></textarea><br />
                                        <div class="upload-div">
                                            <div id="photo_1" class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn-file btn-green add"><span class="fileupload-new">Add Photo</span><input id="fileupload_photo_10" class="fileupload_photo" data-number="1" type="file" name="files[]"></span>
                                            </div>
                                            <div id="progress-photo_10" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 190px;display:none;">
                                                <div class="bar"></div>
                                            </div>
                                            <div id="shown-photo_10" class="files" style="clear:both;"> </div>
                                        </div>
                                    </div><br />';
                }
            }

            //Grab assets
            $assets = '';
            //Editable? We can delete them
            if ($edit) {
                $assets.='<label class="checkbox inline" style="padding-top:0;padding-left:0;color:#cc0000;">Remove?</label>';
            }
            $assets.='<ul class="assets">';
            $query_assets = "SELECT id, name, path, type, DATE_FORMAT(date_created, '%m/%d/%Y %l:%i%p') as date_created_assets FROM assets WHERE request_id = '".$obj->id."' AND active = 1 ORDER BY date_created ASC";
            $result_assets = $mysqli->query($query_assets);
            $row_cnt_assets = $result_assets->num_rows;
            if ($row_cnt_assets > 0) {
                while ($obj_assets = $result_assets->fetch_object()) {
                    if ($obj_assets->name != '') {
                        $assets.='<li>';
                        if ($edit) {
                            $assets.='<input type="checkbox" id="delete_asset[]" name="delete_asset[]" value="'.$obj_assets->id.'" style="margin:0 16px 0 0;">';
                        }
                        $assets.='<a href="'.$obj_assets->path.'" target="_blank">'.$obj_assets->name.'</a>';
                    } else {
                        $assets.='<li>';
                        if ($edit) {
                            $assets.='<input type="checkbox" id="delete_asset[]" name="delete_asset[]" value="'.$obj_assets->id.'" style="margin:0 16px 0 0;">';
                        }
                        $assets.='<li><a href="'.$obj_assets->path.'" target="_blank">'.$obj_assets->path.'</a>';
                    }
                    if ($obj_assets->type == 'Presentation') { $assets.=' (Presentation)'; }
                    if ($obj_assets->type == 'Media') { $assets.=' (Media File)'; }
                    if ($obj_assets->type == 'Survey') { $assets.=' (Survey File)'; }
                    if ($obj_assets->type == 'Resource') { $assets.=' (Resource)'; }
                    if ($obj_assets->type == 'Contacts') {
                        $assets.=' (Contact List - The contacts on this list have opted in for marketing contact: ';
                        if ($obj->contactlist_optin_url == 1) {
                            $assets.='Yes';
                        } else {
                            $assets.='No';
                        }
                        $assets.=')';
                    }
                    if ($obj_assets->type == 'Briefs') { $assets.=' (Campaign Brief)'; }
                    if ($obj_assets->type == 'Regpage') { $assets.=' (Registration Page Image)'; }
                    if ($obj_assets->type == 'Confpage') { $assets.=' (Confirmation Page Image)'; }
                    if ($obj_assets->type == 'Logo') { $assets.=' (Logo)'; }
                    $assets.=' <span style="font-style:italic;color:#999;">'.$obj_assets->date_created_assets.'</span></li>';
                }
            } else {
                $assets.='<li>No media files uploaded</li>';
            }
            $assets.='</ul>';

            //Grab notes
            $notes = '<ul class="notes">';
            $query_notes = "SELECT notes, DATE_FORMAT(date_created, '%m/%d/%Y %l:%i%p') AS date_created_notes FROM notes WHERE request_id = '".$obj->id."' AND active = 1 ORDER BY date_created DESC";
            $result_notes = $mysqli->query($query_notes);
            $row_cnt_notes = $result_notes->num_rows;
            if ($row_cnt_notes > 0) {
                while ($obj_n = $result_notes->fetch_object()) {
                    $notes.='<li>- '.$obj_n->notes.' | '.$obj_n->date_created_notes.'</li>';
                }
            } else {
                $notes.='<li>No notes</li>';
            }
            $notes.='</ul>';

            //Grab user data for admin
            if ($_SESSION['admin'] == 1) {
                $query_user = "SELECT * FROM users WHERE id = '".$obj->user_id."' LIMIT 1";
                $result_user = $mysqli->query($query_user);
                $row_cnt_user = $result_user->num_rows;
                if ($row_cnt_user > 0) {
                    while ($obj_u = $result_user->fetch_object()) {
                        echo '<div class="row odd"><label>Requestor Name</label> <div class="pull-left">'.$obj_u->fname.' '.$obj_u->lname.'</div></div>';
                        echo '<div class="row even"><label>Requestor Email</label> <div class="pull-left"><a href="mailto:'.$obj_u->email.'">'.$obj_u->email.'</a></div></div>';
                    }
                }
            }
            echo '<div class="row odd"><label>Date Created</label> <div class="pull-left">'.$obj->date_created_request.'</div></div>';
            echo '<div class="row even"><label>Request Type</label> <div class="pull-left">';
            //Admin can change request type
            //NOT DOING THIS YET
            /*
            if ($_SESSION['admin'] == 1) {
                echo '<select id="request_type_update" name="request_type_update">
                            <option value="Urgent update"'; if ($obj->request_type == 'Urgent update') { echo ' selected'; } echo '>Urgent update</option>
                            <option value="Content refresh"'; if ($obj->request_type == 'Content refresh') { echo ' selected'; } echo '>Content refresh</option>
                            <option value="Substantive change/new pages"'; if ($obj->request_type == 'Substantive change/new pages') { echo ' selected'; } echo '>Substantive change/new pages</option>
                            <option value="Other"'; if ($obj->request_type == 'Other') { echo ' selected'; } echo '>Other</option>
                        </select>';
            } else {
                echo $obj->request_type;
            }
            */
            /*
            echo ucwords($obj->request_type);
            echo '</div></div>';
            
            echo '<div class="row odd"><label>Status</label> <div class="pull-left">';
            //Admin can change status
            if ($_SESSION['admin'] == 1) {
                $query_status = "SELECT * FROM status ORDER BY sort_order ASC";
                $result_status = $mysqli->query($query_status);
                echo '<select id="status_update" name="status_update">';
                while ($obj_s = $result_status->fetch_object()) {
                    echo '<option value="'.$obj_s->id.'"';
                    if ($obj->status == $obj_s->id) {
                        echo ' selected="selected"';
                    }
                    echo '>'.$obj_s->value.'</option>';
                }
                echo '</select>';
            } else {
                echo '<input type="hidden" id="status_update" name="status_update" value="'.$obj->status.'">'; //For new Edit
                echo $obj->status_value;
            }
            echo '</div></div>';
            echo '<h1><br />'.ucwords($obj->request_type).' Details</h1>';

            //Array's
            $geo_array = explode(', ', $obj->geo);
            $topics_array = explode(', ', $obj->topics_covered);

            //Editable fields
            if ($edit) {

                echo '<div class="row odd"><label>Webinar Title</label> <div class="pull-left"><input type="text" id="webinar_title" name="webinar_title"  class="input-wide required" value="'.$obj->webinar_title.'"></div></div>';
                echo '<div class="row even"><label>Webinar Owner</label> <div class="pull-left"><input type="text" id="webinar_owner" name="webinar_owner"  class="input-wide required" value="'.$obj->webinar_owner.'"></div></div>';
                echo '<div class="row odd"><label>Webinar Date</label> <div class="pull-left">'.$obj->webinar_date.'</div></div>';
                //echo '<div class="row odd"><label>Webinar Date</label> <div class="pull-left"><div id="datetimepicker" class="input-append date" style=""><input type="text" id="webinar_date" name="webinar_date" class="input-medium required" value="'.$obj->webinar_date.'"><span class="add-on"><i class="icon-th"></i></span></div></div></div>';
                echo '<div class="row even"><label>Webinar Time</label> <div class="pull-left"><input type="text" id="time" name="time"  class="input-wide required" value="'.$obj->time.'" style="width: 76px;margin-right: 2px;"> <select id="time_zone" name="time_zone" class="input-medium required"><option value="">Select One</option>';
                                reset($timezones_array);
                                foreach ($timezones_array as $value_tz) {
                                    echo '<option value="'.$value_tz.'"'; if ($obj->time_zone == $value_tz) { echo ' selected'; } echo '>'.$value_tz.'</option>';
                                }
                                echo '</select></div></div>';
                echo '<div class="row odd"><label>Length</label> <div class="pull-left"><select id="length" name="length" class="input-medium">
                                <option value="">Select One</option>
                                <option value="60 minutes"'; if ($obj->length == '60 minutes') { echo ' selected'; } echo '>60 minutes</option>
                                <option value="90 minutes"'; if ($obj->length == '90 minutes') { echo ' selected'; } echo '>90 minutes</option>
                                <option value="2 hours"'; if ($obj->length == '2 hours') { echo ' selected'; } echo '>2 hours</option>
                                <option value="2+ hours"'; if ($obj->length == '2+ hours') { echo ' selected'; } echo '>2+ hours</option>
                            </select></div></div>';
                echo '<div class="row even"><label>Expected Attendees</label> <div class="pull-left"><select id="expected_attendees" name="expected_attendees" class="input-medium">
                                <option value="">Select One</option>
                                <option value="50 - 100"'; if ($obj->expected_attendees == '50 - 100') { echo ' selected'; } echo '>50 - 100</option>
                                <option value="100 - 200"'; if ($obj->expected_attendees == '100 - 200') { echo ' selected'; } echo '>100 - 200</option>
                                <option value="200+"'; if ($obj->expected_attendees == '200+') { echo ' selected'; } echo '>200+</option>
                            </select></div></div>';
                echo '<div class="row odd"><label>Target GEO</label> <div class="pull-left"><div class="controls"><label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="geo[]" name="geo[]" value="APJ"'; if (in_array('APJ', $geo_array)) { echo ' checked'; } echo '> APJ</label>
                                <label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="geo[]" name="geo[]" value="EMEA"'; if (in_array('EMEA', $geo_array)) { echo ' checked'; } echo '> EMEA</label>
                                <label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="geo[]" name="geo[]" value="LAR"'; if (in_array('LAR', $geo_array)) { echo ' checked'; } echo '> LAR</label>
                                <label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="geo[]" name="geo[]" value="NAR"'; if (in_array('NAR', $geo_array)) { echo ' checked'; } echo '> NAR</label>
                                <label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="geo[]" name="geo[]" value="PRC"'; if (in_array('PRC', $geo_array)) { echo ' checked'; } echo '> PRC</label></div></div></div>';
                echo '<div class="row even"><label>Sector</label> <div class="pull-left"><select id="sector" name="sector" class="input-wide required">
                    <option value="">Sector</option>
                    <option value="Embedded"'; if ($obj->sector == 'Embedded') { echo ' selected'; } echo '>Embedded</option>
                    <option value="ITDM"'; if ($obj->sector == 'ITDM') { echo ' selected'; } echo '>ITDM</option>
                    <option value="Channel"'; if ($obj->sector == 'Channel') { echo ' selected'; } echo '>Channel</option>
                </select><br/>
                <select id="subsector" name="subsector" class="input-wide">
                    <option value="">Sub-Sector</option>
                    <option value="ITDM-ESS"'; if ($obj->subsector == 'ITDM-ESS') { echo ' selected'; } echo '>ITDM-ESS</option>
                    <option value="ITDM-WA"'; if ($obj->subsector == 'ITDM-WA') { echo ' selected'; } echo '>ITDM-WA</option>
                    <option value="Retail Resellers"'; if ($obj->subsector == 'Retail Resellers') { echo ' selected'; } echo '>Retail Resellers</option>
                    <option value="Retail Ecosystem"'; if ($obj->subsector == 'Retail Ecosystem') { echo ' selected'; } echo '>Retail Ecosystem</option>
                    <option value="Other"'; if ($obj->subsector == 'Other') { echo ' selected'; } echo '>Other</option>
                </select><br /><input type="text" id="subsector_other" name="subsector_other" class="input-wide" placeholder="If other, please enter the other sub-sector" value="'.$obj->subsector_other.'" style="clear:both;margin-top:10px;"'; if (!isset($obj->subsector_other) || $obj->subsector_other != 'Other') { echo 'disabled'; } echo '></div></div>';
                echo '<div class="row odd"><label>Webinar Initial Invitation Date</label> <div class="pull-left"><div id="webinar_initial_date_picker" class="input-append date" style=""><input type="text" id="webinar_initial_date" name="webinar_initial_date" class="input-medium required" value="'.$obj->webinar_initial_date.'"><span class="add-on"><i class="icon-th"></i></span></div></div></div>';
                echo '<div class="row even"><label>Webinar Reminder 1 Date</label> <div class="pull-left"><div id="webinar_reminder_1_date_picker" class="input-append date" style=""><input type="text" id="webinar_reminder_1_date" name="webinar_reminder_1_date" class="input-medium required" value="'.$obj->webinar_reminder_1_date.'"><span class="add-on"><i class="icon-th"></i></span></div></div></div>';
                echo '<div class="row odd"><label>Webinar Reminder 2 Date</label> <div class="pull-left"><div id="webinar_reminder_2_date_picker" class="input-append date" style=""><input type="text" id="webinar_reminder_2_date" name="webinar_reminder_2_date" class="input-medium" value="'.$obj->webinar_reminder_2_date.'"><span class="add-on"><i class="icon-th"></i></span></div></div></div>';
                echo '<div class="row even"><label>Topics Covered</label> <div class="pull-left">
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Big Data"'; if (in_array('Big Data', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Big Data</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Cloud Computing"'; if (in_array('Cloud Computing', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Cloud Computing</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Consumerization of IT"'; if (in_array('Consumerization of IT', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Consumerization of IT</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Data Center Efficiency"'; if (in_array('Data Center Efficiency', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Data Center Efficiency</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Desktop Virtualization"'; if (in_array('Desktop Virtualization', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Desktop Virtualization</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Embedded"'; if (in_array('Embedded', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Embedded</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="High Performance Computing"'; if (in_array('High Performance Computing', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> High Performance Computing</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Internet of Things"'; if (in_array('Internet of Things', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Internet of Things</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Manageability"'; if (in_array('Manageability', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Manageability</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Mobility"'; if (in_array('Mobility', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Mobility</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Security"'; if (in_array('Security', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Security</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Virtualization"'; if (in_array('Virtualization', $topics_array)) { echo ' checked'; } echo ' class="check-topics"> Virtualization</label><br />
                            <label class="checkbox inline"> <input type="checkbox" id="topics_covered[]" name="topics_covered[]" value="Other"'; if (in_array('Other', $topics_array)) { echo ' checked'; } echo ' class="check-topics other-topic"> Other</label><br />
                            <input type="text" id="topics_covered_other" name="topics_covered_other" class="input-wide" placeholder="If Other, please enter topic(s) here" value="'.$obj->topics_covered_other.'" style="width:456px;clear:both;margin-top:10px;"'; if (empty($topics_array) || !in_array('Other', $topics_array)) { echo 'disabled'; } echo '>
                        </div></div>';
                echo '<div class="row odd"><label>Description</label> <div class="pull-left" style="width:690px;"><textarea id="description" name="description" class="input-wide textarea-medium required" placeholder="Up to 200 words">'.$obj->description.'</textarea></div></div>';
                //echo '<div class="row odd"><label>Content Type</label> <div class="pull-left">'.$obj->content_type.'</div></div>';
                echo '<div class="row even"><label>Series Based?</label> <div class="pull-left"><label class="radio inline"> <input type="radio" id="series_based[]" name="series_based" value="Yes"'; if ($obj->series_based == 'Yes' || $obj->series_based_name != '') { echo ' checked'; } echo '> Yes</label> <label class="radio inline"> <input type="radio" id="series_based[]" name="series_based" value="No"'; if ($obj->series_based == 'No') { echo ' checked'; } echo '> No</label><br /><input type="text" id="series_based_name" name="series_based_name" class="input-wide" placeholder="If yes, please enter the series name" value="'.$obj->series_based_name.'" style="clear:both;margin-top:10px;"'; if ($obj->series_based == 'No') { echo ' disabled'; } echo '></div></div>';
                echo '<div class="row odd"><label>Speakers &amp; Moderators</label> <div class="pull-left">'.$speakers.'</div></div>';
                echo '<div class="row even"><label>URL</label> <div class="pull-left"><label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="url" name="url" value="1"'; if ($obj->url == 1) { echo 'checked'; } echo '> Enable</label><br /><input type="text" id="url_value" name="url_value" class="input-wide" placeholder="Enter URL" value="'.$obj->url_value.'" style="margin-top:10px;"></div></div>';
                echo '<div class="row odd"><label>Survey</label> <div class="pull-left"><label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="surveycheck" name="surveycheck" value="1"'; if ($obj->surveycheck == 1) { echo 'checked'; } echo '> Enable</label></div></div>';
                echo '<div class="row even"><label>Social Media</label> <div class="pull-left"><label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="socialmedia" name="socialmedia" value="1"'; if ($obj->socialmedia == 1) { echo 'checked'; } echo '> Enable</label><br /><input type="text" id="socialmedia_url" name="socialmedia_url" class="input-wide" placeholder="Enter URL" value="'.$obj->socialmedia_url.'" style="margin-top:10px;"></div></div>';
                echo '<div class="row odd"><label>Resources</label> <div class="pull-left"><label class="checkbox inline" style="padding-top:0;"> <input type="checkbox" id="resourcelist" name="resourcelist" value="1"'; if ($obj->resourcelist == 1) { echo 'checked'; } echo '> Enable</label><br />(<em>edit Resources below in Assets Submitted section</em>)<br /><input type="text" id="resourcelist_url" name="resourcelist_url" class="input-wide" placeholder="Enter URL" value="'.$_SESSION['resourcelist_url'].'"></div></div>';
                $parsed_cl = parse_url($obj->contactlist_url);
                if (empty($parsed_cl['scheme'])) { $url_cl = 'http://' . ltrim($obj->contactlist_url, '/'); } else { $url_cl = $obj->contactlist_url; }
                echo '<div class="row even"><label>Contact list URL</label> <div class="pull-left"><input type="text" id="contactlist_url" name="contactlist_url" class="input-wide" placeholder="Enter URL of contact list (optional)" value="'.$obj->contactlist_url.'"><br /><label class="checkbox inline" style="font-weight:normal;"> <input type="checkbox" id="contactlist_optin_url" name="contactlist_optin_url" value="1"'; if ($obj->contactlist_optin_url == 1) { echo ' checked'; } echo '> The contacts on this list have opted in for marketing contact.</label></div></div>';
                echo '<div class="row odd"><label>Exisiting Contact List</label> <div class="pull-left"><textarea id="eloqua_contact_list" name="eloqua_contact_list" class="input-wide textarea-medium" placeholder="Example 1: All EMEA Embedded Prospects and Responses (approx 11,000). Example 2: Follow segmentation rules in lace for Retail Ecosystem eNurture.">'.$obj->eloqua_contact_list.'</textarea></div></div>';
                echo '<div class="row even"><label>MQL Criteria and Routing Rules</label> <div class="pull-left" style="width:690px;"><textarea id="routing_rules" name="routing_rules" class="input-wide textarea-medium" placeholder="Please add MQL criteria and routing rules">'.$obj->routing_rules.'</textarea><br /><label class="checkbox inline"> <input type="checkbox" id="routing_rules_notify" name="routing_rules_notify" value="1"'; if ($obj->routing_rules_notify == 1) { echo 'checked'; } echo '> Notify me upon fulfillment of criteria.</label></div></div>';
                echo '<div class="row odd"><label>Assets Submitted</label> <div class="pull-left" style="width:690px;">'.$assets.'</div></div>';

            } else {
                //NOT EDITING HERE, JUST LOOKIN'
                echo '<div class="row odd"><label>Webinar Title</label> <div class="pull-left">'.$obj->webinar_title.'</div></div>';
                echo '<div class="row even"><label>Webinar Owner</label> <div class="pull-left">'.$obj->webinar_owner.'</div></div>';
                echo '<div class="row odd"><label>Webinar Date</label> <div class="pull-left">'.$obj->webinar_date.'</div></div>';
                echo '<div class="row even"><label>Webinar Time</label> <div class="pull-left">'.$obj->time.' ('.$obj->time_zone.')</div></div>';
                echo '<div class="row odd"><label>Length</label> <div class="pull-left">'.$obj->length.'</div></div>';
                echo '<div class="row even"><label>Expected Attendees</label> <div class="pull-left">'.$obj->expected_attendees.'</div></div>';
                echo '<div class="row odd"><label>Target GEO</label> <div class="pull-left">'.$obj->geo.'</div></div>';
                echo '<div class="row even"><label>Sector</label> <div class="pull-left">'.$obj->sector; if ($obj->subsector != '' || $obj->subsector_other != '') { echo '<br /><em>'; if ($obj->subsector_other != '') { echo $obj->subsector_other; } else { echo $obj->subsector; } echo'</em> (sub-sector)'; } echo '</div></div>';
                echo '<div class="row odd"><label>Webinar Initial Invitation Date</label> <div class="pull-left">'.$obj->webinar_initial_date.'</div></div>';
                echo '<div class="row even"><label>Webinar Reminder 1 Date</label> <div class="pull-left">'.$obj->webinar_reminder_1_date.'</div></div>';
                echo '<div class="row odd"><label>Webinar Reminder 2 Date</label> <div class="pull-left">'.$obj->webinar_reminder_2_date.'</div></div>';
                echo '<div class="row even"><label>Topics Covered</label> <div class="pull-left" style="width:690px;">'.$obj->topics_covered.' '.$obj->topics_covered_other.'</div></div>';
                echo '<div class="row odd"><label>Description</label> <div class="pull-left" style="width:690px;">'.$obj->description.'</div></div>';
                //echo '<div class="row odd"><label>Content Type</label> <div class="pull-left">'.$obj->content_type.'</div></div>';
                echo '<div class="row even"><label>Series Based?</label> <div class="pull-left">';
                if ($obj->series_based == 'Yes' || $obj->series_based_name != '') { 
                    echo $obj->series_based_name;
                } else {
                    echo 'No';
                }
                echo '</div></div>';
                echo '<div class="row odd"><label>Speakers &amp; Moderators</label> <div class="pull-left" style="width:690px;">'.$speakers.'</div></div>';
                echo '<div class="row even"><label>URL</label> <div class="pull-left">';
                if ($obj->url == 1 || $obj->url_value != '') { 
                    $parsed_url = parse_url($obj->url_value);
                    if (empty($parsed_url['scheme'])) { $url_value = 'http://' . ltrim($obj->url_value, '/'); } else { $url_value = $obj->url_value; }
                    echo '<a href="'.$url_value.'" target="_blank">'.$obj->url_value.'</a>';
                } else {
                    echo 'No';
                }
                echo '</div></div>';
                echo '<div class="row odd"><label>Survey</label> <div class="pull-left">';
                if ($obj->surveycheck == 1) { 
                    echo 'Yes (file below)';
                } else {
                    echo 'No';
                }
                echo '</div></div>';
                echo '<div class="row even"><label>Social Media</label> <div class="pull-left">';
                if ($obj->socialmedia == 1) {
                    $parsed_social = parse_url($obj->socialmedia_url);
                    if (empty($parsed_social['scheme'])) { $url_social = 'http://' . ltrim($obj->socialmedia_url, '/'); } else { $url_social = $obj->socialmedia_url; }
                    echo '<a href="'.$url_social.'" target="_blank">'.$obj->socialmedia_url.'</a>';
                } else {
                    echo 'No';
                }
                echo '</div></div>';
                echo '<div class="row odd"><label>Resources</label> <div class="pull-left">';
                if ($obj->resourcelist == 1) { 
                    echo 'Yes (files below)';
                    $parsed_resource = parse_url($obj->resourcelist_url);
                    if (empty($parsed_resource['scheme'])) { $url_resource = 'http://' . ltrim($obj->resourcelist_url, '/'); } else { $url_resource = $obj->resourcelist_url; }
                    if ($obj->resourcelist_url != '') { echo '<br /><a href="'.$url_resource.'" target="_blank">'.$obj->resourcelist_url.'</a>'; }
                } else {
                    echo 'No';
                }
                echo '</div></div>';
                $parsed_cl = parse_url($obj->contactlist_url);
                if (empty($parsed_cl['scheme'])) { $url_cl = 'http://' . ltrim($obj->contactlist_url, '/'); } else { $url_cl = $obj->contactlist_url; }
                echo '<div class="row even"><label>Contact list URL</label> <div class="pull-left">'; if ($obj->contactlist_url != '') { echo '<a href="'.$url_cl.'" target="_blank">'.$obj->contactlist_url.'</a><br />The contacts on this list have opted in for marketing contact: '; if ($obj->contactlist_optin_url == 1) { echo 'Yes'; } else { echo 'No'; } } echo '</div></div>';
                echo '<div class="row odd"><label>Exisiting Contact List</label> <div class="pull-left">'.$obj->eloqua_contact_list.'</div></div>';
                echo '<div class="row even"><label>MQL Criteria and Routing Rules</label> <div class="pull-left" style="width:690px;">'.$obj->routing_rules; if ($obj->routing_rules != '') { echo '<br /><br />Notify me upon fulfillment of criteria: '; if ($obj->routing_rules_notify == 1) { echo 'Yes'; } else { echo 'No'; } } echo '</div></div>';
                echo '<div class="row odd"><label>Assets Submitted</label> <div class="pull-left" style="width:690px;">'.$assets.'</div></div>';

            }    
            
            echo '<div class="row even"><label>Notes</label> <div class="pull-left" style="width:690px;">'.$notes;
            //Admin can add notes
            if ($_SESSION['admin'] == 1) {
                echo '<br /><textarea id="notes_update" name="notes_update" rows="3" cols="45" class="input-full" style="width:50%;"></textarea>';
            }
            echo '</div></div>';

            //Can upload extra files if in Edit mode
            if ($edit) {
                echo '<br /><br /><h1>Upload Additional Assets</h1>';
                echo '<div class="row odd"><label>Presentation</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                    <div id="presentation" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Add Presentation</span><input id="fileupload_presentation" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-presentation" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="errors-presentation" class="files" style="clear:both;color:#cc0000;"></div>
                                    <div id="shown-presentation" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                echo '<div class="row even"><label>Media Upload</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                    <div id="media" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Add Media</span><input id="fileupload_media" type="file" name="files[]" multiple></span>
                                    </div>
                                    <div id="progress-media" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="errors-media" class="files" style="clear:both;color:#cc0000;"></div>
                                    <div id="shown-media" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                echo '<div class="row odd"><label>Survey</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                    <div id="survey" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Add Survey File</span><input id="fileupload_survey" type="file" name="files[]"></span>
                                    </div>
                                    <div id="progress-survey" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-survey" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                echo '<div class="row even"><label>Resource List</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                    <div id="resource" class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Add File(s)</span><input id="fileupload_resource" type="file" name="files[]" multiple></span>
                                    </div>
                                    <div id="progress-resource" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                        <div class="bar"></div>
                                    </div>
                                    <div id="shown-resource" class="files" style="clear:both;"></div>
                                </div> </div></div>';
                echo '<div class="row odd"><label>Contact List</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload a Contact List</span><input id="fileupload_contacts" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-contacts" class="files" style="clear:both;"></div>
                                    </div> </div></div>';
                echo '<div class="row even"><label>Campaign Brief</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Campaign Brief</span><input id="fileupload_briefs" type="file" name="files[]" multiple></span>
                                        </div>
                                        <div id="progress-briefs" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-briefs" class="files" style="clear:both;"></div>
                                    </div> </div></div>';
                echo '<div class="row odd"><label>Custom Images</label> <div class="pull-left" style="width:690px;"><div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Registration Page Image</span><input id="fileupload_regpage" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-regpage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-regpage" class="files" style="clear:both;"></div>
                                    </div>

                                    <div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Email Confirmation Image</span><input id="fileupload_confpage" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-confpage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-confpage" class="files" style="clear:both;"></div>
                                    </div>

                                    <div class="upload-div">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Your Logo</span><input id="fileupload_logo" type="file" name="files[]"></span>
                                        </div>
                                        <div id="progress-logo" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 246px;display:none;">
                                            <div class="bar"></div>
                                        </div>
                                        <div id="shown-logo" class="files" style="clear:both;"></div>
                                    </div> </div></div>';
            }

            //Admin can add some other things
            if ($_SESSION['admin'] == 1) {
                echo '<br /><br /><h1>Additional Details</h1><p>Provide the requester with the presenter and attendee URL and bridge information, analytics links, and on-demand URL. Please change the request status to "Request complete" when all fields have been provided.</p>';
                echo '<div class="row odd"><label>Presenter Info</label> <div class="pull-left" style="width:690px;"><textarea id="presenter_info" name="presenter_info" rows="3" cols="45" class="input-full" style="width:50%;">'.$obj->presenter_info.'</textarea></div></div>';
                echo '<div class="row even"><label>End User Info</label> <div class="pull-left" style="width:690px;"><textarea id="end_user_info" name="end_user_info" rows="3" cols="45" class="input-full" style="width:50%;">'.$obj->end_user_info.'</textarea></div></div>';
                echo '<div class="row odd"><label>On24 Analytics URL</label> <div class="pull-left" style="width:690px;"><input type="text" id="on24_analytics_url" name="on24_analytics_url" class="input-full" style="width:50%;" value="'.$obj->on24_analytics_url.'"></div></div>';
                echo '<div class="row even"><label>Eloqua Analytics URL</label> <div class="pull-left" style="width:690px;"><input type="text" id="eloqua_analytics_url" name="eloqua_analytics_url" class="input-full" style="width:50%;" value="'.$obj->eloqua_analytics_url.'"></div></div>';
                echo '<div class="row odd"><label>Registration Page URL</label> <div class="pull-left" style="width:690px;"><input type="text" id="regpage_url" name="regpage_url" class="input-full" style="width:50%;" value="'.$obj->regpage_url.'"></div></div>';
                echo '<div class="row even"><label>Aprimo Marketing ID</label> <div class="pull-left" style="width:690px;"><input type="text" id="aprimo_id" name="aprimo_id" class="input-full" style="width:50%;" value="'.$obj->aprimo_id.'"></div></div>';
                echo '<div class="row odd"><label>Webinar ID</label> <div class="pull-left" style="width:690px;"><input type="text" id="webinar_id" name="webinar_id" class="input-full" style="width:50%;" value="'.$obj->webinar_id.'"></div></div>';
            } else {
                if ($obj->presenter_info != '' || $obj->end_user_info != '' || $obj->on24_analytics_url != '' || $obj->eloqua_analytics_url != '' || $obj->regpage_url != '' || $obj->aprimo_id != '' || $obj->webinar_id != '') {
                    echo '<br /><br /><h1>Additional Details</h1>';
                    echo '<div class="row odd"><label>Presenter Info</label> <div class="pull-left" style="width:690px;">'.$obj->presenter_info.'</div></div>';
                    echo '<div class="row even"><label>End User Info</label> <div class="pull-left" style="width:690px;">'.$obj->end_user_info.'</div></div>';
                    echo '<div class="row odd"><label>On24 Analytics URL</label> <div class="pull-left" style="width:690px;"><a href="'.$obj->on24_analytics_url.'" target="_blank">'.$obj->on24_analytics_url.'</a></div></div>';
                    echo '<div class="row even"><label>Eloqua Analytics URL</label> <div class="pull-left" style="width:690px;"><a href="'.$obj->eloqua_analytics_url.'" target="_blank">'.$obj->eloqua_analytics_url.'</a></div></div>';
                    echo '<div class="row odd"><label>Registration Page URL</label> <div class="pull-left" style="width:690px;"><a href="'.$obj->regpage_url.'" target="_blank">'.$obj->regpage_url.'</a></div></div>';
                    echo '<div class="row even"><label>Aprimo Marketing ID</label> <div class="pull-left" style="width:690px;">'.$obj->aprimo_id.'</div></div>';
                    echo '<div class="row odd"><label>Webinar ID</label> <div class="pull-left" style="width:690px;">'.$obj->webinar_id.'</div></div>';
                }
            }
        }
    } else {
        echo '<p><strong>This request either isn\'t active, hasn\'t been created yet, or doesn\'t belong to you.</p>';
    }

    //Let's show some survey results
    if ($_SESSION['admin'] == 1) {
        //Rating
        $query_survey = "SELECT * FROM surveys WHERE request_id = '".$_GET['id']."' LIMIT 1";
        $result_survey = $mysqli->query($query_survey);
        $row_cnt_survey = $result_survey->num_rows;

        if ($row_cnt_survey > 0) { //Survey has been submitted
            echo '<h1 style="margin-top:30px;">Survey Results</h1>';
            echo '<div class="single-request">';

            while ($obj_survey = $result_survey->fetch_object()) {
                echo '<div class="row even"><div class="pull-left" style="width:700px;">How satisfied are you with the promptness of our response to your initial request?</div> <div class="pull-right" style="width:115px;">'.getStarRating($obj_survey->promptness).'</div></div>';
                echo '<div class="row even"><div class="pull-left" style="width:700px;">How satisfied are you with the timeliness of the published update to the Intel Customer Connect website?</div> <div class="pull-right" style="width:115px;">'.getStarRating($obj_survey->timelines).'</div></div>';
                echo '<div class="row even"><div class="pull-left" style="width:700px;">How satisfied are you with the final outcome on the Intel Customer Connect website?</div> <div class="pull-right" style="width:115px;">'.getStarRating($obj_survey->outcome).'</div></div>';
                echo '<div class="row even"><div class="pull-left" style="width:700px;">How satisfied are you overall with your experience utilizing Intel Customer Connect to submit and manage requests?</div> <div class="pull-right" style="width:115px;">'.getStarRating($obj_survey->overall).'</div></div>';
                echo '<div class="row even"><div class="pull-left" style="width:700px;"><strong>Average Rating</strong></div> <div class="pull-right" style="width:115px;">'.getStarRating($obj_survey->average).'</div></div>';
                
                if ($obj_survey->comments != '') {
                    echo '<h1 style="margin-top:5px;">Survey Comments</h1>';
                    echo '<div class="row even">'.$obj_survey->comments.'</div>';
                }
            }

            echo '</div>';
        }
    }
    //End survey

    echo '<p><br /><a href="requests" class="btn-orange back pull-left">Back to list</a>';
    if ($_SESSION['admin'] == 1 || $edit == 1) {
        //echo '<input type="submit" class="btn-green submit submit-btn pull-right" data-analytics-label="Submit Form: Request Update" value="Submit updates"></p></form></div>';
        echo '<a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Request Update">Submit updates</a></form></div>';
    } else {
        echo '</p></div>';
    }

*/

?>