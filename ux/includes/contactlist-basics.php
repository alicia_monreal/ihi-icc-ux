<div class="control-group">
	<label for="create_contactlist" class="control-label">Do you have a contact list to upload for this tactic?</label>
	<div class="controls">
		<label class="radio inline"> <input type="radio" id="create_contactlist[]" name="create_contactlist" value="1"> Yes</label>
		<label class="radio inline"> <input type="radio" id="create_contactlist[]" name="create_contactlist" value="0" checked> No</label><br />
	</div>
</div>
<div id="contactlist-fields" class="hide">
	<div class="control-group">
		<label for="list_tag" class="control-label">Contact List Tag</label>
		<div class="controls">
			<span class="required-mark">*</span><input type="text" id="list_tag" name="list_tag" class="input-wide" value=""><i class="helper-icon popover-link icon-question-sign" data-container="body" data-toggle="popover" data-content="For Example: 2014.07.22 or Retail Touchpoints or Seoul World IT And Security 2013"></i>
			<p>The List Tag is a friendly name for distinguishing the list. Once created, you will not be able to edit this value. Click the pop up for examples.</p>
		</div>
	</div>
	<div class="control-group">
		<label for="contact_list_request_type" class="control-label">Contact List Request Type</label>
		<div class="controls">
			<span class="required-mark">*</span><select id="contact_list_request_type" name="contact_list_request_type" class="input-wide">
				<option value="">Select one</option>
				<option value="Contact"<?php if ($_SESSION['contact_list_request_type'] == 'Contact') { echo ' selected'; } ?>>Contact</option>
				<!--<option value="Lead"<?php if ($_SESSION['contact_list_request_type'] == 'Lead') { echo ' selected'; } ?>>Lead</option>
				<option value="Contact and Lead"<?php if ($_SESSION['contact_list_request_type'] == 'Contact and Lead') { echo ' selected'; } ?>>Contact and Lead</option>
				<option value="Prospect Assessment"<?php if ($_SESSION['contact_list_request_type'] == 'Prospect Assessment') { echo ' selected'; } ?>>Prospect Assessment</option>-->
				<option value="RAW"<?php if ($_SESSION['contact_list_request_type'] == 'RAW') { echo ' selected'; } ?>>RAW</option>
			</select><i class="helper-icon popover-link icon-question-sign" data-container="body" data-toggle="popover" data-content="Contact list: These lists use the provided sector-specific templates to load your contacts.<br /><br />RAW: These are unformated lists of contacts that do not use the standard upload templates. Do note that these lists may take longer to process and may require some back and forth. These request types also do not follow the standard SLA timing for list loads."></i>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" style="margin-top: -5px;">Contact List Upload</label>
		<div class="controls">
			<div class="controls-minigroup">
				<p>All the contact data that you upload will be purged from this portal 30 days after the upload per Intel's privacy guidelines.</p>
				<div class="upload-div" style="margin-top:8px;">
				    <div class="fileupload fileupload-new" data-provides="fileupload">
						<span class="btn-file btn-green add"><span class="fileupload-new">Upload a Contact List</span><input id="fileupload_contacts" type="file" name="files[]"></span>
					</div>
					<div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 218px;display:none;">
				        <div class="bar"></div>
				    </div>
				    <div id="shown-contacts" class="files" style="clear:both;"></div>
				</div>
				<p>&nbsp;</p>
			</div>
		</div>
	</div>
</div>
<div class="control-group">
	<label for="existing_contact_list" class="control-label">Existing Contact List</label>
	<div class="controls"><textarea id="existing_contact_list" name="existing_contact_list" class="input-wide textarea-medium" placeholder="Enter segmentation rules for existing contacts"><?php echo getFieldValue('existing_contact_list', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
</div>
<div class="control-group">
	<label for="additional_instructions" class="control-label">Additional Instructions</label>
	<div class="controls">
		<textarea id="additional_instructions" name="additional_instructions" class="input-wide textarea-medium" placeholder="Please provide any additional instructions you'd like to add to your request."><?php echo getFieldValue('additional_instructions', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea>
	</div>
</div>