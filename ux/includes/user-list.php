<?php
@session_start();
include('_globals.php');

$query = "SELECT 
                *,
				DATE_FORMAT( date_created,  '%m/%d/%Y' ) AS date_created_user,
                DATE_FORMAT( date_updated,  '%m/%d/%Y' ) AS date_updated_user
			FROM users";


if (isset($_GET['id']) && $_SESSION['admin'] == 1) {
	//SHOWING SINGLE RECORD

	if ($_SESSION['admin'] == 1) { //Show user
		$query.=" WHERE id = '".$_GET['id']."' LIMIT 1";
	} else {
		$query.=" WHERE id = '".$_SESSION['user_id']."' AND active = 1 LIMIT 1";
	}
				
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    //print_r($query);

    if ($row_cnt > 0) {
        echo '<div class="single-request">';

        //Show a message?
        if (isset($_GET['update'])) {
        	if ($_GET['update'] == 'success') {
        		echo '<p style="color:#ff0000;"><strong>User has been updated.</strong><br /></p>';
        	} else if ($_GET['update'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>We\'re sorry, there has been a problem updating this user.</strong><br /></p>';
            }
        }
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'success') {
                echo '<p style="color:#ff0000;"><strong>User has been submitted.</strong><br /></p>';
            }
        }

        while ($obj = $result->fetch_object()) {
            $current_user_id = $_GET['id'];

        	//Admins get a form
	        if ($_SESSION['admin'] == 1) {
	        	echo '<form id="user-update-form" action="includes/_user-admin.php?id='.$current_user_id.'&action=update" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="update">
					<input type="hidden" id="user_id" name="user_id" value="'.$current_user_id.'">
					<input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">
                    <input type="hidden" id="user_email" name="user_email" value="'.$obj->email.'">';
			}
        	
            echo '<div class="row odd row-border"><label>Date Created</label> <div class="pull-left">'.$obj->date_created_user.'</div></div>';
            echo '<div class="row even"><label>First Name</label> <div class="pull-left"><input type="text" id="fname" name="fname" placeholder="First Name" value="'.$obj->fname.'" class="form-control required"></div></div>';
            echo '<div class="row odd"><label>Last Name</label> <div class="pull-left"><input type="text" id="lname" name="lname" placeholder="Last Name" value="'.$obj->lname.'" class="form-control required"></div></div>';
            echo '<div class="row even"><label>Email</label> <div class="pull-left"><input type="text" id="email" name="email" placeholder="Email" value="'.$obj->email.'" class="form-control email required"></div></div>';
            echo '<div class="row odd"><label>Title</label> <div class="pull-left"><input type="text" id="title" name="title" placeholder="Title" value="'.$obj->title.'" class="form-control"></div></div>';
            echo '<div class="row even"><label>Company</label> <div class="pull-left"><input type="text" id="company" name="company" placeholder="Company" value="'.$obj->company.'" class="form-control"></div></div>';
            echo '<div class="row odd"><label>Role</label> <div class="pull-left">';
            //Status
            if ($_SESSION['admin'] == 1) {
		    	echo '<select name="role" id="role" class="form-control required" style="width:290px;"><option value="">Please select a role for this user</option>';
                    $query_roll = "SELECT * FROM roles WHERE active = 1 ORDER BY name ASC";
                    $result_roll = $mysqli->query($query_roll);
                    while ($obj_roll = $result_roll->fetch_object()) {
                        echo '<option value="'.$obj_roll->id.'"'; if ($obj->role == $obj_roll->id) { echo ' selected'; } echo '>'.$obj_roll->name.'</option>';
                    }
                echo '</select>';
	        } else {
                $query_roll = "SELECT * FROM roles WHERE id = $obj->role";
                $result_roll = $mysqli->query($query_roll);
                while ($obj_roll = $result_roll->fetch_object()) {
                    echo $obj_roll->name;
                }
			}
			echo '</div></div>';
            echo '<div class="row even"><label>Point of Contact</label> <div class="pull-left"><input type="text" id="point_of_contact" name="point_of_contact" placeholder="Point of Contact" value="'.$obj->point_of_contact.'" class="form-control"></div></div>';
            echo '<div class="row even"><label>Point of Contact Email</label> <div class="pull-left"><input type="text" id="point_of_contact_email" name="point_of_contact_email" placeholder="Point of Contact Email" value="'.$obj->point_of_contact_email.'" class="form-control"></div></div>';
            echo '<div class="row even"><label>Content Access Control</label> <div class="pull-left">';

            //Access Control
            $query_access = "SELECT * FROM access_control WHERE active = 1 ORDER BY id ASC";
            $result_access = $mysqli->query($query_access);
            $access_control_arr = explode(", ", $obj->access_control);
            while ($obj_access = $result_access->fetch_object()) {
                echo '<label class="checkbox-inline"> <input type="checkbox" id="access_control[]" name="access_control[]" value="'.$obj_access->id.'"'; if (in_array($obj_access->id, $access_control_arr)) { echo 'checked'; } echo '> '.$obj_access->name.'</label>';
            }
            echo '</div></div>';

            //Count requests
            $query_req_made = "SELECT * FROM request WHERE user_id = $current_user_id";
            $result_req_made = $mysqli->query($query_req_made);
            $row_cnt_req_made = $result_req_made->num_rows;
            echo '<div class="row odd"><label>Requests Made</label> <div class="pull-left">'.$row_cnt_req_made.'</div></div>';

            //Count requests completed
            $query_req_complete = "SELECT * FROM request WHERE user_id = $current_user_id AND status = 4";
            $result_req_complete = $mysqli->query($query_req_complete);
            $row_cnt_req_complete = $result_req_complete->num_rows;
            echo '<div class="row even"><label>Requests Completed</label> <div class="pull-left">'.$row_cnt_req_complete.'</div></div>';
            echo '<div class="row odd"><label>Last Updated</label> <div class="pull-left">'.$obj->date_updated_user.'</div></div>';
            
            echo '</div>';
        }
	} else {
		echo '<p><strong>This user either isn\'t active or hasn\'t been created yet.</p>';
	}

	echo '<p><br /><a href="manage-users" class="btn-orange back pull-left">Back to list</a>';
	if ($_SESSION['admin'] == 1) {
		echo '<input type="submit" class="btn-green submit submit-btn pull-right" data-analytics-label="Submit Form: Update User" value="Update user"></p></form></div>';
	} else {
		echo '</p></div>';
	}

} else {
	//SHOWING FULL LIST
    //Show a message?
    if (isset($_GET['message'])) {
        if ($_GET['message'] == 'approved') {
            echo '<p style="color:#ff0000;"><strong>User has been approved. Click their name to add them to access control groups.</strong><br /></p>';
        } else if ($_GET['message'] == 'denied') {
            echo '<p style="color:#ff0000;"><strong>User has been denied.</strong><br /></p>';
        } else if ($_GET['message'] == 'updated') {
            echo '<p style="color:#ff0000;"><strong>User has been updated.</strong><br /></p>';
        }
    }

	if ($_SESSION['admin'] != 1) { //Show ALL requests for admin
		//Close query, limit to only view self if stumbled upon
		$query.=" WHERE id = '".$_SESSION['user_id']."' AND active = 1";
	}
	$query.=" ORDER BY date_created DESC";
				
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    //print_r($query);

    if ($row_cnt > 0) {
    	echo '<br /><table class="table table-striped table-bordered table-hover tablesorter requests-table">';
    	echo '<thead><th>Created <b class="icon-white"></b></th><!--<th>First Name <b class="icon-white"></b></th>--><th>Name <b class="icon-white"></b></th><th>Email <b class="icon-white"></b></th><th>Role <b class="icon-white"></b></th><th>Last Login <b class="icon-white"></b></th><th>Logins <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th><th>Admin</th></thead>';
    	echo '<tbody>';
        while ($obj = $result->fetch_object()) {
            //Additions details for the user
            //Last login
            $query_login = "SELECT DATE_FORMAT(logged_in, '%m/%d/%Y') AS date_logged_in FROM log_users WHERE user_id = '".$obj->id."' ORDER BY logged_in ASC";
            $result_login = $mysqli->query($query_login);
            $row_cnt_login = $result_login->num_rows;
            if ($row_cnt_login > 0) {
                while ($obj_l = $result_login->fetch_object()) {
                    $last_login = $obj_l->date_logged_in;
                }
            } else {
                $last_login = 'Never';
            }

            $query_login_count = "SELECT DATE_FORMAT(logged_in, '%m/%d/%Y') AS date_logged_in FROM log_users WHERE user_id = '".$obj->id."' ORDER BY logged_in DESC";
            $result_login_count = $mysqli->query($query_login_count);
            $number_logins = $result_login_count->num_rows;

            $query_roll = "SELECT * FROM roles WHERE id = $obj->role";
            $result_roll = $mysqli->query($query_roll);
            while ($obj_roll = $result_roll->fetch_object()) {
                $role = $obj_roll->name;
            }

            if ($obj->active == 1) {
                $status = '<td>Active</td><td style="text-align:center;"><a href="manage-users?id='.$obj->id.'" class="action-icon icon-pencil popover-hover" data-toggle="popover" data-content="Edit"></a><a href="" onclick="denyUser(\'includes/_user-admin.php?id='.$obj->id.'&action=deny\');return false;" class="action-icon icon-close popover-hover" data-toggle="popover" data-content="Make Inactive"></a></td>';
            } else {
                $status = '<td><em style="color:#ff0000;">Inactive</em></td><td style="text-align:center;"><a href="" onclick="denyUser(\'includes/_user-admin.php?id='.$obj->id.'&action=deny\');return false;" class="action-icon icon-ban popover-hover" data-toggle="popover" data-content="Deny User"></a><a href="" onclick="approveUser(\'includes/_user-admin.php?id='.$obj->id.'&action=approve\');return false;" class="action-icon icon-check popover-hover" data-toggle="popover" data-content="Approve User"></a></td>';
            }

        	echo '<tr>';
            echo '<td>'.$obj->date_created_user.'</td>';
            //Admin's can click a name to edit
            if ($_SESSION['admin'] == 1) {
                echo '<td><a href="manage-users?id='.$obj->id.'">'.$obj->lname.', '.$obj->fname.'</a></td>';
            } else {
                echo '<td>'.$obj->lname.', '.$obj->fname.'</td>';
            }
            echo '<td><a href="mailto:'.$obj->email.'">'.$obj->email.'</a></td>';
            echo '<td>'.$role.'</td>';
            echo '<td>'.$last_login.'</td>';
            echo '<td>'.$number_logins.'</td>';
            echo $status;
            echo '</tr>';
        }
        echo '</tbody></table>';
	} else {
		echo '<p><strong>There are current no users.</strong></p>';
	}
}

?>