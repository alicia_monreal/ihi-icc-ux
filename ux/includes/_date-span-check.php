<?php
session_start();
include('_globals.php');

$date = $_GET['date'];

$lessTime = strtotime('-2 weeks', strtotime($date));
$lessTime = date('Y-m-d', $lessTime);

$moreTime = strtotime('+2 weeks', strtotime($date));
$moreTime = date('Y-m-d', $moreTime);

$query = "SELECT webinar_title,
				webinar_owner,
				geo,
				sector,
				subsector,
				subsector_other,
				time,
				time_zone,
				description,
				DATE_FORMAT(webinar_date, '%m/%d/%Y') AS webinar_date
			FROM requests
			WHERE webinar_date BETWEEN '".$lessTime."' and '".$moreTime."'
			AND DATE(webinar_date) > DATE(NOW())
			AND active = 1
			ORDER BY webinar_date ASC";
$result = $mysqli->query($query);

if ($result->num_rows > 0) {
	echo '<p><strong><em style="color: #cc0000;">Please note:</em> the following webinars are close to the date you\'ve selected</strong></p><hr>';
	while ($obj = $result->fetch_object()) {
		$clean_desc = str_replace('"', "", $obj->description);

		echo '<p><strong style="font-size:16px;">'.$obj->webinar_title.'</strong><br /><strong>Owner:</strong> '.$obj->webinar_owner.'<br /><strong>Time:</strong> '.$obj->webinar_date.', '.$obj->time.' '.$obj->time_zone.'<br /><strong>Target GEO:</strong> '.$obj->geo.'<br /><strong>Sector/Sub-Sector:</strong> '.$obj->sector.'/'.$obj->subsector; if ($obj->subsector == 'Other' && $obj->subsector_other != '') { echo ' ('.$obj->subsector_other.')'; } echo '<br /><strong>Description:</strong> '.$obj->description.'</p><hr>';

		//Popup not working
		//echo '<p><strong><a href="#" data-toggle="popover" data-content="<h4>'.$obj->webinar_title.'</h4><br /><strong>Owner:</strong> '.$obj->webinar_owner.'<br /><strong>Time:</strong> '.$obj->webinar_date.', '.$obj->time.' '.$obj->time_zone.'<br /><br />'.$clean_desc.'" class="popover-link">'.$obj->webinar_title.'</a></strong><br /><em>'.$obj->webinar_date.'</em></p>';
	}
}
?>


