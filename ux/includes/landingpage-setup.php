
<!-- Campaign Setup Modal -->
<div id='landingpage-setup' class="modal fade">
  <div class="modal-dialog icc-modal-dialog">
    <div class="modal-content" style="padding-left:5%;padding-right:5%;">
            <div class="modal-header modal-tabs">
              <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
              <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Landing Page Setup</h2>
              <ul class="nav nav-tabs" role="tablist" style='display: block;width: 56%;margin: 0 auto; margin-top:20px'>
                <li role="presentation" id="details-setup-tab" class="active col-md-6" style='padding:0 5px 0 5px'><a href="#details" style="padding-left:3px;padding-right:3px" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                <li role="presentation" id="schedule-setup-tab" class='col-md-6' style='padding:0 5px 0 5px'><a href="#schedule" style="padding-left:3px;padding-right:3px" aria-controls="schedule" role="tab" data-toggle="tab">Schedule</a></li>
              </ul>
            </div>

        <div class="modal-body" style="padding-top:30px;margin: 0 10% 0 8%;">
            <div class="tab-content">
              <!-- Details tab -->
              <div role="tabpanel" class="tab-pane fade in active" id="details">
                  
                  <form id="landingpage-setup-details-form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        <input type="hidden" id="action" name="action" value="create">
                        <input type="hidden" id="step" name="step" value="2">
                        <input type="hidden" id="c_type" name="c_type" value="webinar">
                        <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                        <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
                        <input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
                        <div class="form-group">
                            <label for="tag" class="col-md-3 control-label">Tactic Name</label>
                            <div class="col-md-8">
                                <input type="text" id="tag" name="tag" class="input-wide required" value="<?php echo $_SESSION['tag']; ?>" maxlength="40">
                            </div>
                            <i class="fa fa-question-circle" data-container="body" data-toggle="popover" data-content="The Tactic Tag is a friendly name for distinguishing the tactic. It should have meaning for the requestor and should not contain other elements of the Tactic Name, such as the Tactic Type, which will be auto-generated after creating your tactic."></i>
                        </div>
                        <div class="form-group">
                            <label for="owner" class="col-md-3 control-label">Owner</label>
                            <?php 
                                $owner_name = getFieldValue('owner', isset($_SESSION['clone_id'])?$_SESSION['clone_id']:0,1, ''); 
                                if(empty($owner_name)) // if empty owner name in requested table then fetch it from session
                                    $owner_name = getUserName($_SESSION['user_id']); 

                                $owner_email = getFieldValue('owner_email',isset($_SESSION['clone_id'])?$_SESSION['clone_id']:0,1, '');
                                if(empty($owner_email)) // if empty owner name in requested table then fetch it from session
                                    $owner_email = getEmailAddress($_SESSION['user_id']);
                            ?>
                            <div class="col-md-8">
                                <input type="text" id="owner" name="owner" class="form-control required" value="<?php echo $owner_name; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="owner_email" class="col-md-3 control-label">Owner Email</label>
                            <div class="controls col-md-8">
                                <input type="text" id="owner_email" name="owner_email" class="form-control required email" placeholder="Email*" value="<?php echo $owner_email; ?>"></div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-md-3 control-label">Description</label>
                            <div class="controls col-md-8"><textarea id="description" name="description" class="form-control textarea-medium form-control required" placeholder="Up to 200 words*"></textarea><p class="input-note">This is the publically viewed description of your event that will appear on listing pages and registration pages.</p></div>
                        </div>
                        
                    </form>
              </div>
                <!-- Schedule tab -->
                <div role="tabpanel" class="tab-pane fade" id="schedule">
                   
                    <form id="landingpage-setup-schedule-form" class="form-horizontal" method="POST" enctype="multipart/form-data">

                    <div class='row'>
                        <div class='col-md-6'>
                             <div class="form-group">
                                <label for="start_date" class="control-label col-md-4" style='padding-left:0;padding-right:0'>Landing Page Start Date</label>
                                <div class="col-md-8">
                                    <div id="start_date" class="input-group margin-bottom-sm" style="">
                                        <input type="text" id="datetimepicker" name="start_date" class="form-control required" value="">
                                        <span class="input-group-addon" style="margin-right:2px;"><i class=" fa fa-th"></i></span>
                                    </div>
                                </div>                   
                            </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                                <label for="start_date" class="control-label col-md-4" style="padding-left:0;padding-right:0">Landing Page Start Date</label>
                                <div class="col-md-8">
                                    <div id="datetimepicker" class="input-group input-append date margin-bottom-sm" style="">
                                        <input type="text" id="end_date" name="start_date" class="form-control required" style="" value="">
                                        <span class="input-group-addon" style="margin-right:2px;"><i class=" fa fa-th"></i></span>
                                    </div>
                                    <!-- <span style="font-weight: 600;">&nbsp;End Date</span><span class="required-mark" style="margin-left:5px">*</span> <div id="datetimepicker_end" class="input-append date" style=""><input type="text" id="end_date" name="end_date" class="input-medium required" style="width:134px;" value=""><span class="add-on"><i class="icon-th"></i></span></div> -->
                                </div>
                            </div>
                        </div>

                    
                        <div class='col-md-12'>
                            <div class="form-group">
                                 <label for="asset_reviewers" class="col-md-2 control-label">Asset Reviewers</label>
                                 <div class="controls col-md-10"><textarea id="asset_reviewers" name="asset_reviewers" class="form-control textarea-medium form-control required" placeholder="Provide a list of names and email addresses for the approval process."></textarea><p class="input-note">This is the publically viewed description of your event that will appear on listing pages and registration pages.</p></div>                        
                            </div>
                        </div>
                    </div>
                    </form> 
                </div>               
              
            </div>
        </div>
        
          <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
             <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="landingpage-setup-save-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0)" id="landingpage-setup-save" class="btn-green" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
        </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- End - Campaign Setup Modal -->