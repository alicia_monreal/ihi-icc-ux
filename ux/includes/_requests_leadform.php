<?php
session_start();
require_once('validate.php');
include('_globals.php');

if (!empty($_POST) && !empty($_POST['action'])) {

    //Step 2. Saving values to session data
    if ($_POST['step'] == 2) {

        //We're just sending an email for now
        $user_id = $_POST['user_id'];
        $user_name = getUserName($user_id);
        $user_email = $_POST['user_email'];
        $message = $_POST['message'];
        
        if ($message != '') {
            /////////////////////////////////////////////////////////////////////////////////////
            //Ok, let's email this puppy. It's going to support@
            $message_email = nl2br($message, false);
            $message_ms = $message_header;
            $message_ms.="<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">A new lead form has been submitted.<br /></p>
                <table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"6\" style=\"width:540px;\">
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Name</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_name."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" valign=\"middle\" align=\"right\" style=\"width:140px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Requestor Email</b></p></td>
                        <td width=\"24\" style=\"width:24px;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" style=\"width:376px;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$user_email."</p></td>
                    </tr>
                    <tr>
                        <td width=\"140\" bgcolor=\"#e9e9e9\" valign=\"middle\" align=\"right\" style=\"width:140px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;font-weight:bold;\"><b>Message</b></p></td>
                        <td width=\"24\" bgcolor=\"#e9e9e9\" style=\"width:24px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\">&nbsp;</td>
                        <td width=\"376\" bgcolor=\"#e9e9e9\" style=\"width:376px;background-color:#e9e9e9;padding-top:4px;padding-bottom:4px;\"><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:15px;color:#464646;\">".$message_email."</p></td>
                    </tr>
                </table>
            <p>&nbsp;</p>";

            $message_ms.=$message_footer;

            $mail->AddAddress('support@customerconnect.intel.com', 'Customer Connect');
            $mail->Subject = 'Intel Customer Connect: New Lead Form';
            $mail->MsgHTML($message_ms);
            $mail->AltBody = 'A new lead form request has been submitted: '.$message_email.'';
            //$mail->Send();

            //Finally, let's let 'em know
            header('Location:/request?action=emailed');
        } else {
            header('Location:/request-leadform-2');
        }
    }

} else {
    header('Location:request?action=failure'); 
}
?>