<div id='webinar-audience' class="modal fade">
  <div class="modal-dialog" style="width:75%;max-width:1000px">
    <div class="modal-content" style="padding-left:5%;padding-right:5%">
        <div class="modal-header modal-tabs">
          <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
          <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Audience</h2>
          <ul class="nav nav-tabs" role="tablist" style='display: block;width: 56%;margin: 0 auto; margin-top:20px'>
            <li role="presentation" class="active col-md-6" style='padding:0 5px 0 5px'><a href="#new-contacts" style="padding-left:3px;padding-right:3px" aria-controls="new-contacts" role="tab" data-toggle="tab">New Contacts</a></li>
            <li role="presentation" class='col-md-6' style='padding:0 5px 0 5px'><a href="#existing-contacts" style="padding-left:3px;padding-right:3px" aria-controls="existing-contacts" role="tab" data-toggle="tab">Existing Contacts</a></li>
          </ul>
        </div>

        <div class="modal-body" style="padding-bottom:90px;padding-top:40px;margin:0 5% 0 0%">
         <div role="tabpanel">
         <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="new-contacts">

          <form id="webinar-audience-form-1" class="content-update-form form-horizontal">
            <input type="hidden" id="action" name="action" value="create">
            <input type="hidden" id="step" name="step" value="5">
            <input type="hidden" id="c_type" name="c_type" value="webinar">
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
            <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
            <input type="hidden" id="in_progress_form" name="in_progress_form" value="0">
            <div class='form-group' id='new-campaign-type' style="margin-bottom:30px;margin-left:15%">
              <label class="radio-inline col-md-7" style="font-family:'IntelClearBold'">Do you have a contact list to upload for this tactic?</label>
              <div class='col-md-5'>
              <label class="radio-inline" style="font-family:'IntelClearBold';margin-right:40px">
                  <input type="radio" id="create_contactlist[]" name="create_contactlist" value="1" > Yes
              </label>
              <label class="radio-inline" style="font-family:'IntelClearBold'">
                  <input type="radio" id="create_contactlist[]" name="create_contactlist" value="0" checked> No
              </label>
            </div>
          </div>
          <div id='contactlist-fields' style="display:none">
            <div class="form-group">
              <label for='contact-list-tag' class="col-md-4">Contact List Name</label>
              <div class="col-md-8">
                <input type="text" name='list_tag' class="form-control" id="list_tag" placeholder="Enter name *" value="" maxlength="120">
                <p>The List Name is a friend name for distinguishing the list.  (e.g. 2015 NRF)</p>
              </div>
            </div>

            <div class="form-group">
              <label for='contact-list-tag' class="col-md-4">Contact List Request Type</label>
              <div class="col-md-8">
                <select id="contact_list_request_type" name="contact_list_request_type" class="form-control">
                  <option value="">Select one *</option>
                  <option value="Contact"<?php if (isset($_SESSION['contact_list_request_type']) && $_SESSION['contact_list_request_type'] == 'Contact') { echo ' selected'; } ?>>Contact</option>
                  <option value="RAW"<?php if ( isset($_SESSION['contact_list_request_type']) && $_SESSION['contact_list_request_type'] == 'RAW') { echo ' selected'; } ?>>RAW</option>
                </select>
              </div>
            </div>

            <div class="row">
               <label class="col-md-4" style="text-align:right">Contact List Upload</label>
              <div class='col-md-8'>
                <div class="upload-div col-md-7" style="padding:0px;">
                  <div id="contacts" class="fileupload fileupload-new" data-provides="fileupload" style="padding-left:7px">
                    <span class="btn-file btn-green iconned-btn"><span class="fileupload-new">Upload a Contact List<span class="icon-plus-media"><i class='fa fa-plus'></i></span></span><input id="fileupload_contacts" type="file" name="files[]"></span>
                  </div>
                  <div id="progress-contacts" class="progress progress-success progress-striped pull-left span3" style="display:none;">
                      <div class="progress-bar bar"></div>
                  </div>
                  <div id="errors-contacts" class="files" style="clear:both;color:#cc0000;"></div>
                  <div id="shown-contacts" class="files" style="clear:both;"></div>
                </div>
                <span class="col-md-5" style='font-size:12px; padding:0;' for='placeholder-btn'>All the contact data that you upload will be purged from this portal 30 days after the upload per Intel's privacy guidelines</span>
              </div>
            </div>
          </div>
        </form>

      </div>
      <div role="tabpanel" class="tab-pane fade" id="existing-contacts">
        <form class="form-horizontal" id="webinar-audience-form-2">

          <div class="form-group">
            <label for="campaign_cta" class="control-label col-md-3">Audience Segmentation</label>
            <div class='col-md-8'>
              <div class="controls"><textarea id="existing_contact_list" name="existing_contact_list" class=" form-control textarea-medium" placeholder="Enter segmentation rules for existing contacts. E.g. 1) All Security ITP members with Platinum level from Germany. 2) Use contacts from Contact List.IDF2014.526"><?php echo getFieldValue('target_audience', isset($_SESSION['clone_campaign_id']) ? $_SESSION['clone_campaign_id'] : 0, 8, 'textarea'); ?></textarea><i class="helper-icon popover-link icon-question-sign textarea-align" data-container="body" data-toggle="popover" data-content="Example: We incorporated a number of conversion opportunities throughout the site, titled 'We're here to help, Ask an Expert.' Linked to a new Aprimo form."></i></div>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
  </div>
  <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
    <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="audience-save-exit" class="btn-blue" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 5" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0);" id="audience-save" class="btn-green submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 5" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
  </div>

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->