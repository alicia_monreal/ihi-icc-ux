<?php
session_start();
include('_globals.php');


if (!empty($_POST)) {
    $body = $mysqli->real_escape_string($_POST['body']);
    $body_email = cleanForEmail($_POST['body']); //ejh
    $request_id = $_POST['request_id'];
    $request_type_id = $mysqli->real_escape_string($_POST['request_type_id']);
    $the_title = getTitle($request_id, $request_type_id); //ejh
    $user_id = $_SESSION['user_id'];
    $recipients = ($_POST['recipients']); //ejh
    date_default_timezone_set('UTC');
    $date_created = date("Y-m-d H:i:s");
    $active = '1'; 

    $params = $request_id . ',' . $request_type_id . ',' . $user_id . ',"' . $body . '","' . $recipients . '","' . $date_created . '",' . $active;
    $query = "INSERT INTO `messages`(`id`,`request_id`, `request_type`, `user_id`, `body`, `recipients`, `date_created`, `active`) VALUES (NULL," . $params .  ")";
    $result = $mysqli->query($query);

    //Process recipients
    $query_messages = "SELECT users.email,users.fname,users.lname,users.id FROM users WHERE id IN (" . $recipients . ")";
    $result_messages = $mysqli->query($query_messages);
    while ($obj_msg = $result_messages->fetch_object()) {
        $mail->AddAddress($obj_msg->email, $obj_msg->fname.' '.$obj_msg->lname);
    }

    /*
    //Alternative way. ejh
    foreach($recipients AS $key=>$value) {
        $mail->AddAddress(getEmailAddress($value));
    }
    */

    //Send email. ejh
    $alt_as = '';
    $message_as = $message_header;

    $message_as.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">The following message has been added to ".$the_title." by ".getUserName($user_id).":</p><p>".$body_email."</p><p><a href=\"".$domain."/"; if ($request_type_id == 8) { $message_as.="campaign-detail"; } else { $message_as.="request-detail"; } $message_as.="?id=".$request_id."#messages\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
    $alt_as = 'The following message has been added to '.$the_title.': '.$body_email;

    $message_as.=$message_footer;

    $mail->SetFrom(getEmailAddress($user_id), getUserName($user_id));
    $mail->Subject = 'Intel Customer Connect: A new message has been added: '.$the_title;
    $mail->MsgHTML($message_as);
    $mail->AltBody = $alt_as;
    if (!empty($recipients)) {
        $mail->Send();
    }

    date_default_timezone_set('America/Los_Angeles');
    $attrvalue =  date('m/d/Y h:i A',strtotime("$date_created UTC")); //ejh
    echo json_encode(array('date_created'=>$date_created, 'lname'=> $_SESSION['lname'], 'fname'=> $_SESSION['fname'], 'date'=>$attrvalue));

} else {
    echo false;
}
?>
