<?php
@session_start();

function addAddtionalRecipients($mail, $additional_recipients, $email_ms){
	// additional recipients added in cc
    if(!empty($additional_recipients) && is_array($additional_recipients)) {
        foreach ($additional_recipients as $recipients) {
            if($email_ms == getEmailAddress($recipients))
                continue;

            $mail->AddCC(getEmailAddress($recipients), getUserName($recipients)); 
        }
    }
    return $mail;
}

function sendNotificationRulesEmail($mail, $request_type, $campaign_id, $request_id, $mysqli, $message_header, $message_footer, $domain, $mail3){
	/* Send email to notification user ids from default and normal rules */
    // get the user email from the default rule
    $query_default = "SELECT notification_user_ids, default_assignee
                   FROM notification_rules
                   WHERE request_type = ".$request_type." AND default_rule = 1 AND active = 1 ";
    
    $result_default = $mysqli->query($query_default);
    $user_ids = "";
    $default_assignee = "";
    // while ($obj_def = $result_default->fetch_object()) {
    //     $user_ids .= $obj_def->notification_user_ids.",";
    //     $default_assignee = $obj_def->default_assignee;
    // }
    // get the user email from normal rules
    $query_notification = "SELECT notification_user_ids, default_assignee
                   FROM request_campaign as campaign 
                   INNER JOIN notification_rules as rules 
                   ON campaign.geo = rules.geo AND campaign.sector = rules.sector 
                   WHERE campaign.id=".$campaign_id." AND rules.request_type = ".$request_type." AND rules.active = 1 ORDER BY last_updated DESC LIMIT 1";
    $result_not = $mysqli->query($query_notification);
    // while ($obj_not = $result_not->fetch_object()) {
    //     $user_ids .= $obj_not->notification_user_ids.",";
    //     if($obj_not->default_assignee != "")
    //         $default_assignee = $obj_not->default_assignee;
    // }
    $uids = explode(",", $user_ids);
    foreach ($uids as $user) {
        if($user !== ""){
            // add email address
            $mail->AddCC(getEmailAddress($user),getUserName($user));
        }
    }

    /* Send email to default assignee */
    if($default_assignee != ""){
        $update_request = "UPDATE request SET assigned_to = ".$default_assignee.", date_updated = NOW() WHERE id = ".$request_id;
        $mysqli->query($update_request);

        $alt_as = '';
        $the_title = getTitle($request_id, $request_type);
        $tactic_type = getTacticType($request_type);
        $message_as = $message_header;
        $message_as.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">You have been assigned to support the ".$tactic_type." request for ".$the_title.".</p><p><a href=\"".$domain."/"; if ($request_type == 8) { $message_as.="campaign-detail"; } else { $message_as.="request-detail"; } $message_as.="?id=".$request_id."\" style=\"color:#1570a6;\"><img src=\"".$domain."/img/btn-review-request.gif\" alt=\"Review request\"></a><br></p>";
        $alt_as = 'You have been assigned to support the '.$tactic_type.' request for '.$the_title;

        $message_as.=$message_footer;
        $mail3->AddAddress(getEmailAddress($default_assignee),getUserName($default_assignee));
        $mail3->Subject = 'Intel Customer Connect: You have been assigned: '.$the_title;
        $mail3->MsgHTML($message_as);
        $mail3->AltBody = $alt_as;
        $mail3->Send();
    }
    /******/
    return $mail;
}
?>