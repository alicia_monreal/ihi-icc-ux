<?php
$class_active = ' class="active"';
?>
<div id="wrap">
    <div class="auxbar">
		<div class="hfcontainer">
		  <div class="logo pull-left"><a href="http://www.intel.com" target="_blank" data-analytics-label="Header: Intel External"><img src="img/logo-intel-header.png" alt="Intel" id="intellogo"></a><label>Customer Connect</label></div>
          <?php if ($loc != 'login') { ?>
            <div class="kt-icon"><?php echo substr($_SESSION['fname'],0,1); ?><?php echo substr($_SESSION['lname'],0,1); ?></div>
            <div class="kt-menu-ctn">
                <div class="kt-menu">
                    <p><a href="my-account">My Account</a></p>
                    <p><a href="log-out">Logout</a></p>
                </div>
            </div>
          <?php } ?>
		</div>
        <input class="session-user-id" type="hidden" value="<?php echo $_SESSION['user_id']?>" >
    </div>
    <div class="topnav<?php if ($loc == 'login') { echo ' nomenu'; } if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { echo ' adminmenu'; } ?>">
        <div class="container" style="position: relative;">
			<?php if ($loc != 'login') { ?>
            <div class="links">
                <a href="home" nav='option'><div <?php if ($loc == 'home') { echo $class_active; } ?>><div class="icon home"></div>Home<div class="div-line"></div></div></a>
                <a href="learn" nav='option'><div id="learn-menu" <?php if ($loc == 'learn') { echo $class_active; } ?>>
                    <div class="icon learn"></div>Learn<div class="div-line"></div></div></a>

                <!-- Learn submenu -->
                <div class="learn-submenu">
                    <ul>
                        <a href="#"><li val="webinar-sm"><div class="icon submenu webinar-sm"></div>Webinar</li></a>
                        <a href="#"><li val="newsletter-sm"><div class="icon submenu newsletter-sm"></div>Newsletter</li></a>
                        <a href="#"><li val="nurture-sm"><div class="icon submenu nurture-sm"></div>Nurture Series</li></a>
                        <a href="#"><li val="mail-sm"><div class="icon submenu mail-sm"></div>Single Email</li></a>
                        <a href="#"><li val="page-sm"><div class="icon page-sm"></div>Landing Page/Form</li></a>
                        <!-- <a href="#"><li val="event"><div class="icon icon-event"></div>Event</li></a> -->
                        <a href="#"><li val="list-sm"><div class="icon submenu list-sm"></div>Upload Lists</li></a>
                    </ul>
                </div>

                <a href="plan" nav='option'><div <?php if ($loc == 'plan') { echo $class_active; } ?>><div class="icon plan"></div>Plan<div class="div-line"></div></div></a>
                <a href="request" nav='option'><div <?php if ($loc == 'request') { echo $class_active; } ?>><div class="icon request"></div>Request<div class="div-line"></div></div></a>
                <a href="review" nav='option'><div class="last-menu <?php if ($loc == 'review') { echo 'active'; } ?>"><div class="icon review"></div>Review<div class="div-line"></div></div></a>
    			
                <?php if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {  //Admins have second toolbar and special link up top ?>
                <a href="" class="manage-menu"><div class="admin-menu pull-right <?php if ($loc == 'manage') { echo 'active'; } ?>">Manage</div></a>
                <!-- Manage submenu -->
                <div class="manage-menu-ctn">
                    <div class="kt-menu">
                        <!--<p><a href="dashboard">Dashboard</a></p>-->
                        <p><a href="manage-users">Manage Users</a></p>
                        <p><a href="review">Manage Requests</a></p>
                        <p><a href="all-requests">All Requests</a></p>
                        <p><a href="campaigns">Manage Campaigns</a></p>
                        <p><a href="manage-resources">Manage Resources</a></p>
                        <p><a href="notification-rules">Notification Rules</a></p>
                    </div>
                </div>
                <?php } ?>
            </div>
    		<?php } ?>
        </div>
    </div>

    <!--
    <div class="tab pull-left"><a <?php /*if ($loc != 'login') { echo 'href="support"'; } else { echo 'href="#contact-us" data-toggle="modal"'; } */?> data-analytics-label="Header: Support">Support</a></div>
     -->