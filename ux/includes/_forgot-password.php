<?php
session_start();
include('_globals.php');

if (!empty($_POST) && !empty($_POST['email_forgot'])) {

    //Values
    $email = $mysqli->real_escape_string($_POST['email_forgot']);

    $query = "SELECT password FROM users WHERE email = '".$email."' AND active = 1 ORDER BY date_created DESC LIMIT 1";
    $result = $mysqli->query($query);
    $row_cnt = $result->num_rows;

    if ($row_cnt > 0) {
        while ($obj = $result->fetch_object()) {
            $password = $obj->password;
        }

        //Send email to user
        $message_user = $message_header;
        $message_user.= "<p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Your login information for Intel Customer Connect is:</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\"><strong>Username:</strong> <a href=\"mailto:".$email."\" style=\"color:#1570a6;\">".$email."</a><br /><strong>Password:</strong> ".$password."</p><p style=\"font-family:Arial,Helvetica,Verdana,sans-serif;font-size:18px;color:#464646;\">Thank you.</p>";
        $message_user.=$message_footer;

        $mail2->AddAddress($email);
        $mail2->Subject = 'Intel Customer Connect: Forgot Password';
        $mail2->MsgHTML($message_user);
        $mail2->AltBody = 'Your login information for Intel Customer Connect is: '.$email.'  :  '.$password.'';

        //Send the message, check for errors
        if(!$mail2->Send()) {
            echo 'Sorry, there was a problem sending the email. Please <a href="mailto:info@customerconnect.intel.com">contact us</a>.';
            die();
        }


    	echo 'An email was sent with your login information. If you don\'t receive it, please check your spam folder. <a href="" data-dismiss="modal">Close window</a>';
    } else {
        //echo print_r($query);
        echo 'That email was not found in our system or your account has not been activated yet. For questions, <a href="mailto:info@customerconnect.intel.com">contact us</a>. <a href="" data-dismiss="modal">Close window</a>'; 
    }

    /* close connection */
    $mysqli->close();
} else {
    echo 'There has been a problem.';  
}
?>