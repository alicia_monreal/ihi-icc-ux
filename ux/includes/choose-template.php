<div id='choose-template' class="modal fade">
  <div class="modal-dialog icc-modal-dialog">
    <div class="modal-content" style="padding-left:5%;padding-right:5%;">
            <div class="modal-header modal-tabs">
              <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 0;top:15px;margin-right: 15px;">
              <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Choose Template and Layout Configuration</h2>
            </div>

        <div class="modal-body" style="padding-top:30px;margin: 0 4% 0 4%;">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="details" style="display: block !important; visibility: visible;">
                  
                  <form id="choose-template-form" class="content-update-form form-horizontal">
                    <div class="fieldgroup" style="border-bottom:0;">
                      <div class="control-group">
                        <div class="row">
                          <div id="template-standard" class="template-selector gray-selector pull-left span6<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Standard Template') { echo ' active'; } ?>">
                            <input type="radio" id="template[]" name="template" value="Standard Template"<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Standard Template') { echo ' checked'; } ?> style="display:none;" class="required">
                            <img src="img/templates/intel-template-tmb.png" alt="Standard Template">
                            <div style="width:215px;line-height:17px;text-align:justify;" class="pull-right">
                              <h4>Intel Template</h4>
                              Use the approved HQ template for emails and landing pages. Customize the assets to meet your needs with the Asset Editor.
                            </div>
                          </div>
                          <div id="template-custom" class="template-selector gray-selector pull-left span6<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' active'; } ?>">
                            <input type="radio" id="template[]" name="template" value="Custom Template"<?php if (getFieldValue('template', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'Custom Template') { echo ' checked'; } ?> style="margin-top:51px; display:none;" class="required">
                            <div style="width:420px;line-height:17px;text-align:justify;" class="pull-right">
                              <h4>Custom Template</h4>
                              If your GEO or group has its own template, or your agency has already built out your assets for you, you can select this option to upload your own HTML content and image assets.
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
        <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
          <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="choose-template-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Choose Template and Layout Configuration Request: Step 3" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0);" id="choose-template-save" class="btn-green submit" style="font-size: 16px !important;" data-analytics-label="Choose Template and Layout Configuration Request: Step 3">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
        </div>
        <!-- <div class="modal-footer" style="bottom: 0;text-align: center;width: 100%;right: 0%; margin-bottom: 20px; font-size: 16px !important;">
             <a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="javascript:void(0);" id="upload-list-setup-save-exit" class="btn-blue submit" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Exit</a><a href="javascript:void(0)" id="upload-list-setup-save" class="btn-green" style="font-size: 16px !important;" data-analytics-label="Submit Form: Webinar Request: Step 2" val="<?php echo $tactic_name ?>">Save and Continue &nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
        </div> -->
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- End - Campaign Setup Modal -->
