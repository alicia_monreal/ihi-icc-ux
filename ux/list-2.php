<?php
$loc = 'request';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

$tactic_name="leadlist";	

unset($_SESSION['in_progress_id']);
unset($_SESSION['tactic_request_id']);
?>

<div class='container'>

	<div class='row'>
		<div class='col-md-12'>
			<div class='request-header'>
				<h2>Upload a List</h2>
				<h4>
					Follow the steps to build and launch your landing page.
				</h4>
			</div>
		</div>
	</div>
	<div class='row row-centered'>

		<!-- Campaign Setup -->
		<div class='col-md-3 col-centered box-button-container'>
			<a href='#campaign-setup' data-toggle='modal' data-backdrop="static" data-target='#campaign-setup' class="lnk existing-campaign-setup">
				<div class='box-button icon-small' val='campaign-setup'>
					<div class='circled'>
						<div class="icon campaign-setup" style='height:28px !important; margin-top:36%;'></div>
					</div>
					<h3 class='spaced'>Campaign Setup</h3>
					<div class='step-number'><i class='fa fa-2x'>1</i></div>
				</div>
			</a>
		</div>

		<!-- Upload a List -->
		<div class='col-md-3 col-centered box-button-container'>
			<a href='#leadlist-setup' data-target='#leadlist-setup' class="lnk box-button-setup">
				<div class='box-button icon-small inactive' val='setup'>
					<div class='circled'>
						<div class="icon setup"></div>
					</div>
					<h3 class='spaced'>Upload List</h3>
					<div class='step-number'><i class='fa fa-2x'>2</i></div>
				</div>
			</a>
		</div>

	</div>	

	<?php include('includes/campaign-details.php'); ?>
	<?php include('includes/upload-list.php'); ?>

</div>

<div class="push"></div>
<?php include('includes/footer.php'); ?>