<?php
$loc = 'request';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

<div class="container">
	<div class='row'>
		<div class='col-md-12'>
			<div class='request-header'>
				<h2>Create a eNurture</h2>
				<h4>
					Is this a new enurture, a duplicate of an existing enurture, or are you completing a request in process?
				</h4>
			</div>
		</div>
	</div>
</div>

<div class='row row-centered'>
	<div class='col-md-3 col-centered box-button-container'>
		<a href="enurture-2" class="lnk">
			<div class='box-button' val='new-request'>
				<div class='circled'>
					<div class="icon new-request"></div>
				</div>
				<h3>New Request</h3>
			</div>
		</a>
	</div>

	<div class='col-md-3 col-centered box-button-container'>
		<a href="#duplicate-request" data-toggle="modal" data-target="#duplicate-request" class="lnk">
			<div class='box-button' val='duplicate'>
				<div class='circled'>
					<div class="icon duplicate"></div>
				</div>
				<h3>Duplicate<br/>Existing Request</h3>
			</div>
		</a>
	</div>

	<div class='col-md-3 col-centered box-button-container'>
		<a href="#inprogress-request" data-toggle="modal" data-target="#inprogress-request" class="lnk">
			<div class='box-button' val='request-progress'>
				<div class='circled'>
					<div class="icon request-progress"></div>
				</div>
				<h3 class='logo'>Complete a<br/>Request in Process</h3>
			</div>
		</a>
	</div>
</div>


<!-- Duplicate Tactic -->
<div id='duplicate-request' class="modal fade">
	<div class="modal-dialog icc-modal-dialog">
	    <div class="modal-content icc-modal-content">
		    <div class="modal-header icc-modal-header">
		        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="">
		        <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Duplicate an Exisiting Request</h2>
		        <h4 class="request-form-subtitle">
			        All aspects of this request will be duplicated except for the dates and tactic name. You can edit any attribute as needed.
		        </h4>
		    </div>

		    <div class="modal-body" style="padding-bottom:200px">
		    	<form class="form-horizontal" id="content-update-form" action="includes/_requests_route.php" method="POST" enctype="multipart/form-data">
		    		<div class="form-group">
		    			<label for="tag" class="col-md-3 control-label">Select the request to duplicate</label>
		    			<div class="col-md-8">
		    				<input type="text" id="request_search" name="request_search" class="input-wide required" placeholder="Start typing the name or keywords from the request name" value="">
		    			</div>
		    		</div>
		    	</form>
		    </div>

		    <div class="modal-footer icc-modal-footer" style="">
		    	<a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="" id="submit-request-form" class="btn-green submit">Duplicate Request</a>
		    </div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End - Duplicate Tactic -->

<!-- In Progress Tactic -->
<div id='inprogress-request' class="modal fade">
	<div class="modal-dialog icc-modal-dialog">
	    <div class="modal-content icc-modal-content">
		    <div class="modal-header icc-modal-header">
		        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true" style="">
		        <h2 id="requestLabel" class="request-form-label" style='margin-bottom:0'>Complete a Request in Progress</h2>
		        <h4 class="request-form-subtitle">
			        Find the request you'd like to complete below.
		        </h4>
		    </div>

		    <div class="modal-body" style="padding-bottom:200px">
		    	<form class="form-horizontal" id="content-update-form" action="includes/_requests_route.php" method="POST" enctype="multipart/form-data">
		    		<div class="form-group">
		    			<label for="tag" class="col-md-3 control-label">Select the request to complete</label>
		    			<div class="col-md-8">
		    				<?php echo getInProgressRequests($_SESSION['user_id'], 6, $_SESSION['admin']); ?>
		    			</div>
		    		</div>
		    	</form>
		    </div>

		    <div class="modal-footer icc-modal-footer" style="">
		    	<a href='#' class="lnk" style="padding-right:5px" data-dismiss="modal">Cancel</a><a href="" id="submit-request-form" class="btn-green submit">Complete Request</a>
		    </div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End - Duplicate Tactic -->

<div class="push"></div>
<?php include('includes/footer.php'); ?>

<!-- Typeahead -->
<script type="text/javascript">
	$(document).ready(function() {
      	var substringMatcher = function(strs) {
          return function findMatches(q, cb) {
            var matches, substringRegex;
            matches = [];
            substrRegex = new RegExp(q, 'i');

            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                matches.push({ value: str });
              }
            });

            cb(matches);
          };
        };

		request_vals = <?php echo json_encode(getRequestNameArray(6)); ?>;
        var requests = request_vals;

        $('#request_search').typeahead({
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          //name: 'requests',
          displayKey: 'value',
          source: substringMatcher(requests)
        });
    });
</script>