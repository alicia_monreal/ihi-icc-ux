<?php
$loc = 'request';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

<div class="container">
	
	<div class='row'>
		<div class='col-md-12'>
			<div class='request-header'>
				<h2>Upload a List</h2>
				<h4>
					Is this a new list, a duplicate of an existing, or are you completing a request in process?
				</h4>
			</div>
		</div>
	</div>

</div>

<div class='row row-centered'>
		<div class='col-md-3 col-centered box-button-container'>
			<a href='list-2' class="lnk">
				<div class='box-button' val='new-request'>
					
					<div class='circled'>
					<div class="icon new-request"></div>
				</div>
					<h3>New Request</h3>

				</div>
			</a>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<div class='box-button' val='duplicate'>
				<div class='circled'>
				<div class="icon duplicate"></div>
			</div>
			
				<h3>Duplicate<br/>Existing Request</h3>
		
			</div>
		</div>

		<div class='col-md-3 col-centered box-button-container'>
			<div class='box-button' val='request-progress'>
				<div class='circled'>
				<div class="icon request-progress"></div>
			</div>
				<h3 class='logo'>Complete a<br/>Request in Process</h3>
			</div>
		</div>
</div>

<div class="push"></div>
<?php include('includes/footer.php'); ?>