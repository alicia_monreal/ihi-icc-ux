<?php
session_start();
//Redirect to Home if not admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/home');
    die();
}

$loc = 'notification-rules';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="request-header">
                    <?php if (!isset($_GET['id']) && !isset($_GET['action'])) { ?><div class="header-btn"><a href="notification-rules?action=create" class="btn-green">Create a New Rule &nbsp;&nbsp;<i class="fa fa-plus"></i></a></div><?php } ?>
                	<?php if (isset($_GET['action']) && $_GET['action'] ==='create' && !isset($_GET['default'])) { 
                        echo '<h2>Create a New Rule</h2><h4>Use the form below to create a new notification rule.</h4>'; 
                    } else if(isset($_GET['action']) && $_GET['action'] === 'create' && isset($_GET['default'])){
                        echo '<h2>Create a Default Rule</h2><h4>Use the form below to create a default notification rule.</h4>'; 
                    } else if($_GET['action'] !== "create" && isset($_GET['default'])){
                        echo '<h2>Default Rule</h2><p>&nbsp;</p>'; 
                    } else { 
                        echo '<h2>Notification Rules</h2><h4>Create and manage rules for notification emails.</h4>'; 
                    } 
                    ?>
                </div>        
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php include('includes/rules-list.php'); ?>
            </div>
        </div>
    </div>
    <script>
    function deleteResource(url) {
        if (window.confirm("Are you sure you want to delete this notification rule?")) { 
            window.location.href = url;
        }
    }

    function removeThisUser(removeDiv) {   
        /* this function is used in the registration form functionality */
        // remove the input choice
        $(removeDiv).remove();
        return false;
    }

    </script>

<div class="push"></div>
<?php include('includes/footer.php'); ?>