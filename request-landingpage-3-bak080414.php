<?php
$loc = 'campaign-requests';
$step = 3;
$c_type = 'landingpage';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Landing Page Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fourup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li class="active">Step 3</li>
				<li>Step 4</li>
			</ul>
			<div class="request-step step3">
				<div class="fieldgroup">
					<p>Please provide any necessary assets to be used with this tactic.  Assets must be provided 24 hours in advance before any asset creation period can begin.</p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p>
					<h3 class="fieldgroup-header">Tactic Assets</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_landingpage.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="3">
					<input type="hidden" id="c_type" name="c_type" value="landingpage">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					
					<div class="fieldgroup">
						<div class="control-group">
							<label for="customassets" class="control-label no-padding">Tactic Assets</label>
							<div class="controls">
								<p>Please provide the html template and associated graphical assets for your email. Please zip all assets into a single package and name each asset appropriately (naming should reflect asset location).</p>
								<div class="upload-div">
								    <div class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Asset Zip</span><input id="fileupload_customassets" type="file" name="files[]"></span>
									</div>
									<div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 239px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-customassets" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_customassets'])) {
								    		echo $_SESSION['files_customassets'];
            							}
            							?>
								    </div>
								</div>
							</div>
						</div>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-landingpage-2" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Landing Page Request: Step 3">Next</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>