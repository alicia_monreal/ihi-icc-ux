<?php
$loc = 'briefing-docs';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
                <h1>Briefing Docs</h1>
                <p>Begin here to create your website update request. Follow the links below to find the briefing doc form for the page you want to create or modify. The forms specify the exact details of content, including headlines, character counts, links and other assets required to create or update each page, and provide fields for entering your information.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row docs">
            <div class="span4">
                <h3 style="border-bottom:0;margin-bottom:38px;"><a href="files/100 Home.docx" data-analytics-label="Download File: 100 Home.docx">Homepage</a></h3>
                <h3>Intelligent Systems</h3>
                <ul>
                	<li><a href="files/200 Intelligent Systems.docx" data-analytics-label="Download File: 200 Intelligent Systems.docx">Landing Page</a>
                		<ul>
                			<li><a href="files/210 Success Stories Tool.docx" data-analytics-label="Download File: 210 Success Stories Tool.docx">Customer Success Stories</a></li>
                			<li><a href="files/211 Success Story Detail.docx" data-analytics-label="Download File: 211 Success Story Detail.docx">Customer Success Stories Detail</a></li>
                		</ul></li>
                </ul>
                <h3>Products &amp; Solutions</h3>
                <ul>
                    <li><a href="files/300 Products &amp; Solutions.docx" data-analytics-label="Download File: 300 Products &amp; Solutions.docx">Landing Page</a>
                        <ul>
                            <li><a href="files/320 WE Product Overview.docx" data-analytics-label="Download File: 320 WE Product Overview.docx">WE Products</a></li>
                            <li><a href="files/321 WE Product Family.docx" data-analytics-label="Download File: 321 WE Product Family.docx">WE Products Family</a></li>
                            <li><a href="files/3210 WE Product Detail A.docx" data-analytics-label="Download File: 3210 WE Product Detail A.docx">WE Products Detail (Standard)</a></li>
                            <li><a href="files/3210 WE Product Detail B.docx" data-analytics-label="Download File: 3210 WE Product Detail B.docx">WE Products Detail (Auto)</a></li>
                            <li><a href="files/340 Product Lifecycle.docx" data-analytics-label="Download File: 340 Product Lifecycle.docx">Product Lifecycles</a></li>
                            <li><a href="files/360 Download.docx" data-analytics-label="Download File: 360 Download.docx">Downloads</a></li>
                            <li><a href="files/330 Purchase.docx" data-analytics-label="Download File: 330 Purchase.docx">Purchase</a></li>
                            <li><a href="files/350 WE Support.docx" data-analytics-label="Download File: 350 WE Support.docx">Support</a></li>
                        </ul></li>
                </ul>
            </div>
             <div class="span4">
                <h3>Industries</h3>
                <ul>
                    <li><a href="files/400 Industries.docx" data-analytics-label="Download File: 400 Industries.docx">Landing Page</a>
                        <ul>
                            <li><a href="files/410a Industry Detail.docx" data-analytics-label="Download File: 410a Industry Detail.docx">Industry Detail (Standard)</a></li>
                            <li><a href="files/410b Industry Detail.docx" data-analytics-label="Download File: 410b Industry Detail.docx">Industry Detail (Auto)</a></li>
                        </ul></li>
                </ul>
                <h3>Partners</h3>
                <ul>
                    <li><a href="files/500 Partner Tool.docx" data-analytics-label="Download File: 500 Partner Tool.docx">Landing Page</a>
                        <ul>
                            <li><a href="files/501 Partner Detail.docx" data-analytics-label="Download File: 501 Partner Detail.docx">Partner Detail</a></li>
                            <li><a href="files/530 Become A Partner.docx" data-analytics-label="Download File: 530 Become A Partner.docx">Become a Partner</a></li>
                            <li><a href="files/520 For Partners.docx" data-analytics-label="Download File: 520 For Partners.docx">For Partners</a></li>
                        </ul></li>
                </ul>
                <h3>Community</h3>
                <ul>
                    <li><a href="files/600 Community.docx" data-analytics-label="Download File: 600 Community.docx">Landing Page</a>
                        <ul>
                            <li><a href="files/620 Community Events.docx" data-analytics-label="Download File: 620 Community Events.docx">Events</a></li>
                            <li><a href="files/630 Technical References.docx" data-analytics-label="Download File: 630 Technical References.docx">Technical Reference</a></li>
                        </ul></li>
                </ul>
            </div>
             <div class="span4">
                <h3>Campaign Pages</h3>
                <ul>
                    <li><a href="files/070 Campaign Page.docx" data-analytics-label="Download File: 070 Campaign Page.docx">Standard Campaign</a></li>
                    <li><a href="files/071 Nurture Campaign Page.docx" data-analytics-label="Download File: 071 Nurture Campaign Page.docx">Nurture Campaign</a></li>
                    <li><a href="files/072 Unlock Intelligence Campaign Page.docx" data-analytics-label="Download File: 072 Unlock Intelligence Campaign Page.docx">Unlock Intelligence Campaign</a></li>
                </ul>
                <h3>Resources</h3>
                <ul>
                    <li><a href="files/000 Resources.docx" data-analytics-label="Download File: 000 Resources.docx">Landing Page</a></li>
                </ul>
            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>