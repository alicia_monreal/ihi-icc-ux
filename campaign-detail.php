<?php
$loc = 'campaigns';
include('includes/head.php');
include('includes/header.php');
require_once('includes/_globals.php');

$edit = false;
if (isset($_GET['edit'])) {
    $edit = true;
}
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
                <?php
                //We're doing this inline instead of an include. Getting too crowded in there.
                $query_camps = "SELECT
                            req.*,
                            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                            status.value AS status_value,
                            req.request_type AS request_type_id,
                            request_type.request_type AS request_type,
                            users.fname AS fname,
                            users.lname AS lname,
                            users.email AS email,
                            request_campaign.id AS campaign_id
                        FROM request AS req
                        INNER JOIN status on req.status = status.id
                        INNER JOIN request_type on req.request_type = request_type.id
                        INNER JOIN users on req.user_id = users.id
                        INNER JOIN request_campaign on req.id = request_campaign.request_id";

                //This would be the All Requests view for every user. So losing this permission for now
                /*if ($_SESSION['admin'] == 1) {
                    $query.=" WHERE req.active = 1";
                } else {
                    $query.=" WHERE req.user_id = '".$_SESSION['user_id']."' AND req.active = 1";
                }*/

                $query_camps.=" WHERE req.active = 1";

                $query_camps.=" AND req.id = '".$_GET['id']."' LIMIT 1";
                
                $result_camps = $mysqli->query($query_camps);
                $row_cnt_camps = $result_camps->num_rows;

                //print_r($query);

                if ($row_cnt_camps > 0) {
                    //Show a message?
                    if (isset($_GET['update'])) {
                        if ($_GET['update'] == 'success') {
                            echo '<p style="color:#ff0000;"><strong>Campaign has been updated.</strong><br /></p>';
                        } else if ($_GET['update'] == 'success') {
                            echo '<p style="color:#ff0000;"><strong>We\'re sorry, there has been a problem updating this campaign.</strong><br /></p>';
                        }
                    }
                    if (isset($_GET['action'])) {
                        if ($_GET['action'] == 'success') {
                            echo '<p style="color:#ff0000;"><strong>Your campaign has been submitted.</strong><br /></p>';
                        }
                    }
        
                    while ($obj_camps = $result_camps->fetch_object()) {
                        //We need to know if this person owns this
                        if ($_SESSION['admin'] == 1 || $obj_camps->user_id == $_SESSION['user_id']) { $owner = true; } else { $owner = false; }

                        /*
                        if ($owner == true) {
                            if ($edit == false) {
                                echo '<div class="pull-right"><a href="campaign-detail?id='.$_GET['id'].'&edit=1" class="btn-green download">Make changes</a></div>';
                            } else {
                                echo '<div class="pull-right"><a href="campaign-detail?id='.$_GET['id'].'" class="btn-green download">Finish changes</a></div>';
                            }
                        }
                        */

                        //Admins get a form
                        if ($_SESSION['admin'] == 1 || $edit) {
                            echo '<form id="content-update-form-inline" action="includes/_requests_update.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" id="action" name="action" value="update">';
                                if ($edit) { echo '<input type="hidden" id="edit_form" name="edit_form" value="true">'; }
                                echo '<input type="hidden" id="user_id" name="user_id" value="'.$obj_camps->user_id.'">
                                <input type="hidden" id="admin_id" name="admin_id" value="'.$_SESSION['user_id'].'">
                                <input type="hidden" id="request_id" name="request_id" value="'.$_GET['id'].'">
                                <input type="hidden" id="request_type_id" name="request_type_id" value="'.$obj_camps->request_type_id.'">
                                <input type="hidden" id="user_email" name="user_email" value="'.getEmailAddress($obj_camps->user_id).'">';
                        }
                        
                        echo '<div id="zebra" class="single-request">';
                        echo '<h1>'.getTitle($_GET['id'], 8).'</h1>'; 

                        if ($_SESSION['admin'] == 1) {
                            echo '<div class="row row-border"><label>Requestor</label> <div class="pull-left">'.$obj_camps->fname.' '.$obj_camps->lname.' / <a href="mailto:'.$obj_camps->email.'">'.$obj_camps->email.'</a></div></div>';
                        }
                        echo '<div class="row'; if ($_SESSION['admin'] != 1) { echo ' row-border'; } echo '"><label>Date Created</label> <div class="pull-left">'.$obj_camps->date_created.'</div></div>';

                        //Admin can change requestor
                        if ($_SESSION['admin'] == 1) {
                        echo '<div class="row"><label>Change Requestor</label> <div class="pull-left">';
                            $query_users = "SELECT id, fname, lname FROM users WHERE active = 1 ORDER BY fname ASC";
                            $result_users = $mysqli->query($query_users);
                            echo '<select id="new_user_id" name="new_user_id">';
                            while ($obj_users = $result_users->fetch_object()) {
                                echo '<option value="'.$obj_users->id.'"';
                                if ($obj_camps->user_id == $obj_users->id) {
                                    echo ' selected="selected"';
                                }
                                echo '>'.$obj_users->fname.' '.$obj_users->lname.'</option>';
                            }
                            echo '</select>';
                        echo '</div></div>';
                        } else {
                            echo '<input type="hidden" id="new_user_id" name="new_user_id" value="'.$obj->user_id.'">';
                        }

                        echo '<div class="row"><label>Status</label> <div class="pull-left">';
                        //Admin can change status
                        if ($_SESSION['admin'] == 1) {
                            $query_status = "SELECT * FROM status ORDER BY sort_order ASC";
                            $result_status = $mysqli->query($query_status);
                            echo '<select id="status_update" name="status_update">';
                            while ($obj_s = $result_status->fetch_object()) {
                                $belongs_to = explode(',', $obj_s->belongs_to);
                                if (in_array($obj_camps->request_type_id, $belongs_to)) {
                                    echo '<option value="'.$obj_s->id.'"';
                                    if ($obj_camps->status == $obj_s->id) {
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$obj_s->value.'</option>';
                                }
                            }
                            echo '</select>';
                        } else {
                            echo '<input type="hidden" id="status_update" name="status_update" value="'.$obj_camps->status.'">'; //For new Edit
                            echo $obj_camps->status_value;
                        }
                        echo '</div></div>';
                        echo '<div class="row"><label>Assigned To</label> <div class="pull-left">';
                        //Admin can change status
                        if ($_SESSION['admin'] == 1) {
                            echo getAssignedTo($obj_camps->assigned_to, 1);
                        } else {
                            echo '<input type="hidden" id="assigned_to" name="assigned_to" value="'.$obj_camps->assigned_to.'">'; //For new Edit
                            echo getAssignedTo($obj_camps->assigned_to, 0);
                        }
                        echo '</div></div>';
                        if ($_SESSION['admin'] == 1) {
                            echo '<p><br /><a href="" id="submit-request-form-inline" class="btn-green submit pull-right">Submit Updates</a></p>';
                        }
                        echo '<h1><br />Campaign Details';
                        if ($owner == true) {
                            if ($edit == false) {
                                echo '<a href="campaign-detail?id='.$_GET['id'].'&edit=1" class="btn-green download" style="margin-left:14px;">Edit</a>';
                            } else {
                                echo '<a href="campaign-detail?id='.$_GET['id'].'" class="btn-green download" style="margin-left:14px;">Finish Edits</a>';
                            }
                        }
                        echo '</h1>';

                        //Now show the details of the particular type of request
                        echo getRequestDetails($obj_camps->id, $obj_camps->request_type_id, $edit, $owner);

                        //Notes
                        //Grab notes
                        $notes = '<ul class="notes">';
                        $query_notes = "SELECT user_id, notes, col, DATE_FORMAT(date_created, '%m/%d/%Y %l:%i%p') AS date_created_notes FROM notes WHERE request_id = '".$obj_camps->id."' AND active = 1 ORDER BY date_created DESC";
                        $result_notes = $mysqli->query($query_notes);
                        $row_cnt_notes = $result_notes->num_rows;
                        if ($row_cnt_notes > 0) {
                            while ($obj_n = $result_notes->fetch_object()) {
                                $col_clean = str_replace('_', ' ', $obj_n->col);
                                if ($obj_n->notes == 'Value updated') {
                                    $notes.='<li>'.ucwords($col_clean).' Updated | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                } else {
                                    $notes.='<li>'.nl2br($obj_n->notes).' | '.$obj_n->date_created_notes.' ('.getUserName($obj_n->user_id).')</li>';
                                }
                            }
                        } else {
                            $notes.='<li>No notes</li>';
                        }
                        $notes.='</ul>';
                        echo '<div class="row"><label>Notes</label> <div class="pull-left" style="width:680px;">'.$notes;
                        //Admin can add notes
                        if ($_SESSION['admin'] == 1 && $edit == true) {
                            echo '<a href="#" id="notes" class="editable" data-type="textarea" data-pk="'.$_GET['id'].'" data-url="/includes/_requests_update_inline.php?request_type_id='.$obj_camps->request_type_id.'&type=notes">Add a note</a>';
                        }
                        echo '</div></div>';

                        echo '</div>'; //End single-request

                        //Create this
                        $campaign_id = $obj_camps->campaign_id;
                    }

                    //Associated tactics
                    echo '<h1><br />Associated Campaign Tactics</h1>';

                    $query = "SELECT
                            req.*,
                            DATE_FORMAT( req.date_created,  '%m/%d/%Y' ) AS date_created,
                            DATE_FORMAT( req.date_completed,  '%m/%d/%Y' ) AS date_completed,
                            status.value AS status_value,
                            request_type.request_type AS request_type,
                            request_type.id AS request_type_id,
                            users.fname AS fname,
                            users.lname AS lname,
                            users.email AS email
                        FROM request AS req
                        INNER JOIN status on req.status = status.id
                        INNER JOIN request_type on req.request_type = request_type.id
                        INNER JOIN users on req.user_id = users.id
                        WHERE req.request_type != 8
                        AND req.campaign_id = '".$campaign_id."'
                        AND req.active = 1
                        ORDER BY req.id DESC";
                    
                    $result = $mysqli->query($query);
                    $row_cnt = $result->num_rows;

                    if ($row_cnt > 0) {
                        echo '<table class="table table-striped table-bordered table-hover tablesorter requests-table">';
                        echo '<thead><th style="width:114px;">Created <b class="icon-white"></b></th><th>Tactic Name <b class="icon-white"></b></th><th>Owner <b class="icon-white"></b></th><th>Type <b class="icon-white"></b></th><th>Marketing ID <b class="icon-white"></b></th><th>Status <b class="icon-white"></b></th></thead>';
                        echo '<tbody>';
                        while ($obj = $result->fetch_object()) {
                            $marketing_id = getMarketingId($obj->id, $obj->request_type_id);

                            if ($marketing_id != '') {
                                $marketing_id = $marketing_id;
                            } else {
                                $marketing_id = '--';
                            }

                            echo '<tr>';
                            echo '<td>'.$obj->date_created.'</td>';
                            echo '<td><a href="tactic-detail?id='.$obj->id.'" class="notrack">'.getTitle($obj->id, $obj->request_type_id).'</a></td>';
                            echo '<td>'.$obj->fname.' '.$obj->lname.'</td>';
                            echo '<td>'.$obj->request_type.'</td>';
                            echo '<td>'.$marketing_id.'</td>';
                            echo '<td style="white-space:nowrap;"><div style="display:none;">'.$obj->status_value.'</div>'.$obj->status_value.'</td>';
                            //echo '<td>'.$analytics.'</td>';
                            //echo '<td>'.$survey_average.'</td>';
                            echo '</tr>';
                        }
                        echo '</tbody></table>';
                    } else {
                        echo '<p><strong>There are currently no active tactics associated with this campaign.</strong></p>';
                    }
                }

                if (isset($_GET['action']) && $_GET['action'] == 'success') {
                    echo '<p><br /><br /><a href="requests" class="btn-green back pull-left">My Requests</a>';
                } else {
                    echo '<p><br /><br /><a href="campaigns" class="btn-green back pull-left">Back to list</a>';
                }

                /*
                if ($owner == true) {
                    if ($edit == false) {
                        echo '<a href="campaign-detail?id='.$_GET['id'].'&edit=1" class="btn-green download pull-right">Make changes</a>';
                    } else {
                        echo '<a href="campaign-detail?id='.$_GET['id'].'" class="btn-green download pull-right">Finish changes</a>';
                    }
                }
                */

                if ($_SESSION['admin'] == 1 || $edit == 1) {
                    //echo '<a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Campaign Update">Submit updates</a></form></div>';
                    echo '</p></form></div>';
                } else {
                    echo '</p></div>';
                }
                ?>

            </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>