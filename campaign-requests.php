<?php
$loc = 'campaign-requests';
$step = 0;
include('includes/head.php');
include('includes/header.php');
?>

	<div class="container">
		<div id="content-update" class="row intro-body">
			<div class="intro">
				<h1>Request a Campaign</h1>
				<p>Setting up a new campaign is easy. Simply fill out the campaign request form, attach your assets, and let us do the rest.</p>
			</div>

				<div class="request-step step0">
					<form id="content-update-form" class="form-horizontal form-taut" action="includes/_requests.php" method="POST" enctype="multipart/form-data">
						<input type="hidden" id="action" name="action" value="start">
						<input type="hidden" id="step" name="step" value="0">
						<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
						<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">

						<div class="fieldgroup">
							<div class="control-group">
								<label for="geo" class="control-label">Target GEO</label>
								<div class="controls">
									<label class="checkbox inline"> <input type="checkbox" id="selectall" name="selectall" value="" class="checkall"> Select all</label>
									<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="APJ" <?php if (in_array('APJ', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> APJ</label>
									<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="EMEA" <?php if (in_array('EMEA', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> EMEA</label>
									<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="LAR" <?php if (in_array('LAR', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> LAR</label>
									<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="NAR" <?php if (in_array('NAR', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> NAR</label>
									<label class="checkbox inline"> <input type="checkbox" id="geo[]" name="geo[]" value="PRC" <?php if (in_array('PRC', $_SESSION['geo'])) { echo 'checked'; } ?> class="check-geo"> PRC</label>
								</div>
							</div>
							<div class="control-group">
								<label for="sector" class="control-label">Sector</label>
								<div class="controls"><span class="required-mark">*</span><select id="sector" name="sector" class="input-wide required">
										<option value="">Select One</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label for="subsector" class="control-label">Sub-Sector</label>
								<div class="controls"><span class="required-mark">*</span><select id="subsector" name="subsector" class="input-wide required">
										<option value="">Select Sector First</option>
									</select><br />
									<input type="text" id="subsector_other" name="subsector_other" class="input-wide" placeholder="Please enter the other sub-sector" value="<?php echo $_SESSION['subsector_other']; ?>" style="clear:both;margin-top:10px;display:none;" <?php if (!isset($_SESSION['subsector']) || $_SESSION['subsector'] != 'Other') { echo 'disabled'; } ?>>
								</div>
							</div>
							<div class="control-group">
								<label for="type" class="control-label">Type of Campaign</label>
								<div class="controls">
									<span class="required-mark">*</span><select id="type" name="type" class="input-wide required">
										<option value="">Choose a Campaign Type</option>
										<option value="webinar">Webinar (50+ people)</option>
										<option value="enurture">eNurture</option>
										<option value="event">Event</option>
									</select>
								</div>
							</div>

							<!--These will be hidden till selected-->
							<div id="additional-details" class="hide">
								<div id="additional-details-select" class="control-group hide">
									<label for="campaign_details" class="control-label">Additional Campaign Details</label>
									<div class="controls">
										<span class="required-mark">*</span><select id="campaign_details" name="campaign_details" class="input-wide">
											<option value="">Choose Campaign Details</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label for="language" class="control-label">Language Localization</label>
									<div class="controls">
										<select id="language" name="language" class="input-wide">
											<option value="English" selected>English</option>
										</select>
										<div class="input-note">Currently English is the only language supported. Additional languages will be added in the future.</div>
									</div>
								</div>
							</div>

						</div>

						<!--
						<div class="fieldgroup">
							<label for="email-template">Select your landing page and email template from the options below.<br /><span style="font-weight:normal;">Download the campaign brief template to see the editable fields for each template option.</span></label>
							<img src="img/templates/email-template-one.png" alt="Intel Default" style="margin-bottom:6px;"><br />
							<label class="radio inline"> <input type="radio" id="email_template" name="email_template" value="Intel Default" checked="checked"> Intel Default</label>
						</div>
						<div class="fieldgroup">
							<label for="continue">If you haven't filled out the campaign brief, select DOWNLOAD below.<br />If you do have completed templates, select CONTINUE.</label>
							<p style="margin:24px 0 40px 4px;"><a href="files/Intel Customer Connect - Intel Standard Email and Registration Content.docx" class="btn-green download" data-analytics-label="Download: Content Templates" style="margin-right:5px;">Download</a><a href="" id="submit-request-form" class="btn-green submit" data-analytics-label="Submit Form: Campaign Request Step 1" style="margin-left:5px;">Continue</a></p>
						</div>
						-->
					</form>
				</div>

				<!--Webinars-->
	            <div id="tabbed-webinars" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-webinars">
						<li class="active"><a href="#webinars-need">What You'll Need to Know</a></li>
						<li><a href="#webinars-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="webinars-need">
							<ul style="margin-left: 50px;">
								<li>This form is for English-language webinars with 50+ attendees. For fewer than 50 attendees, use Lync.</li>
								<li>Your presentation slides, resource list assets, and videos may be uploaded up to 72 hours before your scheduled event. The campaign brief must be submitted when creating your request.  Please ensure that your assets are finalized when you upload them. Changing files after production has begun can result in significant delays to your project.</li>
							</ul>
						</div>

						<div class="tab-pane" id="webinars-assets">
							<ul style="margin-left: 50px;">
								<li>Campaign Brief (Required): This form includes all the content for your registration page and emails, including specs for optional images. <a href="/files/Intel%20Customer%20Connect%20-%20Intel%20Standard%20Email%20and%20Registration%20Content.docx" class="blue-download">DOWNLOAD<i class="icon-download-blue"></i></a></li>
								<li>Webinar Presentation Template (Required): Your webinar presentation must use this template, and consist of a single PPTX file of not more than 25MB. <a href="/files/Intel_Presentation Template_16x9.potx" class="blue-download">DOWNLOAD<i class="icon-download-blue"></i></a></li>
								<li>Contact List Template (Optional): Should you want to upload your own list of contacts to invite to the webinar, please use the following template specific to each Sector. <a href="/files/Reseller AM Upload Template.xlsx" class="blue-download">RESELLER<i class="icon-download-blue"></i></a>&nbsp;&nbsp;<a href="/files/Embedded AM Upload Template.xlsx" class="blue-download">EMBEDDED<i class="icon-download-blue"></i></a>&nbsp;&nbsp;<a href="/files/ITDM AM Upload Template.xlsx" class="blue-download">ITDM<i class="icon-download-blue"></i></a></li>
								<li>Registration Images (Optional): You may upload custom images for your registration pages and emails. For sizing, see the campaign brief. </li>
								<li>Speaker Photo(s) (Optional): You may upload one or more speaker images to be included within your presentation. Recommended size: 80 px x 90 px</li>
								<li>Survey Template (Optional): This form outlines the format for the optional Survey module. <a href="/files/Intel Customer Connect - Survey Template.docx" class="blue-download">DOWNLOAD<i class="icon-download-blue"></i></a></li>
								<li>Additional Assets (Optional): You will need to provide any videos, URLS, podcasts, links, or other assets you want to be included in your webinar.</li>
								<li>Video Assets (Optional): You may upload up to 20 .flv or .mp4 video files to play during the webinar. They cannot be over 25MB and the recommended dimensions are 640x360, 480x270 or 320x180.</li>
							</ul>
						</div>
					</div>
				</div>

				<!--eNurture-->
	            <div id="tabbed-enurture" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-enurture">
						<li class="active"><a href="#enurture-need">What You'll Need to Know</a></li>
						<li><a href="#enurture-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="enurture-need">
							Need content
						</div>

						<div class="tab-pane" id="enurture-assets">
							Need content
						</div>
					</div>
				</div>


				<div class="fieldgroup" style="text-align:center;">
					<a href="" id="submit-request-form" class="btn-green submit-disabled">Save &amp; Continue</a>
				</div>

				<div class="fieldgroup"></div>

		</div>
	</div>


<?php include('includes/footer.php'); ?>

