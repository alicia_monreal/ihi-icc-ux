<?php
$loc = 'solutions';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
                <h1>Solutions</h1>
                <p>Intel Customer Connect allows Intel marketing managers and their agency partners to set up and manage integrated webinar and e-nurture campaigns. Select a role or solution below for more information and selected resources.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

        	<?php
            //if ($_SESSION['access_blocked'] == true) { //currently not working, always returning 1, tried count() too
            	//echo '<p style="font-weight:bold;">You currently don\'t have access to this area. Please contact support if you would like access.</p>';
            //} else {
            ?>

        	<!--Solutions-->
            <div id="tabbed-solutions" class="tabbed-contents span12" style="margin-bottom:60px;">
            	<ul class="nav nav-tabs" id="tabs-solutions">
					<?php if(in_array(1, $_SESSION['access_control'])) { ?><li class="active"<?php if (count($_SESSION['access_control']) == 1) { echo ' style="width:100%;"'; } ?>><a href="#webinars-sol">Webinars</a></li><?php } ?>
					<?php if(in_array(2, $_SESSION['access_control'])) { ?><li<?php if (count($_SESSION['access_control']) == 1) { echo ' class="active" style="width:100%;"'; } ?>><a href="#marketing-automation-sol">Marketing Automation</a></li><?php } ?>
				</ul>
				 
				<div class="tab-content">
					<?php if(in_array(1, $_SESSION['access_control'])) { ?>
					<div class="tab-pane active" id="webinars-sol">
						<div class="video-player single-player" style="height:auto;padding:30px 0 30px;">
							<div class="single">
								<!--<iframe width="711" height="400" src="//www.youtube.com/embed/wMf_P6nFAno?rel=0" frameborder="0" allowfullscreen id="video-two-source"></iframe>
								<h3>New Marketing Automation Capability (2:56)</h3>-->

								<div style="width:588px;height:410px;margin:0 auto;">
									<!-- Start of Brightcove Player -->

									<div style="display:none">
									First player to be used on the ICC portal. MikeLB 10/2014. Set with Domain restriction. 
									</div>

									<!--
									By use of this code snippet, I agree to the Brightcove Publisher T and C 
									found at https://accounts.brightcove.com/en/terms-and-conditions/. 
									-->

									<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

									<object id="myExperience" class="BrightcoveExperience">
									  <param name="bgcolor" value="#e9e9e9" />
									  <param name="width" value="588" />
									  <param name="height" value="410" />
									  <param name="playerID" value="3855503661001" />
									  <param name="playerKey" value="AQ~~,AAADgrkekSE~,QyVebA-ZGr1jrYWtAZ4BkwBYBRZi5qv0" />
									  <param name="isVid" value="true" />
									  <param name="isUI" value="true" />
									  <param name="dynamicStreaming" value="true" />
									  
									</object>

									<!-- 
									This script tag will cause the Brightcove Players defined above it to be created as soon
									as the line is read by the browser. If you wish to have the player instantiated only after
									the rest of the HTML is processed and the page load is complete, remove the line.
									-->
									<script type="text/javascript">brightcove.createExperiences();</script>

									<!-- End of Brightcove Player -->

									<!--<h3 style="margin-left:auto;text-align:center;">New ICC Asset Editor (7:12)</h3>-->
								</div>
							</div>
						</div>

						<!--<div class="video-player">
							<div class="left">
								<div id="video-one" class="video-itself">
									<iframe width="460" height="259" src="//www.youtube.com/embed/t1O9vdHbgJg?rel=0" frameborder="0" allowfullscreen id="video-one-source"></iframe>
									<h3>ICC - Webinar Capability (2:16)</h3>
								</div>
							</div>
							<div class="right">
								<ul>
									<li><a href="//www.youtube.com/embed/U5w0mXHkMwk?rel=0" data-target="video-one" class="yt-link"><div class="yt-image"><img src="//i1.ytimg.com/vi/U5w0mXHkMwk/default.jpg"></div> <div class="yt-title">ICC - Getting Started (2:21)</div></a></li>
									<li><a href="//www.youtube.com/embed/t1O9vdHbgJg?rel=0" data-target="video-one" class="yt-link"><div class="yt-image"><img src="//i1.ytimg.com/vi/t1O9vdHbgJg/default.jpg"></div> <div class="yt-title">ICC - Webinar Capability (2:16)</div></a></li>
								</ul>
							</div>
						</div>-->

						<p>Once a one-off communications tool, today's webinars are an essential piece of the B2B sales pipeline. The best webinars develop meaningful conversations that can continue to be nurtured even after the webinar ends.</p>

						<p>To create successful webinar campaigns, you need to be able to deliver the right content to the right audience segments at the right touch points, and you need robust analytics so that you can continuously optimize this engagement. Intel's new marketing automation platform, Eloqua, makes this kind of integrated marketing possible. </p>

						<p>With ON24 and Eloqua, you can create engaging webinar campaigns for more than 50 people that automatically track and nurture attendees from registration through post-event communications.</p>

						<p>This site contains everything you need to set up and manage those campaigns. Below are select resources to get you started.</p>
						<p>&nbsp;</p>
						<h3 class="small-header">Key Resources</h3>
						<div class="row docs">
				            <div class="span6">
				                <ul>

				                	<?php 
							            // solutions page resources loaded dynamically
			                            $query = "SELECT resources.link,resources.title,resources.locations
			                                      FROM resources
			                                      WHERE resources.active = 1 ORDER BY resources.publish_date ASC";
			                            $result = $mysqli->query($query);

			                            while ($obj = $result->fetch_object()) {
			                                $locations = explode(", ", $obj->locations);

			                                if(in_array(3,$locations)) { // location 3 represents solution-webinar page
			                                    echo '<li>
			                                    		  <a href="'.$obj->link.'">'.$obj->title.'</a>
			                                          </li>';
			                                }
			                            }

				                	?>
				             
				                </ul>
				            </div>
				        </div>
					</div>
					<?php } ?>

					<?php if(in_array(2, $_SESSION['access_control'])) { ?>
					<div class="tab-pane<?php if (count($_SESSION['access_control']) == 1) { echo " active"; } ?>" id="marketing-automation-sol">
						<div class="video-player single-player" style="height:auto;padding:30px 0 30px;">
							<div class="single">
								<!--<iframe width="711" height="400" src="//www.youtube.com/embed/wMf_P6nFAno?rel=0" frameborder="0" allowfullscreen id="video-two-source"></iframe>
								<h3>New Marketing Automation Capability (2:56)</h3>-->

								<div style="width:588px;height:410px;margin:0 auto;">
									<!-- Start of Brightcove Player -->

									<div style="display:none">
									First player to be used on the ICC portal. MikeLB 10/2014. Set with Domain restriction. 
									</div>

									<!--
									By use of this code snippet, I agree to the Brightcove Publisher T and C 
									found at https://accounts.brightcove.com/en/terms-and-conditions/. 
									-->

									<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

									<object id="myExperience" class="BrightcoveExperience">
									  <param name="bgcolor" value="#e9e9e9" />
									  <param name="width" value="588" />
									  <param name="height" value="410" />
									  <param name="playerID" value="3855503661001" />
									  <param name="playerKey" value="AQ~~,AAADgrkekSE~,QyVebA-ZGr1jrYWtAZ4BkwBYBRZi5qv0" />
									  <param name="isVid" value="true" />
									  <param name="isUI" value="true" />
									  <param name="dynamicStreaming" value="true" />
									  
									</object>

									<!-- 
									This script tag will cause the Brightcove Players defined above it to be created as soon
									as the line is read by the browser. If you wish to have the player instantiated only after
									the rest of the HTML is processed and the page load is complete, remove the line.
									-->
									<script type="text/javascript">brightcove.createExperiences();</script>

									<!-- End of Brightcove Player -->

									<!--<h3 style="margin-left:auto;text-align:center;">New ICC Asset Editor (7:12)</h3>-->
								</div>
							</div>
						</div>

						<p>Marketing automation allows you to easily and automatically nurture prospects in a meaningful way while learning more about their unique interests and needs. Intel's new marketing automation platform, Eloqua, automatically tracks communications with prospects and creates rich profiles that can be used as a basis for personalized follow-up communications.</p>

						<p>This site contains everything you need to set up and manage those e-nurture campaigns. Below are select resources to get you started.</p>
						<p>&nbsp;</p>
						<h3 class="small-header">Key Resources</h3>
						<div class="row docs">
							<div class="span6">
				                <ul>
				                	<?php 
							            // solutions page resources loaded dynamically
			                            $query = "SELECT resources.link,resources.title,resources.locations
			                                      FROM resources
			                                      WHERE resources.active = 1 ORDER BY resources.publish_date ASC";
			                            $result = $mysqli->query($query);

			                            while ($obj = $result->fetch_object()) {
			                                $locations = explode(", ", $obj->locations);

			                                if(in_array(4,$locations)) { // location 4 represents solution-marketing-automation page
			                                    echo '<li>
			                                    		  <a href="'.$obj->link.'">'.$obj->title.'</a>
			                                          </li>';
			                                }
			                            }

				                	?>
				                </ul>
				            </div>
				        </div>
					</div>
					<?php } ?>
				</div>
            </div>
            <!--End Solutions-->

            <?php //} //if access blocked ?>

        </div>
    </div>

<?php include('includes/footer.php'); ?>