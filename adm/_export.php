<?php
session_start();

// SET HEADER INFO - WE'RE DOWNLOADING AN XLS FILE
header("Pragma: cache");
header("Content-Type: application/ms-excel");
header("Content-Disposition: attachment; filename=ICC-Export-".date('m_d_Y').".xls");

// ECHO DATA TO HEADERS
echo $_SESSION['xls_data'];
?>