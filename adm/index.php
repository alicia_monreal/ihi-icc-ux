<?php
die();
session_start();

/*$valid_ips = array('24.21.195.77','71.231.56.30');
$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
if (!in_array($ip,$valid_ips)) {
    header('Location:http://www.getwindowsembedded8.com');
    exit();
}*/

//die();

$link = mysqli_connect('localhost', 'getwin8','P5Rh96fa6fpwakOT');

$_SESSION['xls_data'] = "OEMCRM Lead ID (Lead)\tMSI Customer ID (Lead)\tSalutation (Lead)\tFirst Name (Lead)\tLast Name (Lead)\tOther Name (Lead)\tJob Title (Lead)\tCompany Name (Lead)\tOther Company Name (Lead)\tSource Campaign (Lead)\tLead Status (Lead)\tMQPDate (Lead)\tProspect Score (Lead)\tBusiness Email (Lead)\tBusiness Phone (Lead)\tHome Phone (Lead)\tMobile Phone (Lead)\tOther Phone (Lead)\tFax (Lead)\tWeb Site (Lead)\tAddress Line 1 (Lead)\tAddress Line 2 (Lead)\tCity (Lead)\tState/Province (Lead)\tZIP/Postal Code (Lead)\tCountry (Lead)\tOED Region (Lead)\tPreferred Communication Method (Lead)\tAllow Microsoft E-Mail (Lead)\tAllow Microsoft Phone Call (Lead)\tAllow Microsoft Postal Mail (Lead)\tAllow Partner E-Mail (Lead)\tAllow Partner Phone Call (Lead)\tAllow Partner Postal Mail (Lead)\tDescription (Lead)\tCompany Type (Lead)\tCompany Type Other (Lead)\tContact Specific Role (Lead)\tContact Specific Role Other (Lead)\tToolkit Purchase (Lead)\tPast License (Lead)\tWindows Embedded Knowledge Level (Lead)\tSales Channel (Lead)\tExperience with MS Partners and Distributors (Lead)\tRedList Validation Flag (Lead)\tRedList Date (Lead)\tRedList Employee Name validating the list (Lead)\tRedList Firm validating the list (Lead)\tDNCList Validation Flag (Lead)\tDNCList Date (Lead)\tDNCList Employee Name validating the list (Lead)\tDNCList Firm validating the list (Lead)\tMarketing Activity ID\tGSXID\tName\tSource Campaign\tCampaign Code\tActivity Date\tLocation\tDistributor/Partner\tAdditional Distributor/Partner(s)\tMarketing Activity Type\tMarketing Activity Type Other\tHow did you hear about this event?\tPrimary Reason for Attending\tPrimary Reason for Attending Other\tDevice Category\tDevice Sub-Category\tDevice Sub Category Other\tDevice Name\tDevice Description\tVertical Industry\tPrimary Goal of Project\tPrimary Goal of Project Other\tWhen will Project Begin?\tDevelopment Stage\tTeam Role on Project\tHow Many Devices Plan to Ship?\tPast OS Experience\tPast OS Experience Other\tWhich Distributors have you worked with?\tWhich Distributors have you worked with Other\tWindows Embedded Promotion Code\tWindows Embedded Interest\tCPU Architecture Under Consideration\tCPU Architecture Under Consideration - Other\tCPU Architecture - Final Decision\tOS Under Consideration\tOS Under Consideration-Other\tOS Final Decision\tOS Timeframe\tCompetitive OS\tDescription";

$_SESSION['xls_data'].="\n";

$query = "SELECT
			id,
			fname,
			lname,
			title,
			company,
			email,
			country,
			report,
			date_created
		FROM	getwin8.leads
		WHERE	active = 1
		ORDER BY	date_created  ASC";

if ($stmt = mysqli_prepare($link, $query)) {

	mysqli_stmt_execute($stmt);
    mysqli_stmt_store_result($stmt);
    mysqli_stmt_bind_result($stmt,$id,$fname,$lname,$title,$company,$email,$country,$report,$date_created);

    while (mysqli_stmt_fetch($stmt)) {
    	if ($fname != '') {

    		//Change name of report. Per Jossie. 04/10/13
    		$report_clean = '';
    		switch ($report) {
			    case 'Gartner':
			        $report_clean = 'retail';
			        break;
			    case 'ARC Strategies':
			        $report_clean = 'manufacturing';
			        break;
			    case 'Ovum':
			        $report_clean = 'health';
			        break;
			}

	$_SESSION['xls_data'].="\t\t\t".$fname."\t".$lname."\t\t".$title."\t".$company."\t\t\t\t\t\t".$email."\t\t\t\t\t\t\t\t\t\t\t\t".$country."\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t".$date_created."\t\t\t\t\tWindows 8 Launch website\t".$report_clean."\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

			$_SESSION['xls_data'].="\n";
		}
    }
	
	mysqli_stmt_close($stmt);
}

//echo $_SESSION['xls_data'];
echo '<p><a href="_export.php">Export .xls</a></p>';


mysqli_close($link);
?>