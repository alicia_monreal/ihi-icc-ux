<?php
//turn on verbose error reporting (15) to see all warnings and errors
error_reporting(15);

//define a generic object
class config {};

//setup the configuration object
$CFG = new config;

$CFG->dbhost	= 'localhost';
$CFG->dbname	= '';
$CFG->dbuser	= '';
$CFG->dbpass	= '';

//initialize the SESSION variable if necessary 
session_start();

//connect to the database
//$db_conn = mysql_connect($CFG->dbhost, $CFG->dbname, $CFG->dbuser, $CFG->dbpass);
$mysqli = new mysqli($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname);
?>