<?php
$loc = 'campaign-requests';
$step = 2;
$c_type = 'landingpage';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}

?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Landing Page Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fourup">
				<li>Step 1</li>
				<li class="active">Step 2</li>
				<li>Step 3</li>
				<li>Step 4</li>
			</ul>
			<div class="request-step step2">
				<div class="fieldgroup">
					<p>Please fill out the tactic details below and a member of production support will contact you shortly to discuss your request. </p>
					<p><span style="color:#cc0000;">*</span> Fields marked with an asterisk are required.</p><p>Click on the <i class="icon-question-sign" style="margin-top: 3px;"></i> for more information and examples.</p>
					<h3 class="fieldgroup-header">Tactic Details</h3>
				</div>
				
				<form id="content-update-form" class="form-horizontal" action="includes/_requests_landingpage.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="2">
					<input type="hidden" id="c_type" name="c_type" value="landingpage">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">
					<input type="hidden" id="in_progress_form" name="in_progress_form" value="0">

					<div class="fieldgroup">
						<?php include('includes/tactic-basics.php'); ?>
						<div class="control-group">
							<label for="start_date" class="control-label">Landing Page Start Date</label>
							<div class="controls">
								<span class="required-mark">*</span><div id="datetimepicker" class="input-append date" style=""><input type="text" id="start_date" name="start_date" class="input-medium required" style="width:134px;" value="<?php echo $_SESSION['start_date']; ?>"><span class="add-on" style="margin-right:31px;"><i class="icon-th"></i></span></div>
								<span style="font-weight: 600;">&nbsp;End Date</span><span class="required-mark" style="margin-left:5px">*</span> <div id="datetimepicker_end" class="input-append date" style=""><input type="text" id="end_date" name="end_date" class="input-medium required" style="width:134px;" value="<?php echo $_SESSION['end_date']; ?>"><span class="add-on"><i class="icon-th"></i></span></div>
							</div>
						</div>
						<div class="control-group">
							<label for="description" class="control-label">Description</label>
							<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="description" name="description" class="input-wide textarea-medium required"><?php echo getFieldValue('description', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
						</div>
						<div class="control-group">
							<label for="asset_reviewers" class="control-label">Asset Reviewers</label>
							<div class="controls"><textarea id="asset_reviewers" name="asset_reviewers" class="input-wide textarea-medium" placeholder="Provide a list of names and email addresses for the approval process."><?php echo getFieldValue('asset_reviewers', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea></div>
						</div>
					</div>

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request" class="btn-green back pull-left">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" style="margin-left:8px;">Next</a><a href="" id="save-and-exit" class="btn-green submit plain pull-right">Save &amp; Exit</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>