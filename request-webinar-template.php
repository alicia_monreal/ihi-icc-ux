<?php

$loc = 'campaign-requests';
$step = 3;
$c_type = 'campaign';

// if(isset($_GET['legacy']))
    include('includes/editorheadx.php');
// else
    // include('includes/editorhead.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
// if (!isset($_SESSION['campaign_id'])) {
    // header('Location:request?error=request-id');
    // echo "Initiate a request";
    // exit();
?>

<div class="container-fluid">
    <div class="row intro-body" style="margin-bottom:1px;">
        <div class="intro span12">
            <h1>Customize template</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3 tpl-sidebar">
            <h4>Headers to show</h4>

            <div class="tpl-head-select">
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerA"> Header A
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerB"> Header B
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerC"> Header C
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerD"> Header D
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerE"> Header E
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerF"> Header F
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerG"> Header G
                </label>
                <label>
                  <input type="checkbox" class="tpl-head-checkbox" data-attr="headerH"> Header H
                </label>

            </div>

            <br/><h4>Modules to show</h4>
            <div class="tpl-head-select">
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="image_sidebar"> Image in Sidebar
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="location_sidebar"> Location in Sidebar
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="offers_content"> Offers in Content
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="standard_content"> Standard Content
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="standard_cta"> Standard Content with CTA
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="three_speaker"> Speaker Module (3 column)
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="two_speaker"> Speaker Module (2 column)
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="three_agenda"> Agenda Module (3 column)
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="two_agenda"> Agenda Module (2 column)
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="one_agenda"> Agenda Module (standard)
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="location"> Location Module (2 column)
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="event_summary"> Event Summary
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="no_content"> No Content
                </label>
                <label>
                  <input type="checkbox" class="tpl-cont-checkbox" data-attr="intel_badge"> Intel Badge Module
                </label>
            </div>

            <br/><h4>Select Footer</h4>
            <div class="tpl-head-select">
                <label>
                  <input type="checkbox" class="tpl-foot-checkbox" data-attr="standard_foot"> Standard
                </label>
                <label>
                  <input type="checkbox" class="tpl-foot-checkbox" data-attr="badge_foot"> Badge
                </label>
            </div>


        </div>
        <div class="span9 tpl-editor" >
            <div class="tpl-head">

                <!-- <div class="tpl-inj-space" style="border-bottom:0;padding-top:10px;padding-bottom:10px;"></div> -->

                <!--Header B-->
                <!-- <div class="tpl-container">
                    <?php //include('email_headers/headerB.php'); ?>
                </div> -->

                <!-- Space Injected -->
<!--                 <div class="tpl-inj-space" style="border-bottom:0;padding-top:10px;padding-bottom:10px;"></div>
 -->
                <!--Header C-->

            </div>


            <div class="tpl-modules">

            </div>
            <div class="tpl-foot">

            </div>


        </div>
    </div>

    <div class="tpl-actions row-fluid">
        <p style="margin:0 0 40px 0;" class="pull-right">
            <input type="submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Update User Account" value="Cancel">
            <input type="submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Update User Account" value="Save & Continue">
        </p>
    </div>

</div>






