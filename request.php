<?php
include('includes/_globals.php');
$loc = 'campaign-requests';
$step = 0;
include('includes/head.php');
include('includes/header.php');

//Kill some session vars
killSessionVars();
?>

	<div class="container">
		<div id="content-update" class="row intro-body" style="margin-bottom:0;">
			<div class="intro">
				<h1>Submit Request</h1>
				<p>Choose the type of request you wish to submit.  A production support team will assist in fulfilling your request.  If you do not see the request activity you are looking for or have questions, please contact us at <a href="mailto:support@customerconnect.intel.com">support@customerconnect.intel.com</a>.</p>

				<p>All tactic and list upload requests require you to have a campaign to associate the request with. Non-demand gen webinar request are an exception to this requirement and you can submit this request without first creating a campaign. <a href="img/campaign-hierarchy.png" class="cbox-image" title="Learn about the campaign hierarchy"><i class="icon-picture" style="margin-top: 3px;margin-right: 2px;"></i> Learn about the campaign hierarchy</a></p>

				<?php /*<p>If you are looking for reporting on Leads, please go to <a href="http://mybi.intel.com" target="_blank">http://mybi.intel.com</a> and request an account. For E2E reporting or any other reporting needs, please send an email to <a href="mailto:reporting@customerconnect.intel.com">reporting@customerconnect.intel.com</a> with your request.</p> */?>

				<p style="padding-top:14px;"><i class="icon-picture" style="margin-top: 3px;margin-right: 4px;"></i><a href="img/guide.png" class="cbox-image" title="Need help getting started?">Need help getting started?</a></p>

				<p style="padding-top:4px;"><i class="icon-info-sign" style="margin-top: 3px;margin-right: 4px;"></i><a class="popover-link" style="cursor:pointer;" data-container="body" data-toggle="popover" data-trigger="hover" data-content="If you are looking for reporting on Leads, please go to <a href='http://mybi.intel.com' target='_blank'>http://mybi.intel.com</a> and request an account. For E2E reporting or any other reporting needs, please <a href='mailto:reporting@customerconnect.intel.com'>send an email</a> with your request. For more info see the <a href='http://wiki.ith.intel.com/pages/viewpage.action?pageId=264280254' target='_blank'>Online Sales Playbook</a>.">How do I get reporting for my tactics / campaigns?</a></p>
				<a href="javascript:void(0)" id="placeholder-btn" class="btn-green plain pull-right">Add Placeholder Tactic</a>
				<?php
				if (isset($_GET['error'])) {
					if ($_GET['error'] == 'request-id') {
						echo '<p class="error">You must create a new Campaign ID or select an existing Campaign to proceed.</p>';
					}
				}
				if (isset($_GET['action'])) {
					if ($_GET['action'] == 'failure') {
						echo '<p class="error">There has been a problem with your request, please <a href="mailto:support@customerconnect.intel.com">contact support</a>.</p>';
					}
					if ($_GET['action'] == 'emailed') {
						echo '<p class="error">Thank you for submitting your request, we\'ll get back to you shortly.</p>';
					}
				}
				?>
			</div>
		


				<div class="request-step step0">
					<form id="content-update-form" class="form-horizontal form-taut" action="includes/_requests_route.php" method="POST" enctype="multipart/form-data" style="margin-bottom:0;">
						<input type="hidden" id="action" name="action" value="start">
						<input type="hidden" id="step" name="step" value="0">
						<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
						<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">

						<div id="select-request" class="fieldgroup filedgroup-header-class" style="padding-top:0px;">
							<h3 class="fieldgroup-header">Select a Request</h3>
						</div>

						<div id="select-request-content" class="fieldgroup" style="border:none;padding-bottom:0;">
							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="8" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create a Campaign <em class="sla-notice pull-right">SLA: 1 business day.</em></h4>
									<p>Campaigns are a series of planned marketing tactics over a specified time frame.  All other types of requests must have a campaign to be associated to.  Creating a campaign or choosing an existing campaign is always the first step to executing a tactic.</p>
									<div id="cloned-container-campaign" class="control-group hide">
										<div id="cloned-container-group" class="control-group">
											<label for="cloned_campaign" class="control-label">Is this a new campaign, or a duplicate of an existing one?</label>
											<div class="controls">
												<div id="eloqua-question" style="margin-bottom:12px;">
													<label class="radio inline"> <input type="radio" id="cloned_campaign[]" name="cloned_campaign" value="0" <?php if (!isset($_SESSION['cloned_campaign']) || $_SESSION['cloned_campaign'] == '0') { echo 'checked'; } ?>> New Campaign</label>
													<label class="radio inline"> <input type="radio" id="cloned_campaign[]" name="cloned_campaign" value="1" <?php if ($_SESSION['cloned_campaign'] == '1') { echo 'checked'; } ?>> Duplicate Campaign</label><br />
												</div>
											</div>
										</div>
										<div id="request-search-campaign" class="control-group hide">
											<label for="request_search_campaign" class="control-label">Select the campaign to duplicate</label>
											<div class="controls">
												<span class="required-mark">*</span><input type="text" id="request_search_campaign" name="request_search_campaign" class="input-wide required" style="width:80%;" placeholder="Start typing the GEOs or keywords from the campaign name" value="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="add-contacts" class="fieldgroup filedgroup-header-class" style="padding-top:0px;">
							<h3 class="fieldgroup-header">Upload Responses/Prospects/Leads</h3>
						</div>

						<div id="add-contacts-content" class="fieldgroup" style="border:none;padding-bottom:0;">
							<div class="control-group request-type-group">
								<p style="padding-bottom: 12px;margin-top: -20px;"><i class="icon-info-sign" style="margin-top: 3px;margin-right: 4px;"></i><a href="/files/Contact_Lead List Request Processes.pptx">Learn more about contact / lead / prospect assessment list upload processes</a></p>
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="4" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Upload a Contact List <em class="sla-notice pull-right">SLA: 1 business day / 1.5 business days for RAWs.</em></h4>
									<p>Uploading lists allows us to enter additional contacts into the database for future targeting.</p>
								</div>
							</div>

							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="9" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Upload a Lead List or Prospect Assessment <em class="sla-notice pull-right">SLA: 1 business day / 1.5 business days for RAWs.</em></h4>
									<p>Uploading lists allows us to enter additional leads into the database for future targeting.</p>
								</div>
							</div>
						</div>

						<div id="add-tactics" class="fieldgroup filedgroup-header-class" style="padding-top:0px;">
							<h3 class="fieldgroup-header">Add Tactics to a Campaign</h3>
						</div>

						<div id="add-tactics-content" class="fieldgroup" style="border:none;padding-bottom:0;">
							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="1" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create a Webinar <em class="sla-notice pull-right">SLA: 10 business days.</em></h4>
									<p>The webinar tactic will allow you to utilize the ON24 platform to engage prospects, as well as deliver registration, reminder, and follow up emails.</p>
								</div>
							</div>

							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="5" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create an email series based on single template (Newsletter) <em class="sla-notice pull-right">SLA: defined per project basis.</em></h4>
									<p>A newsletter email series will use a single email template from which the newsletter will be delivered to the target audience on a defined cadence.</p>
								</div>
							</div>

							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="2" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create a configurable email series (eNurture) <em class="sla-notice pull-right">SLA: defined per project basis.</em></h4>
									<p>An eNurture email series allows you to create emails with flexible layout and delivery rules.</p>
								</div>
							</div>

							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="6" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create a single email <em class="sla-notice pull-right">SLA: 4 business days.</em></h4>
									<p>The single email tactic will allow you to send an adhoc email to your target audience.</p>
								</div>
							</div>

							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="7" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create a landing page <em class="sla-notice pull-right">SLA: 5 business days.</em></h4>
									<p>A landing page provides a landing point to collect prospects.</p>
								</div>
							</div>

							<div class="control-group request-type-group">
								<div class="cr-icon pull-left">
									<p><input type="radio" id="request_type[]" name="request_type" value="999999" class="required"></p>
								</div>
								<div class="cr-description pull-left">
									<h4>Create a Lead Form <em class="sla-notice pull-right">SLA: defined per project basis.</em></h4>
									<p>A lead form provides a landing page to collect leads. This form can include BANT questions.</p>
								</div>
							</div>
						</div>

						<div>
							<div id="show_all_types" class="hide" style="margin-left: 120px;margin-top: -12px;"><a href="" id="show_all_types_link">+ Show all requests</a></div>
						</div>

						<!--These will be hidden till selected-->
						<div id="additional-details" class="hide" style="margin-bottom:38px;">
							<div class="fieldgroup" style="padding-top:0px;">
								<h3 class="fieldgroup-header">Campaign Requirement</h3>
							</div>
							<div class="control-group">
								<p style="margin-top:10px;">You are required to have a campaign with completed attributes (with the exception of non-demand gen webinars) before we can begin creating assets for any tactic or upload any list. If you do not have a campaign to associate your request to, please select "Create a Campaign" from the above list of requests to enter a new campaign into the system. If the campaign you are associating the request to exists, but is incomplete, you must update the campaign from the "My Requests" tab (if you are the campaign creator), or contact the campaign creator to complete their submission.</p>
							</div>
							<?php
							/*Removing per Hubert. 10/27/14
							<div id="eloqua-integrate" class="control-group hide">
								<label for="eloqua_integrate" class="control-label">Is your webinar meant to generate leads and/or have &gt;50 attendees?</label>
								<div class="controls">
									<div id="eloqua-question" style="margin-bottom:12px;">
										<label class="radio inline"> <input type="radio" id="eloqua_integrate[]" name="eloqua_integrate" value="1" <?php if (!isset($_SESSION['eloqua_integrate']) || $_SESSION['eloqua_integrate'] == '1') { echo 'checked'; } ?>> Yes</label>
										<label class="radio inline"> <input type="radio" id="eloqua_integrate[]" name="eloqua_integrate" value="0" <?php if ($_SESSION['eloqua_integrate'] == '0') { echo 'checked'; } ?>> No</label><br />
									</div>
								</div>
							</div>
							*/
							?>
							<div id="leads-question" class="control-group hide">
								<label for="generate_leads_q" class="control-label">Will this tactic generate leads<span id="leads-question-webinar" class="hide"><!-- and/or have &gt;50 attendees--></span>?</label>
								<div class="controls">
									<div id="leads-questions" style="margin-bottom:12px;">
										<label class="radio inline"> <input type="radio" id="generate_leads_q[]" name="generate_leads_q" value="1" <?php if (!isset($_SESSION['generate_leads']) || $_SESSION['generate_leads'] == '1') { echo 'checked'; } ?>> Yes</label>
										<label class="radio inline"> <input type="radio" id="generate_leads_q[]" name="generate_leads_q" value="0" <?php if ($_SESSION['generate_leads'] == '0') { echo 'checked'; } ?>> No</label><br />
									</div>
								</div>
							</div>
							<div id="itp-question" class="control-group hide">
								<label for="itp_audience" class="control-label">Is this meant for an ITP audience?</label>
								<div class="controls">
									<div id="itp-questions" style="margin-bottom:12px;">
										<label class="radio inline"> <input type="radio" id="itp_audience[]" name="itp_audience" value="1" <?php if ($_SESSION['itp_audience'] == '1') { echo 'checked'; } ?>> Yes</label>
										<label class="radio inline"> <input type="radio" id="itp_audience[]" name="itp_audience" value="0" <?php if (!isset($_SESSION['itp_audience']) || $_SESSION['itp_audience'] == '0') { echo 'checked'; } ?>> No</label><br />
									</div>
								</div>
							</div>
							<div id="geography-select" class="control-group hide">
								<label for="target_geography" class="control-label">What is the target geography for this tactic?</label>
								<div class="controls">
									<select id="target_geography" name="target_geography" class="input-wide">
										<option value="APJ">APJ</option>
										<option value="ASMO">ASMO</option>
										<option value="EMEA">EMEA</option>
										<option value="PRC">PRC</option>
									</select>
								</div>
							</div>
							<?php
							/*
							<div class="control-group">
								<label for="campaign_id" class="control-label">Select the campaign for the request to be associated to</label>
								<div class="controls">
									<div id="eloqua-yes">
										<span class="required-mark">*</span><select id="campaign_id" name="campaign_id" class="input-wide required">
											<option value="">Select an existing campaign</option>
											<?php
											$campaign_array = array();
						                    //$query = "SELECT id, name FROM request_campaign WHERE approved = 1 ORDER BY name ASC"; //Put back when Approved/Complete is active
						                    $query = "SELECT request_campaign.id, request_campaign.name FROM request_campaign INNER JOIN request on request_campaign.request_id = request.id WHERE request.active = 1 AND DATE(end_date) > DATE(NOW()) AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
						                    $result = $mysqli->query($query);
						                    while ($obj = $result->fetch_object()) {
						                        echo '<option value="'.$obj->id.'">'.$obj->name.'</option>';
						                        $campaign_array[] = $obj->name;
						                    }
						                    ?>
										</select>
									</div>
									<div id="eloqua-no" class="hide">
										<p><em>The non-DG webinar you are creating does not require a campaign to be associated with it.  Instead we will automatically associate it with the campaign "[campaign name here]" should you need to locate it in the campaign listing.  Or you can find the webinar in your "My Requests" section.</em></p>
									</div>
								</div>
							</div>
							*/
							?>
							<div id="campaign-search" class="control-group hide">
								<label for="campaign_search" class="control-label">Select the campaign for the request to be associated to</label>
								<div class="controls">
									<div id="eloqua-yes">
										<?php
										$campaign_array = array();
					                    $query = "SELECT request_campaign.id, request_campaign.name FROM request_campaign INNER JOIN request on request_campaign.request_id = request.id WHERE request.active = 1 AND request_campaign.id != 17 ORDER BY request_campaign.name ASC";
					                    //AND DATE(end_date) > DATE(NOW()) 
					                    $result = $mysqli->query($query);
					                    while ($obj = $result->fetch_object()) {
					                        $campaign_array[] = $obj->name;
					                    }
					                    ?>
										<span class="required-mark">*</span><input type="text" id="campaign_search" name="campaign_search" class="input-wide required" placeholder="Start typing the GEOs or keywords from the campaign name" value="">
									</div>
									<div id="eloqua-no" class="hide">
										<p><em>The non-DG webinar you are creating does not require a campaign to be associated with it. Instead we will automatically associate it with the campaign <strong>"<span id="preselect-campaign">WW.Non-Eloqua Webinar.76</span>"</strong> should you need to locate it in the campaign listing. Or you can find the webinar in your "My Requests" section.</em></p>
									</div>
								</div>
							</div>
							<div id="language-select" class="control-group hide">
								<label for="language" class="control-label">Language Localization</label>
								<div class="controls">
									<select id="language" name="language" class="input-wide">
										<?php
						                    $query_lan = "SELECT id, language_name FROM languages ORDER BY language_name ASC";
						                    $result_lan = $mysqli->query($query_lan);
						                    while ($obj_lan = $result_lan->fetch_object()) {
						                        echo '<option value="'.$obj_lan->id.'"'; if ($obj_lan->id == 1) { echo ' selected'; } echo '>'.$obj_lan->language_name.'</option>';
						                    }
						                    ?>
									</select>
								</div>
							</div>
							<div id="aprimo-marketing-id" class="control-group hide">
								<label for="marketing_id" class="control-label">Do you already have an existing Aprimo Marketing Activity ID?</label>
								<div class="controls">
									<div id="existing-question" style="margin-bottom:12px;">
										<label class="radio inline"> <input type="radio" id="existing_mid[]" name="existing_mid" value="1" <?php if (!isset($_SESSION['existing_mid']) || $_SESSION['existing_mid'] == '1') { echo 'checked'; } ?>> Yes</label>
										<label class="radio inline"> <input type="radio" id="existing_mid[]" name="existing_mid" value="0" <?php if ($_SESSION['existing_mid'] == '0') { echo 'checked'; } ?>> No</label><br />
									</div>
									<div id="existing-yes">
										<input type="text" id="marketing_id" name="marketing_id" class="input-wide" placeholder="If yes, please enter your Aprimo Marketing ID" value="">
									</div>
									<div id="existing-no" class="hide">
										<p>Since you do not have a Marketing Activity ID, we can create one for you.  Please provide some additional information for us to create the mID for you.  Following the creation of this mID, we will place this mID back into your request for your reference.</p>
										<p><strong>Source Activity Name</strong><br />
										<input type="text" id="source_activity_name" name="source_activity_name" class="input-wide" value=""></p>
										<p><strong>Source Description</strong><br />
										<textarea type="text" id="source_description" name="source_description" class="input-wide"></textarea></p>
										<p><strong>Source Program</strong><br />
										<select id="source_program" name="source_program" class="input-wide">
											<option value="">Select a source program</option>
											<option value="Education">Education</option>
											<option value="General Embedded">General Embedded</option>
											<option value="General ESS">General ESS</option>
											<option value="General Reseller">General Reseller</option>
											<option value="General WA">General WA</option>
											<option value="Intel IT Center">Intel IT Center</option>
											<option value="Retail">Retail</option>										
										</select>
										</p>
										<p><strong>Source Activity Type</strong><br />If you are unsure of which category to choose, please choose "Other".
										<select id="source_activity_type" name="source_activity_type" class="input-wide">
											<option value="">Select a source activity type</option>
											<option value="Banner Ad">Banner Ad</option>
											<option value="Blog or Discussion">Blog or Discussion</option>
											<option value="Call Out">Call Out</option>
											<option value="Content Syndication">Content Syndication</option>
											<option value="Electronic Direct Mail">Electronic Direct Mail</option>
											<option value="eNurture Campaign">eNurture Campaign</option>
											<option value="Events">Events</option>
											<option value="Field Provided List">Field Provided List</option>
											<option value="Program Registration">Program Registration</option>
											<option value="Search Engine Marketing">Search Engine Marketing</option>
											<option value="Sponsorship">Sponsorship</option>
											<option value="Third Party List Purchase">Third Party List Purchase</option>
											<option value="Training">Training</option>
											<option value="Web Registration">Web Registration</option>
											<option value="Webinar">Webinar</option>
											<option value="Other">Other</option>
										</select>
										</p>
										<p><strong>Source Offer Type</strong><br />If you are unsure of which category to choose, please choose "Other".
										<select id="source_offer_type" name="source_offer_type" class="input-wide">
											<option value="">Select a source offer type</option>
											<option value="Board Offer">Board Offer</option>
											<option value="Book Offer">Book Offer</option>
											<option value="Hands-on Lab Training">Hands-on Lab Training</option>
											<option value="White Paper">White Paper</option>
											<option value="Other">Other</option>
										</select>
										</p>
										<p><strong>Marketing Team</strong><br />If you are unsure of which category to choose, please choose "Other".
										<select id="marketing_team" name="marketing_team" class="input-wide">
											<option value="">Select a marketing team</option>
											<option value="APAC Embedded">APAC Embedded</option>
											<option value="APAC ESS">APAC ESS</option>
											<option value="APAC OSC">APAC OSC</option>
											<option value="APAC Reseller">APAC Reseller</option>
											<option value="APAC WA">APAC WA</option>
											<option value="Embedded HQ">Embedded HQ</option>
											<option value="EMEA Embedded">EMEA Embedded</option>
											<option value="EMEA ESS">EMEA ESS</option>
											<option value="EMEA OSC">EMEA OSC</option>
											<option value="EMEA WA">EMEA WA</option>
											<option value="ESS-AMG">ESS-AMG</option>
											<option value="ESS-CMG">ESS-CMG</option>
											<option value="ESS-DCSG">ESS-DCSG</option>
											<option value="ESS-HQ">ESS-HQ</option>
											<option value="IJKK OSC">IJKK OSC</option>
											<option value="PRC ESS">PRC ESS</option>
											<option value="PRC OSC">PRC OSC</option>
											<option value="Reseller-HQ">Reseller-HQ</option>
											<option value="WA-HQ">WA-HQ</option>
											<option value="Other">Other</option>
										</select>
										</p>
									</div>
								</div>
							</div>

							<div id="cloned-container" class="control-group hide">
								<label for="cloned" class="control-label">Is this a new request, or a duplicate of an existing one?</label>
								<div class="controls">
									<div id="eloqua-question" style="margin-bottom:12px;">
										<label class="radio inline"> <input type="radio" id="cloned[]" name="cloned" value="0" <?php if (!isset($_SESSION['cloned']) || $_SESSION['cloned'] == '0') { echo 'checked'; } ?>> New Request</label>
										<label class="radio inline"> <input type="radio" id="cloned[]" name="cloned" value="1" <?php if ($_SESSION['cloned'] == '1') { echo 'checked'; } ?>> Duplicate Request</label><br />
									</div>
								</div>
							</div>
							<div id="request-search" class="control-group hide">
								<label for="request_search" class="control-label">Select the request to duplicate</label>
								<div class="controls">
									<span class="required-mark">*</span><input type="text" id="request_search" name="request_search" class="input-wide required" placeholder="Start typing the name or keywords from the request name" value="">
									<p class="input-note" style="margin-left:0;">All aspects of this request will be duplicated except for the dates and tactic tag. You can edit any attribute as needed.</p>
								</div>
							</div>
						</div>

					</form>
				</div>

				<!--Contact Lists-->
	            <div id="tabbed-contacts" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-contacts">
						<li class="active"><a href="#contacts-need">Important Info</a></li>
						<li><a href="#contacts-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="contacts-need">
							<p>Please provide your contact lists in one of the provided sector-specific templates in the "Required Assets" section.  These templates are provided for your convinence and define the contact attributes we require for processing.  RAW files are accepted but will require additional time for processing.</p>
						</div>

						<div class="tab-pane" id="contacts-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Channel sector.</td>
										<td><a href="/files/Channel Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Embedded sector.</td>
										<td><a href="/files/Embedded Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the ITDM sector.</td>
										<td><a href="/files/ITDM Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!--Lead Lists-->
	            <div id="tabbed-leads" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-leads">
						<li class="active"><a href="#leads-need">Important Info</a></li>
						<li><a href="#leads-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="leads-need">
							<p>Please provide your lead lists in one of the provided sector-specific templates in the "Required Assets" section.  These templates are provided for your convinence and define the lead attributes we require for processing.  RAW files are accepted but will require additional time for processing.</p>
						</div>

						<div class="tab-pane" id="leads-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Lead Upload template you are to use if you are uploading leads for the Channel sector.</td>
										<td><a href="/files/Reseller Lead Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Lead Upload template you are to use if you are uploading leads for the Embedded sector.</td>
										<td><a href="/files/Embedded Lead Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM-WA Template (optional)</td>
										<td>This is the Lead Upload template you are to use if you are uploading leads for the ITDM-WA sector.</td>
										<td><a href="/files/ITDM-WA Lead Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM-ESS Template (optional)</td>
										<td>This is the Lead Upload template you are to use if you are uploading leads for the ITDM-ESS sector.</td>
										<td><a href="/files/ITDM-ESS Lead Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!--Webinars-->
	            <div id="tabbed-webinars" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-webinars">
						<li class="active"><a href="#webinars-need">Important Info</a></li>
						<li><a href="#webinars-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="webinars-need">
							<!--<p>Please review current webinar offerings before submitting a request. If your webcast does not fit within the current offering, you should send an email to <a href="mailto:monica.l.wellington@intel.com">Monica Wellington</a> to be added to a waitlist and have your requirements documented. <a href="/files/SMGExternalPhase1.pptx">See this document for more info</a>.</p>
							<p>This form is for webinars with 50+ expected attendees. Standard acceptance rates are around 10% of invitees will attend.  Please plan accordingly.   For fewer than 50 attendees, use Lync.</p>
							<p>Your presentation slides, resource list assets, and videos MUST be uploaded up to 72 hours before your scheduled event. Please ensure that your assets are finalized when you upload them. Changing files after production has begun can result in significant delays to your project.  Changes within 24 hours of your scheduled webinar cannot be guaranteed.</p>
							<p>If you want to submit multiple webinars at once, you must complete all the information in the multi-webinar campaign brief before we can begin.</p>-->
							<p>Select the Required Assets tab to see available templates.</p> 
							<p>Review <a href="/files/SMG_Capabilities.pptx">currently supported features and upcoming roadmap</a>.</p>
							<p>Standard acceptance rates are around 10% of invitees—plan accordingly.</p>
							<p>Presentation slides and required assets MUST be uploaded 72 hours before your scheduled event. Changes to assets could cause delays in the process. Changes within 24 hours of your scheduled webinar cannot be made.</p>
							<p>For compatibility with older mail clients, please do not use special characters (e.g. &reg;, &trade;, &copy;, etc.) for any of the email copy.</p>
						</div>

						<div class="tab-pane" id="webinars-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<!--<tr>
										<td>Campaign Brief (Multi-Webinar Request)</td>
										<td>This form includes all the content required for us to get your series of webinars setup. If you intend to have a series of webinars created, please fully complete this form.</td>
										<td><a href="/files/Campaign Brief (Multi-Webinar Request).xlsx" class="btn-green download">Download</a></td>
									</tr>-->
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Webinar Presentation Template</td>
										<td>Your webinar presentation must use this template, and consist of a single PPTX file of not more than 25MB.</td>
										<td><a href="/files/Intel_Presentation Template_16x9.potx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Contact Upload template you are to use for uploading contacts for the Channel sector.</td>
										<td><a href="/files/Channel Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Contact Upload template you are to use for uploading contacts for the Embedded sector.</td>
										<td><a href="/files/Embedded Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM Template (optional)</td>
										<td>This is the Contact Upload template you are to use for uploading contacts for the ITDM sector.</td>
										<td><a href="/files/ITDM Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!--Newsletters-->
	            <div id="tabbed-newsletter" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-newsletter">
						<li class="active"><a href="#newsletter-need">Important Info</a></li>
						<li><a href="#newsletter-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="newsletter-need">
							<p>Your HTML template and associated assets must be uploaded at least 48 hours before your first delivery date.</p>
							<p>Content for your first newsletter should be included in the HTML template that you upload to production support.  For subsequent iterations, production support will provide back a spreadsheet modelled after your template, for you to fill out and upload at least 48 hours before your next scheduled delivery date.</p>
						</div>

						<div class="tab-pane" id="newsletter-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Channel sector.</td>
										<td><a href="/files/Channel Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Embedded sector.</td>
										<td><a href="/files/Embedded Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the ITDM sector.</td>
										<td><a href="/files/ITDM Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!--eNurture-->
	            <div id="tabbed-enurture" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-enurture">
						<li class="active"><a href="#enurture-need">Important Info</a></li>
						<li><a href="#enurture-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="enurture-need">
							<p>Your HTML template and associated assets must be uploaded at least 48 hours before your first delivery date.</p>
						</div>

						<div class="tab-pane" id="enurture-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Channel sector.</td>
										<td><a href="/files/Channel Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Embedded sector.</td>
										<td><a href="/files/Embedded Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the ITDM sector.</td>
										<td><a href="/files/ITDM Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!--Email-->
	            <div id="tabbed-email" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-email">
						<li class="active"><a href="#email-need">Important Info</a></li>
						<li><a href="#email-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="email-need">
							<p>Your HTML template and associated assets must be uploaded at least 48 hours before your first delivery date.</p>
						</div>

						<div class="tab-pane" id="email-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Channel sector.</td>
										<td><a href="/files/Channel Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Embedded sector.</td>
										<td><a href="/files/Embedded Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the ITDM sector.</td>
										<td><a href="/files/ITDM Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!--Landing Page-->
	            <div id="tabbed-landingpage" class="tabbed-contents span12" style="margin-bottom:60px;margin-left:0px;display:none;">
	            	<ul class="nav nav-tabs" id="tabs-landingpage">
						<li class="active"><a href="#landingpage-need">Important Info</a></li>
						<li><a href="#landingpage-assets">Required Assets</a></li>
					</ul>
					 
					<div class="tab-content">
						<div class="tab-pane active" id="landingpage-need">
							<p>Your HTML template and associated assets must be uploaded at least 48 hours before your first delivery date.</p>
						</div>

						<div class="tab-pane" id="landingpage-assets">
							<table class="table table-striped table-bordered table-hover requests-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Intel Eloqua Template Assets</td>
										<td>New Eloqua email and landing page template assets.</td>
										<td><a href="/files/Eloqua-Template-Assets.zip" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Channel Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Channel sector.</td>
										<td><a href="/files/Channel Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>Embedded Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the Embedded sector.</td>
										<td><a href="/files/Embedded Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
									<tr>
										<td>ITDM Template (optional)</td>
										<td>This is the Contact Upload template you are to use if you are uploading contacts for the ITDM sector.</td>
										<td><a href="/files/ITDM Contact Upload Template.xlsx" class="btn-green download">Download</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>


				<div class="fieldgroup" style="text-align:center;">
					<a href="" id="submit-request-form" class="btn-green submit-disabled">Save &amp; Continue</a>
				</div>

				<div class="fieldgroup"></div>
			</div>
		</div>
	</div>


<?php include('includes/footer.php'); ?>


</body>
</html>