<?php
$loc = 'campaign-requests';
$step = 3;
$c_type = 'webinar';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

//Redirect if a Campaign ID hasn't been set
if (!isset($_SESSION['campaign_id'])) {
	header('Location:request?error=request-id');
	exit();
}
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:12px;">
			<div class="intro span12">
				<h1>Webinar Request Form</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="content-update" class="row">
			<ul class="nav nav-tabs-request fiveup">
				<li>Step 1</li>
				<li>Step 2</li>
				<li class="active">Step 3</li>
				<li>Step 4</li>
				<li>Step 5</li>
			</ul>
			<div class="request-step step2">


				<form id="content-update-form" class="form-horizontal" action="includes/_requests_webinar.php" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="action" name="action" value="create">
					<input type="hidden" id="step" name="step" value="3">
					<input type="hidden" id="c_type" name="c_type" value="webinar">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
					<input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['email']; ?>">

					<div class="fieldgroup">
						<h3 class="fieldgroup-header">Webinar Type</h3>
					</div>

					<div class="fieldgroup" style="border-bottom:0;">
						<div class="control-group">
							<label for="type_of_webinar" class="control-label" style="padding-top:0;">What type of webinar request is this?</label>
							<div class="controls">
								<span class="required-mark" style="vertical-align:top;">*</span>
								<select id="type_of_webinar" name="type_of_webinar" class="input-wide required">
									<option value="">Please select your type of webinar</option>
									<option value="single"<?php if (getFieldValue('type_of_webinar', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'single') { echo ' selected'; } ?>>Single Webinar Request</option>
									<option value="multi"<?php if (getFieldValue('type_of_webinar', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'multi') { echo ' selected'; } ?>>Multi-Webinar Request</option>
								</select>
								<div id="multi-request-text" class="<?php if (getFieldValue('type_of_webinar', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('type_of_webinar', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'single') { echo 'hide'; } ?>">
									<p class="input-note" style="width:100%;">Please download and complete the <a href="/files/Campaign Brief (Multi-Webinar Request).xlsx">Campaign Brief (Multi-Webinar Request)</a> if you haven't done so already. This form includes all the content required for us to get your series of webinars setup. Upload the completed Campaign Brief (Multi-Webinar Request) below.</p>

									<div class="upload-div" style="margin-left:64px;">
									    <div class="fileupload fileupload-new" data-provides="fileupload">
											<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Campaign Brief</span><input id="fileupload_campaignbrief" type="file" name="files[]"></span>
										</div>
										<div id="progress-campaignbrief" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 239px;display:none;">
									        <div class="bar"></div>
									    </div>
									    <div id="shown-campaignbrief" class="files" style="clear:both;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="campaign-brief-fields"<?php if (getFieldValue('type_of_webinar', $_SESSION['clone_id'], $_SESSION['request_type'], '') == '' || getFieldValue('type_of_webinar', $_SESSION['clone_id'], $_SESSION['request_type'], '') == 'multi') { echo ' class="hide"'; } ?>>
						<div class="fieldgroup" style="padding-top:0;">
							<h3 class="fieldgroup-header">Email &amp; Landing Page Template Details</h3>
						</div>

						<div class="fieldgroup" style="padding-top:10px;">
							<p style="padding-bottom:16px;">The following fields are REQUIRED — We cannot complete your request without information in all of these fields. We will reuse this information for all assets. For compatibility with older mail clients, please do not use special characters (e.g. &reg;, &trade;, &copy;, etc.) for any of the email copy.</p>
							<div class="control-group">
								<label for="event_header" class="control-label">Event Header</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><input type="text" id="event_header" name="event_header" class="input-wide required" placeholder="Will be used on all email notifications. Max characters w/spaces: 75" value="<?php echo getFieldValue('event_header', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?>" maxlength="75"><a href="img/email-01.jpg" class="cbox-image" title="Event Header"><i class="helper-icon icon-picture"></i></a></div>
							</div>
							<div class="control-group">
								<label for="event_title" class="control-label">Event Title</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><input type="text" id="event_title" name="event_title" class="input-wide required" placeholder="Will be used on all forms/reg. pages. Max characters w/spaces: 45" value="<?php echo getFieldValue('event_title', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?>" maxlength="45"><a href="img/email-02.jpg" class="cbox-image" title="Event Title"><i class="helper-icon icon-picture"></i></a></div>
							</div>
							<div class="control-group">
								<label for="paragraph_section_copy" class="control-label">Paragraph Section Copy</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="paragraph_section_copy" name="paragraph_section_copy" class="input-wide textarea-medium required" placeholder="Will be used on all forms/reg. pages. Max characters w/spaces: 300" maxlength="300"><?php echo getFieldValue('paragraph_section_copy', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea><a href="img/email-03.jpg" class="cbox-image" title="Paragraph Section Copy"><i class="helper-icon icon-picture textarea-align"></i></a></div>
							</div>
							<div class="control-group">
								<label for="bulleted_section_title" class="control-label">Bulleted Section Title</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><input type="text" id="bulleted_section_title" name="bulleted_section_title" class="input-wide required" placeholder="Will be used on all forms/reg. pages. Max characters w/spaces: 30" value="<?php echo getFieldValue('bulleted_section_title', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?>" maxlength="30"><a href="img/email-04.jpg" class="cbox-image" title="Bulleted Section Title"><i class="helper-icon icon-picture"></i></a></div>
							</div>
							<div class="control-group">
								<label for="bulleted_section_copy" class="control-label">Bulleted Section Copy</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="bulleted_section_copy" name="bulleted_section_copy" class="input-wide textarea-medium required" placeholder="Will be used on all forms/reg. pages. Max characters w/spaces: 75 each. Bullet points, max of 6." maxlength="450"><?php echo getFieldValue('bulleted_section_copy', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea><a href="img/email-05.jpg" class="cbox-image" title="Bulleted Section Copy"><i class="helper-icon icon-picture textarea-align"></i></a></div>
							</div>
							<div class="control-group">
								<label for="registration_call_to_action" class="control-label">Additional Registration Call to Action</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><input type="text" id="registration_call_to_action" name="registration_call_to_action" class="input-wide required" placeholder="Additional call to action for registration. Max characters w/spaces: 50" value="<?php echo getFieldValue('registration_call_to_action', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?>" maxlength="50"><a href="img/email-06.jpg" class="cbox-image" title="Additional Registration Call to Action"><i class="helper-icon icon-picture"></i></a></div>
							</div>
							<div class="control-group">
								<label for="email_subject_line" class="control-label">Email Subject Line</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><input type="text" id="email_subject_line" name="email_subject_line" class="input-wide required" placeholder="Will be used on all email notifications. Max characters w/spaces: 150" value="<?php echo getFieldValue('email_subject_line', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?>" maxlength="150"><a href="img/email-subject.jpg" class="cbox-image" title="Email Subject Line"><i class="helper-icon icon-picture"></i></a></div>
							</div>
							<?php include('includes/email-basics.php'); ?>
							<div class="control-group">
								<label for="registration_opt_in_copy" class="control-label">Registration Opt-In Copy</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="registration_opt_in_copy" name="registration_opt_in_copy" class="input-wide textarea-medium required" placeholder="Messaging accompanying the opt-in check box. Max characters w/spaces: 200" maxlength="200"><?php echo getFieldValue('registration_opt_in_copy', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea><a href="img/email-08.jpg" class="cbox-image" title="Registration Opt-In Copy"><i class="helper-icon icon-picture textarea-align"></i></a><p class="input-note">USE THIS COPY UNLESS YOU HAVE ALTERNATE APPROVED OPT-IN LANGUAGE<br /><strong>I agree to opt in to periodic email communications for [TOPIC HERE] from Intel. I have read the terms and conditions and agree to them. Please review the Terms of Use and Privacy Policy.</strong></p></div>
							</div>
							<div class="control-group">
								<label for="thank_you_for_attending_paragraph_section_copy" class="control-label">Thank you for attending Paragraph Section Copy</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="thank_you_for_attending_paragraph_section_copy" name="thank_you_for_attending_paragraph_section_copy" class="input-wide textarea-medium required" placeholder="Main message of the thank you email. Max characters w/spaces: 300. You can add hyperlinks without it counting against your character limit."><?php echo getFieldValue('thank_you_for_attending_paragraph_section_copy', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea><a href="img/email-09.jpg" class="cbox-image" title="Thank you for attending Paragraph Section Copy"><i class="helper-icon icon-picture textarea-align"></i></a></div>
							</div>
							<div class="control-group">
								<label for="sorry_we_missed_you_paragraph_section_copy" class="control-label">Sorry we missed you Paragraph Section Copy</label>
								<div class="controls"><span class="required-mark" style="vertical-align:top;">*</span><textarea id="sorry_we_missed_you_paragraph_section_copy" name="sorry_we_missed_you_paragraph_section_copy" class="input-wide textarea-medium required" placeholder="Main message of the sorry we missed you email. Max characters w/spaces: 300. You can add hyperlinks without it counting against your character limit."><?php echo getFieldValue('sorry_we_missed_you_paragraph_section_copy', $_SESSION['clone_id'], $_SESSION['request_type'], 'textarea'); ?></textarea><a href="img/email-10.jpg" class="cbox-image" title="Sorry we missed you Paragraph Section Copy"><i class="helper-icon icon-picture textarea-align"></i></a></div>
							</div>
						</div>
					</div>

					<div class="fieldgroup" style="padding-top:0;">
						<h3 class="fieldgroup-header">Email &amp; Landing Page Template Assets</h3>
					</div>

					<div class="fieldgroup">
						<div class="control-group">
							<div class="accordion thumbnail-selector" id="accordion1">
			                    <div class="accordion-group">
			                        <div class="accordion-heading">
			                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">(<span class="plusminus">+</span>) Select Email Featured Image <span class="normal">Optional</span></a>
			                        </div>
			                        <div id="collapseOne" class="accordion-body collapse<?php if (isset($_SESSION['files_emailfeaturedimage']) || isset($_SESSION['email_featured_image'])) { echo ' in'; } ?>">
			                            <div class="accordion-inner">
			                                <ul>
			                                	<li>
			                                		<a href="img/email-images/270x270/HS_Lab_19.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/HS_Lab_19.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="HS_Lab_19.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'HS_Lab_19.jpg') { echo ' checked'; } ?>> A</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/int_brand_682_ManTabletServers_6303.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/int_brand_682_ManTabletServers_6303.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="int_brand_682_ManTabletServers_6303.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'int_brand_682_ManTabletServers_6303.jpg') { echo ' checked'; } ?>> B</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/int_brand_685_TechServerDoor_8157.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/int_brand_685_TechServerDoor_8157.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="int_brand_685_TechServerDoor_8157.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'int_brand_685_TechServerDoor_8157.jpg') { echo ' checked'; } ?>> C</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/int_brand_728_ManWomanWorking_3717.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/int_brand_728_ManWomanWorking_3717.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="int_brand_728_ManWomanWorking_3717.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'int_brand_728_ManWomanWorking_3717.jpg') { echo ' checked'; } ?>> D</label>
			                                	</li>
			                                	<li class="last">
			                                		<a href="img/email-images/270x270/int_brand_738_aerial_factory_view.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/int_brand_738_aerial_factory_view.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="int_brand_738_aerial_factory_view.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'int_brand_738_aerial_factory_view.jpg') { echo ' checked'; } ?>> E</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/int_brand_820_ManWomanHall_2135.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/int_brand_820_ManWomanHall_2135.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="int_brand_820_ManWomanHall_2135.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'int_brand_820_ManWomanHall_2135.jpg') { echo ' checked'; } ?>> F</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/ItT_businessman_outside.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/ItT_businessman_outside.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="ItT_businessman_outside.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'ItT_businessman_outside.jpg') { echo ' checked'; } ?>> G</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/MS_Classroom_2.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/MS_Classroom_2.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="MS_Classroom_2.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'MS_Classroom_2.jpg') { echo ' checked'; } ?>> H</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/270x270/Xeon_E3_07.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/Xeon_E3_07.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="Xeon_E3_07.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'Xeon_E3_07.jpg') { echo ' checked'; } ?>> I</label>
			                                	</li>
			                                	<li class="last">
			                                		<a href="img/email-images/270x270/DoctorConsult_1982.jpg" class="cbox-image" title="270x270px"><img src="img/email-images/thumbnails/DoctorConsult_1982.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="email-featured-image[]" name="email-featured-image" value="DoctorConsult_1982.jpg"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'DoctorConsult_1982.jpg') { echo ' checked'; } ?>> J</label>
			                                	</li>
			                                </ul>

		                                	<div class="upload-div pull-left" style="margin-bottom:26px;">
											    <div class="fileupload fileupload-new" data-provides="fileupload">
													<input type="radio" id="email-featured-image[]" name="email-featured-image" value="custom"<?php if (isset($_SESSION['email_featured_image']) && $_SESSION['email_featured_image'] == 'custom') { echo ' checked'; } ?>>&nbsp;&nbsp;&nbsp;<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_emailfeaturedimage" type="file" name="files[]"></span>&nbsp;&nbsp;&nbsp;1MB max. 270x270px
												</div>
												<div id="progress-emailfeaturedimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 408px;display:none;">
											        <div class="bar"></div>
											    </div>
											    <div id="shown-emailfeaturedimage" class="files" style="clear:both;">
											    	<?php
											    	if (isset($_SESSION['files_emailfeaturedimage'])) {
											    		$filebox = '<div name="'. ($_SESSION['files_emailfeaturedimage']). '">';
								    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_emailfeaturedimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_emailfeaturedimage'].'</p>';

								    					/*This works, but needs formatting and some logic to hide stock images
								    					if (isset($_SESSION['files_emailfeaturedimage_path'])) {
								    						$filebox.= '<img src="'.$_SESSION['files_emailfeaturedimage_path'].'">';
								    					} else {
								    						$filebox.= $_SESSION['files_emailfeaturedimage'];
								    					}
								    					*/

								    					/*This works better, popup instead of inline. But the problem is the _path session var doesn't get killed if you upload a new image
								    					if (isset($_SESSION['files_emailfeaturedimage_path'])) {
								    						$filebox.= '<a href="'.$_SESSION['files_emailfeaturedimage_path'].'" class="cbox-image" title="'.$_SESSION['files_emailfeaturedimage'].'">'.$_SESSION['files_emailfeaturedimage'].'</a>';
								    					} else {
								    						$filebox.= $_SESSION['files_emailfeaturedimage'];
								    					}
								    					*/

							                    		$filebox.= '</div>';
							                    		echo $filebox;
			            							}
			            							?>
											    </div>
											</div>

		                                	<!--<a href="" class="btn-green submit pull-right" style="margin-top:18px;">Continue</a>-->
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <div class="accordion thumbnail-selector" id="accordion2">
			                    <div class="accordion-group">
			                        <div class="accordion-heading">
			                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">(<span class="plusminus">+</span>) Select Registration Page Image <span class="normal">Optional</span></a>
			                        </div>
			                        <div id="collapseTwo" class="accordion-body collapse<?php if (isset($_SESSION['files_registrationpageimage']) || isset($_SESSION['registration_page_image'])) { echo ' in'; } ?>">
			                            <div class="accordion-inner">
			                                <ul>
			                                	<li>
			                                		<a href="img/email-images/560x100/HS_Lab_19.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/HS_Lab_19.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="HS_Lab_19.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'HS_Lab_19.jpg') { echo ' checked'; } ?>> A</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/int_brand_682_ManTabletServers_6303.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/int_brand_682_ManTabletServers_6303.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="int_brand_682_ManTabletServers_6303.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'int_brand_682_ManTabletServers_6303.jpg') { echo ' checked'; } ?>> B</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/int_brand_685_TechServerDoor_8157.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/int_brand_685_TechServerDoor_8157.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="int_brand_685_TechServerDoor_8157.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'int_brand_685_TechServerDoor_8157.jpg') { echo ' checked'; } ?>> C</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/int_brand_728_ManWomanWorking_3717.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/int_brand_728_ManWomanWorking_3717.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="int_brand_728_ManWomanWorking_3717.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'int_brand_728_ManWomanWorking_3717.jpg') { echo ' checked'; } ?>> D</label>
			                                	</li>
			                                	<li class="last">
			                                		<a href="img/email-images/560x100/int_brand_738_aerial_factory_view.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/int_brand_738_aerial_factory_view.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="int_brand_738_aerial_factory_view.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'int_brand_738_aerial_factory_view.jpg') { echo ' checked'; } ?>> E</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/int_brand_820_ManWomanHall_2135.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/int_brand_820_ManWomanHall_2135.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="int_brand_820_ManWomanHall_2135.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'int_brand_820_ManWomanHall_2135.jpg') { echo ' checked'; } ?>> F</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/ItT_businessman_outside.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/ItT_businessman_outside.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="ItT_businessman_outside.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'ItT_businessman_outside.jpg') { echo ' checked'; } ?>> G</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/MS_Classroom_2.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/MS_Classroom_2.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="MS_Classroom_2.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'MS_Classroom_2.jpg') { echo ' checked'; } ?>> H</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/560x100/Xeon_E3_07.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/Xeon_E3_07.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="Xeon_E3_07.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'Xeon_E3_07.jpg') { echo ' checked'; } ?>> I</label>
			                                	</li>
			                                	<li class="last">
			                                		<a href="img/email-images/560x100/DoctorConsult_1982.jpg" class="cbox-image" title="560x100px"><img src="img/email-images/thumbnails/DoctorConsult_1982.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="registration-page-image[]" name="registration-page-image" value="DoctorConsult_1982.jpg"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'DoctorConsult_1982.jpg') { echo ' checked'; } ?>> J</label>
			                                	</li>
			                                </ul>

		                                	<div class="upload-div pull-left" style="margin-bottom:26px;">
											    <div class="fileupload fileupload-new" data-provides="fileupload">
													<input type="radio" id="registration-page-image[]" name="registration-page-image" value="custom"<?php if (isset($_SESSION['registration_page_image']) && $_SESSION['registration_page_image'] == 'custom') { echo ' checked'; } ?>>&nbsp;&nbsp;&nbsp;<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_registrationpageimage" type="file" name="files[]"></span>&nbsp;&nbsp;&nbsp;1MB max. 560x100px
												</div>
												<div id="progress-registrationpageimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 408px;display:none;">
											        <div class="bar"></div>
											    </div>
											    <div id="shown-registrationpageimage" class="files" style="clear:both;">
											    	<?php
											    	if (isset($_SESSION['files_registrationpageimage'])) {
											    		$filebox = '<div name="'. ($_SESSION['files_registrationpageimage']). '">';
								    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_registrationpageimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_registrationpageimage'].'</p>';
							                    		$filebox.= '</div>';
							                    		echo $filebox;
			            							}
			            							?>
											    </div>
											</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <div class="accordion thumbnail-selector" id="accordion3">
			                    <div class="accordion-group">
			                        <div class="accordion-heading">
			                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">(<span class="plusminus">+</span>) Select Confirmation Page Image <span class="normal">Optional</span></a>
			                        </div>
			                        <div id="collapseThree" class="accordion-body collapse<?php if (isset($_SESSION['files_confirmationpageimage']) || isset($_SESSION['confirmation_page_image'])) { echo ' in'; } ?>">
			                            <div class="accordion-inner">
			                                <ul>
			                                	<li>
			                                		<a href="img/email-images/350x350/HS_Lab_19.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/HS_Lab_19.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="HS_Lab_19.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'HS_Lab_19.jpg') { echo ' checked'; } ?>> A</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/350x350/int_brand_682_ManTabletServers_6303.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/int_brand_682_ManTabletServers_6303.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="int_brand_682_ManTabletServers_6303.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'int_brand_682_ManTabletServers_6303.jpg') { echo ' checked'; } ?>> B</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/350x350/int_brand_685_TechServerDoor_8157.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/int_brand_685_TechServerDoor_8157.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="int_brand_685_TechServerDoor_8157.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'int_brand_685_TechServerDoor_8157.jpg') { echo ' checked'; } ?>> C</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/350x350/int_brand_728_ManWomanWorking_3717.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/int_brand_728_ManWomanWorking_3717.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="int_brand_728_ManWomanWorking_3717.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'int_brand_728_ManWomanWorking_3717.jpg') { echo ' checked'; } ?>> D</label>
			                                	</li>
			                                	<li class="last">
			                                		<a href="img/email-images/350x350/int_brand_738_aerial_factory_view.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/int_brand_738_aerial_factory_view.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="int_brand_738_aerial_factory_view.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'int_brand_738_aerial_factory_view.jpg') { echo ' checked'; } ?>> E</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/780x520/int_brand_820_ManWomanHall_2135.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/int_brand_820_ManWomanHall_2135.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="int_brand_820_ManWomanHall_2135.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'int_brand_820_ManWomanHall_2135.jpg') { echo ' checked'; } ?>> F</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/350x350/ItT_businessman_outside.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/ItT_businessman_outside.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="ItT_businessman_outside.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'ItT_businessman_outside.jpg') { echo ' checked'; } ?>> G</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/350x350/MS_Classroom_2.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/MS_Classroom_2.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="MS_Classroom_2.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'MS_Classroom_2.jpg') { echo ' checked'; } ?>> H</label>
			                                	</li>
			                                	<li>
			                                		<a href="img/email-images/350x350/Xeon_E3_07.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/Xeon_E3_07.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="Xeon_E3_07.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'Xeon_E3_07.jpg') { echo ' checked'; } ?>> I</label>
			                                	</li>
			                                	<li class="last">
			                                		<a href="img/email-images/350x350/DoctorConsult_1982.jpg" class="cbox-image" title="350x350px"><img src="img/email-images/thumbnails/DoctorConsult_1982.jpg"></a><br />
			                                		<label class="radio inline"><input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="DoctorConsult_1982.jpg"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'DoctorConsult_1982.jpg') { echo ' checked'; } ?>> J</label>
			                                	</li>
			                                </ul>

		                                	<div class="upload-div pull-left" style="margin-bottom:26px;">
											    <div class="fileupload fileupload-new" data-provides="fileupload">
													<input type="radio" id="confirmation-page-image[]" name="confirmation-page-image" value="custom"<?php if (isset($_SESSION['confirmation_page_image']) && $_SESSION['confirmation_page_image'] == 'custom') { echo ' checked'; } ?>>&nbsp;&nbsp;&nbsp;<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_confirmationpageimage" type="file" name="files[]"></span>&nbsp;&nbsp;&nbsp;1MB max. 350x350px
												</div>
												<div id="progress-confirmationpageimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 408px;display:none;">
											        <div class="bar"></div>
											    </div>
											    <div id="shown-confirmationpageimage" class="files" style="clear:both;">
											    	<?php
											    	if (isset($_SESSION['files_confirmationpageimage'])) {
											    		$filebox = '<div name="'. ($_SESSION['files_confirmationpageimage']). '">';
								    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_confirmationpageimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> '.$_SESSION['files_confirmationpageimage'].'</p>';
							                    		$filebox.= '</div>';
							                    		echo $filebox;
			            							}
			            							?>
											    </div>
											</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <div class="accordion thumbnail-selector" id="accordion4">
			                    <div class="accordion-group">
			                        <div class="accordion-heading">
			                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour">(<span class="plusminus">+</span>) Footer Logo <span class="normal">Optional</span></a>
			                        </div>
			                        <div id="collapseFour" class="accordion-body collapse<?php if (isset($_SESSION['files_footerimage']) || isset($_SESSION['files_footerimage'])) { echo ' in'; } ?>">
			                            <div class="accordion-inner" style="padding-top: 0px;">
		                                	<div class="upload-div pull-left" style="margin-bottom:26px;">
											    <div class="fileupload fileupload-new" data-provides="fileupload">
													<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Custom Image</span><input id="fileupload_footerimage" type="file" name="files[]"></span>&nbsp;&nbsp;&nbsp;500KB max. 125x50px
												</div>
												<div id="progress-footerimage" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 408px;display:none;">
											        <div class="bar"></div>
											    </div>
											    <div id="shown-footerimage" class="files" style="clear:both;">
											    	<?php
											    	if (isset($_SESSION['files_footerimage'])) {
											    		$filebox = '<div name="'. ($_SESSION['files_footerimage']). '">';
								    					$filebox .= '<p><a class="btn btn-danger delete-file" data="' . ($_SESSION['files_footerimage']) . '" href="#"><i class="icon-trash icon-white"></i></a> ' . $_SESSION['files_footerimage'] . '</p>';
							                    		$filebox.= '</div>';
							                    		echo $filebox;
			            							}
			            							?>
											    </div>
											</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>


			            </div>
					</div>

					<!--

					<div class="fieldgroup">
						<div class="control-group">
							<label for="customassets" class="control-label no-padding">Email &amp; Landing Page Template Assets</label>
							<div class="controls" style="margin-left:244px;">
								<p>Please provide any custom images you would like to include in your invitation/registration/reminder/follow-up emails.</p>
								<p>Below are the file sizes and dimensions for the optional imagery you may choose to use in your communications. If you opt to use these images, name the asset using the titles below.</p>
								<ul>
									<li>Email Featured Image (used in all emails): 1MB max. 270x270px</li>
									<li>Footer Logo (optional): 500KB max. 125x50px</li>
									<li>Registration Graphic (optional): 1MB max. 560x100px</li>
									<li>Confirmation Page Featured Image (optional – can use email image here as well): 1MB max. 350x350px</li>
								</ul>
								<p>If no images are provided, we will use the <a href="img/email-confirm-default.jpg" class="cbox-image" title="Default Email Confirmation Image">following image</a> as the default image in emails.</p>
								<div class="upload-div">
								    <div class="fileupload fileupload-new" data-provides="fileupload">
										<span class="btn-file btn-green add" style="width:172px;"><span class="fileupload-new">Upload Asset Zip</span><input id="fileupload_customassets" type="file" name="files[]"></span>
									</div>
									<div id="progress-customassets" class="progress progress-success progress-striped pull-left span3" style="margin-top: -33px;margin-left: 239px;display:none;">
								        <div class="bar"></div>
								    </div>
								    <div id="shown-customassets" class="files" style="clear:both;">
								    	<?php
								    	if (isset($_SESSION['files_customassets'])) {
								    		echo $_SESSION['files_customassets'];
            							}
            							?>
								    </div>
								</div>
							</div>
						</div>
					</div>

					-->

					<div class="fieldgroup">
						<p style="margin:0 0 40px 4px;"><a href="request-webinar-2" class="btn-green back pull-left" data-analytics-label="Go Back">Go Back</a><a href="" id="submit-request-form" class="btn-green submit pull-right" data-analytics-label="Submit Form: Webinar Request: Step 3">Next</a></p>
					</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>
