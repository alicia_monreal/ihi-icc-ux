<?php
session_start();
//Redirect to Home if not admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/home');
    die();
}

$loc = 'manage-users';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
            	<h1>Manage Users</h1>
            	<p></p>
                <?php include('includes/user-list.php'); ?>
            </div>
        </div>
    </div>
    <script>
    function approveUser(url) {
    	if (window.confirm("Are you sure you want to approve this user?")) { 
			window.location.href = url;
		}
    }
    function denyUser(url) {
    	if (window.confirm("Are you sure you want to mark this user as inactive?")) { 
			window.location.href = url;
		}
    }

    </script>
<?php include('includes/footer.php'); ?>