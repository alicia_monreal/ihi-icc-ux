<?php
$loc = 'my-account';
include('includes/head.php');
include('includes/header.php');

function generateFormToken($form) {
    $token = md5(uniqid(microtime(), true));  
    $_SESSION[$form.'_token'] = $token; 
    
    return $token;
}
$newToken = generateFormToken('account-update-form'); 
?>

	<div class="container">
		<div class="row intro-body" style="margin-bottom:15px;">
			<div class="intro span12">
				<h1>My Account</h1>
				<?php
				if (isset($_GET['success'])) {
					echo '<h4 class="account-update-form" style="color:#ff0000;">';
					if ($_GET['success'] == 'true') {
						echo 'Your changes have been saved.';
					} else { 
						echo 'Sorry, there was a problem updating your account. Please try again or contact us for support.';
					}
					echo '</h4>';
				}
				?>
				<?php
				if (isset($_GET['new'])) {
					echo '<p>Thank you for creating an account with Intel Customer Connect. Please create a new password below.</p>';
				} else { 
					echo '<p>Please use this page to make changes to your account information.</p>';
				}
				?>
				
			</div>
		</div>
	</div>

	<div class="container">
		<div id="account-update" class="row">
			<form id="account-update-form" action="includes/_user-account.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
				<input type="hidden" name="token" value="<?php echo $newToken; ?>">

				<div class="span8">
					<p><input type="text" id="fname" name="fname" placeholder="First Name" value="<?php echo $_SESSION['fname']; ?>" class="input-xlarge required" autocomplete="off"></p>
					<p><input type="text" id="lname" name="lname" placeholder="Last Name" value="<?php echo $_SESSION['lname']; ?>" class="input-xlarge required" autocomplete="off"></p>
					<p><input type="text" id="email" name="email" placeholder="Email" value="<?php echo $_SESSION['email']; ?>" class="input-xlarge email required" autocomplete="off"></p>
					<p><input type="text" id="title" name="title" placeholder="Title" value="<?php echo $_SESSION['title']; ?>" class="input-xlarge" autocomplete="off"></p>
					<p><input type="text" id="company" name="company" placeholder="Company" value="<?php echo $_SESSION['company']; ?>" class="input-xlarge required" autocomplete="off"></p>
					<?php if ($_SESSION['role'] == 2) { ?>
					<p><input type="text" id="point_of_contact" name="point_of_contact" placeholder="Who is your Intel point of contact?" value="<?php echo $_SESSION['point_of_contact']; ?>" class="input-xlarge" autocomplete="off"></p>
					<p><input type="text" id="point_of_contact_email" name="point_of_contact_email" placeholder="Point of contact email address" value="<?php echo $_SESSION['point_of_contact_email']; ?>" class="input-xlarge" autocomplete="off"></p>
					<?php } ?>
					<p><label for="password">Create a new password. It is recommended that you not use your Intel password for this portal.</label>
					<input type="password" id="password" placeholder="New Password" name="password" value="" class="input-xlarge" autocomplete="off"></p>
					<p><input type="password" id="password_confirm" placeholder="Password Confirm" name="password_confirm" value="" class="input-xlarge" autocomplete="off"></p>
					<p style="margin:0 0 40px 0;"><input type="submit" class="btn-green submit submit-btn" data-analytics-label="Submit Form: Update User Account" value="Update"></p>
				</div>
			</form>
		</div>
	</div>
<?php include('includes/footer.php'); ?>