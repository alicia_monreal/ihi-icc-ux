<?php
$loc = 'solutions';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
                <h1>Add New Resource</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
        	<div class="span12">

        		<table class="survey-table">
						<tbody>
							<tr>
								<td style="width:287px;"><strong>Title</strong><br />This is helpful text</td>
								<td align="center"><input type="text" id="" name="" value=""></td>
							</tr>
							<tr>
								<td><strong>Description</strong></td>
								<td align="center"><textarea id="" name="" value=""></textarea></td>
							</tr>
							<tr>
								<td><strong>Type</strong></td>
								<td align="center"><select type="text" id="" name=""><option value="">Select type</option><option value="">Powerpoint</option><option value="">Word</option></select></td>
							</tr>
							<tr>
								<td><strong>Role</strong></td>
								<td align="center"><select type="text" id="" name=""><option value="">Select role</option><option value="">Role 1</option><option value="">Role 2</option></select></td>
							</tr>
							<tr>
								<td><strong>Thumbnail</strong></td>
								<td align="center"><input type="text" id="" name="" value="" placeholder="Popup image selector"></td>
							</tr>
							<tr>
								<td><strong>Publish Date</strong></td>
								<td align="center"><input type="text" id="" name="" value="" placeholder="Popup date field"></td>
							</tr>
							<tr>
								<td><strong>URL</strong><br />Optional if it isn't a locally linked file</td>
								<td align="center"><input type="text" id="" name="" value=""></td>
							</tr>
							<tr>
								<td><strong>YouTube/Vimeo URL</strong><br />Optional if it's an external video<br />(maybe only show if Type=Video)</td>
								<td align="center"><input type="text" id="" name="" value=""></td>
							</tr>
							<tr>
								<td valign="top"><strong>Location</strong></td>
								<td>
									<p>Home Page<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Resources<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Latest Updates<br />
									</p>
									<p>&nbsp;</p>
									<p>Solutions<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Webinars<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Marketing Automation<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Partner Sales<br />
									</p>
									<p>&nbsp;</p>
									<p>Training<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Webinars<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Marketing Automation<br />
										&nbsp;&nbsp;<input type="checkbox" id="" name="" value=""> Partner Sales<br />
									</p>
									<p>&nbsp;</p>
								</td>
							</tr>
						</tbody>
					</table>

             </div>
        </div>
    </div>

<?php include('includes/footer.php'); ?>