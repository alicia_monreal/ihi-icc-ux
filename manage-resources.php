<?php
session_start();
//Redirect to Home if not admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] != 1) {
    header('Location:/home');
    die();
}

$loc = 'manage-resources';
include('includes/head.php');
include('includes/header.php');
?>

   <div class="container">
        <div class="row intro-body">
            <div class="intro span12">
            	<?php if (isset($_GET['action']) && $_GET['action'] == 'create') { echo '<h1>Create a New Resource</h1><p>&nbsp;</p>'; } else { echo '<h1>Manage Resources</h1>'; } ?>
            	
                <?php if (!isset($_GET['id']) && !isset($_GET['action'])) { ?><p><a href="manage-resources?action=create" class="btn-green add">Create a New Resource</a></a></p><?php } ?>
                <?php include('includes/resource-list.php'); ?>
            </div>
        </div>
    </div>
    <script>
    function deleteResource(url) {
        if (window.confirm("Are you sure you want to delete this resource?")) { 
            window.location.href = url;
        }
    }

    </script>

<?php include('includes/footer.php'); ?>