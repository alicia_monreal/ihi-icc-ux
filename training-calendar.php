<?php
$loc = 'training-calendar';
include('includes/head.php');
include('includes/header.php');
include('includes/_globals.php');

$single = false;
$query = "SELECT *, DATE_FORMAT( date,  '%m/%d/%Y' ) AS date_cleaned FROM trainings WHERE active = 1";

if (isset($_GET['eid'])) {
	$single = true;
	$query.=" AND id = '".$_GET['eid']."'";
}
$query.=" ORDER BY date ASC";

$result = $mysqli->query($query);
$row_cnt = $result->num_rows;
?>

	<?php if ($single == false) { ?>
	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
            	<div class="pull-right form-inline">
					<div class="btn-group">
						<button class="btn btn-default" data-calendar-nav="prev">Prev</button>
						<button class="btn" data-calendar-nav="today">Today</button>
						<button class="btn btn-default" data-calendar-nav="next">Next</button>
					</div>
					<div class="btn-group">
						<button class="btn btn-default" data-calendar-view="year">Year</button>
						<button class="btn btn-default active" data-calendar-view="month">Month</button>
						<button class="btn btn-default" data-calendar-view="week">Week</button>
						<button class="btn btn-default" data-calendar-view="day">Day</button>
					</div>
				</div>

                <h1></h1>
                <div id="calendar" style="margin-top:32px;"></div>
            </div>
        </div>
    </div>
    <?php } ?>

    <!--
	
	<?php
	//SINGLE RECORD
	if ($single && $row_cnt > 0) {
        while ($obj = $result->fetch_object()) {
    ?>
	<div class="container">
        <div class="row intro-body" style="margin-top:35px;margin-bottom:20px;">
            <div class="intro span12">
                <h1><?php echo $obj->title; ?></h1>
            </div>
        </div>
    </div>

    <div class="container">
    	<div class="row">
    		<div class="single-request">
		        <?php
		        echo '<div class="row odd"><label>Session Link</label> <div class="pull-left"><a href="'.$obj->description.'" target="_blank">'.$obj->description.'</a></div></div>';
		        echo '<div class="row even"><label>Date</label> <div class="pull-left">'.$obj->date_cleaned.'</div></div>';
		        echo '<div class="row odd"><label>Time</label> <div class="pull-left">'.$obj->time.'</div></div>';
		        echo '<div class="row even"><label>Bridge</label> <div class="pull-left">'.$obj->bridge.'</div></div>';
		        echo '<div class="row odd"><label>Organizer</label> <div class="pull-left">'.$obj->organizer.'';
		        if ($obj->organizer_phone != '') {
		        	echo '<br />'.$obj->organizer_phone;
		        }
		        if ($obj->organizer_email != '') {
		        	echo '<br /><br /><a href="mailto:'.$obj->organizer_email.'" class="btn-green">Contact Organizer</a>';
		        }
		        echo '</div></div>';
				?>
				<p style="margin-top:20px;"><a href="training" class="btn-green pull-right">View All</a></p>
			</div>
        </div>
    </div>

    <?php
    	}
    } else {
    //ALL RECORDS
    ?>

    <div class="container">
    	<div class="row">
    		<div class="single-request">
		        <?php
		        $cnt = 1;
		        for ($i = 0; $obj = $result->fetch_object(); ++$i) {
		        	if ($i/2) { $class = 'even'; } else { $class = 'odd'; }
					echo '<div class="row '.$class.' grouped" style="padding-top:15px;"><label>Title</label> <div class="pull-left">'.$obj->title.'</div></div>';
		        	echo '<div class="row '.$class.' grouped"><label>Session Link</label> <div class="pull-left"><a href="'.$obj->description.'" target="_blank">'.$obj->description.'</a></div></div>';
		        	echo '<div class="row '.$class.' grouped"><label>Date</label> <div class="pull-left">'.$obj->date_cleaned.'</div></div>';
		        	echo '<div class="row '.$class.' grouped" style="padding-bottom:15px;"><label></label> <div class="pull-left"><a href="training?eid='.$obj->id.'" class="btn-green">Learn More</a></div></div>';
				}
		        ?>
		    </div>
        </div>
    </div>
    <?php
	}
	?>

	-->

	<!--Modals-->
	<div class="modal hide fade" id="events-modal">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3 style="font-size:26px;">Event</h3>
	    </div>
	    <div class="modal-body" style="height: 400px">
	    </div>
	    <div class="modal-footer">
	        <a href="#" data-dismiss="modal" class="btn">Close</a>
	    </div>
	</div>

<?php include('includes/footer.php'); ?>