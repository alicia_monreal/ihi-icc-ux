<?php
$loc = 'home';
include('includes/head.php');
include('includes/header.php');
?>

    <!-- Hero -->
	<div class="hero">
		<div class="container" style="position: relative;">
            <div id="slideshow">
                <div class="slideshow-container">
                    <div class="left">
                        <h1>Windows Embedded Website Update Portal</h1>
                    </div>
                    <div class="right"><a class="various" href="http://www.youtube.com/embed/X9Dg09HwYEA?rel=0&amp;wmode=transparent" data-analytics-label="Watch Video (Marquee): Unlock Business Intelligence with Windows Embedded 8"><img src="img/thumbnails/video-fpo.jpg" alt="The Internet of Things: Changing the Way Business Works"></a></div>
                </div>
                <div class="slideshow-container">
                    <div class="left">
                        <h1>Lorem Ipsum dolor sit amet consectetur adipisicing</h1>
                    </div>
                    <div class="right"><a class="various" href="http://www.youtube.com/embed/X9Dg09HwYEA?rel=0&amp;wmode=transparent" data-analytics-label="Watch Video (Marquee): Unlock Business Intelligence with Windows Embedded 8"><img src="img/thumbnails/video-fpo.jpg" alt="The Internet of Things: Changing the Way Business Works"></a></div>
                </div>
                <div class="slideshow-container">
                    <div class="left">
                        <h1>Lorem Ipsum dolor sit amet consectetur adipisicing</h1>
                    </div>
                    <div class="right"><a class="various" href="http://www.youtube.com/embed/X9Dg09HwYEA?rel=0&amp;wmode=transparent" data-analytics-label="Watch Video (Marquee): Unlock Business Intelligence with Windows Embedded 8"><img src="img/thumbnails/video-fpo.jpg" alt="The Internet of Things: Changing the Way Business Works"></a></div>
                </div>
            </div>
		</div>
	</div>

    <div class="container">
    	<div class="row intro-body">
	    	<div class="intro span7">
	    		<h1>Your resource for updating the Windows Embedded website</h1>
	    		<p>Whether you're launching a product, embarking on a new campaign, updating technical information or offering new resources to enterprises, partners, developers and others, this website has all the information and tools you need to put the power of the Windows Embedded website behind your effort. On this microsite, you'll find everything you need to effectively and efficiently submit and track your update requests, including style and content guidelines, forms and processes for submissions, social media guidelines and handy tips to make your content resonate with your intended audience.</p>
			</div>

			<div class="accordion-menu span4 pull-right">
                <div class="accordion-head green-arrow"><strong>Resources</strong> - Find guidelines for update requests below.<div class="learn-more"><a href="briefing-docs">Learn More</a></div></div>
                <div class="accordion" id="accordion2">
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                        Resources
                      </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                      <div class="accordion-inner">
                        <a href="files/" target="_blank" data-analytics-label="Download File: User's Guide">User's Guide</a>
                      </div>
                      <div class="accordion-inner">
                        <a href="files/" target="_blank" data-analytics-label="Download File: Design, editorial and social media guidelines">Design, editorial and social media guidelines</a>
                      </div>
                      <div class="accordion-inner">
                        <a href="https://brandtools.microsoft.com/Windows/Windows_Embedded/Pages/StoryBoard.aspx#story-Landing0" target="_blank" data-analytics-label="Download File: Brand guidelines">Brand guidelines</a>
                      </div>
                      <div class="accordion-inner">
                        <a href="webwebteam@microsoft.com" data-analytics-label="Resources: Contact us">Contact us</a>
                      </div>
                    </div>
                  </div>
                </div>
			</div>
		</div>
    </div>

    <div class="row odd">
    	<div class="container">
    		<div class="span5 pull-left calendar">
    			<h1>Content calendar</h1>
                <p>Find information about planned content updates on WindowsEmbedded.com and other owned channels.</p>
                <ul>
                    <li><a href="">06/04/2013 - Lorem ipsum dolor sit amet</a></li>
                    <li><a href="">06/09/2013 - Lorem ipsum dolor</a></li>
                    <li><a href="">06/14/2013 - Lorem ipsum dolor sit</a></li>
                    <li><a href="">07/01/2013 - Lorem ipsum dolor sit amet conseciture</a></li>
                </ul>
                <p><a class="btn-orange various" href="" data-analytics-label="View All: Content Calendar">View All</a></p>
    		</div>
    		<div class="span5 pull-right calendar">
                <h1>Content performance</h1>
                <p>Track and optimize your content's performance by monitoring our website metrics, updated in near-real-time.</p>
                <ul>
                    <li><a href="">06/04/2013 - Lorem ipsum dolor sit amet</a></li>
                    <li><a href="">06/09/2013 - Lorem ipsum dolor</a></li>
                    <li><a href="">06/14/2013 - Lorem ipsum dolor sit</a></li>
                    <li><a href="">07/01/2013 - Lorem ipsum dolor sit amet conseciture</a></li>
                </ul>
                <p><a class="btn-orange various" href="" data-analytics-label="View All: Website Updates">View All</a></p>
            </div>
    	</div>
    </div>

    <div class="row dark-gray">
        <div class="container">
            <div class="span12">
                <div class="span7 pull-left">
                    <h4>Featured Update</h4>
                    <p>When Steven Bridgeland and his Windows Embedded Compact 2013 team planned their product launch, they knew they’d rely on the Windows Embedded website to serve as a source for enterprises, OEMs and partners looking for product information and related technical resources. Bridgeland wrote and submitted content for multiple product pages, his team provided the assets, and the pages went live on June 13, 2013.</p>
                    <p><a href="http://www.microsoft.com/windowsembedded/en-us/windows-embedded-compact-2013.aspx" class="btn-orange" data-analytics-label="Button: Featured Update - Steven Bridgeland">Learn More</a></p>
                </div>
                <div class="pull-right">
                    <a class="various fancybox.iframe" href="http://www.youtube.com/embed/M2xCOJM4OEY?rel=0&amp;wmode=transparent" data-analytics-label="Watch Video: KUKA"><img src="img/fpo-338.png" alt="" class="first"></a>
                </div>
            </div>
        </div>
    </div>


    <!-- Modals -->
    <!-- Ovum -->
    <div id="download-ovum" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="download-ovumLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="download-ovumLabel" class="ovum-form-label">Want to learn more about Intelligent Systems?</h3>
        <h4 class="ovum-form-subtitle">Download a free healthcare report from Ovum.</h4>
      </div>
      <div class="modal-body">
        <p class="ovum-form-link"></p>
        <form action="" id="ovum-form" class="download-form" data-form="ovum">
            <fieldset>
                <p><input type="text" name="fname" id="fname" placeholder="First Name" value="" class="required"></p>
                <p><input type="text" name="lname" id="lname" placeholder="Last Name" value="" class="required"></p>
                <p><input type="text" name="title" id="title" placeholder="Job Title" value="" class="required"></p>
                <p><input type="text" name="company" id="company" placeholder="Company" value="" class="required"></p>
                <p><input type="text" name="email" id="email" placeholder="Email" value="" class="required email"></p>
                <p><input type="text" name="country" id="country" placeholder="Country" value="" class="required"></p>
                <p class="pull-right"><a href="" class="btn-orange submit" data-analytics-label="Submit Form: Ovum">Submit</a></p>
            </fieldset>
        </form>
      </div>
    </div>

    <!-- Gartner -->
    <div id="download-gartner" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="download-gartnerLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="download-gartnerLabel" class="gartner-form-label">Want to learn more about Intelligent Systems?</h3>
        <h4 class="gartner-form-subtitle">Download a free retail report from Gartner.</h4>
      </div>
      <div class="modal-body">
        <p class="gartner-form-link"></p>
        <form action="" id="gartner-form" class="download-form" data-form="gartner">
            <fieldset>
                <p><input type="text" name="fname" id="fname" placeholder="First Name" value="" class="required"></p>
                <p><input type="text" name="lname" id="lname" placeholder="Last Name" value="" class="required"></p>
                <p><input type="text" name="title" id="title" placeholder="Job Title" value="" class="required"></p>
                <p><input type="text" name="company" id="company" placeholder="Company" value="" class="required"></p>
                <p><input type="text" name="email" id="email" placeholder="Email" value="" class="required email"></p>
                <p><input type="text" name="country" id="country" placeholder="Country" value="" class="required"></p>
                <p class="pull-right"><a href="" class="btn-orange submit" data-analytics-label="Submit Form: Gartner">Submit</a></p>
            </fieldset>
        </form>
      </div>
    </div>

    <!-- ARC Strategies -->
    <div id="download-arc" class="download-modal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="download-arcLabel" aria-hidden="true">
      <div class="modal-header">
        <img src="img/close.png" class="close" data-dismiss="modal" aria-hidden="true">
        <h3 id="download-arcLabel" class="arc-form-label">Want to learn more about Intelligent Systems?</h3>
        <h4 class="arc-form-subtitle">Download a free manufacturing report from ARC.</h4>
      </div>
      <div class="modal-body">
        <p class="arc-form-link"></p>
        <form action="" id="arc-form" class="download-form" data-form="arc">
            <fieldset>
                <p><input type="text" name="fname" id="fname" placeholder="First Name" value="" class="required"></p>
                <p><input type="text" name="lname" id="lname" placeholder="Last Name" value="" class="required"></p>
                <p><input type="text" name="title" id="title" placeholder="Job Title" value="" class="required"></p>
                <p><input type="text" name="company" id="company" placeholder="Company" value="" class="required"></p>
                <p><input type="text" name="email" id="email" placeholder="Email" value="" class="required email"></p>
                <p><input type="text" name="country" id="country" placeholder="Country" value="" class="required"></p>
                <p class="pull-right"><a href="" class="btn-orange submit" data-analytics-label="Submit Form: ARC Strategies">Submit</a></p>
            </fieldset>
        </form>
      </div>
    </div>

<?php include('includes/footer.php'); ?>